classdef LTIOperator
    %LTIOperator Linear yime invariant (LTI) convolution operator,
    %represented by its finite-dimensional realisation.
    properties (SetAccess = protected)
        Name; % (string)
        N; % (1)  dimension
        % -- Time-domain realisation
        A; % (NxN) state matrix
        B; % (NxNi) control matrix
        C; % (NoxN) observation matrix
        D; % (NoxNi) feedthrough matrix
    end
    methods
        function obj = LTIOperator(A,B,C,D,varargin)
        %LTIOperator Constructor.
        % Inputs:
        %   A,B,C,D State-space representation
        % Inputs (optional):
        %   Name (string)
                % -- Validate arguments
            validateattributes(A,{'numeric'},{'square'},mfilename,'A');
            validateattributes(B,{'numeric'},{'vector','nrows',size(A,1)},mfilename,'B');
            validateattributes(C,{'numeric'},{'vector','ncols',size(A,1)},mfilename,'C');
            validateattributes(D,{'numeric'},{'vector','size',[size(C,1),size(B,2)]},mfilename,'D');
                    % Optional
            p = inputParser; p.FunctionName = mfilename; p.KeepUnmatched=1;
            addParameter(p,'Name','',@(x)(validateattributes(x,{'char'},{})));
            parse(p,varargin{:});  oA = p.Results;
                % -- Set up fields
            obj.A=A;
            obj.B=B;
            obj.C=C;
            obj.D=D;
            obj.Name = oA.Name;
            obj.N = size(A,1);
        end
        
        function M = computeMaxElement(obj)
        % computeMaxElement Compute maximum element of state-space
        % representation
            M = max(abs([obj.A(:);abs(obj.B(:));abs(obj.C(:));abs(obj.D(:))]));  
            M = full(M);
        end
        function Z = Laplace(obj,s)
        %Laplace Laplace transform (i.e. that of the approximated
        %diffusive operator).
        % Inputs:
        %   s (1) complex Laplace variable (rad/s)
        % Outputs:
        %   Z (NoxNi) Laplace transform Z(s)
        validateattributes(s,{'numeric'},{'scalar'});
            Z = obj.C*((s*eye(size(obj.A))-obj.A)\obj.B) + obj.D;
        end
        
        function r = plus(obj1,obj2)
        %plus Addition of two LTIOperator of same size.
            r = LTIOperator(obj1.A+obj2.A,...
                obj1.B+obj2.B,...
                obj1.C+obj2.C,...
                obj1.D+obj2.D,...
                'Name',[obj1.Name,' + ', obj2.Name]);
        end
    end
end