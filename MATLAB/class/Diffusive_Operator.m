classdef Diffusive_Operator
    %Diffusive_Operator Small class that describes a diffusive operator,
    %seen as an observer of a diagonal state (known as the diffusive
    %variables).
    
    properties
        Name; % (string) Identifier
        % Angular frequencies associated with the diffusive variables phi_xi
        xi; % (Nxi)
        % Observer
        C; % (Nxi) output matrix
        D; % (1) feedtrough matrix
        dt; % (1) delay value (s)
        % Exact Laplace transform (complex-valued)
        Ld_ex; % (PxR)->(PxR)
    end
    
    methods
        function obj = Diffusive_Operator(Name,Ld_ex, xi,C, D, dt)
        %Diffusive_Operator Class constructor.
        %Input:
        %   Name (string) Identifier
        %   xi (Nxi) Angular frequency of diffusive variables
        %   Ld_ex (PxR)->(PxR) Laplace transform (complex-valued)
        %   C (Nxi) outpout matrix
        %   D (1) feedthrough matrix
        %   dt (1) delay value (>=0) (s)

                % -- Arguments check
        validateattributes(Name,{'char'},{});
        validateattributes(xi,{'numeric'},{'vector'});
        Nxi = length(xi);
        validateattributes(Ld_ex,{'function_handle'},{});
        x = linspace(1,10,10);
        y = linspace(-10,10,10);
        [X,Y] = meshgrid(x,y);
        S = X + 1i*Y;
        validateattributes(Ld_ex(S),{'numeric'},{'size',size(S)});
        validateattributes(C,{'numeric'},{'vector','numel',Nxi});      
        validateattributes(D,{'numeric'},{'scalar'});
        validateattributes(dt,{'numeric'},{'scalar','nonnegative'});        
                % ---
        obj.Name = Name;
        obj.xi = xi;
        obj.C = C;
        obj.D = D;
        obj.dt = dt;
        obj.Ld_ex = Ld_ex;
        end
        
        function Z = Ld(obj,s)
        %Ld Effective Laplace transform, i.e. that of the approximated
        %diffusive operator.
        % Inputs:
        %   s (PxR) complex Laplace variable (rad/s)
        % Outputs:
        %   Z (PxR) Laplace transform Z(s)
        
        validateattributes(s,{'numeric'},{'2d'});
        
        Nxi = length(obj.xi);
        A = zeros(size(s,1),size(s,2),Nxi);
        for i=1:Nxi
            A(:,:,i) = obj.C(i)./(s+obj.xi(i));
        end
        Z = sum(A,3) + obj.D;
            % delay
        Z = exp(-obj.dt*s).*Z;
        end
        
        function PlotLaplace(obj,x,y)
        %PlotLaplace Plot Laplace transform of the diffusive operator.
        %TODO
        
        end
        
        function PlotBodeDiagaram(obj,x,y)
        %PlotLaplace Plot Bode diagrams, comparing exact and approximated
        %operators.
        %TODO
        
        end
        
        function PlotReIm(obj,f)
        %PlotReIm Plot Re-Im diagrams, comparing exact and approximated
        %Laplace Transform.
        % Inputs:
        %   f (P)   frequency (Hz)
        
            validateattributes(f,{'numeric'},{'vector'});
            fig=clf;
            lg = sprintf('Exact');
            plotComplex(fig,f,obj.Ld_ex(1i*2*pi*f),'b',lg);
            lg = sprintf('Approx.');
            plotComplex(fig,f,obj.Ld(1i*2*pi*f),'r',lg);
            xlabel('Frequency (Hz)');
            subplot(1,2,1)
            title(sprintf('Diffusive operator: %s',obj.Name));
        end
    end
    
end

