classdef DiffusiveOperator < LTIOperator
%DiffusiveOperator Linear time invariant convolution operator that admits a
%diffusive realisation (also: of diffusive type).
    
    properties (SetAccess = protected)
        mu; % (N) weight
        xi; % (N) poles
        Laplace_ex; % (PxR) -> (PxR) Exact Laplace transform
        Type; % (string)
    end
    
    methods
        function obj = DiffusiveOperator(mu,xi,varargin)
        %DiffusiveOperator Class constructor.
        % Inputs:
        %   mu (N)  weight
        %   xi (N)  angular frequency
        % Inputs (optional):
        %   Name (string)
        %   Laplace  (?) Exact Laplace transform
        %   Type (string) 'Standard' (default) or 'Extended'
        
            % -- Check arguments
        validateattributes(mu,{'numeric'},{'vector'},mfilename,'mu');
        validateattributes(xi,{'numeric'},{'vector','numel',length(xi)},mfilename,'xi');
                    % Optional
        psub = inputParser; psub.FunctionName = mfilename; psub.KeepUnmatched=1;
        addParameter(psub,'Laplace',@(s)(zeros(size(s))),@(x)(checkLaplaceTransform(x)));
        addParameter(psub,'Type','Standard',@(x)(checkDiffusiveType(x)));
        parse(psub,varargin{:});  psuboA = psub.Results;
            % -- Construct superclass
        A = -sparse(1:length(xi),1:length(xi),xi(:)); %A = -diag(xi)
        B = ones(length(xi),1);
        if strcmp(psuboA.Type,'Standard')
            C = transpose(mu(:));
            D = 0;
        elseif strcmp(psuboA.Type,'Extended')
            C = -transpose(xi(:).*mu(:));
            D = sum(mu);
        end
        obj@LTIOperator(A,B,C,D,varargin{:});
            % -- Construct class
        obj.mu = mu;
        obj.xi = xi;
        obj.Laplace_ex = psuboA.Laplace;
        obj.Type=psuboA.Type;
        end
        
        function r = plus(obj1,obj2)
        %plus Addition of two DiffusiveOperator of same size and type.
            r = DiffusiveOperator(obj1.mu+obj2.mu,...
                obj1.xi+obj2.xi,...
                'Name',[obj1.Name,' + ', obj2.Name],...
                'Laplace',@(s)(obj1.Laplace_ex(s)+obj2.Laplace_ex(s)),...
                'Type',obj1.Type);
        end
                
        function Z = Laplace(obj,s)
        %Laplace Laplace transform (i.e. that of the approximated
        %diffusive operator).
        % Inputs:
        %   s (PxR) complex Laplace variable (rad/s)
        % Outputs:
        %   Z (PxR) Laplace transform Z(s)
 
        validateattributes(s,{'numeric'},{'2d'});
        Nxi = length(obj.xi);
        A = zeros(size(s,1),size(s,2),Nxi);
        for i=1:Nxi
            A(:,:,i) = obj.C(i)./(s+obj.xi(i));
        end
        Z = sum(A,3) + obj.D;
        end
        
       function tau = computeCharacteristicTime(obj)
        %computeCharacteristicTime Compute the characteristic time, defined
        %as 2*pi/Lambda_max, where Lambda_max is the eigenvalue of A with
        %maximum modulus.
        % Output:
        %   tau (1) 2*pi/Lambda_max
            tau = 2*pi/max(abs(diag(obj.A)));
       end
        
       function ximax = computeMaxPulsation(obj)
        %computeMaxPulsation 
            ximax = max(abs(obj.xi(:)));
       end
       
        function  plotRealIm(obj,gh,w)
        %plotRealIm Plot real and imaginary part of both exact and
        %approximated  Laplace transform.
        % Inputs:
        %   gh graphic handle
        %   w (Nw) [wmin,wmax] pulsation for plot (rad/s)

        if(~ishghandle(gh))
            error('[%s]: <gh> should be a graphic handle: use gh=figure',mfilename);
        else
            figure(gh); % Focus
        end
        %plotComplex(gh,w,(obj.Laplace_ex(1i*w)-obj.Laplace(1i*w))./obj.Laplace(1i*w),'r','Rel. error');
        plotComplex(gh,w,obj.Laplace_ex(1i*w),'r','Exact');
        plotComplex(gh,w,obj.Laplace(1i*w),'b',sprintf('Approx. (N_{xi}=%d,xi_{max}=%1.2g,xi_{min}=%1.2g)',obj.N,max(obj.xi),min(obj.xi)));
        title(['H(1i*w):',obj.Name]);
        xlabel('w (angular frequency)');
        end
        
        function  plotRealImError(obj,gh,w)
        %plotRealIm Plot error on real and imaginary part.
        % Inputs:
        %   gh graphic handle
        %   w (Nw) [wmin,wmax] pulsation for plot (rad/s)

        if(~ishghandle(gh))
            error('[%s]: <gh> should be a graphic handle: use gh=figure',mfilename);
        else
            figure(gh); % Focus
        end
        plotComplex(gh,w,(obj.Laplace_ex(1i*w)-obj.Laplace(1i*w))./obj.Laplace(1i*w),'r',sprintf('Rel. Error. (N_{xi}=%d,xi_{max}=%1.2g,xi_{min}=%1.2g)',obj.N,max(obj.xi),min(obj.xi)));
        title(['H(1i*w):',obj.Name]);
        xlabel('w (angular frequency)');
        end

        function plotBodeDiagram(obj,gh,w)
        %plotBodeDiagram Plot magnitude and phase of both exact and
        %approximated  Laplace transform.
        % Inputs:
        %   gh graphic handle
        %   w (Nw) [wmin,wmax] pulsation for plot (rad/s)
        if(~ishghandle(gh))
            error('[%s]: <gh> should be a graphic handle: use gh=figure',mfilename);
        else
            figure(gh); % Focus
        end  %gh,pulse_range,H,name,N
        plotBodeDiagram(gh,[max(1,w(1)),w(end)],obj.Laplace_ex,'Exact',length(w));
        plotBodeDiagram(gh,[max(1,w(1)),w(end)],@(s)(obj.Laplace(s)),sprintf('Approx. (N_{xi}=%d,xi_{max}=%1.2g,xi_{min}=%1.2g)',obj.N,max(obj.xi),min(obj.xi)),length(w));
        title(['H(1i*w):',obj.Name]);
        xlabel('w (angular frequency)');
        end
        
        function plotBodeDiagramError(obj,gh,w)
        %plotBodeDiagram Plot relative error magnitude and phase of both
        %exact and approximated  Laplace transform.
        % Inputs:
        %   gh graphic handle
        %   w (Nw) [wmin,wmax] pulsation for plot (rad/s)
            if(~ishghandle(gh))
                error('[%s]: <gh> should be a graphic handle: use gh=figure',mfilename);
            else
                figure(gh); % Focus
            end  %gh,pulse_range,H,name,N
            w = logspace(log10(max(1,w(1))),log10(w(end)),length(w)); w=w(:);
            G = 20*log10(abs(obj.Laplace(w*1i))); 
            G = G - 20*log10(abs(obj.Laplace_ex(w*1i)));
            G = G(:); % Gain in dB        
            Phi = angle(obj.Laplace_ex(w*1i));
            Phi = Phi - angle(obj.Laplace(w*1i));
            Phi = Phi(:); % Phase in rad
            slope = polyfit(log10(w),G,1); slope=slope(1);

            subplot(1,2,1)
            hold all
            semilogx(w,G);
            title(['H(1i*w):',obj.Name]);
            xlabel('pulsation (rad/s)');
            ylabel('Gain (dB)');
            hc=get(legend(gca),'String'); % get legend from current axes.
            legend(hc,sprintf('Diff. (N_{xi}=%d,xi_{max}=%1.2g,xi_{min}=%1.2g)',obj.N,max(obj.xi),min(obj.xi)));
            hold all
            grid on
            subplot(1,2,2)
            hold all
            semilogx(w,(180/pi)*unwrap(Phi));
            title('Phase');
            xlabel('pulsation (rad/s)');
            ylabel('Phase (°)');
            hold all
            grid on
        end

    end
end
function checkLaplaceTransform(f)
%checkLaplaceTransform Checks validity of f as a complex-valued function
    validateattributes(f,{'function_handle'},{},mfilename,'Laplace');
    x = linspace(1,10,10);
    y = linspace(-10,10,10);
    [X,Y] = meshgrid(x,y);
    S = X + 1i*Y;
    validateattributes(f(S),{'numeric'},{'size',size(S)},mfilename,'Laplace(S)');
end
function checkDiffusiveType(Type)
%checkDiffusiveType Check validity of string Type.
    validateattributes(Type,{'char'},{},mfilename,'Type');
    if ~strcmp(Type,'Standard') && ~strcmp(Type,'Extended')
        error('Unknown type: ''Standard'' or ''Extended'' expected.');
    end
end