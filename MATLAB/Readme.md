This folder contains the MATLAB code used during the PhD.

# Misc. remarks

## Scripts, Classes, & Functions

Scripts, classes and functions are always separated in different folders. Typically, classes and functions are were the magic happens and are neatly coded. By contrast, scripts mainly consists of calls to functions and classes; since they are suited to one particular use, they can be messier. However, they showcase how the particular functions/classes can be used.

## Coding practice

Functions and classes are always heavily commented. Care has been taken to fully describe the inputs and outputs by giving its type and a short description. The function 'validateattributes' is often used to check the type of input arguments, this leads to some verbosity, but this is a reliable and officially supported method in MATLAB; I don't know better ones at least.

# Folder structure

`ArtimonDG`: Contains all files related to Artimon DG. Details are given in the corresponding Readme file.
    
**The remaining files (scripts, classes or functions) do not use artimon dg in any way.**
    
`class`: This folder contains classes that are elementary.

- `LTIOperator` and `DiffusiveOperator` are simply used as a convenient way of storing an (A,B,C,D) realization. Alongside the matrices A, B, C and D, some useful functions are provided.

- `Diffusive_Operator` is a legacy version of `DiffusiveOperator` that is used in the 1D DG codes, so it can be safely ignored.
        
`fun`: This folder contains function files. When relevant, functions have been gathered by topic under an appropriately named folder. Functions are fully commented. A large part of the functions are elementary or small; most of them are just simply convenient.
    
`script`: This folder contains various scripts, used throughout the thesis. The scripts are gathered by topic. See the corresponding Readme for more details.
    
`Main.m`: This file can be used to load all the subfolder in the path. If it is used, it should be run first.

`Playground.m`: This file is just a gathering of various commands; it is just a temporary file. It can be ignored.
    
# Get started
To start reusing codes provided in this folder:

1. Run the `addpath(...)` command in `Main.m` so that all the subfolders are added to the path. Remark: At this stage, check that experimental data files (if needed) are also included in the path.
Rmk: Set this path as default to avoid running this every time.
2. Identify the script you are interested in:
    - If the script is related to Artimon DG, look into `ArtimonDG/script`. These scripts provide examples of how Artimon DG can be used.
    - If the script is not related to Artimon DG (e.g. discretisation of diffusive representation, simulation of fractional differential equation, or plot of impedance models), then look into the `script` folder.

**Remark:** Unless they are completely trivial, scripts will usually call functions and may use classes, stored in the 'fun' and 'class' folders.
