%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - LEE 2D Cross section
%-------------------------------------------------------------------------
% Purpose: 
%   - assemble the global DG formulation for the LEE cross section 2D
%   - solve in time or frequency domain
%   - perform spectrum analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear cst % structure that holds constants
%% Constants
cst.c0 = 1; % speed of sound
cst.z0 = 1; % air impedance
cst.rho0 = cst.z0/cst.c0;
%% Base flow: cross-section
% 2D Cross-section duct
% Variables: q = [u,v,w,p/z0]
% Base flow u0(y,z)e_x
cst.Nq = 4; % number of variables
%NumFlux = 'Alpham1';
NumFlux = 'Beta0';
run 'DG2D_LEECrossSection_ZoneDefinition.m'
cst.k = 0; % longitudinal wavenumber
    % Uniform base flow
cst.u0 = @(y,z)0; cst.du0dy = @(y,z)0; cst.du0dz = @(y,z)0;
cst.u0eq = cst.u0(mesh.bound(2,1)/2,mesh.bound(2,2)/2);
CaseName_Flow = sprintf('Uniform\n');
    % Poiseuille base flow
% f = @(x)0.5*x.*(1-x); df = @(x)(0.5*(1-2*x));
% cst.u0 = @(y,z)f(y); cst.du0dy = @(y,z)df(y); cst.du0dz = @(y,z)0;
% cst.u0eq = cst.u0(mesh.bound(2,1)/2,mesh.bound(2,2)/2);
% CaseName_Flow = sprintf('Poiseuille\n');
% clear f
% Matrices
Ax_fun = @(y,z)Ax(cst.c0);
Ay_fun = @(y,z)Ay(cst.c0);
B_fun = @(y,z)B(cst.u0(y,z),cst.du0dy(y,z),cst.du0dz(y,z),cst.k,cst.c0);
%% Impedance boundary condition
% z = a0 + a1*s + a * exp(-tau*s) + a*Q(s) + a*Q(s)*exp(-tau*s)
% Three operators: Direct delay, LTI and LTI delayed.
a0 = 10; a1 = 10; aQ = 0; aDD = 0; tau= 100;
z = @(s)(a0+a1*s + aQ*sqrt(s) + aDD*exp(-s*tau));
    % -- Proportional derivative
    % beta0 and a1
beta0 = beta_fun(a0);
%beta0 = 1;
a1 = a1;
    % -- Direct delay
DD_coeff = aDD;
DD_delay = tau;
    % -- Boundary operator - Undelayed
BndOp_coeff = aQ;
Nxi = 100;
beta = 0.5; mu_an = @(xi)(sin(beta*pi)./(pi*xi.^beta)); % xi>0
[xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(mu_an,Nxi,'CoVParam',2);
BndOp_op  = DiffusiveOperator(mu,xi,'Laplace',@(s)(sqrt(s)),'Type','Extended');
clear Nxi beta mu_an xi mu
    % -- Boundary operator - Delayed
BndOpDelay_coeff = 0;
BndOpDelay_op = BndOp_op;
BndOpDelay_delay = 1e-10;
    % -- Create impedance object
cst.Impedance = ImpedanceBoundaryCondition('CT57',beta0,a1,{DD_coeff},{DD_delay},{BndOp_coeff},{BndOp_op},{BndOpDelay_coeff},{BndOpDelay_op},{BndOpDelay_delay},'Laplace',@(s)(0));
clear beta0 DD_coeff DD_delay BndOp_coeff BndOp_op  
clear BndOpDelay_coeff BndOpDelay_op BndOpDelay_delay
    % -- Case name
CaseName = [CaseName_Flow,cst.Impedance.getImpedanceSummary()];
fprintf('%s\n',CaseName);
%% Build mass and stiffness: case definition
% Define thecell array 'zone'.
    % Cross-section: hard-wall cavity
CaseName = ['Hard-wall cavity | ', CaseName];
zoneCase = DG2D_LEECrossSection_createZone('HardWallCavity',struct('Interior',[5],'Wall',[1,2,3,4]),cst,zone);    
    % Cross-section: soft-wall cavity
CaseName = ['Soft-wall cavity | ', CaseName];
zoneCase = DG2D_LEECrossSection_createZone('SoftWallCavity',struct('Interior',[5],'Impedance',[1,2,3,4]),cst,zone);
%% Build mass and stiffness: global formulation
[M,K,F,cst.DirectDelay,cst.BndOp,cst.BndOpDelay]=DG2D_LEE_buildGlobalFormulation(Ax_fun,Ay_fun,B_fun,DGMesh,zoneCase,cst.Impedance);
%% Solve dispersion relation (soft wall only)
    % Soft wall
k0 = 0:pi:50; % starting values
omega = dispRel_RectDuctAcoustics(mesh.bound(2,1),mesh.bound(2,2),cst.c0,cst.k,cst.u0eq,cst.rho0,z,k0);
    % Hard wall
%omega = dispRel_RectHardDuctAcoustics(6,6,cst.u0eq,cst.k,cst.c0,1,1);
%% Spectral analysis: Build coupled system
Np = 5; Nk = 1;
A = DG2D_buildParabolicHyperbolicRealisation(M,K,DGMesh,cst.Nq,'DirectDelay',cst.DirectDelay,'LTIOp',cst.BndOp,'LTIOpDelayed',cst.BndOpDelay,'Np',Np,'Nk',Nk);
%% Spectral analysis 
% Play with initial guess andtolerance to locate spectrum
clear opt
tic
    % Sparse eigenvalues computation
%opt.isreal = 1;
opt.tol = 1e-4;
%opt.maxit = 50;
opt.disp = 0;
%[V,D]=eig(full(A));  % if size < 3k
%R = diag(D);
% R = eigs(A,50,0.1,opt);
% R = [R;eigs(A,50,0.1+1i*5e-2,opt)];
% R = [R;eigs(A,50,0.1+1i*1e-1,opt)];
    % Full eigenvalue computation
R = eig(full(A));
toc
Rm = find(real(R(:))>0);
figure
clf
hold all
plot(real(R),imag(R),'bx');
%plot(real(Spec),imag(Spec),'x');
plot(real(1i*omega),imag(1i*omega),'ko');
title(sprintf('Spectrum (R_{max}=%1.2g)\n%s',max(real(R(Rm))),cst.Impedance.getImpedanceSummary()));
xlabel('Real(Lambda)');
ylabel('Imag(Lambda)');
xlim([-0.1,0.1])
ylim([0,20])
%axis([-0.1,0.1,-1,1]*1e-2)
%plot(0*ylim,ylim,'--');
%plot(xlim,0*xlim,'--');
%% Write spectra to file
    % -- DG spectrum
Spec = R;
    % Keep right part only
Spec = Spec(real(Spec)>(-2));
Spec = Spec(imag(Spec)>=(-1));
    % Remove essential part (diffusive)
%Spec = Spec(imag(Spec)~=0);
    % Remove points too close to each other
tolerance = 5e-2;
idx = [true; abs(diff(Spec))>tolerance];
Spec = Spec(idx);
    % Write to file
CaseName = sprintf('CavitySpectrum_DG%d-%dTriangles-Flux-%s_a0-%1.1e_a1-%1.1e.csv',DGCell.N+1,DGMesh.mesh.N,NumFlux,a0,a1);
filename = CaseName;
dlmwrite(filename,'Re,Im','Delimiter','');
dlmwrite(filename,[real(Spec(:)),imag(Spec(:))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
    % -- Dispersion relation spectrum
Spec = 1i*omega;
Spec = Spec(imag(Spec)>=(-1));
    % Write to file
CaseName = sprintf('CavitySpectrum_DispRel_a0-%1.1e_a1-%1.1e.csv',a0,a1);
filename = CaseName;
dlmwrite(filename,'Re,Im','Delimiter','');
dlmwrite(filename,[real(Spec(:)),imag(Spec(:))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
%% Eigenmode
find(abs(real(R)+1.703)<0.1)
x = V(:,1078) - V(:,1079);
%% Time-domain solving: initial condition and source
tf = 2; % Final time
CFL = 0.1; % CFL number
dt = CFL*min(mesh.charLength)/norm(cst.c0); % Time step
    % -- Initial condition
%q0 = @(x)(zeros(cst.Nq*size(x,1),1));
    % -- Initial condition (Gaussian pressure pulse) (Nx2) -> Nx1
% q0_x0 = [0.5,0.5]; q0_xd = [0.2,0.2]; q0_A = 1;
% f = @(x)(AnF_gaussianPulse(x,q0_x0,q0_xd,q0_A));
% q0 = @(x)(reshape([zeros(cst.Nq-1,size(x,1));f(x)'],cst.Nq*size(x,1),1));
    % -- Initial condition: mode
f = @(x)(cos(1*x(:,1)).*cos(1*x(:,2)));
q0 = @(x)(reshape([zeros(cst.Nq-1,size(x,1));f(x)'],cst.Nq*size(x,1),1));
    % -- Source
%f = @(t)(AnF_smoothHeaviside(t,0,tf/8));
%f = @(t)(AnF_bumpFunction(t,0,tf/2));
omega = 10; Ai = 1; % (Pa)
q_source = @(t)(Ai*[f(t).*sin(omega*t);zeros(size(t));f(t).*sin(omega*t)/cst.z0]);
%q_source = @(t)(Ai*[f(t);zeros(size(t));f(t)/cst.z0]);
% --
nodes = DGMesh.getNodesCoordinates();
StHypRealisation = struct('Np',2,'Nk',2);
[x,t] = DG2D_SolveTimeDomain(M,K,F,DGMesh,cst.Nq,tf,dt,'x0',q0(nodes),'xs',q_source,'storeOnlyLast',0,'DirectDelay',cst.DirectDelay,'LTIOp',cst.BndOp,'LTIOpDelayed',cst.BndOpDelay);
%% Harmonic solving (Hard wall)
    % Hard wall: Mode indices (n,m)
n = 1; ky = n*pi/mesh.bound(2,1);
m = 3; kz = m*pi/mesh.bound(2,2);
omega = roots([1,-2*cst.u0eq*cst.k,(cst.u0eq*cst.k)^2-(cst.c0)^2*(ky^2+kz^2+cst.k^2)]);
omega = omega(real(omega)>0);
Omega = omega-cst.u0eq*cst.k;
    % Set up initial condition
g = @(x,k)-(1+k/(cst.rho0*Omega)).*exp(1i*k*x)+(1-k/(cst.rho0*Omega)).*exp(-1i*k*x);
p = @(x)(g(x(:,1),ky).*g(x(:,2),kz));
%p = @(x)([cos(ky*x(:,1)).*cos(kz*x(:,2))]);
f = @(x)([0*p(x),0*p(x),0*p(x),p(x)]);
q0 = @(x)(reshape(f(x)',cst.Nq*size(x,1),1));
    % Harmonic solving
nodes = DGMesh.getNodesCoordinates();
x = DG2D_SolveFrequencyDomain(M,K,M,DGMesh,q0(nodes),omega);
clear nodes
    % Recover field
idx_r = cst.Nq*(0:(DGCell.Np*mesh.N-1));
x(4+idx_r) = real(x(4+idx_r));
x(1+idx_r) = imag(x(1+idx_r));
x(2+idx_r) = imag(x(2+idx_r));
x(3+idx_r) = imag(x(3+idx_r));
x = x ./ max(x(4+idx_r)); % normalize
%% Harmonic solving: Plot solution
    % get mesh plot
[trianglesPlot, nodesPlot] = DGMesh.buildPlotMesh();
x_plot = DGMesh.interpolateField2PlotNodes(cst.Nq,x);
idx_r = cst.Nq*(0:(DGCell.Np*mesh.N-1));
% --
figure(1)
clf
subplot(2,2,1)
trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),x_plot(1 + idx_r),'EdgeColor','None');
xlabel('y');
ylabel('z');
colorbar
view([0,90]);
title(sprintf('u (max: %1.2g)\n(u0=%1.2g,k=%1.2g,n=%d,m=%d,omega=%1.2g)\nNp*Nk=%d',max(x_plot(1 + idx_r)),cst.u0eq,cst.k,n,m,omega,DGCell.Np*mesh.N));
subplot(2,2,2)
trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),x_plot(2 + idx_r),'EdgeColor','None');
xlabel('y');
ylabel('z');
title(sprintf('v (max: %1.2g)',max(x_plot(2 + idx_r))));
colorbar
view([0,90]);
subplot(2,2,3)
trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),x_plot(3 + idx_r),'EdgeColor','None');
xlabel('y');
ylabel('z');
title(sprintf('w (max: %1.2g)',max(x_plot(3 + idx_r))));
colorbar
view([0,90]);
subplot(2,2,4)
trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),x_plot(4 + idx_r),'EdgeColor','None');
xlabel('y');
ylabel('z');
title(sprintf('p/z0 (max: %1.2g)',max(x_plot(4 + idx_r))));
colorbar
view([0,90]);
%% Harmonic solving: Plot mode (n,m)
x_k = linspace(0,1,1e2);
[Y,Z] = meshgrid(x_k,x_k);
Zp = cos(ky*Y).*cos(kz*Z);  Zp=real(Zp);
Zu = cst.k/(cst.rho0*Omega)*Zp; Zu=real(Zu);
Zv = -1i*(ky/(cst.rho0*Omega))*sin(ky*Y).*cos(kz*Z);Zv=imag(Zv);
Zw = -1i*(kz/(cst.rho0*Omega))*cos(ky*Y).*sin(kz*Z);Zw=imag(Zw);
figure(2)
clf
subplot(2,2,1)
surf(Y,Z,Zu,'EdgeColor','none');
view([0,90])
title(sprintf('u (max: %1.2g)\n(u0=%1.2g,k=%1.2g,n=%d,m=%d,omega=%1.2g)',max(Zu(:)),cst.u0eq,cst.k,n,m,omega));
xlabel('y');
ylabel('z');
colorbar
subplot(2,2,2)
surf(Y,Z,Zv,'EdgeColor','none');
view([0,90])
title(sprintf('v (max: %1.2g)',max(Zv(:))));
xlabel('y');
ylabel('z');
colorbar
subplot(2,2,3)
surf(Y,Z,Zw,'EdgeColor','none');
view([0,90])
title(sprintf('w (max: %1.2g)',max(Zw(:))));
xlabel('y');
ylabel('z');
colorbar
subplot(2,2,4)
surf(Y,Z,Zp,'EdgeColor','none');
view([0,90])
title(sprintf('p/z0 (max: %1.2g)',max(Zp(:))));
xlabel('y');
ylabel('z');
colorbar