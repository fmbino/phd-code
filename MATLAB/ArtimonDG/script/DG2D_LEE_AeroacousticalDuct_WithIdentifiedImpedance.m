%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - LEE 2D Longitudinal - Lined duct
%-------------------------------------------------------------------------
% Purpose:
%   - time-domain simulation of a lined duct
%   - compute SPL along the wall
% Rmk: This is a temporary script, whose sole purpose is to generate SPL
% curves corresponding to the best possible fit (i.e. using the identified
% impedance at each average Mach and frequency).
% The identified impedance is enforced by tuning a
% proportional-integral-derivative impedance at the chosen frequency.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
run 'DG2D_LEELongitudinal_ZoneDefinition.m'
run 'BaseFlowModel.m'
%% Load mesh
    % NASA GFIT
%meshfile='NASA-GFIT-2013_veryshort_coarse_withBoundaryLayer_110triangles.msh';
meshfile='NASA-GFIT-2013_veryshort_coarse_withBoundaryLayer_326triangles.msh';
%meshfile='NASA-GFIT-2013_veryshort_coarse_withBoundaryLayer_386triangles.msh';
%meshfile='NASA-GFIT-2013_veryshort_coarse_withBoundaryLayer_552triangles.msh';
%meshfile='NASA-GFIT-2013_veryshort_coarse_withBoundaryLayer_1600triangles.msh';
    % NASA GIT
%meshfile='NASA-GIT-2005_veryshort_52triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_106triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_1364triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_1050triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_814triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_1676triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_552triangles.msh';
N = 3; % polynomial degree (order is N+1)
m = load_gmsh2(meshfile,[1 2]); % read lines (1) and triangles (2)
nodes = m.POS(:,1:2); triangles = m.TRIANGLES(:,[4,1,2,3]);
boundFaces = m.LINES(:,[3,1,2]); physicalEntitiesIdx = m.physicalEntitiesIdx;
physicalEntitiesName = m.physicalEntitiesName;
mesh = Mesh_2D_Triangle(nodes,boundFaces,triangles,physicalEntitiesIdx,physicalEntitiesName);
clear m nodes triangles boundFaces physicalEntitiesIdx physicalEntitiesName
mesh.printSummary();
    % Initialize DG cell & mesh
DGCell = DGCell_2D_Triangle(N); % cell order
DGMesh = DGMesh_2D_Triangle(mesh,DGCell);
%mesh.show();
%DGMesh.showDGNodes();
%DGCell.show();
    % Impedance boundary condition on the upper wall
IBC_flux = 'Impedance_FluxBeta'; % Impedance numerical flux function
zoneDef = struct('Interior',[5],'Source',[4],'Outlet',[2],'Wall',1,IBC_flux,3);
%% Experimental duct: NASA GIT (2005)
    % Load full experimental data
ExpData = ExpDataLoadNASAGIT2005CT57();
    % Air @295 K (used for impedance identification in [Jones2005])
nu = 1.568e-5;
gamma = 1.4;
c0_exp = sqrt(gamma*287.058*295); % experimental sound speed (m/s)
Pr = 0.707;
    % -- Experimental parameters
xliner_exp = [0.203,0.609]; % experimental liner zone (m)
Lduct_exp = 0.8128; % duct experimental length (m)
H_exp = 0.051; % duct experimental height (m)
    % Microphone location (m)
x_probe_exp = 1e-3*[0.1,25.5,101.7,203.3,228.7,254.1,279.4,304.8,317.6,330.3,343.0,355.7,368.4,381.1,393.8,406.5,419.1,431.9,444.6,457.3,470.0,482.7,495.4,508.1,533.5,558.9,584.3,609.7,711.4,787.5,812.9];
y_probe_exp = 0;
    % [Impedance boundary condition] prop-der-integral model (NASA GIT-CT57)
    % Impedance written as
    %           z(s) =  a(1)/s + a(2) + a(3)*s.
    % Identified impedance coefficient [Jones2005]
IdZ = zeros(6,3,6);
        % NASA-GIT-2005 Identified @500Hz 
IdZ(1,:,1) = Impedance2PropDerInt(0.51,-1.68,500); % Mavg=0
IdZ(1,:,2)=Impedance2PropDerInt(0.76,-1.47,500); % Mavg=0.079
IdZ(1,:,3)=Impedance2PropDerInt(0.75,-1.61,500); % Mavg=0.172
IdZ(1,:,4)=Impedance2PropDerInt(0.57,-1.13,500);  % Mavg=0.255
IdZ(1,:,5)=Impedance2PropDerInt(0.61,-1.59,500); % Mavg=0.335
IdZ(1,:,6)=Impedance2PropDerInt(0.74,-0.36,500);  % Mavg=0.4
        % NASA-GIT-2005 Identified @600Hz 
IdZ(1,:,1) = Impedance2PropDerInt(0.43,-1.23,600); % Mavg=0
IdZ(1,:,2)=Impedance2PropDerInt(0.54,-1.16,600); % Mavg=0.079
IdZ(1,:,3)=Impedance2PropDerInt(0.63,-1.09,600); % Mavg=0.172
IdZ(1,:,4)=Impedance2PropDerInt(0.63,-0.94,600);  % Mavg=0.255
IdZ(1,:,5)=Impedance2PropDerInt(0.73,-1.08,600); % Mavg=0.335
IdZ(1,:,6)=Impedance2PropDerInt(0.76,-1.36,600);  % Mavg=0.4
        % NASA-GIT-2005 Identified @1kHz
IdZ(2,:,1)=Impedance2PropDerInt(0.46,0,1e3); % Mavg=0
IdZ(2,:,2)=Impedance2PropDerInt(0.38,0.06,1e3); % Mavg=0.079
IdZ(2,:,3)=Impedance2PropDerInt(0.31,0.15,1e3); % Mavg=0.172
IdZ(2,:,4)=Impedance2PropDerInt(0.27,0.10,1e3); % Mavg=0.255
IdZ(2,:,5)=Impedance2PropDerInt(0.17,0.14,1e3); % Mavg=0.335
IdZ(2,:,6)=Impedance2PropDerInt(0.09,0.14,1e3); % Mavg=0.4
    % NASA-GIT-2005 Identified @1.5kHz
IdZ(3,:,1)=Impedance2PropDerInt(1.02,1.3,1.5e3); % Mavg=0
IdZ(3,:,2)=Impedance2PropDerInt(1.01,1.25,1.5e3); % Mavg=0.079
IdZ(3,:,3)=Impedance2PropDerInt(1.25,1.18,1.5e3); % Mavg=0.172
IdZ(3,:,4)=Impedance2PropDerInt(1.26,1.26,1.5e3); % Mavg=0.255
IdZ(3,:,5)=Impedance2PropDerInt(1.18,1.27,1.5e3); % Mavg=0.335
IdZ(3,:,6)=Impedance2PropDerInt(1.21,1.08,1.5e3); % Mavg=0.4
        % NASA-GIT-2005 Identified @2kHz
IdZ(4,:,1)=Impedance2PropDerInt(4.05,0.62,2e3); % Mavg=0
IdZ(4,:,2)=Impedance2PropDerInt(3.66,1.22,2e3); % Mavg=0.079
IdZ(4,:,3)=Impedance2PropDerInt(4.81,-0.41,2e3); % Mavg=0.172
IdZ(4,:,4)=Impedance2PropDerInt(6.43,-0.23,2e3); % Mavg=0.255
IdZ(4,:,5)=Impedance2PropDerInt(4.40,-1.64,2e3); % Mavg=0.335
IdZ(4,:,6)=Impedance2PropDerInt(2.91,-3.36,2e3); % Mavg=0.4
        % NASA-GIT-2005 Identified @2.5kHz
IdZ(5,:,1)=Impedance2PropDerInt(1.54,-1.60,2.5e3); % Mavg=0
IdZ(5,:,2)=Impedance2PropDerInt(1.42,-1.39,2.5e3); % Mavg=0.079
IdZ(5,:,3)=Impedance2PropDerInt(1.19,-1.55,2.5e3); % Mavg=0.172
IdZ(5,:,4)=Impedance2PropDerInt(1.02,-1.46,2.5e3); % Mavg=0.255
IdZ(5,:,5)=Impedance2PropDerInt(0.93,-1.43,2.5e3); % Mavg=0.335
IdZ(5,:,6)=Impedance2PropDerInt(0.75,-1.29,2.5e3); % Mavg=0.4
        % NASA-GIT-2005 Identified @3kHz
IdZ(6,:,1)=Impedance2PropDerInt(0.70,-0.29,3e3); % Mavg=0
IdZ(6,:,2)=Impedance2PropDerInt(0.68,-0.29,3e3); % Mavg=0.079
IdZ(6,:,3)=Impedance2PropDerInt(0.77,-0.11,3e3); % Mavg=0.172
IdZ(6,:,4)=Impedance2PropDerInt(0.73,-0.18,3e3); % Mavg=0.255
IdZ(6,:,5)=Impedance2PropDerInt(0.73,-0.24,3e3); % Mavg=0.335
IdZ(6,:,6)=Impedance2PropDerInt(0.73,-0.26,3e3); % Mavg=0.4
%% Experimental duct: NASA GFIT (2013)
    % Load full experimental data
ExpData = ExpDataLoadNASAGFIT2013MP();
    % Air @295 K (used for impedance identification in [Jones2005])
nu = 1.568e-5;
gamma = 1.4;
c0_exp = sqrt(gamma*287.058*295); % experimental sound speed (m/s)
Pr = 0.707;
    % -- Experimental parameters
xliner_exp = [203.2,812.8]/1e3; % experimental liner zone (m)
Lduct_exp = 1016/1e3; % duct experimental length (m)
H_exp = 63.5/1e3; % duct experimental height (m)
    % Microphone location (m)
x_probe_exp = [0,0.0254,0.0508,0.0508,0.1016,0.127,0.1524,0.1651,0.1778,0.1905,0.2032,0.2159,0.2286,0.2413,0.254,0.2794,0.3048,0.3302,0.3556,0.381,0.4064,0.4318,0.4445,0.4572,0.4699,0.4826,0.4953,0.508,0.5207,0.5334,0.5461,0.5588,0.5715,0.5842,0.6096,0.635,0.6604,0.6858,0.7112,0.7366,0.762,0.7747,0.7874,0.8001,0.8128,0.8255,0.8382,0.8509,0.8636,0.889,0.9144,0.9652,0.9652,0.9906,1.016];
y_probe_exp = 0;
    % [Impedance boundary condition] prop-der-integral model (NASA GFIT-MP)
    % z(s) =  a(1)/s + a(2) + a(3)*s
    % Identified impedance coefficient [Primus2013]
IdZ = zeros(12,3,3);
        % NASA-GFIT-2013 Identified @400Hz
IdZ(1,:,1) = Impedance2PropDerInt(0.489,-3.768,400); % Mavg=0
IdZ(1,:,2) = Impedance2PropDerInt(1.544,-3.569,400); % Mavg=0.18
IdZ(1,:,3) = Impedance2PropDerInt(2.056,-3.351,400); % Mavg=0.271
        % NASA-GFIT-2013 Identified @600Hz
IdZ(2,:,1) = Impedance2PropDerInt(0.316,-2.384,600); % Mavg=0
IdZ(2,:,2) = Impedance2PropDerInt(0.828,-2.172,600); % Mavg=0.18
IdZ(2,:,3) = Impedance2PropDerInt(1.446,-2.249,600); % Mavg=0.271
        % NASA-GFIT-2013 Identified @800Hz
IdZ(3,:,1) = Impedance2PropDerInt(0.228,-1.552,800); % Mavg=0
IdZ(3,:,2) = Impedance2PropDerInt(0.556,-1.33,800); % Mavg=0.18
IdZ(3,:,3) = Impedance2PropDerInt(0.966,-1.358,800); % Mavg=0.271
        % NASA-GFIT-2013 Identified @1kHz
IdZ(4,:,1) = Impedance2PropDerInt(0.207,-1.071,1e3); % Mavg=0
IdZ(4,:,2) = Impedance2PropDerInt(0.652,-0.937,1e3); % Mavg=0.18
IdZ(4,:,3) = Impedance2PropDerInt(0.911,-0.911,1e3); % Mavg=0.271
        % NASA-GFIT-2013 Identified @1.2kHz
IdZ(5,:,1) = Impedance2PropDerInt(0.231,-0.678,1.2e3); % Mavg=0
IdZ(5,:,2) = Impedance2PropDerInt(0.66,-0.607,1.2e3); % Mavg=0.18
IdZ(5,:,3) = Impedance2PropDerInt(0.976,-0.668,1.2e3); % Mavg=0.271
        % NASA-GFIT-2013 Identified @1.4kHz
IdZ(6,:,1) = Impedance2PropDerInt(0.283,-0.258,1.4e3); % Mavg=0
IdZ(6,:,2) = Impedance2PropDerInt(0.595,-0.278,1.4e3); % Mavg=0.18
IdZ(6,:,3) = Impedance2PropDerInt(1.016,-0.571,1.4e3); % Mavg=0.271
        % NASA-GFIT-2013 Identified @1.6kHz
IdZ(7,:,1) = Impedance2PropDerInt(0.203,-0.106,1.6e3); % Mavg=0
IdZ(7,:,2) = Impedance2PropDerInt(0.675,-0.197,1.6e3); % Mavg=0.18
IdZ(7,:,3) = Impedance2PropDerInt(0.936,-0.205,1.6e3); % Mavg=0.271
        % NASA-GFIT-2013 Identified @1.8kHz
IdZ(8,:,1) = Impedance2PropDerInt(0.187,0.106,1.8e3); % Mavg=0
IdZ(8,:,2) = Impedance2PropDerInt(0.684,0.01,1.8e3); % Mavg=0.18
IdZ(8,:,3) = Impedance2PropDerInt(1.025,-0.096,1.8e3); % Mavg=0.271
        % NASA-GFIT-2013 Identified @2kHz
IdZ(9,:,1) = Impedance2PropDerInt(0.205,0.326,2e3); % Mavg=0
IdZ(9,:,2) = Impedance2PropDerInt(0.762,0.266,2e3); % Mavg=0.18
IdZ(9,:,3) = Impedance2PropDerInt(1.225,0.086,2e3); % Mavg=0.271
        % NASA-GFIT-2013 Identified @2.2kHz
IdZ(10,:,1) = Impedance2PropDerInt(0.251,0.563,2.2e3); % Mavg=0
IdZ(10,:,2) = Impedance2PropDerInt(0.869,0.469,2.2e3); % Mavg=0.18
IdZ(10,:,3) = Impedance2PropDerInt(1.347,0.259,2.2e3); % Mavg=0.271
        % NASA-GFIT-2013 Identified @2.4kHz
IdZ(11,:,1) = Impedance2PropDerInt(0.275,0.744,2.4e3); % Mavg=0
IdZ(11,:,2) = Impedance2PropDerInt(0.998,0.69,2.4e3); % Mavg=0.18
IdZ(11,:,3) = Impedance2PropDerInt(1.401,0.396,2.4e3); % Mavg=0.271
        % NASA-GFIT-2013 Identified @2.6kHz
IdZ(12,:,1) = Impedance2PropDerInt(0.318,0.946,2.6e3); % Mavg=0
IdZ(12,:,2) = Impedance2PropDerInt(1.262,1.15,2.6e3); % Mavg=0.18
IdZ(12,:,3) = Impedance2PropDerInt(2.149,-0.058,2.6e3); % Mavg=0.271
%% Duct scaling and diagnostic
% Experimental duct is on [0,Lduct_DG].
% Speed of sound is scaled to keep both numerical and experimental 
% frequencies identical.
    % -- Numerical parameters
Lduct_num = Lduct_exp; % numerical duct length (choice)
c0_num = c0_exp * Lduct_num/Lduct_exp; % numerical sound speed
L_num = mesh.bound(2,1)-mesh.bound(1,1); % numerical length
if L_num<Lduct_num
   error('Numerical length is smaller than chosen numerical duct length.'); 
end
H_num = mesh.bound(2,2)-mesh.bound(1,2); % numerical height
if abs(H_num - H_exp * (Lduct_num/Lduct_exp))<eps
    warning('Numerical aspect ratio does not match experimental one.');
end
xliner_num = xliner_exp * (Lduct_num/Lduct_exp); % numerical liner zone
tfNoRefl_num = (2*L_num-Lduct_num)/c0_num; % numerical simulation time w/o reflection
        % Min. Freq w/o reflection
        % (Lduct_num/c0_num+3*T_source <= tfNoRefl_num
w_source_NoRefl = 2*pi*(3)*(tfNoRefl_num-Lduct_num/c0_num)^(-1);
        % Duct no flow cut-off frequency (identical to experimental one)
w_cutoff = 2*pi*c0_num/(2*H_num);
w_cutoff_longi_num = pi*c0_num/(mesh.bound(2,1)-mesh.bound(1,1));
        % DG max frequency
        % 1D estimation based on 'doF_per_1Dcell'
DoF_per_1Dcell = N+1; % choice (typically ~# of node on edge)
w_to_PPW = @(w)2*pi*c0_num*DoF_per_1Dcell/(min(mesh.charLength(:))*w);
PPW_to_w = @(PPW)2*pi*c0_num*DoF_per_1Dcell/(min(mesh.charLength(:))*PPW);
        % Min. # of PPPW for 1D Wave Equation from [Hu,1999].
        % Function of number of nodes per cell.
dof_to_PPWmin = [0,15.7,10.5,7.9,6.8,6.2,6.2,5.3]; % value for N=7 made up
fprintf('-- Numerical simulation characteristics.\n');
fprintf('*Dimensions: numerical (experiment)\n');
fprintf('\tDuct length: %1.3e (%1.3e)\n',Lduct_num,Lduct_exp);
fprintf('\tTotal length: %1.3e (N/A)\n',L_num);
fprintf('\tHeight: %1.3e (%1.3e)\n',H_num,H_exp);
fprintf('\tAspect ratio: %1.3e (%1.3e)\n',Lduct_num/H_num,Lduct_exp/H_exp);
fprintf('\tSound speed: %1.3e (%1.3e)\n',c0_num,c0_exp);
fprintf('\tLiner position: [%1.3e,%1.3e] ([%1.3e,%1.3e])\n',xliner_num(1),xliner_num(2),xliner_exp(1),xliner_exp(2));
fprintf('*Frequencies/times of interest\n');
fprintf('No flow cut-off: %1.3e rad/s %1.3e Hz\n',w_cutoff,w_cutoff/(2*pi));
fprintf('Max. time w/o reflection: %1.3e s\n',tfNoRefl_num);
fprintf('Min. freq. w/o refl.: %1.3e rad/s %1.3e Hz\n',w_source_NoRefl,w_source_NoRefl/(2*pi));
fprintf('Mesh cut-off longitudinal: %1.3e rad/s %1.3e Hz \n',w_cutoff_longi_num,w_cutoff_longi_num/(2*pi));
fprintf('*DG frequency properties (assuming DoF=%d)\n',DoF_per_1Dcell);
fprintf('Cut-off: %d PPW \n',w_to_PPW(w_cutoff));
fprintf('Min. freq. w/o refl.: %d PPW\n',w_to_PPW(w_source_NoRefl));
fprintf('Min. # of PPW [Hu,1999]: %d\n',dof_to_PPWmin(DoF_per_1Dcell));
for PPW=3:10
    fprintf('\tMax. freq. @PPW_min=%d: %1.3e rad/s %1.3e Hz\n',PPW,PPW_to_w(PPW),PPW_to_w(PPW)/(2*pi));
end
fprintf('--\n');
%% Numerical probe placement
% Denser bunching of probes
x_probe = linspace(x_probe_exp(1),x_probe_exp(end),150);
y_probe = 0*x_probe; % probe location
probe = [x_probe(:),y_probe(:)];
%% Base flow definition
cst.c0 = c0_num;
Mavg = 0.271; % average Mach number
a = mesh.bound(1,2); b = mesh.bound(2,2);
y2r = @(y,a,b)2/(b-a)*(y-(b+a)/2); y2r_dy = @(a,b)2/(b-a);
r2y = @(r,a,b) (b+a)/2 + (b-a)/2*r; r2y_dr = @(a,b)(b-a)/2;
    % -- Uniform base flow
cst.u0 = @(x,y)Mavg;
cst.du0dy = @(x,y)0;
FlowName = sprintf('Uniform flow M_{avg}=%1.3g',Mavg);
    % -- Poiseuille base flow
cst.u0 = @(x,y)cst.c0*M0_Poi(y2r(y,a,b),Mavg); 
cst.du0dy = @(x,y)cst.c0*y2r_dy(a,b).*dM0_dr_Poi(y2r(y,a,b),Mavg);
FlowName = sprintf('Poiseuille M_{avg}=%1.3g',Mavg);
    % -- Hyperbolic velocity profile
delta = 0.22; % adimensional boundary layer thickness (in (0,1))
cst.u0 = @(x,y)cst.c0*M0_Hy(y2r(y,a,b),Mavg,delta);
cst.du0dy = @(x,y)cst.c0*y2r_dy(a,b).*dM0_dr_Hy(y2r(y,a,b),Mavg,delta);
FlowName = sprintf('Hyperbolic M_{avg}=%1.3g, delta=%1.3g',Mavg,delta);
    % -- Plot flow in duct
y = linspace(mesh.bound(1,2),mesh.bound(2,2),1e2);
figure(9)
clf
subplot(2,1,1)
hold all
plot(cst.u0(1,y)/cst.c0,y,'DisplayName',sprintf('M_c=%1.3g',max(cst.u0(1,y)/cst.c0)));
plot(M0_Poi(y2r(y,a,b),Mavg),y,'DisplayName',sprintf('Poiseuille M_c=%1.3g',max(M0_Poi(y2r(y,a,b),Mavg))));
set(gca,'DataAspectRatio',[1,1,1]);
legend('show');
ylim([min(y),max(y)]);
xlim([0,1]);
ylabel('y (m)');
xlabel('M0(y)');
title(FlowName)
subplot(2,1,2)
hold all
plot(cst.du0dy(1,y)/cst.c0,y,'DisplayName',sprintf('Max=%1.3g/m',max(abs(cst.du0dy(1,y)/cst.c0))));
plot(y2r_dy(a,b).*dM0_dr_Poi(y2r(y,a,b),Mavg),y,'DisplayName',sprintf('Poiseuille Max=%1.3g/m',max(abs(y2r_dy(a,b).*dM0_dr_Poi(y2r(y,a,b),Mavg)))));
legend('show');
%set(gca,'DataAspectRatio',[1,1,1]);
ylim([min(y),max(y)]);
ylabel('y (m)');
xlabel('dM0(y)/dy');
title(FlowName)
%% [Impedance boundary condition] liner position
xliner = xliner_num; % lined zone
xliner_ind = @(x,y)((x>=xliner(1)).*(x<=xliner(2))); % indicator function
% dts = 0.05; % spread (Heaviside is dts=0)
% xliner_ind = @(x,y)(AnF_smoothHeaviside(x,xliner(1)-dts,dts).*(1-AnF_smoothHeaviside(x,xliner(2),dts)));
%%clf
%hold all
%plot(r,AnF_smoothHeaviside(r,xliner(1)-dts,dts).*(1-AnF_smoothHeaviside(r,xliner(2),dts)));
%plot(r,xliner_ind(r,1));
%% [Time integration] Choose time step
CFL = 0.53; % stability limit for N = 4 (order 5)
CFL = 0.85; % stability limit for N = 3 (order 4)
%% [Time integration] Source definition (NASA GIT)
w_source = 2*pi*ExpData{1}.freq*1e3; % available frequency
Exp_idx_Mach = 6; % index for Mach number
    % Choose frequencies index to consider
w_source_idx = [1,6,11,16,21,26]; % 0.5,1,1.5,2,2.5,3 kHz
w_source_idx = [2,6,11,16,21,26]; % 0.6,1,1.5,2,2.5,3 kHz
IdZ_idx = 1:length(w_source_idx);
T_source = (2*pi)./w_source; % source period (s)
fprintf('Period w/o reflection: %d\n',floor(tfNoRefl_num/max(T_source(w_source_idx))));
%% [Time integration] Source definition (NASA GFIT)
w_source = 2*pi*ExpData{1}.freq*1e3; % available frequency
Exp_idx_Mach = 3; % index for Mach number
    % Choose frequencies index to consider
w_source_idx = [7,8,9,10,11,12]; % [1.6,1.8,2,2.2,2.4,2.6]kHz
w_source_idx = [2,4,6,7,10,12]; %[0.6,1,1.4,1.6,2.2,2.6]kHz best frequencies at M=0
%w_source_idx = [5,6]; % [1.2kHz, 1.4kHz] best frequencies at M=0
IdZ_idx = w_source_idx;
T_source = (2*pi)./w_source; % source period (s)
fprintf('Period w/o reflection: %d\n',floor(tfNoRefl_num/max(T_source(w_source_idx))));
%% [Time integration] Frequency-by-frequency
p_DG = cell(length(w_source_idx),1);
t_DG = cell(length(w_source_idx),1);
tf_DG = cell(length(w_source_idx),1);
dt_DG = cell(length(w_source_idx),1);
for i=1:length(w_source_idx) % one simulation per source
    ps = @(t)sin(w_source(w_source_idx(i))*t);
        % Set impedance to identified value
    a = IdZ(IdZ_idx(i),:,Exp_idx_Mach); % Choose impedance
        % -- Reflection coefficient
    if a(1)==0 && a(3)==0 % Proportional impedance
        cst.Impedance = ImpedanceBoundaryCondition(sprintf('Prop. z(s)=%1.1e',a(2)),@(x,y)beta_fun(beta_fun(a(2)))*xliner_ind(x,y),0,{0},{0},{0},{0},{0},{0},{0});
    elseif a(1)==0 % Proportional derivative impedance
        % beta(s) = 1 + h(s), where h(s) = -2/(1+z(s)) has a diffusive
        % representation.
        h_an = @(s)(-2./(1+a(2)+a(3)*s));
        BndOp_op  = DiffusiveOperator(-2/a(3),(1+a(2))/a(3),'Laplace',h_an,'Type','Standard');    
        cst.Impedance = ImpedanceBoundaryCondition(sprintf('Prop. Der. z(s)=%1.1e+%1.1e*s',a(2),a(3)),@(x,y)beta_fun(1)*xliner_ind(x,y),0,{0},{0},{xliner_ind},{BndOp_op},{0},{0},{0});
    elseif a(3)==0 % Proportional integral impedance
        % beta(s) = beta0 + h(s), where h(s) has a diffusive
        % representation.
        h_an = @(s)(2*a(1)/(1+a(2))*1./(a(1)+(1+a(2))*s));
        BndOp_op  = DiffusiveOperator(2*a(1)/(1+a(2))^2,a(1)/(1+a(2)),'Laplace',h_an,'Type','Standard');
        cst.Impedance = ImpedanceBoundaryCondition(sprintf('Prop. Int. z(s)=%1.1e+%1.1e/s',a(2),a(1)),@(x,y)beta_fun(beta_fun(a(2)))*xliner_ind(x,y),0,{0},{0},{xliner_ind},{BndOp_op},{0},{0},{0});
    end
    cst.Impedance.printImpedanceSummary();
        % Impedance value at given frequency
    fprintf('Impedance @%1.2ekHz: %s\n',w_source(w_source_idx(i))/(2*pi*1e3),num2str(z_fun(cst.Impedance.Laplace(1i*w_source(w_source_idx(i)),0.5,1))));
        % Perform simulation
    tf = max(tfNoRefl_num,60*T_source(i)); % Final time
    [p,t,probeDG] = DG2D_SolveTimeDomain_Wrapper(DGMesh,zoneDef,zone,c0_num,cst.u0,cst.du0dy,cst.Impedance,CFL,tf,ps,'probe',probe);
    p_DG{i} = p; t_DG{i} = t; tf_DG{i} = tf; dt_DG{i} = max(diff(t));
        % store full field
    %[p,t,probeDG] = DG2D_SolveTimeDomain_Wrapper(DGMesh,zoneDef,zone,c0_num,cst.u0,cst.du0dy,cst.Impedance,CFL,tf,ps,'Delay_Np',Delay_Np,'Delay_Nk',Delay_Nk);
end
%% [Post processing] Filter pressure probe
% Compute 'p_DG_filtered' from 'p_DG'
Filter_order = 2; % the higher the order, the higher the response time
% --
p_DG_filtered = cell(length(w_source_idx),1);
if numel(p_DG)==numel(w_source_idx) % frequency by frequency
    for k=1:length(w_source_idx) % for each frequency
        dt = dt_DG{k};
                % filter definition
        d = designfilt('bandpassiir','FilterOrder',Filter_order,...
                        'HalfPowerFrequency1',0.96*w_source(w_source_idx(k))/(2*pi),...
                        'HalfPowerFrequency2',1.04*w_source(w_source_idx(k))/(2*pi),...
                        'SampleRate',1/dt);
        for i=1:size(p_DG{k},1) % for each probe
            p_DG_filtered{k}(i,:) = filter(d,p_DG{k}(i,:));
        end
    end
else % multiple frequency
    for k=1:length(w_source_idx) % for each frequency
        dt = dt_DG{k};
                % filter definition
        d = designfilt('bandpassiir','FilterOrder',Filter_order,...
                        'HalfPowerFrequency1',0.96*w_source(w_source_idx(k))/(2*pi),...
                        'HalfPowerFrequency2',1.04*w_source(w_source_idx(k))/(2*pi),...
                        'SampleRate',1/dt);
        for i=1:size(p_DG{1},1) % for each probe
            p_DG_filtered{k}(i,:) = filter(d,p_DG{1}(i,:));
        end
    end
end
%% [Post processing] Compute RMS value
% Compute RMS values of 'p_DG_filtered'
% RMS should be constant for lossless duct on [0,Lduct_num]
% Main parameters:
%   Number of reflections (best case: longer transient).
%   Integration interval for RMS. (The higher tmin and tmin-tmax, the
%   better.)
%   -> Influence of these parameters should be studied on any result.
%   -> The higher the base flow Mach number, the higher the influence of
%   reflections. (Poiseuille flow: reflections are negligible for Mc<=0.5
%   1e-2 difference on dB).
% Inputs: p_DG, idxProbeNodes, c0, tf, T_source, 
% Outputs: %d
% To compute RMS value, two strategies:
%   -> Remove the transient (first n periods) and compute RMS value
%   -> Filter with bandpass and compute RMS value
% The higher the filter order, the higher must be Nper_RMS_min.
Nper_RMS_min = 0; % first period to consider for RMS computations
% --
p_DG_RMS = zeros(size(probeDG,1),numel(p_DG_filtered));
for k=1:numel(p_DG_filtered) % for each frequency
    p_DG_RMS(:,k) = zeros(size(p_DG_filtered{k},1),1);
    for i=1:size(p_DG_filtered{k},1) % for each probe
            % Time interval to compute RMS
        tmin = Nper_RMS_min*T_source(w_source_idx(k)) + probeDG(i,1)/c0_num; % remove transient
        tmax = tf_DG{k}; % until end of simulation
        %tmax = min(tfNoRefl_num,tf_DG{k}); % without reflection
        mask = logical((t_DG{k}>=(tmin)).*(t_DG{k}<=tmax));
            % RMS value
        p_DG_RMS(i,k) = rms(p_DG_filtered{k}(i,mask));
    end
        % Convert to SPL
    p_DG_RMS(:,k) = 20*log10(p_DG_RMS(:,k)/p_DG_RMS(1,k));
end
%% [Plot] Pressure probe measurement
% Plot time series and spectrum
Source_idx = 1; % source to plot (for probe)
Probe_idx = [2,40,100]; % indexes of probes to plot
p_plot = p_DG_filtered;
%p_plot = p_DG;
    % -- Probe pressure measurements
figure(4)
clf
p_DG_p = p_plot{Source_idx};
t_DG_p = t_DG{Source_idx};
for i=1:length(Probe_idx) % for the selected probes
    xprobe = probeDG(Probe_idx(i),:); % position of probe
    mask = logical(t_DG_p>=(xprobe(1)/c0_num));
    p_DG_probe = p_DG_p(Probe_idx(i),mask);
    t_DG_probe = t_DG_p(mask)-xprobe(1)/c0_num;
    t_DG_probe = t_DG_probe/T_source(w_source_idx(Source_idx));
        % Plot time series
    subplot(2,1,1)
    hold all
    plot(t_DG_probe,p_DG_probe,'DisplayName',sprintf('(x,y)=(%5.2e,%5.2e)',xprobe(1),xprobe(2)));
    subplot(2,1,2)
    hold all
        % Plot power spectral density
    fs = 1/dt; % sample frequency (Hz)
    [psd,freq] = periodogram(p_DG_probe,rectwin(length(p_DG_probe)),length(p_DG_probe),fs);
    plot(freq,10*log10(psd/max(psd)),'DisplayName',sprintf('(x,y)=(%5.2e,%5.2e)',xprobe(1),xprobe(2)));
    xlim([0,1e4]);
    grid on
    xlabel('frequency (Hz)');
    title('Power spectral density (dB/Hz)');  
end
subplot(2,1,1)
legend('show');
%xlabel('shifted time t - x/c0 (s)');
xlabel('shifted time t - x/c0 (T_{source})');
ylabel('p/z0');
if numel(p_plot)==numel(w_source_idx) % frequency by frequency
    title(sprintf('Source @%1.2e Hz',w_source(w_source_idx(Source_idx))/(2*pi)));
else % multiple frequency
    title(sprintf('Multiple frequencies: %s Hz',mat2str(w_source(w_source_idx(:))/(2*pi))));
end
%% [Plot] RMS value along wall
% Plot RMS value for every frequencies against experiments
PlotExpData = 1;
Exp_idx_freq = w_source_idx; % Index for experimental source frequency
Exp_idx_SPL = 1; % Index for source SPL
%Exp_idx_Mach = 1; % Index for experimental Mach number
SPL_0 = 120; % SPL at x=0
% --
figure(9)
clf
for k=1:length(w_source_idx) % for each frequency
   subplot(floor(length(w_source_idx)/2),2,k)
    hold all
        % Experimental data
    if PlotExpData && exist('ExpData','var')
            % Use experimental SPL at x=0
        SPL_0 = ExpData{Exp_idx_Mach,Exp_idx_SPL}.MicroSPL(1,Exp_idx_freq(k));
        plot(ExpData{Exp_idx_Mach,Exp_idx_SPL}.Microxpos,ExpData{Exp_idx_Mach,Exp_idx_SPL}.MicroSPL(:,Exp_idx_freq(k)),'x',...
            'DisplayName',sprintf('%s-%1.2ekHz',ExpData{Exp_idx_Mach,Exp_idx_SPL}.name,ExpData{Exp_idx_Mach,Exp_idx_SPL}.freq(Exp_idx_freq(k))));
    end
        % Computed RMS value
    plot(1e3*probeDG(:,1),SPL_0+p_DG_RMS(:,k)-p_DG_RMS(1,k),'-',...
        'DisplayName',sprintf('f_{src}=%1.2ekHz tf=%1.2e*T_{src}',w_source(w_source_idx(k))/(1e3*2*pi),tf_DG{k}/T_source(w_source_idx(k))));
    legend('show');
    legend('Location','northoutside');
    ylim([110,140])
    xlim([0,L_num]*1e3);
    grid on
end
xlabel('distance x (mm)');
ylabel('SPL (dB)');
%% Export SPL curves to file
namestr = sprintf('%s_IdentifiedImp_DG%d-%dtriangles-CFL%3.2g_%s.csv',ExpData{Exp_idx_Mach,Exp_idx_SPL}.name,DGCell.N+1,mesh.N,CFL,FlowName);
csvhead = 'x(m)';
    % Adjust SPL at x=0 before saving
for k=1:length(w_source_idx) % for each frequency
    SPL_0 = ExpData{Exp_idx_Mach,Exp_idx_SPL}.MicroSPL(1,Exp_idx_freq(k));
    p_DG_RMS(:,k) = SPL_0+p_DG_RMS(:,k)-p_DG_RMS(1,k);
    csvhead = sprintf('%s,SPL%d',csvhead,k);
end
dlmwrite(namestr,csvhead,'Delimiter','');
dlmwrite(namestr,[probeDG(:,1),p_DG_RMS],'-append','Delimiter',',','newline','unix','precision','%1.6e');