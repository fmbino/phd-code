%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG
%-------------------------------------------------------------------------
% Monodimensional LEE.
% dq/dt(t,x) + Ax*dq/dx(t,x) = 0    t>0 x in [0,L]
% q = (u, p/z0)
%   Source w/o reflection at x=0
%   Impedance boundary condition at x=L
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Initialize Mesh
bound = [0,1];
Nx = 10; % number of cell
mesh = Mesh_1D_Uniform(bound, Nx);

%% Initialize DG Cell
N = 8; % Nodes per cell
DGCell = DGCell_1D(N);

%% Initialize LEE Case
% -- Physical constants
c0 = 340.29; % Speed of sound (m/s)
rho0 = 1.177; % Density (kg/m^3)
z0 = c0*rho0;
U0 = 0; % Mean flow velocity (m/s)

% -- Domain
L = 1; % (m) Domain is [0,L]

% -- Impedance boundary condition
% p/z0(s) = z(s) * u(s) with Re(s)>0
% with z(s) = a0+a1*s+ad1*D1(s)+ad2*D2(s)y_
% D2 may be delayed.
a0 = 1; a1 = 1; ad1 = 0e-3; ad2 = 0*1;
D1 = @(s)(sqrt(s));
%Diff1 = Diffusive_Operator('sqrt(s)',D1,xik_optim(:),-muk_optim(:).*xik_optim(:), sum(muk_optim), 0);
Diff1 = Diffusive_Operator('sqrt(s)',D1,0,0, 0, 0);
tau = 0.1/c0;
D2 = @(s)(sqrt(s)*exp(-tau*s));
%Diff2 = Diffusive_Operator('sqrt(s)*exp(-tau*s))',D2,xik_optim(:),-muk_optim(:).*xik_optim(:), sum(muk_optim), tau);
Diff2 = Diffusive_Operator('sqrt(s)*exp(-tau*s))',D2,0,0, 0, tau);
z = @(s)(a0+a1*s+ad1*D1(s)+ad2*D2(s));
LEECase = Case_LEE_1D('LEE Test',[0,L],[U0,c0,rho0],[a0,a1,ad1,ad2],Diff1,Diff2);
%% Display LEE Case
LEECase.PlotHarmonicSolution(0,1e3,1);
%% Build Global DG formulation
DGLEE = ArtimonDG_LEE_1D(mesh,DGCell,LEECase);
DGLEE = DGLEE.buildGlobalFormulation();
DGLEE.printSummary();
%% Harmonic solving
% -- Source
Ai = 1; % Ai (Pa)
omega = 1e3; % (rad/s)
% -- Harmonic Solving
[u,p] = DGLEE.solveHarmonicDomain(omega,Ai);
% -- Exact Harmonic solution
x = DGLEE.buildVectorGlobalCoordinates(); 
y_ex_harm = LEECase.q_eff_harm(x,0,omega,Ai);
% -- Compute error
epsL2 = sqrt(mean((real([u;p]-[transpose(y_ex_harm(1,:));transpose(y_ex_harm(2,:))])).^2));
fprintf('-- Harmonic solving\nTarget order: %d Nodes: %d, Err_L2=%1.2e, Max value=%1.2e\n--\n',DGCell.N,mesh.N*DGCell.N,epsL2,max(abs(real([p;u]))));
%% Harmonic solving: plot
clf
subplot(1,2,1)
leg=cell(0);
hold all
plot(x,y_ex_harm(1,:));
leg(end+1)={sprintf('Exact harmonic')};
plot(x,u);
leg(end+1)={sprintf('Computed harmonic')};
legend(leg);
xlabel('x');
ylabel('u');
subplot(1,2,2)
hold all
plot(x,y_ex_harm(2,:));
plot(x,p);
xlabel('x');
ylabel('p/z0');
%% Linear stability analysis
DGLEE.doLinearStabilityAnalysis();
%% Time-domain solving
% -- Source
Ai = 1; % Ai (Pa)
omega = 1e3; % (rad/s)
q_source = @(t)((Ai/z0)*[exp(1i*omega*t');exp(1i*omega*t')]);
% -- Initial condition
q0 = @(x)(zeros(2,length(x))); % (P)->(NqxP)
% -- Time integration
tf = 2*max(mesh.bound)/c0; % (1) tfinal
CFL = 1e-5;
[t,q1,q2,phi] = DGLEE.solveTimeDomain(tf,CFL,q0,q_source);
%% Time-domain solving: comparison to harmonic solution
    % Time instant
tp = 0.8*tf;
[~,i_tp] = min(abs(t-tp));
tp = t(i_tp);
    % Exact Harmonic solution
x = DGLEE.buildVectorGlobalCoordinates(); 
y_ex_harm = LEECase.q_ex_harm(x,0,omega,Ai);
    % Compute L2 error
epsL2 = sqrt(mean((real([q1(:,i_tp);q2(:,i_tp)]-exp(1i*omega*tp)*[transpose(y_ex_harm(1,:));transpose(y_ex_harm(2,:))])).^2));
fprintf('-- Time-domain solving\nTarget order: %d Nodes: %d\n@t=%1.2g*tf, Err_L2=%1.2e, Max value=%1.2e (harmonic: %1.2e)\n--\n',DGCell.N,mesh.N*DGCell.N,tp/tf,epsL2,max(abs(real([q1(:,i_tp);q2(:,i_tp)]))),max(abs(real(exp(1i*omega*tp)*y_ex_harm(:)))));
%% Time-domain solving: comparison to harmonic solution (plot)
clf
subplot(1,2,1)
leg=cell(0);
hold all
plot(x,y_ex_harm(1,:)*exp(1i*omega*tp));
leg(end+1)={sprintf('Exact harmonic')};
plot(x,q1(:,i_tp));
leg(end+1)={sprintf('Computed')};
legend(leg);
xlabel('x');
ylabel('u');
subplot(1,2,2)
hold all
plot(x,y_ex_harm(2,:)*exp(1i*omega*tp));
plot(x,q2(:,i_tp));
legend(leg);
xlabel('x');
ylabel('p/z0');
subplot(1,2,1)
title(sprintf('%s - DG%d\n(Nx=%d,Nodes=%d,CFL=%1.1g)\nit:%d/%d\nt=%3.2g/%3.2g s',LEECase.CaseName,DGCell.N,mesh.N,DGCell.N*mesh.N,CFL,i_tp,length(t),t(i_tp),t(end)));
subplot(1,2,2)
omega_max = (2*pi*DGLEE.cellDG.N*c0)/(DGLEE.PPW(N)*DGLEE.mesh.cell_size(1));
title(sprintf('Max. puls. = %1.2e rad/s\nPuls. = %1.2e rad/s',omega_max,omega));
%% Time-domain solving: comparison to harmonic solution (video)
duration = 6; % (s)
fps = 15;
filename = 'test_no_delay.avi';
% --
figure
hold all
nframe = min(ceil(duration*fps),length(t)); % number of frame to display
idx_int = ceil(linspace(1,length(t),nframe)); % idx to display
clear Vi
Vi(length(idx_int)) = struct('cdata',[],'colormap',[]);
for i=1:length(idx_int)
    clf
    subplot(1,2,1)
    hold all
    leg = cell(0);
        % Display iteration n°i+1
    plot(x,real(q1(:,idx_int(i))));
    leg(end+1)={sprintf('Temporal')};
    plot(x,y_ex_harm(1,:)*exp(1i*omega*t(idx_int(i))));
    leg(end+1)={sprintf('Exact harmonic')};
    xlabel('x');    
    ylabel(sprintf('q_%d',1));
    legend(leg);
    title(sprintf('%s - DG%d\n(Nx=%d,Nodes=%d,CFL=%1.1g)\nit:%d/%d\nt=%3.2g/%3.2g s',LEECase.CaseName,DGCell.N,mesh.N,DGCell.N*mesh.N,CFL,idx_int(i),length(t),t(idx_int(i)),t(end)));
    axis([mesh.bound(1), mesh.bound(2),-1e-2,1e-2])
    subplot(1,2,2)
    hold all
    leg = cell(0);
        % Display iteration n°i+1
    plot(x,real(q2(:,idx_int(i))));
    leg(end+1)={sprintf('Temporal')};
    plot(x,y_ex_harm(2,:)*exp(1i*omega*t(idx_int(i))));
    leg(end+1)={sprintf('Exact harmonic')};
    xlabel('x');    
    ylabel(sprintf('q_%d',2));
    legend(leg);
    title(sprintf('%s - DG%d\n(Nx=%d,Nodes=%d,CFL=%1.1g)\nit:%d/%d\nt=%3.2g/%3.2g s',LEECase.CaseName,DGCell.N,mesh.N,DGCell.N*mesh.N,CFL,idx_int(i),length(t),t(idx_int(i)),t(end)));
    axis([mesh.bound(1), mesh.bound(2),-1e-2,1e-2])
    drawnow % force plot to show
    Vi(i) = getframe(gcf);
end
fprintf('Now generating video file...');
movie(Vi);
movie2avi(Vi,filename,'fps',fps);
fprintf('Done\n');
clear Vi