%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - Benchmark
%-------------------------------------------------------------------------
% The purpose of this script is to benchmark the various functions of
% Artimon DG.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Setup
zone = 5; % Choose mesh zone to loop
    % Mesh
if ~exist('DGMesh')
    error('[%d] Create a DGMesh object.');
else
    fprintf('DGMesh has %d nodes.\n',DGMesh.N);
end
    % PDE
c0 = 1;
z0 = 1;
U0 = 0.5; V0 = 0;
Ax = [U0,0,c0;0,U0,0;c0,0,U0];
Ay = [V0,0,0;0,V0,c0;0,c0,V0];
Nq = size(Ax,1);
B = zeros(Nq,Nq);
    % Numerical flux for upwind
fout = @(n)(max(0,dot([U0,V0],n))*[n(2)^2,-n(1)*n(2),0;-n(1)*n(2),n(1)^2,0;0,0,0] + (dot([U0,V0],n)+c0)/2*[n(1)^2,n(1)*n(2),n(1);n(1)*n(2),n(2)^2,n(2);n(1),n(2),1]);
finw = @(n)(-fout(-n));
%% Benchmark mass and stiffness matrix assembly
zone = 5;
tic
[M,K] = DG2D_LoopTriangles_UpwindFVS(Ax,Ay,B,fout,finw,DGMesh,zone);
t = toc();
fprintf('Constant: %1.2g s\n',t);
clear M K
tic
[M,K] = DG2D_LoopTriangles_UpwindFVS_Parallel(Ax,Ay,B,fout,finw,DGMesh,zone);
t = toc();
fprintf('Constant: %1.2g s (parallel)\n',t);
clear M K
tic
[M,K] = DG2D_LoopTriangles_UpwindFVS(@(x,y)(Ax),@(x,y)(Ay),@(x,y)(B),@(n,x,y)(fout(n)),@(n,x,y)(finw(n)),DGMesh,zone);
t = toc();
fprintf('Variable: %1.2g s\n',t);
clear M K
tic
[M,K] = DG2D_LoopTriangles_UpwindFVS_Parallel(@(x,y)(Ax),@(x,y)(Ay),@(x,y)(B),@(n,x,y)(fout(n)),@(n,x,y)(finw(n)),DGMesh,zone);
t = toc();
fprintf('Variable: %1.2g s (parallel)\n',t);
fprintf('Order N=%d, Nq=%d, DoF=%d\n',DGCell.N,Nq,size(M,1));
clear M K