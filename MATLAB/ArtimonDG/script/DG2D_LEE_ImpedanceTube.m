%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - II LEE 2D (Acoustics)
%-------------------------------------------------------------------------
% Impedance tube
% (Source)                                             (Impedance)
%   |------------------------------------------------------|
%  x=0                                                    x=L
% Validation case
%       Comparison with the 1D semi-analytical solution.
%       Comparison between Z and beta formulation
% Impedance boundary condition can be nonlinear. The following nonlinear
% algebraic model is implemented
%       Z(u) = a0*u + Cd * u * |u|
%       beta(v) = (Z-I)(Z+I)^(-1) (scattering operator)
% Rmk: The implementation of a nonlinear impedance boundary condition
% relies on using the 'DirectDelay' field (originally intested for a direct
% delay term z(t)=delta(t-tau) in the impedance). This is a quick and
% convenient trick.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
run 'DG2D_LEELongitudinal_ZoneDefinition.m'
%% Load mesh
meshfile = 'ImpedanceTubeUnstructured240triangles.msh';
%meshfile = 'ImpedanceTubeUnstructured562triangles.msh';
%meshfile = 'ImpedanceTubeUnstructured806triangles.msh';
N = 3; % polynomial order
    % Read mesh
m = load_gmsh2(meshfile,[1 2]); % read lines (1) and triangles (2)
nodes = m.POS(:,1:2);
triangles = m.TRIANGLES(:,[4,1,2,3]);
boundFaces = m.LINES(:,[3,1,2]);
physicalEntitiesIdx = m.physicalEntitiesIdx;
physicalEntitiesName = m.physicalEntitiesName;
mesh = Mesh_2D_Triangle(nodes,boundFaces,triangles,physicalEntitiesIdx,physicalEntitiesName);
clear m nodes triangles boundFaces physicalEntitiesIdx physicalEntitiesName
    % Initialize DG cell & mesh
DGCell = DGCell_2D_Triangle(N); % cell order
DGMesh = DGMesh_2D_Triangle(mesh,DGCell);
mesh.show();
    % -- Probe location
    % Check that the probe is properly located
x_probe = 0; y_probe = 0; % probe location
    % Locate probe
k = DGMesh.getNearestNodeIndex([x_probe(:),y_probe(:)]);
    % indices in solution vector
idxVar = [1,3]; % Variable to observe (u and p/z0)
idxQ = zeros(length(idxVar),length(k));
for i=1:length(k)
    idxQ(:,i) = cst.Nq*(k(i)-1) + idxVar;
end
idx = idxQ(:);
%% I Case definition: Impedance tube on [0,L]
% Impedance boundary condition at x=L
L = mesh.bound(2,1); % length of impedance tube
    % Air @295 K (used for impedance identification in [Jones2005])
gamma = 1.4;
cst.c0 = sqrt(gamma*287.058*295); % sound speed (m/s)
    % No base flow
cst.u0 = @(x,y)0; cst.du0dx = @(x,y)0; cst.du0dy = @(x,y)0;
cst.v0 = @(x,y)0; cst.dv0dx = @(x,y)0; cst.dv0dy = @(x,y)0;
    % Matrices
Ax_fun = @(x,y)Ax(cst.u0(x,y),cst.c0);
Ay_fun = @(x,y)Ay(cst.v0(x,y),cst.c0);
B_fun = @(x,y)B(cst.u0(x,y),cst.du0dx(x,y),cst.du0dy(x,y),cst.v0(x,y),cst.dv0dx(x,y),cst.dv0dy(x,y),cst.c0);
%% I Misc Constants
    % Algebraic Cummings model
    % z(u)/z0=a0*u+(Cd/c0)*|u|/c0*u
beta_Cummings_denom = @(v,a0,Cd,c0)(1+sqrt(1+4*Cd*abs(v)./((1+a0)^2*c0)));
beta_Cummings = @(v,a0,Cd,c0)(beta_fun(a0)*2*v./beta_Cummings_denom(v,a0,Cd,c0)+(Cd/(c0*(1+a0)^2))*4*abs(v).*v./(beta_Cummings_denom(v,a0,Cd,c0).^2));
Z_Cummings = @(u,Cd,c0)(Cd/c0)*u.*abs(u);
%% [Impedance Boundary Condition] Constant value (beta formulation)
beta0 = 1; % in (-1,1) for a passive operator
    % -- DG: beta formulation
cst.Impedance = ImpedanceBoundaryCondition(sprintf('Beta0-%1.2e_Beta-Flux',beta0),beta_fun(beta0),0,{0},{0},{0},{0},{0},{0},{0});
cst.Impedance.printImpedanceSummary();
cst.Impedance.plot(linspace(1,30,1e2),'reflcoeff');
        % Numerical flux function
IBC_flux = 'Impedance_FluxBeta';
    % -- Analytical solution
AnalyticalSolution = @(dt,f,tf,L,c0)LEE_AnalyticalSolution_ImpTube_Temp(f,beta0,0,0,0,[],[],L,c0,tf,dt,0);
%% [Impedance Boundary Condition] Proportional derivative (Z formulation)
% z(s) = a(1) + a(2)*s
% beta(s) = 1 + h(s), where h(s) = -2/(1+z(s)) has a diffusive
a0 = -1;
a1 = 0;
    % -- DG: Z formulation
cst.Impedance = ImpedanceBoundaryCondition(sprintf('Prop-Der-a0-%1.2e-a1-%1.2e_Z-Flux',a0,a1),beta_fun(a0),a1,{0},{0},{0},{0},{0},{0},{0});
cst.Impedance.printImpedanceSummary();
clf
cst.Impedance.plot(linspace(1,30,1e2),'impedance');
        % Numerical flux function
IBC_flux = 'Impedance_FluxZ';
    % Impedance tube analytical solution
AnalyticalSolution = @(dt,f,tf,L,c0)LEE_AnalyticalSolution_ImpTube_Temp(f,1,-2/a1,(1+a0)/a1,0,0,0,L,c0,tf,dt,0);
%% [Impedance Boundary Condition] Linear TDIBC (beta formulation)
% beta(s) = beta0  + mu_k/(s+xi_k) + exp(-s*delay)*mu_k/(s+xi_k)
beta0 = 0.1;
mu1 = [1];
xi1 = -[-1];
delay = 1e-1;
mu2 = [1];
xi2 = -[-5];
    % -- DG: beta formulation
BndOp_op  = DiffusiveOperator(mu1,xi1,'Type','Standard');
BndOpDelay_op  = DiffusiveOperator(mu2,xi2,'Type','Standard');
cst.Impedance = ImpedanceBoundaryCondition('Linear-TDIBC-Beta',beta_fun(beta0),0,{0},{0},{1},{BndOp_op},{1},{BndOpDelay_op},{delay});
cst.Impedance.printImpedanceSummary();
clf
cst.Impedance.plot(linspace(1,30,1e2),'reflcoeff');
        % Numerical flux function
IBC_flux = 'Impedance_FluxBeta';
    % -- Analytical solution
AnalyticalSolution = @(dt,f,tf,L,c0)LEE_AnalyticalSolution_ImpTube_Temp(f,beta0,mu1,xi1,delay,mu2,xi2,L,c0,tf,dt,0);
%% [Impedance Boundary Condition] Fractional polynomial
% z(s) = a(1) +  a(2)*sqrt(s) + a(3)*s
% beta(s) = 1 + h(s), where h(s) = -2/(1+z(s)) has a diffusive
% representation without poles.
a = [1,1,0];
z = @(s)(a(1)+a(2)*sqrt(s)+a(3)*s);
h_an = @(s)(-2./(1+z(s)));
mu_an=@(xi)(((h_an(xi*exp(-1i*pi))-h_an(xi*exp(1i*pi)))/(2*1i*pi)));
    % -- DG: beta formulation
Nxi = 4;
[xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(mu_an,Nxi,'CoVParam',1.4);
BndOp_op  = DiffusiveOperator(mu,xi,'Laplace',h_an,'Type','Standard');
cst.Impedance = ImpedanceBoundaryCondition(sprintf('Linear-TDIBC-Beta-FractionalPolynomial-Nxi%d',Nxi),beta_fun(1),0,{0},{0},{1},{BndOp_op},{0},{0},{0});
cst.Impedance.printImpedanceSummary();
        % Choose numerical flux function
IBC_flux = 'Impedance_FluxBeta';
    % -- DG: Z formulation
Nxi = 1;
[xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(@(xi)(1./(pi*sqrt(xi))),Nxi);
BndOp_op  = DiffusiveOperator(mu,xi,'Laplace',@(s)(sqrt(s)),'Type','Extended');
cst.Impedance = ImpedanceBoundaryCondition(sprintf('Linear-TDIBC-Z-FractionalPolynomial-Nxi%d',Nxi),beta_fun(a(1)),a(3),{0},{0},{a(2)},{BndOp_op},{0},{0},{0});
cst.Impedance.printImpedanceSummary();
%cst.Impedance.plot(linspace(0,1e4,1e2),'impedance');
        % Choose numerical flux function
%IBC_flux = 'Impedance_FluxZunstable';
IBC_flux = 'Impedance_FluxZ';
    % -- Analytical solution
[xi_ex,mu_ex] = ComputeDiscreteDiffusiveRep_QuadratureGL(mu_an,10,'CoVParam',1.4);
AnalyticalSolution = @(dt,f,tf,L,c0)LEE_AnalyticalSolution_ImpTube_Temp(f,1,mu_ex,xi_ex,0,0,0,L,c0,tf,dt,0);
%% [Impedance Boundary Condition] Linear TDIBC from file
load('GFIT-MP-SPL120dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-6Oscillatory2DiffusivePolesFinal','TDIBC');
%load('GFIT-MP-SPL120dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4Oscillatory2DiffusivePolesFinal','TDIBC');
cst.Impedance = TDIBC;
cst.Impedance.checkRealizationStability();
    % check passivity over given frequency range
cst.Impedance.checkPassivity(linspace(1,1e4,1e2),'reflcoeff',1,1);
cst.Impedance.printImpedanceSummary();
clf
cst.Impedance.plot(linspace(0,1e4,1e3),'reflcoeff');
        % Numerical flux function
IBC_flux = 'Impedance_FluxBeta';
    % Impedance tube analytical solution
[weightBndOp,weightBndOpDelay,delay,xi,sn] = cst.Impedance.getPolesandWeights();
weightBndOp=weightBndOp{1};
weightBndOpDelay=weightBndOpDelay{1};
delay = delay{1};
beta0 = beta2z(cst.Impedance.beta0(L,mesh.bound(2,2)));
AnalyticalSolution = @(dt,f,tf,L,c0)LEE_AnalyticalSolution_ImpTube_Temp(f,beta0,weightBndOp,-[xi(:);sn(:)],delay,weightBndOpDelay,-[xi(:);sn(:)],L,c0,tf,dt,0);
%% [Impedance Boundary Condition] Algebraic Cummings model (beta formulation)
Cd= 0.5; a0 = Cd/5;
Cd= 1; a0 = 0;
NonLinearOp_op = @(v)beta_Cummings(v,a0,Cd,cst.c0); % Boundary operator to be used
    % -- DG: beta formulation
cst.Impedance = ImpedanceBoundaryCondition(sprintf('Algebraic-Cummings-a0-%1.2e-Cd%1.2e_Beta-Flux',a0,Cd),beta_fun(0),0,{1},{0},{0},{0},{0},{0},{0});
cst.Impedance.printImpedanceSummary();
        % Numerical flux function
IBC_flux = 'Impedance_FluxBeta';
    % Impedance tube analytical solution
AnalyticalSolution = @(dt,f,tf,L,c0)LEE_AnalyticalSolution_ImpTube_NonLinear_Temp(f,@(v)beta_Cummings(v,a0,Cd,cst.c0),L,c0,tf,dt,0);
%% [Impedance Boundary Condition] Algebraic Cummings model (Z formulation)
Cd= 0.5; a0 = Cd/5;
Cd= 1; a0 = 0;
NonLinearOp_op = @(u)Z_Cummings(u,Cd,cst.c0); % Boundary operator to be used
    % -- DG: Z formulation
cst.Impedance = ImpedanceBoundaryCondition(sprintf('Alegbraic-Cummings-a0-%1.2e-Cd%1.2e_Z-Flux',a0,Cd),beta_fun(a0),0,{1},{0},{0},{0},{0},{0},{0});
cst.Impedance.printImpedanceSummary();
        % Numerical flux function
IBC_flux = 'Impedance_FluxZ';
    % Impedance tube analytical solution
AnalyticalSolution = @(dt,f,tf,L,c0)LEE_AnalyticalSolution_ImpTube_NonLinear_Temp(f,@(v)beta_Cummings(v,a0,Cd,cst.c0),L,c0,tf,dt,0);
%% [DG] Build global formulation
zoneCase = DG2D_LEELongitudinal_createZone(struct('Interior',[5],'Source',[4],'Wall',[1,3],IBC_flux,[2]),cst,zone);
    % Build mass and stiffness: global formulation
if isempty(cst.Impedance.DirectDelay) % linear IBC
    [M,K,F,cst.DirectDelay,cst.BndOp,cst.BndOpDelay]=DG2D_LEE_buildGlobalFormulation(Ax_fun,Ay_fun,B_fun,DGMesh,zoneCase,cst.Impedance);
else % nonlinear IBC
    [M,K,F,cst.NonLinearOp,cst.BndOp,cst.BndOpDelay]=DG2D_LEE_buildGlobalFormulation(Ax_fun,Ay_fun,B_fun,DGMesh,zoneCase,cst.Impedance);
end
%% [Time integration] Definition of source and final time
% Outputs:
%   f(t)    scalar source function, supported in [0,tsrc]
%   tsrc    support of f(t)
%   tf      computation final time
% Rmk: p_dB = 20*log10(p_RMS/pref) = 20*log10(p_peak/(sqrt(2)*pref))
    % -- Monochromatic source
T_src = (1/3)*(2*L/cst.c0); % source period (s)
nper = 3; % number of period to compute
SPL_src = 10; pref=2e-5; % Pa
A_src = sqrt(2)*pref/cst.z0 * 10^(SPL_src/20);
w_src = 2*pi/T_src; % source angular frequency (rad/s)
tsrc = nper*T_src;
f = @(t)((A_src*sin(w_src*t)).*(t>=0).*(t<=(nper*T_src)));
SourceName = sprintf('Monochromatic-Amp%1.5g-SPL%1.5gdB-%1.5gkHz',A_src,SPL_src,w_src/(2*pi*1e3));
if (nper*T_src)>(2*L/(cst.c0))
   warning('Both source and reflection will be measured at x=0.\n');
end
    % -- Gaussian-modulated sinusoidal pulse
fc = 2e3;     % fc: central frequency (Hz)
bw = 0.2;    % bw: frequency bandwidth (Hz) at bwr
bwr = -15;     % bwr: reference level for band width (dB)
SPL_src = 145; pref=2e-5; % Pa
A_src = sqrt(2)*pref/cst.z0 * 10^(SPL_src/20);
f = @(t)A_src*GaussianWavePacket(t,fc,bw,bwr);
w_src = 2*pi*fc;
tsrc = 1.25*gauspuls('cutoff',fc,bw,bwr);
SourceName = sprintf('GaussianPulse-Amp%1.5g-SPL%1.5gdB-fc%1.5gkHz-bw%1.5gHz-bwr%1.5gdB',A_src,SPL_src,fc*1e-3,bw,abs(bwr));
t = linspace(0,tsrc,1e3);
clf
plot(t*cst.c0/L,f(t));
title(sprintf('Gaussian pulse. Peak: %1.2e. RMS: %1.2edB',max(f(t)),20*log10(rms(f(t))/pref)));
    % -- Smoother at t=0
    % (can cause NaN, so not the best idea in the world)
% f_smooth = @(t)(AnF_smoothHeaviside(t,0,T_src));
% f = @(t)f(t).*f_smooth(t).*f_smooth(nper*T_src-t);
%f_smooth = @(t)(AnF_bumpFunction(t,0,T_src));
%f = @(t)(exp(-(t/T_source).^2));
    % -- Set final time
tf = max(tsrc +  2*L/cst.c0,5*L/cst.c0); % Final time
if tsrc>(2*L/cst.c0)
   warning('Source is too long: tsrc=%1.1e*2L/c0',tsrc/(2*L/cst.c0));
end
% [Time Integration] CFL number and delay
    %  Time integration: CFL
CFL = 1.06; % stability CFL number for N=3 (Unstructured mesh, 562 triangles)
CFL = 0.85; % stability CFL number for N=3 (Unstructured mesh, 240 triangles)
    % Time-domain solving: initial condition and source
dt = CFL*min(mesh.charLength)/norm(cst.c0); % Time step
    % Time delay
Delay_Np = 4; % # of nodes per element
Delay_Nk = 1; % # of elements
PPW_delay = @(fmax,delay)Delay_Np*Delay_Nk/(delay*fmax);
    % Trick: change NonLinearOp to add nonlinear operator
if isfield(cst,'NonLinearOp')
    for i=1:length(cst.NonLinearOp)
        cst.NonLinearOp{i}.Operator = NonLinearOp_op;
    end
end
    % Check characteristic time of boundary operators
fprintf('With RK 8-4 (C=7.7), conservative limit: |xi|<=%1.3g\n',0.3*7.7/dt);
for i=1:length(cst.Impedance.BndOp)
    fprintf('BndOp{%d} max. pulsation: %1.3g\n',i,cst.Impedance.BndOp{i}.Operator.computeMaxPulsation());
end
for i=1:length(cst.Impedance.BndOpDelay)
    fprintf('BndOpDelay{%d} max. pulsation: %1.3g\n',i,cst.Impedance.BndOpDelay{i}.Operator.computeMaxPulsation());
    fprintf('BndOpDelay{%d} CFL: %1.3g.\n',i,Delay_Nk*dt/cst.Impedance.BndOpDelay{i}.Delay);
    fprintf('BndOpDelay{%d} PPW@%1.5gkHz: %1.3g.\n',i,w_src/(2*pi*1e3),PPW_delay(w_src/(2*pi),cst.Impedance.BndOpDelay{i}.Delay));
end
% [Time integration] Numerical and exact solution
    % Compute DG solution
if isempty(cst.Impedance.DirectDelay) % linear IBC
    [x,t_DG] = DG2D_SolveTimeDomain(M,K,F,DGMesh,cst.Nq,tf,dt,'nidx',idx,'x0',zeros(size(M,1),1),'xs',@(t)([f(t);zeros(size(t));f(t)]),'storeOnlyLast',0,'DirectDelay',cst.DirectDelay,'LTIOp',cst.BndOp,'LTIOpDelayed',cst.BndOpDelay,'Np',Delay_Np,'Nk',Delay_Nk);    
else % nonlinear IBC
    [x,t_DG] = DG2D_SolveTimeDomain_NonLinear(M,K,F,DGMesh,cst.Nq,tf,dt,'nidx',idx,'x0',zeros(size(M,1),1),'xs',@(t)([f(t);zeros(size(t));f(t)]),'storeOnlyLast',0,'NonLinearOp',cst.NonLinearOp,'LTIOp',cst.BndOp,'LTIOpDelayed',cst.BndOpDelay,'Np',Delay_Np,'Nk',Delay_Nk);
end
    % Extract quantities of interest
u_DG = real(x(1,:)); p_DG = real(x(2,:));
    % Compute analytical solution
dt_ex=dt;
[u_ex,p_ex,t_ex] = AnalyticalSolution(dt_ex,f,tf,L,cst.c0);
%%u_ex = 0*u_ex; p_ex=0*p_ex;
uex=real(u_ex); p_ex=real(p_ex);
err_L2=0; err_Linf=0;
if length(u_ex)==length(u_DG)
    err_L2 =  sqrt(mean(([u_DG(:);p_DG(:)]-[u_ex(:);p_ex(:)]).^2));
    err_Linf = max(abs([u_DG(:);p_DG(:)]-[u_ex(:);p_ex(:)]));
end
% [PostProcessing] Time domain plot
t_DG_plot = t_DG*cst.c0/L; t_ex_plot = t_ex*cst.c0/L;
figure(4)
clf
subplot(3,1,1)
hold all
leg=cell(0);
plot(t_ex_plot,u_ex/A_src);
leg{end+1}=sprintf('Exact');
plot(t_DG_plot,u_DG/A_src);
leg{end+1}=sprintf('DG (Order N=%d,CFL=%1.2g,%s) (%s)',N,CFL,meshfile,cst.Impedance.Name);
plot([tsrc,tsrc]*cst.c0/L,ylim,'k--');
legend(leg)
title(sprintf('Imp. tube. %s\nL=%1.3g, c0=%1.3g. Probe at x=%1.3g\n ErrL2= %1.2e, Linf=%1.2e\nSPL_{src}=%1.3gdB, f_{src}=%1.4gHz',cst.Impedance.Name,L,cst.c0,x_probe,err_L2,err_Linf,SPL_src,w_src/(2*pi)));
xlabel('t*c0/L');
ylabel('u/A_src');
ylim([-1,1])
subplot(3,1,2)
hold all
leg=cell(0);
plot(t_ex_plot,p_ex/A_src);
plot(t_DG_plot,p_DG/A_src);
plot([tsrc,tsrc]*cst.c0/L,ylim,'k--');
xlabel('t*c0/L');
ylabel('p/A_src');
ylim([-1,1])
subplot(3,1,3)
leg=cell(0);
hold all
idx_p = logical(t_ex<=tsrc);
plot(t_ex_plot(idx_p),p_ex(idx_p)/max(p_ex),'b--');
leg{end+1}=sprintf('Source (Exact)');
idx_p = logical(t_DG<=tsrc);
plot(t_DG_plot(idx_p),p_DG(idx_p)/max(p_DG),'b');
leg{end+1}=sprintf('Source (DG)');
idx_p = logical((t_DG>=(2*L/cst.c0)).*(t_DG<=(tsrc+2*L/cst.c0)));
plot(t_DG_plot(idx_p)-2,p_DG(idx_p)/max(p_DG),'r');
leg{end+1}=sprintf('Reflected (DG)');
idx_p = logical((t_ex>=(2*L/cst.c0)).*(t_ex<=(tsrc+2*L/cst.c0)));
plot(t_ex_plot(idx_p)-2,p_ex(idx_p)/max(p_ex),'r--');
leg{end+1}=sprintf('Reflected (Exact)');
plot([tsrc,tsrc]*cst.c0/L,ylim,'k--');
legend(leg);
xlabel('t*c0/L');
ylabel('p/p_{max}');
ylim([-1,1])
%% [PostProcessing] Power spectral density
% Spectra of source and reflection.
clf
figure(1)
clf
hold all
    % Source (DG)
idx_p = logical(t_DG<=tsrc);
p_dsp = p_DG(idx_p);
t_dsp = t_DG_plot(idx_p);
fs = 1/max(diff(t_dsp)); % sample frequency (Hz)
[psd,freq] = periodogram(p_dsp,rectwin(length(t_dsp)),length(t_dsp),fs);
plot(freq,10*log10(psd/max(psd)),'DisplayName','Source (DG)');
    % Reflection (DG)
idx_p = logical((t_DG>=(2*L/cst.c0)).*(t_DG<=(tsrc+2*L/cst.c0)));
p_dsp = p_DG(idx_p);
t_dsp = t_DG_plot(idx_p);
fs = 1/max(diff(t_dsp)); % sample frequency (Hz)
[psd,freq] = periodogram(p_dsp,rectwin(length(t_dsp)),length(t_dsp),fs);
plot(freq,10*log10(psd/max(psd)),'DisplayName','Reflected (DG)');
% xlim([0,1e3]);
% ylim([-50,0])
grid on
legend('show');
xlabel('frequency (Hz)');
title(sprintf('Power spectral density (dB/Hz) fs=%1.3gkHz',fs/1e3));
    % Reflected (DG)
%% Export to file
    % -- Exact solution
namestr = sprintf('ImpedanceTube_%s_%s_Exact.csv',cst.Impedance.Name,SourceName);
namestr = strrep(namestr,'+','p');
t_ex_export = t_ex(:)*cst.c0/L; p_ex = p_ex(:);
istr = 1:floor(length(t_ex)/2):length(t_ex);
istr = 1:1:length(t_ex);
dlmwrite(namestr,'t*c0/L,p','Delimiter','');
dlmwrite(namestr,[t_ex_export(istr),p_ex(istr)/A_src],'-append','Delimiter',',','newline','unix','precision','%1.6e');
    % -- DG
if isempty(cst.Impedance.BndOpDelay) % no delay
    namestr = sprintf('ImpedanceTube_%s_%s_DG%d-%dtriangles-CFL%3.2g.csv',cst.Impedance.Name,SourceName,DGCell.N+1,mesh.N,CFL);
else % delay
    namestr = sprintf('ImpedanceTube_%s_%s_DG%d-%dtriangles-CFL%3.2g_Delay-PPW%3.3g-Np%d-Nk%d.csv',cst.Impedance.Name,SourceName,DGCell.N+1,mesh.N,CFL,PPW_delay(max(w_src)/(2*pi),cst.Impedance.BndOpDelay{1}.Delay),Delay_Np,Delay_Nk);
end
namestr = strrep(namestr,'+','p');
t_DG_export = t_DG(:)*cst.c0/L; p_DG = p_DG(:);
istr = 1:floor(length(t_DG)/2):length(t_DG);
istr = 1:1:length(t_DG);
dlmwrite(namestr,'t*c0/L,p','Delimiter','');
dlmwrite(namestr,[t_DG_export(istr),p_DG(istr)/A_src],'-append','Delimiter',',','newline','unix','precision','%1.6e');
%% IV Compute impedance curve
CFL = 0.5; % CFL number
dt = CFL*min(mesh.charLength)/norm(cst.c0); % Time step

pref=2e-5; % Pa
SPl_src_tab = [100];
w_src_tab = (2*pi*cst.c0/L) * [0.1,0.5,1,2];
Z = zeros(length(SPl_src_tab),length(w_src_tab));
for i = 1:length(w_src_tab) % Loop over source freq
   for j = 1:length(SPl_src_tab)  % Loop over source SPL
            % define source
    A_src = pref/cst.z0 * 10^(SPl_src_tab(j)/20);
    w_src = w_src_tab(i);
    T_src = 2*pi/w_src;
    f = @(t)(A_src*sin(w_src*t)).*(t<=T_src); % causal source
    tf = T_src +  3*L/cst.c0; % Final time
            % Solve time-domain
    %[x,t] = DG2D_SolveTimeDomain_NonLinear(M,K,F,DGMesh,cst.Nq,tf,dt,'nidx',idx,'x0',zeros(size(M,1),1),'xs',@(t)([f(t);zeros(size(t));f(t)]),'storeOnlyLast',0,'NonLinearOp',cst.NonLinearOp,'LTIOp',cst.BndOp,'LTIOpDelayed',cst.BndOpDelay,'Np',2,'Nk',3);
    %u = x(1,:); p = x(2,:);
            % Compute analytical solution
    %dt_ex=dt/1e3;
    [u,p,t] = AnalyticalSolution(dt,f,tf,L,cst.c0);
            % Compute Impedance
    fprintf('A_src = %1.1e\n',A_src);
    Z(i,j) = LEE_ImpTube_ZMeasurement(t,p,L,cst.c0,w_src);
   end
end

% --
% Plot Z(f,SPL)
figure(1)
clf
subplot(2,1,1)
hold all
leg=cell(0);
plot(w_src_tab,a0*ones(size(w_src_tab)),'x--');
leg{end+1} = sprintf('Model');
for j=1:length(SPl_src_tab)
    plot(w_src_tab,real(Z(:,j)),'-o');
    leg{end+1}=sprintf('SPL=%3.1g',SPl_src_tab(j));
end
legend(leg);
xlabel('w (rad/s)');
ylabel('Re(Z)')
ylim([0,10])
subplot(2,1,2)
hold all
plot(w_src_tab,a1*w_src_tab,'x--');
leg{end+1} = sprintf('Model');
leg=cell(0);
for j=1:length(SPl_src_tab)
    plot(w_src_tab,imag(Z(:,j)),'-o')
end
xlabel('w (rad/s)');
ylabel('Im(Z)')
ylim([-5,5])