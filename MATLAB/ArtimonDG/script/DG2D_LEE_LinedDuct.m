%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - LEE 2D Longitudinal - Lined duct
%-------------------------------------------------------------------------
% Purpose:
%   - time-domain simulation of a lined duct
%   - compute SPL along the wall
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
run 'DG2D_LEELongitudinal_ZoneDefinition.m'
%% Load mesh
meshfile = 'NASA-GIT-2005_veryshort_106triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_106triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_360triangles.msh';
meshfile = 'NASA-GIT-2005_veryshort_240triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_552triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_1364triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_1364triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_2992triangles_NoBoundaryLayer.msh';
%meshfile = 'NASA-GIT-2005_verylong_coarse.msh';
N = 4; % order
m = load_gmsh2(meshfile,[1 2]); % read lines (1) and triangles (2)
nodes = m.POS(:,1:2); triangles = m.TRIANGLES(:,[4,1,2,3]);
boundFaces = m.LINES(:,[3,1,2]); physicalEntitiesIdx = m.physicalEntitiesIdx;
physicalEntitiesName = m.physicalEntitiesName;
mesh = Mesh_2D_Triangle(nodes,boundFaces,triangles,physicalEntitiesIdx,physicalEntitiesName);
clear m nodes triangles boundFaces physicalEntitiesIdx physicalEntitiesName
mesh.printSummary();
    % Initialize DG cell & mesh
DGCell = DGCell_2D_Triangle(N); % cell order
DGMesh = DGMesh_2D_Triangle(mesh,DGCell);
%mesh.show();
%DGMesh.showDGNodes();
%DGCell.show();
    % Impedance boundary condition on the upper wall
IBC_flux = 'Impedance_FluxBeta'; % Impedance numerical flux function
zoneDef = struct('Interior',[5],'Source',[4],'Outlet',[2],'Wall',1,IBC_flux,3);
%% Experimental duct: Ozyoruk
    % -- Experimental parameters
c0_exp = 340.29; % experimental sound speed (m/s)
xliner_exp = [0.20955,0.5969]; % experimental liner zone (m)
Lduct_exp = 0.8382; % duct experimental length (m)
H_exp = 0.3*Lduct_exp; % duct experimental height (m)
x_probe_exp = xliner_exp; 
%% Experimental duct: NASA GIT (2005)
    % Load full experimental data
ExpData = ExpDataLoadNASAGIT2005CT57();
% Air @295 K (used for impedance identification in [Jones2005])
nu = 1.568e-5; 
gamma = 1.4;
c0_exp = sqrt(gamma*287.058*295); % experimental sound speed (m/s)
Pr = 0.707;
    % -- Experimental parameters
xliner_exp = [0.153,0.559]; % experimental liner zone (m)
Lduct_exp = 0.8128; % duct experimental length (m)
H_exp = 0.051; % duct experimental height (m)
    % Microphone location (m)
x_probe_exp = 1e-3*[0.1,25.5,101.7,203.3,228.7,254.1,279.4,304.8,317.6,330.3,343.0,355.7,368.4,381.1,393.8,406.5,419.1,431.9,444.6,457.3,470.0,482.7,495.4,508.1,533.5,558.9,584.3,609.7,711.4,787.5,812.9];
y_probe_exp = 0;
%% Duct scaling
% Experimental duct is on [0,Lduct_DG].
% Speed of sound is scaled to keep both numerical and experimental 
% frequencies identical.
    % -- Numerical parameters
Lduct_num = Lduct_exp; % numerical duct length (choice)
c0_num = c0_exp * Lduct_num/Lduct_exp; % numerical sound speed
L_num = mesh.bound(2,1)-mesh.bound(1,1); % numerical length
if L_num<Lduct_num
   error('Numerical length is smaller than chosen numerical duct length.'); 
end
H_num = mesh.bound(2,2)-mesh.bound(1,2); % numerical height
if abs(H_num - H_exp * (Lduct_num/Lduct_exp))<eps
    warning('Numerical aspect ratio does not match experimental one.');
end
xliner_num = xliner_exp * (Lduct_num/Lduct_exp); % numerical liner zone
tfNoRefl_num = (2*L_num-Lduct_num)/c0_num; % numerical simulation time w/o reflection
        % Min. Freq w/o reflection
        % (Lduct_num/c0_num+3*T_source <= tfNoRefl_num
w_source_NoRefl = 2*pi*(3)*(tfNoRefl_num-Lduct_num/c0_num)^(-1);
        % Duct no flow cut-off frequency (identical to experimental one)
w_cutoff = 2*pi*c0_num/(2*H_num);
w_cutoff_longi_num = pi*c0_num/(mesh.bound(2,1)-mesh.bound(1,1));
        % DG max frequency
        % 1D estimation based on 'doF_per_1Dcell'
DoF_per_1Dcell = N+1; % choice (typically ~# of node on edge)
w_to_PPW = @(w)2*pi*c0_num*DoF_per_1Dcell/(min(mesh.charLength(:))*w);
PPW_to_w = @(PPW)2*pi*c0_num*DoF_per_1Dcell/(min(mesh.charLength(:))*PPW);
        % Min. # of PPPW for 1D Wave Equation from [Hu,1999].
        % Function of number of nodes per cell.
dof_to_PPWmin = [0,15.7,10.5,7.9,6.8,6.2,6.2,5.3]; % value for N=7 made up
fprintf('-- Numerical simulation characteristics.\n');
fprintf('*Dimensions: numerical (experiment)\n');
fprintf('\tDuct length: %1.3e (%1.3e)\n',Lduct_num,Lduct_exp);
fprintf('\tTotal length: %1.3e (N/A)\n',L_num);
fprintf('\tHeight: %1.3e (%1.3e)\n',H_num,H_exp);
fprintf('\tAspect ratio: %1.3e (%1.3e)\n',Lduct_num/H_num,Lduct_exp/H_exp);
fprintf('\tSound speed: %1.3e (%1.3e)\n',c0_num,c0_exp);
fprintf('\tLiner position: [%1.3e,%1.3e] ([%1.3e,%1.3e])\n',xliner_num(1),xliner_num(2),xliner_exp(1),xliner_exp(2));
fprintf('*Frequencies/times of interest\n');
fprintf('No flow cut-off: %1.3e rad/s %1.3e Hz\n',w_cutoff,w_cutoff/(2*pi));
fprintf('Max. time w/o reflection: %1.3e s\n',tfNoRefl_num);
fprintf('Min. freq. w/o refl.: %1.3e rad/s %1.3e Hz\n',w_source_NoRefl,w_source_NoRefl/(2*pi));
fprintf('Mesh cut-off longitudinal: %1.3e rad/s %1.3e Hz \n',w_cutoff_longi_num,w_cutoff_longi_num/(2*pi));
fprintf('*DG frequency properties (assuming DoF=%d)\n',DoF_per_1Dcell);
fprintf('Cut-off: %d PPW \n',w_to_PPW(w_cutoff));
fprintf('Min. freq. w/o refl.: %d PPW\n',w_to_PPW(w_source_NoRefl));
fprintf('Min. # of PPW [Hu,1999]: %d\n',dof_to_PPWmin(DoF_per_1Dcell));
for PPW=3:10
    fprintf('\tMax. freq. @PPW_min=%d: %1.3e rad/s %1.3e Hz\n',PPW,PPW_to_w(PPW),PPW_to_w(PPW)/(2*pi));
end
fprintf('--\n');
%% Probe placement
    % Denser bunching of probes
x_probe = linspace(x_probe_exp(1),x_probe_exp(end),150);
y_probe = 0*x_probe; % probe location
    % Locate probe
idxProbeNodes = DGMesh.getNearestNodeIndex([x_probe(:),y_probe(:)]);
%DGMesh.showDGNodes('HighlightNodesIndex',idxNodes);
    % indices in solution vector
idxVar = [3]; % Variable to observe (p/z0)
idxQ = zeros(length(idxVar),length(idxProbeNodes));
for i=1:length(idxProbeNodes)
    idxQ(:,i) = cst.Nq*(idxProbeNodes(i)-1) + idxVar;
end
idxQ = idxQ(:);
%% Base flow definition
cst.c0 = c0_num;
Mavg = 0; % average Mach number
a = mesh.bound(1,2); b = mesh.bound(2,2);
y2r = @(y,a,b)2/(b-a)*(y-(b+a)/2); y2r_dy = @(a,b)2/(b-a);
r2y = @(r,a,b) (b+a)/2 + (b-a)/2*r; r2y_dr = @(a,b)(b-a)/2;
    % -- Uniform base flow
cst.u0 = @(x,y)Mavg; cst.du0dx = @(x,y)0; cst.du0dy = @(x,y)0;
cst.v0 = @(x,y)0; cst.dv0dx = @(x,y)0; cst.dv0dy = @(x,y)0;
FlowName = sprintf('Uniform flow M_{avg}=%1.3g',Mavg);
    % -- Poiseuille base flow
% cst.u0 = @(x,y)cst.c0*M0_Poi(y2r(y,a,b),Mavg); 
% cst.du0dy = @(x,y)cst.c0*y2r_dy(a,b).*dM0_dr_Poi(y2r(y,a,b),Mavg);
% cst.v0 = @(x,y)0; cst.dv0dx = @(x,y)0; cst.dv0dy = @(x,y)0;
% FlowName = sprintf('Poiseuille M_{avg}=%1.3g',Mavg);
    % -- Hyperbolic velocity profile
% delta = 0.3; % adimensional boundary layer thickness (in (0,1))
% cst.u0 = @(x,y)cst.c0*M0_Hy(y2r(y,a,b),Mavg,delta);
% cst.du0dy = @(x,y)cst.c0*y2r_dy(a,b).*dM0_dr_Hy(y2r(y,a,b),Mavg,delta);
% cst.v0 = @(x,y)0; cst.dv0dx = @(x,y)0; cst.dv0dy = @(x,y)0;
% FlowName = sprintf('Hyperbolic M_{avg}=%1.3g, delta=%1.3g',Mavg,delta);
    % -- Average profile
% delta = 0.3;
% alpha = 0.5; % in (0,1)
% cst.u0 = @(x,y)cst.c0*M0_Av_Hy(y2r(y,a,b),Mavg,delta,alpha);
% cst.du0dy = @(x,y)cst.c0*y2r_dy(a,b).*dM0_dr_Av_Hy(y2r(y,a,b),Mavg,delta,alpha);
% cst.v0 = @(x,y)0; cst.dv0dx = @(x,y)0; cst.dv0dy = @(x,y)0;
% FlowName = sprintf('Average M_{avg}=%1.3g delta=%1.2e alpha=%1.2e',Mavg,delta,alpha);
    % -- Curve fit
% cst.u0 = @(x,y)cst.c0*M0_fit(y);
% cst.du0dy = @(x,y)cst.c0*dM0_dy_fit(y);
% cst.v0 = @(x,y)0; cst.dv0dx = @(x,y)0; cst.dv0dy = @(x,y)0;
% FlowName = sprintf('Fit M_{avg}=%1.3g',Mavg);
    % -- Plot flow in duct
y = linspace(mesh.bound(1,2),mesh.bound(2,2),1e2);
figure(9)
clf
subplot(2,1,1)
hold all
plot(cst.u0(1,y)/cst.c0,y,'DisplayName',sprintf('M_c=%1.3g',max(cst.u0(1,y)/cst.c0)));
plot(M0_Poi(y2r(y,a,b),Mavg),y,'DisplayName',sprintf('Poiseuille M_c=%1.3g',max(M0_Poi(y2r(y,a,b),Mavg))));
set(gca,'DataAspectRatio',[1,1,1]);
legend('show');
ylim([min(y),max(y)]);
xlim([0,1]);
ylabel('y (m)');
xlabel('M0(y)');
title(FlowName)
subplot(2,1,2)
hold all
plot(cst.du0dy(1,y)/cst.c0,y,'DisplayName',sprintf('Max=%1.3g/m',max(abs(cst.du0dy(1,y)/cst.c0))));
plot(y2r_dy(a,b).*dM0_dr_Poi(y2r(y,a,b),Mavg),y,'DisplayName',sprintf('Poiseuille Max=%1.3g/m',max(abs(y2r_dy(a,b).*dM0_dr_Poi(y2r(y,a,b),Mavg)))));
legend('show');
%set(gca,'DataAspectRatio',[1,1,1]);
ylim([min(y),max(y)]);
ylabel('y (m)');
xlabel('dM0(y)/dy');
title(FlowName)
    % -- Definition of LEE Matrices
Ax_fun = @(x,y)Ax(cst.u0(x,y),cst.c0);
Ay_fun = @(x,y)Ay(cst.v0(x,y),cst.c0);
B_fun = @(x,y)B(cst.u0(x,y),cst.du0dx(x,y),cst.du0dy(x,y),cst.v0(x,y),cst.dv0dx(x,y),cst.dv0dy(x,y),cst.c0);
%% Impedance boundary condition: liner position
xliner = xliner_num; % lined zone
xliner_ind = @(x,y)((x>=xliner(1)).*(x<=xliner(2))); % indicator function
% dts = 0.05; % spread (Heaviside is dts=0)
% xliner_ind = @(x,y)(AnF_smoothHeaviside(x,xliner(1)-dts,dts).*(1-AnF_smoothHeaviside(x,xliner(2),dts)));
%%clf
%hold all
%plot(r,AnF_smoothHeaviside(r,xliner(1)-dts,dts).*(1-AnF_smoothHeaviside(r,xliner(2),dts)));
%plot(r,xliner_ind(r,1));
%% Impedance boundary condition: rigid wall
% z(s)=inf, so beta(s)=1.
cst.Impedance = ImpedanceBoundaryCondition('Hard wall.',beta_fun(1),0,{0},{0},{0},{0},{0},{0},{0});
cst.Impedance.printImpedanceSummary();
%% Impedance boundary condition: prop-der-integral model
% z(s) =  a(1)/s + a(2) + a(3)*s
    % -- Impedance definition
a = [0,0.46,0];
        % NASA-GIT-2005 Identified @500Hz 
%a = [1.68*(2*pi*500), 0.51,0]; % Mavg=0
% a = [0.59*(2*pi*500), 0.61,0]; % Mavg=0.335
        % NASA-GIT-2005 Identified @1kHz
% a = [0,0.46,0]; % Mavg=0
%a = [0, 0.17,0.14/(2*pi*1e3)]; % Mavg=0.335
    % NASA-GIT-2005 Identified @1.5kHz
% a = [0,1.02,1.30/(2*pi*1.5e3)]; % Mavg=0
% a = [0,1.18,1.27/(2*pi*1.5e3)]; % Mavg=0.335
        % NASA-GIT-2005 Identified @2kHz
% a = [0,4.05,0.62/(2*pi*2e3)]; % Mavg=0
%a = [1.64*(2*pi*2e3),4.40,0]; % Mavg=0.335
        % NASA-GIT-2005 Identified @2.5kHz
%a = [1.60*(2*pi*2.5e3), 1.54,0]; % Mavg=0
%a = [1.43*(2*pi*2.5e3), 0.93,0]; % Mavg=0.335
        % NASA-GIT-2005 Identified @3kHz
%a = [0.29*(2*pi*3e3), 0.70,0]; % Mavg=0
%a = [0.24*(2*pi*3e3), 0.73,0]; % Mavg=0.335
    % -- Reflection coefficient
cst.Impedance=[];
if a(1)==0 && a(3)==0 % Proportional impedance
    cst.Impedance = ImpedanceBoundaryCondition(sprintf('Prop. z(s)=%1.1e',a(2)),@(x,y)beta_fun(beta_fun(a(2)))*xliner_ind(x,y),0,{0},{0},{0},{0},{0},{0},{0});
elseif a(1)==0 % Proportional derivative impedance
    % beta(s) = 1 + h(s), where h(s) = -2/(1+z(s)) has a diffusive
    % representation.
    h_an = @(s)(-2./(1+a(2)+a(3)*s));
    BndOp_op  = DiffusiveOperator(-2/a(3),(1+a(2))/a(3),'Laplace',h_an,'Type','Standard');    
    cst.Impedance = ImpedanceBoundaryCondition(sprintf('Prop. Der. z(s)=%1.1e+%1.1e*s',a(2),a(3)),@(x,y)beta_fun(1)*xliner_ind(x,y),0,{0},{0},{xliner_ind},{BndOp_op},{0},{0},{0});
elseif a(3)==0 % Proportional integral impedance
    % beta(s) = beta0 + h(s), where h(s) has a diffusive
    % representation.
    h_an = @(s)(2*a(1)/(1+a(2))*1./(a(1)+(1+a(2))*s));
    BndOp_op  = DiffusiveOperator(2*a(1)/(1+a(2))^2,a(1)/(1+a(2)),'Laplace',h_an,'Type','Standard');
    cst.Impedance = ImpedanceBoundaryCondition(sprintf('Prop. Int. z(s)=%1.1e+%1.1e/s',a(2),a(1)),@(x,y)beta_fun(beta_fun(a(2)))*xliner_ind(x,y),0,{0},{0},{xliner_ind},{BndOp_op},{0},{0},{0});
end
cst.Impedance.printImpedanceSummary();
%cst.Impedance.plot(linspace(0,1e5,1e2),'reflcoeff')
fprintf('Impedance @2.5kHz is\n');
z_fun(cst.Impedance.Laplace(1i*2*pi*3e3,0.3,0))
%% Fractional impedance
mu_an=@(xi)(((h_an(xi*exp(-1i*pi))-h_an(xi*exp(1i*pi)))/(2*1i*pi)));
Nxi = 4;
[xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(mu_an,Nxi,'CoVParam',1.4);
BndOp_op  = DiffusiveOperator(mu,xi,'Laplace',h_an,'Type','Standard');
cst.Impedance = ImpedanceBoundaryCondition(sprintf('Nxi=%d',Nxi),beta_fun(1),0,{0},{0},{xliner_ind},{BndOp_op},{0},{0},{0});
cst.Impedance.printImpedanceSummary();
%% Impedance boundary condition: CT57 liner
load('GIT-CT57-SPL130dB-Mavg000-2DFEM-ReflCoeff-1xik-2sn','TDIBC');
%cst.Impedance=TDIBC;
%cst.Impedance.printImpedanceSummary();
z_fun(TDIBC.Laplace(1i*2*pi*2e3,1,1))
%% II DG solution: building global formulation
zoneCase = DG2D_LEELongitudinal_createZone(zoneDef,cst,zone);
[M,K,F,cst.DirectDelay,cst.BndOp,cst.BndOpDelay]=DG2D_LEE_buildGlobalFormulation(Ax_fun,Ay_fun,B_fun,DGMesh,zoneCase,cst.Impedance);
%% II Time integration: choose time step
CFL = 0.5;
    % Time delay
Delay_Np = 8; % # of points per cell
Delay_Nk = 12; % # of cell
    % Check characteristic time of boundary operators
dt = CFL*min(mesh.charLength)/norm(cst.c0); % Time step
fprintf('With RK 8-4 (C=7.7), conservative limit: |xi|<=%1.3g rad/s = %1.3g Hz\n',0.5*7.7/dt,0.5*7.7/dt/(2*pi));
for i=1:length(cst.Impedance.BndOp)
    fprintf('BndOp{%d} max. pulsation: %1.3g\n',i,cst.Impedance.BndOp{i}.Operator.computeMaxPulsation());
end
for i=1:length(cst.Impedance.BndOpDelay)
    fprintf('BndOpDelay{%d} max. pulsation: %1.3g\n',i,cst.Impedance.BndOpDelay{i}.Operator.computeMaxPulsation());
    fprintf('BndOpDelay{%d} CFL: %1.3g.\n',Delay_Nk*dt/cst.Impedance.BndOpDelay{i}.Delay);
end
for i=1:length(cst.Impedance.DirectDelay)
    fprintf('DirectDelay{%d} CFL: %1.3g.\n',Delay_Nk*dt/cst.Impedance.DirectDelay{i}.Delay);
end
%% II Time integration
% Format:
% p_DG{i} is length(idxNodes)xlength(t))
    % Choose source frequencies
w_source = 1*(2*pi*cst.c0/Lduct_num); % source angular frequency (rad/s)
w_source = 0.9*w_cutoff;
w_source = 2*pi*2.5e3;
T_source = (2*pi)./w_source; % source period (s)
    % Print info
fprintf('Period w/o reflection: %d\n',floor(tfNoRefl_num/T_source));
    % Time integration
p_DG = cell(length(w_source),1);
t_DG = cell(length(w_source),1);
tf_DG = cell(length(w_source),1);
for i=1:length(w_source)
        % Naive source
    f = @(t)sin(w_source(i)*t);
%    f = @(t)sin(2*pi*(2e3)*t)+sin(2*pi*(2.5e3)*t); % source @2kHz + 2.5kHz
        % Source at given SPL
%    f = @(t) 10^(128/20)*sin(2*pi*t/T_source(i))*sqrt(2);
    % -- Smoother at t=0 (not necessary)
    %     f_smooth = @(t)(AnF_smoothHeaviside(t,0,T_source));
    %     f = @(t)f(t).*f_smooth(t);
    tf = max(tfNoRefl_num,40*T_source(i)); % Final time
%    tf = max(tfNoRefl_num, 10*T_source(i));
     q_source = @(t)([f(t);zeros(size(t));f(t)]); q0 = zeros(size(M,1),1);
     [x,t] = DG2D_SolveTimeDomain(M,K,F,DGMesh,cst.Nq,tf,dt,'nidx',idxQ,'x0',q0,'xs',q_source,'storeOnlyLast',0,'DirectDelay',cst.DirectDelay,'LTIOp',cst.BndOp,'LTIOpDelayed',cst.BndOpDelay,'Np',Delay_Np,'Nk',Delay_Nk);
     p_DG{i} = x; t_DG{i} = t; tf_DG{i} = tf;
     clear x t
%       tf = min(tfNoRefl_num,5*T_source(i));
%       [x,t] = DG2D_SolveTimeDomain(M,K,F,DGMesh,cst.Nq,tf,dt,'x0',q0,'xs',q_source,'storeOnlyLast',0,'DirectDelay',cst.DirectDelay,'LTIOp',cst.BndOp,'LTIOpDelayed',cst.BndOpDelay,'Np',Delay_Np,'Nk',Delay_Nk);
end
%% Plot RMS value as a function of x
SPL_0 = 130; % Convential value of SPL at x=0
Source_idx = 1; % source to plot (for probe)
%Probe_idx = [1,20,40,85,140]; % indexes of probes to plot
Probe_idx = [1,8,140]; % indexes of probes to plot
pref = 1; % convential reference pressure for computation
SPL_freq = 2.5e3; % (Hz) target frequency
% --
CaseName = sprintf('DG%d Nk=%d  DoF=%d L = %1.2em, c0=%1.2e m/s\n',DGCell.N,mesh.N,size(M,1),L_num,cst.c0);
CaseName = sprintf('%s Mesh "%s" Nodes=%d\n',CaseName,meshfile,mesh.N);
CaseName = sprintf('%sdt=%1.2e s =%1.2e*(L/c0) CFL=%1.2e\n',CaseName,dt,dt/(L_num/cst.c0),CFL);
CaseName = sprintf('%s%s | %s\n',CaseName,cst.Impedance.Name,FlowName);
SourceName = @(w)sprintf('Source@%1.2eHz=%1.2e rad/s=%1.2e*f_c',w_source(Source_idx)/(2*pi),w_source(Source_idx),w_source(Source_idx)/w_cutoff);
tfName = @(tf)sprintf('tf=%1.2e s=%1.2e*L/c0=%1.2e*t_{norefl}\n',tf,tf/(L_num/cst.c0),tf/tfNoRefl_num);
nodes = DGMesh.getNodesCoordinates();
    % -- Probe pressure measurements
    % Plot time series and spectrum - Unfiltered
figure(4)
clf
p_DG_p = p_DG{Source_idx};
t_DG_p = t_DG{Source_idx};
for i=1:length(Probe_idx) % for the selected probes
    mask = logical(t_DG_p>=nodes(idxProbeNodes(Probe_idx(i)),1)/cst.c0);
    p_DG_probe = p_DG_p(Probe_idx(i),mask);
    t_DG_probe = t_DG_p(mask)-nodes(idxProbeNodes(Probe_idx(i)),1)/cst.c0;
    t_DG_probe = t_DG_probe/T_source;
        % Plot time series
    subplot(2,1,1)
    hold all
    plot(t_DG_probe,p_DG_probe,'DisplayName',sprintf('(x,y)=(%5.2e,%5.2e)',nodes(idxProbeNodes(Probe_idx(i)),1),nodes(idxProbeNodes(Probe_idx(i)),2)));
        % Plot spectrum
    Fs = 1/dt; % sampling frequency
    subplot(2,1,2)
    hold all
    Y = fft(p_DG_probe);
    L = length(Y); P2 = abs(Y/L); P1 = P2(1:L/2+1); P1(2:end-1) = 2*P1(2:end-1);
    plot(Fs*(0:(L/2))/L,P1,'DisplayName',sprintf('(x,y)=(%5.2e,%5.2e)',nodes(idxProbeNodes(Probe_idx(i)),1),nodes(idxProbeNodes(Probe_idx(i)),2)));    
end
subplot(2,1,1)
legend('show');
%xlabel('shifted time t - x/c0 (s)');
xlabel('shifted time t - x/c0 (T_{source})');
ylabel('p/z0');
title(sprintf('%s%s\n%s',CaseName,SourceName(w_source(Source_idx)),tfName(tf_DG{Source_idx})));
subplot(2,1,2)
xlim([2.5e3,3.5e3])
ylim([0,1])
legend('show');
xlabel('frequency (Hz)');
ylabel('|p/z0|');
title('Single-sided amplitude spectrum');
    % Plot time series and spectrum - Filtered
figure(5)
clf
        % filter definition
f_sample = 1/dt; % sample rate (Hz)
d = designfilt('bandpassiir',...
    'FilterOrder',8, ...
    'HalfPowerFrequency1',0.90*SPL_freq,...
    'HalfPowerFrequency2',1.10*SPL_freq, ...
    'SampleRate',f_sample);
p_DG_p = p_DG{Source_idx};
t_DG_p = t_DG{Source_idx};
for i=1:length(Probe_idx) % for the selected probes
    mask = logical(t_DG_p>=nodes(idxProbeNodes(Probe_idx(i)),1)/cst.c0);
    p_DG_probe = p_DG_p(Probe_idx(i),mask);
    t_DG_probe = t_DG_p(mask)-nodes(idxProbeNodes(Probe_idx(i)),1)/cst.c0;
    t_DG_probe = t_DG_probe/T_source;
    p_DG_probe_filtered = filter(d,p_DG_probe);
        % Plot time series
    subplot(2,1,1)
    hold all
        % Plot time series with bandpass filter
    plot(t_DG_probe,p_DG_probe_filtered,'DisplayName',sprintf('(x,y)=(%5.2e,%5.2e) filtered',nodes(idxProbeNodes(Probe_idx(i)),1),nodes(idxProbeNodes(Probe_idx(i)),2)));
        % Plot spectrum
    Fs = 1/dt; % sampling frequency
    subplot(2,1,2)
    hold all
    Y = fft(p_DG_probe_filtered);
    L = length(Y); P2 = abs(Y/L); P1 = P2(1:L/2+1); P1(2:end-1) = 2*P1(2:end-1);
    plot(Fs*(0:(L/2))/L,P1,'DisplayName',sprintf('(x,y)=(%5.2e,%5.2e) filtered',nodes(idxProbeNodes(Probe_idx(i)),1),nodes(idxProbeNodes(Probe_idx(i)),2)));    
end
subplot(2,1,1)
legend('show');
%xlabel('shifted time t - x/c0 (s)');
xlabel('shifted time t - x/c0 (T_{source})');
ylabel('p/z0');
title(sprintf('%s%s\n%s',CaseName,SourceName(w_source(Source_idx)),tfName(tf_DG{Source_idx})));
subplot(2,1,2)
xlim([2.5e3,3.5e3])
ylim([0,1])
legend('show');
xlabel('frequency (Hz)');
ylabel('|p/z0|');
title('Single-sided amplitude spectrum');
    % -- RMS value at probes
% Compute RMS values carefully
% RMS should be constant for lossless duct on [0,Lduct_num]
% Main parameters:
%   Number of reflections (best case: longer transient).
%   Integration interval for RMS. (The higher tmin and tmin-tmax, the
%   better.)
%   -> Influence of these parameters should be studied on any result.
%   -> The higher the base flow Mach number, the higher the influence of
%   reflections. (Poiseuille flow: reflections are negligible for Mc<=0.5
%   1e-2 difference on dB).
% Inputs: p_DG, idxProbeNodes, c0, tf, T_source, 
% Outputs: %d
% To compute RMS value, two strategies:
%   -> Remove the transient (first n periods) and compute RMS value
%   -> Filter with bandpass and compute RMS value
figure(6)
clf
hold all
p_DG_RMS = cell(size(p_DG));
dt_DG_RMS = cell(size(p_DG)); % min. integration time
for k=1:length(p_DG) % for each frequency
    p_DG_RMS{k} = size(size(p_DG{k},1),1);
    dt_DG_RMS{k} = tf - nodes(idxProbeNodes(i),1)/cst.c0;
    for i=1:size(p_DG{k},1) % for each probe
            % Time interval to compute RMS
        tmin = 10*T_source(k) + nodes(idxProbeNodes(i),1)/cst.c0; % remove transient
        tmax = tmin+tf_DG{k}; % until end of simulation
        %tmax = min(tfNoRefl_num,tf_DG{k}); % without reflection
        mask = logical((t_DG{k}>=(tmin)).*(t_DG{k}<=tmax));
            % Raw RMS value
        %p_DG_RMS{k}(i) = rms(p_DG{k}(i,mask));
            % Filtered RMS value
        p_DG_filter = filter(d,p_DG{k}(i,:));
        p_DG_RMS{k}(i) = rms(p_DG_filter(mask));        
        dt_DG_RMS{k} = min(dt_DG_RMS{k},max(t_DG{k}(mask))-min(t_DG{k}(mask)));
    end
end
leg=cell(0);
x_DG_probe = 1e3*nodes(idxProbeNodes,1);
for k=1:length(p_DG_RMS) % for each frequency
    leg{end+1}=sprintf('f=%1.2e rad/s=%1.2e Hz=%1.2e*w_c tf=%1.2e*t_{norefl} Dt=%1.2e*T_{source}',w_source(k),w_source(k)/(2*pi),w_source(k)/w_cutoff,max(t_DG{k})/tfNoRefl_num,dt_DG_RMS{k}/T_source(k));
    plot(x_DG_probe,SPL_0+20*log10(p_DG_RMS{k}/p_DG_RMS{k}(1)),'-');
end
Exp_idx_i = 1; % Index for average Mach number
Exp_idx_j = 2; % Index for source SPL
Exp_idx_freq = 16; % Index for source frequency
if exist('ExpData','var')
    for i=1:length(Exp_idx_i)
       for j=1:length(Exp_idx_j)
            plot(ExpData{Exp_idx_i(i),Exp_idx_j(j)}.Microxpos,ExpData{Exp_idx_i(i),Exp_idx_j(j)}.MicroSPL(:,Exp_idx_freq),'x');
            leg{end+1}=sprintf('%s Freq = %1.2e kHz',ExpData{Exp_idx_i(i),Exp_idx_j(j)}.name,ExpData{Exp_idx_i(i),Exp_idx_j(j)}.freq(Exp_idx_freq));
       end
    end
end
legend(leg,'Location','northoutside');
xlabel('distance x (mm)');
ylabel('SPL (dB)');
title(sprintf('%s%s\n%s',CaseName,SourceName(w_source(Source_idx)),tfName(tf_DG{Source_idx})));
%ylim([120,135]*1e0);
ylim([60,140]*1e0);
    % Liner position
plot(1e3*xliner_num(1)*[1,1],ylim,'k--');
plot(1e3*xliner_num(2)*[1,1],ylim,'k--');
%% Export SPL curve to file
SPL_DG = SPL_0+20*log10(p_DG_RMS{k}/p_DG_RMS{k}(1));
namestr = 'GIT_SPLwall_DG_Imp_M335_1kHz.csv';
dlmwrite(namestr,'x,SPL','Delimiter','');
dlmwrite(namestr,[x_DG_probe(:)/max(x_DG_probe(:)),SPL_DG(:)],'-append','Delimiter',',','newline','unix','precision','%1.6e');
%% Plot for convergence study
p_DG_RMS_CONVSTUDY(:,4)=SPL_0+20*log10(p_DG_RMS{k}/p_DG_RMS{k}(1));
x_DG_probe_CONVSTUDY(:,4)=x_DG_probe;
%% --
figure
clf
hold all
leg=cell(0);
plot(x_DG_probe_CONVSTUDY(:,1),p_DG_RMS_CONVSTUDY(:,1),'-');
leg{end+1}=sprintf('Multi-sine, 2kHz');
plot(x_DG_probe_CONVSTUDY(:,2),p_DG_RMS_CONVSTUDY(:,2),'-');
leg{end+1}=sprintf('Multi-sine, 2.5kHz');
plot(x_DG_probe_CONVSTUDY(:,3),p_DG_RMS_CONVSTUDY(:,3),'--');
leg{end+1}=sprintf('Mono-sine, 2kHz');
plot(x_DG_probe_CONVSTUDY(:,4),p_DG_RMS_CONVSTUDY(:,4),'--');
leg{end+1}=sprintf('Mono-sine, 2.5kHz');
% Exp_idx_i = 1; % Index for average Mach number
% Exp_idx_j = 2; % Index for source SPL
% Exp_idx_freq = 11; % Index for source frequency
% if exist('ExpData','var')
%     for i=1:length(Exp_idx_i)
%        for j=1:length(Exp_idx_j)
%             plot(ExpData{Exp_idx_i(i),Exp_idx_j(j)}.Microxpos,ExpData{Exp_idx_i(i),Exp_idx_j(j)}.MicroSPL(:,Exp_idx_freq),'x');
%             leg{end+1}=sprintf('%s Freq = %1.2e kHz',ExpData{Exp_idx_i(i),Exp_idx_j(j)}.name,ExpData{Exp_idx_i(i),Exp_idx_j(j)}.freq(Exp_idx_freq));
%        end
%     end
% end
legend(leg);
legend(leg,'Location','northoutside');
xlabel('distance x (mm)');
ylabel('SPL - SPL @ Delta 0.3 triangles (dB)');
%ylabel('SPL (dB)');
ylim([80,134])
plot(1e3*xliner_num(1)*[1,1],ylim,'k--');
plot(1e3*xliner_num(2)*[1,1],ylim,'k--');
%% SS
probe = 1;

