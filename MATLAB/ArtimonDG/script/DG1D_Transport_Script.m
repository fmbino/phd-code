%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 1D
%-------------------------------------------------------------------------
% Transport PDE
% dq/dt(t,x) + c*dq/dx(t,x) = 0    t>0 x in [0,L]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% PDE constants
c = 1;
L = 1;
%% Build DG formulation
Np = 4; % number of nodes per cell
Nk = 1e2; % number of elements in [0,L]
[K,F] = DG1D_Transport(c,[0,L],Np,Nk);
cell_size = L/Nk;
%% Time integration
% --
q_source = @(t)(sin(t));
% -- Initial condition
tf = 5;
CFL = 1e-2;
dt = CFL*min(cell_size)/norm(c); % Time step
q0 = zeros(size(K,1),1);
[x,t] = LSERK4_op(@(x,t)(K*x+F*q_source(t)),q0,tf,dt);
%% Build vector of global coordinates
xg = zeros(Nk*Np,1);
mesh = Mesh_1D_Uniform([0,L], Nk);
DGCell = DGCell_1D(Np);
for k=1:mesh.N % loop over each element
    i_range=(k-1)*Np+(1:Np);
    xg(i_range)=mesh.x(k)+cell_size*(DGCell.xi+1)/2;
end
clear i_range
%% Plot
tp = 0.50*tf;
[~,i_tp] = min(abs(t-tp));
tp = t(i_tp);
plot(xg,x(:,i_tp));
xlabel('x');
ylabel('x(t)');
xlim([0,1])
ylim([-1,1])
%% Time-domain solving: comparison to harmonic solution (video)
duration = 2; % (s)
fps = 15;
% --
figure
hold all
nframe = min(ceil(duration*fps),length(t)); % number of frame to display
idx_int = ceil(linspace(1,length(t),nframe)); % idx to display
clear Vi
Vi(length(idx_int)) = struct('cdata',[],'colormap',[]);
for i=1:length(idx_int)
    clf
    hold all
    leg = cell(0);
        % Display iteration n°i+1
    plot(xg,x(:,idx_int(i)));
    leg(end+1)={sprintf('Temporal')};
    xlabel('x');    
    ylabel(sprintf('q_%d',1));
    legend(leg);
    title(sprintf('%s - DG%d\n(Nx=%d,Nodes=%d,CFL=%1.1g)\nit:%d/%d\nt=%3.2g/%3.2g s','Transport',DGCell.N,mesh.N,DGCell.N*mesh.N,CFL,idx_int(i),length(t),t(idx_int(i)),t(end)));
    axis([mesh.bound(1), mesh.bound(2),-1,1])
    legend(leg);
    drawnow % force plot to show
    Vi(i) = getframe(gcf);
end
fprintf('Now generating video file...');
movie(Vi);
%movie2avi(Vi,filename,'fps',fps);
fprintf('Done\n');
%clear Vi

%% Eigenvalue scaling: model
Lmax = @(Np,Nk,c,C)(C*(Np-1).^2*Nk*c); % model for |lambda_max|
%% Eigenvalue scaling: Nk and Np dependency
Nk = 1:50;
Np = 2:10;
[NK,NP] = meshgrid(Nk,Np);
lambda=[];
for i=1:size(NK,1)
    for j=1:size(NK,2)
        [K,F] = DG1D_Transport(c,[0,L],NP(i,j),NK(i,j));
        lambda(i,j) = max(abs(eig(full(K))));
    end
end
%% Plot
clf
surface(NK,NP,lambda);
%set(gca,'DataAspectRatio',[1,1,1])
xlabel('Nk');
ylabel('Np');