%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - II Transport 2D
%-------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% PDE definition (without boundary conditions)
Nq = 1;
c = [1;0]; % [cx;cy] propagation speed (m/s)
Ax = diag(c(1)*ones(1,Nq));
Ay = diag(c(2)*ones(1,Nq));
B = zeros(Nq,Nq);
%% Numerical flux: FVS
finw = @(n)(diag(min(0,dot(c,n))*ones(1,Nq)));
fout = @(n)(diag(max(0,dot(c,n))*ones(1,Nq)));
arg_Upwind_FVS = NumericalFlux_2D_UpwindFVS(@(n)(fout(n)));
%% Build mass and stiffness: case definition
% 'zone' is a cell of structure that describes each zone
zone = cell(0);
zone{end+1} = struct('name','Interior_UpwindFVS','physicalEntity',[5],'arg',arg_Upwind_FVS);
%zone{end+1} = struct('name','Outlet_UpwindFVS','physicalEntity',[4 2],'arg',arg_Upwind_FVS);
%zone{end+1} = struct('name','Inlet_UpwindFVS','physicalEntity',[4],'arg',arg_Upwind_FVS);
zone{end+1} = struct('name','Periodic_UpwindFVS','physicalEntity',[4,2],'arg',arg_Upwind_FVS);
%% Build mass and stiffness: global formulation
[M,K,C,F_source] = DG2D_buildGlobalFormulation(Ax,Ay,B,DGMesh,zone);
%% Time-domain solving: initial condition and source
tf = 10; % Final time
    % -- Initial condition (Gaussian pulse) (Nx2) -> Nx1
q0_x0 = [0.5,0.5]; q0_xd = [0.1,0.1]; q0_A = 0.5;
f = @(x)(AnF_gaussianPulse(x,q0_x0,q0_xd,q0_A));
    % -- Initial condition (Sinus)
%f = @(x)(AnF_sinusDirichlet(x,mesh.bound));
q0 = @(x)(reshape(repmat(f(x)',[Nq,1]),Nq*size(x,1),1));
    % -- Intial condition (Null)
%q0 = @(x)(zeros(Nq*size(x,1),1));
    % -- Source
f = @(t)(exp(-(1-(1-t).^2).^(-1)));
%f = @(t)(1e-1*AnF_bumpFunction(t,0,tf/2));
f = @(t)(1e-1*AnF_smoothHeaviside(t,0,tf/8));
omega = 5e1;
q_source = @(t)(f(t).*sin(omega*t)*ones(Nq,1));
%% Time-domain solving
CFL = 0.1; % CFL number
dt = CFL*min(mesh.charLength)/norm(c); % Time step
%in = 10; % node to store
nodes = DGMesh.getNodesCoordinates();
[x,t] = DG2D_SolveTimeDomain(M,K,F_source,DGMesh,Nq,tf,dt,'x0',q0(nodes),'xs',q_source,'storeOnlyLast',0);
%% Create display mesh
[trianglesPlot, nodesPlot] = DGMesh.buildPlotMesh();
%% Plot solution
it = 1e4; % iteration to plot
var = 1; % variable to plot
% --
%figure(2)
Np = DGCell.Np; Nk = mesh.N;
x_plot = DGMesh.interpolateField2PlotNodes(Nq,x(:,min(it,size(x,2))));
%x_plot = q0(nodesPlot);
x_plot = x_plot(var + Nq*(0:(Np*Nk-1)));
clf
trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),x_plot,'EdgeColor','None');
xlabel('x');
ylabel('y');
%set(gca, 'DataAspectRatio', [1 1 1])
%axis([-1,1,-1,0,-2,3])
title(sprintf('Solution: q_%d (t=%1.3g)',var,t(min(it,size(t,2)))));
view(45,45);
%% Plot solution (video)
var = 1; % variable to plot
duration = 3; % (s)
fps = 15;
filename = 'Transport_Source.avi';
% --
var = min(var,Nq);
figure
hold all
nframe = min(ceil(duration*fps),length(t)); % number of frame to display
idx_int = ceil(linspace(1,length(t),nframe)); % idx to display
clear Vi
Vi(length(idx_int)) = struct('cdata',[],'colormap',[]);
for i=1:length(idx_int)
    clf
    hold all
    leg = cell(0);
        % Display iteration n°i+1
    x_plot = DGMesh.interpolateField2PlotNodes(Nq,x(:,idx_int(i)));
    x_plot = x_plot(var + Nq*(0:(DGCell.Np*mesh.N-1)));
    trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),x_plot,'EdgeColor','None');
    xlabel('x');
    ylabel('y');
    set(gca, 'DataAspectRatio', [1 1 1])
    axis([0,1,0,1,-0.1,1])
    view(45,45);
    title(sprintf('%s - DG%d - q%d\n(Nk=%d,Nodes=%d,CFL=%1.1g)\nit:%d/%d\nt=%3.2g/%3.2g s','Transport',DGCell.N,var,mesh.N,DGCell.Np*mesh.N,0,idx_int(i),length(t),t(idx_int(i)),t(end)));
    drawnow % force plot to show
    Vi(i) = getframe(gcf);
end
fprintf('Now generating video file...');
%movie(Vi);
% movie2avi(Vi,filename,'fps',fps);
% system(sprintf('ffmpeg -i %s %s.mp4 && rm %s',filename,filename,filename));
fprintf('Done\n');
clear Vi