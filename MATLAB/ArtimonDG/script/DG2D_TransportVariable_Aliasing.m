%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - II Transport 2D (Variable coefficient)
% Case1: Hesthaven, Example 5.3
% The purpose of this example is to highlight the aliasing effect:
% transport speed c(x) has a stiff variations over [-1,1].
%-------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Read mesh (gmesh)
m = load_gmsh2('Rectangle-Aliasing1D.msh',[1 2]); % read lines (1) and triangles (2)
nodes = m.POS(:,1:2);
triangles = m.TRIANGLES(:,[4,1,2,3]);
boundFaces = m.LINES(:,[3,1,2]);
physicalEntitiesIdx = m.physicalEntitiesIdx;
physicalEntitiesName = m.physicalEntitiesName;
mesh = Mesh_2D_Triangle(nodes,boundFaces,triangles,physicalEntitiesIdx,physicalEntitiesName);
%mesh.show();
%mesh.showTriangle(1);
mesh.printSummary();
clear m nodes triangles bound faces physicalEntitiesIdx physicalEntitiesName
    % -- Initialize DG cell
N = 4; % order
DGCell = DGCell_2D_Triangle(N);
    % --  Initialize DG Mesh
DGMesh = DGMesh_2D_Triangle(mesh,DGCell);
%% PDE definition (without boundary conditions)
Nq = 1;
    % Second case
cx = @(x)(((1-x.^2).^5+1));
B = @(x,y)(0);
u0 = @(x)(sin(4*pi*x)); % Initial condition
c = @(x,y)([cx(x);0]); % [cx;cy] propagation speed (m/s)
Ax = @(x,y)(cx(x));
Ay = @(x,y)(0);
%% Numerical flux: FVS
fout = @(n,x,y)(diag(max(0,dot(c(x,y),n))*ones(1,Nq)));
arg_Upwind_FVS = NumericalFlux_2D_UpwindFVS(@(n,x,y)(fout(n,x,y)));
arg_Upwind_FVS_Periodic = NumericalFlux_2D_UpwindFVS(@(n)(fout(n,0,0)));
zone = cell(0);
zone{end+1} = struct('name','Interior_UpwindFVS','physicalEntity',[5],'arg',arg_Upwind_FVS);
zone{end+1} = struct('name','Periodic_UpwindFVS','physicalEntity',[4,2],'arg',arg_Upwind_FVS_Periodic);
%% Build global formulation
[M,K,C,F_source] = DG2D_buildGlobalFormulation(Ax,Ay,B,DGMesh,zone);
%% Time-domain solving: initial condition and source
tf = 10.5; % Final time
    % -- Initial condition (Gaussian pulse) (Nx2) -> Nx1
f = @(x)(u0(x));
q0 = @(x)(reshape(repmat(f(x(:,1))',[Nq,1]),Nq*size(x,1),1));
    % -- Source (from exact solution)
f = @(t)(u(0,t));
q_source = @(t)(f(t)*ones(Nq,1));
%% Time-domain solving
CFL = 0.1; % CFL number
x = linspace(0,1,1e2);
dt = CFL*min(mesh.charLength)/max(cx(x)); % Time step
clear x
%in = 10; % node to store
nodes = DGMesh.getNodesCoordinates();
[x,t] = DG2D_SolveTimeDomain(M,K,F_source,DGMesh,Nq,tf,dt,'x0',q0(nodes),'xs',q_source,'storeOnlyLast',0);