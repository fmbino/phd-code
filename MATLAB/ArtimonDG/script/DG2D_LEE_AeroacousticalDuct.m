%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - LEE 2D Longitudinal - Lined duct
%-------------------------------------------------------------------------
% This script has been used to generate the curves of the (JCP2018). After
% loading the mesh and defining the base flow, several TDIBC are available.
% The postprocessing is tailored to produce the graph of interest for the
% papers. The verbosity in some regions is justified by the need of
% automating as much as possible the graph generation.
% This is the most advanced script utilising Artimon DG.
% Purpose:
%   - time-domain simulation of a lined duct (NASA GIT or GFIT)
%   - compute SPL along the wall
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
run 'DG2D_LEELongitudinal_ZoneDefinition.m'
run 'BaseFlowModel.m'
%% Load mesh
    % NASA GIT
%meshfile='NASA-GIT-2005_veryshort_52triangles.msh';
meshfile = 'NASA-GIT-2005_veryshort_106triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_240triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_360triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_552triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_814triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_1364triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_1364triangles.msh';
%meshfile = 'NASA-GIT-2005_veryshort_2992triangles_NoBoundaryLayer.msh';
%meshfile = 'NASA-GIT-2005_verylong_coarse.msh';
    % NASA GFIT
% meshfile='NASA-GFIT-2013_veryshort_coarse_withBoundaryLayer_110triangles.msh';
% meshfile='NASA-GFIT-2013_veryshort_coarse_withBoundaryLayer_326triangles.msh';
%meshfile='NASA-GFIT-2013_veryshort_coarse_withBoundaryLayer_386triangles.msh';
%meshfile='NASA-GFIT-2013_veryshort_coarse_withBoundaryLayer_552triangles.msh';
%meshfile='NASA-GFIT-2013_veryshort_coarse_withBoundaryLayer_1600triangles.msh';
N = 3; % polynomial order
m = load_gmsh2(meshfile,[1 2]); % read lines (1) and triangles (2)
nodes = m.POS(:,1:2); triangles = m.TRIANGLES(:,[4,1,2,3]);
boundFaces = m.LINES(:,[3,1,2]); physicalEntitiesIdx = m.physicalEntitiesIdx;
physicalEntitiesName = m.physicalEntitiesName;
mesh = Mesh_2D_Triangle(nodes,boundFaces,triangles,physicalEntitiesIdx,physicalEntitiesName);
clear m nodes triangles boundFaces physicalEntitiesIdx physicalEntitiesName
mesh.printSummary();
    % Initialize DG cell & mesh
DGCell = DGCell_2D_Triangle(N);
DGMesh = DGMesh_2D_Triangle(mesh,DGCell);
%mesh.show();
%DGMesh.showDGNodes();
%DGCell.show();
    % Impedance boundary condition on the upper wall
IBC_flux = 'Impedance_FluxBeta'; % Impedance numerical flux function
zoneDef = struct('Interior',[5],'Source',[4],'Outlet',[2],'Wall',1,IBC_flux,3);
%% Experimental duct: Ozyoruk
    % -- Experimental parameters
c0_exp = 340.29; % experimental sound speed (m/s)
xliner_exp = [0.20955,0.5969]; % experimental liner zone (m)
Lduct_exp = 0.8382; % duct experimental length (m)
H_exp = 0.3*Lduct_exp; % duct experimental height (m)
x_probe_exp = xliner_exp; 
%% Experimental duct: NASA GIT (2005)
    % Load full experimental data
ExpData = ExpDataLoadNASAGIT2005CT57();
    % Air @295 K (used for impedance identification in [Jones2005])
nu = 1.568e-5;
gamma = 1.4;
c0_exp = sqrt(gamma*287.058*295); % experimental sound speed (m/s)
Pr = 0.707;
    % -- Experimental parameters
xliner_exp = [0.203,0.609]; % experimental liner zone (m)
Lduct_exp = 0.8128; % duct experimental length (m)
H_exp = 0.051; % duct experimental height (m)
    % Microphone location (m)
x_probe_exp = 1e-3*[0.1,25.5,101.7,203.3,228.7,254.1,279.4,304.8,317.6,330.3,343.0,355.7,368.4,381.1,393.8,406.5,419.1,431.9,444.6,457.3,470.0,482.7,495.4,508.1,533.5,558.9,584.3,609.7,711.4,787.5,812.9];
y_probe_exp = 0;
%% Experimental duct: NASA GFIT (2013)
    % Load full experimental data
ExpData = ExpDataLoadNASAGFIT2013MP();
    % Air @295 K (used for impedance identification in [Jones2005])
nu = 1.568e-5;
gamma = 1.4;
c0_exp = sqrt(gamma*287.058*295); % experimental sound speed (m/s)
Pr = 0.707;
    % -- Experimental parameters
xliner_exp = [203.2,812.8]/1e3; % experimental liner zone (m)
Lduct_exp = 1016/1e3; % duct experimental length (m)
H_exp = 63.5/1e3; % duct experimental height (m)
    % Microphone location (m)
x_probe_exp = [0,0.0254,0.0508,0.0508,0.1016,0.127,0.1524,0.1651,0.1778,0.1905,0.2032,0.2159,0.2286,0.2413,0.254,0.2794,0.3048,0.3302,0.3556,0.381,0.4064,0.4318,0.4445,0.4572,0.4699,0.4826,0.4953,0.508,0.5207,0.5334,0.5461,0.5588,0.5715,0.5842,0.6096,0.635,0.6604,0.6858,0.7112,0.7366,0.762,0.7747,0.7874,0.8001,0.8128,0.8255,0.8382,0.8509,0.8636,0.889,0.9144,0.9652,0.9652,0.9906,1.016];
y_probe_exp = 0;
%% Numerical probe placement
% Denser bunching of probes
x_probe = linspace(x_probe_exp(1),x_probe_exp(end),150);
y_probe = 0*x_probe; % probe location
probe = [x_probe(:),y_probe(:)];
%% Duct scaling and diagnostic
% Experimental duct is on [0,Lduct_DG].
% Speed of sound is scaled to keep both numerical and experimental 
% frequencies identical.
    % -- Numerical parameters
Lduct_num = Lduct_exp; % numerical duct length (choice)
c0_num = c0_exp * Lduct_num/Lduct_exp; % numerical sound speed
L_num = mesh.bound(2,1)-mesh.bound(1,1); % numerical length
if L_num<Lduct_num
   error('Numerical length is smaller than chosen numerical duct length.'); 
end
H_num = mesh.bound(2,2)-mesh.bound(1,2); % numerical height
if abs(H_num - H_exp * (Lduct_num/Lduct_exp))<eps
    warning('Numerical aspect ratio does not match experimental one.');
end
xliner_num = xliner_exp * (Lduct_num/Lduct_exp); % numerical liner zone
tfNoRefl_num = (2*L_num-Lduct_num)/c0_num; % numerical simulation time w/o reflection
        % Min. Freq w/o reflection
        % (Lduct_num/c0_num+3*T_source <= tfNoRefl_num
w_source_NoRefl = 2*pi*(3)*(tfNoRefl_num-Lduct_num/c0_num)^(-1);
        % Duct no flow cut-off frequency (identical to experimental one)
w_cutoff = 2*pi*c0_num/(2*H_num);
w_cutoff_longi_num = pi*c0_num/(mesh.bound(2,1)-mesh.bound(1,1));
        % DG max frequency
        % 1D estimation based on 'doF_per_1Dcell'
DoF_per_1Dcell = N+1; % choice (typically ~# of node on edge)
w_to_PPW = @(w)2*pi*c0_num*DoF_per_1Dcell/(min(mesh.charLength(:))*w);
PPW_to_w = @(PPW)2*pi*c0_num*DoF_per_1Dcell/(min(mesh.charLength(:))*PPW);
        % Min. # of PPPW for 1D Wave Equation from [Hu,1999].
        % Function of number of nodes per cell.
dof_to_PPWmin = [0,15.7,10.5,7.9,6.8,6.2,6.2,5.3]; % value for N=7 made up
fprintf('-- Numerical simulation characteristics.\n');
fprintf('*Dimensions: numerical (experiment)\n');
fprintf('\tDuct length: %1.3e (%1.3e)\n',Lduct_num,Lduct_exp);
fprintf('\tTotal length: %1.3e (N/A)\n',L_num);
fprintf('\tHeight: %1.3e (%1.3e)\n',H_num,H_exp);
fprintf('\tAspect ratio: %1.3e (%1.3e)\n',Lduct_num/H_num,Lduct_exp/H_exp);
fprintf('\tSound speed: %1.3e (%1.3e)\n',c0_num,c0_exp);
fprintf('\tLiner position: [%1.3e,%1.3e] ([%1.3e,%1.3e])\n',xliner_num(1),xliner_num(2),xliner_exp(1),xliner_exp(2));
fprintf('*Frequencies/times of interest\n');
fprintf('No flow cut-off: %1.3e rad/s %1.3e Hz\n',w_cutoff,w_cutoff/(2*pi));
fprintf('Max. time w/o reflection: %1.3e s\n',tfNoRefl_num);
fprintf('Min. freq. w/o refl.: %1.3e rad/s %1.3e Hz\n',w_source_NoRefl,w_source_NoRefl/(2*pi));
fprintf('Mesh cut-off longitudinal: %1.3e rad/s %1.3e Hz \n',w_cutoff_longi_num,w_cutoff_longi_num/(2*pi));
fprintf('*DG frequency properties (assuming DoF=%d)\n',DoF_per_1Dcell);
fprintf('Cut-off: %d PPW \n',w_to_PPW(w_cutoff));
fprintf('Min. freq. w/o refl.: %d PPW\n',w_to_PPW(w_source_NoRefl));
fprintf('Min. # of PPW [Hu,1999]: %d\n',dof_to_PPWmin(DoF_per_1Dcell));
for PPW=3:10
    fprintf('\tMax. freq. @PPW_min=%d: %1.3e rad/s %1.3e Hz\n',PPW,PPW_to_w(PPW),PPW_to_w(PPW)/(2*pi));
end
fprintf('--\n');
%% Base flow definition
cst.c0 = c0_num;
Mavg = 0.4; % average Mach number
a = mesh.bound(1,2); b = mesh.bound(2,2);
y2r = @(y,a,b)2/(b-a)*(y-(b+a)/2); y2r_dy = @(a,b)2/(b-a);
r2y = @(r,a,b) (b+a)/2 + (b-a)/2*r; r2y_dr = @(a,b)(b-a)/2;
    % -- Uniform base flow
cst.u0 = @(x,y)Mavg;
cst.du0dy = @(x,y)0;
FlowName = sprintf('Uniform flow M_{avg}=%1.3g',Mavg);
    % -- Poiseuille base flow
cst.u0 = @(x,y)cst.c0*M0_Poi(y2r(y,a,b),Mavg); 
cst.du0dy = @(x,y)cst.c0*y2r_dy(a,b).*dM0_dr_Poi(y2r(y,a,b),Mavg);
FlowName = sprintf('Poiseuille M_{avg}=%1.3g',Mavg);
    % -- Hyperbolic velocity profile
delta = 0.2; % adimensional boundary layer thickness (in (0,1))
cst.u0 = @(x,y)cst.c0*M0_Hy(y2r(y,a,b),Mavg,delta);
cst.du0dy = @(x,y)cst.c0*y2r_dy(a,b).*dM0_dr_Hy(y2r(y,a,b),Mavg,delta);
FlowName = sprintf('Hyperbolic-M_{avg}=%1.3g-delta=%1.3g',Mavg,delta);
    % -- Average profile
% delta = 0.2;
% alpha = 0.5; % in (0,1)
% cst.u0 = @(x,y)cst.c0*M0_Av_Hy(y2r(y,a,b),Mavg,delta,alpha);
% cst.du0dy = @(x,y)cst.c0*y2r_dy(a,b).*dM0_dr_Av_Hy(y2r(y,a,b),Mavg,delta,alpha);
% FlowName = sprintf('Average M_{avg}=%1.3g delta=%1.2e alpha=%1.2e',Mavg,delta,alpha);
    % -- Curve fit
% cst.u0 = @(x,y)cst.c0*M0_fit(y);
% cst.du0dy = @(x,y)cst.c0*dM0_dy_fit(y);
% FlowName = sprintf('Fit M_{avg}=%1.3g',Mavg);
    % -- Plot flow in duct
y = linspace(mesh.bound(1,2),mesh.bound(2,2),1e2);
figure(9)
clf
subplot(2,1,1)
hold all
plot(cst.u0(1,y)/cst.c0,y,'DisplayName',sprintf('M_c=%1.3g',max(cst.u0(1,y)/cst.c0)));
plot(M0_Poi(y2r(y,a,b),Mavg),y,'DisplayName',sprintf('Poiseuille M_c=%1.3g',max(M0_Poi(y2r(y,a,b),Mavg))));
set(gca,'DataAspectRatio',[1,1,1]);
legend('show');
ylim([min(y),max(y)]);
xlim([0,1]);
ylabel('y (m)');
xlabel('M0(y)');
title(FlowName)
subplot(2,1,2)
hold all
plot(cst.du0dy(1,y)/cst.c0,y,'DisplayName',sprintf('Max=%1.3g/m',max(abs(cst.du0dy(1,y)/cst.c0))));
plot(y2r_dy(a,b).*dM0_dr_Poi(y2r(y,a,b),Mavg),y,'DisplayName',sprintf('Poiseuille Max=%1.3g/m',max(abs(y2r_dy(a,b).*dM0_dr_Poi(y2r(y,a,b),Mavg)))));
legend('show');
%set(gca,'DataAspectRatio',[1,1,1]);
ylim([min(y),max(y)]);
ylabel('y (m)');
xlabel('dM0(y)/dy');
title(FlowName)
%% [Impedance boundary condition] liner position
xliner = xliner_num; % lined zone
xliner_ind = @(x,y)((x>=xliner(1)).*(x<=xliner(2))); % indicator function
% dts = 0.05; % spread (Heaviside is dts=0)
% xliner_ind = @(x,y)(AnF_smoothHeaviside(x,xliner(1)-dts,dts).*(1-AnF_smoothHeaviside(x,xliner(2),dts)));
%%clf
%hold all
%plot(r,AnF_smoothHeaviside(r,xliner(1)-dts,dts).*(1-AnF_smoothHeaviside(r,xliner(2),dts)));
%plot(r,xliner_ind(r,1));
%% [Impedance boundary condition] rigid wall
% z(s)=inf, so beta(s)=1.
cst.Impedance = ImpedanceBoundaryCondition('Hard wall',beta_fun(1),0,{0},{0},{0},{0},{0},{0},{0});
cst.Impedance.printImpedanceSummary();
%% [Impedance boundary condition] prop-der-integral model
% z(s) =  a(1)/s + a(2) + a(3)*s
    % -- Impedance definition
a = [0,0.5,0];
    % -- Reflection coefficient
cst.Impedance=[];
if a(1)==0 && a(3)==0 % Proportional impedance
    cst.Impedance = ImpedanceBoundaryCondition(sprintf('Prop. z(s)=%1.1e',a(2)),@(x,y)beta_fun(beta_fun(a(2)))*xliner_ind(x,y),0,{0},{0},{0},{0},{0},{0},{0});
elseif a(1)==0 % Proportional derivative impedance
    % beta(s) = 1 + h(s), where h(s) = -2/(1+z(s)) has a diffusive
    % representation.
    h_an = @(s)(-2./(1+a(2)+a(3)*s));
    BndOp_op  = DiffusiveOperator(-2/a(3),(1+a(2))/a(3),'Laplace',h_an,'Type','Standard');    
    cst.Impedance = ImpedanceBoundaryCondition(sprintf('Prop. Der. z(s)=%1.1e+%1.1e*s',a(2),a(3)),@(x,y)beta_fun(1)*xliner_ind(x,y),0,{0},{0},{xliner_ind},{BndOp_op},{0},{0},{0});
elseif a(3)==0 % Proportional integral impedance
    % beta(s) = beta0 + h(s), where h(s) has a diffusive
    % representation.
    h_an = @(s)(2*a(1)/(1+a(2))*1./(a(1)+(1+a(2))*s));
    BndOp_op  = DiffusiveOperator(2*a(1)/(1+a(2))^2,a(1)/(1+a(2)),'Laplace',h_an,'Type','Standard');
    cst.Impedance = ImpedanceBoundaryCondition(sprintf('Prop. Int. z(s)=%1.1e+%1.1e/s',a(2),a(1)),@(x,y)beta_fun(beta_fun(a(2)))*xliner_ind(x,y),0,{0},{0},{xliner_ind},{BndOp_op},{0},{0},{0});
end
cst.Impedance.printImpedanceSummary();
%cst.Impedance.plot(linspace(0,1e5,1e2),'reflcoeff')
fprintf('Impedance @2.5kHz is\n');
beta2z(cst.Impedance.Laplace(1i*2*pi*3e3,0.3,0))
%% [Impedance boundary condition] Fractional impedance
mu_an=@(xi)(((h_an(xi*exp(-1i*pi))-h_an(xi*exp(1i*pi)))/(2*1i*pi)));
Nxi = 4;
[xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(mu_an,Nxi,'CoVParam',1.4);
BndOp_op  = DiffusiveOperator(mu,xi,'Laplace',h_an,'Type','Standard');
cst.Impedance = ImpedanceBoundaryCondition(sprintf('Nxi=%d',Nxi),beta_fun(1),0,{0},{0},{xliner_ind},{BndOp_op},{0},{0},{0});
cst.Impedance.printImpedanceSummary();
%% [Impedance boundary condition] Advanced TDIBC
    % TDIBC A
load('GFIT-MP-SPL120dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-6Oscillatory2DiffusivePolesFinal.mat','TDIBC');
    % TDIBC B
load('GFIT-MP-SPL120dB-Mavg271-2DFEM_TDIBC_ReflCoeff-ExpeOptim-6Oscillatory1DiffusivePoles.mat','TDIBC');
    % TDIBC C
load('GFIT-MP-SPL120dB-Mavg271-2DFEM_TDIBC_ReflCoeff-ExpeOptim-2OscillatoryPolesPhysivalValueRecovery.mat','TDIBC');
    % TDIBC D
load('GIT-CT57-SPL130dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4OscillatoryPolesFinal.mat','TDIBC');
    % TDIBC E
load('GIT-CT57-SPL130dB-Mavg400-2DFEM_TDIBC_ReflCoeff-ExpeOptim-2OscillatoryPolesFinal.mat','TDIBC');
    % TDIBC F
load('GIT-CT57-SPL130dB-Mavg400-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4Oscillatory2DiffusivePoles900HzFinal.mat','TDIBC');
TDIBC.printPolesAndWeights(6);
[~,~,~,xi,sn]=TDIBC.getPolesandWeights();
fprintf('Max pole: %1.3e kHz\n',max([xi(:);abs(sn(:))])/(2*pi*1e3));
    % NASA GIT
% load('GIT-CT57-SPL130dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-2OscillatoryPolesFinal','TDIBC');
% load('GIT-CT57-SPL130dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4OscillatoryPolesFinal','TDIBC');
% load('GIT-CT57-SPL130dB-Mavg400-2DFEM_TDIBC_ReflCoeff-ExpeOptim-2OscillatoryPolesFinal','TDIBC');
% load('GIT-CT57-SPL130dB-Mavg400-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4Oscillatory2DiffusivePoles600HzFinal','TDIBC');
% load('GIT-CT57-SPL130dB-Mavg400-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4Oscillatory2DiffusivePoles700HzFinal','TDIBC');
% load('GIT-CT57-SPL130dB-Mavg400-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4Oscillatory2DiffusivePoles800HzFinal','TDIBC');
% load('GIT-CT57-SPL130dB-Mavg400-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4Oscillatory2DiffusivePoles900HzFinal','TDIBC');
    % NASA GFIT
% load('GFIT-MP-SPL120dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-2OscillatoryPolesFinal','TDIBC');
% load('GFIT-MP-SPL120dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4Oscillatory2DiffusivePolesFinal','TDIBC');
% load('GFIT-MP-SPL120dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-6Oscillatory2DiffusivePolesFinal','TDIBC');
% load('GFIT-MP-SPL120dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-8OscillatorypolesFinal','TDIBC');
% load('GFIT-MP-SPL120dB-Mavg271-2DFEM_TDIBC_ReflCoeff-ExpeOptim-2OscillatoryPolesPhysivalValueRecovery','TDIBC');
% load('GFIT-MP-SPL120dB-Mavg271-2DFEM_TDIBC_ReflCoeff-ExpeOptim-6Oscillatory1DiffusivePoles','TDIBC');
    % check stability of realization
TDIBC.checkRealizationStability();
    % check passivity over given frequency range
TDIBC.checkPassivity(linspace(1,1e4,1e2),'reflcoeff',1,1);
    % Account for liner position
cst.Impedance = TDIBC;
cst.Impedance = cst.Impedance.multiplyCoefficient(xliner_ind);
%cst.Impedance.printImpedanceSummary();
clf
TDIBC.plotPolesAndWeights(0.8e3)
%% [Time integration] Choose time step
CFL = 0.53; % stability limit for N = 4 (order 5)
CFL = 0.85; % stability limit for N = 3 (order 4)
    % Time delay
Delay_Np = 8; % # of nodes per element
Delay_Nk = 1; % # of elements
PPW_delay = @(fmax,delay)Delay_Np*Delay_Nk/(delay*fmax);
    % Case Name
CaseName = sprintf('DG%d Nk=%d L = %1.2em, c0=%1.2e m/s\n',DGCell.N,mesh.N,L_num,cst.c0);
CaseName = sprintf('%s Mesh "%s"\n',CaseName,meshfile);
CaseName = sprintf('%sCFL=%1.2e\n',CaseName,CFL);
CaseName = sprintf('%s%s | %s\n',CaseName,cst.Impedance.Name,FlowName);
    % -- Diagnostic message for time-domain simulation
        % Check characteristic time of boundary operators
dt = CFL*min(DGMesh.mesh.charLength)/norm(cst.c0); % Time step
fprintf('With RK 8-4 (C=7.7), conservative limit: |xi|<=%1.3g rad/s = %1.3g Hz\n',0.5*7.7/dt,0.5*7.7/dt/(2*pi));
for i=1:length(cst.Impedance.BndOp)
    fprintf('BndOp{%d} max. pulsation: %1.3g\n',i,cst.Impedance.BndOp{i}.Operator.computeMaxPulsation());
end
for i=1:length(cst.Impedance.BndOpDelay)
    fprintf('BndOpDelay{%d} max. pulsation: %1.3g\n',i,cst.Impedance.BndOpDelay{i}.Operator.computeMaxPulsation());
    fprintf('BndOpDelay{%d} CFL: %1.3g.\n',i,Delay_Nk*dt/cst.Impedance.BndOpDelay{i}.Delay);
    fprintf('BndOpDelay{%d} PPW@3kHz: %1.3g.\n',i,PPW_delay(3e3,cst.Impedance.BndOpDelay{i}.Delay));
end
for i=1:length(cst.Impedance.DirectDelay)
    fprintf('DirectDelay{%d} CFL: %1.3g.\n',i,Delay_Nk*dt/cst.Impedance.DirectDelay{i}.Delay);
end
%% [Time integration] Source definition
    % Choose source angular frequency (rad/s)
w_source = 2*pi*[0.6,1,1.5,2,2.5,3]*1e3; % GIT
%w_source = 2*pi*[0.6,1,1.4,1.6,2.2,2.6]*1e3; % GFIT
T_source = (2*pi)./w_source; % source period (s)
fprintf('Period w/o reflection: %d\n',floor(tfNoRefl_num/max(T_source)));
%% [Time integration] Frequency-by-frequency
p_DG = cell(length(w_source),1);
t_DG = cell(length(w_source),1);
tf_DG = cell(length(w_source),1);
dt_DG = cell(length(w_source),1);
for i=1:length(w_source) % one simulation per source
    ps = @(t)sin(w_source(i)*t);
        % -- Smoother at t=0 (not necessary)
%     ps_smooth = @(t)(AnF_smoothHeaviside(t,0,T_source));
%     ps = @(t)ps(t).*ps_smooth(t);
    tf = max(tfNoRefl_num,60*T_source(i)); % Final time
    [p,t,probeDG] = DG2D_SolveTimeDomain_Wrapper(DGMesh,zoneDef,zone,c0_num,cst.u0,cst.du0dy,cst.Impedance,CFL,tf,ps,'probe',probe,'Delay_Np',Delay_Np,'Delay_Nk',Delay_Nk);
    p_DG{i} = p; t_DG{i} = t; tf_DG{i} = tf; dt_DG{i} = max(diff(t));
        % store full field
    %[p,t,probeDG] = DG2D_SolveTimeDomain_Wrapper(DGMesh,zoneDef,zone,c0_num,cst.u0,cst.du0dy,cst.Impedance,CFL,tf,ps,'Delay_Np',Delay_Np,'Delay_Nk',Delay_Nk);
end
%% [Time integration] One fell swoop
p_DG = cell(1);
t_DG = cell(length(w_source),1);
tf_DG = cell(length(w_source),1);
dt_DG = cell(length(w_source),1);
    % Source definition (multiple frequencies)
ps = @(t)0;
for i=1:length(w_source)
    ps = @(t) ps(t)+sin(w_source(i)*t);
end
tf = max(tfNoRefl_num,80*max(T_source)); % Final time
[p,t,probeDG] = DG2D_SolveTimeDomain_Wrapper(DGMesh,zoneDef,zone,c0_num,cst.u0,cst.du0dy,cst.Impedance,CFL,tf,ps,'probe',probe,'Delay_Np',Delay_Np,'Delay_Nk',Delay_Nk);
p_DG{1} = p;
    % store full field
% tf = tfNoRefl_num; % Final time
% [x,t,probeDG] = DG2D_SolveTimeDomain_Wrapper(DGMesh,zoneDef,zone,c0_num,cst.u0,cst.du0dy,cst.Impedance,CFL,tf,ps,'Delay_Np',Delay_Np,'Delay_Nk',Delay_Nk);
for k=1:length(w_source)
    t_DG{k} = t; tf_DG{k} = tf; dt_DG{k} = max(diff(t));
end
%% [Post processing] Filter pressure probe
% Compute 'p_DG_filtered' from 'p_DG'
Filter_order = 6; % the higher the order, the higher the response time
% --
p_DG_filtered = cell(length(w_source),1);
if numel(p_DG)==numel(w_source) % frequency by frequency
    for k=1:length(w_source) % for each frequency
        dt = dt_DG{k};
                % filter definition
        d = designfilt('bandpassiir','FilterOrder',Filter_order,...
                        'HalfPowerFrequency1',0.96*w_source(k)/(2*pi),...
                        'HalfPowerFrequency2',1.04*w_source(k)/(2*pi),...
                        'SampleRate',1/dt);
        for i=1:size(p_DG{k},1) % for each probe
            p_DG_filtered{k}(i,:) = filter(d,p_DG{k}(i,:));
        end
    end
else % multiple frequency
    for k=1:length(w_source) % for each frequency
        dt = dt_DG{k};
                % filter definition
        d = designfilt('bandpassiir','FilterOrder',Filter_order,...
                        'HalfPowerFrequency1',0.96*w_source(k)/(2*pi),...
                        'HalfPowerFrequency2',1.04*w_source(k)/(2*pi),...
                        'SampleRate',1/dt);
        for i=1:size(p_DG{1},1) % for each probe
            p_DG_filtered{k}(i,:) = filter(d,p_DG{1}(i,:));
        end
    end
end
%% [Post processing] Compute RMS value
% Compute RMS values of 'p_DG_filtered'
% RMS should be constant for lossless duct on [0,Lduct_num]
% Main parameters:
%   Number of reflections (best case: longer transient).
%   Integration interval for RMS. (The higher tmin and tmin-tmax, the
%   better.)
%   -> Influence of these parameters should be studied on any result.
%   -> The higher the base flow Mach number, the higher the influence of
%   reflections. (Poiseuille flow: reflections are negligible for Mc<=0.5
%   1e-2 difference on dB).
% Inputs: p_DG, idxProbeNodes, c0, tf, T_source, 
% Outputs: %d
% To compute RMS value, two strategies:
%   -> Remove the transient (first n periods) and compute RMS value
%   -> Filter with bandpass and compute RMS value
% The higher the filter order, the higher must be Nper_RMS_min.
Nper_RMS_min = 20; % first period to consider for RMS computations
% --
p_DG_RMS = zeros(size(probeDG,1),numel(p_DG_filtered));
for k=1:numel(p_DG_filtered) % for each frequency
    p_DG_RMS(:,k) = zeros(size(p_DG_filtered{k},1),1);
    for i=1:size(p_DG_filtered{k},1) % for each probe
            % Time interval to compute RMS
        tmin = Nper_RMS_min*T_source(k) + probeDG(i,1)/c0_num; % remove transient
        tmax = tf_DG{k}; % until end of simulation
        %tmax = min(tfNoRefl_num,tf_DG{k}); % without reflection
        mask = logical((t_DG{k}>=(tmin)).*(t_DG{k}<=tmax));
            % RMS value
        p_DG_RMS(i,k) = rms(p_DG_filtered{k}(i,mask));
    end
        % Convert to SPL
    p_DG_RMS(:,k) = 20*log10(p_DG_RMS(:,k)/p_DG_RMS(1,k));
end
%% [Plot] Pressure probe measurement
% Plot time series and spectrum
Source_idx = 1; % source to plot (for probe)
Probe_idx = [2,60,150]; % indexes of probes to plot
p_plot = p_DG_filtered;
p_plot = p_DG;
    % -- Probe pressure measurements
figure(5)
clf
p_DG_p = p_plot{Source_idx};
t_DG_p = t_DG{Source_idx};
for i=1:length(Probe_idx) % for the selected probes
    xprobe = probeDG(Probe_idx(i),:); % positi  on of probe
    mask = logical(t_DG_p>=(xprobe(1)/c0_num));
    p_DG_probe = p_DG_p(Probe_idx(i),mask);
    t_DG_probe = t_DG_p(mask)-xprobe(1)/c0_num;
    t_DG_probe = t_DG_probe/T_source(Source_idx);
        % Plot time series
    subplot(2,1,1)
    hold all
    plot(t_DG_probe,p_DG_probe,'DisplayName',sprintf('(x,y)=(%5.2e,%5.2e)',xprobe(1),xprobe(2)));
    subplot(2,1,2)
    hold all
        % Plot power spectral density
    fs = 1/dt; % sample frequency (Hz)
    [psd,freq] = periodogram(p_DG_probe,rectwin(length(p_DG_probe)),length(p_DG_probe),fs);
    plot(freq,10*log10(psd/max(psd)),'DisplayName',sprintf('(x,y)=(%5.2e,%5.2e)',xprobe(1),xprobe(2)));
    xlim([0,1e4]);
    grid on
    xlabel('frequency (Hz)');
    title('Power spectral density (dB/Hz)');    
end
subplot(2,1,1)
legend('show');
%xlabel('shifted time t - x/c0 (s)');
xlabel('shifted time t - x/c0 (T_{source})');
ylabel('p/z0');
if numel(p_plot)==numel(w_source) % frequency by frequency
    title(sprintf('Source @%1.2e Hz',w_source(Source_idx)/(2*pi)));
else % multiple frequency
    title(sprintf('Multiple frequencies: %s Hz',mat2str(w_source/(2*pi))));
end
%% [Plot] RMS value along wall
% Plot RMS value for every frequencies against experiments
PlotExpData = 1;
SPL_0 = 130; % SPL at x=0
    % NASA GIT
%Exp_idx_freq = [1,6,11,16,21,26]; % Index for experimental source frequency
Exp_idx_freq = [2,6,11,16,21,26]; % Index for experimental source frequency
Exp_idx_SPL = 2; % Index for source SPL
Exp_idx_Mach = 6; % Index for experimental Mach number
    % NASA GFIT
% Exp_idx_freq = [2,4,6,7,10,12]; % 600Hz,1kHz,1.4kHz,1.6kHz,2.2kHz,2.6kHz
% Exp_idx_SPL = 1; % Index for source SPL
% Exp_idx_Mach = 3; % Index for experimental Mach number
% --
figure
clf
for k=1:length(w_source) % for each frequency
    subplot(floor(length(w_source)/2),2,k)
    hold all
        % Experimental data
    if PlotExpData && exist('ExpData','var')
            % Use experimental SPL at x=0
        SPL_0 = ExpData{Exp_idx_Mach,Exp_idx_SPL}.MicroSPL(1,Exp_idx_freq(k));
        plot(ExpData{Exp_idx_Mach,Exp_idx_SPL}.Microxpos,ExpData{Exp_idx_Mach,Exp_idx_SPL}.MicroSPL(:,Exp_idx_freq(k)),'x',...
            'DisplayName',sprintf('%s-%1.2ekHz',ExpData{Exp_idx_Mach,Exp_idx_SPL}.name,ExpData{Exp_idx_Mach,Exp_idx_SPL}.freq(Exp_idx_freq(k))));
    end
        % Computed RMS value
    plot(1e3*probeDG(:,1),SPL_0+p_DG_RMS(:,k)-p_DG_RMS(1,k),'-',...
        'DisplayName',sprintf('f_{src}=%1.2ekHz tf=%1.2e*T_{src}',w_source(k)/(1e3*2*pi),tf_DG{k}/T_source(k)));
    legend('show');
    legend('Location','northoutside');
    %ylim([100,135])
    xlim([0,L_num]*1e3);
end
xlabel('distance x (mm)');
ylabel('SPL (dB)');
    % TODO: CaseName
CaseName
%% [Plot] Base flow vs experimental data
% Remark: In [Jones2005], the base flow velocity profiles are taken above 
% the liner. As such, they do not obey a no-slip condition at the boundary.
% The 'fit' is mostly done on the average Mach number and the boundary 
% layer thickness.
% -- Input parameter
Exp_idx_Mavg = 5; % Choose experimental average Mach number
delta = 0.3; % boundary layer thickness
Mavg = ExpData{Exp_idx_Mavg,1}.Mavg; % average Mach
% -------
clf
figure(1)
hold all
leg=cell(0);
    % -- Experimental data
M0y = ExpData{Exp_idx_Mavg,1}.M0_y(:,1); M0y = M0y(:);
S = trapz(M0y,ones(size(M0y))); % cross section surface
    % Plot each cross-section measurement
for k=1:size(ExpData{Exp_idx_Mavg,1}.M0_dz,3)
%    plot(M0y,ExpData{i,1}.M0_dz(:,1,k),'x-');
    Avg = trapz(M0y,ExpData{Exp_idx_Mavg,1}.M0_dz(:,1,k))./S;
    %leg{end+1}=sprintf('%s (avg: %1.2e)',ExpData{i,1}.M0_name{k},Avg);
end
M0_exp_mean = mean(ExpData{Exp_idx_Mavg,1}.M0_dz,3);
plot(M0y,M0_exp_mean,'x-');
Avg = trapz(M0y,mean(ExpData{Exp_idx_Mavg,1}.M0_dz,3))./S;
leg{end+1}=sprintf('mean (avg:%1.2e)',Avg);
    % ----- Flow models
a=0; b = max(M0y); 
    % -- Poiseuille flow
plot(M0y,M0_Poi(y2r(M0y,a,b),Mavg),'mo-');
r = linspace(-1,1,1e2); Avg=trapz(r,M0_Poi(r,Mavg))/2;
Shearmax =  max(abs(dM0_dr_Poi(r,Mavg)));
leg{end+1}=sprintf('Poiseuille (avg:%1.2e) shear=%1.2e',Avg,Shearmax);
    % -- Hyperbolic velocity profile
        % (a) Given: average Mach and delta
y = M0_Hy(y2r(M0y,a,b),Mavg,delta);
r = linspace(-1,1,1e2);
Avg=trapz(r,M0_Hy(r,Mavg,delta))/2;
Shearmax =  max(abs(dM0_dr_Hy(r,Mavg,delta)));
plot(M0y,y,'ro-');
leg{end+1}=sprintf('Hyperbolic (Given: Mavg & delta) (Mavg=%1.2e,Mc=%1.2e,delta=%1.2e,shear=%1.2e)',Avg,max(y),delta,Shearmax);
    % (b) Given: average Mach only (optimization on delta)
fun = @(delta,fdata)M0_Hy(y2r(M0y,a,b),Mavg,delta);
[delta_optim,res] = lsqcurvefit(fun,delta,M0y,M0_exp_mean,0);
y = M0_Hy(y2r(M0y,a,b),Mavg,delta_optim);
r = linspace(-1,1,1e2);
Avg=trapz(r,M0_Hy(r,Mavg,delta))/2;
Shearmax =  max(abs(dM0_dr_Hy(r,Mavg,delta_optim)));
% plot(M0y,y,'rx-');
% leg{end+1}=sprintf('Hyperbolic (Given: Mavg only) (Mavg=%1.2e,Mc=%1.2e,delta=%1.2e,shear=%1.2e)',Avg,max(y),delta_optim,Shearmax);
    % -- Corrected Power law
        % Given: Mavg. Optimization on N
fun = @(N,fdata)M0_Pw(y2r(M0y,a,b),Mavg,N);
[N_optim,res] = lsqcurvefit(fun,1,M0y,M0_exp_mean,0);
r = linspace(-1,1,1e2); Avg=trapz(r,M0_Pw(r,Mavg,N_optim))/2;
Shearmax =  max(abs(dM0_dr_Pw(r,Mavg,N_optim)));
% plot(M0y,M0_Pw(y2r(M0y,a,b),Mavg,N_optim),'g*-');
% leg{end+1}=sprintf('Power (Given: Mavg) Mavg=%1.2e, N=%1.2e,shear=%1.2e',Avg,N_optim,Shearmax);
    % -- Barycenter of Corrected power law and Poiseuille
alpha = 0.25;
r = linspace(-1,1,1e2); Shearmax =  max(abs(dM0_dr_Av(r,Mavg,N_optim,alpha)));
% plot(M0y,M0_Av(y2r(M0y,a,b),Mavg,N_optim,alpha),'gs-');
% leg{end+1}=sprintf('Average Power-Poiseuille (Given: Mavg) alpha=%1.2e,shear=%1.2e',alpha,Shearmax);
    % -- Eddy viscosity model from (Marx&Auregan, JSV2013) Eq.(3&4)
r = linspace(-1,1,1e3);
U0_Edv_v = zeros(1,length(r));
for k=1:length(r)
   U0_Edv_v(k) = M0_Edv(r(k));
end
Mc = max(y); % centerline Mach number
y = Mc*U0_Edv_v/max(U0_Edv_v); % normalization
Avg=trapz(r,y)./2; Shearmax = max(abs(dM0_dr_Edv(r,Re,nu_t(r,k,Re,A))));
plot(r2y(r,a,b),y,'k-');
leg{end+1}=sprintf('Eddy viscosity (Given: Mc) Mavg=%1.2e, Mc=%1.2e,Re=%1.2e,shear=%1.2e',Avg,max(y),Re,Shearmax);
    % -- Fit with cubic spline
        % Remove end points
M0y_fit = M0y(3:(end-3)); M0y_fit=M0y_fit(:);
M0_exp_fit = M0_exp_mean(3:(end-3)); M0_exp_fit=M0_exp_fit(:);
        % Null values at endpoint
M0y_fit = [b;M0y_fit;a]; M0_exp_fit = [0;M0_exp_fit;0];
    % Get fit
[M0_fit,dM0_dy_fit] = ExpDataFitBaseFlow(M0y_fit*1e-3,M0_exp_fit,Mavg);
Shearmax = max(abs(r2y_dr(0,b*1e-3)*dM0_dy_fit(M0y*1e-3)));
% plot(M0y,M0_fit(M0y*1e-3),'g');
% leg{end+1}=sprintf('Spline fit (Given: Mavg) shear=%1.2e',Shearmax);
    %
title(ExpData{Exp_idx_Mavg,1}.name);
legend(leg,'location','south');
xlabel('y (mm)');
ylabel('M (y)');
%% II - Plot cross-section velocity profile (derivative)
figure(2) % Compare derivative
clf
r = linspace(0,1,1e3);
leg=cell(0);
hold all
    % -- Poiseuille flow
y = abs(dM0_dr_Poi(r,Mavg));
plot(r,y,'m-');
leg{end+1}=sprintf('Poiseuille (max:%1.2e)',max(y));
    % -- Hyperbolic velocity profile
        % (a) Given: average Mach and delta
y=abs(dM0_dr_Hy(r,Mavg,delta));
plot(r,y,'r--');
leg{end+1}=sprintf('Hyperbolic (Given: Mavg & delta) (max=%1.2e)',max(y));
    % (b) Given: average Mach only (optimization on delta)
fun = @(delta,fdata)M0_Hy(y2r(M0y,a,b),Mavg,delta);
[delta_optim,res] = lsqcurvefit(fun,delta,M0y,M0_exp_mean,0);
y = abs(dM0_dr_Hy(r,Mavg,delta_optim));
plot(r,y,'r-');
leg{end+1}=sprintf('Hyperbolic (Given: Mavg only) (max=%1.2e)',max(y));
    % -- Corrected Power law
    % Given: Mavg. Optimization on N
fun = @(N,fdata)M0_Pw(y2r(M0y,a,b),Mavg,N);
[N_optim,res] = lsqcurvefit(fun,1,M0y,M0_exp_mean,0);
y = abs(dM0_dr_Pw(r,Mavg,N_optim));
% plot(r,y,'g-');
% leg{end+1}=sprintf('Power (Given: Mavg) max=%1.2e',max(y));
    % -- Barycenter of Corrected power law and Poiseuille
% y = abs(dM0_dr_Av(r,Mavg,N_optim,alpha));
% plot(r,y,'g--');
% leg{end+1}=sprintf('Average Power-Poiseuille (Given: Mavg) max=%1.2e',max(y));
    % -- Eddy viscosity model from (Marx&Auregan, JSV2013) Eq.(3&4)
y = abs(dM0_dr_Edv(r,Re,nu_t(r,k,Re,A)));
plot(r,y,'k-');
leg{end+1}=sprintf('Eddy viscosity (Given: Mc) max=%1.2e',max(y));    
    % -- Fit with cubic spline
% y = abs(r2y_dr(0,b*1e-3)*dM0_dy_fit(1e-3*r2y(r,a,b)));
% plot(r,y,'k-');
% leg{end+1}=sprintf('Fit max=%1.2e',max(y));    
title(ExpData{Exp_idx_Mavg,1}.name);
legend(leg,'location','north');
xlabel('r (rad)');
ylabel('dM/dr (r)');
ylim([0,5])
%% [Plot] Impedance vs experimental data
% -- Input parameter
Exp_idx_Mach = 1; % Index for experimental Mach number
Exp_idx_SPL = 1; % Index for source SPL
    % Load time-domain impedance boundary condition from file
        % NASA GIT
%Exp_idx_freq = [2,6,11,16,21,26]; % Index for experimental source frequency
load('GIT-CT57-SPL130dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-2OscillatoryPolesFinal','TDIBC');
load('GIT-CT57-SPL130dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4OscillatoryPolesFinal','TDIBC');
load('GIT-CT57-SPL130dB-Mavg400-2DFEM_TDIBC_ReflCoeff-ExpeOptim-2OscillatoryPolesFinal','TDIBC');
load('GIT-CT57-SPL130dB-Mavg400-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4Oscillatory2DiffusivePoles600HzFinal','TDIBC');
load('GIT-CT57-SPL130dB-Mavg400-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4Oscillatory2DiffusivePoles700HzFinal','TDIBC');
load('GIT-CT57-SPL130dB-Mavg400-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4Oscillatory2DiffusivePoles800HzFinal','TDIBC');
load('GIT-CT57-SPL130dB-Mavg400-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4Oscillatory2DiffusivePoles900HzFinal','TDIBC');
        % NASA GFIT
Exp_idx_freq =  [2,4,6,7,10,12]; % Index for experimental source frequency
load('GFIT-MP-SPL120dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-2OscillatoryPolesFinal','TDIBC');
load('GFIT-MP-SPL120dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4Oscillatory2DiffusivePolesFinal','TDIBC');
load('GFIT-MP-SPL120dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-6Oscillatory2DiffusivePolesFinal','TDIBC');
load('GFIT-MP-SPL120dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-8OscillatorypolesFinal','TDIBC');
load('GFIT-MP-SPL120dB-Mavg271-2DFEM_TDIBC_ReflCoeff-ExpeOptim-2OscillatoryPolesPhysivalValueRecovery','TDIBC');
load('GFIT-MP-SPL120dB-Mavg271-2DFEM_TDIBC_ReflCoeff-ExpeOptim-6Oscillatory1DiffusivePoles','TDIBC');
% -------
Exp_z = ExpData{Exp_idx_Mach,Exp_idx_SPL}.Z_id;
Exp_freq = ExpData{Exp_idx_Mach,Exp_idx_SPL}.freq*1e3;
Exp_name = ExpData{Exp_idx_Mach,Exp_idx_SPL}.name;
    % Plot frequencies (Hz)
freq = linspace(0e2,1e4,1e3);
figure(1)
clf
        % Plot models and experimental data
s = 1i*2*pi*freq;
subplot(2,2,1)
hold all
plot(Exp_freq,real(Exp_z),'ko');
xlabel('Frequency (Hz)');
ylabel('Normalized resistance (rad)');
%set(gca,'DataAspectRatio',[1,1,1])
ylim([0,10])
subplot(2,2,2)
hold all
plot(Exp_freq,imag(Exp_z),'ko');
xlabel('Frequency (Hz)');
ylabel('Normalized reactance (rad)');
%set(gca,'DataAspectRatio',[1,1,1])
subplot(2,1,2)
hold all
plot(Exp_freq,abs(z2beta(Exp_z)),'ko','DisplayName',sprintf('%s',Exp_name));
xlabel('Frequency (Hz)');
ylabel('|reflection coefficient| (rad)');
%set(gca,'DataAspectRatio',[1,1,1]);
        % Plot discrete TDIBC
if exist('TDIBC','var')
    TDIBC.plot(freq,'reflcoeff');
end
legend('location','northoutside');
    % Plot table of error at each frequency
fprintf('Freq (kHz)\tRe(Z)       Im(Z)        |beta|       arg(beta)\n');
fprintf('-----------------------------------------------------------------\n');
for k=1:length(Exp_idx_freq)
        % frequency
    freq_exp = Exp_freq(Exp_idx_freq(k));
        % identified z
    z_exp = Exp_z(Exp_idx_freq(k));
    beta_exp = z2beta(z_exp);
        % TDIBC
    beta_tdibc = TDIBC.Laplace(1i*2*pi*freq_exp,mean(xliner_num),0);
    z_tdibc = beta2z(beta_tdibc);
    err = (z_tdibc-z_exp)/z_exp;
        % Error
    dangle = rad2deg(angle(beta_tdibc)-angle(beta_exp));
    dangle = [dangle,mod(dangle,360)]; [~,idx] = min(abs(dangle)); % closest to 0°
    fprintf('%10.2ekHz %10.4g%% %10.3g%% | %10.4g%% %10.4g°\n',freq_exp,100*(real(z_tdibc-z_exp))/real(z_exp),100*(imag(z_tdibc-z_exp))/imag(z_exp),100*(abs(beta_tdibc)-abs(beta_exp))/abs(beta_exp),dangle(idx));
end
%% Export SPL curves to file
namestr = sprintf('%s_%s_DG%d-%dtriangles-CFL%3.2g_Delay-PPW%3.3g-Np%d-Nk%d_%s.csv',ExpData{Exp_idx_Mach,Exp_idx_SPL}.name,cst.Impedance.Name,DGCell.N+1,mesh.N,CFL,PPW_delay(max(w_source)/(2*pi),cst.Impedance.BndOpDelay{1}.Delay),Delay_Np,Delay_Nk,FlowName);
csvhead = 'x(m)';
    % Adjust SPL at x=0 before saving
for k=1:length(w_source) % for each frequency
    SPL_0 = ExpData{Exp_idx_Mach,Exp_idx_SPL}.MicroSPL(1,Exp_idx_freq(k));
    p_DG_RMS(:,k) = SPL_0+p_DG_RMS(:,k)-p_DG_RMS(1,k);
    csvhead = sprintf('%s,SPL%d',csvhead,k);
end
dlmwrite(namestr,csvhead,'Delimiter','');
dlmwrite(namestr,[probeDG(:,1),p_DG_RMS],'-append','Delimiter',',','newline','unix','precision','%1.6e');
%% -- %%%%%%%%%%%%%%%%%%%%%%%% Area for temporary plot
% Plot for convergence study
p_DG_RMS_CONVSTUDY(:,5)=p_DG_RMS(:,end);
x_DG_probe_CONVSTUDY(:,5)=1e3*probeDG(:,1);
%% --
figure(1)
clf
hold all
leg=cell(0);
plot(x_DG_probe_CONVSTUDY(:,1),p_DG_RMS_CONVSTUDY(:,1),'-');
leg{end+1}=sprintf('Np=6');
plot(x_DG_probe_CONVSTUDY(:,2),p_DG_RMS_CONVSTUDY(:,2),'-');
leg{end+1}=sprintf('Np=5');
plot(x_DG_probe_CONVSTUDY(:,3),p_DG_RMS_CONVSTUDY(:,3),'-');
leg{end+1}=sprintf('Np=4');
plot(x_DG_probe_CONVSTUDY(:,4),p_DG_RMS_CONVSTUDY(:,4),'-');
leg{end+1}=sprintf('Np=3');
plot(x_DG_probe_CONVSTUDY(:,5),p_DG_RMS_CONVSTUDY(:,5),'-');
leg{end+1}=sprintf('Np=2');
% plot(x_DG_probe_CONVSTUDY(:,3),p_DG_RMS_CONVSTUDY(:,3),'-');
% leg{end+1}=sprintf('Np=5');
% plot(x_DG_probe_CONVSTUDY(:,3),p_DG_RMS_CONVSTUDY(:,3),'-');
% leg{end+1}=sprintf('Np=7');
% Exp_idx_i = 1; % Index for average Mach number
% Exp_idx_j = 2; % Index for source SPL
% Exp_idx_freq = 11; % Index for source frequency
% if exist('ExpData','var')
%     for i=1:length(Exp_idx_i)
%        for j=1:length(Exp_idx_j)
%             plot(ExpData{Exp_idx_i(i),Exp_idx_j(j)}.Microxpos,ExpData{Exp_idx_i(i),Exp_idx_j(j)}.MicroSPL(:,Exp_idx_freq),'x');
%             leg{end+1}=sprintf('%s Freq = %1.2e kHz',ExpData{Exp_idx_i(i),Exp_idx_j(j)}.name,ExpData{Exp_idx_i(i),Exp_idx_j(j)}.freq(Exp_idx_freq));
%        end
%     end
% end
legend(leg);
legend(leg,'Location','northoutside');
xlabel('distance x (mm)');
ylabel('SPL (dB)');
%title();
%ylabel('SPL (dB)');
%ylim([80,134])
%plot(1e3*xliner_num(1)*[1,1],ylim,'k--');
%plot(1e3*xliner_num(2)*[1,1],ylim,'k--');