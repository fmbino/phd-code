%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - Plot and Video
%-------------------------------------------------------------------------
% Purpose: 
%   - Plot
%   - Create Video file
% Inputs:
%   x
%   cst.Nq
%   DGCell
%   mesh
%   CaseName
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Create display mesh
[trianglesPlot, nodesPlot] = DGMesh.buildPlotMesh();
%% Plot solution
it = 4; % iteration to plot
var = 3; % variable to plot
% --
it = min(it,size(x,2));
if ~exist('t') || isempty(t)
   t = 1; 
end
Np = DGCell.Np; Nk = mesh.N;
x_plot = DGMesh.interpolateField2PlotNodes(cst.Nq,x(:,it));
x_plot = x_plot(var + cst.Nq*(0:(Np*Nk-1)));
clf
trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),real(x_plot),'EdgeColor','None');
xlabel('x');
ylabel('y');
%set(gca, 'DataAspectRatio', [1 1 1])
%axis([mesh.bound(1,1),mesh.bound(2,1),mesh.bound(1,2),mesh.bound(2,2),-1,1])
%caxis([-2 2]);
colormap(jet)
colorbar
title(sprintf('%s\n q%d | DG%d | Nk=%d,Nodes=%d,CFL=%1.1g | it:%d/%d,t=%3.2g/%3.2g s',CaseName,var,DGCell.N,mesh.N,DGCell.Np*mesh.N,CFL,it,length(t),t(min(it,length(t))),t(end)));
view(0,90);
%% Plot solution (all field)
x_plot = real(DGMesh.interpolateField2PlotNodes(cst.Nq,x));
idx_r = cst.Nq*(0:(Np*Nk-1));
figure(1)
clf
subplot(2,2,1)
trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),x_plot(1 + idx_r),'EdgeColor','None');
xlabel('y');
ylabel('z');
colorbar
view([0,90]);
title(sprintf('u (max: %1.2g)',max(x_plot(1 + idx_r))));
subplot(2,2,2)
trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),x_plot(2 + idx_r),'EdgeColor','None');
xlabel('y');
ylabel('z');
title(sprintf('v (max: %1.2g)',max(x_plot(2 + idx_r))));
colorbar
view([0,90]);
subplot(2,2,3)
trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),x_plot(3 + idx_r),'EdgeColor','None');
xlabel('y');
ylabel('z');
title(sprintf('w (max: %1.2g)',max(x_plot(3 + idx_r))));
colorbar
view([0,90]);
subplot(2,2,4)
trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),x_plot(4 + idx_r),'EdgeColor','None');
xlabel('y');
ylabel('z');
title(sprintf('p/z0 (max: %1.2g)',max(x_plot(4 + idx_r))));
colorbar
view([0,90]);
%% Plot solution (video)
duration = 15; % (s)
fps = 24;
var = [3]; % variable(s) to plot
% --
hFig = figure(2);
    % set position and aspect ratio
x_length = 1024; aspratio = 0.056;
x_length = 1024; aspratio = 0.065;
set(hFig, 'Position', [1000 500 x_length aspratio*x_length]);
    % white background
hFig.Color = [1,1,1];
hold all
nframe = min(ceil(duration*fps),length(t)); % number of frame to display
idx_int = ceil(linspace(1,length(t),nframe)); % idx to display
clear Vi
Vi(length(idx_int)) = struct('cdata',[],'colormap',[]);
for i=1:length(idx_int)
    clf
    hold all
        % Display iteration n°i+1
    x_plot = DGMesh.interpolateField2PlotNodes(cst.Nq,x(:,idx_int(i)));
    for j=1:length(var) % for each variable
    subplot(length(var),1,j)
    x_plot2 = x_plot(var(j) + cst.Nq*(0:(DGCell.Np*mesh.N-1)));
    trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),real(x_plot2),'EdgeColor','None');
    shading interp
    %xlabel('x');
    %ylabel('y');
    axis([mesh.bound(1,1),mesh.bound(2,1),mesh.bound(1,2),mesh.bound(2,2),-5,5])
        % crop only the duct part
    if exist('Lduct_num','var')
        axis([mesh.bound(1,1),Lduct_num,mesh.bound(1,2),mesh.bound(2,2),-5,5])
    end
    caxis([-1 1]);
%    colorbar;
    colormap(jet)
    view(0,90);
    if var(j)==1
        title(sprintf('%s\n q%d | DG%d | Nk=%d,Nodes=%d,CFL=%1.1g | it:%d/%d,t=%3.2g/%3.2g s',CaseName,var(j),DGCell.N,mesh.N,DGCell.Np*mesh.N,CFL,idx_int(i),length(t),t(idx_int(i)),t(end)));
    else
        title(sprintf('q_%d',var(j)));
    end
    end
    %drawnow % force plot to show
        % no margins
    set(gca,'YTickLabel',[]);
    set(gca,'XTickLabel',[]);
    set(gca,'position',[0 0 1 1],'units','normalized');
    set(gca, 'DataAspectRatio', [1 1 1])
    Vi(i) = getframe(gcf);
end
fprintf('Now generating video file...');
%% Play movie
filename = 'LEE-Soft_Nk-188_Nxi-4_Np-2_Nk-3';
filename = 'LEE-GIT-Nk-552-Rigid.mp4';
filename = 'LEE-GIT-Nk-552-2p5kHz-M000.mp4';
hFig = gcf;
set(0,'DefaultFigureWindowStyle','default');
set(hFig, 'Position', [2000 10 900 1200]);
%movie(Vi);
%movie2avi(Vi,filename,'fps',fps);
v = VideoWriter([filename,'.avi'],'Archival'); v.FrameRate = fps;
open(v);
writeVideo(v,Vi); close(v);
    % command adapted from LaTeX media9 manual
system(sprintf('ffmpeg -i %s -vf scale="trunc(iw/2)*2:trunc(ih/2)*2" -c:v libx264 -crf 23 -profile:v high -pix_fmt yuv420p %s',v.Filename,[v.Filename,'_conv.mp4']));
system(sprintf('rm %s',v.Filename));
fprintf('Done\n');
%clear Vi