%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - II Transport 2D (Variable coefficient)
% Case1: Hesthaven, Exercise 5.10.2
% This 1D case has an exact solution.
% Mesh over [0,1]
%-------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% PDE definition (without boundary conditions)
u0 = @(x)(sin(x)); % Initial condition
Nq = 1;
    % First case (with exact solution)
c0 = 0.4;
cx = @(x)(c0+sin(pi*x-1)/pi);
B = @(x,y)(-cos(pi*x-1));
c = @(x,y)([cx(x);0]); % [cx;cy] propagation speed (m/s)
Ax = @(x,y)(cx(x));
Ay = @(x,y)(0);
u = @(x,t)(u0(1/pi+(2/pi)*atan(exp(-t).*tan((pi*(x-c0*t)-1)/2)))); % exact solution
%% Numerical flux: FVS
finw = @(n,x,y)(diag(min(0,dot(c(x,y),n))*ones(1,Nq)));

%% Build mass and stiffness: global formulation
fout = @(n,x,y)(diag(max(0,dot(c(x,y),n))*ones(1,Nq)));
arg_Upwind_FVS = NumericalFlux_2D_UpwindFVS(@(n,x,y)(fout(n,x,y)));
    % 'zone' is a cell of structure that describes each zone
zone = cell(0);
zone{end+1} = struct('name','Interior_UpwindFVS','physicalEntity',[5],'arg',arg_Upwind_FVS);
zone{end+1} = struct('name','Outlet_UpwindFVS','physicalEntity',[2,4],'arg',arg_Upwind_FVS);
zone{end+1} = struct('name','Inlet_UpwindFVS','physicalEntity',[4],'arg',arg_Upwind_FVS);
[M,K,C,F_source] = DG2D_buildGlobalFormulation(Ax,Ay,B,DGMesh,zone);
%% Time-domain solving: initial condition and source
tf = 15; % Final time
    % -- Initial condition (Gaussian pulse) (Nx2) -> Nx1
f = @(x)(u0(x));
q0 = @(x)(reshape(repmat(f(x(:,1))',[Nq,1]),Nq*size(x,1),1));
    % -- Source (from exact solution)
f = @(t)(u(0,t));
q_source = @(t)(f(t)*ones(Nq,1));
    % check source term
t = linspace(0,tf,1e2);
plot(t,q_source(t))
%% Time-domain solving
CFL = 0.1; % CFL number
x = linspace(0,1,1e2);
dt = CFL*min(mesh.charLength)/max(cx(x)); % Time step
clear x
%in = 10; % node to store
nodes = DGMesh.getNodesCoordinates();
[x,t] = DG2D_SolveTimeDomain(M,K,F_source,DGMesh,Nq,tf,dt,'x0',q0(nodes),'xs',q_source,'storeOnlyLast',1);
%% Plot error
it=length(t);
tplot=t(end);
Np = DGCell.Np; Nk = mesh.N;
[trianglesPlot, nodesPlot] = DGMesh.buildPlotMesh();
x_plot = DGMesh.interpolateField2PlotNodes(Nq,x(:,min(it,size(x,2))));
xdg = DGMesh.getNodesCoordinates(); xdg = xdg(:,1); xdgplot = nodesPlot(:,1);
x_plot = x_plot(:)-u(xdgplot,tplot);
clf
trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),x_plot,'EdgeColor','None');
xlabel('x');
ylabel('y');
%set(gca, 'DataAspectRatio', [1 1 1])
%axis([-1,1,-1,0,-2,3])
title(sprintf('Solution: q_%d (t=%1.3g) L2 Error: %1.2e',var,tplot,sqrt(mean((x(:)-u(xdg,tplot)).^2))));
view(45,45);