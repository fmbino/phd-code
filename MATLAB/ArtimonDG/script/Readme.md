This folder contains MATLAB script that uses Artimon DG. The relevant files are highlighted below.


`DG1D_*`: These files belong to the 1D version of Artimon, less clean than the 2D version.
    

# Scripts used for [10.1016/j.jcp.2018.08.037](https://doi.org/10.1016/j.jcp.2018.08.037)
    
`DG2D_LEE_AeroacousticalDuct.m`:  This script has been used to generate the curves of [10.1016/j.jcp.2018.08.037](https://doi.org/10.1016/j.jcp.2018.08.037). After loading the mesh and defining the base flow, several TDIBC are available. The postprocessing is tailored to produce the graph of interest for the papers. The verbosity in some regions is justified by the need of automating as much as possible the graph generation. This is the most advanced script utilising Artimon DG: by following it step by step, one can get a fair understanding of how to use Artimon for the LEEs.

`DG2D_LEE_AeroacousticalDuct_WithIdentifiedImpedance.m`: This script is close to `DG2D_LEE_AeroacousticalDuct.m` and has been used for [10.1016/j.jcp.2018.08.037](https://doi.org/10.1016/j.jcp.2018.08.037). The main difference is that the TDIBC are not fancy multipole, but elementary proportional-integral-derivative model, tuned to the impedance values identified in the experiments. This script has been used to generate the 'PID' curves of [10.1016/j.jcp.2018.08.037](https://doi.org/10.1016/j.jcp.2018.08.037).
    
`DG2D_LEE_ImpedanceTube.m`: This script has been used for the Impedance tube section of [10.1016/j.jcp.2018.08.037](https://doi.org/10.1016/j.jcp.2018.08.037). The impedance tube is solved with a 2D discretization, which is obviously a tad wasteful since we could equally well use a 1D discretization. The verbosity of the script mainly comes from the many TDIBCs that have been implemented: each TDIBC is defined in a separate section. The user only interested in, say, the beta formulation for a linear TDIBC can remove most of the lines.
    
`DG2D_LEECrossSection.m`: This script has been used to plot the eigenvalues of an acoustical cavity, and show that some numerical flux can break the passivity of the continuous problem. It is called `LEECrossSection` because initially it was intented to solve the cross-section LEEs with base flow, but this has never been used  seriously.
    

# Other scripts solving the LEEs
    
`DG2D_LEE_LinedDuct.m`: This scripts solves the LEEs for a 2D lined duct. The scripts used for [10.1016/j.jcp.2018.08.037](https://doi.org/10.1016/j.jcp.2018.08.037) have been derived from this script. Hence, it is essentially a  simpler version of the scripts used for [10.1016/j.jcp.2018.08.037](https://doi.org/10.1016/j.jcp.2018.08.037).
    
    
# Generic scripts

`DG2D_PlotVideo.m`: This script plots the solution (stored in the 'x' variables) and generates a video.

`DG2D_ReadMesh.m`: This script demonstrates reading a mesh and initializing a DG mesh. The commands demonstrated in this script are basic and needed regardless of the PDE of interest.

# Scripts solving the 2D transport equation

`DG2D_Transport.m`, `DG2D_TransportVariable.m`, `DG2D_TransportVariable_Aliasing.m`: These scripts demonstrate the use of Artimon DG for the 2D transport equation.
    
# Validation scripts

The scripts in the folder `Validation` have been used during the development of Artimon, in particular to validate the order of the spatial discretization.