%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - Mesh and cell definition
%-------------------------------------------------------------------------
% The purpose of this script is to read a mesh file and initialise the
% class 'DGMesh'.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Read mesh (gmesh format) and initialise object
%ls '/d/fmontegh/TIBC/Data/Mesh'
ls '/home/bino/Donnees-Laptop/Study-Scientific/TDIBC/Data/Mesh/'
m = load_gmsh2('SquareUnstructured68.msh',[1 2]); % read lines (1) and triangles (2)
%m = load_gmsh2('mesh_refined.msh',[1 2]); % read lines (1) and triangles (2)
%m = load_gmsh2('mesh_coarse.msh',[1 2]); % read lines (1) and triangles (2)
nodes = m.POS(:,1:2);
triangles = m.TRIANGLES(:,[4,1,2,3]);
boundFaces = m.LINES(:,[3,1,2]);
physicalEntitiesIdx = m.physicalEntitiesIdx;
physicalEntitiesName = m.physicalEntitiesName;
mesh = Mesh_2D_Triangle(nodes,boundFaces,triangles,physicalEntitiesIdx,physicalEntitiesName);
clear m nodes triangles boundFaces physicalEntitiesIdx physicalEntitiesName
mesh.show();
%mesh.showTriangle(1);
mesh.printSummary();
%% Initialize DG cell
DGCell = DGCell_2D_Triangle(5); % degree of polynomial on cell
%DGCell.printSummary();
%DGCell.show();
%% Initialize DG Mesh
DGMesh = DGMesh_2D_Triangle(mesh,DGCell);
%DGMesh.printSummary();
%DGMesh.showPlotMesh();
DGMesh.showDGNodes();