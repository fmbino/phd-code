%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - Non-regression test
%-------------------------------------------------------------------------
% The purpose of this script is to validate the functions of Artimon DG.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Set up DG
m = load_gmsh2('test-mesh.msh',[1 2]); % read lines (1) and triangles (2)
nodes = m.POS(:,1:2);
triangles = m.TRIANGLES(:,[4,1,2,3]);
boundFaces = m.LINES(:,[3,1,2]);
physicalEntitiesIdx = m.physicalEntitiesIdx;
physicalEntitiesName = m.physicalEntitiesName;
mesh = Mesh_2D_Triangle(nodes,boundFaces,triangles,physicalEntitiesIdx,physicalEntitiesName);
clear m nodes triangles bound faces physicalEntitiesIdx physicalEntitiesName
N = 4; % order
DGCell = DGCell_2D_Triangle(N);
DGMesh = DGMesh_2D_Triangle(mesh,DGCell);
%% Set up PDE
Nq = 3;
Ax = rand(Nq,Nq);
Ay = rand(Nq,Nq);
B = rand(Nq,Nq);
%% Set up flux matrices
    % FVS split
finw = @(n)(Ax);
fout = @(n)(Ay);
    % Proportional impedance flux
f0 = @(n)(Ax);
%% Validate
    % Loop over elements and interior faces
[M,K] = DG2D_LoopTriangle(Ax,Ay,B,fout,finw,DGMesh,5);
[M1,K1] = DG2D_LoopTriangle_Parallel(Ax,Ay,B,fout,finw,DGMesh,5);
fprintf('[LoopTriangle]: Parallel (M) %1.1e (K) %1.1e\n',full(max(abs(M1(:)-M(:)))),full(max(abs(K1(:)-K(:)))));
clear M1 K1
[M1,K1] = DG2D_LoopTriangle_Variable(@(x,y)(Ax),@(x,y)(Ay),@(x,y)(B),@(n,x,y)(fout(n)),@(n,x,y)(finw(n)),DGMesh,5);
fprintf('[LoopTriangle]: Variable (M) %1.1e (K) %1.1e\n',full(max(abs(M1(:)-M(:)))),full(max(abs(K1(:)-K(:)))));
clear M1 K1
[M1,K1] = DG2D_LoopTriangle_Variable_Parallel(@(x,y)(Ax),@(x,y)(Ay),@(x,y)(B),@(n,x,y)(fout(n)),@(n,x,y)(finw(n)),DGMesh,5);
fprintf('[LoopTriangle]: Variable parallel (M) %1.1e (K) %1.1e\n',full(max(abs(M1(:)-M(:)))),full(max(abs(K1(:)-K(:)))));
clear M K M1 K1
    % Outlet FVS
[K] = DG2D_LoopBoundaryFaces_OutletFVS(fout,DGMesh,[3]);
[K1] = DG2D_LoopBoundaryFaces_OutletFVS_Variable(@(n,x,y)(fout(n)),DGMesh,[3]);
fprintf('[LoopBoundaryFaces_OutletFVS]: Variable (K) %1.1e\n',full(max(abs(K1(:)-K(:)))));
clear K K1
    % Inlet FVS
F_source = DG2D_LoopBoundaryFaces_InletFVS(finw,DGMesh,1);
F_source1 = DG2D_LoopBoundaryFaces_InletFVS_Variable(@(n,x,y)(finw(n)),DGMesh,1);
fprintf('[LoopBoundaryFaces_InletFVS]: Variable (K) %1.1e\n',full(max(abs(F_source1(:)-F_source(:)))));
clear F_source F_source1
    % Proportional impedance
K = DG2D_LoopBoundaryFaces_Impedance_Prop(f0,DGMesh,[2,4]);
K1 = DG2D_LoopBoundaryFaces_Impedance_Prop_Variable(@(n,x,y)(f0(n)),DGMesh,[2,4]);
fprintf('[LoopBoundaryFaces_InletFVS]: Variable (K) %1.1e\n',full(max(abs(K1(:)-K(:)))));
clear K K1
    % Periodicity (no 'Variable' counterpart)
%K = DG2D_LoopBoundaryFaces_PeriodicFVS(fout,finw,DGMesh,[1,3]);