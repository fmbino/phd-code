%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - II LEE 2D Cross-section
%-------------------------------------------------------------------------
% The purpose of this script is to define the numerical zones avalaible.
% A numerical zone is what is called in gmsh a 'PhysicalEntity'; it can be
% a boundary or a surface.
% The structure array 'zone' is the output of this script, and should be
% used to define computational cases.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
beta_fun = @(z)((z-1)/(z+1));
z_fun = @(beta)((1+beta)/(1-beta));
%% PDE definition
% q = [u,v,w,p/z0];
% Axial wavenumber: f(z)=exp(-1i*k*z) (longitudinal direction, not solved)
Ax = @(c0)(...
    [0,0,0,0;
    0,0,0,c0;...
    0,0,0,0;...
    0,c0,0,0]);
Ay = @(c0)(...
    [0,0,0,0;
    0,0,0,0;...
    0,0,0,c0;...
    0,0,c0,0]);
B = @(u0,du0dy,du0dz,k,c0)(...
    [-1i*k*u0,du0dy,du0dz,-1i*k*c0;
    0,-1i*k*u0,0,0;...
    0,0,-1i*k*u0,0;...
    -1i*k*c0,0,0,-1i*k*u0]);
%% Numerical flux function
% Subsonic and smooth base flow
% To define a zone, a numerical flux function is (usually) necessary.
    % FVS split
fout = @(n,c0)(c0/2*...
    [0,0,0,0;...
    0,n(1)^2,n(1)*n(2),n(1);...
    0,n(1)*n(2),n(2)^2,n(2);...
    0,n(1),n(2),1]);
    % -- Rigid wall 
fwall = @(n,c0)((c0/2)*[0,0,0,0;0,0,0,n(1);0,0,0,n(2);0,n(1),n(2),0]*...
    ([-1*eye(3),0*[0;n(:)];2*[0,n(:)'],1]+eye(4)));
    % -- Impedance: ghost state expression
    % Choose desired numerical flux with 'NumFLux' variable
TenN = @(n)([0,0,0;0,n(1)^2,n(1)*n(2);0,n(1)*n(2),n(2)^2]); % n(x)n
        % Best condition number (alpha=beta0)
if strcmp(NumFlux,'Beta0')
Ghost0 = @(n,beta0)([-beta0*TenN(n),(1-beta0)*[0;n(:)];(1+beta0)*[0,n(:)'],beta0]);
Ghost1 = @(n,beta0,a1)((1-beta0)*a1*[-eye(3),0*[0;n(:)];[0,n(:)'],0]);
Ghostk = @(n,beta0,ak)((1-beta0)*ak*[-[0;n(:)];1]);
end
        % Exact energy balance (alpha=-1)
if strcmp(NumFlux,'Alpham1')
Ghost0 = @(n,beta0)([1*TenN(n) ,0*[0;n(:)];(2*z_fun(beta0))*[0,n(:)'],-1]);
Ghost1 = @(n,beta0,a1)(a1*[0*eye(3),0*[0;n(:)];[0,2*n(:)'],0]);
Ghostk = @(n,beta0,ak)(ak*[0*[0;n(:)];2]);
end
    %Proportional impedance flux (using reflexion coeff. beta0)
    % (beta0 = 1 for rigid wall)
f0 = @(n,beta0,c0)((c0/2)*[0,0,0,0;0,0,0,n(1);0,0,0,n(2);0,n(1),n(2),0]*...
    (Ghost0(n,beta0)+eye(4)));
clear Ghost0
    % Derivative impedance flux (using impedance coefficient a1)
f1 = @(n,beta0,a1,c0)((c0/2)*[0,0,0,0;0,0,0,n(1);0,0,0,n(2);0,n(1),n(2),0]*(Ghost1(n,beta0,a1)));
clear Ghost1
    % Boundary operator impedance flux (using impedance coefficient ak)
fBndOp = @(n,beta0,ak,c0)((c0/2)*[0,0,0,0;0,0,0,n(1);0,0,0,n(2);0,n(1),n(2),0]*Ghostk(n,beta0,ak));
cBndOp = @(n)([0;n(:);0]); % observation vector
clear Ghostk
%% Zone definition
    % Arguments for zone definition
arg_UpwindFVS = @(c0)NumericalFlux_2D_UpwindFVS(@(n,x,y)(fout(n,c0)));
arg_Impedance_Rigid = @(c0)NumericalFlux_2D_Generic(@(n)(fwall(n,c0)));
arg_Impedance_Prop = @(beta0,c0)NumericalFlux_2D_Generic(@(n,x,y)(f0(n,beta0(x,y),c0)));
arg_Impedance_Der = @(beta0,a1,c0)NumericalFlux_2D_Generic(@(n,x,y)(f1(n,beta0(x,y),a1(x,y),c0)));
arg_Impedance_Operator = @(beta0,ak,c0)NumericalFlux_2D_Generic_Operator(@(n,x,y)(fBndOp(n,beta0(x,y),ak(x,y),c0)),cBndOp);
clear fout fwall f0 f1 fBndOp cBndOp
    % 'zone.zoneName' is a structure array that describes 'zoneName'
    % idx: index of Physical entity
clear zone
zone.PDEName = 'LEE 2D Cross Section';
zone.Interior_UpwindFVS = @(idx,c0)struct('name','Interior_UpwindFVS','physicalEntity',idx,'arg',arg_UpwindFVS(c0));
zone.Outlet_UpwindFVS = @(idx,c0)struct('name','Outlet_UpwindFVS','physicalEntity',idx,'arg',arg_UpwindFVS(c0));
zone.Inlet_UpwindFVS = @(idx,c0)struct('name','Inlet_UpwindFVS','physicalEntity',idx,'arg',arg_UpwindFVS(c0));
zone.Impedance_Rigid = @(idx,c0)struct('name','Boundary_Generic_Proportional','physicalEntity',idx,'arg',arg_Impedance_Rigid(c0));
zone.Impedance_Proportional = @(idx,beta0,c0)struct('name','Boundary_Generic_Proportional','physicalEntity',idx,'arg',arg_Impedance_Prop(beta0,c0));
zone.Impedance_Derivative = @(idx,beta0,a1,c0)struct('name','Boundary_Generic_Derivative','physicalEntity',idx,'arg',arg_Impedance_Der(beta0,a1,c0));
zone.Impedance_Operator = @(idx,beta0,ak,c0,secondaryName)struct('name','Boundary_Generic_Operator','secondaryName',secondaryName,'physicalEntity',idx,'arg',arg_Impedance_Operator(beta0,ak,c0));
    % Clean workspace
clear arg_UpwindFVS arg_Impedance_Rigid arg_Impedance_Prop arg_Impedance_Der arg_Impedance_Operator