%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Validate Artimon DGCell class, with Hesthaven code
% ---
% The purpose of this script is to compare the class DGCell_2D_Triangle to
% the script provided by Hesthaven, for validation purposes.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Globals2D; % Global variables used by Hesthaven
N = 2; % Polynomial order used for approximation (Hesthaven)

%% Load mesh
nodes = h5read('/d/fmontegh/TIBC/Data/Mesh/mesh_Nodes.h5','/dset');
boundFaces = h5read('/d/fmontegh/TIBC/Data/Mesh/mesh_Faces.h5','/dset');
triangles = h5read('/d/fmontegh/TIBC/Data/Mesh/mesh_Triangles.h5','/dset');
Nv = length(nodes); % no. of vertices
K = length(triangles); % no. of triangles
VX = nodes(:,1)';
VY = nodes(:,2)';
EToV = triangles(:,2:end); % triangles

%% I - Hesthaven: Initialize solver and construct grid and metric
% The value of the metric terms are subject to innacuracy with the
% Hesthaven method (depends upon the value of N chosen).
StartUp2D;

%% II Artimon - Initialize DG cell
DGCell = DGCell_2D_Triangle(N);
%cellDG.printSummary();
%cellDG.show();

%% Compare
fprintf('Polynomial order: %d\n',DGCell.N-N);
fprintf('Number of nodes: %d\n',DGCell.Np-Np);
fprintf('Nodes per edge: %d\n',DGCell.Nfp-Nfp);
fprintf('Nodes: %d\n',max(max(abs(DGCell.xi-[r,s]))));
fprintf('Face Index (numbering): %d\n',max(max(abs(DGCell.faceIdx-Fmask))))
fprintf('Mass matrix M [[<li,lj>]_i,j] %1.1e\n',max(max(abs(DGCell.M-inv(V*V')))));
    % Extract Emat from Lift2D first
fprintf('Edge integral, Face 1: %d\n',issymmetric(DGCell.Eps(:,:,1)));
fprintf('Edge integral, Face 2: %d\n',issymmetric(DGCell.Eps(:,:,2)));
fprintf('Edge integral, Face 3: %d\n',issymmetric(DGCell.Eps(:,:,3)));
fprintf('Edge integral, Face 1: %1.1e\n',max(max(abs(DGCell.Eps(:,Fmask(:,1),1) - Emat(:,1:Nfp)))));
fprintf('Edge integral, Face 2: %1.1e\n',max(max(abs(DGCell.Eps(:,Fmask(:,2),2) - Emat(:,Nfp+(1:Nfp))))));
fprintf('Edge integral, Face 3: %1.1e\n',max(max(abs(DGCell.Eps(:,Fmask(:,3),3) - Emat(:,2*Nfp+(1:Nfp))))));