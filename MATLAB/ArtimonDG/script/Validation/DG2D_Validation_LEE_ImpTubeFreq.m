%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - II LEE 2D (Acoustics)
%-------------------------------------------------------------------------
% Order validation script.
% Two cases are implemented below:
%   (a) Impedance tube case.
%       Comparison with the 1D analytical solution.
%   (b) Plane wave in rigid duct case.
%       (= free field plane wave)
%       Comparison with analytical solution.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
run 'DG2D_LEELongitudinal_ZoneDefinition.m'
%% I Compute order: cases to consider
N = [4,6,8,10,12,14,16]; % degree of polynomial on cell
    % Structured mesh
meshfile = {'Square2.msh','Square8.msh','Square18.msh','Square32.msh','Square50.msh','Square98.msh','Square220.msh'}; % coarse -> refined
meshfile = {'Square8.msh','Square18.msh','Square32.msh','Square50.msh','Square98.msh','Square220.msh'}; % coarse -> refined
    % Unstructured mesh
%meshfile = {'SquareUnstructured16.msh','SquareUnstructured28.msh','SquareUnstructured40.msh','SquareUnstructured68.msh'}; % coarse -> refined
%% IIa PDE definition: impedance tube (common)
% Impedance tube on [0,L]
% Impedance boundary condition at x=L
% q = [u,v,p/z0];
    % -- Case definition
L = 1; % length of impedance tube
    % Uniform base flow
cst.u0 = @(x,y)0; cst.du0dx = @(x,y)0; cst.du0dy = @(x,y)0;
cst.v0 = @(x,y)0; cst.dv0dx = @(x,y)0; cst.dv0dy = @(x,y)0;
    % Matrices
Ax_fun = @(x,y)Ax(cst.u0(x,y),cst.c0);
Ay_fun = @(x,y)Ay(cst.v0(x,y),cst.c0);
B_fun = @(x,y)B(cst.u0(x,y),cst.du0dx(x,y),cst.du0dy(x,y),cst.v0(x,y),cst.dv0dx(x,y),cst.dv0dy(x,y),cst.c0);
    % Source
Ai = 1; % Ai (Pa)
omega = 20; % (rad/s)
%% IIa Impedance boundary condition: z formulation
% Arbitrary number of operators
% z(s) = a0 + a1*s + aDD*exp(-s*tauDD) + aQ*Q(s) + aR*R(s)*exp(-s*tauR)
a0 = 5; a1 = 10;
    % direct delay
aDD = {1,2}; tauDD = {1,2}; 
    % undelayed operators
aQ = {10,1}; Q = {@(s)sqrt(s),@(s)1./sqrt(s)};
    % delayed operators
aR = {1,2}; tauR = {1,2}; R = {@(s)sqrt(s),@(s)1./sqrt(s)};
    % Value of z(1i*omega)
z = a0 + a1*1i*omega;
for i=1:length(aDD)
   z = z + aDD{i}*exp(-1i*omega*tauDD{i}); 
end
for i=1:length(aQ)
    z = z + aQ{i}*Q{i}(1i*omega);
end
for i=1:length(aR)
    z = z + aR{i}*R{i}(1i*omega)*exp(-1i*omega*tauR{i});
end
    % -- Analytical solution (Impedance tube)
AnalyticalSolution = @(x)(DG2D_LEE_AnalyticalSolution_ImpTube(x,omega,Ai,cst.c0,beta_fun(z),L));
    % -- DG boundary condition
        % only a correct 'Laplace_ex' is needed to solve in the frequency domain
BndOp = cell(0);
for i=1:length(aQ)
    BndOp{i} = DiffusiveOperator(1,1,'Laplace',Q{i});
end
BndOpDelay = cell(0);
for i=1:length(aQ)
    BndOpDelay{i} = DiffusiveOperator(1,1,'Laplace',R{i});
end
cst.Impedance = ImpedanceBoundaryCondition('Z',beta_fun(a0),a1,aDD,tauDD,aQ,BndOp,aR,BndOpDelay,tauR);
    % -- Numerical flux function
IBC_flux = 'Impedance_FluxZunstable';
IBC_flux = 'Impedance_FluxZ';
zoneCase = DG2D_LEELongitudinal_createZone(struct('Interior',[5],'Source',[4],'Wall',[1,3],IBC_flux,[2]),cst,zone);
CaseName = sprintf('LEE Impedance tube (omega=%1.2g)\n %s',omega,IBC_flux);
%% IIa Impedance boundary condition: zy formulation
% Arbitrary number of operators
% y(s) = a0 + a1*s + aDD*exp(-s*tauDD) + aQ*Q(s) + aR*R(s)*exp(-s*tauR)
a0 = 5; a1 = 10;
    % direct delay
aDD = {1,2}; tauDD = {1,2};
    % undelayed operators
aQ = {10,1}; Q = {@(s)sqrt(s),@(s)1./sqrt(s)};
    % delayed operators
aR = {1,2}; tauR = {1,2}; R = {@(s)sqrt(s),@(s)1./sqrt(s)};
    % Value of y(1i*omega)
y = a0 + a1*1i*omega;
for i=1:length(aDD)
   y = y + aDD{i}*exp(-1i*omega*tauDD{i}); 
end
for i=1:length(aQ)
    y = y + aQ{i}*Q{i}(1i*omega);
end
for i=1:length(aR)
    y = y + aR{i}*R{i}(1i*omega)*exp(-1i*omega*tauR{i});
end
    % -- Analytical solution (Impedance tube)
AnalyticalSolution = @(x)(DG2D_LEE_AnalyticalSolution_ImpTube(x,omega,Ai,cst.c0,beta_fun(1/y),L));
    % -- DG boundary condition
        % only a correct 'Laplace_ex' is needed to solve in the frequency domain
BndOp = cell(0);
for i=1:length(aQ)
    BndOp{i} = DiffusiveOperator(1,1,'Laplace',Q{i});
end
BndOpDelay = cell(0);
for i=1:length(aQ)
    BndOpDelay{i} = DiffusiveOperator(1,1,'Laplace',R{i});
end
Admittance = ImpedanceBoundaryCondition('Y',beta_fun(a0),a1,aDD,tauDD,aQ,BndOp,aR,BndOpDelay,tauR);
BndOp = DiffusiveOperator(1,1,'Laplace',@(s)(1/y*(s./s)));
Impedance = ImpedanceBoundaryCondition('Z',beta_fun(0),0,{0},{0},{1},{BndOp},{0},{BndOp},{0});
cst.Impedance = ImpedanceBoundaryCondition_ZY(Impedance,Admittance);
    % Numerical flux function
IBC_flux = 'Impedance_FluxZY';
zoneCase = DG2D_LEELongitudinal_createZone(struct('Interior',[5],'Source',[4],'Wall',[1,3],IBC_flux,[2]),cst,zone);
CaseName = sprintf('LEE Impedance tube (omega=%1.2g)\n %s',omega,IBC_flux);
%% IIa Impedance boundary condition: beta formulation
% Arbitrary number of operators
% beta(s) = 1+ aQ*Q(s) + aR*R(s)*exp(-s*tauR)
    % undelayed operators
aQ = {1,1}; Q = {@(s)1./(1+sqrt(s)),@(s)1./(2+sqrt(s))};
    % delayed operators
aR = {1,1}; tauR = {1,1}; R = {@(s)1./(1+sqrt(s)),@(s)1./(2+sqrt(s))};
    % Value of beta(1i*omega)
for i=1:length(aQ)
    beta = beta + aQ{i}*Q{i}(1i*omega);
end
for i=1:length(aR)
    beta = beta + aR{i}*R{i}(1i*omega)*exp(-1i*omega*tauR{i});
end
    % -- Analytical solution (Impedance tube)
AnalyticalSolution = @(x)(DG2D_LEE_AnalyticalSolution_ImpTube(x,omega,Ai,cst.c0,beta,L));
    % -- DG formulation
BndOp = cell(0);
for i=1:length(aQ)
    BndOp{i} = DiffusiveOperator(1,1,'Laplace',Q{i});
end
BndOpDelay = cell(0);
for i=1:length(aQ)
    BndOpDelay{i} = DiffusiveOperator(1,1,'Laplace',R{i});
end
cst.Impedance = ImpedanceBoundaryCondition('beta',beta_fun(1),0,{},{},aQ,BndOp,aR,BndOpDelay,tauR);
    % Ignore the above and set directly the IBC
beta=0.5;
cst.Impedance = ImpedanceBoundaryCondition('beta',beta_fun(beta),0,{},{},{},{},{},{},{});
AnalyticalSolution = @(x)(DG2D_LEE_AnalyticalSolution_ImpTube(x,omega,Ai,cst.c0,beta,L));
    % Numerical flux function
IBC_flux = 'Impedance_FluxBeta';
zoneCase = DG2D_LEELongitudinal_createZone(struct('Interior',[5],'Source',[4],'Wall',[1,3],IBC_flux,[2]),cst,zone);
CaseName = sprintf('LEE Impedance tube (omega=%1.2g)\n %s',omega,IBC_flux);
%% IIb PDE definition: free field
% Free propagation of a plane wave (rigid infinite duct)
    % -- Case definition
c0 = 1;
z0 = 1;
U0 = 0.8; V0 = 0;
Ax_fun = @(x,y)[U0,0,c0;0,U0,0;c0,0,U0];
Ay_fun = @(x,y)[V0,0,0;0,V0,c0;0,c0,V0];
Nq = 3;
B_fun = @(x,y)zeros(Nq,Nq);
CaseName = sprintf('LEE Infinite Rigid Duct (omega=%1.2g, U0=%1.2g)',omega,U0);
    % Source
Ai = 1; % Ai (Pa)
omega = 1e1; % (rad/s)
cst.DirectDelay = [];
cst.BndOp = [];
cst.BndOpDelay = [];
    % -- Case definition: Infinite Duct
zoneCase = cell(0);
zoneCase{end+1} = zone.Interior_UpwindFVS(5,@(x,y)U0,@(x,y)V0,c0);
zoneCase{end+1} = zone.Outlet_UpwindFVS([4,2],@(x,y)U0,@(x,y)V0,c0);
zoneCase{end+1} = zone.Inlet_UpwindFVS([4],@(x,y)U0,@(x,y)V0,c0);
zoneCase{end+1} = zone.Impedance_Rigid([1,3],c0);
    % -- Analytical solution
L = 1; % Duct on [0,L]
AnalyticalSolution = @(x)(DG2D_LEE_AnalyticalSolution_RigidDuct(x,omega,Ai,c0,U0));
%% III Compute order: loop
errL2 = zeros(length(meshfile),length(N)); % spatial L2 error
h = zeros(length(meshfile),1); % characteristic length
    for i=1:length(meshfile) % loop on mesh
            m = load_gmsh2(meshfile{i},[1 2]); % Load mesh
            mesh = Mesh_2D_Triangle(m.POS(:,1:2),m.LINES(:,[3,1,2]),m.TRIANGLES(:,[4,1,2,3]),m.physicalEntitiesIdx,m.physicalEntitiesName);
            h(i) = mean(mesh.charLength);
        for j=1:length(N) % loop on order
            DGCell = DGCell_2D_Triangle(N(j)); % create DG cell
            DGMesh = DGMesh_2D_Triangle(mesh,DGCell);
            [M,K,F,cst.DirectDelay,cst.BndOp,cst.BndOpDelay]=DG2D_LEE_buildGlobalFormulation(Ax_fun,Ay_fun,B_fun,DGMesh,zoneCase,cst.Impedance);
                % Solve in the frequency domain
            x_harm = DG2D_SolveFrequencyDomain(M,K,F,DGMesh,[Ai;0;Ai],omega,'DirectDelay',cst.DirectDelay,'LTIOp',cst.BndOp,'LTIOpDelayed',cst.BndOpDelay);
                % Compute exact solution
            xdg = DGMesh.getNodesCoordinates(); xdg = xdg(:,1);
            x_harm_ex = AnalyticalSolution(xdg);
                % Compute error
            errL2(i,j) = sqrt(mean((abs(x_harm(:)-x_harm_ex(:))).^2));

            clear M K C F x_harm x_harm_ex xdg ydg
        end
    end
h = h/h(1);
clear DGMesh DGCell mesh
%% IV Plot order result
figure
clf
hold on
leg = cell(0);
xlabel('log(h/h0) (mean characteristic length)');
ylabel('log eps_{L2}');
title(sprintf('Error plot. %s',CaseName));
for j=1:length(N)
    plot(log10(h),log10(errL2(:,j)),'-o');
    slope=0;
    for i=1:(length(h)-1) % find largest slope
        slope=max(slope,polyfit(log10(h(i:(i+1))),log10(errL2(i:(i+1),j)),1));
    end    
    leg{end+1}=sprintf('Max. slope = %1.2g (N=%d)',slope(1),N(j));
end
legend(leg);
%set(gca, 'DataAspectRatio', [1 1 1])
grid
%% Export order curves to csv file
for j=1:length(N)
    namestr = sprintf('ValidationOrderImpedanceTube_N=%d.csv',N(j));
    dlmwrite(namestr,'h,Error','Delimiter','');
    dlmwrite(namestr,[h,errL2(:,j)],'-append','Delimiter',',','newline','unix','precision','%1.6e');
end