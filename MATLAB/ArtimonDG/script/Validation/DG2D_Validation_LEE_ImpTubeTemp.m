%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - II LEE 2D (Acoustics)
%-------------------------------------------------------------------------
% Validation script.
%       Impedance tube case in the time domain.
%       Comparison with the 1D semi-analytical solution.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
run 'DG2D_LEELongitudinal_ZoneDefinition.m'
%% I Case definition: Impedance tube on [0,L] 
% Impedance boundary condition at x=L
L = 1; % length of impedance tube
    % Uniform base flow
cst.u0 = @(x,y)0; cst.du0dx = @(x,y)0; cst.du0dy = @(x,y)0;
cst.v0 = @(x,y)0; cst.dv0dx = @(x,y)0; cst.dv0dy = @(x,y)0;
    % Matrices
Ax_fun = @(x,y)Ax(cst.u0(x,y),cst.c0);
Ay_fun = @(x,y)Ay(cst.v0(x,y),cst.c0);
B_fun = @(x,y)B(cst.u0(x,y),cst.du0dx(x,y),cst.du0dy(x,y),cst.v0(x,y),cst.dv0dx(x,y),cst.dv0dy(x,y),cst.c0);
    % Source
tstop = L/cst.c0;
f_source = (pi/tstop)*1/(2*pi);
f = @(t)(sin(2*pi*f_source*t).*(t>=0).*(t<=(tstop))); % causal source
f = @(t)(AnF_bumpFunction(t,0,tstop));
tf = 4*L/cst.c0; % Final time
%% I IBC : arbitrary beta formulation
% beta(s) = 1 + h1(s) + exp(-s*delay)*h2(s),
% with hn(s) = Σ_k mun_k / (s+xin_k)  (Re(s)>0).
    % -- Definition of beta(s)
mu1 = [1,1];
xi1 = -[-50,-1];
delay = 1e-1;
mu2 = [1,1];
xi2 = -[-10+1i,-10-1i];
CaseName = sprintf('beta. xi_max=%d',max(abs([xi1(:);xi2(:)])));
    % -- Analytical solution
AnalyticalSolution = @(dt)LEE_AnalyticalSolution_ImpTube_Temp(f,mu1,xi1,delay,mu2,xi2,L,cst.c0,tf,dt,0);
dt = 0.1*L/cst.c0;
[u_ex,p_ex,t_ex] = AnalyticalSolution(dt);
    % -- DG: beta formulation
BndOp_op  = DiffusiveOperator(mu1,xi1,'Type','Standard');
BndOpDelay_op  = DiffusiveOperator(mu2,xi2,'Type','Standard');
cst.Impedance = ImpedanceBoundaryCondition('IBC',beta_fun(1),0,{0},{0},{1},{BndOp_op},{1},{BndOpDelay_op},{delay});
cst.Impedance.printImpedanceSummary();
        % Numerical flux function
IBC_flux = 'Impedance_FluxBeta';
%% I IBC: fractional polynomial (a) Analytical solution
% z(s) = a(1) +  a(2)*sqrt(s) + a(3)*s
% beta(s) = 1 + h(s), where h(s) = -2/(1+z(s)) has a diffusive
% representation without poles.
a = [1,1,0];
z = @(s)(a(1)+a(2)*sqrt(s)+a(3)*s);
h_an = @(s)(-2./(1+z(s)));
mu_an=@(xi)(((h_an(xi*exp(-1i*pi))-h_an(xi*exp(1i*pi)))/(2*1i*pi)));
        % Analytical solution
[xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(mu_an,20);
AnalyticalSolution = @(dt)LEE_AnalyticalSolution_ImpTube_Temp(f,mu,xi,0,0,0,L,cst.c0,tf,dt,0);
dt = 6/max(abs(xi));
[u_ex,p_ex,t_ex] = AnalyticalSolution(dt);
fprintf('max(u_ex)=%1.1e\n',max(u_ex));
%% I IBC: fractional polynomial (b) DG formulation (beta formulation)
Nxi = 4;
CaseName = sprintf('beta. Nxi=%d',Nxi);
[xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(mu_an,Nxi,'CoVParam',1.4);
BndOp_op  = DiffusiveOperator(mu,xi,'Laplace',h_an,'Type','Standard');
cst.Impedance = ImpedanceBoundaryCondition('beta',beta_fun(1),0,{0},{0},{1},{BndOp_op},{0},{0},{0});
cst.Impedance.printImpedanceSummary();
    % -- Choose numerical flux function
IBC_flux = 'Impedance_FluxBeta';
%% I IBC: fractional polynomial (b) DG formulation (z formulation)
Nxi = 2;
CaseName = sprintf('z. Nxi=%d',Nxi);
[xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(@(xi)(1./(pi*sqrt(xi))),Nxi);
BndOp_op  = DiffusiveOperator(mu,xi,'Laplace',@(s)(sqrt(s)),'Type','Extended');
cst.Impedance = ImpedanceBoundaryCondition('Z',beta_fun(a(1)),a(3),{0},{0},{a(2)},{BndOp_op},{0},{0},{0});
cst.Impedance.printImpedanceSummary();
    % -- Choose numerical flux function
%IBC_flux = 'Impedance_FluxZunstable';
IBC_flux = 'Impedance_FluxZ';
%% I IBC: fractional polynomial (b) DG formulation (zy formulation)
Nxi = 4;
CaseName = sprintf('zy formulation. Nxi=2x%d',Nxi);
[xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(@(xi)(1./(pi*sqrt(xi))),Nxi);
BndOp_op  = DiffusiveOperator(mu,xi,'Laplace',@(s)(sqrt(s)),'Type','Extended');
Impedance = ImpedanceBoundaryCondition('Z',beta_fun(a(1)),a(3),{0},{0},{a(2)},{BndOp_op},{0},{0},{0});
Impedance.printImpedanceSummary();
    % Admittance
y = @(s)(1./z(s));
mu_an=@(xi)(((y(xi*exp(-1i*pi))-y(xi*exp(1i*pi)))/(2*1i*pi)));
[xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(mu_an,Nxi);
BndOp_op  = DiffusiveOperator(mu,xi,'Laplace',y,'Type','Standard');
Admittance = ImpedanceBoundaryCondition('Y',beta_fun(0),0,{0},{0},{1},{BndOp_op},{0},{0},{0});
Admittance.printImpedanceSummary();
Merger = ImpedanceBoundaryCondition_ZY(Impedance,Admittance);
Merger.printImpedanceSummary();
cst.Impedance=Merger;
    % -- Choose numerical flux function
IBC_flux = 'Impedance_FluxZY';
%% II DG solution: user parameters
meshfile = 'Square32.msh';
N = 4; % order
CFL = 0.5; % CFL number
x_probe = 0; y_probe = 0.5; % probe location
% II DG solution: mesh loading and probe location
% Check that the probe is properly located
    % Read mesh
m = load_gmsh2(meshfile,[1 2]); % read lines (1) and triangles (2)
nodes = m.POS(:,1:2);
triangles = m.TRIANGLES(:,[4,1,2,3]);
boundFaces = m.LINES(:,[3,1,2]);
physicalEntitiesIdx = m.physicalEntitiesIdx;
physicalEntitiesName = m.physicalEntitiesName;
mesh = Mesh_2D_Triangle(nodes,boundFaces,triangles,physicalEntitiesIdx,physicalEntitiesName);
clear m nodes triangles boundFaces physicalEntitiesIdx physicalEntitiesName
    % Initialize DG cell & mesh
DGCell = DGCell_2D_Triangle(N); % cell order
DGMesh = DGMesh_2D_Triangle(mesh,DGCell);
    % Locate probe
k = DGMesh.getNearestNodeIndex([x_probe(:),y_probe(:)]);
    % indices in solution vector
idxVar = [1,3]; % Variable to observe (u and p/z0)
idxQ = zeros(length(idxVar),length(k));
for i=1:length(k)
    idxQ(:,i) = cst.Nq*(k(i)-1) + idxVar;
end
idx = idxQ(:);
% II DG solution: computation
zoneCase = DG2D_LEELongitudinal_createZone(struct('Interior',[5],'Source',[4],'Wall',[1,3],IBC_flux,[2]),cst,zone);
    % Build mass and stiffness: global formulation
[M,K,F,cst.DirectDelay,cst.BndOp,cst.BndOpDelay]=DG2D_LEE_buildGlobalFormulation(Ax_fun,Ay_fun,B_fun,DGMesh,zoneCase,cst.Impedance);
    % Time-domain solving: initial condition and source
dt = CFL*min(mesh.charLength)/norm(cst.c0); % Time step
    % Check characteristic time of boundary operators
fprintf('With RK 8-4 (C=7.7), conservative limit: |xi|<=%1.3g\n',0.3*7.7/dt);
cst.Impedance.printImpedanceSummary();
%% II DG solution: Time integration
    % Source definition
q_source = @(t)([f(t);zeros(size(t));f(t)]);
q0 = @(x)(zeros(cst.Nq*size(x,1),1));
nodes = DGMesh.getNodesCoordinates();
[x,t] = DG2D_SolveTimeDomain(M,K,F,DGMesh,cst.Nq,tf,dt,'nidx',idx,'x0',q0(nodes),'xs',q_source,'storeOnlyLast',0,'DirectDelay',cst.DirectDelay,'LTIOp',cst.BndOp,'LTIOpDelayed',cst.BndOpDelay,'Np',2,'Nk',3);
%[x,t] = DG2D_SolveTimeDomain(M,K,F,DGMesh,cst.Nq,tf,dt,'x0',q0(nodes),'xs',q_source,'storeOnlyLast',0,'DirectDelay',cst.DirectDelay,'LTIOp',cst.BndOp,'LTIOpDelayed',cst.BndOpDelay,'Np',2,'Nk',3);
    % Extract quantities of interest
u_DG = x(1,:);
p_DG = x(2,:);
%% Comparison plot
figure
subplot(2,1,1)
hold all
leg=cell(0);
plot(t,u_DG);
leg{end+1}=sprintf('DG (Order N=%d,CFL=%1.2g,%s) (%s)',N,CFL,meshfile,CaseName);
plot(t_ex,u_ex);
leg{end+1}=sprintf('Exact');
legend(leg)
title(sprintf('Imp. tube. L=%1.2g, c0=%1.2g. Probe at x=%1.2g',L,cst.c0,x_probe));
xlabel('t (s)');
ylabel('u');
subplot(2,1,2)
hold all
leg=cell(0);
plot(t,p_DG);
plot(t_ex,p_ex);
xlabel('t (s)');
ylabel('p/z0');