%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Validate Artimon DGmesh class, with Hesthaven code
% ---
% The purpose of this script is to compare the class DGMesh_2D_Triangle to
% the script provided by Hesthaven, for validation purposes.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Globals2D; % Global variables used by Hesthaven
N = 2; % Polynomial order used for approximation (Hesthaven)

%% Load mesh
nodes = h5read('/d/fmontegh/TIBC/Data/Mesh/mesh_Nodes.h5','/dset');
boundFaces = h5read('/d/fmontegh/TIBC/Data/Mesh/mesh_Faces.h5','/dset');
triangles = h5read('/d/fmontegh/TIBC/Data/Mesh/mesh_Triangles.h5','/dset');
Nv = length(nodes); % no. of vertices
K = length(triangles); % no. of triangles
VX = nodes(:,1)';
VY = nodes(:,2)';
EToV = triangles(:,2:end); % triangles

%% I - Hesthaven: Initialize solver and construct grid and metric
% The value of the metric terms are subject to innacuracy with the
% Hesthaven method (depends upon the value of N chosen).
StartUp2D;
vmapM = reshape(vmapM,Nfp,3,K);
vmapP = reshape(vmapP,Nfp,3,K);

%% II - Artimon: Initialize DG mesh
physicalEntitiesIdx = double(h5read('/d/fmontegh/TIBC/Data/Mesh/mesh_PhysicalEntities_idx.h5','/dset'));
fileID = fopen('/d/fmontegh/TIBC/Data/Mesh/mesh_PhysicalEntities_name.txt');
C = textscan(fileID,'%s','Delimiter','\n');
fclose(fileID);
physicalEntitiesName = C{1};
mesh = Mesh_2D_Triangle(nodes,boundFaces,triangles,physicalEntitiesIdx,physicalEntitiesName);
DGCell = DGCell_2D_Triangle(N);
DGMesh = DGMesh_2D_Triangle(mesh,DGCell);
%% Interior face connectivity
clc
if ~isequal(DGMesh.vmapM,vmapM)
    fprintf('Interior nodes connectivity mismatch.\n');
end
if ~isequal(DGMesh.vmapP,vmapP)
    fprintf('Exterior nodes connectivity mismatch.\n');
end
%% Interior face connectivity (display)
for k=1:K % for each element
    for l=1:3 % for each face
        kplus = mesh.EToE(k,l); lplus = mesh.EToF(k,l); % neighbor        
        fprintf('Triangle %d/%d connects to %d/%d\n',k,l,kplus,lplus);
        fprintf('Global index of interior: (A) %s\n',mat2str(DGMesh.vmapM(:,l,k)));
        fprintf('Global index of interior: (H) %s\n',mat2str(vmapM(:,l,k)));
        fprintf('Global index of exterior: (A) %s\n',mat2str(DGMesh.vmapP(:,l,k)));
        fprintf('Global index of exterior: (H) %s\n',mat2str(vmapP(:,l,k)));
    end
end
%% Boundary face connectivity
clc
vmapB = reshape(vmapB,Nfp,length(mesh.boundaryFaces));
    % (Beware that the boundary face numbering of Hesthaven is different
    % from Artimon's, hence the use of find.)
for i=1:length(mesh.boundaryFaces) % boundary face n°I
    k = mesh.FToE(i); l = mesh.FToF(i);
    nodeM = DGMesh.vmapM(:,l,k);
    iH = find(vmapB(1,:)==nodeM(1));
    if length(iH)~=1
        fprintf('Boundary face %d does not exist for Hesthaven\n',i);
    else
%         fprintf('Boundary face %d (%d for Hesthaven) connects to %d/%d\n',i,iH,k,l);
%         fprintf('Global index: (A) %s\n',mat2str(nodeM));
%         fprintf('Global index: (H) %s\n',mat2str(vmapB(:,iH)));
    end
end