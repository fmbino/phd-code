%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - II LEE 2D Longitudinal
%-------------------------------------------------------------------------
% The purpose of this script is to define:
%   - PDE: matrices Ax, Ay, B
%   - Numerical fluxes: f(n)
%   - Numerical zones. A numerical zone is what is called in gmsh 
% a 'PhysicalEntity'; it can be a boundary or a surface. The structure
% array 'zone' is design to enable easy definition of computational cases.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Utility function
beta_fun = @(z)((z-1)./(z+1)); % beta from z
z_fun = @(beta)((1+beta)./(1-beta)); % z from beta
z2beta = @(z)((z-1)./(z+1)); % beta from z
beta2z = @(beta)((1+beta)./(1-beta)); % z from beta
%% Constants
clear cst % structure that holds constants
cst.c0 = 1; % speed of sound
cst.z0 = 1; % air impedanc
cst.rho0 = cst.z0/cst.c0;
%% PDE definition
% q = [u,v,p/z0];
Ax = @(u0,c0)(...
    [u0,0,c0;
    0,u0,0;...
    c0,0,u0]);
Ay = @(v0,c0)(...
    [v0,0,0;...
    0,v0,c0;...
    0,c0,v0]);
B = @(u0,du0dx,du0dy,v0,dv0dx,dv0dy,c0)(...
    [-dv0dy,du0dy,u0*du0dx/c0+v0*du0dy/c0;...
    dv0dx,-du0dx,u0*dv0dx/c0+v0*dv0dy/c0;...
    0,0,0]);
cst.Nq = 3; % number of variables
%% Numerical flux function
% Subsonic and smooth base flow
% To define a zone, a numerical flux function is (usually) necessary.
    % -- FVS split
fout = @(n,u0,v0,c0)(max(0,dot([u0,v0],n))*[n(2)^2,-n(1)*n(2),0;-n(1)*n(2),n(1)^2,0;0,0,0] + (dot([u0,v0],n)+c0)/2*[n(1)^2,n(1)*n(2),n(1);n(1)*n(2),n(2)^2,n(2);n(1),n(2),1]);
    % -- Rigid wall
fwall = @(n,c0)((c0/2)*[2*n(1)^2,2*n(1)*n(2),2*n(1);2*n(1)*n(2),2*n(2)^2,2*n(2);0*n(1),0*n(2),0]);
%% Numerical flux function: Impedance
% Numerical flux function is expressed using ghost states.
    % Proportional impedance flux
f_Prop = @(n,c0,Ghost)((c0/2)*[zeros(2),n(:);n(:)',0]*(Ghost + eye(3)));
    % Derivative impedance flux
f_Deriv = @(n,c0,Ghost)((c0/2)*[zeros(2),n(:);n(:)',0]*Ghost);
    % Boundary operator impedance flux
f_BndOp = @(n,c0,Ghost)((c0/2)*[zeros(2),n(:);n(:)',0]*Ghost);
% --
    % Ghost state expression
TenN = @(n)kron(n(:),n(:)'); % Tensorial product: n(x)n
    % -- Unstable z formulation ('alpha=beta0' in Waves'17)
    % Split: z(s) = a0 + a1*s + ak*Qk(s)
    % coeff = [beta0] (reflection coefficient, beta0=1 for rigid wall)
Ghost_Zunstable_Prop = @(n,coeff)([-coeff(1)*TenN(n),(1-coeff(1))*n(:);(1+coeff(1))*n(:)',coeff(1)]);
    % coeff = [beta0,a1]
Ghost_Zunstable_Deriv = @(n,coeff)((1-coeff(1))*coeff(2)*[-eye(2),0*n(:);n(:)',0]);
    % coeff = [beta0,ak]
Ghost_Zunstable_BndOp = @(n,coeff)((1-coeff(1))*coeff(2)*[-n(:);1]); 
c_Zunstable_BndOp = @(n)([n(:);0]); % observation vector: BndOp(dot(c,q))
    % -- z-formulation ('alpha=-1' in Waves'17)
    % Split: z(s) = a0 + a1*s + ak*Qk(s)
    % coeff = [beta0]
Ghost_Z_Prop = @(n,coeff)([1*TenN(n),0*n(:);(2*z_fun(coeff(1)))*n(:)',-1]);
    % coeff = [a1]
Ghost_Z_Deriv = @(n,coeff)(coeff(1)*[0*eye(2),0*n(:);2*n(:)',0]);
    % coeff = [ak]
Ghost_Z_BndOp = @(n,coeff)(coeff(1)*[0*n(:);2]);
c_Z_BndOp = @(n)([n(:);0]);
    % -- y-formulation

    % -- zy-formulation
        % coeff = [a0,b0]
Ghost_ZY_Prop = @(n,coeff)([0*TenN(n),coeff(2)*n(:);coeff(1)*n(:)',0]);
        % coeff = [a1,b1]
Ghost_ZY_Deriv = @(n,coeff)([0*TenN(n),coeff(2)*n(:);coeff(1)*n(:)',0]);
        % coeff = [ak]
Ghost_ZY_BndOpZ = @(n,coeff)(coeff(1)*[0*n(:);1]);
c_ZY_BndOpZ = @(n)([n(:);0]);
        % coeff = [bk]
Ghost_ZY_BndOpY = @(n,coeff)(coeff(1)*[1*n(:);0]);
c_ZY_BndOpY = @(n)([0*n(:);1]);
    % -- beta-formulation
    % Split: beta(s) = a0 + a1*s + ak*Qk(s)
    % coeff = [a0]
Ghost_Beta_Prop = @(n,coeff)([-coeff(1)*TenN(n),(1-coeff(1))*n(:);(1+coeff(1))*n(:)',coeff(1)]);
    % coeff = [a1]
Ghost_Beta_Deriv = @(n,coeff)(coeff(1)*[-1*TenN(n),-1*n(:);1*n(:)',1]);
    % coeff = [ak]
Ghost_Beta_BndOp = @(n,coeff)(coeff(1)*[-n(:);1]);
c_Beta_BndOp = @(n)([n(:);1]); % observation vector BndOp(p+u.n) BEWARE OF DIMENSION ("p+un")
fprintf('Numerical flux function: beta-formulation.\n');
%% Zone definition
    % Arguments for zone definition
arg_UpwindFVS = @(u0,v0,c0)NumericalFlux_2D_UpwindFVS(@(n,x,y)(fout(n,u0(x,y),v0(x,y),c0)));
arg_Impedance_Rigid = @(c0)NumericalFlux_2D_Generic(@(n)(fwall(n,c0)));
    % 'zone.zoneName' is a structure array that describes 'zoneName'
    % idx: index of Physical entity
clear zone
zone.PDEName = 'LEE 2D Longitudinal';
zone.Interior_UpwindFVS = @(idx,u0,v0,c0)struct('name','Interior_UpwindFVS','secondaryName','','physicalEntity',idx,'arg',arg_UpwindFVS(u0,v0,c0));
zone.Outlet_UpwindFVS = @(idx,u0,v0,c0)struct('name','Outlet_UpwindFVS','secondaryName','','physicalEntity',idx,'arg',arg_UpwindFVS(u0,v0,c0));
zone.Inlet_UpwindFVS = @(idx,u0,v0,c0)struct('name','Inlet_UpwindFVS','secondaryName','','physicalEntity',idx,'arg',arg_UpwindFVS(u0,v0,c0));
zone.Impedance_Rigid = @(idx,c0)struct('name','Boundary_Generic_Proportional','secondaryName','','physicalEntity',idx,'arg',arg_Impedance_Rigid(c0));
%% Zone definition: impedance
        % -- unstable formulation
        % coeff = [beta0]
zone.Impedance_Zunstable_Prop = @(idx,coeff,c0)struct('arg',NumericalFlux_2D_Generic(@(n,x,y)(f_Prop(n,c0,Ghost_Zunstable_Prop(n,coeff(x,y))))),'name','Boundary_Generic_Proportional','secondaryName','','physicalEntity',idx);
        % coeff = [beta0,a1]
zone.Impedance_Zunstable_Deriv = @(idx,coeff,c0)struct('arg',NumericalFlux_2D_Generic(@(n,x,y)(f_Deriv(n,c0,Ghost_Zunstable_Deriv(n,coeff(x,y))))),'name','Boundary_Generic_Derivative','secondaryName','','physicalEntity',idx);
        % coeff = [beta0,ak]
zone.Impedance_Zunstable_BndOp = @(idx,coeff,c0,secondaryName)struct('arg',NumericalFlux_2D_Generic_Operator(@(n,x,y)(f_BndOp(n,c0,Ghost_Zunstable_BndOp(n,coeff(x,y)))),c_Zunstable_BndOp),'name','Boundary_Generic_Operator','secondaryName',secondaryName,'physicalEntity',idx);
        % -- z formulation
        % coeff = [beta0]
zone.Impedance_Z_Prop = @(idx,coeff,c0)struct('arg',NumericalFlux_2D_Generic(@(n,x,y)(f_Prop(n,c0,Ghost_Z_Prop(n,coeff(x,y))))),'name','Boundary_Generic_Proportional','secondaryName','','physicalEntity',idx);
        % coeff = [beta0,a1]
zone.Impedance_Z_Deriv = @(idx,coeff,c0)struct('arg',NumericalFlux_2D_Generic(@(n,x,y)(f_Deriv(n,c0,Ghost_Z_Deriv(n,coeff(x,y))))),'name','Boundary_Generic_Derivative','secondaryName','','physicalEntity',idx);
        % coeff = [beta0,ak]
zone.Impedance_Z_BndOp = @(idx,coeff,c0,secondaryName)struct('arg',NumericalFlux_2D_Generic_Operator(@(n,x,y)(f_BndOp(n,c0,Ghost_Z_BndOp(n,coeff(x,y)))),c_Z_BndOp),'name','Boundary_Generic_Operator','secondaryName',secondaryName,'physicalEntity',idx);
        % -- y formulation
        
        % -- zy-formulation
zone.Impedance_ZY_Prop = @(idx,coeff,c0)struct('arg',NumericalFlux_2D_Generic(@(n,x,y)f_Prop(n,c0,Ghost_ZY_Prop(n,coeff(x,y)))),'name','Boundary_Generic_Proportional','secondaryName','','physicalEntity',idx);
zone.Impedance_ZY_Deriv = @(idx,coeff,c0)struct('arg',NumericalFlux_2D_Generic(@(n,x,y)f_Deriv(n,c0,Ghost_ZY_Deriv(n,coeff(x,y)))),'name','Boundary_Generic_Derivative','secondaryName','','physicalEntity',idx);
zone.Impedance_ZY_BndOpZ = @(idx,coeff,c0,secondaryName)struct('arg',NumericalFlux_2D_Generic_Operator(@(n,x,y)f_BndOp(n,c0,Ghost_ZY_BndOpZ(n,coeff(x,y))),c_ZY_BndOpZ),'name','Boundary_Generic_Operator','secondaryName',secondaryName,'physicalEntity',idx);
zone.Impedance_ZY_BndOpY = @(idx,coeff,c0,secondaryName)struct('arg',NumericalFlux_2D_Generic_Operator(@(n,x,y)f_BndOp(n,c0,Ghost_ZY_BndOpY(n,coeff(x,y))),c_ZY_BndOpY),'name','Boundary_Generic_Operator','secondaryName',secondaryName,'physicalEntity',idx);
        % -- beta formulation
        % coeff = []
zone.Impedance_Beta_Prop = @(idx,coeff,c0)struct('arg',NumericalFlux_2D_Generic(@(n,x,y)(f_Prop(n,c0,Ghost_Beta_Prop(n,coeff(x,y))))),'name','Boundary_Generic_Proportional','secondaryName','','physicalEntity',idx);
        % coeff = []
zone.Impedance_Beta_Deriv = @(idx,coeff,c0)struct('arg',NumericalFlux_2D_Generic(@(n,x,y)(f_Deriv(n,c0,Ghost_Beta_Deriv(n,coeff(x,y))))),'name','Boundary_Generic_Derivative','secondaryName','','physicalEntity',idx);
        % coeff = []
zone.Impedance_Beta_BndOp = @(idx,coeff,c0,secondaryName)struct('arg',NumericalFlux_2D_Generic_Operator(@(n,x,y)(f_BndOp(n,c0,Ghost_Beta_BndOp(n,coeff(x,y)))),c_Beta_BndOp),'name','Boundary_Generic_Operator','secondaryName',secondaryName,'physicalEntity',idx);
    % Clean workspace
clear arg_UpwindFVS arg_Impedance_Rigid arg_Impedance_Prop arg_Impedance_Der arg_Impedance_Operator