%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Validate Artimon mesh class, with Hesthaven code
% ---
% The purpose of this script is to compare the class Mesh_2D_Triangle to
% the script provided by Hesthaven, for validation purposes.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Globals2D; % Global variables used by Hesthaven
N = 2; % Polynomial order used for approximation (Hesthaven)

%% Load mesh
nodes = h5read('/d/fmontegh/TIBC/Data/Mesh/mesh_Nodes.h5','/dset');
boundFaces = h5read('/d/fmontegh/TIBC/Data/Mesh/mesh_Faces.h5','/dset');
triangles = h5read('/d/fmontegh/TIBC/Data/Mesh/mesh_Triangles.h5','/dset');
Nv = length(nodes); % no. of vertices
K = length(triangles); % no. of triangles
VX = nodes(:,1)';
VY = nodes(:,2)';
EToV = triangles(:,2:end); % triangles

%% I - Hesthaven: Initialize solver and construct grid and metric
% The value of the metric terms are subject to innacuracy with the
% Hesthaven method (depends upon the value of N chosen).
StartUp2D;
    % Reshape metric terms
J = J(1,:)';
rx = rx(1,:)';
ry = ry(1,:)';
sx = sx(1,:)';
sy = sy(1,:)';
charLength = dtscale2D;
    % unit normal at each face
n(:,:,1) = [nx(1,:)',ny(1,:)'];
n(:,:,2) = [nx(1+Nfp,:)',ny(1+Nfp,:)'];
n(:,:,3) = [nx(1+2*Nfp,:)',ny(1+2*Nfp,:)'];
    % "Surface Jacobian" at each face
edJ(:,1) = sJ(1,:)';
edJ(:,2) = sJ(1+Nfp,:)';
edJ(:,3) = sJ(1+2*Nfp,:)';
    % Ratio of surface to volume jacobian
Fscale_new = [Fscale(1,:)',Fscale(1+Nfp,:)',Fscale(1+2*Nfp,:)'];
Fscale_other = [edJ(:,1)./J,edJ(:,2)./J,edJ(:,3)./J]; % not the same !
%% II - Artimon: Construct metric
physicalEntitiesIdx = double(h5read('/d/fmontegh/TIBC/Data/Mesh/mesh_PhysicalEntities_idx.h5','/dset'));
fileID = fopen('/d/fmontegh/TIBC/Data/Mesh/mesh_PhysicalEntities_name.txt');
C = textscan(fileID,'%s','Delimiter','\n');
fclose(fileID);
physicalEntitiesName = C{1};
mesh = Mesh_2D_Triangle(nodes,boundFaces,triangles,physicalEntitiesIdx,physicalEntitiesName);
mesh.show();
%% Compare
fprintf('Comparison Artimon and Hesthaven (N=%d)',N);
    % Metric
fprintf('Jacobian: %1.1e\n',max(abs(mesh.J-J)));
fprintf('rx: %1.1e\n',max(abs(mesh.rx-rx)));
fprintf('ry: %1.1e\n',max(abs(mesh.ry-ry)));
fprintf('sx: %1.1e\n',max(abs(mesh.sx-sx)));
fprintf('sy: %1.1e\n',max(abs(mesh.sy-sy)));
fprintf('char. length: %1.1e\n',max(abs(mesh.charLength-charLength)));
fprintf('edge Jacobian (face 1): %1.1e\n',max(abs(mesh.edJ(:,1)-edJ(:,1))));
fprintf('edge Jacobian (face 2): %1.1e\n',max(abs(mesh.edJ(:,2)-edJ(:,2))));
fprintf('edge Jacobian (face 3): %1.1e\n',max(abs(mesh.edJ(:,3)-edJ(:,3))));
fprintf('edge length vs edge Jacobian (face 1): %1.1e\n',max(abs(mesh.edL(:,1)/2-edJ(:,1))));
fprintf('edge length vs edge Jacobian (face 2): %1.1e\n',max(abs(mesh.edL(:,2)/2-edJ(:,2))));
fprintf('edge length vs edge Jacobian (face 3): %1.1e\n',max(abs(mesh.edL(:,3)/2-edJ(:,3))));
fprintf('normal face 1: %1.1e\n',max(max(abs(mesh.n(:,:,1)-n(:,:,1)))));
fprintf('normal face 2: %1.1e\n',max(max(abs(mesh.n(:,:,2)-n(:,:,2)))));
fprintf('normal face 3: %1.1e\n',max(max(abs(mesh.n(:,:,3)-n(:,:,3)))));
fprintf('EToE connectivity : %1.1e\n',max(max(abs(mesh.EToE-EToE))));
fprintf('EToF connectivity : %1.1e\n',max(max(abs(mesh.EToF-EToF))));