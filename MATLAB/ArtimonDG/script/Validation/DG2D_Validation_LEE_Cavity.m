%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon DG 2D - II LEE 2D (Acoustics)
%-------------------------------------------------------------------------
% Validation script.
% Acoustical cavity using longitudinal LEE.
% Three parts:
%   I PDE and impedance definition (common to I and II)
%   II Comparison between computed spectrum and dispersion relation.
%   (Optional: plot mode.)
%   III Order validation on one mode.
% Rmk: II and III are independent of each other.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
run 'DG2D_LEELongitudinal_ZoneDefinition.m'
    % Analytical expression of pressure mode
g = @(x,k,z,Omega,rho0,omega)(-(1+k*z(1i*omega)/(rho0*Omega))*exp(1i*k*x) +(1-k*z(1i*omega)/(rho0*Omega))*exp(-1i*k*x));
dg = @(x,k,z,Omega,rho0,omega)(-1i*k*(1+k*z(1i*omega)/(rho0*Omega))*exp(1i*k*x) -1i*k*(1-k*z(1i*omega)/(rho0*Omega))*exp(-1i*k*x));
p = @(x,kx,ky,z,Omega,rho0,omega)g(x(:,1),kx,z,Omega,rho0,omega).*g(x(:,2),ky,z,Omega,rho0,omega);
dpdx = @(x,kx,ky,z,Omega,rho0,omega)dg(x(:,1),kx,z,Omega,rho0,omega).*g(x(:,2),ky,z,Omega,rho0,omega);
dpdy = @(x,kx,ky,z,Omega,rho0,omega)g(x(:,1),kx,z,Omega,rho0,omega).*dg(x(:,2),ky,z,Omega,rho0,omega);
%% I PDE definition: longitudinal LEE
% Acoustical cavity on (0,a)x(0,b)
% Impedance boundary condition on four boundaries.
    % -- Case definition
L = 1; % length of impedance tube
    % Uniform base flow
cst.u0 = @(x,y)0; cst.du0dx = @(x,y)0; cst.du0dy = @(x,y)0;
cst.v0 = @(x,y)0; cst.dv0dx = @(x,y)0; cst.dv0dy = @(x,y)0;
    % Matrices
Ax_fun = @(x,y)Ax(cst.u0(x,y),cst.c0);
Ay_fun = @(x,y)Ay(cst.v0(x,y),cst.c0);
B_fun = @(x,y)B(cst.u0(x,y),cst.du0dx(x,y),cst.du0dy(x,y),cst.v0(x,y),cst.dv0dx(x,y),cst.dv0dy(x,y),cst.c0);
%% I Impedance boundary condition: z formulation
% z = a0 + a1*s + a * exp(-tau*s) + a*Q1(s) + a*Q2(s)*exp(-tau*s)
% Three operators: Direct delay, LTI and LTI delayed.
    % -- Proportional derivative
    % beta0 and a1
a0 = 0;
beta0 = beta_fun(a0);
%beta0 = 1;
a1 = 0;
    % -- Direct delay
DD_coeff = 0*a0/2;
DD_delay = 1e-1;
    % -- Boundary operator - Undelayed
BndOp_coeff = 1;
mu = [1,1]; xi = -[-100+1i,-100-1i];
[xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(@(xi)(1./(pi*sqrt(xi))),20);
BndOp_op  = DiffusiveOperator(mu,xi,'Laplace',@(s)(sqrt(s)),'Type','Extended');
    % -- Boundary operator - Delayed
BndOpDelay_coeff = 0;
BndOpDelay_op = BndOp_op;
BndOpDelay_delay = 1e-15;
    % -- Create impedance object
z = @(s)(a0+a1*s+DD_coeff*exp(-DD_delay*s)+BndOp_coeff*BndOp_op.Laplace_ex(s) + BndOpDelay_coeff*BndOpDelay_op.Laplace_ex(s).*exp(-BndOpDelay_delay*s));
cst.Impedance = ImpedanceBoundaryCondition('CT57',beta0,a1,{DD_coeff},{DD_delay},{BndOp_coeff},{BndOp_op},{BndOpDelay_coeff},{BndOpDelay_op},{BndOpDelay_delay},'Laplace',z);
cst.Impedance.printImpedanceSummary()
clear a0 beta0 a1 DD_coeff DD_delay BndOp_coeff BndOp_op  
clear BndOpDelay_coeff BndOpDelay_op BndOpDelay_delay
    % -- Choose numerical flux function
%IBC_flux = 'Impedance_FluxZunstable';
IBC_flux = 'Impedance_FluxZ';
%% Impedance boundary condition (ZY formulation)
% z(s) = a(1) +  a(2)*sqrt(s) + a(3)*s
% y(s) has a diffusive representation, without poles.
a = [0.1,1,0.1];
z = @(s)(a(1) + a(2)*sqrt(s)+a(3)*s);
    % Impedance
[xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(@(xi)(1./(pi*sqrt(xi))),10);
BndOp_op  = DiffusiveOperator(mu,xi,'Laplace',@(s)(sqrt(s)),'Type','Extended');
Impedance = ImpedanceBoundaryCondition('Z',beta_fun(a(1)),a(3),{0},{0},{a(2)},{BndOp_op},{0},{0},{0});
Impedance.printImpedanceSummary();
    % Admittance
y = @(s)(1./z(s));
mu_an=@(xi)(((y(xi*exp(-1i*pi))-y(xi*exp(1i*pi)))/(2*1i*pi)));
[xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(mu_an,10);
BndOp_op  = DiffusiveOperator(mu,xi,'Laplace',y,'Type','Standard');
Admittance = ImpedanceBoundaryCondition('Y',beta_fun(0),0,{0},{0},{1},{BndOp_op},{0},{0},{0});
Admittance.printImpedanceSummary();
Merger = ImpedanceBoundaryCondition_ZY(Impedance,Admittance);
Merger.printImpedanceSummary();
cst.Impedance=Merger;
    % -- Choose numerical flux function
IBC_flux = 'Impedance_FluxZY';
%% I Impedance boundary condition: beta formulation
% z(s) = a(1) +  a(2)*sqrt(s) + a(3)*s
% beta(s) = 1 + h(s), where h(s) = -2/(1+z(s)) has a diffusive
% representation without poles.
a = [1,1,0.1];
z = @(s)(a(1) + a(2)*sqrt(s)+a(3)*s);
    % Reflection coefficient
h = @(s)(-2./(1+z(s)));
mu_an=@(xi)(((h(xi*exp(-1i*pi))-h(xi*exp(1i*pi)))/(2*1i*pi)));
[xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(mu_an,20);
BndOp_op  = DiffusiveOperator(mu,xi,'Laplace',h,'Type','Standard');
cst.Impedance = ImpedanceBoundaryCondition('beta',beta_fun(1),0,{0},{0},{1},{BndOp_op},{0},{0},{0});
cst.Impedance.printImpedanceSummary();
    % -- Choose numerical flux function
IBC_flux = 'Impedance_FluxBeta';
%% Impedance boundary condition: beta formulation
% z(s) = infinity (hard wall)
% beta(s) = 1
cst.Impedance = ImpedanceBoundaryCondition('beta',beta_fun(1),0,{0},{0},{0},{0},{0},{0},{0});
cst.Impedance.printImpedanceSummary();
    % -- Choose numerical flux function
IBC_flux = 'Impedance_FluxBeta';
%% II Spectral analysis: Build DG formulation
    % user parameter
meshfile = 'Square32.msh';
N = 4; % order
    % Read mesh
m = load_gmsh2(meshfile,[1 2]); % read lines (1) and triangles (2)
mesh = Mesh_2D_Triangle( m.POS(:,1:2),m.LINES(:,[3,1,2]),m.TRIANGLES(:,[4,1,2,3]),m.physicalEntitiesIdx,m.physicalEntitiesName);
clear m
    % Initialize DG cell & mesh
DGCell = DGCell_2D_Triangle(N); % cell order
DGMesh = DGMesh_2D_Triangle(mesh,DGCell);    % Define computational case
    % Longitudinal: soft-wall cavity
zoneCase = DG2D_LEELongitudinal_createZone(struct('Interior',[5],IBC_flux,[1,2,3,4]),cst,zone);
    % Build mass and stiffness: global formulation
[M,K,F,cst.DirectDelay,cst.BndOp,cst.BndOpDelay]=DG2D_LEE_buildGlobalFormulation(Ax_fun,Ay_fun,B_fun,DGMesh,zoneCase,cst.Impedance);
    % Build coupled system
A = DG2D_buildParabolicHyperbolicRealisation(M,K,DGMesh,cst.Nq,'DirectDelay',cst.DirectDelay,'LTIOp',cst.BndOp,'LTIOpDelayed',cst.BndOpDelay,'Np',2,'Nk',2);
%% II Spectral analysis: Solve dispersion relation (Soft wall)
    % Soft wall
k0 = -50:pi:50; % starting values
[omega,k_cs] = dispRel_RectDuctAcoustics(mesh.bound(2,1),mesh.bound(2,2),cst.c0,0,0,cst.rho0,z,k0);
%% II Spectral analysis: Solve dispersion relation (Hard wall)
    % Hard wall
omega = dispRel_RectHardDuctAcoustics(6,6,0,0,cst.c0,mesh.bound(2,1),mesh.bound(2,2));
%% II Spectral analysis: Compute and plot spectrum
% Play with initial guess and tolerance to locate spectrum
clear opt
opt.isreal = 1;
opt.tol = 1e-16;
opt.maxit = 100;
opt.disp = 2;
tic
%R = eigs(A,10,10*1i,opt);
R = eig(full(A));
toc
Rm = find(real(R(:))>0);
figure
clf
hold all
plot(real(R),imag(R),'o');
plot(real(1i*omega),imag(1i*omega),'x');
title(sprintf('Spectrum (Max:%1.2g,Min:%1.4g)',max(real(R(Rm))),max(abs(R(:)))));
xlabel('Real(Lambda)');
ylabel('Imag(Lambda)');
axis([-4,0.1,-10,10])
%plot(0*ylim,ylim,'--');
%plot(xlim,0*xlim,'--');
%% II Spectral analysis: compute mode
    % choose [omega,kx,ky]
n = 21;
w = omega(n); kx = k_cs(n,1); ky = k_cs(n,2);
Omega = cst.c0*sqrt(kx^2+ky^2);
    % set up q0
f = @(x)[dpdx(x,kx,ky,z,Omega,cst.rho0,w)/(-1i*cst.rho0*Omega),dpdy(x,kx,ky,z,Omega,cst.rho0,w)/(-1i*cst.rho0*Omega),p(x,kx,ky,z,Omega,cst.rho0,w)];
q0 = @(x)(reshape(transpose(f(x)),cst.Nq*size(x,1),1));
    % Harmonic solving
nodes = DGMesh.getNodesCoordinates();
x_harm = DG2D_SolveFrequencyDomain(M,K,M,DGMesh,q0(nodes),w);
x_harm = x_harm/max(x_harm);
    % Exact mode
x_harm_ex = q0(nodes);
x_harm_ex=x_harm_ex/max(x_harm_ex); 
%% II Spectral analysis: plot solution
    % Choose field to plot
x_plot = real(DGMesh.interpolateField2PlotNodes(cst.Nq,x_harm_ex));
%x_plot = real(DGMesh.interpolateField2PlotNodes(cst.Nq,x_harm));
idx_r = cst.Nq*(0:(DGCell.Np*mesh.N-1));
    % get mesh plot
[trianglesPlot, nodesPlot] = DGMesh.buildPlotMesh();
% --
figure
clf
subplot(2,2,1)
trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),x_plot(1 + idx_r),'EdgeColor','None');
xlabel('y');
ylabel('z');
colorbar
view([0,90]);
title(sprintf('u (max: %1.2g)\n(u0=%1.2g,k=%1.2g,n=%d,m=%d,omega=%1.2g)\nNp*Nk=%d',max(x_plot(1 + idx_r)),0,0,0,0,w,DGCell.Np*mesh.N));
subplot(2,2,2)
trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),x_plot(2 + idx_r),'EdgeColor','None');
xlabel('y');
ylabel('z');
title(sprintf('v (max: %1.2g)',max(x_plot(2 + idx_r))));
colorbar
view([0,90]);
subplot(2,2,3)
trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),x_plot(3 + idx_r),'EdgeColor','None');
xlabel('y');
ylabel('z');
title(sprintf('p/z0 (max: %1.2g)',max(x_plot(3 + idx_r))));
colorbar
view([0,90]);
%% III Compute order: user input
% Beware: the computed order is very sensible to the accuracy of the
% computed root of the dispersion relation.
N = [2,4,6]; % target order
meshfile = {'Square2.msh','Square8.msh','Square18.msh','Square32.msh','Square50.msh','Square220.msh'}; % coarse -> refined
%meshfile = {'Square8.msh','Square18.msh','Square32.msh','Square50.msh'}; % coarse -> refined
    % mode to consider
        % Soft wall
k0 = 0:pi:15; % starting values
[omega,k_cs] = dispRel_RectDuctAcoustics(1,1,cst.c0,0,0,cst.rho0,z,k0,'tol',1e-13);
        % choose [omega,kx,ky]
n = 1;
w = omega(n); kx = k_cs(n,1); ky = k_cs(n,2);
Omega = cst.c0*sqrt(kx^2+ky^2);
        % set up q0
f = @(x)[dpdx(x,kx,ky,z,Omega,cst.rho0,w)/(-1i*cst.rho0*Omega),dpdy(x,kx,ky,z,Omega,cst.rho0,w)/(-1i*cst.rho0*Omega),p(x,kx,ky,z,Omega,cst.rho0,w)];
q0 = @(x)(reshape(transpose(f(x)),cst.Nq*size(x,1),1));
%% III Compute order: loop
errL2 = zeros(length(meshfile),length(N)); % spatial L2 error
h = zeros(length(meshfile),1); % characteristic length
run 'DG2D_LEELongitudinal_ZoneDefinition.m'
    % Longitudinal: soft-wall cavity
zoneCase = DG2D_LEELongitudinal_createZone('SoftWallCavity',struct('Interior',[5],IBC_flux,[1,2,3,4]),cst,zone);
    for i=1:length(meshfile) % loop on mesh
            m = load_gmsh2(meshfile{i},[1 2]); % Load mesh
            mesh = Mesh_2D_Triangle(m.POS(:,1:2),m.LINES(:,[3,1,2]),m.TRIANGLES(:,[4,1,2,3]),m.physicalEntitiesIdx,m.physicalEntitiesName);
            h(i) = mean(mesh.charLength);
        for j=1:length(N) % loop on order
            DGCell = DGCell_2D_Triangle(N(j)); % create DG cell
            DGMesh = DGMesh_2D_Triangle(mesh,DGCell);
            [M,K,F,cst.DirectDelay,cst.BndOp,cst.BndOpDelay]=DG2D_LEE_buildGlobalFormulation(Ax_fun,Ay_fun,B_fun,DGMesh,zoneCase,cst.Impedance);
            idx_r = cst.Nq*(0:(DGCell.Np*mesh.N-1));
                % Compute exact solution
            nodes = DGMesh.getNodesCoordinates();
            x_harm_ex = q0(nodes);
                % Extract normalized pressure
            x_harm_ex = real(x_harm_ex(3+idx_r));
            x_harm_ex = x_harm_ex/max(x_harm_ex);
                % Solve in the frequency domain
            x_harm = DG2D_SolveFrequencyDomain(M,K,M,DGMesh,q0(nodes),w,'DirectDelay',cst.DirectDelay,'LTIOp',cst.BndOp,'LTIOpDelayed',cst.BndOpDelay);
            x_harm = real(x_harm(3+idx_r));
            x_harm = x_harm/max(x_harm);
                % Compute error (beware of normalization)
            errL2(i,j) = sqrt(mean((real(x_harm(:)-x_harm_ex(:))).^2));
            clear M K C F x_harm x_harm_ex xdg ydg
        end
    end
h = h/h(1);
clear DGMesh DGCell mesh
%% IV Plot order result
figure
clf
hold on
leg = cell(0);
xlabel('log(h/h0) (mean characteristic length)');
ylabel('log eps_{L2}');
title(sprintf('Error plot.'));
for j=1:length(N)
    plot(log10(h),log10(errL2(:,j)),'-o');
    slope=0;
    for i=1:(length(h)-1) % find largest slope
        slope=max(slope,polyfit(log10(h(i:(i+1))),log10(errL2(i:(i+1),j)),1));
    end    
    leg{end+1}=sprintf('Max. slope = %1.2g (N=%d)',slope(1),N(j));
end
legend(leg);
%set(gca, 'DataAspectRatio', [1 1 1])
grid