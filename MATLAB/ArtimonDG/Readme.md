
This folder contains MATLAB code related to Artimon DG.

# What is Artimon DG?
Artimon DG is a 2D DG solver; the employed variational formulation is described in the provided manual. It consists of two components:

**(1)** Classes that describes the computational domain (mesh and cell)

- `Mesh_2D_Triangle.m`: Describes a 2D triangular mesh.
- `DGCell_2D_Triangle`: Describes a 2D DG Cell.
- `DGMesh_2D_Triangle`: Describes the union of `Mesh_2D_Triangle` and `DGCell_2D_Triangle`.

The job of these classes is mainly to handle the dirty connectivity work, so that everything related to connectivity is done in these classes. Methods are provided that enable to check and display them.
    
    
**(2)** Assembly functions, stored in `fun/Assembly`, which compute the global matrix formulation from the knowledge of `DGMesh_2D_Triangle` and the PDE at hand.
    
The components (1) and (2) are the only needed components to build a global DG formulation that can be advanced in time using a Runge-Kutta.
    
For convenience, a lot of additional functions are also provided to build the formulation for the LEEs. Basically, these functions are wrapper that call the assembly functions (2). Functions for time-domain integration are also provided. Hence, scripts that use Artimon for the LEEs relies on these functions.

However, these functions do not belong to the core of Artimon, which is (1) and (2). A user that wants to adapt or extend the solver will typically build its own functions around (1) and (2) and discard the rest.

# Folder structure
    
`class`: This folder contains classes. The core classes of Artimon have been listed above. Classes that contain `1D` enable to do a 1D DG that have not been used during the thesis; however, they are not as clean as Artimon DG and can be ignored. The need to experiment with TDIBCs during the thesis has lead to defining various classes to describe Numerical Fluxes or IBCs for convenience. Their use is demonstrated in some of the scripts.

`doc`: Contains a document that describes the formulation used by Artimon, provided in PDF and source format.

`fun`: Contains various functions related to DG. Most of them are utility functions with a very limited use. As highlighted above when describing the core of Artimon, the most important functions are the assembly functions gathered in `fun/Assembly`.
    
Note that, typically, a user script will use just a few files in `class` and `fun`: the core of Artimon (defined above) as well as some utility functions that depends on the PDE considered.

`script`: This folder contains scripts that actually use Artimon to do stuff. See the corresponding `Readme` for a detail of the files. The typical use is split into three steps:

**(a) Build mesh.** This mandatory step is common to all scripts.

Use `DG2D_ReadMesh.m` to load and display the mesh. The methods of the class `Mesh_2D_Triangle`, `DGCell_2D_Triangle`, and `DGMesh_2D_Triangle` are showcased in this script. They are useful to check that the mesh is allright.

**(b) Build global formulation.** This mandatory step is to be tailored to each PDE.


Now that we have a mesh, it remains to use the assembly functions (`fun/Assembly`) to actually build a global formulation corresponding to our PDE. Three PDEs are covered in the provided scripts.

- (i) Transport equation. `DG2D_Transport.m` is the simplest of example, it demonstrates the use of Artimon for a 2D transport equation.
- (ii) Longitudinal LEEs, in most `DG2D_LEE*` files.
- (iii) CrossSection LEEs, in `DG2D_CrossSection*` files. These have not really been used a lot in the thesis.

**(c) Computation of the solution.** This mandatory step is to be tailored to each PDE.

Once the global formulation is known, we have a finite-dimensional ODE on our hands. Provided that we know an initial condition and (if applicable) a source, let's solve it.

- (i) Time-domain solving: use any RK scheme for instance. Some functions are provided in `fun` to perform a time-domain integration, such as `DG2D_SolveTimeDomain`; they are essentially wrapper around a RK integration function. The function `DG2D_inverseMassMatrix` can be called to inverse the mass matrix using its block diagonal structure.
- (ii) Time-harmonic solving: replace the derivative by a multiplication and enjoy your matrix inversion.

**(d) Post-processing].** This optional step can be common to all scripts.

Once the solution has been computed, it can be displayed and analysed. The script `DG2D_PlotVideo` shows example of use of posprocessing functions.

Remark: For some LEEs, the step (b) is very lengthy. This is only due to the fact that, during the thesis, the scripts have been used to experiment various formulation of TDIBCs. As a result, they can be cut-down considerably, for instance if only the reflection coefficient is considered.

# Get started

1. Read manual to understand the formulation used by Artimon.
2. Run and understand a simple script like `DG2D_Transport`.
3. Switch to the PDE of interest.