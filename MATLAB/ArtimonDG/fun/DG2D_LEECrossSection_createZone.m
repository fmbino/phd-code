function zoneCase = DG2D_LEECrossSection_createZone(casename,id,cst,zone)
%DG2D_LEELongitudinal_createZone Create the cell 'zoneCase' that describes
%a computational case for the LEE cross-section.
% Inputs:
%   casename (string)   Name of case. Accepted:
%       - 'HardWallCavity'
%       - 'SoftWallCavity'
%   id (struct)         Numerical identifier of each zone. Fields:
%       - 'Interior'
%       - 'Wall'
%       - 'Impedance'
%   cst (struct)        Constants needed to build the case.
%   zone (struct)       Available zones.
% Output:
%   zoneCase (cell)

        % -- Validate attributes
    validateattributes(casename,{'char'},{},mfilename,'casename');
    validateattributes(id,{'struct'},{},mfilename,'id');
    validateattributes(cst,{'struct'},{},mfilename,'cst');
    validateattributes(zone,{'struct'},{},mfilename,'zone');
        % -- 
    zoneCase = cell(0);
    switch casename % Which case?
        case 'HardWallCavity'
            % id
            %   'Interior', 'Wall'
            % cst
            %   'c0'
            zoneCase{end+1} = zone.Interior_UpwindFVS(id.Interior,cst.c0);
            zoneCase{end+1} = zone.Impedance_Rigid(id.Wall,cst.c0);
        case 'SoftWallCavity'
            % id
            %   'Interior', 'Impedance'
            % cst
            %   'c0'
            %   'Impedance' with fields:
            %   'beta0', 'a1', 'DirectDelay', 'BndOp', 'BndOpDelay'
            zoneCase{end+1} = zone.Interior_UpwindFVS(id.Interior,cst.c0);
            zoneCase{end+1} = zone.Impedance_Proportional(id.Impedance,cst.Impedance.beta0,cst.c0);
            zoneCase{end+1} = zone.Impedance_Derivative(id.Impedance,cst.Impedance.beta0,cst.Impedance.a1,cst.c0);
            if ~isempty(cst.Impedance.DirectDelay)
                zoneCase = DG2D_addZone(zoneCase,@(idx,coeff)(zone.Impedance_Operator(idx,cst.Impedance.beta0,coeff,cst.c0,'Impedance_DirectDelay')),id.Impedance,cst.Impedance.DirectDelay.Coeff);
            end
            if ~isempty(cst.Impedance.BndOp)
                zoneCase = DG2D_addZone(zoneCase,@(idx,coeff)(zone.Impedance_Operator(idx,cst.Impedance.beta0,coeff,cst.c0,'Impedance_BoundaryOperator')),id.Impedance,cst.Impedance.BndOp.Coeff);
            end
            if ~isempty(cst.Impedance.BndOpDelay)
                zoneCase = DG2D_addZone(zoneCase,@(idx,coeff)(zone.Impedance_Operator(idx,cst.Impedance.beta0,coeff,cst.c0,'Impedance_BoundaryOperatorDelayed')),id.Impedance,cst.Impedance.BndOpDelay.Coeff);
            end
        otherwise
            error('[%s] Unknown case.',mfilename);
    end
end