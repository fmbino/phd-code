function [f0,f1,fd1,fd2] = LEE1D_NumFlux_Impedance_Universal(ImpCoeff,Ax)
%NumFlux_LEE1D_Impedance_Universal Compute the impedance numerical flux
% matrices, using the universal numerical flux.
% Beware, it's not very pretty.
% Centred flux, with ghost state
% f*(q,nx) = Ax*nx*(q+q_ghost)/2
%          = f0(nx)*q + f1(nx)*dq/qt + fd1(nx)*D1(q) + fd2(nx)*D2(q)
% Inputs:
%   ImpCoeff (4) Coefficients of the impedance model
%       1 <-> q, 2 <-> dq/dt, 3 <-> D1(q), 4 <-> D2(q)
%   Ax (NqxNq) Jacobian of the flux matrix (Nq=2)
% Outputs:
%   f0 (1)->(NqxNq) Dependency on q
%   f1 (1)->(NqxNq) Dependency on dq/dt
%   fd1 (1)->(Nqx1) Dependency on D1(q)
%   fd2 (1)->(Nqx1) Dependency on D2(q)

    validateattributes(ImpCoeff,{'numeric'},{'numel',4,'nonnegative'});
    validateattributes(Ax,{'numeric'},{'size',[2,2]});

    % Computation of gamma 0,1,2,3
    % [gamma_0, gamma_1, gammma_2, gamma_3]
    gam = (ImpCoeff - 2)./(ImpCoeff + 2);
    f = @(x)(1+x); g = @(x)((1-x)/2);
    fg = f(gam); gg = g(gam);
        % cover your eyes...
    alpha=[]; % [alpha0, alpha1, alpha2, alpha3]
    alpha(1) = prod(gg(logical([0,1,1,1])));
    alpha(2) = prod(gg(logical([1,0,1,1])));
    alpha(3) = prod(gg(logical([1,1,0,1])));
    alpha(4) = prod(gg(logical([1,1,1,0])));
        % at last...
        % Not assuming u_n+ = u_n-
    f0 = @(nx)((1/2)*Ax*nx*([2-fg(1)*alpha(1),prod(gg)*nx;
                                fg(1)*alpha(1)*nx,2-prod(gg)]));
    f1 = @(nx)((1/2)*Ax*nx*(fg(2)*alpha(2)*[-1,0;nx,0]));
    fd1 = @(nx)((1/2)*Ax*nx*(fg(3)*alpha(3)*[-nx;1]));
    fd2 = @(nx)((1/2)*Ax*nx*(fg(4)*alpha(4)*[-nx;1]));
        % Assuming u_n+=u_n-
%     f0 = @(nx)((1/2)*Ax*nx*([2,0;
%                             fg(1)*alpha(1)*nx,2-prod(gg)]));
%     f1 = @(nx)((1/2)*Ax*nx*(fg(2)*alpha(2)*[0,0;nx,0]));
%     fd1 = @(nx)((1/2)*Ax*nx*(fg(3)*alpha(3)*[0;1]));
%     fd2 = @(nx)((1/2)*Ax*nx*(fg(4)*alpha(4)*[0;1]));    
end
