function [K,F] = DG1D_Transport(c,L,Np,Nk)
%DG1D_Transport Build a DG formulation for the scalar transport equation:
%       dq/dt + c dq/dx = 0     (L1<x<L2) (c>0)
% with inlet condition:
%       q(x=L1,t) = f(t).
% The global formulation is:
%       dQ/dt = K*Q + F*f(t),
%where [Q] = Np*Nk.
% Remark: Beware, the code could be clearer.
% Inputs:
%   c (1) convection speed
%   L (2) bounds of physical domain
%   Np (1) Number of nodes per cell (order: <Np-1>)
%   Nk (1) Number of DG cell
% Outputs:
%   K ([Q]x[Q])
%   F ([Q]x1)

    % -- Validate attributes
    validateattributes(c,{'numeric'},{'scalar','>',0},1);
    validateattributes(L,{'numeric'},{'vector','numel',2});
    validateattributes(Np,{'numeric'},{'scalar','integer','>=',1});
    validateattributes(Nk,{'numeric'},{'scalar','integer','>=',1});
    Nq = 1; % number of variables
    % -- Initialise mesh and cell
mesh = Mesh_1D_Uniform(L, Nk);
DGCell = DGCell_1D(Np);
    % index functions
idx_q_el = @(k)((k-1)*DGCell.N*Nq+(1:(DGCell.N*Nq)));
idx_q_pt = @(k,n)((k-1)*DGCell.N*Nq+(n-1)*Nq+(1:Nq));
    % numerical flux function (upwind FVS)
finw = @(n)(diag(min(0,dot(c,n))*ones(1,Nq)));
fout = @(n)(diag(max(0,dot(c,n))*ones(1,Nq)));
    % -- Build global formulation
    % M*dQ/dt + K*Q = F*f
        % Loop over elements
[M,K] = LEE1D_LoopElements(c,0,DGCell.xi,mesh);
        % Loop over interior faces
F0 = LEE1D_LoopFaces_Interior_FVS(mesh,DGCell.xi,DGCell.N,Nq,fout,finw);
K = K - F0;
clear F0
        % Inlet (left-end of the domain)
[F0,F] = LEE1D_LoopFaces_SourceLeft_FVS(mesh,DGCell,Nq,fout(1),finw(1),idx_q_el,idx_q_pt);
K = K - F0;
clear F0
        % Outlet (right-end of the domain)
[F0,~] = LEE1D_LoopFaces_SourceRight_FVS(mesh,DGCell,Nq,fout(1),finw(1),idx_q_el,idx_q_pt);
K = K-F0;
clear F0 
    % -- Inversion
K = -M\K;
F = M\F;
    % -- Filtering top modes
%V = VandermondeLegendre1D(DGCell.N-1,DGCell.xi);
%Filter = ones(Np,1);
    % update filter
% Nc = Np-4; % cut-off mode
% alpha = -log(eps);
% s = 30;
% for i=Nc:Np
%     Filter(i) = exp(-alpha*((i-Nc)/(Np-Nc))^s);
% end
% Filter = linspace(0.8,0,Np);
%Filter = V*diag(Filter)*inv(V);
%Filter = kron(speye(Nk),Filter);
%K = K*Filter;
end

