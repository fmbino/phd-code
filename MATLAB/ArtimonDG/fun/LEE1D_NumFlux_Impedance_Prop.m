function [f0] = LEE1D_NumFlux_Impedance_Prop(ImpCoeff,Ax)
%NumFlux_LEE1D_Impedance_Universal Compute the impedance numerical flux
% matrices, using the universal numerical flux.
% Beware, it's not very pretty.
% Centred flux, with ghost state
% f*(q,nx) = Ax*nx*(q+q_ghost)/2
%          = f0(nx)*q + f1(nx)*dq/qt + fd1(nx)*D1(q) + fd2(nx)*D2(q)
% Inputs:
%   ImpCoeff (4) Coefficients of the impedance model
%       1 <-> q, 2 <-> dq/dt, 3 <-> D1(q), 4 <-> D2(q)
%   Ax (NqxNq) Jacobian of the flux matrix (Nq=2)
% Outputs:
%   f0 (1)->(NqxNq) Dependency on q

    validateattributes(ImpCoeff,{'numeric'},{'numel',1,'nonnegative'});
    validateattributes(Ax,{'numeric'},{'size',[2,2]});
    
    beta = (ImpCoeff-1)./(ImpCoeff+1);
    f0 = @(nx)((1/2)*nx*Ax*[1-beta,(1-beta)*nx;
                            (1+beta)*nx, (1+beta)]);
end