function [A,F,DirectDelay,LTIOp,LTIOpDelayed] = DG2D_inverseMassMatrix(M,K,DGMesh,Nq,varargin)
%DG2D_inverseMassMatrix Compute the formulation
%       dx/dt = A*x + F*xs + FQ*Q(x)
% from the global formulation
%       M*dx/dt + K*x = F*xs + FQ*Q(x).
% Essentially, this function compute (fastly) the inverse of M.
% Inputs:
%   M   Mass matrix
%   K   Stiffness matrix
%   DGMesh
%   Nq (1) Number of variables in the PDE
% Optional inputs (flux matrix)
%   F   Source flux matrix
%   DirectDelay (cell array of structure) Direct delay. Fields:
%       - F (cell array)
%   LTIOp (cell array of structure) LTI operator (no delay). Fields:
%       - F (cell array)
%   LTIOpDelayed (structure array) LTI operator (with delay). Fields:
%       - F (cell array of structure)
% Outputs:
%   A
%   F
%   DirectDelay
%   LTIOp
%   LTIOpDelayed

        % -- Validate attributes
    validateattributes(M,{'numeric'},{'square'},mfilename,'M');
    validateattributes(K,{'numeric'},{'square','size',size(M)},mfilename,'K');
    validateattributes(DGMesh,{'DGMesh_2D_Triangle'},{},mfilename,'DGMesh');
    validateattributes(Nq,{'numeric'},{'scalar','positive','integer'},mfilename,'Nq');
            % Optional arguments
    p = inputParser; p.FunctionName = mfilename;
    addParameter(p,'DirectDelay',[]);
    addParameter(p,'LTIOp',[]);
    addParameter(p,'LTIOpDelayed',[]);
    addParameter(p,'F',[]);
    parse(p,varargin{:}); oA = p.Results; % structure which contains optional arguments
    F = oA.F;
    if ~isempty(F)
        validateattributes(F,{'numeric'},{'2d','nrows',size(M,1)},mfilename,'F');
    end
    checkCellArrayOfStruct(oA.DirectDelay,{'F'},{'numeric'},'DirectDelay');
    checkCellArrayOfStruct(oA.LTIOp,{'F'},{'numeric'},'LTIOp');
    checkCellArrayOfStruct(oA.LTIOpDelayed,{'F'},{'numeric'},'LTIOpDelay');
    DirectDelay = oA.DirectDelay; LTIOp = oA.LTIOp; LTIOpDelayed = oA.LTIOpDelayed;
        % -- Inverse mass matrix (block diagonal)
        % Global formulation: dx/dt = A*x + F*x + FQ*Q(C*x)
    Minv = blkinv(M,DGMesh.DGCell.Np*Nq);
    A = -Minv*K;
    if ~isempty(F)
        F = Minv*F;
    end
    for i=1:length(DirectDelay)
        DirectDelay{i}.F = Minv*DirectDelay{i}.F;
    end
    for i=1:length(LTIOp)
        LTIOp{i}.F = Minv*LTIOp{i}.F;
    end
    for i=1:length(LTIOpDelayed)
        LTIOpDelayed{i}.F = Minv*LTIOpDelayed{i}.F;
    end
    clear Minv
end