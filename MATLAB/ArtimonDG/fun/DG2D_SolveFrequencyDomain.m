function x = DG2D_SolveFrequencyDomain(M,K,F,DGMesh,xs,omega,varargin)
%DG2D_SolveFrequencyDomain Solve the global formulation
%           M*dx/dt + K*x = F*xs(t) + FQ*Q(CQ*x),
%in the frequency domain. Return the full field.
% Rmk: Only the field 'Laplace_ex' of LTIOperator is used for solving.
% Inputs:
%   M,K,F Global formulation (N variables)
%   DGMesh
%   xs source amplitude
%   omega angular frequency
% Optional inputs (boundary operator)
%   DirectDelay (cell of structure) Direct delay. Fields:
%       - F (matrix) 
%       - C (matrix)
%       - Delay (vector)
%   LTIOp (cell of structure) LTI operator (no delay). Fields:
%       - F (matrix)
%       - C (matrix)
%       - Operator (LTIOperator) 
%   LTIOpDelayed (cell of structure) LTI operator (with delay). Fields:
%       - F (matrix)
%       - C (matrix)
%       - Operator (LTIOperator)
%       - Delay (vector)
% Output:
%  x field

        % -- Validate attributes
    validateattributes(M,{'numeric'},{'2d','square'},mfilename,'M');
    N = size(M);
    validateattributes(K,{'numeric'},{'size',N},mfilename,'K');
    validateattributes(F,{'numeric'},{'2d','size',[N(1),NaN]},mfilename,'F');
    validateattributes(DGMesh,{'DGMesh_2D_Triangle'},{},mfilename,'DGMesh');
    validateattributes(xs,{'numeric'},{'vector','size',[size(F,2),1]},mfilename,'xs');
    validateattributes(omega,{'numeric'},{'scalar'},mfilename,'omega');
        % Optional arguments
    p = inputParser; p.FunctionName = mfilename;
    addParameter(p,'DirectDelay',[]);
    addParameter(p,'LTIOp',[]);
    addParameter(p,'LTIOpDelayed',[]);
    parse(p,varargin{:}); oA = p.Results; % structure which contains optional arguments
                % check structures for boundary operators
    checkCellArrayOfStruct(oA.DirectDelay,{'F','C','Delay'},{'numeric','numeric','numeric'},'DirectDelay');
    checkCellArrayOfStruct(oA.LTIOp,{'F','C','Operator'},{'numeric','numeric','LTIOperator'},'LTIOp');
    checkCellArrayOfStruct(oA.LTIOpDelayed,{'F','C','Operator','Delay'},{'numeric','numeric','LTIOperator','numeric'},'LTIOpDelay');
    DirectDelay = oA.DirectDelay; LTIOp = oA.LTIOp; LTIOpDelayed=oA.LTIOpDelayed;

        % -- Compute frequency-domain solution
        % (sM + K - Q(s)*FQ*CQ)*x = F*xs (s:= 1i*omega)
    H = 1i*omega*M + K;
            % Direct delay contribution
    for i=1:length(DirectDelay)
       H = H - exp(-1i*omega*DirectDelay{i}.Delay)*DirectDelay{i}.F*DirectDelay{i}.C; 
    end
            % Boundary operator contribution
    for i=1:length(LTIOp)
       H = H - LTIOp{i}.Operator.Laplace_ex(1i*omega)*LTIOp{i}.F*LTIOp{i}.C; 
    end
            % Delayed boundary operator contribution
    for i=1:length(LTIOpDelayed)
       H = H - exp(-1i*omega*LTIOpDelayed{i}.Delay)*LTIOpDelayed{i}.Operator.Laplace_ex(1i*omega)*LTIOpDelayed{i}.F*LTIOpDelayed{i}.C; 
    end      
    x = H\(F*xs);
end


function checkLaplaceTransform(f)
%checkLaplaceTransform Checks validity of f as a complex-valued function
    validateattributes(f,{'function_handle'},{},mfilename,'Laplace');
    x = linspace(1,10,10);
    y = linspace(-10,10,10);
    [X,Y] = meshgrid(x,y);
    S = X + 1i*Y;
    validateattributes(f(S),{'numeric'},{'size',size(S)},mfilename,'Laplace(S)');
end