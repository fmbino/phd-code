function F = ArtimonDG_Build_Fluxes_INTERIOR(mesh,xi_i,N,Nq, Fn_int,Fn_ext)
%ArtimonDG_Build_Fluxes Build the fluxes matrix.
%               dq/dt + d(Ax*q)/dx + B*q = 0
% Inputs:
%   mesh (Mesh_1D_Uniform)
%   xi_i (Nx1) positions of nodes in each elements, within [-1,1] 
%   N (1x1) No of DoFs per cell
%   Nq (1x1) No. of variable
%       Local weak form flux matrices: (N*NqxNq)
%   Fn_int (NqxNq)
%   Fn_ext (NqxNq)

% Outputs:
%   F (N*Nx*Nq x N*Nx*Nq) Global flux matrix, which only accounts for the
%   interior faces.

Nx = mesh.N;
% Index for element k and point n°n in the global vector q
% k in [[1,Nk]]
% n in [[1,N]]
% ! zero-based (1,1) = 0.
% range for the whole element k
idx_q_el=@(k)((k-1)*N*Nq+(1:(N*Nq)));
% range for the whole point n°n on element k
idx_q_pt=@(k,n)((k-1)*N*Nq+(n-1)*Nq+(1:Nq));

    % Value of the Lagrange poly n°i
L1 = transpose(VandermondeLagrange1D(xi_i,1)); % (li(1))i
Lm1 = transpose(VandermondeLagrange1D(xi_i,-1)); % (li(-1))i

        % Flux vector splitting
        % (Conservativity property is used to bypass the normal)
        %       Local weak form flux matrices: (N*NqxNq)
            % cell left of the face
Fn_FVS_left_left = BuildBlockMatrix(-L1,Fn_int(1));
Fn_FVS_left_right = BuildBlockMatrix(-L1,Fn_ext(1));
            % cell right of the face
Fn_FVS_right_left = BuildBlockMatrix(Lm1,Fn_int(1));
Fn_FVS_right_right = BuildBlockMatrix(Lm1,Fn_ext(1));

    %-- Loop over interior face : global static flux matrix
    % Global Flux matrix
F = sparse(1,1,0,Nx*N*Nq,Nx*N*Nq);
for k=1:size(mesh.Face_int,1) % loop over every interior face
    kl=mesh.Face_int(k,1); kr=mesh.Face_int(k,2);
        % Contribution to the element n°kl:=k-1
    F(idx_q_el(kl),idx_q_pt(kl,N))=sparse(Fn_FVS_left_left);% dep. on itself
    F(idx_q_el(kl),idx_q_pt(kr,1))=sparse(Fn_FVS_left_right);% dep. on the right element
        % Contribution to the element n°kr:=k
    F(idx_q_el(kr),idx_q_pt(kl,N)) = sparse(Fn_FVS_right_left); % dep. on the left element
    F(idx_q_el(kr),idx_q_pt(kr,1))= sparse(Fn_FVS_right_right); % dep. on itself
end

end