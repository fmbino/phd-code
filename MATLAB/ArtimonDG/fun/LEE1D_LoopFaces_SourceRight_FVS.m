function [F0,F_source] = LEE1D_LoopFaces_SourceRight_FVS(mesh,cell,Nq,f_out,f_inw,idx_q_el,idx_q_pt)
%ArtimonDG_loopFace_SourceLeft Build contribution of the right boundary:
%source without reflexion (FVS flux).
% Centred flux, with ghost state
% f*(q,nx) = f_+(nx)*q + f_-(nx)*q
% Monodimensional LEE, global formulation:
%    M dx/dt + K dx/dt = F_delay*xd(t-dt).
% Inputs:
%   mesh (Mesh_1D_Uniform)
%   cell (DGCell_1D)
%   Nq (1) Number of variables
%   Fn_out (NqxNq) Outward FVS flux (f_+(1))
%   Fn_inw (NqxNq) Inward FVS flux (f_-(1))
%   idx_q_el index function (cell k)
%   idx_q_pt index function (cell k, node n)
% Outputs:
%   F0 (N*Nx*Nq x N*Nx*Nq) Global flux matrix
%   F_source (N*Nx*Nq x Nq) Global source flux matrix

    
    % -- Validate arguments
    validateattributes(mesh,{'Mesh_1D_Uniform'},{});
    validateattributes(cell,{'DGCell_1D'},{});
    validateattributes(Nq,{'numeric'},{'scalar','positive'});
    validateattributes(f_out,{'numeric'},{'size',[Nq,Nq]});
    validateattributes(f_inw,{'numeric'},{'size',[Nq,Nq]});
    validateattributes(idx_q_el,{'function_handle'},{});
    validateattributes(idx_q_pt,{'function_handle'},{});
    
    % -- Constants
    xi_i = cell.xi;
    N = cell.N;
    Nx = mesh.N;
        % Value of the Lagrange poly n°i
    Lp1 = transpose(VandermondeLagrange1D(xi_i,1)); % (li(1))i

    % -- Allocations
    F0 = sparse(1,1,0,Nx*N*Nq,Nx*N*Nq);
    F_source = sparse(1,1,0,Nx*N*Nq,Nq);
    
        % -- Left end of the domain = cell right of the first face
        % Block matrices (N*NqxNq)
    Fblock_out = sparse(BuildBlockMatrix(Lp1,f_inw)); % -li(1)*f_+(-1) = li(1)*f_-(1) 
    Fblock_inw = sparse(BuildBlockMatrix(Lp1,f_out)); % -li(1)*f_-(-1) = li(1)*f_+(1) 
    for k=1:length(mesh.Face_right_bound) % right boundary
        kr=mesh.Face_right_bound(k,1);
            % No-reflexion condition
        F0(idx_q_el(kr),idx_q_pt(kr,N))=-Fblock_inw; % li(1)*f+(1)
            % Source input
        F_source(idx_q_el(kr),1:Nq) = Fblock_out; % li(1)*f-(1)
    end
    fprintf('[SourceLeft] F_source built.\n');
end