function q_interp = DG2D_interpolateField2PlotNodes(DGCell,mesh,Nq,q)
%DG2D_interpolateField2PlotTriangles Interpolate field q to the plot nodes.
% Input:
%   DGCell (DGCell_2D_Triangle)
%   mesh (Mesh_2D_Triangle)
%   q (Nk*Np*Nq) solution at DG nodes
% Output:
%   q_interp (Nk*Np*Nq) solution at plot nodes

    validateattributes(DGCell,{'DGCell_2D_Triangle'},{});
    validateattributes(mesh,{'Mesh_2D_Triangle'},{});
Np=DGCell.Np;
Nk=mesh.N;
    validateattributes(Nq,{'numeric'},{'scalar','positive'});
    validateattributes(q,{'numeric'},{'vector','numel',Np*Nq*Nk});
    
    if Nq>1
interp = BuildBlockMatrix(DGCell.interpXiToXiplot,eye(Nq));
    else
interp = DGCell.interpXiToXiplot;
    end
    % interpolation to display nodes
q_interp = interp*reshape(q(:),[Np*Nq,Nk]);
end