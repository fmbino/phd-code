function [M,K] = ArtimonDG_Build_MassStiff(Ax,B,xi_i,mesh)
%ArtimonDG_Build_MassStiff Build the mass and stiffness matrix.
%               dq/dt + d(Ax*q)/dx + B*q = 0
% Inputs:
%   xi_i (Nx1) positions of nodes in each elements, within [-1,1] 
%   Ax, B (NqxNq)
%   mesh (Mesh_1D_Uniform) Used for the cell size and the no. of cells Nx
% Outputs:
%   M (NNxNq x NNxNq) mass matrix
%   K (NNxNq x NNxNq) stiffness matrix

    % Check arguments
Nq = size(Ax,1); % Nq deduced from Ax
N = size(xi_i,1); % N deduced from xi_i

    %-- Build polynomial matrices
    % Output:
    %   L (NxN) L(i,j)=<li,lj>_R quadrature on [-1,1]
    %   S % (NxN) <dli/dx,lj>_R quadrature (=<>_Dk)
L = zeros(N,N); % (NxN) L(i,j)=<li,lj>_R quadrature on [-1,1]
V = VandermondeLegendre1D(N-1,xi_i); % (NxN)
L = inv(V*transpose(V)); % OK
clear V
Dr = zeros(N,N); % (NxN) <dlj/dr(xi_i)> derivation matrix Lagrange poly
Dr = Dmatrix1DLagrange(N-1,xi_i,xi_i);
S = zeros(N,N); % (NxN) <dli/dx,lj>_R quadrature (=<>_Dk)
S = transpose(Dr)*L;
clear Dr

    %--  Build local masss and stiffness matrices
    % Output:
    %   Mk, Kk (NNqxNNq)
Mk = (mesh.cell_size/2)*BuildBlockMatrix(L,eye(Nq));
Sk = BuildBlockMatrix(S,Ax); % no metric term!
Bk = (mesh.cell_size/2)*BuildBlockMatrix(L,B);
validateattributes(Mk,{'numeric'},{'size',[N*Nq,N*Nq]});
validateattributes(Sk,{'numeric'},{'size',[N*Nq,N*Nq]});
validateattributes(Bk,{'numeric'},{'size',[N*Nq,N*Nq]});
Kk = -Sk+Bk;
clear Bk Sk

    %-- Build global matrices: block diagonal matrices from local ones
Nx = mesh.N;
I_Nx = speye(Nx);
M = sparse(1,1,1,Nx*N*Nq,Nx*N*Nq);
K = sparse(1,1,1,Nx*N*Nq,Nx*N*Nq);
M = kron(I_Nx,sparse(Mk));
K = kron(I_Nx,sparse(Kk));
clear I_Nx

end

