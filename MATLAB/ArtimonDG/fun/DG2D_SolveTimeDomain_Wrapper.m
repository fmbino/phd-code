function [p,t,xprobe] = DG2D_SolveTimeDomain_Wrapper(DGMesh,zoneDef,zoneList,c0,u0,du0dy,TDIBC,CFL,tf,ps,varargin)
%DG2D_SolveTimeDomain_Wrapper Wrapper around DG2D_SolveTimeDomain for
%simpler use with the Linearized Euler equation on [u;v;p/z0].
% Inputs:
%   DGMesh (DGMesh_2D_Triangle)
%   zoneDef (struct) definition of mesh zones
%   zoneList (struct) list of all possible zones
%   c0 (1) sound speed
%   u0(x,y) (function_handle) base flow u0(x,y)
%   du0dy(x,y) (function_handle) base flow du0/dy(x,y) 
%   TDIBC (ImpedanceBoundaryCondition) Time-domain IBC
%   CFL (1) CFL number
%   tf (1)  final time
%   ps (function_handle) pressure source
% Inputs (optional):
%   probe (Nx2) desired pressure probe position [x,y]
%   Delay_Np (1) hyperbolic realization of delay, # of points per cell (2)
%   Delay_Nk (1) hyperbolic realization of delay, # of cells (1)
% Outputs:
%   p (RxP)    pressure (p/z0) (if pressure probe given)
%              or full field [u,v,p/z0]
%   t (P)      time instants
%   xprobe (Nx2) Actual probe position [x,y]
 
        % -- Validate attributes
            % Mandatory arguments
    validateattributes(DGMesh,{'DGMesh_2D_Triangle'},{},mfilename,'DGMesh');
    validateattributes(zoneDef,{'struct'},{},mfilename,'zoneDef');
    validateattributes(zoneList,{'struct'},{},mfilename,'zoneList');
    validateattributes(c0,{'numeric'},{'scalar','positive'},mfilename,'c0');
    validateattributes(u0,{'function_handle'},{},mfilename,'u0');
    validateattributes(du0dy,{'function_handle'},{},mfilename,'du0dy');
    validateattributes(TDIBC,{'ImpedanceBoundaryCondition'},{},mfilename,'TDIBC');
    validateattributes(CFL,{'numeric'},{'scalar','positive'},mfilename,'CFL');
    validateattributes(tf,{'numeric'},{'scalar','positive'},mfilename,'tf');
    validateattributes(ps,{'function_handle'},{},mfilename,'ps');
            % Optional arguments
    p = inputParser; p.FunctionName = mfilename;
    addParameter(p,'probe',[],@(x)(validateattributes(x,{'numeric'},{'numeric','ncols',2})));
    addParameter(p,'Delay_Np',2,@(x)(validateattributes(x,{'numeric'},{'scalar'})));
    addParameter(p,'Delay_Nk',1,@(x)(validateattributes(x,{'numeric'},{'scalar'})));
    parse(p,varargin{:}); oA = p.Results; % structure which contains optional arguments
    probe = oA.probe; Delay_Np = oA.Delay_Np; Delay_Nk=oA.Delay_Nk;
    cst.Nq = 3; % Number of variables
        % -- Probe placement
    xprobe = [];
    if ~isempty(probe)
                % Locate probe
        idxProbeNodes = DGMesh.getNearestNodeIndex(probe);
        %DGMesh.showDGNodes('HighlightNodesIndex',idxNodes);
            % indices in solution vector
        idxVar = [3]; % Variable to observe (p/z0)
        idxQ = zeros(length(idxVar),length(idxProbeNodes));
        for i=1:length(idxProbeNodes)
            idxQ(:,i) = cst.Nq*(idxProbeNodes(i)-1) + idxVar;
        end
        idxQ = idxQ(:);
        nodes = DGMesh.getNodesCoordinates();
        xprobe = nodes(idxProbeNodes,:);
    end
        % -- Base flow
    cst.c0 = c0; cst.u0 = u0; cst.du0dy = du0dy; cst.du0dx = @(x,y)0;
    cst.v0 = @(x,y)0; cst.dv0dx = @(x,y)0; cst.dv0dy = @(x,y)0;
        % -- Definition of LEE Matrices
            % PDE definition
            % q = [u,v,p/z0];
    Ax = @(u0,c0)(...
        [u0,0,c0;
        0,u0,0;...
        c0,0,u0]);
    Ay = @(v0,c0)(...
        [v0,0,0;...
        0,v0,c0;...
        0,c0,v0]);
    B = @(u0,du0dx,du0dy,v0,dv0dx,dv0dy,c0)(...
        [-dv0dy,du0dy,u0*du0dx/c0+v0*du0dy/c0;...
        dv0dx,-du0dx,u0*dv0dx/c0+v0*dv0dy/c0;...
        0,0,0]);
    Ax_fun = @(x,y)Ax(cst.u0(x,y),cst.c0);
    Ay_fun = @(x,y)Ay(cst.v0(x,y),cst.c0);
    B_fun = @(x,y)B(cst.u0(x,y),cst.du0dx(x,y),cst.du0dy(x,y),cst.v0(x,y),cst.dv0dx(x,y),cst.dv0dy(x,y),cst.c0);
        % -- DG solution: building global formulation
    cst.Impedance = TDIBC;
    zoneCase = DG2D_LEELongitudinal_createZone(zoneDef,cst,zoneList);
    [M,K,F,cst.DirectDelay,cst.BndOp,cst.BndOpDelay]=DG2D_LEE_buildGlobalFormulation(Ax_fun,Ay_fun,B_fun,DGMesh,zoneCase,TDIBC);
        % -- Time integration
            % Source
     q_source = @(t)([ps(t);zeros(size(t));ps(t)]);
            % Initial condition
     q0 = zeros(size(M,1),1);
            % Time step
    dt = CFL*min(DGMesh.mesh.charLength)/norm(cst.c0); % Time step
            % Probe
     if isempty(probe) % no probe, compute full field
            % here, p stores the full field 
        [p,t] = DG2D_SolveTimeDomain(M,K,F,DGMesh,cst.Nq,tf,dt,'x0',q0,'xs',q_source,'storeOnlyLast',0,'DirectDelay',cst.DirectDelay,'LTIOp',cst.BndOp,'LTIOpDelayed',cst.BndOpDelay,'Np',Delay_Np,'Nk',Delay_Nk);
     else % probe given, keep only given indices
        [p,t] = DG2D_SolveTimeDomain(M,K,F,DGMesh,cst.Nq,tf,dt,'nidx',idxQ,'x0',q0,'xs',q_source,'storeOnlyLast',0,'DirectDelay',cst.DirectDelay,'LTIOp',cst.BndOp,'LTIOpDelayed',cst.BndOpDelay,'Np',Delay_Np,'Nk',Delay_Nk);
     end
end