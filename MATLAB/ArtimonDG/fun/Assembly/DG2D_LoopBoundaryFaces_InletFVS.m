function F = DG2D_LoopBoundaryFaces_InletFVS(f,DGMesh,physicalEntity)
%DG2D_LoopBoundaryFaces_InletFVS Build the source flux matrix that
%results from the inlet condition:
%           F = - [(f(n,x),li)_k,n]_i,
% where (,)_k,n is the border integral over the face n of element k.
% Inputs:
%   f (function_handle) f([nx,ny]) or f([nx,ny],x,y) is NqxNq
%   DGMesh (DGMesh_2D_Triangle) DG mesh
%   physicalEntity (?) number of each physical entity (zone)
% Output:
%   F (Nk*Np*Nq x Nq) (sparse) flux matrix

    % -- Validate attributes
validateattributes(f,{'function_handle'},{},mfilename,'f');
if nargin(f)==1 % f does no depend on (x,y)
    validateattributes(f([1,1]),{'double'},{'2d','nonempty'},mfilename,'f([1,1])');
    Nq = size(f([1,1]),1);
    fprintf('[%s] Numerical flux do not depend on (x,y): f(n).\n',mfilename);
    x = []; % no coordinates needed
    buildLocalFluxMatrix = @buildLocalFluxMatrix_Constant;
else
    validateattributes(f([1,1],1,1),{'double'},{'2d','nonempty'});
    Nq = size(f([1,1],1,1),1);
    fprintf('[%s] Numerical flux depends on (x,y): f(n,x,y).\n',mfilename);
    x = DGMesh.getNodesCoordinates(); % global coordinates [x,y]
    buildLocalFluxMatrix = @buildLocalFluxMatrix_Variable;
end
validateattributes(DGMesh,{'DGMesh_2D_Triangle'},{});
validateattributes(physicalEntity,{'numeric'},{'vector','integer','positive'});
    % -- Retrieve key arrays
mesh = DGMesh.mesh;
DGCell = DGMesh.DGCell;
Np = DGCell.Np;
Nk = mesh.N;
EpsFace = DGCell.Eps;
nFace = mesh.n;
vmapM = DGMesh.vmapM;
    % -- Process boundaries
idx = [];
for i=1:length(physicalEntity)
    idx = [idx(:); find(mesh.boundaryFaces(:,1)==physicalEntity(i))];
end
fprintf('[%s] %d faces in zones %s.\n',mfilename,length(idx),mat2str(physicalEntity));
    % -- Build flux matrix
    %(construction uses i,j,v sparse syntax)
Nflux = (DGCell.Nfp)*(Nq*Nq); % no. of elements in flux matrix, per face
Nface = length(idx); % upper boundary on the number of faces
F_idx_i = zeros(Nface*Nflux,1); % line index
F_idx_j = zeros(Nface*Nflux,1); % column index
F_value = zeros(Nface*Nflux,1); % values
idx_F = 0; % counter
for i=idx' % for each boundary face
        % connects to element k, local face l
    k = mesh.FToE(i); l = mesh.FToF(i);
    edL = mesh.edL(k,l);
    nk = nFace(k,:,l);
    scale = (edL/2);
    nodesM = vmapM(:,l,k);
        % local flux matrix
    Fl = -scale*buildLocalFluxMatrix(EpsFace(:,:,l),f,nk,nodesM,x);
        % F_source(nodesM,:) = F_source(nodesM,:) + Finw;
    nodesM = expandIndex(nodesM,Nq);
    [F_idx_i(idx_F+(1:Nflux)), F_idx_j(idx_F+(1:Nflux)),F_value(idx_F+(1:Nflux))] = make_sparse_index(nodesM,1:Nq,Fl);
    idx_F = idx_F + Nflux;
end
F = sparse(F_idx_i,F_idx_j,F_value,Nk*Np*Nq,Nq);
end

function Fl = buildLocalFluxMatrix_Constant(EpsFace,f,nk,varargin)
%buildLocalFluxMatrix_Constant Build Local flux matrix F^(k,n)_+(n^(k,n)),
%in the case where the numerical flux function does not depend on (x,y).
% Inputs:
%   EpsFace (NfpxNfp) Edge quadratures
%   f (function_handle) Numerical flux function f(n) 
%   nk (2)  Outward normal
% Outputs:
%   Fl (Nfp*Nq,Nq) Local flux matrix
    Fl = kron(sum(EpsFace,2),f(nk));
end

function Fl = buildLocalFluxMatrix_Variable(EpsFace,f,nk,nodesM,x)
%buildLocalFluxMatrix_Constant Build Local flux matrix F^(k,n)_+(n^(k,n)),
%in the case where the numerical flux function does depend on (x,y).
% Inputs:
%   EpsFace (NfpxNfp) Edge quadratures
%   f (function_handle) numerical flux function f(n,x,y) 
%   nk (2)  Outward normal
%   nodesM  Edge nodes
%   x       Nodes global coordinates
% Outputs:
%   Fl (Nfp*Nq,Nq) Local flux matrix
    Fl = buildBlockMatrixFun2D(EpsFace,x(nodesM,:),@(x,y)(f(nk,x,y)));    
    Fl = sumBlock(Fl,size(EpsFace,2));
end