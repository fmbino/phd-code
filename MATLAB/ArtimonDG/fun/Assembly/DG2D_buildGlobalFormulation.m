function [M,K,F,FQ,CQ] = DG2D_buildGlobalFormulation(Ax,Ay,B,DGMesh,zone)
%DG2D_buildGlobalFormulation Global DG 2D formulation for the 2D linear
% PDE:
%               dq(x,t)/dt + d(Am*q)(x,t)/dm + B*q(x,t) = 0,
% where Ax, Ay and B are constant matrices. The global formulation is
%       M*dX/dt + K*X = F*qs(t) + FQ_k*Q_k(Cq_k*q),
%where Q_k is a single-input single-output operator. (The expression of Q_k
%need not be known.)
% Inputs:
%   Ax, Ay, B (NqxNq)
%   DGMesh (DGMesh_2D_Triangle) DG mesh
%   zone (cell array)  zones & boundary conditions
% Outputs:
%   M (Nk*Np*Nq x Nk*Np*Nq) (sparse) mass matrix
%   K (Nk*Np*Nq x Nk*Np*Nq) (sparse) stiffness matrix
%   F (Nk*Np*Nq x Nq)   (sparse) source matrix
%   FQ (Nk*Np*Nq x Nface*Nfp) cell array  of (sparse) flux matrix
%   CQ (Nface*Nfp x Nk*Np*Nq) cell array of (sparse) observation matrix
% Remark: Zone is a cell array of structure/object that follows the format:
% zone{i} = struct('name','physicalEntity','arg').
% Below are the supported zones (Name, arg):
%       'Interior_UpwindFVS', NumericalFlux_2D_UpwindFVS
%       'Outlet_UpwindFVS', //
%       'Inlet_UpwindFVS', //
%       'Periodic_UpwindFVS', //
%       'Boundary_Generic_Proportional', NumericalFlux_2D_Generic 
%       'Boundary_Generic_Derivative' //
%       'Boundary_Generic_Operator', NumericalFlux_2D_Generic_Operator

        % -- Validate attributes
    validateattributes(DGMesh,{'DGMesh_2D_Triangle'},{},mfilename,'DGMesh');
    validateattributes(zone,{'cell'},{},mfilename,'zone');
    
        % -- Loop over zones
        % Challenge: minimize copy operations.
    M = []; K = []; F = []; FQ=cell(0); CQ=cell(0);
    fprintf('[%s] Building global formulation...\n',mfilename);
    msg = '[%s] <%s> on zone(s) [%s].\n';
    for i=1:length(zone) % for each defined zone
        validateattributes(zone{i},{'struct'},{});
        validateattributes(zone{i}.name,{'char'},{});
        arg = zone{i}.arg; % structure which contains arguments
        switch zone{i}.name % What kind of physical entity?
            case 'Interior_UpwindFVS'
                validateattributes(arg,{'NumericalFlux_2D_UpwindFVS'},{},mfilename,sprintf('zone{%d}.arg',i));
                fprintf(msg,mfilename,'Interior_UpwindFVS',mat2str(zone{i}.physicalEntity));
                [M_tmp,K_tmp] = DG2D_LoopTriangles_UpwindFVS(Ax,Ay,B,arg.fout,arg.finw,DGMesh,zone{i}.physicalEntity);
                M = updateSparse(M,M_tmp);
                K = updateSparse(K,K_tmp);
                clear M_tmp K_tmp
            case 'Outlet_UpwindFVS'
                validateattributes(arg,{'NumericalFlux_2D_UpwindFVS'},{},mfilename,sprintf('zone{%d}.arg',i));
                fprintf(msg,mfilename,'Outlet_UpwindFVS',mat2str(zone{i}.physicalEntity));
                [K_tmp] = -DG2D_LoopBoundaryFaces_Generic(arg.fout,DGMesh,zone{i}.physicalEntity);
                K = updateSparse(K,K_tmp);
                clear K_tmp
            case 'Inlet_UpwindFVS'
                validateattributes(arg,{'NumericalFlux_2D_UpwindFVS'},{},mfilename,sprintf('zone{%d}.arg',i));
                fprintf(msg,mfilename,'Inlet_UpwindFVS',mat2str(zone{i}.physicalEntity));
                F = DG2D_LoopBoundaryFaces_InletFVS(arg.finw,DGMesh,zone{i}.physicalEntity);
            case 'Periodic_UpwindFVS'
                validateattributes(arg,{'NumericalFlux_2D_UpwindFVS'},{},mfilename,sprintf('zone{%d}.arg',i));
                fprintf(msg,mfilename,'Periodic_UpwindFVS',mat2str(zone{i}.physicalEntity));
                K_tmp = DG2D_LoopBoundaryFaces_PeriodicFVS(arg.fout,arg.finw,DGMesh,zone{i}.physicalEntity);
                K = updateSparse(K,K_tmp);
                clear K_tmp
            case 'Boundary_Generic_Proportional'
                validateattributes(arg,{'NumericalFlux_2D_Generic'},{},mfilename,sprintf('zone{%d}.arg',i));
                fprintf(msg,mfilename,'Boundary_Generic_Proportional',mat2str(zone{i}.physicalEntity));
                K_tmp = -DG2D_LoopBoundaryFaces_Generic(arg.f,DGMesh,zone{i}.physicalEntity);
                K = updateSparse(K,K_tmp);
            case 'Boundary_Generic_Derivative'
                validateattributes(arg,{'NumericalFlux_2D_Generic'},{},mfilename,sprintf('zone{%d}.arg',i));
                fprintf(msg,mfilename,'Boundary_Generic_Derivative',mat2str(zone{i}.physicalEntity));
                M_tmp = -DG2D_LoopBoundaryFaces_Generic(arg.f,DGMesh,zone{i}.physicalEntity);
                M = updateSparse(M,M_tmp);
                clear M_tmp
            case 'Boundary_Generic_Operator'
                validateattributes(arg,{'NumericalFlux_2D_Generic_Operator'},{},mfilename,sprintf('zone{%d}.arg',i));
                fprintf(msg,mfilename,'Boundary_Generic_Operator',mat2str(zone{i}.physicalEntity));
                fprintf('[%s] Boundary_Generic_Operator: %d numerical flux function(s) given.\n',mfilename,length(arg));
                for k=1:length(arg) % for each given flux
                    fprintf('[%s] Building FQ_%d and CQ_%d ...\n',mfilename,k,k);
                    [FQ{end+1},CQ{end+1}] = DG2D_LoopBoundaryFaces_Generic_Operator(arg(k).f,arg(k).c,DGMesh,zone{i}.physicalEntity);
                end
            otherwise
                error('[%s] Unexpected zone type: <%s>',mfilename, zone{i}.name);
        end
    end
    fprintf('[%s] global formulation built:\n',mfilename);
    fprintf(' M*dX/dt + K*X = F*qs(t) + FQ_k*Q_k(CQ_k*q)\n');
    fprintf('\t Mass Matrix M (%dx%d)\n',size(M,1),size(M,2));
    fprintf('\t Stiffness Matrix K (%dx%d)\n',size(K,1),size(K,2));
    fprintf('\t Source Matrix F (%dx%d)\n',size(F,1),size(F,2));
    fprintf('\t Q-operator matrices:\n');
    for k=1:length(FQ)
        fprintf('\t\t Flux FQ_%d (%dx%d)\n',k,size(FQ{k},1),size(FQ{k},2));
        fprintf('\t\t Observation CQ_%d (%dx%d)\n',k,size(CQ{k},1),size(CQ{k},2));
    end
end

function A = updateSparse(A,dA)
%updateSparse Returns A = A + dA, and tries to minimize cost.
% Inputs:
%   A   sparse matrix
%   dA  sparse matrix (same size as A)
% Outputs:
%   A = A +dA

    if isempty(A)
       A = dA; % no copy 
    elseif nnz(A)>=nnz(dA) % A is larger than dA
       A = A + dA; % nnz(dA)-nnz(A) element created
    else
       dA = A + dA; % nnz(A)-nnz(dA) element created
       A = dA; % no copy
    end
end