function [F,C] = DG2D_LoopBoundaryFaces_Generic_Operator(f,c,DGMesh,physicalEntity)
%DG2D_LoopBoundaryFaces_Generic Build the flux and observation matrices 
%associated with the numerical flux function f and observation vector c, 
%defined as:
%                  F = - [(lj*f(n,x),li)_k,n]_(i,j)
%                  C = [c(n) * q_i]_i,
% where (,)_k,n is the border integral over the face n of element k.
% The corresponding right hand side in the global formulation reads:
%           M*dq/dt + K*q = F*Q(C*q),
% where Q is a single-input single-output operator. (The expression of Q
% does not need to be known here.)
% Inputs:
%   f (function_handle) f([nx,ny]) or f([nx,ny],x,y) is Nq
%   c (function_handle) c([nx,ny]) is Nq
%   DGMesh (DGMesh_2D_Triangle) DG mesh
%   physicalEntity (?) number of each physical entity (zone)
% Output:
%   F (Nk*Np*Nq x Nface*Nfp) (sparse) flux matrix
%   C (Nface*Nfp x Nk*Np*Nq) (sparse) observation matrix

    % -- Validate attributes
validateattributes(f,{'function_handle'},{},mfilename,'f');
if nargin(f)==1 % f does not depend on (x,y)
    validateattributes(f([1,1]),{'double'},{'column','nonempty'},mfilename,'f([1,1])');
    Nq = length(f([1,1]));
    fprintf('[%s] Numerical flux does not depend on (x,y): f(n).\n',mfilename);
    x = []; % no coordinates needed
    buildLocalFluxMatrix = @buildLocalFluxMatrix_Constant;
else
    validateattributes(f([1,1],1,1),{'double'},{'column','nonempty'},mfilename,'f([1,1],1,1)');
    Nq = length(f([1,1],1,1));
    fprintf('[%s] Numerical flux depends on (x,y): f(n,x,y).\n',mfilename);
    x = DGMesh.getNodesCoordinates(); % global coordinates [x,y]
    buildLocalFluxMatrix = @buildLocalFluxMatrix_Variable;
end
validateattributes(c,{'function_handle'},{},mfilename,'c');
validateattributes(c([1,1]),{'numeric'},{'vector','numel',Nq},mfilename,'c([1,1])');
if size(c([1,1]),1)>1
    c = @(n)(transpose(c(n))); % ensure c(n) is a line (for convenience)
end
validateattributes(DGMesh,{'DGMesh_2D_Triangle'},{},mfilename,'DDGMesh');
validateattributes(physicalEntity,{'numeric'},{'vector','integer','positive'},mfilename,'physicalEntity');

    % -- Retrieve key arrays
mesh = DGMesh.mesh;
DGCell = DGMesh.DGCell;
Np = DGCell.Np;
Nfp = DGCell.Nfp;
Nk = mesh.N;
EpsFace = DGCell.Eps;
nFace = mesh.n;
vmapM = DGMesh.vmapM;
    % -- Process boundaries
idx = [];
for i=1:length(physicalEntity)
    idx = [idx(:); find(mesh.boundaryFaces(:,1)==physicalEntity(i))];
end
fprintf('[%s] %d faces in zone(s) %s.\n',mfilename,length(idx),mat2str(physicalEntity));
    % -- Compute Flux and Observation matrix
    %(construction uses i,j,v sparse syntax)
Nface = length(idx); % Number of boundary faces
    % Flux matrix
N_F = prod([Nfp*Nq,Nfp]); % [nrow,ncol] size of local matrix (per face)
F_idx_i = zeros(Nface*N_F,1); % line index
F_idx_j = zeros(Nface*N_F,1); % column index
F_value = zeros(Nface*N_F,1); % values
    % Observation matrix
N_C = prod([Nfp,Nq]); % [nrow,ncol] size of local matrix (per face)
C_idx_i = zeros(Nface*N_C,1); % line index
C_idx_j = zeros(Nface*N_C,1); % column index
C_value = zeros(Nface*N_C,1); % values
    % Counters
idx_F = 0; % number of non-zero elements in F
idx_C = 0; % number of non-zero elements in C
idx_Face = 0; % current face number, in [0,Face-1]
for i=idx' % for each boundary face
        % connects to element k, local face l
    k = mesh.FToE(i); l = mesh.FToF(i);
    edL = mesh.edL(k,l);
    nk = nFace(k,:,l);
    scale = (edL/2);
    nodesM = vmapM(:,l,k);
        % build local flux matrix F^(k,n) (Nfp*Nq x Nfp)
    Fl = -scale*buildLocalFluxMatrix(EpsFace(:,:,l),f,nk,nodesM,x);
        % F(nodesM, idx_Face*Nfp + 1:Nfp) = F(nodesM, idx_Face*Nfp + 1:Nfp) + Fl
    nodesM = expandIndex(nodesM,Nq); % length Nfp*Nq
    [F_idx_i(idx_F+(1:N_F)), F_idx_j(idx_F+(1:N_F)),F_value(idx_F+(1:N_F))] = make_sparse_index(nodesM,idx_Face*Nfp+(1:Nfp),Fl);
    idx_F = idx_F + N_F;
        % build local observation matrix C^(k,n)
    Cl = repmat(c(nk),[1,Nfp]); % 1 x (Nfp*Nq)
    C_idx_i(idx_C + (1:N_C)) = idx_Face*Nfp + kron(1:Nfp,ones(1,Nq));
    C_idx_j(idx_C + (1:N_C)) = nodesM;
    C_value(idx_C + (1:N_C)) = Cl;
    idx_C = idx_C + N_C;
    idx_Face = idx_Face + 1;
end 
F = sparse(F_idx_i,F_idx_j,F_value,Nk*Np*Nq,Nface*Nfp);
C = sparse(C_idx_i,C_idx_j,C_value,Nface*Nfp,Nk*Np*Nq);
end

function Fl = buildLocalFluxMatrix_Constant(EpsFace,f,nk,varargin)
%buildLocalFluxMatrix_Constant Build Local flux matrix F^(k,n)_+(n^(k,n)),
%in the case where the numerical flux function does not depend on (x,y).
% Inputs:
%   EpsFace (NfpxNfp) Edge quadratures
%   f (function_handle) Numerical flux function f(n) 
%   nk (2)  Outward normal
% Outputs:
%   Fl (Nfp*Nq,Nfp) Local flux matrix
    Fl = kron(EpsFace,f(nk));
end

function Fl = buildLocalFluxMatrix_Variable(EpsFace,f,nk,nodesM,x)
%buildLocalFluxMatrix_Constant Build Local flux matrix F^(k,n)_+(n^(k,n)),
%in the case where the numerical flux function does depend on (x,y).
% Inputs:
%   EpsFace (NfpxNfp) Edge quadratures
%   f (function_handle) numerical flux function f(n,x,y) 
%   nk (2)  Outward normal
%   nodesM  Edge nodes
%   x       Nodes global coordinates
% Outputs:
%   Fl (Nfp*Nq,Nfp) Local flux matrix
    Fl = buildBlockMatrixFun2D(EpsFace,x(nodesM,:),@(x,y)(f(nk,x,y)));
end