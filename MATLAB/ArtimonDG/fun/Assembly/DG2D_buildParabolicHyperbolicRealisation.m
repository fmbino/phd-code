function [A,F] = DG2D_buildParabolicHyperbolicRealisation(M,K,DGMesh,Nq,varargin)
%DG2D_buildParabolicHyperbolicRealisation Build the parabolic-hyperbolic
%realisation:
%           dX/dt = A*X + F*xs
% from the global formulation
%           dx/dt = A*x + F*xs + FQ*Q(CQ*x) + FQ*Q(CQ*x)(t-tau) 
%                       + F*C*x(t-tau).
% The three additional terms designate: boundary
% operator, delayed boundary operator, and direct boundary delay.
% Rmk: Only one delay value 'tau' is implemented.
% Inputs:
%   M,K           Global formulation
%   DGMesh (DGMesh_2D_Triangle)
%   Nq (1)          No. of variables in the PDE
% Optional inputs (flux):
%   F       source flux matrix
% Optional inputs (boundary operator)
%   DirectDelay (cell array of structure) Direct delay. Fields:
%       - F (matrix)
%       - C (matrix)
%       - Delay (scalar)
%   LTIOp (cell array of structure) LTI operator (no delay). Fields:
%       - F (matrix)
%       - C (matrix)
%       - Operator (LTIOperator)
%   LTIOpDelayed (cell array of structure) LTI operator (with delay). Fields:
%       - F (matrix)
%       - C (matrix)
%       - Operator (LTIOperator)
%       - Delay (scalar)
% Optional inputs (hyperbolic realisation with DG).
%   Nk (1) number of element in (0,1) 
%   Np (1) number of nodes per element (element order: <Np-1>)
% Outputs:
%   A
%   F

        % -- Optional arguments
    p = inputParser; p.FunctionName = mfilename;
    addParameter(p,'F',[]);
    addParameter(p,'DirectDelay',[]);
    addParameter(p,'LTIOp',[]);
    addParameter(p,'LTIOpDelayed',[]);
    addParameter(p,'Nk',2,@(x)(validateattributes(x,{'numeric'},{'scalar','integer','>=',1})));
    addParameter(p,'Np',2,@(x)(validateattributes(x,{'numeric'},{'scalar','integer','>=',1})));
    parse(p,varargin{:}); oA = p.Results; % structure which contains optional arguments
    F = oA.F; Np = oA.Np; Nk=oA.Nk;
    DirectDelay=oA.DirectDelay; LTIOp=oA.LTIOp;
    LTIOpDelayed=oA.LTIOpDelayed;
                % check structures for boundary operators
    checkCellArrayOfStruct(DirectDelay,{'F','C','Delay'},{'numeric','numeric','numeric'},'DirectDelay');
    checkCellArrayOfStruct(LTIOp,{'F','C','Operator'},{'numeric','numeric','LTIOperator'},'LTIOp');
    checkCellArrayOfStruct(LTIOpDelayed,{'F','C','Operator','Delay'},{'numeric','numeric','LTIOperator','numeric'},'LTIOpDelay');

        % -- Inverse mass matrix
    [A,F,DirectDelay,LTIOp,LTIOpDelayed] = DG2D_inverseMassMatrix(M,K,DGMesh,Nq,'F',F,'DirectDelay',DirectDelay,'LTIOp',LTIOp,'LTIOpDelayed',LTIOpDelayed);
    if ~isempty(DirectDelay) || ~isempty(LTIOp) || ~isempty(LTIOpDelayed)
        % -- Build hyperbolic-parabolic realisation
        % dx/dt = A*x + F*xs + B*(C*x)(t-Delay))
            % Create field 'B' from 'F'
        for i=1:length(DirectDelay)
            [DirectDelay{i}.B] = DirectDelay{i}.F;
        end
        for i=1:length(LTIOp)
            [LTIOp{i}.B] = LTIOp{i}.F;
        end
        for i=1:length(LTIOpDelayed)
            [LTIOpDelayed{i}.B] = LTIOpDelayed{i}.F;
        end
        fprintf('[%s] Building parabolic realisation...\n',mfilename);
        [A,B,C] = DDELTI_BuildParabolicRealisation(A,DirectDelay,LTIOp,LTIOpDelayed);
        fprintf('[%s] Building parabolic realisation: finished.\n',mfilename);
        if any(B(:))
                % -- Build hyperbolic realisation
                % dx/dt = A*x
                % choose any delay (several delay case not implemented)
            if ~isempty(DirectDelay)
                Delay = DirectDelay{1}.Delay;
                fprintf('[%s] Delay chosen:  DirectDelay{1}.Delay = %1.1e.\n',mfilename,Delay);
            elseif ~isempty(LTIOpDelayed)
                Delay = LTIOpDelayed{1}.Delay;
                fprintf('[%s] Delay chosen: LTIOpDelayed{1}.Delay = %1.1e.\n',mfilename,Delay);
            end
            fprintf('[%s] Building hyperbolic realisation...\n',mfilename);
            A = DDEConstantDelay_BuildHyperbolicRealisation(A,B,Delay,C,'Np',Np,'Nk',Nk);
            fprintf('[%s] Building hyperbolic realisation: finished.\n',mfilename);
        end
    end
        % extend F
    if ~isempty(F) && (size(A,1)>size(M,1))
        F = [F;sparse(size(A,1)-size(M,1),size(F,2))]; % expand source term
    end
end