function [M,K] = DG2D_LoopTriangles_UpwindFVS_Parallel(Ax,Ay,B,fout,finw,DGMesh,physicalEntity)
% Same as DG2D_LoopTriangles_UpwindFVS, with slight modifications to enable the use of
% parfor, from the Parallel Computing Toolbox.
        % Constant or variable coefficients ?
        % (Assembly functions are too dissimilar to be merged in a readable
        % fashion.)
    if isa(Ax,'function_handle') && isa(Ay,'function_handle') && ...
       isa(B,'function_handle')
        fprintf('[%s] Coeff. depends on space: Ax(x,y).\n',mfilename);
        if (nargin(finw)~=3) || (nargin(fout)~=3)
            error('[%s] Coeff. depends on space, while the numerical flux function do not.',mfilename);
        else
            [M,K] = DG2D_LoopTriangle_Variable(Ax,Ay,B,fout,finw,DGMesh,physicalEntity);
        end
    elseif isa(Ax,'numeric') && isa(Ay,'numeric') && isa(B,'numeric')
        fprintf('[%s] Coeff. do not depend on space: Ax.\n',mfilename);
        [M,K] = DG2D_LoopTriangle_Constant(Ax,Ay,B,fout,finw,DGMesh,physicalEntity);
    else
        error('[%s] Some coeff. depends on space while some do not.',mfilename);
    end
end

function [M,K] = DG2D_LoopTriangle_Constant(Ax,Ay,B,fout,finw,DGMesh,physicalEntity)
%DG2D_LoopTriangle_Parallel Covers the constant-coefficient case.

        % -- Validate attributes
    validateattributes(Ax,{'double'},{'2d','nonempty'});
    Nq = size(Ax,1);
    validateattributes(Ax,{'double'},{'2d','nonempty','size',[Nq,Nq]});
    validateattributes(Ay,{'double'},{'2d','nonempty','size',[Nq,Nq]});
    validateattributes(B,{'double'},{'2d','nonempty','size',[Nq,Nq]});
    validateattributes(fout,{'function_handle'},{});
    validateattributes(fout([1,1]),{'double'},{'size',[Nq,Nq]});
    validateattributes(finw,{'function_handle'},{});
    validateattributes(finw([1,1]),{'double'},{'size',[Nq,Nq]});
    validateattributes(DGMesh,{'DGMesh_2D_Triangle'},{});
    validateattributes(physicalEntity,{'numeric'},{'scalar','integer','positive'});
        % -- Identity physical entity
    mesh = DGMesh.mesh;
    idx = find(mesh.triangles(:,1)==physicalEntity);
    Nk = length(idx);
    fprintf('[DG2D_LoopTriangle] %d triangles in zone %d.\n',Nk,physicalEntity);
    
	% -- Retrieve key arrays
    DGCell = DGMesh.DGCell;
    Np = DGCell.Np;
    nFace = mesh.n;
    vmapP = DGMesh.vmapP;
    vmapM = DGMesh.vmapM;
        % metric terms
    J = sparse(1:Nk,1:Nk,mesh.J);
    rx = sparse(1:Nk,1:Nk,mesh.rx);
    sx = sparse(1:Nk,1:Nk,mesh.sx);
    ry = sparse(1:Nk,1:Nk,mesh.ry);
    sy = sparse(1:Nk,1:Nk,mesh.sy);
	% quadrature matrices (NpxNp)
    M = DGCell.M; % [<li,lj>]_i,j
    Sr = DGCell.Sr; Ss = DGCell.Ss; % [<li,dlj/dr>]_i,j
    EpsFace = DGCell.Eps;

        % -- Build local matrix (Nq*NpxNq*Np)
    Mk = sparse(kron(M,eye(Nq)));
    Kk_rx = sparse(kron(Sr',Ax));
    Kk_sx = sparse(kron(Ss',Ax));
    Kk_ry = sparse(kron(Sr',Ay));
    Kk_sy = sparse(kron(Ss',Ay));
    Kk_b = sparse(kron(M,B));
        % -- Build global matrices: block diagonal from standard matrix
            % global mass matrix (built directly)
    M = kron(J,Mk);
            % stiffness matrix (built directly)
    K = -kron(J.*rx,Kk_rx) - kron(J.*sx,Kk_sx);
    K = K -kron(J.*ry,Kk_ry) - kron(J.*sy,Kk_sy);
    K = K + kron(J,Kk_b);
            % flux matrix 
	    %(built using i,j,v sparse syntax)
    Nflux = (DGCell.Nfp^2)*(Nq*Nq); % no. of elements in flux matrix, per face
        % upper boundary on the number of faces
    Fout_idx_i = zeros(Nflux,Nk,3); Finw_idx_i = zeros(Nflux,Nk,3);
    Fout_idx_j = zeros(Nflux,Nk,3); Finw_idx_j = zeros(Nflux,Nk,3); 
    Fout_value = zeros(Nflux,Nk,3); Finw_value = zeros(Nflux,Nk,3);

    parfor k=idx' % for each triangle
        for l=1:3 % for each face
            nk = nFace(k,:,l);
            kplus = mesh.EToE(k,l); % neighbour ('+')  
            scale = mesh.edL(k,l)/2;
            if kplus~=k % not a boundary face
                Fout = -scale*kron(EpsFace(:,:,l),fout(nk)); Fout = sparse(Fout);
                Finw = -scale*kron(EpsFace(:,:,l),finw(nk)); Finw = sparse(Finw);
                nodesM = expandIndex(vmapM(:,l,k),Nq); nodesP = expandIndex(vmapP(:,l,k),Nq); % needed if Nq>1
                %F(nodesM,nodesM) = F(nodesM,nodesM) + sparse(Fout);
                [Fout_idx_i(:,k,l), Fout_idx_j(:,k,l),Fout_value(:,k,l)] = make_sparse_index(nodesM,nodesM,Fout);
                %F(nodesM,nodesP) = F(nodesM,nodesP) + sparse(Finw);
                [Finw_idx_i(:,k,l), Finw_idx_j(:,k,l),Finw_value(:,k,l)] = make_sparse_index(nodesM,nodesP,Finw);
            end
        end
    end
        % discard null entries
    idx_out = Fout_idx_i>0;
    idx_inw = Finw_idx_i>0;
    F = sparse([Fout_idx_i(idx_out);Finw_idx_i(idx_inw)],[Fout_idx_j(idx_out);Finw_idx_j(idx_inw)],[Fout_value(idx_out);Finw_value(idx_inw)],Nq*Np*Nk,Nq*Np*Nk);
    K = K - F;
end



function [M,K] = DG2D_LoopTriangle_Variable(Ax,Ay,B,fout,finw,DGMesh,physicalEntity)
%DG2D_LoopTriangle_Variable Covers the variable-coefficient case.

        % -- Validate attributes
    validateattributes(Ax,{'function_handle'},{});
    validateattributes(Ax(1,1),{'double'},{'2d','nonempty'});
    Nq = size(Ax(1,1),1);
    validateattributes(Ay,{'function_handle'},{});
    validateattributes(Ay(1,1),{'double'},{'2d','nonempty','size',[Nq,Nq]});
    validateattributes(B,{'function_handle'},{});
    validateattributes(B(1,1),{'double'},{'2d','nonempty','size',[Nq,Nq]});
    validateattributes(fout,{'function_handle'},{});
    validateattributes(fout([1,1],1,1),{'double'},{'size',[Nq,Nq]});
    validateattributes(finw,{'function_handle'},{});
    validateattributes(finw([1,1],1,1),{'double'},{'size',[Nq,Nq]});
    validateattributes(DGMesh,{'DGMesh_2D_Triangle'},{});
    validateattributes(physicalEntity,{'numeric'},{'scalar','integer','positive'});
        
        % -- Retrieve key arrays
    mesh = DGMesh.mesh;
    DGCell = DGMesh.DGCell;
    Np = DGCell.Np;
            % metric terms
    J = mesh.J; rx = mesh.rx; sx = mesh.sx;    
    ry = mesh.ry; sy = mesh.sy;
            % coordinates and connectivity
    x = DGMesh.getNodesCoordinates(); % global coordinates [x,y]
    nFace = mesh.n;
    vmapP = DGMesh.vmapP;
    vmapM = DGMesh.vmapM;
            % quadrature matrices (NpxNp)
    M = DGCell.M;
    Sr = DGCell.Sr; Ss = DGCell.Ss;
    EpsFace = DGCell.Eps;
        % -- Identity physical entity
    idx_K = find(mesh.triangles(:,1)==physicalEntity);
    Nk = length(idx_K);
    fprintf('%d triangles in zone %d.\n',Nk,physicalEntity);   
        % -- Local matrices (Nq*NpxNq*Np)
    Mk = sparse(kron(M,eye(Nq))); % can be built directly !
    Kk = cell(0);
        % -- Set up global flux matrix (Nq*Np*NkxNq*Np*Nk)
        %(built using i,j,v sparse syntax)
    Nflux = (DGCell.Nfp^2)*(Nq*Nq); % no. of elements in flux matrix, per face
        % upper boundary on the number of interior faces
    Fout_idx_i = zeros(Nflux,Nk,3); Finw_idx_i = zeros(Nflux,Nk,3);
    Fout_idx_j = zeros(Nflux,Nk,3); Finw_idx_j = zeros(Nflux,Nk,3); 
    Fout_value = zeros(Nflux,Nk,3); Finw_value = zeros(Nflux,Nk,3);

    parfor k=idx_K' % for each triangle
            % local stiffness matrix (idx_x_el cannot be used in a parfor)
        Kk_rx = -J(k)*rx(k)*buildBlockMatrixFun2D(Sr',x((k-1)*Np+(1:Np),:),Ax);
        Kk_sx = -J(k)*sx(k)*buildBlockMatrixFun2D(Ss',x((k-1)*Np+(1:Np),:),Ax);
        Kk_ry = -J(k)*ry(k)*buildBlockMatrixFun2D(Sr',x((k-1)*Np+(1:Np),:),Ay);
        Kk_sy = -J(k)*sy(k)*buildBlockMatrixFun2D(Ss',x((k-1)*Np+(1:Np),:),Ay);
        Kk_b = J(k)*buildBlockMatrixFun2D(M,x((k-1)*Np+(1:Np),:),B);
        Kk{k} = sparse(Kk_rx + Kk_sx + Kk_ry + Kk_sy + Kk_b);
        for l=1:3 % for each face
            nk = nFace(k,:,l);
            kplus = mesh.EToE(k,l); % neighbour ('+')  
            scale = mesh.edL(k,l)/2;
            if kplus~=k % not a boundary face
                nodesM = vmapM(:,l,k); nodesP = vmapP(:,l,k);
                Fout = -scale*buildBlockMatrixFun2D(EpsFace(:,:,l),x(nodesM,:),@(x,y)(fout(nk,x,y)));
                Finw = -scale*buildBlockMatrixFun2D(EpsFace(:,:,l),x(nodesP,:),@(x,y)(finw(nk,x,y)));
                nodesM = expandIndex(nodesM,Nq); nodesP = expandIndex(nodesP,Nq); 
                %F(nodesM,nodesM) = F(nodesM,nodesM) + sparse(Fout);
                [Fout_idx_i(:,k,l), Fout_idx_j(:,k,l),Fout_value(:,k,l)] = make_sparse_index(nodesM,nodesM,Fout);
                %F(nodesM,nodesP) = F(nodesM,nodesP) + sparse(Finw);
                [Finw_idx_i(:,k,l), Finw_idx_j(:,k,l),Finw_value(:,k,l)] = make_sparse_index(nodesM,nodesP,Finw);
            end
        end
    end
        % -- Build global matrices: block diagonal from standard matrix
    M = kron(sparse(1:Nk,1:Nk,J),Mk);
    K = blkdiag(Kk{:});
        % discard null entries
    idx_out = Fout_idx_i>0;
    idx_inw = Finw_idx_i>0;
    F = sparse([Fout_idx_i(idx_out);Finw_idx_i(idx_inw)],[Fout_idx_j(idx_out);Finw_idx_j(idx_inw)],[Fout_value(idx_out);Finw_value(idx_inw)],Nq*Np*Nk,Nq*Np*Nk);
    K = K - F;
end
