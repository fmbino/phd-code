function [M,K,F,DirectDelay,BndOp,BndOpDelay] = DG2D_LEE_buildGlobalFormulation(Ax_fun,Ay_fun,B_fun,DGMesh,zoneCase,Impedance)
%DG2D_LEE_buildGlobalFormulation Build global formulation for the LEE 2D:
% The global formulation is
%       M*dX/dt + K*X = F*qs(t) + FQ_k*Q_k(Cq_k*q),
%where Q_k is a single-input single-output operator. (The expression of Q_k
%need not be known.)
% The global formulation is stored using  variables 'DirectDelay', 'BndOp' 
% and 'BndOpDelay' to ease its usage.
% Rmk: This is a simple wrapper around DG2D_buildGlobalFormulation; it 
% merely sorts the FQ into the right structure.
% Inputs:
%   Ax_fun,Ay_fun,B_fun,DGMesh,zoneCase see DG2D_buildGlobalFormulation
%   Impedance (ImpedanceBoundaryCondition or ImpedanceBoundaryCondition_ZY)
%       describes impedance boundary condition
% Outputs:
%   M,K,F global formulation
%   DirectDelay (cell of structure). Empty if no direct delay. Fields:
%       - F
%       - C
%       - Delay
%   BndOp (cell of structure). Empty if no operator. Fields:
%       - F
%       - C
%       - Operator
%   BndOpDelay (cell of structure). Empty if no operator. Fields:
%       - F
%       - C
%       - Operator
%       - Delay
    
        % (0) Validate argument
        % (Done by DG2D_buildGlobalFormulation)
    if ~isa(Impedance,'ImpedanceBoundaryCondition') && ...
            ~isa(Impedance,'ImpedanceBoundaryCondition_ZY')
        error('[%s] Impedance is not of expected type.',mfilename);
    end

        % (1) Build global formulation
        % (Object Impedance not needed here.)
    fprintf('[%s] Building global formulation...\n',mfilename);
    [M,K,F,FQ,CQ] = DG2D_buildGlobalFormulation(Ax_fun,Ay_fun,B_fun,DGMesh,zoneCase);
        % (2) Conversion: FQ,CQ -> DirectDelay, BndOp, BndOpDelay
        % This is a simple sorting operation, based on the secondary names
        % of zones given in zoneCase.
        % Operators in Impedance are matched to their corresponding
        % matrices F and C.
    DirectDelay = cell(0);
    BndOp = cell(0);
    BndOpDelay = cell(0);
    k = 0; % No. of generic boundary conditions found
    N_DD = 0; % No. of direct delay found
    N_BndOp = 0; % No. of boundary operators found
    N_BndOpDelay = 0; % No. of delayed boundary operators found
    for i=1:length(zoneCase)
        if strcmp(zoneCase{i}.name,'Boundary_Generic_Operator')
            k=k+1;
            switch zoneCase{i}.secondaryName  % what kind of generic boundary condition?
                case 'Impedance_DirectDelay'
                    N_DD = N_DD + 1;
                    DirectDelay{end+1}=struct('F',FQ{k},'C',CQ{k},'Delay',Impedance.DirectDelay{N_DD}.Delay);
                case 'Impedance_BoundaryOperator'
                    N_BndOp = N_BndOp + 1;
                    BndOp{end+1}=struct('F',FQ{k},'C',CQ{k},'Operator',Impedance.BndOp{N_BndOp}.Operator);
                case 'Impedance_BoundaryOperatorDelayed'
                    N_BndOpDelay = N_BndOpDelay + 1;
                    BndOpDelay{end+1}=struct('F',FQ{k},'C',CQ{k},'Operator',Impedance.BndOpDelay{N_BndOpDelay}.Operator,'Delay',Impedance.BndOpDelay{N_BndOpDelay}.Delay);
                otherwise
                    fprintf('[%s] Unknown generic boundary condition: %s.\n',mfilename,zoneCase{i}.secondaryName);
            end
        end
    end
    fprintf('[%s] Generic boundary conditions found:\n',mfilename);
    fprintf('\t Direct delay: %d\n',length(DirectDelay));
    fprintf('\t Boundary operator: %d\n',length(BndOp));
    fprintf('\t Delayed boundary operator: %d\n',length(BndOpDelay));
end