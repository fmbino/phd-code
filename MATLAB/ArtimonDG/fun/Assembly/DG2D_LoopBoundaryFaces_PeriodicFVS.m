function K = DG2D_LoopBoundaryFaces_PeriodicFVS(fout,finw,DGMesh,physicalEntities)
%DG2D_LoopBoundaryFaces_PeriodicFVS Build the stiffness matrix which
%results from the periodicity condition.
% Inputs:
%   fout,finw (function_handle) FVS split: outward and inward
%       fout(n) and finw(n)
%   DGMesh (DGMesh_2D_Triangle) DG mesh
%   physicalEntity (?) number of each physical entity (zone)
% Outputs:
%   K (Nk*Np*Nq x Nk*Np*Nq) (sparse) stiffness matrix

    % -- Validate attributes
validateattributes(fout,{'function_handle'},{},mfilename,'fout');
validateattributes(fout([1,1]),{'double'},{'2d','nonempty'},mfilename,'fout([1,1])');
Nq = size(fout([1,1]),1);
validateattributes(finw,{'function_handle'},{},mfilename,'finw');
validateattributes(finw([1,1]),{'double'},{'size',[Nq,Nq]},mfilename,'finw([1,1])');
validateattributes(DGMesh,{'DGMesh_2D_Triangle'},{},mfilename,'DGMesh');
validateattributes(physicalEntities,{'numeric'},{'vector','integer','positive','numel',2},mfilename,'physicalEntities');
    % -- Retrieve key arrays
mesh = DGMesh.mesh;
DGCell = DGMesh.DGCell;
Np = DGCell.Np;
Nk = mesh.N;
EpsFace = DGCell.Eps;
nFace = mesh.n;
vmapP = DGMesh.vmapP;
vmapM = DGMesh.vmapM;

    % -- Process boundaries
        % Check that boundaries are conforming
idx_1 = find(mesh.boundaryFaces(:,1)==physicalEntities(1));
idx_2 = find(mesh.boundaryFaces(:,1)==physicalEntities(2));
Nper = length(idx_1);
if length(idx_1)==length(idx_2)
    fprintf('[%s] Conforming boundaries.\n',mfilename);
    fprintf('[%s] %d faces in zones %s.\n',mfilename,Nper,mat2str(physicalEntities));
else
    error('[%s] Boundaries not conforming.\n',mfilename);
end
        % Modify boundary face connectivity
FToE = zeros(Nper,2);
FToF = zeros(Nper,2);
for i=1:Nper % for each boundary face
    FToE(i,:) = [mesh.FToE(idx_1(i)), mesh.FToE(idx_2(i))];
    FToF(i,:) = [mesh.FToF(idx_1(i)), mesh.FToF(idx_2(i))];
end

    % -- Set up stiffness matrix
    %(built using i,j,v sparse syntax)
Nflux = (DGCell.Nfp^2)*(Nq*Nq); % no. of elements in flux matrix, per face
Nface = Nper; % upper boundary on the number of faces
K_idx_i = zeros(Nface*Nflux,1); % line index
K_idx_j = zeros(Nface*Nflux,1); % column index
K_value = zeros(Nface*Nflux,1); % values
idx_K = 0; % counter

for i=1:Nper % for each boundary face
        % connects to element k, local face l
        %          and element kplus, local face lplus
    k = FToE(i,1); kplus = FToE(i,2); 
    l = FToF(i,1); lplus = FToF(i,2);
    nk = nFace(k,:,l); nkplus = nFace(kplus,:,lplus);    
    edL = mesh.edL(k,1); 
    scale = (edL/2);
        % element k
    Fout = -scale*kron(EpsFace(:,:,l),fout(nk)); Fout = sparse(Fout);
    Finw = -scale*kron(EpsFace(:,:,l),finw(nk)); Finw = sparse(Finw);
    nodesM = expandIndex(vmapM(:,l,k),Nq); nodesP = expandIndex(vmapM(:,lplus,kplus),Nq);% to be optimised!
    %K(nodesM,nodesM) = K(nodesM,nodesM) - Fout;
    [K_idx_i(idx_K+(1:Nflux)), K_idx_j(idx_K+(1:Nflux)),K_value(idx_K+(1:Nflux))] = make_sparse_index(nodesM,nodesM,-Fout); 
    idx_K = idx_K + Nflux;
    %K(nodesM,nodesP) = K(nodesM,nodesP) - Finw;
    [K_idx_i(idx_K+(1:Nflux)), K_idx_j(idx_K+(1:Nflux)),K_value(idx_K+(1:Nflux))] = make_sparse_index(nodesM,nodesP,-Finw);
    idx_K = idx_K + Nflux;
        % element kplus
    Fout = -scale*kron(EpsFace(:,:,lplus),fout(nkplus)); Fout = sparse(Fout);
    Finw = -scale*kron(EpsFace(:,:,lplus),finw(nkplus)); Finw = sparse(Finw);
    nodesM = expandIndex(vmapM(:,lplus,kplus),Nq); nodesP = expandIndex(vmapM(:,l,k),Nq);% to be optimised!
    %K(nodesM,nodesM) = K(nodesM,nodesM) - Fout;
    [K_idx_i(idx_K+(1:Nflux)), K_idx_j(idx_K+(1:Nflux)),K_value(idx_K+(1:Nflux))] = make_sparse_index(nodesM,nodesM,-Fout);
    idx_K = idx_K + Nflux;
    %K(nodesM,nodesP) = K(nodesM,nodesP) - Finw;
    [K_idx_i(idx_K+(1:Nflux)), K_idx_j(idx_K+(1:Nflux)),K_value(idx_K+(1:Nflux))] = make_sparse_index(nodesM,nodesP,-Finw);
    idx_K = idx_K + Nflux;
end
    K = sparse(K_idx_i,K_idx_j,K_value,Nq*Np*Nk,Nq*Np*Nk);
end