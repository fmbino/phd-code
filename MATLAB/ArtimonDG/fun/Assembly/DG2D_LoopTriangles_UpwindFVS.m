function [M,K] = DG2D_LoopTriangles_UpwindFVS(Ax,Ay,B,fout,finw,DGMesh,physicalEntity)
%DG2D_LoopTriangles_UpwindFVS Build the mass and stiffness matrix, by looping over
%each triangle, and interior faces.
%               M = [(l_j,l_i)_k * eye(Nq)]_(i,j)
%               K = [-(l_j*Am,dl_i/dm)_k + (l_j*B,l_i)_k]_(i,j),
%where (,)_k is the surface integral over element k.
% Inputs:
%   Ax, Ay, B   (NqxNq) or 
%               (function_handle) Ax(x,y), Ay(x,y), B(x,y) is (NqxNq)
%   fout,finw (function_handle) FVS split: outward and inward
%       (fout(n) and finw(n)) or (fout(n,x,y) and finw(n,x,y))
%   DGMesh (DGMesh_2D_Triangle) DG mesh
%   physicalEntity (?) number of each physical entity (zone)
% Outputs:
%   M (Nk*Np*Nq x Nk*Np*Nq) (sparse) mass matrix
%   K (Nk*Np*Nq x Nk*Np*Nq) (sparse) stiffness matrix

        % Constant or variable coefficients ?
        % (Assembly functions are too dissimilar to be merged in a readable
        % fashion.)
    if isa(Ax,'function_handle') && isa(Ay,'function_handle') && ...
       isa(B,'function_handle')
        fprintf('[%s] Coeff. depends on space: Ax(x,y).\n',mfilename);
        if (nargin(finw)~=3) || (nargin(fout)~=3)
            error('[%s] Coeff. depends on space, while the numerical flux function do not.',mfilename);
        else
            [M,K] = DG2D_LoopTriangle_Variable(Ax,Ay,B,fout,finw,DGMesh,physicalEntity);
        end
    elseif isa(Ax,'numeric') && isa(Ay,'numeric') && isa(B,'numeric')
        fprintf('[%s] Coeff. do not depend on space: Ax.\n',mfilename);
        [M,K] = DG2D_LoopTriangle_Constant(Ax,Ay,B,fout,finw,DGMesh,physicalEntity);
    else
        error('[%s] Some coeff. depends on space while some do not.',mfilename);
    end
end

function [M,K] = DG2D_LoopTriangle_Constant(Ax,Ay,B,fout,finw,DGMesh,physicalEntity)
%DG2D_LoopTriangle_Constant Covers the constant-coefficients case.
        % -- Validate attributes
    validateattributes(Ax,{'double'},{'2d','nonempty'},mfilename,'Ax');
    Nq = size(Ax,1);
    validateattributes(Ax,{'double'},{'2d','nonempty','size',[Nq,Nq]},mfilename,'Ax');
    validateattributes(Ay,{'double'},{'2d','nonempty','size',[Nq,Nq]},mfilename,'Ay');
    validateattributes(B,{'double'},{'2d','nonempty','size',[Nq,Nq]},mfilename,'B');
    validateattributes(fout,{'function_handle'},{},mfilename,'fout');
    validateattributes(fout([1,1]),{'double'},{'size',[Nq,Nq]},mfilename,'fout([1,1])');
    validateattributes(finw,{'function_handle'},{},mfilename,'finw');
    validateattributes(finw([1,1]),{'double'},{'size',[Nq,Nq]},mfilename,'finw([1,1])');
    validateattributes(DGMesh,{'DGMesh_2D_Triangle'},{},mfilename,'DGMesh');
    validateattributes(physicalEntity,{'numeric'},{'scalar','integer','positive'},mfilename,'physicalEntity');
    
        % -- Retrieve key arrays
    mesh = DGMesh.mesh;
    DGCell = DGMesh.DGCell;
    Np = DGCell.Np;
            % connectivity
    nFace = mesh.n;
    vmapP = DGMesh.vmapP;
    vmapM = DGMesh.vmapM;
            % quadrature matrices (NpxNp)
    M = DGCell.M; % [<li,lj>]_i,j
    Sr = DGCell.Sr; Ss = DGCell.Ss; % [<li,dlj/dr>]_i,j
    EpsFace = DGCell.Eps;
    
        % -- Identity physical entity
    idx = find(mesh.triangles(:,1)==physicalEntity);
    Nk = length(idx);
    fprintf('[%s] %d triangles in zone %d.\n',mfilename,Nk,physicalEntity);
    
        % -- Metric terms
    J = sparse(1:Nk,1:Nk,mesh.J);
    rx = sparse(1:Nk,1:Nk,mesh.rx);
    sx = sparse(1:Nk,1:Nk,mesh.sx);
    ry = sparse(1:Nk,1:Nk,mesh.ry);
    sy = sparse(1:Nk,1:Nk,mesh.sy);

        % -- Local matrix (Nq*NpxNq*Np)
    Mk = sparse(kron(M,eye(Nq)));
    Kk_rx = sparse(kron(Sr',Ax));
    Kk_sx = sparse(kron(Ss',Ax));
    Kk_ry = sparse(kron(Sr',Ay));
    Kk_sy = sparse(kron(Ss',Ay));
    Kk_b = sparse(kron(M,B));
    
        % -- Build global matrices: block diagonal from standard matrix
            % global mass matrix (built directly)
    M = kron(J,Mk);
            % stiffness matrix (built directly)
    K = -kron(J.*rx,Kk_rx) - kron(J.*sx,Kk_sx);
    K = K -kron(J.*ry,Kk_ry) - kron(J.*sy,Kk_sy);
    K = K + kron(J,Kk_b);
            % flux matrix 
	    %(built using i,j,v sparse syntax)
    Nflux = (DGCell.Nfp^2)*(Nq*Nq); % no. of elements in flux matrix, per face
    Nface = 3*Nk; % upper boundary on the number of faces
    F_idx_i = zeros(2*Nface*Nflux,1); % line index
    F_idx_j = zeros(2*Nface*Nflux,1); % column index
    F_value = zeros(2*Nface*Nflux,1); % values
    idx_F = 0; % counter

    for k=idx' % for each triangle
        for l=1:3 % for each face
            nk = nFace(k,:,l);
            kplus = mesh.EToE(k,l); lplus = mesh.EToF(k,l); nkplus=nFace(kplus,:,lplus); % neighbour ('+')  
            scale = mesh.edL(k,l)/2;
            if kplus~=k % not a boundary face
                Fout = -scale*kron(EpsFace(:,:,l),fout(nk)); Fout = sparse(Fout);
                Finw = -scale*kron(EpsFace(:,:,l),finw(nk)); Finw = sparse(Finw);
                nodesM = expandIndex(vmapM(:,l,k),Nq); nodesP = expandIndex(vmapP(:,l,k),Nq); % needed if Nq>1
                %F(nodesM,nodesM) = F(nodesM,nodesM) + Fout;
                [F_idx_i(idx_F+(1:Nflux)), F_idx_j(idx_F+(1:Nflux)),F_value(idx_F+(1:Nflux))] = make_sparse_index(nodesM,nodesM,Fout);
                idx_F = idx_F + Nflux;
                %F(nodesM,nodesP) = F(nodesM,nodesP) + Finw;
                [F_idx_i(idx_F+(1:Nflux)), F_idx_j(idx_F+(1:Nflux)),F_value(idx_F+(1:Nflux))] = make_sparse_index(nodesM,nodesP,Finw);
                idx_F = idx_F + Nflux;
            end
        end
    end
    idx = F_idx_i>0;
    F = sparse(F_idx_i(idx),F_idx_j(idx),F_value(idx),Nq*Np*Nk,Nq*Np*Nk);
    K = K - F;
end

function [M,K] = DG2D_LoopTriangle_Variable(Ax,Ay,B,fout,finw,DGMesh,physicalEntity)
%DG2D_LoopTriangle_Variable Covers the variable-coefficient case.

        % -- Validate attributes
    validateattributes(Ax,{'function_handle'},{},mfilename,'Ax');
    validateattributes(Ax(1,1),{'double'},{'2d','nonempty'},mfilename,'Ax(1,1)');
    Nq = size(Ax(1,1),1);
    validateattributes(Ay,{'function_handle'},{},mfilename,'Ay');
    validateattributes(Ay(1,1),{'double'},{'2d','nonempty','size',[Nq,Nq]},mfilename,'Ay(1,1)');
    validateattributes(B,{'function_handle'},{},mfilename,'B');
    validateattributes(B(1,1),{'double'},{'2d','nonempty','size',[Nq,Nq]},mfilename,'B(1,1)');
    validateattributes(fout,{'function_handle'},{},mfilename,'fout');
    validateattributes(fout([1,1],1,1),{'double'},{'size',[Nq,Nq]},mfilename,'fout([1,1],1,1)');
    validateattributes(finw,{'function_handle'},{},mfilename,'finw');
    validateattributes(finw([1,1],1,1),{'double'},{'size',[Nq,Nq]},mfilename,'finw([1,1],1,1)');
    validateattributes(DGMesh,{'DGMesh_2D_Triangle'},{},mfilename,'DGMesh');
    validateattributes(physicalEntity,{'numeric'},{'scalar','integer','positive'},mfilename,'physicalEntity');
        
        % -- Retrieve key arrays
    mesh = DGMesh.mesh;
    DGCell = DGMesh.DGCell;
    Np = DGCell.Np;
            % coordinates and connectivity
    x = DGMesh.getNodesCoordinates(); % global coordinates [x,y]
    idx_x_el = @(k)(DGMesh.idx_q_el(k,1));
    nFace = mesh.n;
    vmapP = DGMesh.vmapP;
    vmapM = DGMesh.vmapM;
            % quadrature matrices (NpxNp)
    M = DGCell.M;
    Sr = DGCell.Sr; Ss = DGCell.Ss;
    EpsFace = DGCell.Eps;
        
        % -- Identity physical entity
    idx_K = find(mesh.triangles(:,1)==physicalEntity);
    Nk = length(idx_K);
    fprintf('[%s] %d triangles in zone %d.\n',mfilename,Nk,physicalEntity);
    
        % -- Metric terms
    J = mesh.J; rx = mesh.rx; sx = mesh.sx;    
    ry = mesh.ry; sy = mesh.sy;
    
        % -- Local matrix (Nq*NpxNq*Np)
    Mk = sparse(kron(M,eye(Nq))); % can be built directly !
    Kk = cell(0);
    
        % -- Set up global flux matrix (Nq*Np*NkxNq*Np*Nk)
        %(built using i,j,v sparse syntax)
    Nflux = (DGCell.Nfp^2)*(Nq*Nq); % no. of elements in flux matrix, per face
    Nfaceint = 3*Nk; % upper boundary on the number of interior faces
    F_idx_i = zeros(2*Nfaceint*Nflux,1);
    F_idx_j = zeros(2*Nfaceint*Nflux,1);
    F_value = zeros(2*Nfaceint*Nflux,1);
    idx_F = 0;

    for k=idx_K' % for each triangle
            % local stiffness matrix
        Kk_rx = -J(k)*rx(k)*buildBlockMatrixFun2D(Sr',x(idx_x_el(k),:),Ax);
        Kk_sx = -J(k)*sx(k)*buildBlockMatrixFun2D(Ss',x(idx_x_el(k),:),Ax);
        Kk_ry = -J(k)*ry(k)*buildBlockMatrixFun2D(Sr',x(idx_x_el(k),:),Ay);
        Kk_sy = -J(k)*sy(k)*buildBlockMatrixFun2D(Ss',x(idx_x_el(k),:),Ay);
        Kk_b = J(k)*buildBlockMatrixFun2D(M,x(idx_x_el(k),:),B);
        Kk{k} = sparse(Kk_rx + Kk_sx + Kk_ry + Kk_sy + Kk_b);
        for l=1:3 % for each face
            nk = nFace(k,:,l);
            kplus = mesh.EToE(k,l); % neighbour ('+')  
            scale = mesh.edL(k,l)/2;
            if kplus~=k % not a boundary face
                nodesM = vmapM(:,l,k); nodesP = vmapP(:,l,k);
                Fout = -scale*buildBlockMatrixFun2D(EpsFace(:,:,l),x(nodesM,:),@(x,y)(fout(nk,x,y)));
                Finw = -scale*buildBlockMatrixFun2D(EpsFace(:,:,l),x(nodesP,:),@(x,y)(finw(nk,x,y)));
                nodesM = expandIndex(nodesM,Nq); nodesP = expandIndex(nodesP,Nq); % to be optimised! (array in DGCase) 
                %F(nodesM,nodesM) = F(nodesM,nodesM) + sparse(Fout);
                [F_idx_i(idx_F+(1:Nflux)), F_idx_j(idx_F+(1:Nflux)),F_value(idx_F+(1:Nflux))] = make_sparse_index(nodesM,nodesM,Fout);
                idx_F = idx_F + Nflux;
                %F(nodesM,nodesP) = F(nodesM,nodesP) + sparse(Finw);
                [F_idx_i(idx_F+(1:Nflux)), F_idx_j(idx_F+(1:Nflux)),F_value(idx_F+(1:Nflux))] = make_sparse_index(nodesM,nodesP,Finw);
                idx_F = idx_F + Nflux;
            end
        end
    end
        % -- Build global matrices: block diagonal from standard matrix
    M = kron(sparse(1:Nk,1:Nk,J),Mk);
    K = blkdiag(Kk{:});
    idx = F_idx_i>0;
    F = sparse(F_idx_i(idx),F_idx_j(idx),F_value(idx),Nq*Np*Nk,Nq*Np*Nk);
    K = K - F;
end
