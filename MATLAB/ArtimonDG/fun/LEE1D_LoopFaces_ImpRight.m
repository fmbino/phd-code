function [M,K,F_delay,I_delay] = LEE1D_LoopFaces_ImpRight(mesh,cell,Nq,f0,f1,fd1,D1,fd2,D2,idx_q_el,idx_q_pt)
%ArtimonDG_loopFace_ImpRight Build contribution of the right boundary:
%impedance boundary condition.
% Centred flux, with ghost state
% f*(q,nx) = Ax*nx*(q+q_ghost)/2
%          = f0(nx)*q + f1(nx)*dq/qt + fd1(nx)*D1(q) + fd2(nx)*D2(q)
% Assumptions on the second diffusive operator D2:
%   - It is delayed
%   - It observes the same diffusive variables as D1
% Monodimensional LEE, global formulation:
%    M dx/dt + K dx/dt = F_delay*xd(t-dt).
% Inputs:
%   mesh (Mesh_1D_Uniform)
%   cell (DGCell_1D)
%   Nq (1) Number of variables
%   f0 (1) -> (NqxNq) Dependency on q
%   f1 (1) -> (NqxNq) Dependency on dq/dt
%   fd1 (1) -> (Nq) Dependency on D1(q)
%   D1 (Diffusive_Operator) Diffusive operator D1
%   fd2 (1) -> (Nq) Dependency on D2(q)
%   D2 (Diffusive_Operator) Diffusive operator D2
%   idx_q_el index function (cell k)
%   idx_q_pt index function (cell k, node n)
% Outputs:
%   M (N*Nx*Nq+Nxi x N*Nx*Nq+Nxi) Global mass matrix
%   K (N*Nx*Nq+Nxi x N*Nx*Nq+Nxi) Global stiffness matrix
%   F_delay (N*Nx*Nq+Nxi x [xd]) Global delay flux matrix
%   I_delay ([xd]) Indices of x that are delayed
% Rmk: [xd] = 1+Nxi. (delayed variables)

    % -- Validate arguments
    validateattributes(mesh,{'Mesh_1D_Uniform'},{});
    validateattributes(cell,{'DGCell_1D'},{});
    validateattributes(Nq,{'numeric'},{'scalar','positive'});
    validateattributes(f0,{'function_handle'},{});
    validateattributes(f0(1),{'numeric'},{'size',[Nq,Nq]});
    validateattributes(f1,{'function_handle'},{});
    validateattributes(f1(1),{'numeric'},{'size',[Nq,Nq]});
    validateattributes(fd1,{'function_handle'},{});
    validateattributes(fd1(1),{'numeric'},{'size',[Nq,1]});
    validateattributes(D1,{'Diffusive_Operator'},{});
    validateattributes(fd2,{'function_handle'},{});
    validateattributes(fd2(1),{'numeric'},{'size',[Nq,1]});
    validateattributes(D2,{'Diffusive_Operator'},{});
    validateattributes(idx_q_el,{'function_handle'},{});
    validateattributes(idx_q_pt,{'function_handle'},{});
    
    % -- Constants
    xi_i = cell.xi;
    N = cell.N;
    Nx = mesh.N;
        % Value of the Lagrange poly n°i
    L1 = transpose(VandermondeLagrange1D(xi_i,1)); % (li(1))i
    
    % -- Allocations
    F0 = sparse(1,1,0,Nx*N*Nq,Nx*N*Nq);
    F1 = sparse(1,1,0,Nx*N*Nq,Nx*N*Nq);
    Fd1 = sparse(1,1,0,Nx*N*Nq,1);
    Fd2 = sparse(1,1,0,Nx*N*Nq,1);
    F_delay = [];
    I_delay = [];
    
    % Right end of the domain = cell left of the last face
    Fblock_0 = sparse(BuildBlockMatrix(-L1,f0(1))); % -li(1)*f0(nx=1)
    Fblock_1 = sparse(BuildBlockMatrix(-L1,f1(1))); % -li(1)*f1(nx=1)
    Fblock_d1 = sparse(BuildBlockMatrix(-L1,fd1(1))); % -li(1)*fd1(nx=1)
    Fblock_d2 = sparse(BuildBlockMatrix(-L1,fd2(1))); % -li(1)*fd2(nx=1)
    % loop over impedance boundary faces
    for k=1:length(mesh.Face_right_bound) % right boundary
        kl=mesh.Face_right_bound(k,1);
            % Build F0,F1,Fd1,Fd2
        F0(idx_q_el(kl),idx_q_pt(kl,N)) =   Fblock_0;
        F1(idx_q_el(kl),idx_q_pt(kl,N)) =   Fblock_1;
        Fd1(idx_q_el(kl),1)             =   Fblock_d1;
        Fd2(idx_q_el(kl),1)             =   Fblock_d2;
            % Include F0 and F1 in M and K
        M = -F1;
        K = -F0;
        fprintf('[ImpRight] F0 and F1 built and integrated to M and K.\n');
        
        if (sum(fd1(1)~=0) || sum(fd2(1)~=0)) % at least one diffusive operator
                % Identify node at the boundary
            delta = sparse(1,1,0,Nx*N*Nq,1);
            idx_el = idx_q_pt(kl,N); % (Nq) indexes of element kl, node N 
            delta(idx_el(1)) = 1; % index of (normal) acoustic speed
                % Which set of diffusive variables?
        if (sum(fd1(1)~=0) && sum(fd2(1)~=0)) % both operators non null
            if sum(D1.xi~=D2.xi)>0
                error('[ImpRight] Diff1 and Diff2 do not observe the same diffusive variables. (Case not implemented.)');
            end
            fprintf('[ImpRight] Both observers are non-null and observe the same state.\n');
            xi = D1.xi;
        else
            fprintf('[ImpRight] One of the operator is null.\n');
            fprintf('[ImpRight] Using the diffusive variables of the non-null operator.\n');
            xi = (D1.xi)*(sum(fd1(1)~=0)~=0) + (D2.xi)*(sum(fd2(1)~=0)~=0);
        end
                % Add diffusive variables
            [M,K] = addDiffusiveVariables(M,K,xi,delta);
                % Add diffusive operator D1
            [M,K] = addDiffusiveOperator(M,K,Fd1,D1.C,D1.D,delta);
                % Include Fd2 in F_delay
            if(fd2(1)~=0)
                Nxi = length(D1.xi);
                F_delay = sparse(1,1,0,Nx*N*Nq+Nxi,1+Nxi);
                    % upper left block
                F_delay(1:Nx*N*Nq,1) = D2.D*Fd2;
                    % upper right block
                F_delay(1:(Nx*N*Nq),1+(1:Nxi)) = kron(Fd2(:),D2.C(:)');
                    % Indices of x that are delayed
                I_delay(1) = idx_el(1);
                I_delay(1+(1:Nxi))=Nx*N*Nq+(1:Nxi);
                fprintf('[ImpRight] F_delay built to account for D2 (assumed delayed).\n');
                fprintf('[ImpRight] %d variables are delayed.\n',length(I_delay));
            else
                fprintf('[ImpRight] No diffusive operator D2 specified.\n');
                fprintf('[ImpRight] D2 ignored.\n');
            end
        else
            fprintf('[ImpRight] No diffusive operator specified.\n');
        end
    end
end

function [M,K] = addDiffusiveVariables(M,K,xi,delta)
% addDiffusiveVariables Extended the mass and stiffness matrices to add Nxi
% equations:
%   dphi/dt +xi*phi = delta.q
% Inputs:
%   M (N*Nx*Nqx N*Nx*Nq) Global mass matrix
%   K (N*Nx*Nqx N*Nx*Nq) Global stiffness matrix
%   xi (Nxi) Diffusive poles (rad/s)
%   delta (N*Nx*Nq) Index vector (define observed variable)
% Outputs:
%   M (N*Nx*Nq + Nxi x N*Nx*Nq + Nxi) Extended global mass matrix
%   K (N*Nx*Nq + Nxi x N*Nx*Nq + Nxi) Extended global stiffness matrix

        % Constants
    Nxi = length(xi);
    Nt = size(M,1);
        % Update and extend stiffness matrix M
    M(end+(1:Nxi),end+(1:Nxi)) = speye(Nxi,Nxi);
        % Update and extend stiffness matrix K
            % lower left block
    K(end+(1:Nxi),:) = -kron(ones(Nxi,1),delta(:)'); 
        % lower right block
    K(Nt+(1:Nxi),end+(1:Nxi)) = diag(xi);
    fprintf('[ImpRight] M and K extended: %d additional variables.\n',Nxi);
end

function [M,K] = addDiffusiveOperator(M,K,Fd,C,D,delta)
%addDiffusiveOperator Add a diffusive operator to the (already extended)
%mass and stiffness matrices.
% Inputs:
%   M (N*Nx*Nq + Nxi x N*Nx*Nq + Nxi) Extended global mass matrix
%   K (N*Nx*Nq + Nxi x N*Nx*Nq + Nxi) Extended global stiffness matrix
%   Fd (N*Nx*Nqx1)  Global flux vector
%   C (Nxi) Observer output matrix
%   D (1) Observer feedtrough matrix
%   delta (N*Nx*Nq) Index vector (define observed variable)
% Ouputs:
%   M (N*Nx*Nq + Nxi x N*Nx*Nq + Nxi) Updated global mass matrix
%   K (N*Nx*Nq + Nxi x N*Nx*Nq + Nxi) Updated global stiffness matrix

        % Retrieve info from diffusive operator D1
    Nxi = length(C);
    Nt = size(M,1) - Nxi;
       % Update and extend stiffness matrix K
            % upper left block
    K(1:Nt,1:Nt) = K(1:Nt,1:Nt) - (D)*(kron(Fd(:),delta(:)'));
            % upper right block
    K(1:Nt,Nt+(1:Nxi)) = -kron(Fd(:),C(:)');
    fprintf('[ImpRight] M and K updated to account for D1.\n');
end