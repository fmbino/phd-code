function F = LEE1D_LoopFaces_Interior_FVS(mesh,xi_i,N,Nq, Fn_int,Fn_ext)
%ArtimonDG_Build_Fluxes Build the fluxes matrix.
%               dq/dt + d(Ax*q)/dx + B*q = 0
% Inputs:
%   mesh (Mesh_1D_Uniform)
%   xi_i (Nx1) positions of nodes in each elements, within [-1,1] 
%   N (1x1) No of DoFs per cell
%   Nq (1x1) No. of variable
%   Fn_int (1->NqxNq) outward flux f+(n) (FVS)
%   Fn_ext (1->NqxNq) inward flux f-(n) (FVS)

% Outputs:
%   F (N*Nx*Nq x N*Nx*Nq) Global flux matrix, which only accounts for the
%   interior faces.

Nx = mesh.N;
% Index for element k and point n°n in the global vector q
% k in [[1,Nk]]
% n in [[1,N]]
% ! zero-based (1,1) = 0.
% range for the whole element k
idx_q_el=@(k)((k-1)*N*Nq+(1:(N*Nq)));
% range for the whole point n°n on element k
idx_q_pt=@(k,n)((k-1)*N*Nq+(n-1)*Nq+(1:Nq));

    % Value of the Lagrange poly n°i
L1 = transpose(VandermondeLagrange1D(xi_i,1)); % (li(1))i
Lm1 = transpose(VandermondeLagrange1D(xi_i,-1)); % (li(-1))i

        % Flux vector splitting
        % (Conservativity property is used to bypass the normal)
        %       Local weak form flux matrices: (N*NqxNq)
            % cell left of the face
Fn_FVS_left_left = sparse(BuildBlockMatrix(-L1,Fn_int(1)));
Fn_FVS_left_right = sparse(BuildBlockMatrix(-L1,Fn_ext(1)));
            % cell right of the face
Fn_FVS_right_left = sparse(BuildBlockMatrix(Lm1,Fn_int(1)));
Fn_FVS_right_right = sparse(BuildBlockMatrix(Lm1,Fn_ext(1)));


Nfaceint = size(mesh.Face_int,1); % no. of interior faces
%     % FIRST STRATEGY : SLOW (Direct indexing of sparse matrix)
%     %-- Loop over interior face : global static flux matrix
%     % Global Flux matrix
%  F = sparse(1,1,0,Nx*N*Nq,Nx*N*Nq);
% Nf = Nfaceint*(2*N*Nq); % no. of element in F
% %F = sparse(1,1,0,Nx*N*Nq,Nx*N*Nq,Nf);
% for k=1:Nfaceint % loop over every interior face
%     kl=mesh.Face_int(k,1); kr=mesh.Face_int(k,2);
%         % Contribution to the element n°kl:=k-1
%     F(idx_q_el(kl),idx_q_pt(kl,N))=Fn_FVS_left_left;% dep. on itself
%     F(idx_q_el(kl),idx_q_pt(kr,1))=Fn_FVS_left_right;% dep. on the right element
%         % Contribution to the element n°kr:=k
%     F(idx_q_el(kr),idx_q_pt(kl,N)) = Fn_FVS_right_left; % dep. on the left element
%     F(idx_q_el(kr),idx_q_pt(kr,1))= Fn_FVS_right_right; % dep. on itself
% end


    % SECOND STRATEGY : FAST, but less readable (Separate sparse building)
    %-- Loop over interior face : global static flux matrix
    % Global Flux matrix
Nflux = N*(Nq)^2; % No. of elements in flux matrix PER mesh element
F_idx_i = zeros(1,Nfaceint*Nflux);
F_idx_j = zeros(1,Nfaceint*Nflux);
F_value = zeros(Nfaceint*Nflux,1);
idx_F = 0;
for k=1:Nfaceint % loop over every interior face
    kl=mesh.Face_int(k,1); kr=mesh.Face_int(k,2);
        % Contribution to the element n°kl:=k-1
    [idx_i, idx_j,val] = make_sparse_index(idx_q_el(kl),idx_q_pt(kl,N),Fn_FVS_left_left); % dep. on itself
    F_idx_i(1,idx_F+(1:Nflux)) = idx_i;
    F_idx_j(1,idx_F+(1:Nflux)) = idx_j;
    F_value(idx_F+(1:Nflux),1) = val;
    idx_F = idx_F + Nflux;
    [idx_i, idx_j,val] = make_sparse_index(idx_q_el(kl),idx_q_pt(kr,1),Fn_FVS_left_right); % dep. on the right element
    F_idx_i(1,idx_F+(1:Nflux),1) = idx_i;
    F_idx_j(1,idx_F+(1:Nflux),1) = idx_j;
    F_value(idx_F+(1:Nflux),1) = val;
    idx_F = idx_F + Nflux;
%         % Contribution to the element n°kr:=k
    [idx_i, idx_j,val] = make_sparse_index(idx_q_el(kr),idx_q_pt(kl,N),Fn_FVS_right_left); % dep. on the left element
    F_idx_i(1,idx_F+(1:Nflux),1) = idx_i;
    F_idx_j(1,idx_F+(1:Nflux),1) = idx_j;
    F_value(idx_F+(1:Nflux),1) = val;
    idx_F = idx_F + Nflux;
    [idx_i, idx_j,val] = make_sparse_index(idx_q_el(kr),idx_q_pt(kr,1),Fn_FVS_right_right); % dep. on itself
    F_idx_i(1,idx_F+(1:Nflux),1) = idx_i;
    F_idx_j(1,idx_F+(1:Nflux),1) = idx_j;
    F_value(idx_F+(1:Nflux),1) = val;
    idx_F = idx_F + Nflux;
end
    % Build the sparse matrix with the value collected
F = sparse(F_idx_i,F_idx_j,F_value,Nx*N*Nq,Nx*N*Nq);
end

function [idx_i, idx_j,val] = make_sparse_index(i,j,F)
%make_sparse_index A(i,j) designates length(i)*length(j) elements of A,
%given by idx_i and idx_j.
% Returns vectors suited for building sparse matrix with 'sparse'. 
% Input
%   i (1xNi) line index
%   j (1xNj) linex indexes
%   F (NixNj) values
% Output
%   idx_i (Ni*Nj)   line indexes 
%   idx_j (Ni*Nj)   column indexes
%   val   (Ni*Nj)   values: A(idx_i(k),idx_j(k)) = val(k).

    idx_i = i; idx_j = j;
    idx_j = reshape(repmat(idx_j,length(i),1),[1,length(i)*length(j)]);
    idx_i = repmat(idx_i,1,length(j));
    val = reshape(F,[numel(F),1]);
end


    
