function [V1D] = VandermondeLegendre1D(N,r)
% Vandermonde1D Compute the Vandermonde matrix associated to the
% orthonormal Legendre basis. V(i,j)=Phi_j(r_i), where the basis Phi
% consists of the Legendre polynomials (orthonormal family).
% Inputs:
%   N (1x1) Max. degree of the polynomial basis.
%   r (Px1) Points within [-1,1]
% Outputs:
%   V1D (Px(N+1)) Vandermonde matrix
% Source: Nodal Discontinuous Methods (Jan S. Hesthaven and Tim Warburton). 

V1D = zeros(length(r),N+1);
for j=1:N+1
    V1D(:,j) = JacobiP(r(:), 0, 0, j-1);
end;
return
