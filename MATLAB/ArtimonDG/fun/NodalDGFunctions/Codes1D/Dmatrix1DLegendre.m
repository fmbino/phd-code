function [DVr] = Dmatrix1DLegendre(N,r)
%GradVandermonde1D(N,r) Initialize the gradient of the Legendre polynomial
%basis Psi (modal basis) at (r) at order N.
% Inputs:
%   N (1x1) Max. order of the polynomial basis
%   r (Px1) or (1xP) points
% Outputs:
%   Dvr (PxN+1) (dPsi_j/dr(ri))i,j 
DVr = zeros(length(r),(N+1));

% Initialize matrix
for i=0:N
   [DVr(:,i+1)] = GradJacobiP(r(:),0,0,i);
end
return
