function [Dr] = Dmatrix1DLagrange(N,xi_i,r)
% Dmatrix1DLagrange Derivative matrix associated to the Lagrange polynomial
% basis associated to (xi).
% Inputs:
%   N (1x1) Max. order of the polynomial basis. (<N+1> polynomials.
%   xi_i (N+1x1) or (1xN+1) Points which define the Lagrange basis.
%   r (Px1) (1xP) Points ri at which to compute the derivatives. 
% Ouputs:
%   Dr (PxN+1) (dlj/dr(ri)) Derivative of the Lagrange polynomials at the
%points ri.

    Dvr = Dmatrix1DLegendre(N, r);
    V = VandermondeLegendre1D(N,xi_i);
    Dr = Dvr/V;
return
