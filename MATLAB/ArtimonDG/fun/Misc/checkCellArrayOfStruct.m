function checkCellArrayOfStruct(St,field,type,name)
%checkCellArrayOfStruct Check that St is a cell array of structure, each 
%with the given fields and types.
% Example: 
%   St={struct('A',value),struct('A',value)};
%   field = {'A'}; type = {'numeric'}; name='test';
% Input:
%   St (cell array of structure)
%   field (cell array of char)
%   type (cell array of char)
%   name (char) for display purposes

    if isempty(St)
        fprintf('[%s] %s: none given.\n',mfilename,name);
        return
    end
    validateattributes(St,{'cell'},{},mfilename,name);
    for i=1:length(St)
        validateattributes(St{i},{'struct'},{},mfilename,sprintf('%s{%d}',name,i));
        if sum(isfield(St{i},field))~=length(field)
            error('[%s] %s{%d} structure invalid. Expected fields: %s, %s, %s.',mfilename,name,i,field{:});
        else
            for j=1:length(field)
                validateattributes(St{i}.(field{j}),type(j),{},mfilename,sprintf('%s{%d}.%s',name,i,field{j}));
            end
        end
    end
    fprintf('[%s] %s: %d given.\n',mfilename,name,length(St));
end