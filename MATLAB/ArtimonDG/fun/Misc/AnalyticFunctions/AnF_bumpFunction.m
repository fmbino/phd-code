function f = AnF_bumpFunction(t,t0,dt)
%AnF_bumpFunction Bump function (C^\infty compact support), with support
%in the closed interval [t0,t0+dt]
% Inputs:
%   t (?)  
%   t0 (1) 
%   dt (1)
% Outputs:
%   f (1)
    % Standard bump function ('abs' to prevent NaN)
Psi = @(t)(exp(1-1./abs(1-t.^2)).*(abs(t)<=1));
f = Psi((2/dt)*t-1-2*t0/dt);
end

