function f = AnF_gaussianPulse(x,xc,xs,A)
%AnF_gaussianPulse Analytic expression of a gaussian pulse of size xs,
%centered on xc, with amplitude A.
% Inputs:
%   x (?x2) coordinates
%   xc (2)  pulse center
%   xs (2)  size
%   A (1)   amplitude
% Outputs:
%   f (?)   pulse f(x)

f = A*exp(-((x(:,1)-xc(1))/xs(1)).^2-((x(:,2)-xc(2))/xs(2)).^2);

end

