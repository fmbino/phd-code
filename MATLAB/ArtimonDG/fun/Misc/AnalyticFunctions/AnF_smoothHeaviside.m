function f = AnF_smoothHeaviside(t,t0,dt)
%AnF_smoothHeaviside Smooth Heaviside function, based on the standard "bump
%function". Support in [t0,\infty[. 0 at t0, 1 at t0+dt.
% Inputs:
%   t (?)  
%   t0 (1)
%   dt 
% Outputs:
%   f (1)
    % Standard bump function 
Psi = @(t)(exp(1-1./(1-min(t,0).^2)).*(t>=(-1)));
f = Psi(t/dt-1-t0/dt);
end
