function f = AnF_sinusDirichlet(x,bound)
%AnF_sinusDirichlet Product of Sinus that obey null Dirichlet boundary
%conditions.
%   sin(kx*x)*sin(ky*y), k = 2*pi*n/lambda, lambda=bound.
% Inputs:
%   x (?x2) coordinates
%   bound (2x2) domain boundary [x1,y1;x2,y2]
%   n (2)   mode index [nx,ny]
% Output:
%   f (?x1)  f(x)

f = sin(pi/(bound(2,1)-bound(1,1))*(x(:,1)-bound(1,1)).*(x(:,1)-bound(2,1)));
f = f.*sin(pi/(bound(2,2)-bound(1,2))*(x(:,2)-bound(1,2)).*(x(:,2)-bound(2,2)));
end

