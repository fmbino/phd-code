function R = blkinv( A, m )
%blkinv Compute inverse of block diagonal matrix A. The block diagonal is
%made of square blocks of size mxm.
% Inputs:
%   A (m*nxm*n) matrix
%   m (1) block size
% Outputs:
%   R (m*nxm*n) inverse of A (sparse if A is sparse)

    C = cell(size(A,1)/m,1);
    k = 1;
    for i = 1:m:size(A,1)
        C{k} = inv(full(A(i:i+m-1,i:i+m-1)));
        k = k + 1;
    end
    R = blkdiag(sparse(0,0),C{:});

    %Q = reshape(nonzeros(A), m, []);
%     Q = extractBlockDiagonal(A,m,size(A,2)/m);
%     B = mat2cell(full(Q), m, ones(size(A,1)/m,1)*m)';
%     D = cellfun(@(X) sparse(inv(X)), B, 'UniformOutput', false);
%     R = blkdiag(D{:});
    
end