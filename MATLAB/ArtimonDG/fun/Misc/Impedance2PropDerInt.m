function ImpedanceCoeff = Impedance2PropDerInt(resistance,reactance,freq)
%Impedance2PropDerInt Compute the impedance coefficient a,b,c such that
%   z(2*pi*1i*freq)=resistance + 1i*reactance,
% where the proportional-integral-derivative impedance is given by
%   z(s) = a/s + b + c*s (Re(s)>0, a,b,c>=0).
% Inputs:
%   resistance (1) resistance value at freq
%   reactance (1) reactance value at freq
%   freq (1) frequency (Hz)
% Output:
%   ImpedanceCoeff (3) [a,b,c]

    
    validateattributes(resistance,{'numeric'},{'scalar','nonnegative'},mfilename,'resistance');
    validateattributes(reactance,{'numeric'},{'scalar'},mfilename,'reactance');
    validateattributes(freq,{'numeric'},{'scalar','nonnegative'},mfilename,'freq');

    
    if reactance==0
       ImpedanceCoeff = [0,resistance,0];
    elseif reactance>0
        ImpedanceCoeff = [0,resistance,reactance/(2*pi*freq)];
    else 
        ImpedanceCoeff = [abs(reactance)*(2*pi*freq),resistance,0];
    end
end

