function [idx_i, idx_j,val] = make_sparse_index(i,j,F)
%make_sparse_index A(i,j) designates length(i)*length(j) elements of A,
%given by idx_i and idx_j.
% Returns vectors suited for building sparse matrix with 'sparse'. 
% Input
%   i (Ni) line index
%   j (Nj) linex indexes
%   F (NixNj) values
% Output
%   idx_i (Ni*Nj)   line indexes 
%   idx_j (Ni*Nj)   column indexes
%   val   (Ni*Nj)   values: A(idx_i(k),idx_j(k)) = val(k).

    idx_i = i(:)'; idx_j = j(:)';
    idx_j = reshape(repmat(idx_j,length(i),1),[1,length(i)*length(j)]);
    idx_i = repmat(idx_i,1,length(j));
    idx_j = idx_j(:); idx_i = idx_i(:);
    val = reshape(F,[numel(F),1]);
end