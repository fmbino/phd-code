function C = KhatriRaoProduct(A,B)
%KhatriRaoProduct Compute (one form of) the Khatri Rao product of A and B.
% Inputs:
%   A (PxN) Matrix, viewed columnwise:
%               [A1, ..., An]
%   B (Rx(N*S)) N blocks of size R*S
% Outputs:
%   C (P*RxN*S)
%           [ A1(x)B(1),...,An(x)B(N)]
%   where '(x)' is the Kronecker tensor product, and B(i) is the i-th block
%   of matrix B.

        % validate attributes (comment them for speed)
    %validateattributes(A,{'double'},{'2d','nonempty'});
    %validateattributes(B,{'double'},{'2d','nonempty'});
    [P,N] = size(A);
    R = size(B,1); S = size(B,2)/N;
%     if rem(S,1)>0
%         error('B must consist of %d blocks.',N);
%     end

        % fast method (inspired from kron)
    B = reshape(B,[R 1 S N]);
    A = reshape(A,[1 P 1 N]);
    C = reshape(bsxfun(@times,A,B),[P*R S*N]);
end