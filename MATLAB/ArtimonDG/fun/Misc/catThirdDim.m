function Ar = catThirdDim(A)
%catThirdDim Create a 2D array from a  3D array, using a vertical
%concatenation of the P matrices A(:,:,k).
% Input:
%   A (NxMxP)   3D array
% Output:
%   Ar (N*PxM)  2D array 

Ar = reshape(permute(A,[1 3 2]),[],size(A,2),1);
end

