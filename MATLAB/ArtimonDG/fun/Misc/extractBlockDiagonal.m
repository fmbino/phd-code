function D = extractBlockDiagonal(A,n,j)
%extractBlockDiagonal Extract the block diagonal of matrix A. The block
%diagonal is made of square blocks, of size nxn.
% Inputs:
%   A (PxQ) matrix
%   n (1) block size
%   j (1) no. of block to extract
% Outputs:
%   D (nx(n*j))   block diagonal (sparse if A is sparse)

    h=repmat({true(n)},j,1);
    D = reshape(A(logical(blkdiag(h{:}))),n,[]);

end

