function k = meshTriangle_findTriangle(face,triangles)
%meshTriangle_findTriangle Find which triangles posess the given face.
% Input:
% face (2) [Node_1, Node_2] Node numbers
% triangles (?x3) [Node_1, Node_2, Node_3] List of triangles
% Outputs:
% k (?) Numbers of triangles which possess <face> (0 if none)

    validateattributes(face,{'numeric'},{'vector','numel',2});
    validateattributes(triangles,{'numeric'},{'2d','ncols',3});
    
    B1 = triangles == face(1);
    B2 = triangles == face(2);
    k = find(sum(B1+B2,2)==2);
    if length(k)==0
        k=0;
    end
end