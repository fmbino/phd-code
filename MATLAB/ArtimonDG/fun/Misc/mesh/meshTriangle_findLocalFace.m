function l = meshTriangle_findLocalFace(nodes,triangle)
%meshTriangle_findLocalFace Find the number l (1,2,3) of the triangle face
%which posses nodes.
%(This is a very simple function, but worth writing.)
% Inputs:
%  nodes (2)     node numbers (2-node face)
%  triangle (3)  node numbers (3-node triangle)
% Outputs:
%  l (1)    local face number (1,2,3) or 0 if no match
%         ! Triangle face ordering (3-node triangle)
%         !        v
%         !        ^
%         !        |
%         !        3
%         !        |`\
%         !        |  `\ Face 2
%         !Face 3  |    `\
%         !        |      `\
%         !        |        `\
%         !        1----------2 --> u
%         !           Face 1

    validateattributes(nodes,{'numeric'},{'vector','numel',2});
    validateattributes(triangle,{'numeric'},{'vector','numel',3});
vn = [[1,2];[2,3];[1,3]];
l = 0;
    for i=1:3
        if sum(nodes==triangle(vn(i,:)))==2 || sum(nodes==flip(triangle(vn(i,:))))==2
           l=i;
        end
    end
end

