function idxE = expandIndex(idx,N)
%expandIndex Expand index vector.

idx = idx(:); l = length(idx);
al = 1:N;
idxE = [];
for i=1:l
    idxE = [idxE,(idx(i)-1)*N + al];    
end
idxE=idxE(:);
end

