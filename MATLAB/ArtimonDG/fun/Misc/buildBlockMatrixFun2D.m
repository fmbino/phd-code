function C = buildBlockMatrixFun2D(A,x,f)
%buildBlockMatrixFun2D build block matrix C, from matrix A, coordinates x
%and function f.
% Inputs:
%   A (PxQ) matrix
%   f (function_handle) f(x,y) is a RxS matrix
%   x (Qx2) [x,y] coordinates
% Outputs:
%   C (P*RxQ*S) The (i,j) block of B is given by
%                       [A(i,j) * f(x(j,:))]

        % validate attributes (comment for speed)
    %validateattributes(A,{'double'},{'2d','nonempty'});
    %validateattributes(x,{'double'},{'2d','nonempty','nrows',size(A,2)});
    %validateattributes(f,{'function_handle'},{});)
    %validateattributes(f(x(1,1),x(1,2)),{'double'},{'2d','nonempty'});
    [~,Q] = size(A);
    [R,S] = size(f(x(1,1),x(1,2)));
    
    %B = arrayfun(f,x(:,1),x(:,2),'UniformOutput', false);
        % faster than arrayfun
    B = zeros(R,Q*S);
    for j=1:Q
        B(:,(j-1)*S+(1:S)) = f(x(j,1),x(j,2));
    end
    C = KhatriRaoProduct(A,B);
end