function C = sumBlock(A,N)
%sumBlock Compute the line sum of block matrix A.
% Inputs:
%   A (Px(N*Q)) Block matrix
%   N (1) block size
% Output:
%   C (PxQ) Line sum

    Q = size(A,2)/N;
    C = zeros(size(A,1),Q);
    for j=1:size(C,2)
        C(:,j) = sum(A(:,j:Q:end),2);
    end
end