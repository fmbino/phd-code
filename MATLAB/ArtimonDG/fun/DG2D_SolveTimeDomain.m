function [x,t] = DG2D_SolveTimeDomain(M,K,F,DGMesh,Nq,tf,dt,varargin)
%DG2D_SolveTimeDomain Solve the global formulation:
%               M*dx/dt + K*x = F*xs + FQ*Q(CQ*x) + FQ*Q(CQ*x)(t-tau)
%                                    + FQ*(CQ*x)(t-tau)
% in the time domain. The three additional terms designate: boundary
% operator, delayed boundary operator, and direct boundary delay.
% Return the full field (without additional variables) or only part of it.
% Inputs:
%   M,K,F           Global formulation
%   DGMesh (DGMesh_2D_Triangle)
%   Nq (1)          No. of variables in the PDE
%   tf (1)          Final time
%   dt (1)          Time-step
% Optional inputs (time integration):
%   x0          initial condition
%   xs    (function handle) source function
%   nidx        node indexes to store
%   storeOnlyLast (boolean) store only the last iteration (default: 0)
%   Cl          eigenvalue scaling parameter (to estimate CFL max)
% Optional inputs (boundary operator)
%   DirectDelay (cell array of structure) Direct delay. Fields:
%       - F (matrix)
%       - C (matrix)
%       - Delay (scalar)
%   LTIOp (cell array of structure) LTI operator (no delay). Fields:
%       - F (matrix)
%       - C (matrix)
%       - Operator (LTIOperator)
%   LTIOpDelayed (cell array of structure) LTI operator (with delay). Fields:
%       - F (matrix)
%       - C (matrix)
%       - Operator (LTIOperator)
%       - Delay (scalar)
% Optional inputs (hyperbolic realisation with DG).
%   Nk (1) number of element in (0,1) 
%   Np (1) number of nodes per element (element order: <Np-1>)
% Output:
%  [x,t] field and corresponding time steps

        % -- Validate attributes
            % Mandatory arguments
    validateattributes(M,{'numeric'},{'square'},mfilename,'M');
    N = size(M,1);
    validateattributes(K,{'numeric'},{'square','size',size(M)},mfilename,'K');
    if ~isempty(F)
        validateattributes(F,{'numeric'},{'2d','nrows',size(M,1)},mfilename,'F');
    end
    validateattributes(DGMesh,{'DGMesh_2D_Triangle'},{},mfilename,'DGMesh');
    validateattributes(Nq,{'numeric'},{'scalar','positive','integer'},mfilename,'Nq');
    validateattributes(tf,{'numeric'},{'scalar'},mfilename,'tf');
    validateattributes(dt,{'numeric'},{'scalar'},mfilename,'dt');
            % Optional arguments
    p = inputParser; p.FunctionName = mfilename;
    addParameter(p,'x0',zeros(N,1),@(x)(validateattributes(x,{'numeric'},{'vector','numel',N})));
    addParameter(p,'xs',@(t)(zeros(size(F,2),1)));
    addParameter(p,'nidx',1:size(M,1),@(x)(validateattributes(x,{'numeric'},{'vector','integer'})));
    addParameter(p,'storeOnlyLast',0,@(x)(validateattributes(x,{'numeric'},{'scalar'})));
    addParameter(p,'Cl',1,@(x)(validateattributes(x,{'numeric'},{'scalar','positive'})));
    addParameter(p,'Nk',1,@(x)(validateattributes(x,{'numeric'},{'scalar','integer','>=',1})));
    addParameter(p,'Np',2,@(x)(validateattributes(x,{'numeric'},{'scalar','integer','>=',1})));
    addParameter(p,'DirectDelay',[]);
    addParameter(p,'LTIOp',[]);
    addParameter(p,'LTIOpDelayed',[]);
    parse(p,varargin{:}); oA = p.Results; % structure which contains optional arguments
    x0 = oA.x0(:); xs = oA.xs; nidx = oA.nidx; Cl = oA.Cl; storeOnlyLast = oA.storeOnlyLast;
    Np = oA.Np; Nk=oA.Nk;
                % check source
    if ~isempty(F)
       checkSourceTerm(xs,size(F,2)); 
    end
                % check structures for boundary operators
    checkCellArrayOfStruct(oA.DirectDelay,{'F','C','Delay'},{'numeric','numeric','numeric'},'DirectDelay');
    checkCellArrayOfStruct(oA.LTIOp,{'F','C','Operator'},{'numeric','numeric','LTIOperator'},'LTIOp');
    checkCellArrayOfStruct(oA.LTIOpDelayed,{'F','C','Operator','Delay'},{'numeric','numeric','LTIOperator','numeric'},'LTIOpDelay');
    DirectDelay = oA.DirectDelay; LTIOp = oA.LTIOp; LTIOpDelayed=oA.LTIOpDelayed;
        % -- Build parabolic-hyperbolic realisation
    [A,F] = DG2D_buildParabolicHyperbolicRealisation(M,K,DGMesh,Nq,'F',F,'DirectDelay',DirectDelay,'LTIOp',LTIOp,'LTIOpDelayed',LTIOpDelayed,'Np',Np,'Nk',Nk);
    Nadd = size(A,1)-N; % No. of additional variables
        % -- Time integration
            % get RK coefficients
    [A2N,B2N,Crk] = RK_get84Coeffs_2NStorage();
    CFLmax = (Crk/Cl)*DGMesh.DGCell.dr;
    fprintf('[%s] Estimation of CFLmax = %1.2g.\n',mfilename,CFLmax);
    x0 = [x0;sparse(Nadd,1)]; % expand initial condition
    if isempty(F)
        [x,t] = LSERK_op(@(x,t)(A*x),x0,tf,dt,A2N,B2N,'q_idx',nidx,'storeOnlyLast',storeOnlyLast);
    else
        [x,t] = LSERK_op(@(x,t)(A*x+F*xs(t)),x0,tf,dt,A2N,B2N,'q_idx',nidx,'storeOnlyLast',storeOnlyLast);
    end
end

function checkSourceTerm(xs,N)
%checkSourceTerm Check the validity of xs as a source term for the global
%formulation.
% Inputs:
%   xs (function_handle)
%   N (1)   No. of columns of source flux matrix F

    validateattributes(xs,{'function_handle'},{},mfilename,'xs');
    try
        r = xs(1);
    catch
        error('[%s] xs cannot be evaluated at t=1.',mfilename);
    end
    validateattributes(r,{'numeric'},{'column','numel',N},mfilename,'xs');
end