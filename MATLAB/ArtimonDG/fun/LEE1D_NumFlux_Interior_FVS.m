function [f_out,f_inw] = LEE1D_NumFlux_Interior_FVS(U0,c0)
%NumFlux_LEE1D_Interior_FVS Compute the numerical flux
% matrices, using the Flux Vector Splitting (FVS).
% The FVS flux splitting reads: f(n) = f_+(n) + f_-(n), where
% f_+ is the outward flux, and f_- the inward flux.
% The FVS numerical flux is:
% f*(q1,q1,nx) = f_+(n)q1 + f_-(n)q2.
% Conservativity property:
%   f_+(1) = -f_-(-1)
%   f_-(1) = -f_+(-1)
% Inputs:
%   U0 (1) basic flow speed (m/s)
%   c0 (1) speed of sound (m/s)
% Outputs:
%   f_out (NqxNq) outward flux f_+(1)
%   f_inw (NqxNq) inward flux f_-(1) 


    validateattributes(U0,{'numeric'},{'scalar'});
    validateattributes(c0,{'numeric'},{'scalar'});
    
    f_out = 1*(U0+c0)/2*[1,1;1,1];
    f_inw = 1*(U0-c0)/2*[1,-1;-1,1];
end