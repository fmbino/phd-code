function x_harm_ex = DG2D_LEE_AnalyticalSolution_ImpTube(x,omega,Ai,c0,beta,L)
%DG2D_LEE_AnalyticalSolution_ImpTube Analytical solution of the
%monodimensional impedance tube: x in [0,L].
%This is essentially a wrapper on 'LEE_AnalyticalSolution_ImpTube_Freq': it
%reshapes the data in Artimon format.
% Inputs:
%   x (Px1) longitudinal coordinate
%   omega (1) angular frequency (rad/s)
%   Ai (1) amplitude
%   c0 (1) speed of sound
%   beta (1) value of reflection coefficient.
%   L (1) tube length
% Outputs:
%   x_harm_ex [u,v,p/z0]
     
    [u,p] = LEE_AnalyticalSolution_ImpTube_Freq(x,omega,Ai,c0,beta,L);
    x_harm_ex = [transpose(u(:));transpose(p(:));zeros(1,length(x))]; % [u;p/z0;v]
    x_harm_ex = reshape(x_harm_ex([1,3,2],:),[],1); % [u,v,p/z0]
end