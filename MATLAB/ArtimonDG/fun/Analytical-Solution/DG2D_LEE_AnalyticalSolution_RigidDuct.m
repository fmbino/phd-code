function x_harm_ex = DG2D_LEE_AnalyticalSolution_RigidDuct(xdg,omega,Ai,c0,U0)
%DG2D_LEE_AnalyticalSolution_ImpTube Analytical solution of the
%monodimensional impedance tube: x in [0,L].
%Based on 'LEE_1D_ExactHarmonicSolution'.
% Inputs:
%   xdg (?x1) longitudinal coordinates
% Outputs:
%   x_harm_ex [u,v,p]

        % dispersion relation
k = roots([c0^2-U0^2,2*omega*U0,-omega^2]);
k = k(real(k)>=0);
	% Compute exact solution
x_harm_ex = Ai*[cos(k*xdg),zeros(size(xdg,1),1),cos(k*xdg)]; % [u,v,p]
x_harm_ex = reshape(x_harm_ex',[],1);

end

