function [u,p,t] = LEE_AnalyticalSolution_ImpTube_NonLinear_Temp(f,beta,L,c0,tf,dt,x)
%LEE_AnalyticalSolution_ImpTube_NonLinear_Temp Analytical solution of the impedance
%tube in the time domain, with nonlinear scattering operator beta.
% du/dt + c0*dp/dx =0 and dp/dt + c0*du/dx = 0 with x in (0,L) t in (0,tf),
% and boundary condition
%   (u,p)(t,x=0)=(us,ps)(t) (Source)
%   p(t,x=L) = z(t)*u(t,x=L) (Impedance boundary condition).
% The Impedance model is given as a nonlinear scattering operator.
% Input:
%   f (function_handle) source f(t)=(us(t)+ps(t))/2 (null for t<0)
%   beta (function_handle) nonlinear scattering operator (vectorized)
%   L (1)  tube length
%   c0 (1) sound speed
%   tf (1) final time
%   dt (1) time step
%   x (1)  output abscissa, in [0,L]
% Output:
%   u (P) acoustic speed u(x,t)
%   p (P) acoustic pressure p(x,t)
%   t (P) time instants

    % -- Validate argument
    validateattributes(f,{'function_handle'},{},mfilename,'f');
    tmp = linspace(1,2,4);
    validateattributes(f(tmp),{'numeric'},{'size',size(tmp)},mfilename,'f(tmp)');
    if ~isempty(find(f(-tmp),1))
        warning('[%s]: source function f(t) is not causal.\n',mfilename);
    end
    clear tmp
    validateattributes(beta,{'function_handle'},{},mfilename,'beta');
        % check wether beta is vectorized
    tmp = linspace(1,2,4);
    try
        v = beta(tmp);
    catch
        error('[%s] beta is not a single-input single-output vectorized operator',mfilename);
    end
    validateattributes(v,{'numeric'},{'size',size(tmp)},mfilename,'beta(v)');
    validateattributes(L,{'numeric'},{'scalar'},mfilename,'L');
    validateattributes(c0,{'numeric'},{'scalar'},mfilename,'c0');
    validateattributes(tf,{'numeric'},{'scalar'},mfilename,'tf');
    validateattributes(dt,{'numeric'},{'scalar'},mfilename,'dt');
    validateattributes(x,{'numeric'},{'scalar'},mfilename,'x');    
    
    t = 0:dt:tf;
    
    %  -- Compute u and p
    % u = f(t-x/c0) -beta(f(t-tau))
    % p = f(t-x/c0) +beta(f(t-tau))
    tau = (2*L-x)/c0; % time delay
    be = beta(f(t-tau)*2)/2; % Beware not to simplify since beta is nonlinear
    u = f(t-x/c0) - be;
    p = f(t-x/c0) + be;
end
