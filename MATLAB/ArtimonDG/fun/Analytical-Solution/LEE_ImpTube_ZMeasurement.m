function Z = LEE_ImpTube_ZMeasurement(t,p,L,c0,w_src)
%LEE_ImpTube_ZMeasurement Compute impedance from time-domain impedance tube
%measurements. Formula (38) from 
% Hubbard 1991 - Aeroacoustics of Flight Vehicles Vol.2
% Chap. 14 - Design and Performance of Duct Acoustic Treatment
%                   A + B exp(i*dphi)
%           Z/z0 =  -----------------,
%                   A - B exp(i*dphi)
% with
% A: source amplitude
% B: reflected amplitude
% dphi: phase difference between source and amplitude.
%   x=0                                          x=L
%   |---------------------------------------------|// Z
%  (Source)                                     (Impedance)
% Inputs:
%   t (N) time instants (s)
%   p (N) pressure measurement at x=0 (Pa)
%   L (1) impedance tube length (m)
%   c0 (1) speed of sound (m/s)
%   w_src (1) angular frequency of (monochromatic) source
% Output:
%   Z (1) impedance

    validateattributes(t,{'numeric'},{'vector'},mfilename,'t');
    validateattributes(p,{'numeric'},{'vector','numel',length(t)},mfilename,'p');
    validateattributes(L,{'numeric'},{'scalar'},mfilename,'L');
    validateattributes(c0,{'numeric'},{'scalar'},mfilename,'c0');
    validateattributes(w_src,{'numeric'},{'scalar'},mfilename,'w_src');
    
    
        % Diagnostic message
    T_src = 2*pi/w_src;
    tf = max(t) - min(t);
    Nper = tf/T_src; % Total no. of period
    Nper_refl = (tf - 2*L/c0)/T_src; % No. of measured reflected period
    fprintf('[%s] Total no. of period: %1.3g\n',mfilename,Nper);
    fprintf('[%s] No. of measured reflected period: %1.3g\n',mfilename,Nper_refl);
    if Nper<1 || Nper_refl<1
        Z = NaN; % no point in trying to identify a value
    end
    
    f = @(t,A,phi)A*sin(w_src*t+phi);
        % (a) Identitfy source amplitude and phase
    idx_p = logical(t<=T_src);
    t_mes = t(idx_p);
    fun = @(coeff,fdata)(f(t_mes,coeff(1),coeff(2)));
    options = optimoptions(@lsqcurvefit,'Display','off');
    [Coeff_optim,~,~,exitflag] = lsqcurvefit(fun,[1;0],t_mes,p(idx_p),[],[],options);
    A_src = Coeff_optim(1);
    phi_src = Coeff_optim(2);
    fprintf('[%s] Source: A=%1.1e, phi=%1.1e (exitflag:%d)\n',mfilename,A_src,phi_src,exitflag);

            % (b) Identify reflection amplitude and phase
        % Curve fit of B*sin(w*t+phi) on reflected wave
%     idx_p = logical(t>=(max(t)-T_src));
%     t_mes = t(idx_p);
%     fun = @(coeff,fdata)(f(t_mes,coeff(1),coeff(2)));
%     [Coeff_optim,~,~,exitflag] = lsqcurvefit(fun,[A_src;phi_src],t_mes,p(idx_p)-f(t_mes,A_src,phi_src),[],[],options);
%     A_ref = Coeff_optim(1);
%     phi_ref = Coeff_optim(2);
%     fprintf('[%s] Refl: A=%1.1e, phi=%1.1e (exitflag:%d)\n',mfilename,A_ref,phi_ref,exitflag);
    
        % (PREVIOUS METHOD)
        % (b) Identify reflection amplitude and phase
        % Curve fit of B*sin(w*t+phi) on reflected wave
    idx_p = logical((t>=(2*L/c0)).*(t<=(T_src+2*L/c0)));
    t_mes = t(idx_p)-2*L/c0;
    fun = @(coeff,fdata)(f(t_mes,coeff(1),coeff(2)));
    [Coeff_optim,~,~,exitflag] = lsqcurvefit(fun,[A_src;phi_src],t_mes,p(idx_p),[],[],options);
    A_ref = Coeff_optim(1);
    phi_ref = Coeff_optim(2);
    fprintf('[%s] Refl: A=%1.1e, phi=%1.1e (exitflag:%d)\n',mfilename,A_ref,phi_ref,exitflag);

        % Compute impedance Z using Eq. (38)
    dphi = phi_ref - 0*phi_src;
    Z = (A_src + A_ref*exp(1i*dphi))/(A_src-A_ref*exp(1i*dphi));
end

