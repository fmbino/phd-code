function [u,p] = LEE_AnalyticalSolution_ImpTube_Freq(x,omega,Ai,c0,beta,L)
%LEE_AnalyticalSolution_ImpTube_Freq Analytical solution of the impedance
%tube in the frequency domain.
% du/dt + c0*dp/dx =0 and dp/dt + c0*du/dx = 0 with x in (0,L) t in (0,tf),
% and boundary condition
%   (u,p)(t,x=0)=(us,ps)(t) (Source)
%   p(t,x=L) = z(t)*u(t,x=L) (Impedance boundary condition).
% The Impedance model is given by the Laplace transform of its reflection 
% coefficient:
%               beta(s) := (z(s)-1)/(z(s)+1) (Re(s)>0).
% Inputs:
%   x (Px1) longitudinal coordinates
%   omega (1) angular frequency (rad/s)
%   Ai (1) amplitude
%   c0 (1) speed of sound
%   beta (1) value of reflection coefficient.
%   L (1) tube length
% Outputs:
%   u  (Px1) acoustic speed
%   p  (Px1) acoustic pressure

    % -- Validate arguments
    validateattributes(x,{'numeric'},{'vector'},mfilename,'x');
    validateattributes(omega,{'numeric'},{'scalar'},mfilename,'omega');
    validateattributes(Ai,{'numeric'},{'scalar'},mfilename,'Ai');
    validateattributes(c0,{'numeric'},{'scalar'},mfilename,'c0');
    validateattributes(beta,{'numeric'},{'scalar'},mfilename,'beta');
    validateattributes(L,{'numeric'},{'scalar'},mfilename,'L');    
    
    % -- Compute field
    s = 1i*omega;
    u = Ai*(exp(-s*x(:)/c0)-beta*exp(-s*(2*L-x(:))/c0));
    p = Ai*(exp(-s*x(:)/c0)+beta*exp(-s*(2*L-x(:))/c0));
end

