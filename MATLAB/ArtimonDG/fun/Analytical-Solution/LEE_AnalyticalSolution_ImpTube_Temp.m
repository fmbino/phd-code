function [u,p,t] = LEE_AnalyticalSolution_ImpTube_Temp(f,beta0,mu1,xi1,delay,mu2,xi2,L,c0,tf,dt,x)
%LEE_AnalyticalSolution_ImpTube_Temp Analytical solution of the impedance
%tube in the time domain.
% du/dt + c0*dp/dx =0 and dp/dt + c0*du/dx = 0 with x in (0,L) t in (0,tf),
% and boundary condition
%   (u,p)(t,x=0)=(us,ps)(t) (Source)
%   p(t,x=L) = z(t)*u(t,x=L) (Impedance boundary condition).
% The Impedance model is given by the Laplace transform of its reflection 
% coefficient:
%               beta(s) := (z(s)-1)/(z(s)+1) (Re(s)>0).
% The reflection coefficient is expressed as
%               beta(s) = beta0 + h1(s) + e^(-s*delay0)*h2(s),
% where h(s) is given by
%               h1(s) = Σ_k mu1_k / (s+xi1_k)  (Re(s)>0),
% and similarly for h2.
% Input:
%   f (function_handle) source f(t)=(us(t)+ps(t))/2 (null for t<0)
%   beta0 (1) real coefficient
%   mu1,xi1 (Nxi) representation of h1(s) (possibly empty)
%   delay (1) time delay (>=0)
%   mu2,xi2 (Nxi2) representation of h2(s) (possibly empty)
%   L (1)  tube length
%   c0 (1) sound speed
%   tf (1) final time
%   dt (1) time step
%   x (1)  output abscissa, in [0,L]
% Output:
%   u (P) acoustic speed u(x,t)
%   p (P) acoustic pressure p(x,t)
%   t (P) time instants

    % -- Validate argument
    validateattributes(f,{'function_handle'},{},mfilename,'f');
    tmp = linspace(1,2,4);
    validateattributes(f(tmp),{'numeric'},{'size',size(tmp)},mfilename,'f(tmp)');
    if ~isempty(find(f(-tmp),1))
        warning('[%s]: source function f(t) is not causal.\n',mfilename);
    end
    clear tmp
    validateattributes(beta0,{'numeric'},{'scalar'},mfilename,'beta0');
    validateattributes(mu1,{'numeric'},{},mfilename,'mu');
    validateattributes(xi1,{'numeric'},{'numel',length(mu1)},mfilename,'xi');
    validateattributes(delay,{'numeric'},{'scalar'},mfilename,'delay');
    validateattributes(mu2,{'numeric'},{},mfilename,'mu2');
    validateattributes(xi2,{'numeric'},{'numel',length(mu2)},mfilename,'xi2');
    validateattributes(L,{'numeric'},{'scalar'},mfilename,'L');
    validateattributes(c0,{'numeric'},{'scalar'},mfilename,'c0');
    validateattributes(tf,{'numeric'},{'scalar'},mfilename,'tf');
    validateattributes(dt,{'numeric'},{'scalar'},mfilename,'dt');
    validateattributes(x,{'numeric'},{'scalar'},mfilename,'x');    
    
    % -- Time integration
    tau = (2*L-x)/c0; % time delay
            % get RK coefficients
    [A2N,B2N,Crk] = RK_get84Coeffs_2NStorage();
    fprintf('[%s] Computation of time-domain convolution h*f...\n',mfilename);
    fprintf('[%s] Estimation of dt_max: %1.3g. Given dt: %1.3g.\n',mfilename,Crk/max(abs([xi1(:);xi2(:)])),dt);
            % solve oscillatory-diffusive systems
    phi1 = 0; phi2=0; t = 0:dt:tf;
    if ~isempty(xi1)
        [phi1,t] = LSERK_op(@(phi,t)(-diag(xi1)*phi + mu1(:)*f(t-tau)),0*mu1(:),tf,dt,A2N,B2N);
    end
    if ~isempty(xi2)
        [phi2,t] = LSERK_op(@(phi,t)(-diag(xi2)*phi + mu2(:)*f(t-tau-delay)),0*mu2(:),tf,dt,A2N,B2N);
    end
    %  -- Compute u and p
    % u = f(t-x/c0) -beta*f(t-tau)
    % p = f(t-x/c0) +beta*f(t-tau)
    u = f(t-x/c0) - (beta0*f(t-tau) + sum(phi1,1) + sum(phi2,1));
    p = f(t-x/c0) + (beta0*f(t-tau) + sum(phi1,1) + sum(phi2,1));
end