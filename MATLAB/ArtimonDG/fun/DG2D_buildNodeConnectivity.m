function [vmapM, vmapP] = DG2D_buildNodeConnectivity(mesh,DGCell)
% DG2D_buildInteriorConnectivity Build connectivity maps for the DG nodes,
% at each face.
% Remark: adapted from Hesthaven's 'BuildMaps2D'.
% Inputs:
%   
% Outputs:
%   vmapM (Nfpx3xNk) Interior nodes
%   vmapP (Nfpx3xNk) Corresponding exterior nodes


    NODETOL = 1e-12;
    K = mesh.N;
    Np = DGCell.Np;
    Nfp = DGCell.Nfp;
    Nfaces = 3; % 3 faces per triangle
    Fmask = DGCell.faceIdx;
    EToE = mesh.EToE;
    EToF = mesh.EToF;
    EToV = mesh.triangles(:,2:end);
    VX = mesh.nodes(:,1)';
    VY = mesh.nodes(:,2)';
    r = DGCell.xi(:,1);
    s = DGCell.xi(:,2);
    % build coordinates of all the nodes
    va = EToV(:,1)'; vb = EToV(:,2)'; vc = EToV(:,3)';
    x = 0.5*(-(r+s)*VX(va)+(1+r)*VX(vb)+(1+s)*VX(vc));
    y = 0.5*(-(r+s)*VY(va)+(1+r)*VY(vb)+(1+s)*VY(vc));


    % number volume nodes consecutively
    nodeids = reshape(1:K*Np, Np, K);
    vmapM   = zeros(Nfp, Nfaces, K); vmapP   = zeros(Nfp, Nfaces, K); 
    mapM    = (1:K*Nfp*Nfaces)';     mapP    = reshape(mapM, Nfp, Nfaces, K);

    % find index of face nodes with respect to volume node ordering
    for k1=1:K
      for f1=1:Nfaces
        vmapM(:,f1,k1) = nodeids(Fmask(:,f1), k1);
      end
    end

    one = ones(1, Nfp);
    for k1=1:K
      for f1=1:Nfaces
        % find neighbor
        k2 = EToE(k1,f1); f2 = EToF(k1,f1);

        % reference length of edge
        v1 = EToV(k1,f1); v2 = EToV(k1, 1+mod(f1,Nfaces));
        refd = sqrt( (VX(v1)-VX(v2))^2 + (VY(v1)-VY(v2))^2);

        % find find volume node numbers of left and right nodes 
        vidM = vmapM(:,f1,k1); vidP = vmapM(:,f2,k2);    
        x1 = x(vidM); y1 = y(vidM); x2 = x(vidP); y2 = y(vidP);
        x1 = x1*one;  y1 = y1*one;  x2 = x2*one;  y2 = y2*one;

        % Compute distance matrix
        D = (x1 -x2').^2 + (y1-y2').^2;
        [idM, idP] = find(sqrt(abs(D))<NODETOL*refd);
        vmapP(idM,f1,k1) = vidP(idP); mapP(idM,f1,k1) = idP + (f2-1)*Nfp+(k2-1)*Nfaces*Nfp;
      end
    end

    % reshape vmapM and vmapP to be vectors and create boundary node list
    vmapP = vmapP(:); vmapM = vmapM(:); mapP = mapP(:);
    mapB = find(vmapP==vmapM); vmapB = vmapM(mapB);
return