function [Vl1D] = VandermondeLagrange1D(xi_i,r)
% Vandermonde1D Compute the Vandermonde matrix associated to the
% Lagrange basis. V(i,j)=l_j(r_i), where the basis l
% consists of the Lagrange polynomials associated to xi_i.
% Inputs:
%   xi_i (N+1x1) Points defining the Lagrange basis (N+1 polynomials)
%   r (Px1) Points within [-1,1]
% Outputs:
%   Vl1D (Px(N+1)) Vandermonde matrix
    N = length(xi_i)-1;
    Vpsi_r = VandermondeLegendre1D(N,r);
    Vpsi_xi = VandermondeLegendre1D(N,xi_i);
    Vl1D = Vpsi_r*inv(Vpsi_xi);
    %Vl1D = Vpsi_r\Vpsi_xi;
return
