function [triangles, nodes] = DG2D_buildPlotMesh(DGCell,mesh)
%DG2D_buildPlotTriangles build a new triangulation associated with the
%equidistant nodes on the standard triangle.
% Inputs:
%   DGCell (DGCell_2D_Triangle) DG cell, with Np nodes
%   mesh (Mesh_2D_Triangle) mesh, with Nk triangles
% Outputs:
%   triangles (?x3) [node1, node2, node3]
%   nodes (Np*Nkx2) [x,y]

    validateattributes(DGCell,{'DGCell_2D_Triangle'},{});
    validateattributes(mesh,{'Mesh_2D_Triangle'},{});

Nk = mesh.N;
Np = DGCell.Np;

    % build triangulation for all equally spaced nodes on all elements
    % (code from Hesthaven)
triangles = [];
for k=1:Nk
  triangles = [triangles; DGCell.trianglesplot+(k-1)*Np];
end

    % build vector of global coordinates
va = mesh.triangles(:,2); vb = mesh.triangles(:,3); vc = mesh.triangles(:,4);
r = DGCell.xiplot(:,1); s = DGCell.xiplot(:,2);
x = 0.5*(-(r+s)*mesh.nodes(va,1)'+(1+r)*mesh.nodes(vb,1)'+(1+s)*mesh.nodes(vc,1)');
y = 0.5*(-(r+s)*mesh.nodes(va,2)'+(1+r)*mesh.nodes(vb,2)'+(1+s)*mesh.nodes(vc,2)');
nodes = [x(:),y(:)];

end

