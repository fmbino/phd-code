function zoneCase = DG2D_addZone(zoneCase,zone_fun,idx,coeff)
%DG2D_addZone Add the zone(s) described by zone_fun to the zoneCase
%structure.
% Rmk: This is a silly function, only useful to avoid code duplication.
% Rmk: To remove a contribution, just set the coefficient cell to [].
% Inputs:
%   zoneCase (cell)    zones for current case
%   zone_fun (function_handle)  zone_fun(idx,coeff{i}) describes a zone.
%   idx (P)                 number(s) of physical entities
%   coeff (cell)            coefficients for iterative definition
% Output:
%   zoneCase (cell)     updated zoneCase

    validateattributes(zoneCase,{'cell'},{},mfilename,'zoneCase');
    validateattributes(zone_fun,{'function_handle'},{},mfilename,'zone_fun');
    validateattributes(idx,{'numeric'},{'vector','integer'},mfilename,'idx');
    if isempty(coeff)
        fprintf('[%s] Coefficient cell: empty.\n',mfilename);
       return 
    else
        fprintf('[%s] Coefficient cell: %d element(s).\n',mfilename,length(coeff));
    end
        validateattributes(coeff,{'cell'},{},mfilename,'coeff');
    try
        zone_fun(idx,coeff{1});
    catch
        error('[%s] zone_fun(idx,coeff{1}) is invalid.',mfilename);
    end
    for i=1:length(coeff)
        zoneCase{end+1} = zone_fun(idx,coeff{i});
    end
end