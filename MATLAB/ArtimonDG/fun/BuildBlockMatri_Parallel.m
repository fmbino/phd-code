function C = BuildBlockMatri_Parallel(A,Block)
%BuildBlockMatrix From the matrix A and Block, build the block matrix C.
% Parallel version.
% Inputs:
%   A (PxQ)
%   Block (RxS)
% Outputs:
%   C (PRxQS) Matrix whose block n°(i,j) is A(i,j) * Block.

C = zeros(size(A).*size(Block));
clear C
P=size(A,1); Q=size(A,2);
R=size(Block,1); S=size(Block,2);


for i=1:P
for j=1:Q
        % block n°(i,j) in the block matrix
    C((i-1)*R+(1:R),(j-1)*S+(1:S)) = A(i,j)*Block;
end
end
end