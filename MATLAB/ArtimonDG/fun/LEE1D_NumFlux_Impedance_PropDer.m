function [f0,f1] = LEE1D_NumFlux_Impedance_PropDer(ImpCoeff,Ax)
%NumFlux_LEE1D_Impedance_Universal Compute the impedance numerical flux
% matrices, using the universal numerical flux.
% Beware, it's not very pretty.
% Centred flux, with ghost state
% f*(q,nx) = Ax*nx*(q+q_ghost)/2
%          = f0(nx)*q + f1(nx)*dq/qt + fd1(nx)*D1(q) + fd2(nx)*D2(q)
% Inputs:
%   ImpCoeff (4) Coefficients of the impedance model
%       1 <-> q, 2 <-> dq/dt, 3 <-> D1(q), 4 <-> D2(q)
%   Ax (NqxNq) Jacobian of the flux matrix (Nq=2)
% Outputs:
%   f0 (1)->(NqxNq) Dependency on q
%   f0 (1)->(NqxNq) Dependency on dq/dt

    validateattributes(ImpCoeff,{'numeric'},{'numel',2,'nonnegative'});
    validateattributes(Ax,{'numeric'},{'size',[2,2]});
    
    beta = (ImpCoeff-1)./(ImpCoeff+1);
    f0 = @(nx)((nx/2)*Ax*[2,0;(1+beta(1))*(1-beta(2))*nx,1+beta(1)+beta(2)-beta(2)*beta(1)]); % (1x1)->(NqxNq)
    f1 = @(nx)((nx/2)*Ax*[0,0;(1-beta(1))*(1+beta(2))*nx,0]);
end