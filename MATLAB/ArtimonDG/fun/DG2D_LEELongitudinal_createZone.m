function zoneCase = DG2D_LEELongitudinal_createZone(id,cst,zone)
%DG2D_LEELongitudinal_createZone Create the cell 'zoneCase' that describes
%a computational case for the LEE longitudinal.
% Inputs:
%   id (struct)         Numerical identifier of each zone. Fields:
%       - 'Interior'
%       - 'Source'
%       - 'Wall'
%       - 'Impedance'
%   cst (struct)        Constants needed to build the case.
%   zone (struct)       Available zones.
% Output:
%   zoneCase (cell)

        % -- Validate attributes
    validateattributes(id,{'struct'},{},mfilename,'id');
    validateattributes(cst,{'struct'},{},mfilename,'cst');
    validateattributes(zone,{'struct'},{},mfilename,'zone');
        % -- 
    zoneCase=cell(0);
    name = fieldnames(id);
    for i=1:length(name)
        switch name{i}
            case 'Interior'
                % cst
                %   'u0', 'v0', 'c0'
                fprintf('[%s] Physical entity %s: %s.\n',mfilename,mat2str(id.(name{i})),name{i});
                zoneCase{end+1} = zone.Interior_UpwindFVS(id.Interior,cst.u0,cst.v0,cst.c0);
            case 'Source'
                % cst
                %   'u0', 'v0', 'c0'
                fprintf('[%s] Physical entity %s: %s.\n',mfilename,mat2str(id.(name{i})),name{i});
                zoneCase{end+1} = zone.Outlet_UpwindFVS(id.Source,cst.u0,cst.v0,cst.c0);
                zoneCase{end+1} = zone.Inlet_UpwindFVS(id.Source,cst.u0,cst.v0,cst.c0);
            case 'Outlet'
                % cst
                %   'u0', 'v0', 'c0'
                fprintf('[%s] Physical entity %s: %s.\n',mfilename,mat2str(id.(name{i})),name{i});
                zoneCase{end+1} = zone.Outlet_UpwindFVS(id.Outlet,cst.u0,cst.v0,cst.c0);
            case 'Wall'
                % cst
                %   'u0', 'v0', 'c0'
                fprintf('[%s] Physical entity %s: %s.\n',mfilename,mat2str(id.(name{i})),name{i});
                zoneCase{end+1} = zone.Impedance_Rigid(id.Wall,cst.c0);
            otherwise
                zoneCase =  addImpedanceZone(zoneCase,name{i},id.(name{i}),cst.Impedance,cst.c0,zone);
        end
    end
end

function zoneCase = addImpedanceZone(zoneCase,fluxType,idImpedance,Impedance,c0,zone)
%addImpedanceZone Add impedance zones to zoneCase.
% Input: 
%   zoneCase (cell)
%   fluxType (char)     zone name
%   idImpedance (P) id of physical entities attached to zone 'fluxType'
%   Impedance (ImpedanceBoundaryCondition)
%   c0 sound speed
%   zone (struct)       Available zones.
%  Ouput:
%   zoneCase (cell)
    % -- Define generic arguments
    switch fluxType
            case 'Impedance_FluxZunstable'
                validateattributes(Impedance,{'ImpedanceBoundaryCondition'},{},mfilename,'cst.Impedance');
                zoneImpedance_Prop = zone.Impedance_Zunstable_Prop;
                coeff_Prop = Impedance.beta0;
                zoneImpedance_Derivative = zone.Impedance_Zunstable_Deriv;
                coeff_Deriv = @(x,y)[Impedance.beta0(x,y),Impedance.a1(x,y)];
                zoneImpedance_Operator= zone.Impedance_Zunstable_BndOp;
                coeff_DirectDelay=cell(0);
                for i=1:length(Impedance.DirectDelay)
                    coeff_DirectDelay{i}=@(x,y)[Impedance.beta0(x,y),Impedance.DirectDelay{i}.Coeff(x,y)];
                end
                coeff_BndOp = cell(0);
                for i=1:length(Impedance.BndOp)
                    coeff_BndOp{i} = @(x,y)[Impedance.beta0(x,y),Impedance.BndOp{i}.Coeff(x,y)];
                end
                coeff_BndOpDelay = cell(0);
                for i=1:length(Impedance.BndOpDelay)
                    coeff_BndOpDelay{i} = @(x,y)[Impedance.beta0(x,y),Impedance.BndOpDelay{i}.Coeff(x,y)];
                end
            case 'Impedance_FluxZ'
                validateattributes(Impedance,{'ImpedanceBoundaryCondition'},{},mfilename,'cst.Impedance');
                zoneImpedance_Prop = zone.Impedance_Z_Prop;
                coeff_Prop = Impedance.beta0;
                zoneImpedance_Derivative = zone.Impedance_Z_Deriv;
                coeff_Deriv = Impedance.a1;
                zoneImpedance_Operator= zone.Impedance_Z_BndOp;
                coeff_DirectDelay=cell(0);
                for i=1:length(Impedance.DirectDelay)
                    coeff_DirectDelay{i}=Impedance.DirectDelay{i}.Coeff;
                end
                coeff_BndOp = cell(0);
                for i=1:length(Impedance.BndOp)
                    coeff_BndOp{i} = Impedance.BndOp{i}.Coeff;
                end
                coeff_BndOpDelay = cell(0);
                for i=1:length(Impedance.BndOpDelay)
                    coeff_BndOpDelay{i} = Impedance.BndOpDelay{i}.Coeff;
                end
            case 'Impedance_FluxY'
                validateattributes(Impedance,{'ImpedanceBoundaryCondition'},{},mfilename,'cst.Impedance');
                error('[%s] FluxY not implemented.',mfilename);
            case 'Impedance_FluxZY'
                    % Annoying case: call a dedicated function
                fprintf('[%s] Physical entity %s: %s.\n',mfilename,mat2str(idImpedance),fluxType);
                zoneCase = addImpedanceZone_ZY(zoneCase,idImpedance,Impedance,c0,zone);
                return
            case 'Impedance_FluxBeta'
                validateattributes(Impedance,{'ImpedanceBoundaryCondition'},{},mfilename,'cst.Impedance');
                zoneImpedance_Prop = zone.Impedance_Beta_Prop;
                coeff_Prop = @(x,y)(1+Impedance.beta0(x,y))./(1-Impedance.beta0(x,y));
                if Impedance.beta0(1,1)==1
                    error('[%s] Infinity value for prop. impedance coeff.\n',mfilename);
                end
                zoneImpedance_Derivative = zone.Impedance_Beta_Deriv;
                coeff_Deriv = Impedance.a1;
                zoneImpedance_Operator= zone.Impedance_Beta_BndOp;
                coeff_DirectDelay=cell(0);
                for i=1:length(Impedance.DirectDelay)
                    coeff_DirectDelay{i}=Impedance.DirectDelay{i}.Coeff;
                end
                coeff_BndOp = cell(0);
                for i=1:length(Impedance.BndOp)
                    coeff_BndOp{i} = Impedance.BndOp{i}.Coeff;
                end
                coeff_BndOpDelay = cell(0);
                for i=1:length(Impedance.BndOpDelay)
                    coeff_BndOpDelay{i} = Impedance.BndOpDelay{i}.Coeff;
                end
        otherwise
                error('[%s] Unknown zone: %s\n',mfilename,fluxType);
    end
    % -- Add zones to zoneCase
    fprintf('[%s] Physical entity %s: %s.\n',mfilename,mat2str(idImpedance),fluxType);
    zoneCase{end+1} = zoneImpedance_Prop(idImpedance,coeff_Prop,c0);
    zoneCase{end+1} = zoneImpedance_Derivative(idImpedance,coeff_Deriv,c0);
    fprintf('\t Direct delay: %d.\n',length(coeff_DirectDelay));
    for i=1:length(coeff_DirectDelay)
        zoneCase{end+1} = zoneImpedance_Operator(idImpedance,coeff_DirectDelay{i},c0,'Impedance_DirectDelay');
    end
    fprintf('\t Undelayed boundary operators: %d.\n',length(coeff_BndOp));
    for i=1:length(coeff_BndOp)
       zoneCase{end+1}=zoneImpedance_Operator(idImpedance,coeff_BndOp{i},c0,'Impedance_BoundaryOperator'); 
    end
    fprintf('\t Delayed boundary operators: %d.\n',length(coeff_BndOpDelay));
    for i=1:length(coeff_BndOpDelay)
        zoneCase{end+1}=zoneImpedance_Operator(idImpedance,coeff_BndOpDelay{i},c0,'Impedance_BoundaryOperatorDelayed');
    end
end


function zoneCase = addImpedanceZone_ZY(zoneCase,idImpedance,Impedance,c0,zone)
%addImpedanceZone_ZY Add impedance zones to zoneCase. (ZY formulation only)
% Rmk: the 'ZY' case is covered here rather than in addImpedanceZone for
% clarity only. Justification: In the 'ZY' case, boundary operators
% observe either 'p' or 'un', which is slightly more complex to handle.
% Input: 
%   zoneCase (cell)
%   idImpedance (P) id of physical entities attached to zone 'fluxType'
%   Impedance (ImpedanceBoundaryCondition_ZY)
%   c0 sound speed
%   zone (struct)       Available zones.
%  Ouput:
%   zoneCase (cell)

    % -- Validate argument
    validateattributes(Impedance,{'ImpedanceBoundaryCondition_ZY'},{},mfilename,'cst.Impedance');
    
    % -- Define generic arguments
    zoneImpedance_Prop = zone.Impedance_ZY_Prop;
    coeff_Prop = Impedance.Prop;
    zoneImpedance_Derivative = zone.Impedance_ZY_Deriv;
    coeff_Deriv = Impedance.Deriv;
    zoneImpedance_OperatorZ = zone.Impedance_ZY_BndOpZ;
    zoneImpedance_OperatorY = zone.Impedance_ZY_BndOpY;
    idx_Z_DD = Impedance.idx_Z_DD;
    idx_Y_DD = Impedance.idx_Y_DD;
    idx_Z_BndOp = Impedance.idx_Z_BndOp;
    idx_Y_BndOp = Impedance.idx_Y_BndOp;
    idx_Z_BndOpDelay = Impedance.idx_Z_BndOpDelay;
    idx_Y_BndOpDelay = Impedance.idx_Y_BndOpDelay;

    % -- Add zones to zoneCase
    zoneCase{end+1} = zoneImpedance_Prop(idImpedance,coeff_Prop,c0);
    zoneCase{end+1} = zoneImpedance_Derivative(idImpedance,coeff_Deriv,c0);
    fprintf('\t Direct delay: %d (z) and %d (y).\n',length(idx_Z_DD),length(idx_Y_DD));
        % Direct delay: z operators (idx_Z)
    for i=idx_Z_DD
        zoneCase{end+1} = zoneImpedance_OperatorZ(idImpedance,Impedance.DirectDelay{i}.Coeff,c0,'Impedance_DirectDelay');        
    end
        % Direct delay: y operators (idx_Y)
    for i=idx_Y_DD
        zoneCase{end+1} = zoneImpedance_OperatorY(idImpedance,Impedance.DirectDelay{i}.Coeff,c0,'Impedance_DirectDelay');        
    end
    fprintf('\t Undelayed boundary operators: %d (z) and %d (y).\n',length(idx_Z_BndOp),length(idx_Y_BndOp));
        % Boundary operator: z operators (idx_Z)
    for i=idx_Z_BndOp
        zoneCase{end+1}=zoneImpedance_OperatorZ(idImpedance,Impedance.BndOp{i}.Coeff,c0,'Impedance_BoundaryOperator');
    end
        % Boundary operator: y operators (idx_Y)
    for i=idx_Y_BndOp
        zoneCase{end+1}=zoneImpedance_OperatorY(idImpedance,Impedance.BndOp{i}.Coeff,c0,'Impedance_BoundaryOperator');
    end
    fprintf('\t Delayed boundary operators: %d (z) and %d (y).\n',length(idx_Z_BndOpDelay),length(idx_Y_BndOpDelay));
        % Delayed boundary operator: z operator (idx_Z)
    for i=idx_Z_BndOpDelay
        zoneCase{end+1}=zoneImpedance_OperatorZ(idImpedance,Impedance.BndOpDelay{i}.Coeff,c0,'Impedance_BoundaryOperatorDelayed');
    end
        % Delayed boundary operator: y operator (idx_Y)
    for i=idx_Y_BndOpDelay
        zoneCase{end+1}=zoneImpedance_OperatorY(idImpedance,Impedance.BndOpDelay{i}.Coeff,c0,'Impedance_BoundaryOperatorDelayed');
    end
end