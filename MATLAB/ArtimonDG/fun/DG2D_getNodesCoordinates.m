function nodes = DG2D_getNodesCoordinates(DGCell,mesh)
%DG2D_buildPlotTriangles compute the coordinates (x,y) of each node.
% Inputs:
%   DGCell (DGCell_2D_Triangle) DG cell, with Np nodes
%   mesh (Mesh_2D_Triangle) mesh, with Nk triangles
% Outputs:
%   nodes (Np*Nkx2) [x,y]

    validateattributes(DGCell,{'DGCell_2D_Triangle'},{});
    validateattributes(mesh,{'Mesh_2D_Triangle'},{});

Nk = mesh.N;
Np = DGCell.Np;

    % build vector of global coordinates
va = mesh.triangles(:,2); vb = mesh.triangles(:,3); vc = mesh.triangles(:,4);
r = DGCell.xi(:,1); s = DGCell.xi(:,2);
x = 0.5*(-(r+s)*mesh.nodes(va,1)'+(1+r)*mesh.nodes(vb,1)'+(1+s)*mesh.nodes(vc,1)');
y = 0.5*(-(r+s)*mesh.nodes(va,2)'+(1+r)*mesh.nodes(vb,2)'+(1+s)*mesh.nodes(vc,2)');
nodes = [x(:),y(:)];

end

