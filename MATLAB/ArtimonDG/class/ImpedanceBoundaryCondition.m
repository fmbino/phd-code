classdef ImpedanceBoundaryCondition
    %ImpedanceBoundaryCondition (Simple) class that describes an impedance
    %boundary condition with Laplace transform (Re(s)>0):
    %       h(s) = a0                               (Proportional)
    %               + a1*s                          (Derivative)
    %               + ad*exp(-tau_d*s)              (Direct delay)
    %               + aq*Q(s)                       (LTI Operator)
    %               + aqd*Q(s)*exp(-tau_q*s)        (Delayed LTI Operator).
    % h(s) can be z(s) (impedance) or beta(s) (reflection coefficient).
    % Rmk: to include the rigid wall case, the coefficient a0 is given
    % through its reflection coefficient beta0.
    properties (SetAccess = protected)
        Name; % (char) Identifier (for convenience)
        beta0; % (function_handle) reflexion coefficient beta0(x,y)
        a1; % (function_handle) impedance coefficient a1(x,y)
        DirectDelay; % Empty or (cell of struct) 'Coeff',  'Delay'
        BndOp; % Empty or (cell of struct) 'Coeff', 'Operator'
        BndOpDelay; % Empty or (cell of struct) 'Coeff', 'Operator', 'Delay'
        Laplace_ex; % (function_handle) Exact Laplace transform h(s)
    end
    
    methods
        function obj = ImpedanceBoundaryCondition(Name,beta0,a1,DD_coeff,DD_delay,BndOp_coeff,BndOp_op,BndOpDelay_coeff,BndOpDelay_op,BndOpDelay_delay,varargin)
        %ImpedanceBoundaryCondition Class constructor
        % Rmk: Impedance coefficients can be given as either scalar or
        % function of two variables variables: @(x,y). They are always
        % stored as functions of two variables.
        % Rmk: Null coefficient will lead  to the corresponding
        % operator being discarded.
        % Inputs:
        %   beta0 (vector or function_handle) 
        %           reflection coefficient (a0-1)/(a0+1)
        %   a1 (vector or function_handle)
        %   DD_coeff,BndOp_coeff,BndOp_delay (cell of coefficient)
        %   DD_delay,BndOpDelay_delay (cell of scalar)
        %   BndOp_op, BndOpDelay_op (cell of LTIOperator)
        % Inputs (optional):
        %   Laplace_ex (function_handle) Exact Laplace transform
        
                % -- Validate arguments
            validateattributes(Name,{'char'},{},mfilename,'Name');
                    % Proportional
            beta0 = ImpedanceBoundaryCondition.checkImpedanceCoefficient(beta0,'beta0');
                    % Derivative
            a1 = ImpedanceBoundaryCondition.checkImpedanceCoefficient(a1,'a1');            
                    % Direct delay0
            validateattributes(DD_coeff,{'cell'},{},mfilename,'DD_coeff');
            for i=1:length(DD_coeff)
                DD_coeff{i} = ImpedanceBoundaryCondition.checkImpedanceCoefficient(DD_coeff{i},'DD_coeff{i}');
            end
            validateattributes(DD_delay,{'cell'},{'numel',length(DD_coeff)},mfilename,'DD_delay');
            for i=1:length(DD_delay)
                validateattributes(DD_delay{i},{'numeric','scalar'},{},mfilename,'DD_delay{i}');
            end
                    % Boundary operator
            validateattributes(BndOp_coeff,{'cell'},{},mfilename,'BndOp_coeff');
            for i=1:length(BndOp_coeff)
                BndOp_coeff{i} = ImpedanceBoundaryCondition.checkImpedanceCoefficient(BndOp_coeff{i},'BndOp_coeff{i}');
            end
            validateattributes(BndOp_op,{'cell'},{'numel',length(BndOp_coeff)},mfilename,'BndOp_op');
            for i=1:length(BndOp_op)
                if ~obj.checkImpedanceCoefficientNullity(BndOp_coeff{i})
                    validateattributes(BndOp_op{i},{'LTIOperator'},{},mfilename,'BndOp_op{i}');
                end
            end
                    % Delayed Boundary operator
            validateattributes(BndOpDelay_coeff,{'cell'},{},mfilename,'BndOpDelay_coeff');
            for i=1:length(BndOpDelay_coeff)
                BndOpDelay_coeff{i} = ImpedanceBoundaryCondition.checkImpedanceCoefficient(BndOpDelay_coeff{i},'BndOpDelay_coeff{i}');
            end
            validateattributes(BndOpDelay_op,{'cell'},{'numel',length(BndOpDelay_coeff)},mfilename,'BndOpDelay_op');
            for i=1:length(BndOpDelay_op)
                if ~obj.checkImpedanceCoefficientNullity(BndOpDelay_coeff{i})
                    validateattributes(BndOpDelay_op{i},{'LTIOperator'},{},mfilename,'BndOpDelay_op{i}');
                end
            end                    
            validateattributes(BndOpDelay_delay,{'cell'},{'numel',length(BndOpDelay_coeff)},mfilename,'BndOpDelay_delay');
            for i=1:length(BndOpDelay_delay)
                validateattributes(BndOpDelay_delay{i},{'numeric','scalar'},{},mfilename,'BndOpDelay_delay{i}');
            end
                    % Laplace transform
            p = inputParser; p.FunctionName = mfilename;
            addParameter(p,'Laplace_ex',@(s)(1),@(x)(validateattributes(x,{'function_handle'},{},mfilename,'Laplace_ex')));
            parse(p,varargin{:});
                % -- Assign values
            obj.Name = Name;
                    % Proportional
            obj.beta0= beta0;
                    % Derivative
            obj.a1 = a1;
                    % Direct delay
                    % Non empty iff at least one non-null element
            obj.DirectDelay = cell(0);
            for i=1:length(DD_coeff)
                if ~obj.checkImpedanceCoefficientNullity(DD_coeff{i})
                    obj.DirectDelay{end+1} = struct('Coeff',DD_coeff{i},'Delay',DD_delay{i});
                else
                    fprintf('[%s] DD_coeff{%d} deemed null.\n',mfilename,i);
                end
            end
                    % Boundary operator
                    % Non empty iff at least one non-null element
            obj.BndOp = cell(0);
            for i=1:length(BndOp_coeff)
                if ~obj.checkImpedanceCoefficientNullity(BndOp_coeff{i})
                    obj.BndOp{end+1}=struct('Coeff',BndOp_coeff{i},'Operator',BndOp_op{i});
                else
                    fprintf('[%s] BndOp_coeff{%d} deemed null.\n',mfilename,i);
                end
            end
                    % Delayed boundary operator
                    % Non empty iff at least one non-null element
            obj.BndOpDelay = cell(0);
            for i=1:length(BndOpDelay_coeff)
                if ~obj.checkImpedanceCoefficientNullity(BndOpDelay_coeff{i})
                        % add an element to tmp
                    obj.BndOpDelay{end+1}=struct('Coeff',BndOpDelay_coeff{i},'Operator',BndOpDelay_op{i},'Delay',BndOpDelay_delay{i});
                else
                    fprintf('[%s] BndOpDelay_coeff{%d} deemed null.\n',mfilename,i);
                end
            end
                    % Exact Laplace transform
                    % (It could  be computed, but that may be slow.)
            obj.Laplace_ex = p.Results.Laplace_ex;
        end
        
        
        function obj = multiplyCoefficient(obj,phi)
        %multiplyCoefficient Multiply impedance coefficient by function
        %phi(x,y). Typically, phi(x,y) is the indicator function of the
        %liner position.
        % Input:
        %   phi (function_handle) phi(x,y)
            validateattributes(phi,{'function_handle'},{},mfilename,'phi');
            phi = obj.checkImpedanceCoefficient(phi,'phi');
                    % Multiply beta0
            obj.beta0 = @(x,y)obj.beta0(x,y).*phi(x,y);        
                    % Multiply a1
            obj.a1 = @(x,y)obj.a1(x,y).*phi(x,y);
                    % Multiply delay coefficient
            for i=1:length(obj.DirectDelay)
                obj.DirectDelay{i}.Coeff = @(x,y)obj.DirectDelay{i}.Coeff(x,y).*phi(x,y);
            end
                    % Multiply BndOp coefficient
            for i=1:length(obj.BndOp)
                obj.BndOp{i}.Coeff = @(x,y)obj.BndOp{i}.Coeff(x,y).*phi(x,y);
            end
                    % Multiply BndOpDelay coefficient
            for i=1:length(obj.BndOpDelay)
                obj.BndOpDelay{i}.Coeff = @(x,y)obj.BndOpDelay{i}.Coeff(x,y).*phi(x,y);
            end           
        end
        
        function checkRealizationStability(obj)
        %checkRealizationStability Elementary function to check the
        %stability of the realization in the time-domain impedance boundary
        %condition.
            boolStable = 1;
                % direct delay sign
            for i=1:length(obj.DirectDelay)
                boolStable = boolStable.*(obj.DirectDelay{i}.Delay>=0);
            end
                    % Boundary operator
            for i=1:length(obj.BndOp)
                boolStable = boolStable.*((max(real(eig(full(obj.BndOp{i}.Operator.A)))))<0);
            end
                    % Boundary delayed operator
            for i=1:length(obj.BndOpDelay)
                boolStable = boolStable.*((max(real(eig(full(obj.BndOpDelay{i}.Operator.A)))))<0);
            end
            
            if boolStable
               fprintf('[%s] TDIBC has stable realization(s)\n',mfilename); 
            else
               fprintf('[%s] TDIBC has at least one unstable realization\n',mfilename); 
            end
        end
        
        function checkPassivity(obj,freq,funType,x,y)
        %checkPassivity Elementary function to check the passivity of the
        %impedance boundary condition z(s,x,y) over a frequency range.
        % Input
        %   freq (N) frequency (Hz)
        %   funType (str) Indicates what is stored in the object
        %       'impedance'  impedance z(s)
        %       'reflcoeff'  reflection coefficient beta(s)
        %   x,y (scalar) coordinates

            validateattributes(freq,{'numeric'},{'vector'},mfilename,'freq');
            validateattributes(funType,{'char'},{},mfilename,'funType');
            validateattributes(x,{'numeric'},{'scalar'});
            validateattributes(y,{'numeric'},{'scalar'});

                % -- Definition of z
            beta2z = @(beta)((1+beta)./(1-beta));
            switch funType
                case 'impedance'
                        % Impedance is stored
                    z_dis = @(s)obj.Laplace(s,1,1);
                case 'reflcoeff'
                        % Reflection coefficient is stored
                    beta_dis = @(s)obj.Laplace(s,1,1);
                    z_dis = @(s)beta2z(beta_dis(s));
                otherwise
                    error('[%s] funType unknown.',mfilename);
            end
            Z = z_dis(1i*2*pi*freq);
            if min(real(Z))<0
               fprintf('[%s] Impedance not passive over [%1.1e,%1.1e]Hz.\n',mfilename,freq(1),freq(end)); 
            else
               fprintf('[%s] Impedance passive over [%1.1e,%1.1e]Hz.\n',mfilename,freq(1),freq(end)); 
            end
        end
        
        function printImpedanceSummary(obj)
        %printImpedance
            fprintf('%s\n',obj.getImpedanceSummary());
        end
        
        function str = getImpedanceSummary(obj)
        %getImpedanceSummary
            str = sprintf('[%s] Summary of Impedance object <%s>:\n',mfilename,obj.Name);
            str = [str,sprintf('\t beta0=%s | a1=%s | \n',func2str(obj.beta0),func2str(obj.a1))];
            str = [str,sprintf('\t Direct delay: %d\n',length(obj.DirectDelay))];
            str = [str,sprintf('\t Boundary operator: %d\n',length(obj.BndOp))];
            for i=1:length(obj.BndOp)
                str = [str,sprintf('\t\tBndOp{%d} dim:%d, max. frequency: %1.3e rad/s %1.3e Hz, max element %1.2e\n',i,size(obj.BndOp{i}.Operator.A,1),obj.BndOp{i}.Operator.computeMaxPulsation(),obj.BndOp{i}.Operator.computeMaxPulsation()/(2*pi),obj.BndOp{i}.Operator.computeMaxElement())];
            end
            str = [str,sprintf('\t Delayed boundary operator: %d\n',length(obj.BndOpDelay))];
            for i=1:length(obj.BndOpDelay)
                str = [str,sprintf('\t\tBndOp{%d} dim:%d, max. frequency: %1.3e rad/s %1.3e Hz, max element %1.2e\n',i,size(obj.BndOpDelay{i}.Operator.A,1),obj.BndOpDelay{i}.Operator.computeMaxPulsation(),obj.BndOpDelay{i}.Operator.computeMaxPulsation()/(2*pi),obj.BndOpDelay{i}.Operator.computeMaxElement)];
            end
        end
        
        function Z = Laplace(obj,s,x,y)
        %Laplace Actual Laplace transform.
        % Inputs:
        %   s (PxR) complex Laplace variable (rad/s)
        %   x,y (scalar) coordinates
        % Outputs:
        %   Z (PxR) Laplace transform Z(s)
            validateattributes(s,{'numeric'},{'2d'});
            validateattributes(x,{'numeric'},{'scalar'});
            validateattributes(y,{'numeric'},{'scalar'});
            if obj.beta0(x,y)==1
                Z=Inf*ones(size(s));
                return
            end
            Z = (1+obj.beta0(x,y))/(1-obj.beta0(x,y))*ones(size(s));
            Z = Z +obj.a1(x,y) * s;
            for i=1:length(obj.DirectDelay)
                Z = Z + obj.DirectDelay{i}.Coeff(x,y)*exp(-obj.DirectDelay{i}.Delay*s);
            end
            for i=1:length(obj.BndOp)
                Z = Z + obj.BndOp{i}.Coeff(x,y)*obj.BndOp{i}.Operator.Laplace(s);
            end
            for i=1:length(obj.BndOpDelay)
                Z = Z + obj.BndOpDelay{i}.Coeff(x,y)*obj.BndOpDelay{i}.Operator.Laplace(s).*exp(-obj.BndOpDelay{i}.Delay*s);
            end
        end
        
        function [Z,beta] = plot(obj,freq,funType,varargin)
        %plot Plot the Fourier transform of the impedance boundary
        % condition. Curves can be retrieved in variables 'Z' and 'beta'.
        % Plot is appended to the current figure.
        % Coordinates (1,1) assumed, i.e. z(s,x=1,y=1)
        % Input:
        %   freq (N) Plot frequencies (Hz)
        %   funType (str) Indicates what is stored in the object
        %       'impedance'  impedance z(s)
        %       'reflcoeff'  reflection coefficient beta(s)
        % Input(optional):
        %   LineSpec (str) Line style, marker symbol, and color
        % Output:
        %   Z (N)       Impedance z(2*pi*1i*freq)                            
        %   beta (N)    Reflection coefficient beta(2*pi*1i*freq)
        
                % -- Validate arguments
            validateattributes(freq,{'numeric'},{'vector'},mfilename,'freq');
            validateattributes(funType,{'char'},{},mfilename,'funType');
                    % Optional argument
            p = inputParser; p.FunctionName = mfilename;
            addParameter(p,'LineSpec','r--',@(x)(validateattributes(x,{'char'},{})));
            parse(p,varargin{:});  oA = p.Results;
            LineSpec = oA.LineSpec;

                % -- Definition of beta from z and vice versa
            beta2z = @(beta)((1+beta)./(1-beta));
            z2beta = @(z)((z-1)./(z+1));
            switch funType
                case 'impedance'
                        % Impedance is stored
                    z_dis = @(s)obj.Laplace(s,1,1);
                    beta_dis = @(s)z2beta(z_dis(s));                    
                case 'reflcoeff'
                        % Reflection coefficient is stored
                    beta_dis = @(s)obj.Laplace(s,1,1);
                    z_dis = @(s)beta2z(beta_dis(s));
                otherwise
                    error('[%s] funType unknown.',mfilename);
            end
                % -- Extract diffusive and complex poles
                % This part checks wether the object contains an
                % oscillatory-diffusive representation.
                % This is only for display purposes, to give a more
                % informative plot.
                % Assumption: diffusive operators all have the same poles.
            xi = 0; poles = 0;
            for i=1:length(obj.BndOp)
                if isa(obj.BndOp{i}.Operator,'DiffusiveOperator')
                    xi = full(diag(obj.BndOp{i}.Operator.A));
                    poles = xi(imag(xi)~=0);
                    xi = xi(imag(xi)==0);
                end
            end
            for i=1:length(obj.BndOpDelay)
                if isa(obj.BndOpDelay{i}.Operator,'DiffusiveOperator')
                    xi = full(diag(obj.BndOpDelay{i}.Operator.A));
                    poles = xi(imag(xi)~=0);
                    xi = xi(imag(xi)==0);
                end
            end            
                % -- Plot
            s = 1i*2*pi*freq;
            Z = z_dis(s);
            beta = beta_dis(s);
            subplot(2,2,1)
            hold all
            plot(freq,real(Z),LineSpec);
            xlabel('Frequency (Hz)');
            ylabel('Normalized resistance (rad)');
            subplot(2,2,2)
            hold all
            plot(freq,imag(Z),LineSpec);
            xlabel('Frequency (Hz)');
            ylabel('Normalized reactance (rad)'); 
            subplot(2,2,3)
            hold all
            plot(freq,abs(beta),LineSpec);
            xlabel('Frequency (Hz)');
            ylabel('|reflection coefficient| (rad)');
            subplot(2,2,4)
            hold all
            plot(freq,angle(beta),LineSpec,'DisplayName',sprintf('Discrete %s\n%d xi_k [%1.1e, %1.1e] Hz, %d s_n [%1.1e, %1.1e] Hz',obj.Name,length(xi),min(xi)/(2*pi),max(xi)/(2*pi),length(poles),min(abs(poles))/(2*pi),max(abs(poles))/(2*pi)));
            xlabel('Frequency (Hz)');
            ylabel('phase(reflection coefficient) (deg)');
        end
        
        function exportImpedance(obj,freq,funType)
        %exportImpedance Export the impedance values in a .csv file.
        % Coordinates (1,1) assumed, i.e. z(s,x=1,y=1)
        % CSV file format:
        % Freq(kHz),Re(z),Im(z),|beta|,phase(beta)(deg)
        % Input:
        %   freq (N) Plot frequencies (Hz)
        %   funType (str) Indicates what is stored in the object
        %       'impedance'  impedance z(s)
        %       'reflcoeff'  reflection coefficient beta(s)
        
                % Get values of Z and beta
            [Z,beta] = obj.plot(freq,funType);
            namestr = sprintf('%s.csv',obj.Name);
            dlmwrite(namestr,'Freq(kHz),Re(z),Im(z),|beta|,phase(beta)(deg)','Delimiter','');
            dlmwrite(namestr,[freq(:)/1e3,real(Z(:)),imag(Z(:)),abs(beta(:)),rad2deg(angle(beta(:)))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
        end
        
        function [weightBndOp,weightBndOpDelay,delay,xi,sn] = getPolesandWeights(obj)
        %getPolesandWeights Returns poles and weights for all the diffusive
        %operators.
        % Assumption: diffusive operators all have the same poles (xi,sn).
        % Outputs:
        %   weightBndOp (P cell array) weights for each of the P BndOp
        %   (diffusive pole first)
        %   weightBndOpDelay (Q cell array) weights for each of the Q
        %   BndOpDelay (diffusive pole first)
        %   delay (Q cell array) delay of each BndOpDelay operator
        %   xi,sn (R) common poles for all operators
        
                    % -- Extract diffusive and complex poles
            % This part checks wether the object contains an
            % oscillatory-diffusive representation.
            % This is only for display purposes, to give a more
            % informative plot.
            % Assumption: diffusive operators all have the same poles.
            xi = 0; sn = 0;
            weightBndOp = cell(length(obj.BndOp));
            weightBndOpDelay = cell(length(obj.BndOpDelay));
            delay = cell(length(obj.BndOpDelay));
            for i=1:length(obj.BndOp)
                if isa(obj.BndOp{i}.Operator,'DiffusiveOperator')
                    xi = full(diag(obj.BndOp{i}.Operator.A));
                    sn = xi(imag(xi)~=0);
                    xi = xi(imag(xi)==0);
                    weightBndOp{i} = full(obj.BndOp{i}.Operator.C);
                end
            end
            for i=1:length(obj.BndOpDelay)
                if isa(obj.BndOpDelay{i}.Operator,'DiffusiveOperator')
                    xi = full(diag(obj.BndOpDelay{i}.Operator.A));
                    sn = xi(imag(xi)~=0);
                    xi = xi(imag(xi)==0);
                    weightBndOpDelay{i} = full(obj.BndOpDelay{i}.Operator.C);
                    delay{i} = obj.BndOpDelay{i}.Delay;
                end
            end
        end
        
        
        function printPolesAndWeights(obj,digit)
        %printPolesAndWeights Print poles and weights for all diffusive
        %operators.
        % Inputs:
        %   digit (1) Number of digits after the decimal point.

            validateattributes(digit,{'numeric'},{'scalar','integer'},mfilename,'digit');
            fmt = sprintf('%%25.%de',digit);
            
            beta2z = @(beta)((1+beta)./(1-beta));
            fprintf(sprintf('beta0 = %s\n',fmt),beta2z(obj.beta0(1,1)));
            [weightBndOp,weightBndOpDelay,delay,xi,sn] = obj.getPolesandWeights();
            for i=1:length(weightBndOp)
                fprintf('--- BndOp #%d\n',i);
                fprintf('Poles (rad/s) | Weight\n');
                num2str([[xi(:);sn(:)],weightBndOp{i}(:)],[fmt,' '])        
            end
            for i=1:length(weightBndOp)
                fprintf('--- BndOpDelay #%d delay=%2.6es\n',i,delay{i});
                fprintf('Poles (rad/s) | Weight\n');
                num2str([[xi(:);sn(:)],weightBndOpDelay{i}(:)],[fmt,' '])        
            end
        end
        
        function plotPolesAndWeights(obj,freqMax)
        %plotPolesAndWeights Plot poles and weights (assume that the
        %ImpedanceBoundary condition contains an oscillatory-diffusive
        %representation.)
        % Inputs:
        %   freqMax (1) Max. frequency (Hz) (for display purposes only)

        [weightBndOp,weightBndOpDelay,delay,xi,sn] = obj.getPolesandWeights();        
        weight1 = weightBndOp{1};
        weight2 = weightBndOpDelay{1};
        delay = delay{1};
        
        subplot(3,1,1)
        hold all
        grid on
        plot(real([xi(:);sn]),imag([xi(:);sn]),'x','DisplayName',sprintf('%s (delay=%1.2es)',obj.Name,delay));
        A = max(abs([-xi(:);sn]));  
        plot(A*cos(linspace(0,2*pi,1e2)),A*sin(linspace(0,2*pi,1e2)),'DisplayName',sprintf('Radius at %1.2eHz',A/(2*pi)));
        plot(2*pi*freqMax*cos(linspace(0,2*pi,1e2)),2*pi*freqMax*sin(linspace(0,2*pi,1e2)),'DisplayName',sprintf('Radius at %1.2eHz',freqMax));
        legend('show');
        % set(gca,'DataAspectRatio',[1,1,1])
        title(sprintf('%d Poles (Max:%1.1e rad/s %1.1e Hz)',numel([-xi(:);sn]),A,A/(2*pi)));
        xlabel('Re(s) (rad/s)');
        xlabel('Im(s) (rad/s)');
        subplot(3,1,2)
        hold all
        grid on
        plot(real(weight1),imag(weight1),'x');
        plot(A*cos(linspace(0,2*pi,1e2)),A*sin(linspace(0,2*pi,1e2)),'DisplayName',sprintf('Radius at %1.2eHz',A/(2*pi)));
        plot(2*pi*freqMax*cos(linspace(0,2*pi,1e2)),2*pi*freqMax*sin(linspace(0,2*pi,1e2)),'DisplayName',sprintf('Radius at %1.2eHz',freqMax));
        % set(gca,'DataAspectRatio',[1,1,1])
        title(sprintf('Weights of h1 (Max:%1.1e)',max(abs(weight1))));
        subplot(3,1,3)
        hold all
        grid on
        plot(real(weight2),imag(weight2),'x');
        plot(A*cos(linspace(0,2*pi,1e2)),A*sin(linspace(0,2*pi,1e2)),'DisplayName',sprintf('Radius at %1.2eHz',A/(2*pi)));
        plot(2*pi*freqMax*cos(linspace(0,2*pi,1e2)),2*pi*freqMax*sin(linspace(0,2*pi,1e2)),'DisplayName',sprintf('Radius at %1.2eHz',freqMax));
        % set(gca,'DataAspectRatio',[1,1,1])
        title(sprintf('Weights of h2 (Max:%1.1e)',max(abs(weight2))));
        end
    end
    
    methods(Static)
        function a = checkImpedanceCoefficient(a,name)
        %checkImpedanceCoefficient Check if an impedance coefficient is a
        %salar or a function of two variables: a(x,y).
        % If a is numeric, it is corrected to be a function.
            if ~isa(a,'function_handle')
                if ~isa(a,'numeric')
                    error('[%s] %s should be: function_handle or numeric.',mfilename,name);
                else
                    a = @(x,y)a;
                end
            else
                if nargin(a)~=2
                    error('[%s] %s should have 2 arguments: %s(x,y).',mfilename,name,name);
                end
            end
            
        end
        
        function bool = checkImpedanceCoefficientNullity(a)
        %checkImpedanceCoefficientNullity Check if the impedance
        %coefficient a is null.
        % Input:
        %   a (function_handle) a(x,y)
            bool = false;
            if (a(1,1)==0) && (a(0.5,0.5)==0)
                bool = true;
            end
        end
        
    end
    
end

