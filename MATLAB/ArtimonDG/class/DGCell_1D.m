classdef DGCell_1D
    %DGCell_1D 1D Discontinuous Galerkin (DG) cell.
    % This class implements a 1D DG cell.
    
    properties

        N; % (1) number of nodes per cell    
        xi; % LGL points associated to define the Lagrange polynomial [-1,1]        
    end
    
    methods
        function cell = DGCell_1D(N)
        %DGCell_1D Class constructor.
        %Input:
        %   N (1) number of nodes per cell
        
                % -- Arguments check
            validateattributes(N,{'numeric'},{'2d','size',[1,1]});
            validateattributes(N,{'numeric'},{'integer','scalar','>',1});
                % ---    
            cell.N = N;
            cell.xi = JacobiGL(0,0,N-1);
        end
        
        
        
        function show(obj)
        %show Show the cell in a new plot window.

        end
        
        function printSummary(obj)
        %PrintSummary Display an object summary on the standard input.
                % Display
            fprintf('---- 1D DG cell summary:\n');
            fprintf('Number of nodes per cell: [%d] \n',obj.N);
            fprintf('Position of the nodes:\n');
            for i=1:length(obj.xi)
                fprintf('\t%1.4g\n', obj.xi(i));               
            end            
            fprintf('------------------\n');
        end
    end    
end

