classdef DGMesh_2D_Triangle
%DGMesh_2D_Triangle DG mesh, which results from joining a triangular mesh
%and a DG cell.

    properties
        N; % (1) Number of DG nodes
        mesh; % (Mesh_2D_Triangle) mesh    
        DGCell; % (DGCell_2D_Triangle) DGCell
        % -- Nodes connectivity 
        vmapM; % (Nfpx3xNk) Interior nodes ('-')
        vmapP % (Nfpx3xNk) Corresponding exterior nodes ('+')
        % -- Index functions
        % Index for element k and point n°n in the global vector q
        % k in [[1,Nk]]
        % range for the whole element k
        idx_q_el;
    end
    
    methods
        function DGMesh = DGMesh_2D_Triangle(mesh,DGCell)
        %DGCell_2D_Triangle Class constructor.
        %Input:
        %   mesh (Mesh_2D_Triangle) triangular mesh
        %   DGCell (DGCell_2D_Triangle) DG cell
        
                % -- Arguments check
            validateattributes(mesh,{'Mesh_2D_Triangle'},{});
            validateattributes(DGCell,{'DGCell_2D_Triangle'},{});
                % ---
            DGMesh.mesh = mesh;
            DGMesh.DGCell = DGCell;
            [DGMesh.vmapM,DGMesh.vmapP] = DG2D_buildNodeConnectivity(mesh,DGCell);
            DGMesh.N = mesh.N*DGCell.Np;
            DGMesh.vmapM = reshape(DGMesh.vmapM,DGCell.Nfp,3,mesh.N);
            DGMesh.vmapP = reshape(DGMesh.vmapP,DGCell.Nfp,3,mesh.N);
            
            Np = DGCell.Np;
            DGMesh.idx_q_el=@(k,Nq)((k-1)*Np*Nq+(1:(Np*Nq)));
        end
        
        function idx = getNearestNodeIndex(obj,XI)
        %getNearestNodeIndex Return the indices of the nearest DG nodes.
        % Input:
        %   XI (Px2) [x,y] coordinates
        % Output:
        %   idx (P) indices of nearest DG nodes.
            
            validateattributes(XI,{'numeric'},{'ncols',2},mfilename,'XI');
            X = obj.getNodesCoordinates();
            [idx,d] = dsearchn(X,XI);
            fprintf('[%s] %d nodes requested.\n',mfilename,length(idx));
            for i=1:length(idx)
                fprintf('Node (x,y)=\t (%12.3e,%12.3e)\n',XI(i,1),XI(i,2));
                fprintf('\t Nearest (%12.3e,%12.3e) (#%d/%d)\n',X(idx(i),1),X(idx(i),2),idx(i),size(X,1));
                fprintf('\t L2-distance: %1.3e\n',d(i));              
            end
        end
        function showDGNodes(obj,varargin)
        %showDGNodes Plot the DG nodes. Note that no triangulation is
        %associated with them (see showPlotMesh).
        % Input (Optional):
        %   HighlightNodesIndex Highlight somes nodes.
        
                    % Optional arguments
            p = inputParser; p.FunctionName = mfilename;
            addParameter(p,'HighlightNodesIndex',[],@(x)(validateattributes(x,{'numeric'},{'vector'})));
            parse(p,varargin{:}); oA = p.Results; % structure which contains optional arguments
            idx = oA.HighlightNodesIndex(:);

            x = obj.getNodesCoordinates();
            clf
            hold all
                % Plot mesh
            obj.mesh.show();
            hc=get(legend(gca),'String'); % get legend from current axes.
            plot(x(:,1),x(:,2),'r.');
            legend(hc,sprintf('DG nodes'));
            if ~isempty(idx)
                plot(x(idx,1),x(idx,2),'kx');
                hc=get(legend(gca),'String');
                legend(hc,sprintf('Highlighted nodes'));
            end
            xlabel('x');
            ylabel('y');
            title(sprintf('DG nodes\n(Nk=%d triangles, order N=%d, %d DG nodes)',obj.mesh.N,obj.DGCell.N+1,obj.N));
            set(gca, 'DataAspectRatio', [1 1 1]);
                % Custom plot window datatip so that it gives node index
            if exist('datatipcallback_DataIndex')~=0
                hdt = datacursormode;
                set(hdt,'DisplayStyle','Window','UpdateFcn',@datatipcallback_DataIndex);
            else
                warning('[DGMesh]: <%s> missing, plot datatip won''t show data index','datatipcallback_DataIndex');
            end
        end
        
        function showPlotMesh(obj)
        %Display Display the DG plot mesh in a new plot window.
        % Remark: the DG mesh cannot be plotted, as there is no triangle
        % connectivity associated with it.
        
            [trianglesPlot, nodesPlot] = buildPlotMesh(obj);
            clf
            trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),0*nodesPlot(:,1));
            xlabel('x');
            ylabel('y');
            set(gca, 'DataAspectRatio', [1 1 1])
            %axis([0,0.10,0,0.3,0,0.1])
            title(sprintf('DG plot mesh\n(Nk=%d triangles, order N=%d, %d DG nodes)',obj.mesh.N,obj.DGCell.N+1,obj.N));
            view(0,90);
        end
        
        function printSummary(obj)
        %PrintSummary Display an object summary on the standard input.
                % Display
            fprintf('DGMesh: %d DG nodes.\n',obj.N);
        end
        
        %%%%%%%%%% Plot related
        function [trianglesPlot, nodesPlot] = buildPlotMesh(obj)
        %DG2D_buildPlotMesh Build the plot mesh.
        % The plot mesh is built from an equidistant nodal distribution on
        % each DG triangle.
        % Outputs:
        %   trianglesPlot (?x3)
        %   nodesPlot (Nx2)
            [trianglesPlot, nodesPlot] = DG2D_buildPlotMesh(obj.DGCell,obj.mesh);
        end
        
        function nodes = getNodesCoordinates(obj)
        %DG2D_getNodesCoordinates Compute the global coordinates [x,y] of
        %each DG node.
        % Outputs:
        %   nodes (Nx2)
            nodes = DG2D_getNodesCoordinates(obj.DGCell,obj.mesh);
        end
        
        function q_interp = interpolateField2PlotNodes(obj,Nq,q)
        %interpolateField2PlotNodes interpolate the field q to the plot
        %nodes.
        % Inputs:
        %   Nq (1) Number of variables in q.
        %   q (Nk*Np*Nq,1) Field at each DG node.
            validateattributes(Nq,{'numeric'},{'scalar','positive'});
            validateattributes(q,{'numeric'},{'2d','size',[Nq*obj.mesh.N*obj.DGCell.Np,1]});       
            q_interp = DG2D_interpolateField2PlotNodes(obj.DGCell,obj.mesh,Nq,q);
        end
    end    
end