classdef ArtimonDG_LEE_1D
    %ArtimonDG_LEE_1D Global Discontinuous Galerkin (DG) formulation of the
    %monodimensional Linearised Euler Equations (LEE). The equations and
    %boundary conditions are described in Case_LEE_1D.
    % This class builds the global formulation and solves in both the
    %frequency and time domain.
    % The global formulation reads:
    %       M*dx/dt(t) + K*dx/dt(t) = F_source*s(t) + F_delay*x_d(t-dt)
    % where
    %   x (Nk*N*Nq + Nxi) acoustic and diffusive variables
    %   x_d (1+Nxi) delayed variables (one acoustic + Nxi diffusive)
    %   M, K ([x]x[x]) mass and stiffness matrices
    %   F_source ([x]xNq) source flux matrix
    %   F_delay ([x]x[x_d]) delay flux matrix
    
    properties (SetAccess=private)
        
        % -- Objects describing the simulation
        mesh; % (Mesh_1D_Uniform) mesh
        cellDG; % (DGCell_1D) DG cell
        caseLEE; % (Case_LEE_1D) computational Case
        
        % -- Numerical flux functions
            % FVS flux (f(n) = f_+(n) + f_-(n))
            % Outward and inward components of the numerical flux function
            % (FVS split)
            % The conservativity property enables to bypass the normal.
            % (Nq)x(Nq) Outward flux f_+(1) (= -f_-(-1) for conservativity)
        fFVS_out;
            % (NqxNq) Inward flux f_-(1) (=-f_+(-1) for conservativity)
        fFVS_inw;
            % Impedance
            % Centre flux, with ghost state
            % f*(q,nx) = Ax*nx*(q+q_ghost)/2
            %          = f0(nx)*q + f1(nx)*dq/qt + fd1(nx)*D1(q) + fd2(nx)*D2(q)
        f0; % (1) -> (NqxNq) Dependency on q
        f1; % (1) -> (NqxNq) Dependency on dq/dt
        fd1; % (1) -> (Nq) Dependency on D1(q)
        fd2; % (1) -> (Nq) Dependency on D2(q)
        
        % -- Global formulation
        M; % ([x]x[x]) mass matrix
        K; % ([x]x[x]) stiffness matrix
        F_source; % ([x]xNq) source flux matrix
        F_delay; % ([x]x[x_d]) delay flux matrix
        I_delay; % ([x_d]) indices of variables of x that are delayed (i.e. x(I_delay) = x_d.)
        
        % -- Misc
            % Number of minimum PPPW for 1D Wave Equation from [Hu,1999].
            % Function of number of nodes per cell.
        PPW = [0,15.7,10.5,7.9,6.8,6.2,6.2,5.3]; % value for N=7 made up
    end
    
    methods
        function obj = ArtimonDG_LEE_1D(mesh,cellDG,caseLEE)
        %ArtimonDG_LEE_1D Class constructor.
        %Input:
        %   mesh (Mesh_1D_Uniform) Monodimensional mesh
        %   cellDG (DGCell_1D) Monodimensional DG cell
        %   caseLEE (Case_LEE_1D) PDEs and boundary conditions
        
                % -- Arguments check
            validateattributes(mesh,{'Mesh_1D_Uniform'},{});
            validateattributes(cellDG,{'DGCell_1D'},{});
            validateattributes(caseLEE,{'Case_LEE_1D'},{});
                
            obj.mesh = mesh;
            obj.cellDG = cellDG;
            obj.caseLEE = caseLEE;
            
                % -- Initialization of the FVS flux functions
            Nq = obj.caseLEE.Nq;
            [obj.fFVS_out,obj.fFVS_inw] = LEE1D_NumFlux_Interior_FVS(obj.caseLEE.U0,obj.caseLEE.c0);

                % -- Impedance flux function
                    % Proportional
            %[obj.f0] = LEE1D_NumFlux_Impedance_Prop(obj.caseLEE.ImpCoeff(1),obj.caseLEE.Ax);
            %obj.f1 = @(nx)(zeros(Nq,Nq));
            %obj.fd1 = @(nx)(zeros(Nq,1));
            %obj.fd2 = @(nx)(zeros(Nq,1));
                    % Proportional Derivative
            %[obj.f0,obj.f1] = LEE1D_NumFlux_Impedance_PropDer(obj.caseLEE.ImpCoeff(1:2),obj.caseLEE.Ax);
            %obj.fd1 = @(nx)(zeros(Nq,1));
            %obj.fd2 = @(nx)(zeros(Nq,1));
                    % Proportional Derivative Diffusive
            %[obj.f0,obj.f1,obj.fd1] = LEE1D_NumFlux_Impedance_PropDerDiff(obj.caseLEE.ImpCoeff(1:3),obj.caseLEE.Ax);
            %obj.fd2 = @(nx)(zeros(Nq,1));
                    % Universal numerical flux function
            [obj.f0, obj.f1, obj.fd1, obj.fd2] = LEE1D_NumFlux_Impedance_Universal(obj.caseLEE.ImpCoeff, obj.caseLEE.Ax);
        end
                
        % -- Build global formulation
        
        function obj = buildGlobalFormulation(obj)
        %buildGlobalFormulation Build the global formulation: M, K,
        %F_source and F_delay.
                % -- Contributions of elements
            [obj.M,obj.K] = LEE1D_LoopElements(obj.caseLEE.Ax,obj.caseLEE.B,obj.cellDG.xi,obj.mesh);
               % -- Contributions of interior faces
            Fn_int = obj.fFVS_out; % Fn_int = f_+(nx=1) (outward flux)
            Fn_ext = obj.fFVS_inw; % Fn_out = f_-(nx=1) (inward flux)
            F0 = LEE1D_LoopFaces_Interior_FVS(obj.mesh,obj.cellDG.xi,obj.cellDG.N,obj.caseLEE.Nq,@(n)(Fn_int),@(n)(Fn_ext));
            obj.K = obj.K - F0;
            clear F0
                % -- Contribution of source (left-end of the domain)
            [F0,obj.F_source] = LEE1D_LoopFaces_SourceLeft_FVS(obj.mesh,obj.cellDG,obj.caseLEE.Nq,obj.fFVS_out,obj.fFVS_inw,@(k)(obj.idx_q_el(k)),@(k,l)(obj.idx_q_pt(k,l)));
            obj.K = obj.K - F0;
            clear F0
                % -- Contribution of impedance boundary
                % (There may be additional variables at this point)
            [M,K,obj.F_delay,obj.I_delay] = LEE1D_LoopFaces_ImpRight(obj.mesh,obj.cellDG,obj.caseLEE.Nq,obj.f0,obj.f1,obj.fd1,obj.caseLEE.Diff1,obj.fd2,obj.caseLEE.Diff2,@(k)(obj.idx_q_el(k)),@(k,l)(obj.idx_q_pt(k,l)));
            N = size(obj.M);
                    % Don't forget what was already in M and K
            M(1:N(1),1:N(2)) = M(1:N(1),1:N(2)) + obj.M;
            K(1:N(1),1:N(2)) = K(1:N(1),1:N(2)) + obj.K;
            obj.M = M;
            obj.K = K;
                    % Check size of F_source if additional variables
            if size(obj.F_source,1)<size(obj.M,1)
                Nxi = length(obj.caseLEE.Diff1.xi);
                obj.F_source(end + (1:Nxi),1:end)=0;
            end
        end        
        % -- Time-domain solving
        function [t,q1,q2,phi] = solveTimeDomain(obj,tf,CFL,q0,q_source)
        %solveTimeDomain Compute the time-domain solution q(x,t) for
        % t in [0, tf].
        % Inputs:
        %   tf (1)      final time (s)
        %   CFL (1)     CFL number for time-step (rad)
        %   q_source (1) -> (Nq) input Source q_source(t)
        %   q0 (P)->(NqxP) initial condition q0(x)        
        % Outputs:
        %   t (Nt)         time (s)
        %   q1 (N*Nx,Nt)   first variable
        %   q2 (N*Nx,Nt)   second variable
        %   phi (Nxi,Nt)   diffusive variable
        
                % Validate arguments
            validateattributes(tf,{'numeric'},{'scalar','positive'});
            validateattributes(CFL,{'numeric'},{'scalar','positive'});
            validateattributes(q_source,{'function_handle'},{});
            validateattributes(q_source(1),{'numeric'},{'size',[obj.caseLEE.Nq,1]});
            validateattributes(q0,{'function_handle'},{});
            validateattributes(q0(linspace(0,1,10)),{'numeric'},{'size',[obj.caseLEE.Nq,10]});
            
            Nxi = size(obj.M,1) - obj.cellDG.N*obj.mesh.N*obj.caseLEE.Nq;
            xg = obj.buildVectorGlobalCoordinates();
                % Compute time step
            max_speed = max(abs(eig(obj.caseLEE.Ax)));
            dt = (CFL*obj.mesh.cell_size)/(max_speed); % time step
                % Matrices for time integration
                % dx/dt(t) = A*x(t) + B*q_source(t) + C*x_d(t-tau)
            A = -obj.M\obj.K;
            B = obj.M\obj.F_source;
            tau = obj.caseLEE.Diff2.dt; % delay on D2 (if any)
                % Time integration
            if tau~=0 && max(abs(obj.fd2(1)))~=0
                    % Needed by CERK_DDEConstantDelay...
                F_delay_full = sparse(1,1,0,size(obj.M,1),size(obj.M,2));
                F_delay_full(:,obj.I_delay) = obj.F_delay;
                C = obj.M\F_delay_full;
                fprintf('[solveTimeDomain] Non-null operator %1.2g*D2 is delayed by dt=%1.2g s\n',obj.caseLEE.ImpCoeff(4),tau);
                fprintf('[solveTimeDomain] Time integration with a Continuous Runge-Kutta.\n');
                fprintf('[solveTimeDomain] Solving M*x''(t) + K*x(t) = F_delay*x_d(t-dt) + F_source*q_source(t).\n');
                    % 5-stage 4th-order
                [A_ERK,b_ERK] = RK_get54Coeffs_2NStorage();
                [A_ERK,b_ERK] = RK_convertCoeffs(A_ERK,b_ERK);
                c_ERK = sum(A_ERK,2);
                    % Natural continuous extension of degree 3 (Bellen 2003) 
                Bpol= [4*(3*c_ERK(:)-2).*b_ERK(:),3*(3-4*c_ERK(:)).*b_ERK(:),0*b_ERK(:),0*b_ERK(:)];
                Bpol(1,:) = [2*(1-4*b_ERK(1)),3*(3*b_ERK(1)-1),1,0];
                [t, x] = CERK_DDEConstantDelay(@(t,x,z)(A*x+B*q_source(t)+C*z),tau,[reshape(q0(xg),[],1);zeros(Nxi,1)],tf,dt,A_ERK,b_ERK,Bpol,4,obj.I_delay);
            else
                fprintf('[solveTimeDomain] ad2*D2 is either null or not delayed.\n');
                fprintf('[solveTimeDomain] ad2*D2 is ignored.\n')
                fprintf('[solveTimeDomain] Time integration with a Discrete Runge-Kutta.\n');
                fprintf('[solveTimeDomain] Solving M*x''(t) + K*x(t) = F_source*q_source(t).\n');
                [x,t] = LSERK4_op(@(x,t)(A*x+B*q_source(t)),[reshape(q0(xg),[],1);zeros(Nxi,1)],tf,dt);
            end
                % Extract variables
            Nq = obj.caseLEE.Nq; N = obj.cellDG.N; Nx = obj.mesh.N;
            q1 = x(1 + Nq*(0:(N*Nx-1)),:);
            q2 = x(2 + Nq*(0:(N*Nx-1)),:);
                if Nxi>0
                    phi = x((end-Nxi):end,:);
                else
                    phi = [];
                end
        end
        % -- Frequency-domain solving
        function [q1,q2,phi] = solveHarmonicDomain(obj,omega,Ai)
        %solveHarmonicDomain Compute the harmonic solution q(x,omega).
        % Inputs:
        %   omega (1)   angular frequency of input source (rad/s)
        %   Ai (1)      amplitude of input source (Pa)
        % Outputs:
        %   q1 (N*Nx)   first variable
        %   q2 (N*Nx)   second variable
        %   phi (Nxi)   diffusive variable
            
                % Validate arguments
            validateattributes(omega,{'numeric'},{'scalar','nonnegative'});
            validateattributes(Ai,{'numeric'},{'scalar','nonnegative'});
                % Compute q_harm
            s = 1i*omega;
            dt = obj.caseLEE.Diff2.dt;
            F_delay_full = sparse(1,1,0,size(obj.M,1),size(obj.M,2));
            F_delay_full(:,obj.I_delay) = obj.F_delay;
            z0 = obj.caseLEE.c0*obj.caseLEE.rho0;
            q_harm = (s*obj.M+obj.K-exp(-s*dt)*F_delay_full)\obj.F_source*[Ai/z0;Ai/z0];
                % Extract q_harm component
            Nq = obj.caseLEE.Nq; N = obj.cellDG.N; Nx = obj.mesh.N;
            Nxi = length(obj.caseLEE.Diff1.xi);
            q1 = q_harm(1 + Nq*(0:(N*Nx-1)));
            q2 = q_harm(2 + Nq*(0:(N*Nx-1)));
            if Nxi>0
                phi = q_harm((end-Nxi):end);
            else
                phi = [];
            end
        end

        % -- Index functions
        % k in [[1,Nk]] (cell)
        % n in [[1,N]] (node in cell)
        function index = idx_q(obj,k,n)
        %idx_q Index for element k and point n°n in the global vector q.
            index = (k-1)*obj.cellDG.N*obj.caseLEE.Nq+(n-1)*obj.caseLEE.Nq;
        end
        
        function range = idx_q_el(obj,k)
        %idx_q_el Range for the whole element k.
            range = (k-1)*obj.cellDG.N*obj.caseLEE.Nq+(1:(obj.cellDG.N*obj.caseLEE.Nq));
        end
        
        function range = idx_q_pt(obj,k,n)
        %idx_q_pt Range for the whole point n°n on element k.
            range = (k-1)*obj.cellDG.N*obj.caseLEE.Nq+(n-1)*obj.caseLEE.Nq+(1:obj.caseLEE.Nq);
        end
        
        % -- Misc
        function x = buildVectorGlobalCoordinates(obj)
        %buildVectorGlobalCoordinates Build the vector which contains the
        %global coordinates x.
        % Output:
        %   x (Nx x N) Coordinates of every DG node.
            x = zeros(obj.mesh.N*obj.cellDG.N,1);
            for k=1:obj.mesh.N % loop over each element
            i_range=(k-1)*obj.cellDG.N+(1:obj.cellDG.N);
            x(i_range)=obj.mesh.x(k)+obj.mesh.cell_size*(obj.cellDG.xi+1)/2;
            end
            clear i_range
        end
        
        function doLinearStabilityAnalysis(obj)
        %displayLinearStability Perform and display a linear stability
        %analysis of the global formulation.
        % Assess impact of the diffusive representation (No delay).
            warning('Costly operation.');
            C_rk = 4.4; % LSERK4
            max_speed = max(abs(eig(obj.caseLEE.Ax)));
            dt_max = @(egnval)(C_rk/max(abs(egnval)));
            CFL_max = @(dt_max)((max_speed/obj.mesh.cell_size)*dt_max);

                % Constants
            N = obj.cellDG.N;
            Nx = obj.mesh.N;
            Nq = obj.caseLEE.Nq;
            xi_max = max(obj.caseLEE.Diff1.xi);
            A1 = -obj.M\obj.K; % Full
            eignval1 = sort(eig(full(A1))); % (Hz)
            dt_max1 = dt_max(eignval1);
            CFL_max1 = CFL_max(dt_max1);
            clf
            hold all
            plot(eignval1,'o');
            plot([xi_max,xi_max]/(2*pi),[min(imag(eignval1)), max(imag(eignval1))],'r-');
            xlabel('Re(lamdda) (Hz)'); ylabel('Im(lambda) (Hz)');
            s = sprintf('DG-%d Nx=%d\nA-spectrum. Min: %1.2e Hz, Max: %1.2e Hz\nLinear stability limit (LSERK4): dt_{max}=%1.2e s, CFL_{max}=%1.2e\nDiffusive: xi_{max}=%1.2e Hz',N,Nx,-max(real(eignval1)),-min(real(eignval1)),dt_max1,CFL_max1,xi_max/(2*pi));
            title(s);
            fprintf('%s\n',s);
        end

        function printSummary(obj)
        %PrintSummary Display an object summary on the standard input.
            N = obj.cellDG.N;
            Nx = obj.mesh.N;
            c0 = obj.caseLEE.c0;
            fprintf('--\nSummary (DG-%d,Nx=%d,Nodes=%d)\n',N,Nx,N*Nx);
            fprintf('For %1.2g Point Per WaveLength:\n',obj.PPW(N));
                % Max. resolvable angular frequency (i.e. minimum no. of PPW respected)
            omega_max = (2*pi*N*c0)/(obj.PPW(N)*obj.mesh.cell_size(1));
            fprintf('\tMax. pulsation = %1.2g rad/s\n',omega_max);
            fprintf('\tMax. frequency = %1.2g Hz\n',omega_max/(2*pi));
            fprintf('Monodimensional LEE, x in [%1.2g,%1.2g]\n',obj.mesh.bound(1),obj.mesh.bound(2));
            fprintf('Source w/o reflexion at x=%1.2g m\n',obj.mesh.bound(1));
            fprintf('IBC at x=%1.2g m\n',obj.mesh.bound(2));
            fprintf('Z(s) = %1.2g + %1.2g*s + %1.2g*D1(s) + %1.2g*D2(s)\n',obj.caseLEE.ImpCoeff(1),obj.caseLEE.ImpCoeff(2),obj.caseLEE.ImpCoeff(3),obj.caseLEE.ImpCoeff(4));
            fprintf('\tD1(s) = %s\n',obj.caseLEE.Diff1.Name);
            fprintf('\tD2(s) = %s\n',obj.caseLEE.Diff2.Name);
            if obj.caseLEE.ImpCoeff(3)~=0
                fprintf('One set of %d diffusive variables phi_xi: max %1.2g Hz\n',length(obj.caseLEE.Diff1.xi),max(obj.caseLEE.Diff1.xi));
                if obj.caseLEE.ImpCoeff(4)~=0
                    fprintf('Delay applied on D2: dt=%1.2g s\n',obj.caseLEE.Diff2.dt);
                else
                    fprintf('No diffusive operator D2.\n');
                end
            else
                fprintf('No set of diffusive variables.\n');
            end            
            fprintf('--\n');
        end
    end
end