classdef Case_LEE_1D
    %Case_LEE_1D This class describes a computational case for the
    %monodimensional Linearised Euler Equations (LEE). Its use is mostly in
    %storing data.
    % [Equations] The LEEs are written under the form:
    %   dq/qt(x,t) + Ax*dq/dx(x,t) + B*q(x,t) = 0 (t>0, x in [x_min,x_max])
    % where q=[u,p/z0] is (Nq) (homogeneous to a speed).
    % [Boundary conditions] Monodimensional impedance tube
    %   (left end) Source q(t)
    %   (right end) Impedance p(s)=z(s)*u(s), with
    %       z(s) = a0 + a1*s + ad1*D1(s) + ad2*D2(s) (Re(s)>0)
    %   where D1 and D2 two diffusive operators.
    
    properties (SetAccess=private)
        CaseName; % (string) Computational case name
        VarName = {'u','p/z0'}; % (cell of Nq string) Variables names
        Nq = 2; % (1) Number of variables
        Ax; % (NqxNq) (Constant) Jacobian of the flux matrix
        B = zeros(2,2); % (NqxNq)
        bound; % (2) [x_min,x_max]
        
        % -- Physical constants
        U0; % (m/s) mean flow velocity
        c0; % (m/s) speed of sound
        rho0; % (kg/m^3) density
        
        % -- Frequency-domain solving
        % omega (rad/s)
        % Ai (Pa)
        % (P,1,1,1) -> (Nq,P) q_ex(x,t,omega,Ai)
        q_eff_harm; % Exact harmonic solution (with effective impedance)
        q_ex_harm; %  Exact harmonic solution (with exact impedance)
        
        % -- Impedance Boundary Condition
        % q2 = z * q1
        % with z(s) = a0+a1*s+ad1*D1(s)+ad2*D2(s) (Re(s)>0)
        % (PxQ)-> (PxQ) Complex-valued z(s) Laplace transform of the IBC
        z_ex; % Exact Laplace tansform
        z_eff; % (Effective) Laplace transform (i.e. with diffusive approx.)
        Diff1; %  (Diffusive_Operator) First diffusive operator
        Diff2; % (Diffusive_Operator) Second diffusive operator
        ImpCoeff; % (4) Impedance coefficients (a0,a1,ad1,ad2)
    end
    
    methods
        
        function obj = Case_LEE_1D(CaseName,bound,Const,a,Diff1, Diff2)
        %Case_LEE_1D Class constructor.
        %Input:
        %   CaseName (string) Computational case name
        %   bound (2) [x_min, x_max]
        %   Const (3) Physical constants [U0, c0, rho0]
        %   a (4) Impedance coefficients a0, a1, ad1, ad2 (>=0)
        %   Diff1 (Diffusive_Operator) First diffusive operator
        %   Diff2 (Diffusive_Operator) Second diffusive operator

                % -- Arguments check
        validateattributes(CaseName,{'char'},{});
        validateattributes(bound,{'numeric'},{'vector','numel',2});      
        validateattributes(Const,{'numeric'},{'vector','numel',3});      
        validateattributes(a,{'numeric'},{'numel',4,'nonnegative'});
        validateattributes(Diff1,{'Diffusive_Operator'},{});
        validateattributes(Diff2,{'Diffusive_Operator'},{});
            % Check whether both operators observe the same state
        if sum(Diff1.xi~=Diff2.xi)>0
            warning('Diff1 and Diff2 do not observe the same diffusive variables.');
        end
                % ---
        obj.CaseName = CaseName;
        obj.bound = bound;
        obj.U0 = Const(1);
        obj.c0 = Const(2);
        obj.rho0 = Const(3);
        obj.Ax = [obj.U0,obj.c0; obj.c0,obj.U0];
        
            % Impedance boundary condition
        obj.ImpCoeff = a;
        obj.Diff1 = Diff1;
        obj.Diff2 = Diff2;
                % Effective Laplace
        obj.z_eff = @(s)(a(1)+a(2)*s+a(3)*obj.Diff1.Ld(s)+a(4)*obj.Diff2.Ld(s));
                % Exact Laplace
        obj.z_ex = @(s)(a(1)+a(2)*s+a(3)*obj.Diff1.Ld_ex(s)+a(4)*obj.Diff2.Ld_ex(s));
            % Exact harmonic solution
        obj.q_eff_harm = LEE_1D_ExactHarmonicSolution(@(s)((obj.z_eff(s)-1)./(obj.z_eff(s)+1)),obj.bound(2)-obj.bound(1),obj.c0,obj.rho0);
        obj.q_ex_harm = LEE_1D_ExactHarmonicSolution(@(s)((obj.z_ex(s)-1)./(obj.z_ex(s)+1)),obj.bound(2)-obj.bound(1),obj.c0,obj.rho0);
        end
        
        function PlotHarmonicSolution(obj,t,omega,Ai)
        %PlotHarmonicSolution Plot the exact harmonic solution q(x,t).
        %   t (1)       instant (s)
        %   omega (1)   angular frequency of input source
        %   Ai (1)      amplitude of input source (Pa)
        
            validateattributes(t,{'numeric'},{'vector','numel',1},1);
            validateattributes(omega,{'numeric'},{'vector','numel',1},1);
            validateattributes(Ai,{'numeric'},{'vector','numel',1},1);
            x = linspace(obj.bound(1),obj.bound(2),1e4);
            y_ex = obj.q_ex_harm(x,t,omega,Ai);
            y_eff = obj.q_eff_harm(x,t,omega,Ai);
            clf
            hold all
            leg = cell(0);
            plot(x,y_ex(1,:),'b');
            leg(end+1)={sprintf('q1=%s (Exact Z(s))',obj.VarName{1})};
            plot(x,y_eff(1,:),'b--');
            leg(end+1)={sprintf('q1=%s (Diff. approx. Z_{diff}(s))',obj.VarName{1})};
            plot(x,y_ex(2,:),'r');
            leg(end+1)={sprintf('q2=%s (Exact)',obj.VarName{2})};
            plot(x,y_eff(2,:),'r--');
            leg(end+1)={sprintf('q2=%s (Diff. approx. Z_{diff}(s))',obj.VarName{2})};
            title(sprintf('[%s]: Exact Harmonic solutions',obj.CaseName));
            xlabel('x');
            legend(leg);
            fprintf('[plotHarmonicSolution] L2 distance between the two exact solutions: %1.1g\n',sqrt(mean((y_ex(1,:)-y_eff(1,:)).^2+(y_ex(2,:)-y_eff(2,:)).^2)));
        end
    end
end

