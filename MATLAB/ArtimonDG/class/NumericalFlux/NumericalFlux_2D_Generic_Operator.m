classdef NumericalFlux_2D_Generic_Operator
%NumericalFlux_2D_Generic_Operator Bi-dimensional generic numerical flux
%function, and associated observation vector.
% The corresponding numerical flux function is given by
%           f*(q-,q+,n) = f(n,x,y)*(c(n)*q-),
%where  '-' denotes the interior trace (within the element).
    
    properties (SetAccess = protected)
        f; % (function_handle) numerical flux function
        c; % (function_handle) observation vector
    end
    
    methods
        function obj = NumericalFlux_2D_Generic_Operator(f,c)
        %NumericalFlux_2D_UpwindFVS Class constructor.
        % Input:
        %   f       (function_handle) Numerical flux function
        %           f([nx,ny]) or f([nx,ny],x,y) is Nqx1
        %   c       (function_handle) Observation vector
        %           c([nx,ny]) is Nq
        
            validateattributes(f,{'function_handle'},{},mfilename,'fout');
            if nargin(f)==1
                validateattributes(f([1,1]),{'numeric'},{'column'},mfilename,'f([1,1])');
            elseif nargin(f)==3
                validateattributes(f([1,1],1,1),{'numeric'},{'column'},mfilename,'f([1,1],1,1)');
            else
                error('[%s] ''f'' must have 1 or 3 input argument(s), not %d',mfilename,nargin(f));
            end
            validateattributes(c,{'function_handle'},{},mfilename,'fout');
            if nargin(c)==1
                validateattributes(c([1,1]),{'numeric'},{'vector'},mfilename,'c([1,1])');
            else
                error('[%s] ''c'' must have 1 input argument, not %d',mfilename,nargin(c));
            end
            obj.f = f;
            obj.c = c;
        end
    end
end