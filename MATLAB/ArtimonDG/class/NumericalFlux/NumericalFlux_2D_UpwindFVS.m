classdef NumericalFlux_2D_UpwindFVS
%NumericalFlux_2D_Upwind Bi-dimensional Upwind flux, written using the Flux
%Vector Splitting formalism.
% The corresponding numerical flux function is given by
%           f*(q-,q+,n) = fout(n,x,y)*q- + finw(n,x,y)*q+,
%where  '-' denotes the interior trace (within the element)
%       '+' denotes the exterior trace (outside the element)
%       'n' denotes the outward unit normal (pointing towards '+')
    
    properties (SetAccess = protected)
        fout; % (function_handle) outward flux
        finw; % (function_handle) inward flux
    end
    
    methods
        function obj = NumericalFlux_2D_UpwindFVS(fout)
        %NumericalFlux_2D_UpwindFVS Class constructor.
        % Input:
        %   fout    (function_handle) Outward flux 
        %           f([nx,ny]) or f([nx,ny],x,y) is NqxNq
        
            validateattributes(fout,{'function_handle'},{},mfilename,'fout');
            if nargin(fout)==1
                validateattributes(fout([1,1]),{'numeric'},{'square'},mfilename,'fout([1,1])');
                finw = @(n)(-fout(-n));
            elseif nargin(fout)==3
                validateattributes(fout([1,1],1,1),{'numeric'},{'square'},mfilename,'fout([1,1],1,1)');
                finw = @(n,x,y)(-fout(-n,x,y));
            else
                error('[%s] ''fout'' must have 1 or 3 input argument(s), not %d',mfilename,nargin(fout));
            end
            obj.fout = fout;
                % deduced from the conservativity condition
            obj.finw = finw;
        end
    end
end