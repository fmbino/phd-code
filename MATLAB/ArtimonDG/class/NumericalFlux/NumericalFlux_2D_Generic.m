classdef NumericalFlux_2D_Generic
%NumericalFlux_2D_Generic Bi-dimensional generic numerical flux function.
% (This class is trivial.)
% The numerical flux function is given by
%           f*(q-,q+,n) = f(n,x,y)*q-,
%where  '-' denotes the interior trace (within the element).
    
    properties (SetAccess = protected)
        f; % (function_handle) numerical flux function
    end
    
    methods
        function obj = NumericalFlux_2D_Generic(f)
        %NumericalFlux_2D_UpwindFVS Class constructor.
        % Input:
        %   f    (function_handle) Numerical flux function
        %           f([nx,ny]) or f([nx,ny],x,y) is NqxNq
        
            validateattributes(f,{'function_handle'},{},mfilename,'fout');
            if nargin(f)==1
                validateattributes(f([1,1]),{'numeric'},{'square'},mfilename,'f([1,1])');
            elseif nargin(f)==3
                validateattributes(f([1,1],1,1),{'numeric'},{'square'},mfilename,'f([1,1],1,1)');
            else
                error('[%s] ''f'' must have 1 or 3 input argument(s), not %d',mfilename,nargin(f));
            end
            obj.f = f;
        end
    end
end