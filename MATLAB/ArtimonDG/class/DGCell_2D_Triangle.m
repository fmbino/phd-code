classdef DGCell_2D_Triangle
    %DGCell_1D 2D Discontinuous Galerkin (DG) cell.
    % The standard triangle, coordinates (r,s) is described below.
    % Standard Triangle (3-node triangle)
    %        s in [-1,1]
    %        ^
    %        |
    %        3
    %        |`\
    %        |  `\ Edge 2 (r=-s)
    % Edge 3  |    `\
    %        |      `\
    %        |        `\
    %        1----------2 --> r in [-1,1]
    %           Edge 1
    
    properties

        N; % (1) maximum polynomial order    
        Np; % (1) number of nodes
        Nfp; % (1) number of nodes per edge
        xi; % (Npx2) nodes [r,s]
        faceIdx; % (Nfpx3) node index of each face
        
        % -- Quadrature matrices 
        M; % (NpxNp) [<li,lj>]_i,j
        Sr; % (NpxNp) [<li,dlj/dr>]_i,j
        Ss; % (NpxNp) [<li,dlj/ds>]_i,j
        Eps; % (NfpxNfpx3) [<li,lj>]_i,j (over the n-th edge of T)
             % (in order given by faceIdx)
        
        % -- Plot-related
        xiplot; % (Npx2) equidistant nodes [r,s]
        trianglesplot; % (?x3) triangles associated with equidistant nodes
        interpXiToXiplot; % (NpxNp) interpolation matrix xi -> xiplot
        
        % -- Metric
        dr; % (1) smallest L2-distance between two nodes
        
    end
    
    methods
        function cell = DGCell_2D_Triangle(N)
        %DGCell_2D_Triangle Class constructor.
        %Input:
        %   N (1) maximum polynomial order
        
                % -- Arguments check
            validateattributes(N,{'numeric'},{'scalar','integer','positive'});
                % ---    
            cell.N = N;
            cell.Np = (N+1)*(N+2)/2;
            cell.Nfp = N+1;
            [r,s] = Nodes2D(N); 
            [r,s] = xytors(r,s);
            cell.xi = [r(:),s(:)];
                % -- Find face indexes
            I1 = find((s+1)<4*eps); % face 1
            I2 = find(abs(r+s)<4*eps); % face 2
            I3 = find((r+1)<4*eps); % face 3
            if length(I1)==(cell.Nfp) && length(I2)==(cell.Nfp) && length(I3)==(cell.Nfp) 
                cell.faceIdx = [I1(:),I2(:),I3(:)];
            else
                error('A face has less/more than Nfp=%d nodes (caused by limited precision of the nodes)',cell.Nfp);
            end
                % -- Build quadrature matrices
            V = Vandermonde2D(N,cell.xi(:,1),cell.xi(:,2)); % [psi_j(xi_i)]_i,j
            [Dr,Ds] = Dmatrices2D(N,cell.xi(:,1),cell.xi(:,2),V); % [dl_i/dr(xi_i)]_i,j
            cell.M = inv(V*transpose(V));
            cell.Sr = cell.M*Dr; cell.Ss = cell.M*Ds;
            
            cell.Eps = zeros(cell.Nfp,cell.Nfp,3);
                    % fist face;
            faceXi = cell.xi(cell.faceIdx(:,1),1); % xi = r
            V1D = Vandermonde1D(cell.N,faceXi); M1D = inv(V1D*V1D');
            cell.Eps(:,:,1)=M1D;
                    % second face;
            faceXi = cell.xi(cell.faceIdx(:,2),1); % xi = r
            V1D = Vandermonde1D(cell.N,faceXi); M1D = inv(V1D*V1D');
            cell.Eps(:,:,2)=M1D;
                    % third face;
            faceXi = cell.xi(cell.faceIdx(:,3),2); % xi = s
            V1D = Vandermonde1D(cell.N,faceXi); M1D = inv(V1D*V1D');
            cell.Eps(:,:,3)=M1D;
            
                % -- Equidistant nodes
                % build equally spaced grid on standard triangle
            counter=[];
            rplot = zeros(cell.Np,1); splot = zeros(cell.Np,1); 
            sk = 1;
            for n=1:cell.N+1
              for m=1:cell.N+2-n
                rplot(sk) = -1 + 2*(m-1)/cell.N;
                splot(sk) = -1 + 2*(n-1)/cell.N;
                counter(n,m) = sk; sk = sk+1;
              end
            end
            cell.xiplot = [rplot(:),splot(:)];
                % build matrix to interpolate field data to equally spaced nodes
            Vplot = Vandermonde2D(cell.N, rplot, splot);
            cell.interpXiToXiplot = Vplot*inv(V);
                % build triangulation of equally spaced nodes on reference triangle
            cell.trianglesplot = [];
            for n=1:cell.N+1
              for m=1:cell.N+1-n,
                v1 = counter(n,m);   v2 = counter(n,m+1); 
                v3 = counter(n+1,m); v4 = counter(n+1,m+1);
                if(v4) 
                  cell.trianglesplot = [cell.trianglesplot;[[v1 v2 v3];[v2 v4 v3]]]; 
                else
                  cell.trianglesplot = [cell.trianglesplot;[[v1 v2 v3]]]; 
                end
              end
            end
            
                % -- Smallest distance
            r = cell.xi(:,1); s = cell.xi(:,2);
            [i1,i2] = meshgrid(1:length(r),1:length(r));
            dmat_sq = sqrt((r(i1) - r(i2)).^2 + (s(i1) - s(i2)).^2);
            dmat_sq(~dmat_sq) = Inf;
            cell.dr = min(dmat_sq(:));
        end
        
        function show(obj)
        %Display Display the cell in a new plot window.
            clf
            hold all
            scatter(obj.xi(:,1),obj.xi(:,2),'o');
            plot([-1,1],[-1,-1],'-');
            plot([-1,-1],[-1,1],'-');
            plot([1,-1],[-1,1],'-');
            xlabel('r');
            ylabel('s');
            title(sprintf('DG cell: Np=%d nodes (order N=%d, distance dr=%1.2g)',obj.Np,obj.N+1,obj.dr));
            b = num2str(1:length(obj.xi));
            c = cell(0);
            for i=1:length(obj.xi)
                c{i}={sprintf('%d',i)};
            end
            text(obj.xi(:,1)+0.01,obj.xi(:,2)+0.04,c,'FontSize',14);
        end
        
        function printSummary(obj)
        %PrintSummary Display an object summary on the standard input.
                % Display
            fprintf('---- 2D DG cell summary:\n');
            fprintf('Polynomial order: [%d] (DG order: %d)\n',obj.N,obj.N+1);
            fprintf('Number of nodes per cell: [%d] \n',obj.Np);
            fprintf('Minimal distance between nodes: %1.2g\n',obj.dr);
            fprintf('Position of the nodes:\n');
            for i=1:length(obj.xi)
                fprintf('\t%1.4g, %1.4g\n', obj.xi(i,1),obj.xi(i,2));               
            end            
            fprintf('------------------\n');
        end
    end    
end

