classdef ImpedanceBoundaryCondition_ZY
    %ImpedanceBoundaryCondition_ZY Describes an impedance boundary
    %condition using the Laplace transforms of both impedance z(s) and 
    %admittance y(s) (Re(s)>0):
    %       z(s) = a0                               (Proportional)
    %               + a1*s                          (Derivative)
    %               + ad*exp(-tau_d*s)              (Direct delay)
    %               + aq*Q(s)                       (LTI Operator)
    %               + aqd*Q(s)*exp(-tau_q*s)        (Delayed LTI Operator),
    % and
    %       y(s) = b0                               (Proportional)
    %               + b1*s                          (Derivative)
    %               + bd*exp(-tau_d*s)              (Direct delay)
    %               + bq*Q(s)                       (LTI Operator)
    %               + bqd*Q(s)*exp(-tau_q*s)        (Delayed LTI Operator).

    properties (SetAccess = protected)
        Name; % (char) Identifier (for convenience)
        Prop; % (function_handle) [a0(x,y),b0(x,y)]
        Deriv; % (function_handle) [a1(x,y),b1(x,y)]
        Laplace_ex_Z; % (function_handle) Exact Laplace transform z(s)
        Laplace_ex_Y; % (function_handle) Exact Laplace transform y(s)
            % -- Direct delay
        DirectDelay; % Empty or (cell of struct) 'Coeff',  'Delay'
        idx_Z_DD; % Empty or (vector) indices of impedance operator
        idx_Y_DD; % Empty or (vector) indices of admittance operator
            % -- Boundary operator
        BndOp; % Empty or (cell of struct) 'Coeff', 'Operator'
        idx_Z_BndOp; % Empty or (vector)
        idx_Y_BndOp; % Empty (vector)
            % -- Delayed boundary operator
        BndOpDelay; % Empty or (cell of struct) 'Coeff', 'Operator', 'Delay'
        idx_Z_BndOpDelay; % Empty or (vector)
        idx_Y_BndOpDelay; % Empty or (vector)
    end
    
    methods
        function obj = ImpedanceBoundaryCondition_ZY(impedance,admittance)
        %ImpedanceBoundaryCondition_ZY Class constructor
        % Inputs:
        %   Impedance (ImpedanceBoundaryCondition) z(s)
        %   Admittance (ImpedanceBoundaryCondition) y(s)
        
                % -- Validate arguments
            validateattributes(impedance,{'ImpedanceBoundaryCondition'},{},mfilename,'impedance');
            validateattributes(admittance,{'ImpedanceBoundaryCondition'},{},mfilename,'admittance');
                % -- Assign properties
            obj.Name = sprintf('%s & %s',impedance.Name,admittance.Name);
            obj.Laplace_ex_Z=impedance.Laplace_ex;
            obj.Laplace_ex_Y=impedance.Laplace_ex;
                    % Proportional
            a0 = @(x,y)(1+impedance.beta0(x,y))./(1-impedance.beta0(x,y));
            b0 = @(x,y)(1+admittance.beta0(x,y))./(1-admittance.beta0(x,y));
            obj.Prop = @(x,y)[a0(x,y),b0(x,y)];
                    % Derivative
            obj.Deriv = @(x,y)[impedance.a1(x,y),admittance.a1(x,y)];
                    % Direct delay
            obj.DirectDelay = cell(0);
            obj.DirectDelay = [impedance.DirectDelay,admittance.DirectDelay];
            obj.idx_Z_DD = 1:length(impedance.DirectDelay);
            obj.idx_Y_DD = length(impedance.DirectDelay) + (1:length(admittance.DirectDelay));       
%             if ~isempty(impedance.DirectDelay) && isempty(admittance.DirectDelay)
%                 obj.DirectDelay.Coeff = impedance.DirectDelay.Coeff;
%                 obj.DirectDelay.Delay = impedance.DirectDelay.Delay;
%                 obj.idx_Z_DD = 1:length(impedance.DirectDelay.Coeff);
%                 obj.idx_Y_DD = [];
%             elseif isempty(impedance.DirectDelay) && ~isempty(admittance.DirectDelay)
%                 obj.DirectDelay.Coeff = admittance.DirectDelay.Coeff;
%                 obj.DirectDelay.Delay = admittance.DirectDelay.Delay;
%                 obj.idx_Z_DD = [];
%                 obj.idx_Y_DD = 1:length(admittance.DirectDelay.Coeff);
%             elseif ~isempty(impedance.DirectDelay) && ~isempty(admittance.DirectDelay)
%                 obj.DirectDelay.Coeff = [impedance.DirectDelay.Coeff,admittance.DirectDelay.Coeff];
%                 obj.DirectDelay.Delay = [impedance.DirectDelay.Delay,admittance.DirectDelay.Delay];
%                 obj.idx_Z_DD = 1:length(impedance.DirectDelay.Coeff);
%                 obj.idx_Y_DD = obj.idx_Z_DD(end) + (1:length(admittance.DirectDelay.Coeff));
%             end
                    % Boundary operator
            obj.BndOp = cell(0);
            obj.BndOp = [impedance.BndOp,admittance.BndOp];
            obj.idx_Z_BndOp = 1:length(impedance.BndOp);
            obj.idx_Y_BndOp = length(impedance.BndOp) + (1:length(admittance.BndOp));
%             if ~isempty(impedance.BndOp) && isempty(admittance.BndOp)
%                 obj.BndOp.Coeff = impedance.BndOp.Coeff;
%                 obj.BndOp.Operator = impedance.BndOp.Operator;
%                 obj.idx_Z_BndOp = 1:length(impedance.BndOp.Coeff);
%                 obj.idx_Y_BndOp = [];
%             elseif isempty(impedance.BndOp) && ~isempty(admittance.BndOp)
%                 obj.BndOp.Coeff = admittance.BndOp.Coeff;
%                 obj.BndOp.Operator = admittance.BndOp.Operator;
%                 obj.idx_Z_BndOp = [];
%                 obj.idx_Y_BndOp = 1:length(admittance.BndOp.Coeff);
%             elseif ~isempty(impedance.BndOp) && ~isempty(admittance.BndOp)
%                 obj.BndOp.Coeff = [impedance.BndOp.Coeff,admittance.BndOp.Coeff];
%                 obj.BndOp.Operator = [impedance.BndOp.Operator,admittance.BndOp.Operator];
%                 obj.idx_Z_BndOp = 1:length(impedance.BndOp.Coeff);
%                 obj.idx_Y_BndOp = obj.idx_Z_BndOp(end) + (1:length(admittance.BndOp.Coeff));
%             end

                    % Delayed boundary operator
            obj.BndOpDelay = cell(0);
            obj.BndOpDelay = [impedance.BndOpDelay,admittance.BndOpDelay];
            obj.idx_Z_BndOpDelay = 1:length(impedance.BndOpDelay);
            obj.idx_Y_BndOpDelay = length(impedance.BndOpDelay) + (1:length(admittance.BndOpDelay));
%             if ~isempty(impedance.BndOpDelay) && isempty(admittance.BndOpDelay)
%                 obj.BndOpDelay.Coeff = impedance.BndOpDelay.Coeff;
%                 obj.BndOpDelay.Operator = impedance.BndOpDelay.Operator;
%                 obj.BndOpDelay.Delay = impedance.BndOpDelay.Delay;
%                 obj.idx_Z_BndOpDelay = 1:length(impedance.BndOpDelay.Coeff);
%                 obj.idx_Y_BndOpDelay = [];
%             elseif isempty(impedance.BndOpDelay) && ~isempty(admittance.BndOpDelay)
%                 obj.BndOpDelay.Coeff = admittance.BndOpDelay.Coeff;
%                 obj.BndOpDelay.Operator = admittance.BndOpDelay.Operator;
%                 obj.BndOpDelay.Delay = admittance.BndOpDelay.Delay;
%                 obj.idx_Z_BndOpDelay = [];
%                 obj.idx_Y_BndOpDelay = 1:length(admittance.BndOpDelay.Coeff);
%             elseif ~isempty(impedance.BndOpDelay) && ~isempty(admittance.BndOpDelay)
%                 obj.BndOpDelay.Coeff = [impedance.BndOpDelay.Coeff,admittance.BndOpDelay.Coeff];
%                 obj.BndOpDelay.Operator = [impedance.BndOpDelay.Operator,admittance.BndOpDelay.Operator];
%                 obj.BndOpDelay.Delay = [impedance.BndOpDelay.Delay,admittance.BndOpDelay.Delay];
%                 obj.idx_Z_BndOpDelay = 1:length(impedance.BndOpDelay.Coeff);
%                 obj.idx_Y_BndOpDelay = obj.idx_Z_BndOpDelay(end) + (1:length(admittance.BndOpDelay.Coeff));
%             end
        end
        
        function printImpedanceSummary(obj)
        %printImpedance
            fprintf('%s\n',obj.getImpedanceSummary());
        end
        
        function str = getImpedanceSummary(obj)
        %getImpedanceSummary
            str = sprintf('[%s] Summary of Impedance&Admittance object <%s>:\n',mfilename,obj.Name);
            str = [str,sprintf('\t Prop=%s | Deriv=%s\n',func2str(obj.Prop),func2str(obj.Deriv))];
            str = [str,sprintf('\t Direct delay: %d (%d z and %d y)\n',length(obj.DirectDelay),numel(obj.idx_Z_DD),numel(obj.idx_Y_DD))];
            str = [str,sprintf('\t Boundary operator: %d (%d z and %d y)\n',length(obj.BndOp),numel(obj.idx_Z_BndOp),numel(obj.idx_Y_BndOp))];
            str = [str,sprintf('\t Delayed boundary operator: %d (%d z and %d y)\n',length(obj.BndOpDelay),numel(obj.idx_Z_BndOpDelay),numel(obj.idx_Y_BndOpDelay))];
        end
    end    
end

