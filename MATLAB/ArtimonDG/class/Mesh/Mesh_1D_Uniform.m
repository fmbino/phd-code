classdef Mesh_1D_Uniform
    %Mesh_1D_RectUniform Uniform 1D mesh.
    % This class implements a uniform 1D mesh. The mesh is
    %stored in a structured manner: every cell is identified by one
    %integer (k). (ref. properties description.)
    % In addition to coordinates, an additional set of arrays is computed,
    % to enable an easy way to loop over the cell faces.
    % Global coordinates (x).
    %
    % Usage:
    %   (ref. constructor description.)
    %
    % Mesh storage convention:
    % Each node is identified by one integer, (k).
    % The cell n°(k) is the one whose left node is (k).
    % (1): far left node.
    % Ex.:
    %   right of (k): (k+1) 
    %   left of (k): (k-1) 
    
    properties

        N; % (1x1) [Nx] number of CELLS in x-direction
        cell_size; % (1x1) [dx]
        bound; % (1x2) mesh boundaries [xmin,xmax;]
        length; % (1x1) [Lx] domain length in the x and y-direction
        
        x; % ((Nx+1)x1) NODES x-coordinates, from left to right 
        
        %-- Arrays which describe the "faces" (2D: edge, 1D: point).
        % Each face is identified by one integer.
        % (Intended use: Riemann solver)
            
            % 1) Faces within the domain interior.
            % Array format (.x2): 2-row large. 
            % The i-th row of one of these arrays gives the (k)
            % coordinates of the left/right cells of the 
            %face n°i.
            %     F(i,:)=[k_left, k_right] (_int)
        Face_int; % faces, domain interior
        
            % 2) Domain boundary (left,right)
            % Format: 1-row wide.
            %   F(i,:) = [k]
        Face_left_bound; % [k_right]
        Face_right_bound; % [k_left]
    end
    
    methods
        
        function mesh = Mesh_1D_Uniform(bound, N)
        %Mesh_1D_Uniform Class constructor.
        %Input:
        %   bound (1x2) mesh boundaries [xmin,xmax;]
        %   N (1x1) [Nx] number of CELLS in x-direction
        
                % -- Arguments check
            validateattributes(bound,{'numeric'},{'2d','size',[1,2]});
            validateattributes(N,{'numeric'},{'2d','size',[1,1]});
            validateattributes(N(1),{'numeric'},{'integer','positive','nonzero'});
                % ---
            
                % Domain boundaries
            xmin = min(bound(1,:));
            xmax = max(bound(1,:));
                
                % Mesh description parameters
            mesh.N = N;
            mesh.bound = [xmin,xmax;];
            mesh.length = bound(:,2) - bound(:,1);
            mesh.cell_size = mesh.length./N;
                
                % Mesh nodes coordinates
            mesh.x = zeros(1,mesh.N(1)+1); 
            mesh.x = linspace(xmin,xmax,mesh.N(1)+1);
                
                % Additional "faces arrays"
            mesh = mesh.BuildFacesArrays();
        end
        
        
        
        function Display(obj)
        %Display Display the mesh in a new plot window.
            y=[0,1];
            [X,Y]=meshgrid(obj.x,y);
            mesh(X',Y',zeros(obj.N(1)+1,length(y)));
            view(0,90);
            set(gca, 'DataAspectRatio', [1 1 1])
            title(sprintf('1D Mesh Nx=%d',obj.N(1)));
            xlabel('x');
            ylabel('y');
            text(obj.x(1),y(1),0,sprintf('(k=%d)',1),'FontSize',20);
            text(obj.x(end),y(1),0,sprintf('(k=%d)',obj.N(1)+1),'FontSize',20);
        end
        
        function PrintSummary(obj)
        %PrintSummary Display an object summary on the standard input.
            
                % Temp variables
            f_int=length(obj.Face_int);
            f_lb = length(obj.Face_left_bound);
            f_rb = length(obj.Face_right_bound);
            
                % Display
            fprintf('---- 1D Uniform mesh summary:\n');
            fprintf('Computational domain: [%d,%d] ([x])\n',obj.bound(1),obj.bound(2));
            fprintf('Number of nodes: (%d)=%d\n',length(obj.x));
            fprintf('Number of cells: Nx=%d\n',obj.N(1));
            fprintf('Cell size: dx=%d\n',obj.cell_size(1));
            fprintf('Mesh faces - Domain interior:\n');
            fprintf('\tVertical faces: %d\n',f_int);
            fprintf('Mesh faces - Domain boundaries:\n');
            fprintf('\tLeft boundary: %d\n',f_lb);
            fprintf('\tRight boundary: %d\n',f_rb);
            fprintf('\t(Total:%d)\n',f_lb+f_rb);
            fprintf('Total number of faces: %d\n',f_lb+f_rb+f_int);
            fprintf('------------------\n');
        end

        function obj=BuildFacesArrays(obj)
        %BuildFacesArrays Build faces arrays (ref. fields description)
        % Input: x and N.
        % Output: Face_*.
            obj.Face_int=zeros(obj.N+1,2);
            obj.Face_int=[(1:(obj.N-1))',(2:(obj.N))'];
            obj.Face_left_bound=[1];
            obj.Face_right_bound=[obj.N];
        end
    end
    
end

