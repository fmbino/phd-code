classdef Mesh_2D_Triangle
    %Mesh_2D_Triangle Triangular conforming 2D mesh.
    % This class implements a triangular conforming mesh. The mesh is
    %stored in an unstructured manner: every triangle is identified by one
    %integer (k). (ref. properties description.)
    % Usage: (ref. constructor description.)
    % Remark:
    % Triangles must be 3-node (no higher order triangles), straight-sided.
    % Zones are boundary must be marked.
    % Be sure, in gmesh, to mark the boundaries and the various zones.
    %
    % Triangle face ordering (3-node triangle)
    %        s
    %        ^
    %        |
    %        3
    %        |`\
    %        |  `\ Edge 2
    % Edge 3  |    `\
    %        |      `\
    %        |        `\
    %        1----------2 --> r
    %           Edge 1

    properties

        N; % (1) number of triangles
            % -- Core
        nodes;  % (?x2) nodes coordinates [x,y]
        boundaryFaces;  % (?x3) boundary faces [physicalEntity, node1, node2]
        triangles; % (Nx4) triangles [physicalEntity, node1, node2, node3]
        physicalEntitiesIdx; % (?) Index of each physical entity
        physicalEntitiesName; % (? cell) Name of each physical entity
        bound; % (2x2) [xmin,ymin;xmax,ymax] bounding box
            % -- Metric (for each triangle)
        J; % (N) jacobian (rad)
        rx; % (N) dr/dx (rad)
        ry; % (N) dr/dy (rad)
        sx; % (N) ds/dx (rad)
        sy; % (N) ds/dy (rad)
            % unitary outward normal
            % n(k,:,l)=[nx,ny] triangle k, local face l in [1,3]
        n;  % (Nx2x3)
        edL; % (Nx3) edge length
        edJ; % (Nx3) edge Jacobian (for each edge)
             % (Rmk: edL/2=edJ, so one of them is not needed in 2D.)
        charLength; % (Nk) characteristic length (radius inscribed circle)
            % -- Element connectivity
            %"The element i, local face j (1,2,3) connects to element k, 
            %local face l (1,2,3)" translates as
            %           EToE(i,j)= k and EToF(i,j) = l.
            % Convention for boundary faces: 
            %           EToE(i,j)= i and EToF(i,j) = j.
        EToE; % (Nx3)
        EToF; % (Nx3)
            % -- Boundary face connectivity
            %"The boundary face i connects to element k, local face l" 
            %translates as
            %           FToE(i)=k and FToF(i) = l
        FToE;
        FToF;
    end
    
    methods
        
        function mesh = Mesh_2D_Triangle(nodes, boundaryFaces, triangles,physicalEntitiesIdx,physicalEntitiesName)
        %Mesh_2D_Triangle Class constructor.
        %Input:
        %   nodes (?x2) nodes coordinates [x,y]
        %   boundaryFaces (?x3) boundary faces [marker, node1, node2]
        %   triangles (?x4) triangles [marker, node1, node2, node3]
        %   physicalEntitiesIdx  (?) Index of each physical entity
        %   physicalEntitiesName (? cell) Name of each physical entity
                % -- Arguments check
            validateattributes(nodes,{'double'},{'2d','nonempty','ncols',2});
            validateattributes(boundaryFaces,{'numeric'},{'2d','nonempty','ncols',3});
            validateattributes(triangles,{'numeric'},{'2d','nonempty','ncols',4});
            validateattributes(physicalEntitiesIdx,{'numeric'},{'vector','nonempty'});
            validateattributes(physicalEntitiesName,{'cell'},{'vector','numel',length(physicalEntitiesIdx)});
                % ---
            mesh.N = size(triangles,1);
            mesh.nodes = nodes;
            mesh.boundaryFaces = boundaryFaces;
            mesh.triangles = triangles;
            mesh.physicalEntitiesIdx = physicalEntitiesIdx;
            mesh.physicalEntitiesName = physicalEntitiesName;
                
                % Build connectivity (two choices, no differences)
            %[mesh.EToE,mesh.EToF] = mesh.buildElementConnectivity(mesh.triangles);
            [mesh.EToE,mesh.EToF] = tiConnect2D(triangles(:,2:end));
            %fprintf('Face connectivity: %d triangles\n',length(mesh.EToE));
            [mesh.FToE,mesh.FToF] = mesh.buildBoundaryFaceConnectivity(mesh.boundaryFaces,mesh.triangles);
                % Build metric (two choices)
            [mesh.J,mesh.rx,mesh.ry,mesh.sx,mesh.sy,mesh.n,mesh.edL,mesh.edJ] = mesh.buildMetric(mesh.triangles,mesh.nodes);
            %[mesh.rx,mesh.sx,mesh.ry,mesh.sy,mesh.J] = mesh.buildMetricAccurate(triangles(:,2:end),nodes);
            [vx,vy] = mesh.computeVertexCoordinates(triangles(:,2:end),nodes);
            mesh.charLength = mesh.buildCharacteristicLength(vx,vy);
            mesh.bound= [min(vx(:)),min(vy(:));max(vx(:)),max(vy(:))];
            fprintf('Validating...');
            mesh.validateMesh(mesh);
            fprintf('OK\n');
        end
        
        function show(obj,varargin)
        %show Display the mesh in a new plot window.
        % Input (optional):
        %   PlotBoundaryNode (boolean) default:false
        
                % Optional arguments
            p = inputParser; p.FunctionName = mfilename;
            addParameter(p,'PlotBoundaryNode',false,@(x)(validateattributes(x,{'logical'},{})));
            parse(p,varargin{:}); oA = p.Results; % structure which contains optional arguments
            PlotBoundaryNode = oA.PlotBoundaryNode(:);
            clf
            hold all
            leg = cell(0);
            for i=1:length(obj.physicalEntitiesIdx)
                idx = find(obj.boundaryFaces(:,1)==i);
                if size(idx)~=0 % boundary physical entity
                    Nodes = obj.boundaryFaces(idx,2:3);
                    Nodes = Nodes(:);
                    xBoundary = obj.nodes(Nodes,1);
                    yBoundary = obj.nodes(Nodes,2);
                    plot(xBoundary,yBoundary,'LineWidth',2);
                    leg{end+1}=sprintf('%s (%d)',obj.physicalEntitiesName{i},obj.physicalEntitiesIdx(i));
                else % zone physical entity
                    idx = find(obj.triangles(:,1)==i);
                    trisurf(obj.triangles(idx,2:end),obj.nodes(:,1),obj.nodes(:,2),-ones(1,length(obj.nodes)));
                    leg{end+1}=sprintf('%s (%d)',obj.physicalEntitiesName{i},obj.physicalEntitiesIdx(i));
                end
            end
                % display boundary nodes
            if PlotBoundaryNode
                xBoundary = [obj.nodes(obj.boundaryFaces(:,2),1);obj.nodes(obj.boundaryFaces(:,3),1)];
                yBoundary = [obj.nodes(obj.boundaryFaces(:,2),2);obj.nodes(obj.boundaryFaces(:,3),2)];
                scatter(xBoundary,yBoundary);
                leg{end+1}=sprintf('Boundary nodes');
            end
            legend(leg,'location','northoutside');
            xlabel('x');
            ylabel('y');
            view(0,90);
            set(gca, 'DataAspectRatio', [1 1 1])
            title(sprintf('No. of triangle: %d, Vertex: %d\n',obj.N,size(obj.nodes,1)));
        end
        
        function show2export(obj,filename,dpi,varargin)
        %show2export Plot mesh for export to (cropped) png file.
        % Croping of png file relies on ImageMagick's 'convert'.
        % Input:
        %   filename (str) name of png file
        % Input (optional):
        %   dpi (1) (150dpi for laser printer,300 for high-quality) 
        %   xlimarg [xmin,xmax]
        %   ylimarg [ymin,ymax]
        
                % Optional arguments
            p = inputParser; p.FunctionName = mfilename;
            addParameter(p,'xlimarg',[]);
            addParameter(p,'ylimarg',[]);
            parse(p,varargin{:}); oA = p.Results; % structure which contains optional arguments
            xlimarg = oA.xlimarg(:);
            ylimarg = oA.ylimarg(:);    
            clf
            hold all
            for i=1:length(obj.physicalEntitiesIdx)
                idx = find(obj.boundaryFaces(:,1)==i);
                if size(idx)~=0 % boundary physical entity
                else % zone physical entity
                    idx = find(obj.triangles(:,1)==i);
                    trisurf(obj.triangles(idx,2:end),obj.nodes(:,1),obj.nodes(:,2),-ones(1,length(obj.nodes)),'facecolor',[0.95 0.95 0.95]);
                end
            end
                % Adjust x and y limits
            xlim(obj.bound(:,1));
            ylim(obj.bound(:,2));
            if ~isempty(xlimarg)
                xlim(xlimarg);
            end
            if ~isempty(ylimarg)
                ylim(ylimarg);
            end
                % Remove axis, ticks and labels
            set(gca,'YTickLabel',[]);
            set(gca,'XTickLabel',[]);
            view(0,90);
            set(gca, 'DataAspectRatio', [1 1 1]);
            set(gca, 'Position', get(gca, 'OuterPosition') - ...
            get(gca, 'TightInset') * [-1 0 1 0; 0 -1 0 1; 0 0 1 0; 0 0 0 1]);
        
                % Export to pdf file, and crop
                % (Create larger file than png)
%             print(gcf, '-dpdf', filename);
%             system(sprintf('pdfcrop %s',filename));
%             system(sprintf('rm %s',filename));
            
                % Export to png file, and crop
            print(gcf, '-dpng', filename,sprintf('-r%d',dpi));
            system(sprintf('convert %s -trim %s.crop',filename,filename));
            system(sprintf('rm %s',filename));
            close(gcf)
        end
        
        function showTriangle(obj, k)
        %showTriangle Display the triangle k.
            validateattributes(k,{'numeric'},{'scalar','integer','positive','<=',obj.N});
            clf
            hold all
            set(gca,'DataAspectRatio',[1,1,1]);
            vertices = obj.triangles(k,2:end);
            x = obj.nodes(vertices,1); y = obj.nodes(vertices,2);
            
            scatter(x,y);
            plot([x(1),x(2)],[y(1),y(2)],'r');
            plot([x(2),x(3)],[y(2),y(3)],'b');
            plot([x(3),x(1)],[y(3),y(1)],'g');
            xlabel('x');
            ylabel('y');
           title(sprintf('Triangle no.%d/%d. J=%1.2g',k,obj.N,obj.J(k)));
            quiver([x(1)+x(2)]/2,[y(1)+y(2)]/2,obj.n(k,1,1),obj.n(k,2,1),'r','MaxHeadSize',0.4);
            quiver([x(2)+x(3)]/2,[y(2)+y(3)]/2,obj.n(k,1,2),obj.n(k,2,2),'b','MaxHeadSize',0.4);
            quiver([x(1)+x(3)]/2,[y(1)+y(3)]/2,obj.n(k,1,3),obj.n(k,2,3),'g','MaxHeadSize',0.4);
        end
        
        function printSummary(obj)
        %PrintSummary Display an object summary on the standard input.
            
                % Display
            fprintf('---- 2D conforming triangular mesh summary:\n');
            fprintf('Number of nodes: %d\n',length(obj.nodes));
            fprintf('Number of triangles: Nk=%d\n',obj.N);
            fprintf('Bounding box:\n\t(%1.2g,%1.2g) -- (%1.2g,%1.2g)\n\tx-length: %1.2g\n\ty-length: %1.2g\n',obj.bound(1,1),obj.bound(1,2),obj.bound(2,1),obj.bound(2,2),obj.bound(2,1)-obj.bound(1,1),obj.bound(2,2)-obj.bound(1,2));
            fprintf('Cell area: \n\tmean=%1.2g\n\tmax=%1.2g\n\tmin=%1.2g\n',2*mean(obj.J),2*max(obj.J),2*min(obj.J));
            fprintf('Characteristic length (radius inscribed triangle): \n\tmean=%1.2g\n\tmax=%1.2g\n\tmin=%1.2g\n',mean(obj.charLength),max(obj.charLength),min(obj.charLength));
            fprintf('Physical entities: %d\n',length(obj.physicalEntitiesIdx));
            for i=1:length(obj.physicalEntitiesIdx)
                fprintf('\t%d - %s\n',obj.physicalEntitiesIdx(i),obj.physicalEntitiesName{i});
            end
            fprintf('------------------\n');
        end
    end
    
    methods (Static,Access = private)
       function [EToE,EToF]=buildElementConnectivity(triangles)
        %buildElementConnectivity Build the connectivity tables for the 
        %triangles.
            EToE=[]; % E(i,j)=k
            EToF=[]; % E(i,j)=l
                % local face numbering
            vn = [[1,2];[2,3];[1,3]];
            for i=1:size(triangles,1) % for each triangle
                for j=1:3 % for each local face
                    face = triangles(i,1+vn(j,:));
                        % Which element has the same two nodes ?
                    k = meshTriangle_findTriangle(face,triangles(:,2:end));
                    if (max(k)==0) || (length(k)>2) % no match
                        error('Incorrect mesh: face (el. %d,face %d) belongs to 0 or more than two triangles.',i,j);
                    elseif length(k)==1 % boundary face 
                        EToE(i,j)=i; EToF(i,j)=j; % Hestaven convention
                    else % interior face
                        k = k(k~=i);
                            % Which local face?
                        l = meshTriangle_findLocalFace(face,triangles(k,2:end));
                        EToE(i,j)=k; EToF(i,j)=l;
                    end
                end
            end
            fprintf('Element connectivity: %d entries.\n',size(EToE,1));
       end
        
        function [FToE,FToF] = buildBoundaryFaceConnectivity(boundaryFaces,triangles)
        %buildBoundaryFaceConnectivity Build the connectivity tables for 
        %the boundary faces.
            FToE=[]; % F(i,j)=k
            FToF=[]; % F(i,j)=l
            for i=1:size(boundaryFaces,1) % for each boundary face
                face = boundaryFaces(i,2:3);
                    % which element has the same two nodes?
                k = meshTriangle_findTriangle(face,triangles(:,2:end));
                if (max(k)==0) || (length(k)>1) % no match
                    error('Incorrect mesh: boundary face (%d) belongs to 0 or more than one triangle.',i);
                else
                        % Which local face?
                    l = meshTriangle_findLocalFace(face,triangles(k,2:end));
                        % Boundary face belongs to element k, local face l
                    FToE(i)=k; FToF(i)=l;
                end
            end
            FToE=FToE(:); FToF=FToF(:);
            fprintf('Face connectivity: %d entries.\n',length(FToE));
        end
        
        function [J,rx,ry,sx,sy,n,edL,edJ] = buildMetric(triangles,nodes)
        %buildMetric Build the metric terms.
            Nk = size(triangles,1);
            J = zeros(Nk,1); % Jacobian (rad)
            rx = zeros(Nk,1); % dr/dx (rad)
            ry = zeros(Nk,1); % dr/dy (rad)
            sx = zeros(Nk,1); % ds/dx (rad)
            sy = zeros(Nk,1); % ds/dy (rad)
                % unitary outward normal
                % n(k,:,l)=[nx,ny] element k, local face l in [1,3]
            n = zeros(Nk,2,3); 
            edL = zeros(Nk,3); % edge length
            edJ = zeros(Nk,3); % edge jacobian
            for k=1:Nk % for each triangle
                vertices = triangles(k,2:end);
                x = nodes(vertices,1); y = nodes(vertices,2);
                J(k) = (1/4)*((x(2)-x(1))*(y(3)-y(1))-(x(3)-x(1))*(y(2)-y(1)));
                rx(k) = (y(3)-y(1))/(2*J(k));
                ry(k) = -(x(3)-x(1))/(2*J(k));
                sx(k) = -(y(2)-y(1))/(2*J(k));
                sy(k) = (x(2)-x(1))/(2*J(k));
                n(k,:,1) = -[sx(k),sy(k)];
                edJ(k,1) = J(k)*norm(n(k,:,1),2);
                n(k,:,1) = n(k,:,1)/norm(n(k,:,1),2);
                n(k,:,2) = [rx(k)+sx(k),ry(k)+sy(k)];
                edJ(k,2) = J(k)*norm(n(k,:,2),2);
                n(k,:,2) = n(k,:,2)/norm(n(k,:,2),2);
                n(k,:,3) = -[rx(k),ry(k)];
                edJ(k,3) = J(k)*norm(n(k,:,3),2);
                n(k,:,3) = n(k,:,3)/norm(n(k,:,3),2);
                edL(k,1) = norm( [x(1);y(1)] - [x(2);y(2)],2);
                edL(k,2) = norm( [x(2);y(2)] - [x(3);y(3)],2);
                edL(k,3) = norm( [x(3);y(3)] - [x(1);y(1)],2);
            end
        end
        
        function [vx,vy] = computeVertexCoordinates(triangles,nodes)
        %computeVertexCoordinates compute vertex coordinates.
        % Inputs:
        %   triangles (Nkx3)
        %   nodes (?x2)
        % Outputs:
        %   vx (3xNk) x-coordinates of vertices
        %   vy (3xNk) y-coordinates of vertices
                % nodes of each vertex
            vx = zeros(3,size(triangles,1));
            vy = zeros(3,size(triangles,1));
            for k=1:size(triangles,1)
                vx(:,k) = nodes(triangles(k,:),1);
                vy(:,k) = nodes(triangles(k,:),2);
            end
        end
        
        function charLength = buildCharacteristicLength(vx,vy)
        %buildCharacteristicLength Compute a characteristic length, defined
        %as the ratio A/s, where s is half the triangle perimeter and A is 
        %the triangle area.
        % Inputs:
        %   vx (3xNk) x-coordinates of vertices
        %   vy (3xNk) y-coordinates of vertices
        % Outputs:
        %   charLength (Nk) characteristic length
        
            % Compute semi-perimeter and area
        len1 = sqrt((vx(1,:)-vx(2,:)).^2+(vy(1,:)-vy(2,:)).^2);
        len2 = sqrt((vx(2,:)-vx(3,:)).^2+(vy(2,:)-vy(3,:)).^2);
        len3 = sqrt((vx(3,:)-vx(1,:)).^2+(vy(3,:)-vy(1,:)).^2);
        sper = (len1 + len2 + len3)/2.0; 
        Area = sqrt(sper.*(sper-len1).*(sper-len2).*(sper-len3));

            % Compute scale using radius of inscribed circle
        charLength = Area./sper;
        end
        
        function [rx,sx,ry,sy,J] = buildMetricAccurate(triangles,nodes)
        %buildMetricAccurate Hesthaven method to compute the metric terms.
        % Beware: it is depended upon the order chosen.
        %   triangles (Nkx3)
        %   nodes (?x2)
            % Compute any nodal set (order does not matter)
        Ni = 6;
        [xi,yi] = Nodes2D(Ni); [ri,si] = xytors(xi,yi);

            % Build the associated derivation matrices
        Vi = Vandermonde2D(Ni,ri,si);
        [Dri,Dsi] = Dmatrices2D(Ni, ri, si, Vi);

            % Build coordinates of all the nodes
        va = triangles(:,1)'; vb = triangles(:,2)'; vc = triangles(:,3)';
        xi = 0.5*(-(ri+si)*nodes(va,1)'+(1+ri)*nodes(vb,1)'+(1+si)*nodes(vc,1)');
        yi = 0.5*(-(ri+si)*nodes(va,2)'+(1+ri)*nodes(vb,2)'+(1+si)*nodes(vc,2)');
            % Compute the metric term
        xri = Dri*xi; xsi = Dsi*xi; yri = Dri*yi; ysi = Dsi*yi; 
        J = -xsi.*yri + xri.*ysi;
        rx = ysi./J; sx =-yri./J; ry =-xsi./J; sy = xri./J;
            % Keep only one value per element
            % (Metric terms are constant on each element, here !)
        J = J(1,:)';
        rx = rx(1,:)';
        ry = ry(1,:)';
        sx = sx(1,:)';
        sy = sy(1,:)';
        end
        
        function validateMesh(mesh)
        % Validate mesh.
            Nk = size(mesh.triangles,1);
            for k=1:Nk
                for l=1:3
                    kp = mesh.EToE(k,l); lp = mesh.EToF(k,l);
                    if kp ~= k % not a boundary face
                        edL = mesh.edL(k,l); edLp = mesh.edL(kp,lp);
                        if (edL-edLp)>2*eps
                            error('Element %d/%d, face %d: edge length do not match.',k,Nk,l);
                        end
                        n = mesh.n(k,:,l); np = mesh.n(kp,:,lp);
                        if norm(n+np,inf)>2*eps
                            error('Element %d/%d, face %d: normal do not match.',k,Nk,l);
                        end
                    end
                end
            end
            fprintf('Edge length vs edge jacobian: %1.1e\n',max(max(abs(mesh.edL/2-mesh.edJ))));
        end
    end
    
end

