function [q1,q2,t,Nop] = LSERK4_splitting_old(L1,L2,M1,M2,q1_0,q2_0,tf,dt1,dt2)
%LSERK4_splitting Solve the system of ODEs:
%       q1' = L1(q1,t) + M1(q2) (dim. N1, timestep dt1)
%       q2' = L2(q2,t) + M2(q1) (dim. N2, timestep dt2<dt1)
% with initial condition on q1 and q2 (t=0).
% Both equation are solved through an LSERK4. To accomodate for dt2<dt1, q1
% is hold constant while advancing q2.
%
% Inputs:
%   L1,L2,M1,M2 function handle
%               ex: M1 is (N2x1,1x1) -> (N1x1)
%   q1_0 (N1x1) initial condition on q1
%   q2_0 (N2x1) initial condition on q2
%   tf (1x1     final time
%   dt1 (1x1)   time-step for q1
%   dt2 (1x1)   time-step for q2
% Outputs:
%   q1 (N1x(Nit+1)) computed values for q1. q1(:,i) is q1(t(i)).
%   q2 (N2x(Nit+1)) computed values for q2. q2(:,i) is q2(t(i)).
%   t (1x(Nit+1)) corresponding instants : t(1) is 0, t(end) is >= tf.
%   Nop (1x1) Estimate of the number of operations performed.

    % Validate arguments
validateattributes(tf,{'numeric'},{'scalar','positive'});
validateattributes(dt1,{'numeric'},{'scalar','positive'});
validateattributes(dt2,{'numeric'},{'scalar','positive'});
validateattributes(dt1-dt2,{'numeric'},{'scalar','positive'});
validateattributes(q1_0,{'numeric'},{'column'});
validateattributes(q2_0,{'numeric'},{'column'});
    % Constants
N1 = length(q1_0);
N2 = length(q2_0);    
        % Time-steps for q1
dt1f = rem(tf,dt1); % Final time-step (for the last iteration)
Nit1 = floor(tf/dt1); % No. of iter. to perform with <dt>
    if(Nit1==0)
        dt1 = dt1f;
    else
        dt1 = [dt1*ones(1,Nit1),dt1f];
    end
Nit1 = Nit1+1; % No. of iter. to perform on q1
clear dt1f
        % Time-steps for q2
Nit2 = floor(dt1/dt2)+1;
dt2 = min(dt2,dt1);
    % Validate operators
validateattributes(L1(q1_0,0),{'numeric'},{'column','nrows',N1});
validateattributes(L2(q2_0,0),{'numeric'},{'column','nrows',N2});
validateattributes(M1(q2_0),{'numeric'},{'column','nrows',N1});
validateattributes(M2(q1_0),{'numeric'},{'column','nrows',N2});

    % Initialisation
q1 = zeros(N1,Nit1+1);
q1(:,1)=q1_0;
q2 = zeros(N2,Nit1+1);
q2(:,1)=q2_0;
t = zeros(1,Nit1+1);
t(2:(end-1)) = dt1(1:(end-1)).*(1:(Nit1-1)); t(end)=t(end-1)+dt1(end);
Nop = Nit1*N1+sum(Nit2)*N2;
fprintf('[LSERK4_splitting]: %d iter. & %d oper. to perform, with %d sub-iter (dt1=%1.2g,dt2=%1.2g)\n',Nit1,Nop,Nit2(1),dt1(1),dt2(1));
reverseStr='';
time = cputime();
    % Low Storage Explicit Runge-Kutta 4 time-integration
    % Coefficients definition (Carpenter and al., Fourth-order 2N-storage 
    %Runge-Kutta schemes, 1994)
a = [0,-567301805773/1357537059087,-2404267990393/2016746695238,-3550918686646/2091501179385,-1275806237668/842570457699];
b = [1432997174477/9575080441755, 5161836677717/13612068292357, 1720146321549/2090206949498, 3134564353537/4481467310338, 2277821191437/14882151754819];
c = [0,1432997174477/9575080441755,2526269341429/6820363962896,2006345519317/3224310063776,2802321613138/2924317926251];
    % Temporary values
k1=zeros(N1,1);
p1=zeros(N1,1);
k2=zeros(N2,1);
p2=zeros(N2,1);
for n1=1:Nit1
    msg = sprintf('[LSERK4_slpitting]: Percent done: %3.1f', floor(100*n1/Nit1));
    fprintf([reverseStr, msg]);
    reverseStr = repmat(sprintf('\b'), 1, length(msg));
    p2 = q2(:,n1);
    dt = dt2(n1);
    for n2=1:(Nit2(n1)-1)
            % -- Compute q2(t_n1+n2*dt2) from q2(t_n1+(n2-1)*dt2)
        t_st = t(n1)+(n2-1)*dt;
        for i=1:5
            k2 = a(i)*k2 + dt*L2(p2,t_st+c(i)*dt) + dt*M2(q1(:,n1));
            p2 = p2 + b(i)*k2;
        end
    end
    % -- Compute q2(t_n1+dt1) from q2(t_n1+(Nit2-1)*dt2)
    dt = dt1(n1)-(Nit2(n1)-1)*dt2(n1);
    t_st = t(n1)+(n2)*dt2(n1);
    for i=1:5
        k2 = a(i)*k2 + dt*L2(p2,t_st+c(i)*dt) + dt*M2(q1(:,n1));
        p2 = p2 + b(i)*k2;
    end
    q2(:,n1+1) = p2;
        % Compute q1(t_n1+dt1) from q1(t_n1)
    dt = dt1(n1);
    t_st = t(n1);
    p1 = q1(:,n1);
    for i=1:5
        k1 = a(i)*k1 + dt*L1(p1,t_st+c(i)*dt) + dt*M1(q2(:,n1));
        p1 = p1 + b(i)*k1;
    end
    q1(:,n1+1) = p1;
end
    fprintf(' - Finished. (%2.2g s)\n',cputime()-time);

end