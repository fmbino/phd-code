function [q,t,Nop,T] = LSERK4_op(L,q_0,tf,dt)
%LSERK4_op Solve the system of ODEs:
%       q' = L(q,t) (dim. N, timestep dt)
% with initial condition on q (t=0).
% The system is solved through a Low-Storage Explicit Runge-Kutta method,
% the order of which is 4.
% Note: A rule of thumb for the linear stability limit is:
%                       |dt * lambda_max| < 4.4.
% Inputs:
%   L           function handle (Nx1,1x1) -> (Nx1)
%               ex: L(q,t) = A*q + u(t) (linear system with input u)
%   q_0 (Nx1) initial condition on q (t=0)
%   tf (1x1     final time
%   dt (1x1)   time-step
% Outputs:
%   q1 (N1x(Nit+1)) computed values. q(:,i) is an approx. of q(t(i)).
%   t (1x(Nit+1)) corresponding instants : t(1) is 0, t(end) is tf.
%   Nop (1x1) Estimate of the number of operations performed.
%   T (1x1) Elapsed time

dt_f = rem(tf,dt); % Final time-step (for the last iteration)
N_it = floor(tf/dt); % No. of iter. to perform with <dt>
    if(N_it==0)
        dt = dt_f;
    else
        dt = [dt*ones(1,N_it),dt_f];
    end
N_it = N_it+1; % Total number of iterations to perform

    % Initialization
N = length(q_0);
q = zeros(N,N_it+1); % Output: q(n) = q(t_{n-1})
q(:,1)=q_0;
t = zeros(1,N_it+1);
t(2:(end-1)) = dt(1:(end-1)).*(1:(N_it-1)); t(end)=t(end-1)+dt_f;
    % Estimate of the number of operations
Nop = N_it*N;

    % Low Storage Explicit Runge-Kutta 4 time-integration
    % Coefficients definition (Carpenter and al., Fourth-order 2N-storage 
    %Runge-Kutta schemes, 1994)
a = [0,-567301805773/1357537059087,-2404267990393/2016746695238,-3550918686646/2091501179385,-1275806237668/842570457699];
b = [1432997174477/9575080441755, 5161836677717/13612068292357, 1720146321549/2090206949498, 3134564353537/4481467310338, 2277821191437/14882151754819];
c = [0,1432997174477/9575080441755,2526269341429/6820363962896,2006345519317/3224310063776,2802321613138/2924317926251];
fprintf('[LSERK4]: %d iter. & %d oper. to perform (dt=%1.2g).\n',N_it,Nop,dt(1));
reverseStr='';
time = cputime();
    % Temporary values
k=zeros(N,1);
p=zeros(N,1);
for n=1:N_it
   msg = sprintf('[LSERK4]: Percent done: %3.1f', floor(100*n/N_it));
   fprintf([reverseStr, msg]);
   reverseStr = repmat(sprintf('\b'), 1, length(msg));
   p=q(:,n);
    for i=1:5
        k = a(i)*k + dt(n)*L(p,t(n)+c(i)*dt(n));
        p = p + b(i)*k;
    end
    q(:,n+1)=p;
end
    T = cputime()-time;
    fprintf(' - Finished. (%2.2g s)\n',T);
end