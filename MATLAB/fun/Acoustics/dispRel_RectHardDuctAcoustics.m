function omega = dispRel_RectHardDuctAcoustics(N,M,u0,k,c0,a,b)
%dispRel_RectHardDuctAcoustics Dispersion relation for rectangular duct.
% Pridmore-Brown equation:
%       ∆p(x) + ((Omega/c0)^2-k^2) p(x) = 0  x1∈(0,a),x2∈(0,b),
% with Omega = omega - u0*k, and impedance boundary condition
%               dp/dn = 0 (hard wall).
% Rmk: dispersion relation is dimensional.
% Inputs:
%   N,M (1) mode indices
%   u0 (1) base flow (m/s)
%   k (1) axial wavenumber (1/m)
%   c0 (1) speed of sound (m/s)
%   a,b (1) cross-section dimensions (m)
%  Output:
%   omega (N) pulsations (rad/s)

    omega_ex = zeros(0);
    for n=0:N
        for m=0:M
           omega_ex(end+1,:) = [n,m,u0*k + c0*sqrt((n*pi/a)^2+(m*pi/b)^2+k^2)];
           %fprintf('(n,m)=(%d,%d) %1.2g\n',omega_ex(end,1),omega_ex(end,2),omega_ex(end,3));fprintf('(n,m)=(%d,%d) %1.2g\n',omega_ex(end,1),omega_ex(end,2),omega_ex(end,3));
        end
    end
    for n=0:N
        for m=0:M
           omega_ex(end+1,:) = [N+1+n,M+1+m,u0*k - c0*sqrt((n*pi/a)^2+(m*pi/b)^2+k^2)];
           %fprintf('(n,m)=(%d,%d) %1.2g\n',omega_ex(end,1),omega_ex(end,2),omega_ex(end,3));fprintf('(n,m)=(%d,%d) %1.2g\n',omega_ex(end,1),omega_ex(end,2),omega_ex(end,3));
        end
    end
    [omega_ex(:,3),I] = sort(omega_ex(:,3));
    omega_ex(:,1) = omega_ex(I,1);
    omega_ex(:,2) = omega_ex(I,2);
    omega = omega_ex(:,3);
        % Remove duplicates
    omega = removeDuplicate_Complex(omega,eps);
end

