function D = dispRel_CylDuctAcoustics(k,m,w,Zeff,M,varargin)
%dispRel_CylDuctAcoustics Dispersion relation for acoustics of cylindrical
%duct (radius 1):
%                   D(k,m,w,Zeff,M) = 0
% From (Khamis Brambley 2016, JFM, Eq. 2.8).
% Fourier convention (synthesis):
% p(x,r,theta,t) = / p^(k,r,m,w)*exp(1i*(w*t-k*x-m*theta))
% The derivative with respect to k can be returned; inputs must then be
% scalar.
% Rmk: The identity alpha=sqrt(...) is used, with branch cut on the
% negative real axis (Re(alpha)>=0).
% Inputs:
%  k (NXP) axial wave number
%  m (1) azimuthal number
%  w (NxP) angular frequency
%  Zeff (NxP) effective wall impedance
%  M (1)   Base flow Mach number
% (Optional)
%  dZeff_dk (1) derivative w.r.t. k
% Outputs: 2 possibilities
%  D (NxP) Dispersion relation (0 if satisfied)
%               OR
%  D (2)  [D, dD/dk]

if abs(w-M*k)<1e-15
   warning('Dangerously close to the critical layer.\n'); 
end

    % derivative w.r.t. k do not depend on this choice
alpha = sqrt((w-M*k).^2-k.^2);
if m==0
    D = -besselj(0,alpha,1)./(alpha.*besselj(1,alpha,1))+Zeff./(1i*(w-M*k));
else
    D = (besselj(m-1,alpha,1)+besselj(m+1,alpha,1))./(2*m*dbesselj(m,alpha,1))+Zeff./(1i*(w-M*k));
end
if nargin>5 % derivative w.r.t. k
    if (length(k)+length(w)+length(Zeff))>3
        error('k,w,Zeff must be scalar when the derivative w.r.t. k is asked');
    end
    if m==0
        D(2) = +1/alpha + besselj(0, alpha,1)/(alpha^2*besselj(1, alpha,1)) - ((besselj(1, alpha,1)/alpha - besselj(0, alpha,1))*besselj(0, alpha,1))/(alpha*besselj(1, alpha,1)^2);
    else
        D(2) = ((besselj(m - 1, alpha,1)*(m - 1))/alpha - (besselj(m + 1, alpha,1)*(m + 1))/alpha)/(m*(besselj(m - 1, alpha,1) - besselj(m + 1, alpha,1))) - ((besselj(m - 1, alpha,1) + besselj(m + 1, alpha,1))*((besselj(m - 1, alpha,1)*(m - 1))/alpha - 2*besselj(m, alpha,1) + (besselj(m + 1, alpha,1)*(m + 1))/alpha))/(m*(besselj(m - 1, alpha,1) - besselj(m + 1, alpha,1))^2);
    end
      D(2) = -(M*(w-M*k)+k)*D(2)/alpha;
     dZeff_dk = varargin{1};
     D(2) = D(2) + dZeff_dk/(1i*(w-M*k)) + Zeff*M/(1i*(w-M*k)^2);
end
end