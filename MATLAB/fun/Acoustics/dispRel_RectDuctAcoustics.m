function [omega,k_cs] = dispRel_RectDuctAcoustics(a,b,c0,k,u0,rho0,z,k0,varargin)
%dispRel_RectDuctAcoustics Dispersion relation for rectangular duct.
% Pridmore-Brown equation:
%       ∆p(x) + ((Omega/c0)^2-k^2) p(x) = 0  x1∈(0,a),x2∈(0,b),
% with Omega = omega - u0*k, and impedance boundary condition
%               p = -z(1i*omega)/(1i*rho0*Omega)*dp/dn.
% Rmk: dispersion relation is dimensional.
% Inputs:
%   a,b (1) cross-section dimensions (m)
%   c0 (1) speed of sound (m/s)
%   rho0 (1) density (kg/m^3)
%   u0 (1) base flow (m/s)
%   k (1) axial wavenumber (1/m)
%   z (function_handle) Laplace transform of impedance kernel z (z(s),Re(s)>0)
%   k0 (P) starting values (for numerical solver)
%  Input (optional):
%   tol (1) tolerance (default: 1e-10)
%  Output:
%   omega (N) pulsations (rad/s)
%   k_cs (Nx2)   cross-section wave numbers (kx,ky)


        % Optional argument
    p = inputParser; p.FunctionName = mfilename;
    addParameter(p,'tol',1e-10,@(x)(validateattributes(x,{'numeric'},{'scalar','positive'})));
    parse(p,varargin{:});  oA = p.Results;
    tol = oA.tol;

    ky0 = reshape(repmat(k0,[length(k0),1]),[],1);
    kz0 = repmat(k0(:),[length(k0),1]);
    k0 = [ky0,kz0];
    Omega_fun = @(x,s)(s*c0*sqrt(x(:,1).^2+x(:,2).^2+k^2)); % s=+/-1
    omega_fun = @(x,s)(u0*k + Omega_fun(x,s));
    z_fun = @(x,s)(z(1i*omega_fun(x,s)));
        % f(x)^2=0 anyway, w/o NAN
    f_opt = @(x,s)([(rho0*Omega_fun(x,s)/z_fun(x,s)+x(1))^2*exp(2*1i*x(1)*a)-(rho0*Omega_fun(x,s)/z_fun(x,s)-x(1))^2;(rho0*Omega_fun(x,s)/z_fun(x,s)+x(2))^2*exp(2*1i*x(2)*b)-(rho0*Omega_fun(x,s)/z_fun(x,s)-x(2))^2]);
    options=optimoptions(@fsolve,'Display','none','TolFun',tol,'TolX',tol);
        %  first run with +1
    k_cs = permute(fsolveLoop(@(x)f_opt(x,+1),permute(k0,[3,2,1]),options),[3,2,1]);
            % remove (0,n)
    [I,~] = find(abs(k_cs)<(tol)); k_cs(I,:) = [];
    omega =  omega_fun(k_cs,1);
        % second run with -1
    k_cs2 = permute(fsolveLoop(@(x)f_opt(x,-1),permute(k0,[3,2,1]),options),[3,2,1]);
            % remove mode of the form (0,n) (not a 2D mode)
    [I,~] = find(abs(k_cs2)<(tol)); k_cs2(I,:) = [];
    omega = [omega;omega_fun(k_cs2,-1)];
    k_cs = [k_cs;k_cs2];
        % remove NaN
    omega = omega(~isnan(omega));   
    I = ~isnan(k_cs);
    I = max(I,[],2);
    kx = k_cs(:,1); kx = kx(I);
    ky = k_cs(:,2); ky = ky(I);
    k_cs = [kx,ky];
end