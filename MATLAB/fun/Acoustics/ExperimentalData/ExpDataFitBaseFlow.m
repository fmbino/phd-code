function [M0_fit,dM0_dy_fit] = ExpDataFitBaseFlow(y,M0,Mavg)
%ExpDataFitBaseFlow Compute curve fitting of base flow measurements.
% Remark: this function requires the Curve Fitting toolbox.
% Inputs:
%   y (N) spatial position
%   M0 (N) base flow at positions y
%   Mavg (1) desired average value for fit
% Outputs:
%   M0_fit (function_handle) function M0(y)
%   dM0_dy_fit (function_handle) function dM0_dy(y)


    validateattributes(y,{'numeric'},{'vector'},mfilename,'y');
    validateattributes(M0,{'numeric'},{'vector'},mfilename,'M0');
    validateattributes(Mavg,{'numeric'},{'scalar'},mfilename,'Mavg');
    
    if isempty(which('fit'))
        % Too lazy to bother defining something meaningful
        M0_fit = @(y)zeros(size(y));
        dM0_dy_fit = @(y)zeros(size(y));
        return
    end
    f=fit(y,M0,'cubicspline');
        % Correct average Mach
    Mavg_fit = integrate(f,max(y),min(y))/(max(y)-min(y));
    M0_fit = @(y)Mavg/Mavg_fit*feval(f,y);
    dM0_dy_fit = @(y)Mavg/Mavg_fit*differentiate(f,y);
end

