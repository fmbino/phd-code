function ExpData = ExpDataLoadNASAGIT2005CT57()
%ExpDataLoadNASAGIT2005CT57 Load the experimental data from NASA GIT 2005
%provided by Jones. Data is stored in csv files extracted from the xls file
%provided by Jones 'AIAA 2005-2853 Database(Update 18Oct2005).xls'
%   Output
%       ExpData (cell array) Matrix that contains the experimental data for
%       every average Mach and source SPL.

    % This code import the experimental GIT data in the cell array GITData.
    SPL_source = {'120','130','140'}; % source SPL (dB)
    Mavg = {'000','079','172','255','335','400'}; % Mach average
    Id_method = '2DFEM'; % identification method '2DFEM' or 'Q3DFEM'
    ExpData=cell(0); % object that contains all the experimental data
    for j=1:length(SPL_source) % for each source SPL
        Zdata = csvread(sprintf('CT57_Impedance_%s_%sdB.csv',Id_method,SPL_source{j}),1,0); % 2DFEM eduction
        for i=1:length(Mavg) % for each average Mach number
            % -- Identifier (source SPL and Mach)
        ExpData{i,j}.name = sprintf('GIT-CT57-SPL%sdB-Mavg%s-%s',SPL_source{j},Mavg{i},Id_method);
        fprintf('--%s\n',ExpData{i,j}.name);
        ExpData{i,j}.SPL = str2num(SPL_source{j});
        ExpData{i,j}.Mavg = str2num(Mavg{i})/1000;
            % -- SPL on opposite wall
        ExpData{i,j}.MicroSPL = csvread(sprintf('CT57_Microphone_SPL%sdB_Mavg%3s.csv',SPL_source{j},Mavg{i}),2,0); % SPL (dB)
        ExpData{i,j}.freq = csvread('CT57_Microphone_FreqkHz.csv',1,0); % Freq (kHz)
        ExpData{i,j}.Microxpos = csvread('CT57_Microphone_xmm.csv',1,0); % longitudinal position (mm)
            % -- Identified impedance
        ExpData{i,j}.Z_id =     Zdata(:,3+3*(i-1)) + 1i*Zdata(:,3+3*(i-1)+1); % Normalized impedance Z/z0
            % -- Flow data
        ExpData{i,j}.M0_y = csvread('CT57_Flow_ymm.csv'); % y (mm)
        ExpData{i,j}.M0_z = csvread('CT57_Flow_zmm.csv'); % z (mm)
        ExpData{i,j}.M0_name = {'x203mm','x356mm','x559mm'};
        try 
            ExpData{i,j}.M0(:,:,1) = csvread(sprintf('CT57_Flow_x203mm_Mavg%s.csv',Mavg{i}),0,0); % Mach number M(y,z)
            ExpData{i,j}.M0(:,:,2) = csvread(sprintf('CT57_Flow_x356mm_Mavg%s.csv',Mavg{i}),0,0); % Mach number M(y,z)
            ExpData{i,j}.M0(:,:,3) = csvread(sprintf('CT57_Flow_x559mm_Mavg%s.csv',Mavg{i}),0,0); % Mach number M(y,z)
        catch % Flow data is not available at some Mach number - > zero value
            warning('No Flow data for Mavg%s',Mavg{i});
            ExpData{i,j}.M0(:,:,:) = zeros(size(ExpData{i,j}.M0_y,1),size(ExpData{i,j}.M0_z,2),3);
        end
            % -- Compute average Mach number
        M0z = ExpData{i,j}.M0_z(1,:); M0z = M0z(:);
        M0y = ExpData{i,j}.M0_y(:,1); M0y = M0y(:);
        S = trapz(M0z,trapz(M0y,ones(length(M0y),length(M0z)),1)); % cross section surface
        I = trapz(M0z,trapz(M0y,ExpData{i,j}.M0,1))./S;
        for k=1:size(I,3)
            fprintf('Average Mach number (%s): %1.2e\n',ExpData{i,j}.M0_name{k},I(k));
        end
        fprintf('Average Mach number: %1.2e\n',mean(I));
            % -- Compute /M0(y,z)dz
        S = trapz(M0z,ones(size(M0z))); % cross section surface
                % easy way
        %I = trapz(M0z,GITData{i,j}.M0,2)./S;
            % Alternative, to discard NaN entries (more verbose)
        I = zeros(size(ExpData{i,j}.M0,1),1,size(ExpData{i,j}.M0,3));
        for k=1:size(ExpData{i,j}.M0,1)
            for l=1:size(ExpData{i,j}.M0,3)
                idx = ~isnan(ExpData{i,j}.M0(k,:,l));
                I(k,1,l) = trapz(M0z(idx),ExpData{i,j}.M0(k,idx,l))./S;
            end
        end
        ExpData{i,1}.M0_dz = I;
        end
    end
end

