function ExpData = ExpDataLoadNASAGFIT2013MP()
%ExpDataLoadNASAGFIT2013MP Load the experimental data from NASA GFIT 2013
%provided by Jones. Data is stored in csv files extracted from the xls file
%provided by Jones (AIAA2013-2273)
%   Output
%       ExpData (cell array) Matrix that contains the experimental data for
%       every average Mach and source SPL.

    % This code import the experimental GIT data in the cell array ExpData.
    SPL_source = {'120'}; % source SPL (dB)
    Mavg = {'000','180','271'}; % Mach average
    Id_method = '2DFEM'; % identification method '2DFEM'
    ExpData=cell(0); % object that contains all the experimental data
    for j=1:length(SPL_source) % for each source SPL
        for i=1:length(Mavg) % for each average Mach number
            % -- Identifier (source SPL and Mach)
        ExpData{i,j}.name = sprintf('GFIT-MP-SPL%sdB-Mavg%s-%s',SPL_source{j},Mavg{i},Id_method);
        fprintf('--%s\n',ExpData{i,j}.name);
        ExpData{i,j}.SPL = str2num(SPL_source{j});
        ExpData{i,j}.Mavg = str2num(Mavg{i})/1000;
            % -- SPL on opposite wall (Experiment)
        ExpData{i,j}.MicroSPL = csvread(sprintf('MP-GFIT_Microphone_SPL%sdB_Mavg%3s_Exp.csv',SPL_source{j},Mavg{i}),1,4); % SPL (dB)
        ExpData{i,j}.freq = csvread('MP-GFIT_Microphone_Freq.csv',1,0); % Freq (kHz)
        ExpData{i,j}.Microxpos = csvread(sprintf('MP-GFIT_Microphone_SPL%sdB_Mavg%3s_Exp.csv',SPL_source{j},Mavg{i}),1,0); % longitudinal position (m)
        ExpData{i,j}.Microxpos = ExpData{i,j}.Microxpos(:,1);
        ExpData{i,j}.Microxpos = 1e3*ExpData{i,j}.Microxpos; % longitudinal position (mm)
            % -- SPL on opposite wall (2D FEM)
        ExpData{i,j}.MicroSPL2DFEM = csvread(sprintf('MP-GFIT_Microphone_SPL%sdB_Mavg%3s_%s.csv',SPL_source{j},Mavg{i},Id_method),1,4); % SPL (dB)
            % -- Identified impedance
        Zdata = csvread(sprintf('MP-GFIT_Impedance_%s_SPL%sdB.csv',Id_method,SPL_source{j}),1,0); % 2DFEM eduction
        ExpData{i,j}.Z_id =     Zdata(:,3+3*(i-1)) + 1i*Zdata(:,3+3*(i-1)+1); % Normalized impedance Z/z0
        end
    end
end

