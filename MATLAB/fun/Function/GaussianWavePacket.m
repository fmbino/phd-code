function y = GaussianWavePacket(t,fc,bw,bwr)
%GaussianWavePacket Gaussian-modulated sinusoidal pulse, based on the
%MATLAB function gauspuls from the Signal Processing Toolbox.
% The pulse is causal (i.e. null for t<0).
% Inputs:
%   t (N)   time instants
%   fc (1)  central frequency (Hz)
%   bw (1)  frequency bandwidth (Hz) measured at bwr
%   bwr (1)  bandwidth reference level (dB) (-n dB)

    validateattributes(t,{'numeric'},{'vector'},mfilename,'t');
    validateattributes(fc,{'numeric'},{'scalar'},mfilename,'fc');
    validateattributes(bw,{'numeric'},{'scalar'},mfilename,'bw');
    validateattributes(bwr,{'numeric'},{'scalar'},mfilename,'bwr');
    
    y = gauspuls(t-2*gauspuls('cutoff',fc,bw,[],bwr),fc,bw);
end

