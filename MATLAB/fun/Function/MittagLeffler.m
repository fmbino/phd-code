function y = MittagLeffler(z)
%MittagLeffler Mittag-Leffler monogenic function, for alpha=1/2.
% Note: Implementation relies on a complex error function (erf).
% Input:
%   z (nxp) complex values
% Output:
%   y (nxp) complex values

y = exp(z.^2).*(1+erf_(z));

end

