function f = dbesselj(nu,z,varargin)
%dbesselj Derivative of besselj(nu,z), w.r.t. z.
% From (Abramowitz and Stegun, 9.1.27)

if nargin>1 % exponential damping
    f=0.5*(besselj(nu-1,z,1)-besselj(nu+1,z,1));
else
    f=0.5*(besselj(nu-1,z)-besselj(nu+1,z));
end

end

