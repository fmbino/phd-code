function identifyDiffusiveRep(h,branchPoint,isVertPeriodic)
%identifyDiffusiveRep Check wether the analytic function h(s) admits an
%oscillatory-diffusive representation, by checking the condition given in
% the thesis (Monteghetti 2018), chapter 2.
% The branch cut is assumed to be included in (-inf,0).
% Not all conditions can be easily verified numerically, but what can be
% easily checked is the decay condition at infinity and at branch
% points. If h does not satisfy these conditions, then it cannot admit an
% OD representation.
% Inputs:
%   h (fun_handle: (PxQ)->(PxQ)) analytic function h(s)
%   branchPoint (N) branch points (other than inf)
%   isVertPeriodic (boolean) 1 if h is vertically periodic, 0 otherwise.

        % -- Validate arguments
    validateattributes(h,{'function_handle'},{},mfilename,'h');
    x = linspace(1,2,5);
    y = linspace(1,2,7);
    [Sr,Si] = meshgrid(x,y);
    S = (Sr+1i*Si);
    validateattributes(h(S),{'numeric'},{'size',size(S)},mfilename,'h(S)');
    clear x y Sr Si S
    validateattributes(branchPoint,{'numeric'},{'finite'},mfilename,'branchPoint');
    validateattributes(isVertPeriodic,{'numeric'},{'binary'},mfilename,'isVertPeriodic');

    fprintf('[%s] Checking if function\n',mfilename);
    fprintf('\t h(s)=%s\n',func2str(h));
    fprintf('admits an Oscillatory-Diffusive representation\n');
    fprintf('by checking the sufficient conditions stated\n');
    fprintf('in the chapter 2 of the thesis (Monteghetti2018).\n');
    fprintf('Every sufficient condition is mentioned below.\n')
    % (0). Analycity in right half-plane
    fprintf('(0). h(s) is analytic in {Re(s)>0}: NOT CHECKED.\n');
    fprintf('\th(s) must not have poles or cuts in {Re(s)>0}.\n');
    
    % (i). Uniform decay condition at infinity
    if isVertPeriodic % uniform decay on vertical lines
        fprintf('(i1). Uniform decay of |h(s)| on {Re(s)=+R}.\n');
        fprintf('Check that the limit is null as R increases:\n');
        fprintf('\t R\t\t sup|h(s)|\n');
        S_line = @(R)(R +1i*linspace(-R,R,1e4));
        R = [1,1e1,1e3,1e4,1e5,1e8,1e10,1e14,1e16];
        for i=1:length(R)
            fprintf('\t%8.2e\t%8.2e\n',R(i),max(abs(h(S_line(R(i))))));        
        end                
        fprintf('(i2). Uniform decay of |h(s)| on {Re(s)=-R}.\n');
        fprintf('Check that the limit is null as R increases:\n');
        fprintf('\t R\t\t sup|h(s)|\n');
        S_line = @(R)(-R +1i*linspace(-R,R,1e4));
        for i=1:length(R)
            fprintf('\t%8.2e\t%8.2e\n',R(i),max(abs(h(S_line(R(i))))));        
        end                
        
    else % uniform decay on circles
        fprintf('(i). Uniform decay of |h(s)| on {|s|=R}.\n');
        fprintf('Check that the limit is null as R increases:\n');
        fprintf('\t R\t\t sup|h(s)|\n');
        theta = linspace(-pi,pi,1e4);
        S_circle = @(R)(R*exp(1i*theta));
        R = [1,1e1,1e3,1e4,1e5,1e8,1e10,1e14,1e16];
        for i=1:length(R)
            fprintf('\t%8.2e\t%8.2e\n',R(i),max(abs(h(S_circle(R(i))))));        
        end        
    end
    
    % (ii). Residue at branch points
    fprintf('(ii). Residue at branch point gamma_i must be finite.\n');
    fprintf('Check that the circle integral on |s-gamma_i|=r\n');
    fprintf('has a limit as r decreases to zero:\n');
    theta = linspace(-pi,pi,1e4);
    for i=1:length(branchPoint)
            % Approximation of residue, circle integral on |s-gamma_i|=r
            % We check if it converges at r -> 0.
        res = @(r)integral(@(t)r*h(branchPoint(i)+r*exp(1i*t)).*exp(1i*t),0,2*pi)/(2*pi);
        r = [1,1e-1,1e-3,1e-4,1e-5,1e-8,1e-10,1e-14,1e-16];
        fprintf('Branch point (%1.1e,%1.1e):\n',real(branchPoint(i)),imag(branchPoint(i)));
        fprintf('\t r\t\t /f(s)ds on |s-gamma_i|=r\n');
        for j=1:length(r)
            fprintf('\t%8.2e\t%8.2e\n',r(j),res(r(j)));
        end        
    end
    
    % (iii). Properties of the diffusive weight
    fprintf('(iii). Properties of the diffusive weight.\n');
    fprintf('(iii-1). Cut included in (-inf,0): NOT CHECKED.\n');
    fprintf('(iii-2). Only singularities in interior of cut are simple poles: NOT CHECKED.\n');
    fprintf('(iii-3). Convergence of the diffusive integral weight /|mu(xi)|(1+xi)dxi, xi in (0,inf):\n');
    mu=@(xi)(((h(xi*exp(-1i*pi))-h(xi*exp(1i*pi)))/(2*1i*pi)));
    fprintf('\t Calling integral...\n');
    I = integral(@(xi)(abs(mu(xi))./(1+xi)),0,inf);
    fprintf('\t /_0^inf |mu|/(1+xi) dxi = %1.1e\n',I);

    % iv. Pole-related condition
    fprintf('(iv). Growth condition on poles and residues: NOT CHECKED.\n');
    fprintf('\t This criterion can be safely assumed to be verified.\n');
    fprintf('\t It is verified provided that the modulus of the\n');
    fprintf('\t residues and poles does not grow exponentially.\n');
end