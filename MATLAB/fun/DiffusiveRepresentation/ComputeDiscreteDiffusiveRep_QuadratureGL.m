function [xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(mu_an,N,varargin)
%ComputeDiscreteDiffusiveRep_QuadratureGL Compute a discretisation of the
%diffusive representation (xi,mu), using a Gauss-Legendre quadrature rule o
%n the the interval (-1,1).
% The greater N, the greater the accuracy of the approximation:
%      -
%     / mu(xi) d(xi) (continuous) -> Sum   muk   (discrete)
%    -  ------                           -------
%       s + xi                           s + xik
% Input:
%   mu_an (N->N) (fun handle) analytical diffusive weight mu(xi)
%   N (1) Number of quadrature nodes
% Optional:
%   CoVParam (1) parameter for change of variables
% Outputs:
%   xik (N) poles
%   muk (N) associated gain

    validateattributes(mu_an,{'function_handle'},{},mfilename,'mu_an');
    xi = [0,1];
    validateattributes(mu_an(xi),{'numeric'},{'size',size(xi)},mfilename,'mu_an(xi)');
    validateattributes(N,{'numeric'},{'scalar','integer','positive'},mfilename,'N');
        % Optional argument
    p = inputParser; p.FunctionName = mfilename;
    addParameter(p,'CoVParam',2,@(x)(validateattributes(x,{'numeric'},{'scalar','positive'})));
    parse(p,varargin{:});  oA = p.Results;
    beta = oA.CoVParam;
    
        % Mapping from (0,1) to (0,Inf)
        % Includes a regularisation at t=0
        % From 'Vectorized adaptive quadrature in MATLAB' Shampine 2008
        % Typical value (beta = 2);
    psi =  @(t)((t./(1-t)).^beta);
    dpsi = @(t)(beta*(t.^(beta-1))./(1-t).^(beta+1));
    
        % Mapping  from (-1,1) to (0,1)
    ki = @(t)((t+1)/2);
        % Get quadrature nodes on (-1,1) (no end points)
    [x,w] = GaussLegendreQuadraturePoints(N);
        % Compute diffusive weights and poles
    mu = 0.5*w.*mu_an(psi(ki(x))).*dpsi(ki(x));
    xi = psi(ki(x));
end