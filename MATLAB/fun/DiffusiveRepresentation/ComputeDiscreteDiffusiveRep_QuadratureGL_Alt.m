function [xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL_Alt(mu_an,N,varargin)
%ComputeDiscreteDiffusiveRep_QuadratureGL Compute a discretisation of the
%diffusive representation (xi,mu), using a Gauss-Legendre quadrature rule o
%n the the interval (-1,1).
% Fractional integral s^alpha with alpha in (0,1).
% The greater N, the greater the accuracy of the approximation:
%      -
%     / mu(xi) d(xi) (continuous) -> Sum   muk   (discrete)
%    -  ------                           -------
%       s + xi                           s + xik
% Input:
%   N (1) Number of quadrature nodes
%   mu_an (N->N) (fun handle) analytical diffusive weight mu(xi)
% Input (optional):
%   CoVParam (1) parameter for change of variables (>=0) (default: 1/2)
%   Optim (struct) parameter that describes the least-squares optimization 
%   to be used to refine poles and weights. Two fields:
%       - h_an: transfer function @(s)h_an(s) to be approximated
%       - Method:
%           'LinOptimStd': least squares on weights only (w/o positivity
%       constraint), standard formulation (h_an is standard)
%           'LinOptimExt': identical, but extended formulation (i.e. h_an
%           is extended)
%           'NonLinOptimStd': least squares on both poles and weights (with 
%       positivity and upper bound constraints), standard formulation
%           'NonLinOptimExt': identical, but h_an is extended
%           'NonLinOptimRefl': identical, but h_an is standard / reflection
%           coefficient formulation for optimization
%       (default: 1e3*N points logarithmically spaced btw xi_min and xi_max) 
%       - (Optional) Nw: number of angular frequencies used to define cost function
%       - (Optional) Tol: tolerance (x & fun) for nonlinear optimization
%       (default: 1e-15)
% Outputs:
%   xik (N) poles
%   muk (N) associated gain

    validateattributes(mu_an,{'function_handle'},{},mfilename,'mu_an');
    xi = [0,1];
    validateattributes(mu_an(xi),{'numeric'},{'size',size(xi)},mfilename,'mu_an(xi)');
    validateattributes(N,{'numeric'},{'scalar','integer','positive'},mfilename,'N');
        % Optional argument
    p = inputParser; p.FunctionName = mfilename;
    addParameter(p,'CoVParam',0.5,@(x)(validateattributes(x,{'numeric'},{'scalar','positive'})));
    addParameter(p,'Optim',[],@(x)(validateattributes(x,{'struct'},{})));
    parse(p,varargin{:});  oA = p.Results;
    beta = oA.CoVParam; Optim=oA.Optim;
            % Get quadrature nodes on (-1,1) (no end points)
    [x,w] = GaussLegendreQuadraturePoints(N);
            % Compute diffusive weights and poles
    xi = (1+x)./(1-x); xi = xi.^(1/beta);
    mu = (1-x).^(-1-1/beta).*(1+x).^(-1+1/beta).*mu_an(xi);
    mu = 2/beta*mu.*w;
    fprintf('[%s] Computation of weights/poles with quadrature (beta=%1.3e)....\n',mfilename,beta);
    fprintf('[%s] Done. xi_max=%1.3e, xi_min=%1.3e, mu_max=%1.3e, mu_in=%1.3e.\n',mfilename,max(xi),min(xi),max(mu),min(mu));
    if ~isempty(Optim)
        if isfield(Optim,'Method') && isfield(Optim,'h_an')
            h_an = Optim.h_an;
            Nw = 1e2*length(xi);
            if isfield(Optim,'Nw')
               Nw = Optim.Nw;
            end
            omegan = logspace(log10(min(xi)),log10(max(xi)),Nw);
            Tol=1e-15;
            if isfield(Optim,'Tol')
               Tol = Optim.Tol;
            end

                % Recompute the weights using a linear least-squares
                % optimization
           if strcmp(Optim.Method,'LinOptimStd')
                fprintf('[%s] Refinment with linear least squares...\n',mfilename);
                [mu,~,~] = ComputeDiscreteDiffusiveRep_Optim_Std(xi,[],omegan,@(om)(om./om),h_an,0);
                xi = xi(:); mu=mu(:);
                fprintf('[%s] Done.\n',mfilename);
                % Recompute both poles and weights using a nonlinear
                % least-squares optimization
            elseif strcmp(Optim.Method,'NonLinOptimStd')
                    % Assuming h_an is standard
               fprintf('[%s] Refinment with nonlinear least squares (Standard)...\n',mfilename);
               [xi,mu] = ComputeDiscreteDiffusiveRep_Optim_StdNonlinear_Alt(xi,mu,omegan,h_an,'Tol',Tol);
               fprintf('[%s] Done.\n',mfilename);
            elseif strcmp(Optim.Method,'NonLinOptimStdNorm')
                    % Assuming h_an is standard
                fprintf('[%s] Refinment with nonlinear least squares (Standard-Normalized)...\n',mfilename);
                [xi,mu] = ComputeDiscreteDiffusiveRep_Optim_StdNonlinear_Alt(xi,mu,omegan,h_an,'Tol',Tol);
                fprintf('[%s] Done.\n',mfilename);
            elseif strcmp(Optim.Method,'NonLinOptimExt')
                    % Assuming h_an is extended
                fprintf('[%s] Refinment with nonlinear least squares (Extended)...\n',mfilename);
                [xi,mu] = ComputeDiscreteDiffusiveRep_Optim_ExtNonlinear_Alt(xi,mu,omegan,@(s)h_an(s),'Tol',Tol);
%                [xi,mu] = ComputeDiscreteDiffusiveRep_Optim_ExtNonlinear_AltLombard(xi,mu,omegan,@(s)h_an(s),'Tol',Tol);
                fprintf('[%s] Done.\n',mfilename);
           elseif strcmp(Optim.Method,'NonLinOptimRefl')
                    % Assuming h_an is standard
                fprintf('[%s] Refinment with nonlinear least squares (Reflection coeff.)...\n',mfilename);
                [xi,mu] = ComputeDiscreteDiffusiveRep_Optim_ReflbNonlinear(xi,mu,omegan,@(s)h_an(s),'Tol',Tol);
                fprintf('[%s] Done.\n',mfilename);
            else
                error('Unknown parameter for ''Optim.Method''.');
           end
        else
           error('Structure ''Optim'' has no field ''Method'' or ''h_an''.');
        end
    end
end