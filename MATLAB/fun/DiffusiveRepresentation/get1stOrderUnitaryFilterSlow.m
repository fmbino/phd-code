function H = get1stOrderUnitaryFilterSlow(s,xi,varargin)
%get1stOrderUnitaryFilterSlow
%   1/(s+xi) (low-pass) or s/(s+xi) (high-pass).
% This is a slow implementation.
% Inputs:
%   s (N) Laplace variable (complex)
%   xi (P) pulsation (rad/s)
% Inputs (optional):
%   type (str) 'low-pass' (default) or 'high-pass'
% Output:
%   H (NxP)

        % Optional argument
    p = inputParser; p.FunctionName = mfilename;
    addParameter(p,'type','low-pass',@(x)(validateattributes(x,{'char'},{})));
    parse(p,varargin{:});  oA = p.Results;
    type = oA.type;


    if isempty(s) || isempty(xi)
        H = [];
    else
        validateattributes(s,{'numeric'},{'vector'},mfilename,'s');
        validateattributes(xi,{'numeric'},{'vector'},mfilename,'xi');
            % Convert to convenient shape
        s=s(:); % column
        xi=transpose(xi(:)); % row
        H = zeros(length(s),length(xi));
            if strcmp(type,'low-pass')
                for j=1:length(s)
                    H(j,:) = (s(j)+xi).^(-1);
                end
            elseif strcmp(type,'high-pass')
                for j=1:length(s)
                    H(j,:) = s(j)*(s(j)+xi).^(-1);
                end
            else
                error('[%s] Unknown type: ''low-pass'' or ''high-pass'' expected.',mfilename);
            end
            
    end
end