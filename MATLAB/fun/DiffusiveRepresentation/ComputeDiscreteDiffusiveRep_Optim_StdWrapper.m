function [xik,muk,rn,sn] = ComputeDiscreteDiffusiveRep_Optim_StdWrapper(xik,sn,omegan,w,H,e)
%ComputeDiscreteDiffusiveRep_Optim_StdWrapper This is a VERY stupid
%function: it only changes the output arguments of
%ComputeDiscreteDiffusiveRep_Optim_Std...
%No value added, but it was too annoying to modify 
%ComputeDiscreteDiffusiveRep_Optim_Std. 

    [muk,rn,sn] = ComputeDiscreteDiffusiveRep_Optim_Std(xik,sn,omegan,w,H,e);
        
    xik = xik(:);
end

