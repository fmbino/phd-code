function [xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL_BesselJ0(N)
%ComputeDiscreteDiffusiveRep_QuadratureGL_BesselJ0 Compute diffusive
%representation of the Bessel function of the first kind and order 0, using
%the identity (Matignon1998,3.9).
%                      muk   
%   J0 =    Sum      -------  (discrete).
%                    s + xik
% Input:
%   N (1) Number of quadrature nodes
% Outputs:
%   xik (N) poles
%   muk (N) associated gain
        % Diffusive weight associated with the upper cut (-inf+1i,1i].
    mu_an = @(xi)1./(pi*sqrt(xi).*sqrt(-xi+2*1i));
            % best choice for fractional operator
    beta = @(alpha)min(alpha,1-alpha);
    [xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL_Alt(mu_an,N,'CoVParam',beta(0.5));
    
    xi = xi-1i;
    mu = mu; % (Matignon1998,3.9) suggests '-mu', this is a typo.
        % Hermitian symmetry
     xi = [xi(:);conj(xi(:))];
     mu = [mu(:);conj(mu(:))];
end