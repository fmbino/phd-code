function [xi,mu] = ComputeDiscreteDiffusiveRep_Optim_StdNonLinear(xi,omegan,H,varargin)
%ComputeDiscreteDiffusiveRep_Optim_StdNonLinear Use a nonlinear
%optimization, i.e. optimization on both weights and poles.
% Argument identical to its linear counterpart.
% Rmk: the oscillatory part is not handled here (it could be).
% Inputs:
%   xi (Nxi) initial pole distribution in (0,infty)
%   H (fun_handle) standard diffusive function being approximated (Laplace)
%   omegan (Nw) discrete pulsation (rad/s)
% Optional input:
%   Tol (scalar) tolerance (step & fun) for lsqcurvefit (default:1e-15)
% Outputs:
%   xi (Nxix1) pole distribution
%   mu (Nxix1) gain for the diffusive part
% Rmk: output variables are NaN if the algorithm did not converge

    if (isempty(xi))
       mu=[];
       fprintf('No diffusive (xi) part (sn) ... Stopping.\n');
       return
    end
    validateattributes(omegan,{'numeric'},{'vector'},3);
    validateattributes(H,{'function_handle'},{'scalar'},5);
                % Optional argument
    p = inputParser; p.FunctionName = mfilename;
    addParameter(p,'Tol',1e-15,@(x)(validateattributes(x,{'numeric'},{'scalar','positive'})));
    parse(p,varargin{:});  oA = p.Results;
    Tol = oA.Tol;

        % Compute initial weight distribution using a linear least squares
        % optimization
    [mu,~,~] = ComputeDiscreteDiffusiveRep_Optim_Std(xi,[],omegan,@(om)(om./om),H,0);
    xi = xi(:); mu=mu(:);
    Nxi = length(xi);
        % (Original) Nonlinear least-squares optimization on both mu and xi
% if Nxi<=50 && Nxi>=2 % Avoid too many parameters
%         % Definition of coeff for optimization
%     coeff_init = [mu(:);xi(:)];
%     coeff2mu = @(coeff)coeff(1:Nxi);
%     coeff2xi = @(coeff)coeff((Nxi+1):end);
%         % Function definition for optimization
%     H_optim = @(s,coeff)get1stOrderUnitaryFilter(s,coeff2xi(coeff),'type','low-pass')*coeff2mu(coeff);
%     fun = @(coeff,xdata)[real(H_optim(1i*xdata(:),coeff));imag(H_optim(1i*xdata(:),coeff))];
%         % Nonlinear least square optimization
%     options = optimoptions('lsqcurvefit','Display','final-detailed','TolX',1e-5,'TolFun',1e-5);
%     ydata = H(1i*omegan); ydata = [real(ydata(:));imag(ydata(:))];
%     coeff_optim = lsqcurvefit(fun,coeff_init,omegan,ydata,[],[],options);    
%         % Check that mu and xi are positive?
%     mu = coeff_optim(1:Nxi);
%     xi = coeff_optim((Nxi+1):end);
% end
    % (Refined) Nonlinear least-squares optimization on both mu and xi
    % Compared to the original version, we try to make a better call to
    % lsqcurvefit
    [xi,mu] = ComputeDiscreteDiffusiveRep_Optim_StdNonlinear_Alt(xi,mu,omegan,H,'Tol',Tol);
end