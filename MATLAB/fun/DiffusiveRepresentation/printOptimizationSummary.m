function printOptimizationSummary(xik,muk,sn,rn,rn_an)
%printOptimizationSummary Print, on the standard output, a summary of the
%optimisation.
% Inputs:
%   xik (Nxi) discrete pulsations for the diffusive part (rad/s)
%   muk (Nxi) weight from the optimisation
%   sn (Np) poles for the local part (rad/s)
%   rn (Np) residual from the optimisation
%   rn_an (Np) residual from the analytical formula rn=f(sn)
    validateattributes(xik,{'numeric'},{'vector'},1);
    validateattributes(muk,{'numeric'},{'vector'},2);
fprintf('---- Optimisation Results Summary\n');
fprintf('Diffusive part:\n');
fprintf('\txi_k (rad/s)\t mu_k\n')
for i=1:length(xik)
    fprintf('\t%1.1e\t\t%1.1e\n',xik(i),muk(i));
end
if ~isempty(sn)
fprintf('Local part: (an = analytical)\n');
fprintf('\t    Poles (analytical)\t\t      Residual (from optim)\tResidual (analytical)\n');
fprintf('\tRe[-s_n] (rad/s) Im[s_n] (rad/s)\tRe[r_n]  Im[r_n]\tRe[r_n_an] Im[r_n_an]\t|r_n/r_n_an|\n');
for i=1:length(sn)
    fprintf('\t% 8.1e \t% 8.1e \t\t% 8.1e % 8.1e \t% 8.1e   % 8.1e \t% 8.1e\n',-real(sn(i)),imag(sn(i)),real(rn(i)),imag(rn(i)),real(rn_an(i)),imag(rn_an(i)),abs(rn(i)/rn_an(i)));
end
end
fprintf('\n');