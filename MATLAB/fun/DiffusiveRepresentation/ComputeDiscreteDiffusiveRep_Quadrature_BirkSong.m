function [xi,mu] = ComputeDiscreteDiffusiveRep_Quadrature_BirkSong(N,alpha,quadrule)
%ComputeDiscreteDiffusiveRep_QuadratureGL Compute a discretisation of the
%diffusive representation (xi,mu), using a Gauss-Jacobi quadrature rule o
%n the the interval (-1,1), from [Birk and Song 2010].
% Fractional integral s^(-alpha) with alpha in (0,1).
% The greater N, the greater the accuracy of the approximation:
%      -
%     / mu(xi) d(xi) (continuous) -> Sum   muk   (discrete)
%    -  ------                           -------
%       s + xi                           s + xik
% Input:
%   N (1) Number of quadrature nodes
%   alpha (1) order of fractional integral
%   quadrule (str) string that contains
%       'BirkSong' Quadrature rule defined in [Birk and Song 2010, Eq.23]
%       'Diethelm' [Birk and Song 2010, Eq.18]
%       'YA' [Birk and Song 2010, Eq.11]
% Outputs:
%   xik (N) poles
%   muk (N) associated gain
    validateattributes(N,{'numeric'},{'scalar','integer','positive'},mfilename,'N');
    validateattributes(alpha,{'numeric'},{'scalar'},mfilename,'alpha');
    validateattributes(quadrule,{'char'},{},mfilename,'quadrule');
    alpha = 1-alpha; % from integral to derivative
    alphabar = 2*alpha -1; % expression of alpha bar for alpha in (0,1)
        % -- [Birk and Song 2010, Eq.23].
    if strcmp(quadrule,'BirkSong')
        gamma = 2*alphabar+1;
        beta = -(2*alphabar-1);
            % Get quadrature nodes on (-1,1) (no end points)
        [x,w] = JacobiGQ(gamma,beta,N); x = x(:); w=w(:);
                % Compute diffusive weights and poles
        xi = (1-x)./(1+x); xi = xi.^4;
        mu = (2*sin(pi*alpha)/pi)*4*w./((1+x).^4);
        % -- [Birk and Song 2010, Eq.18]
        % Does not work for alpha!=1/2, there must be a typo
    elseif strcmp(quadrule,'Diethelm')
        gamma = alphabar;
        beta = -alphabar;
            % Get quadrature nodes on (-1,1) (no end points)
        [x,w] = JacobiGQ(gamma,beta,N); x = x(:); w=w(:);
                % Compute diffusive weights and poles
        xi = (1-x)./(1+x); xi = xi.^2;
        mu = (2*sin(pi*alpha)/pi)*2*w./((1+x).^2);
        % -- [Birk and Song 2010, Eq.11]
        % Never works at all
    elseif strcmp(quadrule,'YA')
        [x,w] = GaussLaguerreQuadraturePoints(N, 0); x=x(:);w=w(:);
        xi = x.^2;
        mu = (2*sin(pi*alpha)/pi)*exp(xi).*w(:).*(xi.^alphabar);
    else
        error('[%s] Unknown quadrature rule',mfilename);
    end
end