function sn = computePolesWebsterLokshin(rho,epsi,N)
%computePolesWebsterLokshin Compute the first poles of the Webster-Lokshin 
%transfer function :
% H(s) = e^-eps*sqrt(s) x [1-r*e^(s+eps*sqrt(s))]^-1 r!=0 and eps>0
% (from Heleschewitz's thesis, chapter 9)
% Inputs:
%   rho (1x1) global reflexion coefficient (real, non-null)
%   epsi (1x1) damping parameter epsilon (positive)
%   N (1x1)
% Outputs:
%   sn (2*N+1x1) first roots of 1-r*e^(s+eps*sqrt(s))
% Remark: The given poles always have hermitian symmetry.

sn = zeros(0); % poles
for n=0:N
Sigma = roots([1,epsi,-(log(sqrt(abs(rho)))+1i*angle(rho)/2+1i*n*pi)]);
Sigma = Sigma(real(Sigma)>=0); % expected: 1 root remaining
    if length(Sigma)~= 1
        warning('No poles for n=%d\n',n);
    else
        sn(end+1) = Sigma^2;
        %fprintf('-- (n=%d) s_n = (%2.1e,%2.1e°) && r_n = (%2.1e,%2.1e°)\n',n,abs(sn(end)),angle(sn(end))*180/pi,abs(rn(end)),angle(rn(end))*180/pi);
            % add the conjugate, if the pole is complex
        if imag(sn(end))<eps
            warning(sprintf('Pole n°%d assumed real (Im=% 5.1e)\n',n,imag(sn(end))));
            sn(end) = real(sn(end));
        else
            sn(end+1)=conj(sn(end));
        end
    end
end
end