function rn = computeResFPCavity(a,lc,alpha,sn)
%computeResFPCavity Compute the residual rn associated to the poles sn.
% Ref. computePolesFPCavity for more details.
% Inputs:
%   a (3x1) coefficient of j*k(s) [a0,aa,a1]
%   lc (1x1) length of the cavity (m)
%   alpha (1x1) fractional power
%   N (1x1)
%   sn (N) poles
% Outputs:
%   rn (N) associated residuals

    rn = exp(-2*a(2)*lc*sn.^alpha)./(2*lc*(a(3)+alpha*a(2)*sn.^(alpha-1)));
end

