function [sn,rn] = computePolesEmpty()
%computePolesEmpty Dummy function which returns empty sets.

    sn = []; % poles
    rn = []; % residual
end

