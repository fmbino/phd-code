function [rn] = computeResWebsterLokshin(epsi,sn)
%computeResWebsterLokshin Compute the residuals of the Webster-Lokshin 
%transfer function (ref. computePolesWebsterLokshin)
% Inputs:
%   epsi (1x1) damping parameter epsilon (positive)
%   sn (N) poles in the complex plane
% Outputs:
%   rn (N) associated residuals

    rn = exp(-epsi*sqrt(sn))./(2*(1+epsi./(2*sqrt(sn))));
end

