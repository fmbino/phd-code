function [sn,rn] = computePolesFPCavity(a,lc,alpha,N)
%computePolesFPCavity Compute the first poles of the diffusive part of the 
%cavity impedance transfer function (assuming a fractional polynomial wavenumber):
%   D(s) = @(s)(exp(-2*aa*lc*s.^alpha)./(1-exp(-2*lc*(a0+a1*s+aa*s.^alpha))));
%
% Inputs:
%   a (3x1) coefficient of j*k(s) [a0,aa,a1]
%   lc (1x1) length of the cavity (m)
%   alpha (1x1) fractional power
%   N (1x1)
% Outputs:
%   sn (2*N+1x1) first roots of 1-exp(-2*lc*(a0+a1*s+aa*s.^alpha)))
% Rmk: only poles with non-zero residual are given

if alpha~=0.5
    warning('Only the case alpha=1/2 is covered.');
end

sn = zeros(0); % poles
for n=0:N
Sigma = roots([a(3)*lc,a(2)*lc,a(1)*lc-1i*n*pi]);
Sigma = Sigma(real(Sigma)>=0); % expected: 1 root remaining
    if isempty(Sigma)
        warning('No poles for n=%d (Sigma has %d elements)\n',n,length(Sigma));
    else
        if length(Sigma)==2
            warning('Sigma has %d elements: same square (%d)\n',length(Sigma),Sigma(1)^2==Sigma(2)^2);
            Sigma=Sigma(1); % keep only one (no difference)
        end
        s = Sigma^2;
        if abs(s)<eps
            % sn=0, associated residual is 0 as well: do not add to sn
            warning(sprintf('Pole n°%d assumed null and removed (|sn|=% 5.1e)\n',n,abs(s)));
        elseif imag(s)<eps
            warning(sprintf('Pole n°%d assumed real (Im=% 5.1e)\n',n,imag(s)));
            sn(end+1) = real(s);
        else
            sn(end+1)=s;
            sn(end+1)=conj(s);
        end
        %fprintf('-- (n=%d) s_n = (%2.1e,%2.1e°) && r_n = (%2.1e,%2.1e°)\n',n,abs(sn(end)),angle(sn(end))*180/pi,abs(rn(end)),angle(rn(end))*180/pi);
    end
end
end