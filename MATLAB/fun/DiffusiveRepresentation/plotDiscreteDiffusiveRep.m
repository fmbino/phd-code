function plotDiscreteDiffusiveRep(gh,f,mu1,xi1,r1,s1,mu2,xi2,r2,s2,H_dis,H_an)
%plotDiscreteDiffusiveRep Plot analytical function H_an vs and its
%approximation H_an(s,h1(s),h2(s)),
% where h1(s) is given by
%           h1(s) = Σ_k mu1_k / (s+xi1_k) + Σ_n r1_n / (s-s1_n) (Re(s)>0),
% and similarly for h2(s).
% Inputs:
%   gh (graphic handle)
%   f (P) plot frequencies (NOT angular frequency w=2*pi*f)
%   mu1,xi1,r1,s1 (Nxi1) poles and weights for h1
%   mu2,xi2,r2,s2 (Nxi2) poles and weights for h2
%   H_dis (function_handle) discrete approximation H(s,h1,h2)
%   H_an (function_handle) analytical H_an(s)


    % -- Validate arguments
    if(~ishghandle(gh))
        error('[%s] <gh> should be a graphic handle: use gh=figure',mfilename);
    else
        figure(gh); % Focus
    end
    validateattributes(mu1,{'numeric'},{},mfilename,'mu1');
    validateattributes(xi1,{'numeric'},{'numel',length(mu1)},mfilename,'xi1');
    validateattributes(r1,{'numeric'},{},mfilename,'r1');
    validateattributes(s1,{'numeric'},{'numel',length(r1)},mfilename,'s1');
    validateattributes(mu2,{'numeric'},{},mfilename,'mu2');
    validateattributes(xi2,{'numeric'},{'numel',length(mu2)},mfilename,'xi2');
    validateattributes(r2,{'numeric'},{},mfilename,'r2');
    validateattributes(s2,{'numeric'},{'numel',length(r2)},mfilename,'s2');
    validateattributes(H_dis,{'function_handle'},{},mfilename,'H');
    tmp = linspace(1,2,4); tmp=tmp(:);
    validateattributes(H_dis(tmp(:),tmp(:),tmp(:)),{'numeric'},{'vector','numel',length(tmp)},mfilename,'H(tmp,tmp,tmp)');
    validateattributes(H_an,{'function_handle'},{},mfilename,'H_an');
    validateattributes(H_an(tmp(:)),{'numeric'},{'vector','numel',length(tmp)},mfilename,'H_an(tmp)');
    clear tmp
    % -- Build discrete approximation of H_an(s)
        % Remark: s row or column -> F_pc(s) column
    F_pc = @(s,sn,rn,xik,muk)((get1stOrderUnitaryFilter(s,[-sn(:);xik(:)]))*[rn(:);muk(:)]);
    h1 = @(s)(F_pc(s,s1(:),r1(:),xi1(:),mu1(:)));
    h2 = @(s)(F_pc(s,s2(:),r2(:),xi2(:),mu2(:)));
    H_discrete = @(s)(H_dis(s,h1(s),h2(s)));
    xi = [xi1(:);xi2(:)];
    s = [s1(:);s2(:)];
    % -- Plot Fourier transform
    omega = 2*pi*f; % angular frequency (rad/s)
    H_d = H_discrete(1i*omega(:));
    subplot(2,1,1)
    hold all
    leg = cell(0);
    plot(f,real(H_an(1i*omega)));
    leg{end+1}='H Analytical';
    plot(f,real(H_d));
    leg{end+1}=sprintf('H Discrete\n%d xi_k in [%1.1e,%1.1e] Hz\n %d s_n |sn| in [%1.1e,%1.1e] Hz',length(xi),min(xi)/(2*pi),max(xi)/(2*pi),length(s),min(abs(s))/(2*pi),max(abs(s))/(2*pi));
    xlim([min(f),max(f)]);
    xlabel('Frequency f (Hz)');
    title('F.T.: real(H(1i*2*pi*f))');
    legend(leg);
    subplot(2,1,2)
    hold all
    plot(f,imag(H_an(1i*omega)));
    plot(f,imag(H_d));
    xlim([min(f),max(f)]);
    xlabel('Frequency f (Hz)');
    title('imag(H(1i*2*pi*f))');
end