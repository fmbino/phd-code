function [xik,muk,rn,sn] = ComputeDiscreteDiffusiveRep_Optim_ExtWrapper(xik,sn,omegan,w,H,e)
%ComputeDiscreteDiffusiveRep_Optim_StdWrapper This is a VERY stupid
%function: it only changes the output arguments of
%ComputeDiscreteDiffusiveRep_Optim_Ext...
%No value added, but it was too annoying to modify 
%ComputeDiscreteDiffusiveRep_Optim_Ext. 
    
    [muk,rn,sn] = ComputeDiscreteDiffusiveRep_Optim_Ext(xik,sn,omegan,w,H,e);
    
    xik = xik(:);
end

