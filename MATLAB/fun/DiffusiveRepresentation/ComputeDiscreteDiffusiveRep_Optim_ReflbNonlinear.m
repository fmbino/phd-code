function [xi,mu] = ComputeDiscreteDiffusiveRep_Optim_ReflbNonlinear(xi,mu,omegan,H,varargin)
%ComputeDiscreteDiffusiveRep_Optim_ReflNonLinear Use a nonlinear
%optimization, i.e. optimization on both weights and poles, using the
%reflection coefficient formulation
% Optimization constraints:
%   [xi,mu]>=0 and xi<=xi_max (initial pole distribution)
% Rmk: the oscillatory part is not handled here (it could be).
% Inputs:
%   xi (Nxi) initial pole distribution in (0,infty)
%   mu (Nxi) initial pole distribution in (0,infty)
%   omegan (Nw) discrete pulsation (rad/s)
%   H (fun_handle) Standard diffusive function being approximated (Laplace)
% Optional input:
%   Tol (scalar) tolerance (step & fun) for lsqcurvefit (default:1e-15)
% Outputs:
%   xi (Nxix1) pole distribution
%   mu (Nxix1) gain for the diffusive part
% Rmk: output variables are NaN if the algorithm did not converge


    validateattributes(xi,{'numeric'},{'vector'},1);
    if (isempty(xi))
       mu=[];
       fprintf('No diffusive (xi) part (sn) ... Stopping.\n');
       return
    end
    validateattributes(mu,{'numeric'},{'vector','numel',length(xi)},2);
    validateattributes(omegan,{'numeric'},{'vector'},3);
    validateattributes(H,{'function_handle'},{'scalar'},4);
            % Optional argument
    p = inputParser; p.FunctionName = mfilename;
    addParameter(p,'Tol',1e-15,@(x)(validateattributes(x,{'numeric'},{'scalar','positive'})));
    parse(p,varargin{:});  oA = p.Results;
    Tol = oA.Tol;
    
    Nxi = length(xi);
    if Nxi<=50 && Nxi>=1 % Avoid too many parameters
        % Definition of coeff for optimization
    coeff_init = [xi(:);mu(:)];
    coeff2xi = @(coeff)coeff(1:Nxi);
    coeff2mu = @(coeff)coeff((Nxi+1):end);
            % Nonlinear least square optimization
    options = optimoptions('lsqcurvefit','Jacobian','on','Display','final-detailed','TolX',Tol,'TolFun',Tol,'MaxIter',5e4,'MaxFunEvals',5e4);
    ydata = z2beta(H(1i*omegan)); ydata = [real(ydata(:));imag(ydata(:))];
            % Lower bound constraints
            %   [xi,mu] >=0
    lb=0*coeff_init; % length 2*Nxi
            % Upper bound constraints
            %   xi<=xi_max, no constraints on mu
    ub = ones(Nxi,1)*max(coeff2xi(coeff_init)); % length Nxi only
            % Optimization
    [coeff_optim,~,~,exitflag] = lsqcurvefit(@fun,coeff_init,omegan,ydata,lb,ub,options);
            % Retrieve xi and mu
    if exitflag<=0 % algorithm did not converge properly
        xi = NaN*coeff_optim;
        mu= NaN*coeff_optim;
    else
        xi = coeff2xi(coeff_optim);
        mu = coeff2mu(coeff_optim);
    end
    end

end

function [F,J] = fun(coeff,xdata)
%fun Function to be used in call to lsqcurvefit
% Outputs:
%   F (2*length(xdata)) function values at coeff
%   J (2*length(xdata) x length(coeff)) Jacobian matrix at coeff

    K = length(xdata); % number of angular frequencies
    Nxi = length(coeff)/2; % number of poles
        % Function definition for optimization
    coeff2xi = @(coeff)coeff(1:Nxi);
    coeff2mu = @(coeff)coeff((Nxi+1):end);
    H_optim = @(s,coeff)z2beta(get1stOrderUnitaryFilter(s,coeff2xi(coeff),'type','low-pass')*coeff2mu(coeff));
    F = [real(H_optim(1i*xdata(:),coeff));imag(H_optim(1i*xdata(:),coeff))];
if nargout > 1   % Two output arguments
        % Matrix: (1i*omega_i+xi_j)^-1 (size K X Nxi)
    H = get1stOrderUnitaryFilter(1i*xdata(:),coeff2xi(coeff),'type','low-pass');
        % Matrix: -mu_j/(1i*omega_i+xi_j)^2 (size K x Nxi)
    G = -(H.*H).*kron(ones(length(H),1),transpose(coeff2mu(coeff))); % a bit costly in memory
        % Matrix: (1+sum_n mu_n/(1i*omega_i+xi_n)_i,j (size K x Nxi)
    R = 1+get1stOrderUnitaryFilter(1i*xdata(:),coeff2xi(coeff),'type','low-pass')*coeff2mu(coeff);
    R = kron(R,ones(1,Nxi));
        % Jacobian matrix
    J = zeros(2*K,2*Nxi);
            % Real part w.r.t. xi (K*Nxi block)
    J(1:K,1:Nxi) = real(2*G./(R.^2));
            % Real part w.r.t. mu (K*Nxi block)
    J(1:K,Nxi+(1:Nxi)) = real(2*H./(R.^2));
            % Imag part w.r.t. xi (K*Nxi block)
    J(K+(1:K),1:Nxi) = imag(2*G./(R.^2));
            % Imag part w.r.t. mu (K*Nxi block)
    J(K+(1:K),Nxi+(1:Nxi)) = imag(2*H./(R.^2));
end
end

function beta = z2beta(z)
    beta = (z-1)./(z+1);
end