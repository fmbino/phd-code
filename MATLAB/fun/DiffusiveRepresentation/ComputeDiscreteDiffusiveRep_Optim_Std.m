function [muk,rn,sn] = ComputeDiscreteDiffusiveRep_Optim_Std(xik,sn,omegan,w,H,e,varargin)
%ComputeDiscreteDiffusiveRep_Optim_Std Compute a discrete oscillatory and
%diffusive representation using the standard optimisation technique
%(pseudo-inverse).
%                (oscillatory part: poles sn and residual rn)
%                (diffusive part: poles xik and weight muk)
%                   r_n          mu_k
% H_num(s) =       -------  + --------- (approximation of H(s))
%                 s - s_n     s + xi_k
% Remark: without contraints, mu can be negative
% Inputs:
%   xik (Nxi) set of (real positive) pulsation (diffusive part)
%   sn (Np) set of (complex) poles (oscillatorys part)
%   omegan (Nw) discrete pulsation (rad/s), N-1 bands
%   w (f_handle) weight function w(omega)
%   H (f_handle) function being approximated
%   e (1) relaxation parameter >0 (cost function)
% Inputs (optional):
%   nonnegConstraint (boolean) enforce nonnegativity of weights
%       (Does not yet distinguish between r_n and mu_k.)
% Outputs:
%   muk (Nxix1) gain for the diffusive part
%   rn (Npx1) residual for the local part
%   sn (Npx1) (complex) poles (re-sorted consistently with rn)
    
    if (isempty(xik) && isempty(sn))
       muk=[]; rn=[]; sn=[];
       fprintf('No diffusive (xik) AND oscillatory part (sn) ... Stopping.\n');
       return
    end
    validateattributes(omegan,{'numeric'},{'vector'},3);
    validateattributes(w,{'function_handle'},{'scalar'},4);
    validateattributes(H,{'function_handle'},{'scalar'},5);
    validateattributes(e,{'numeric'},{'scalar','nonnegative'},6);
        % Optional arguments (Default value: empty or null)
    p = inputParser; p.FunctionName = mfilename;
    addParameter(p,'nonnegConstraint',0,@(x)validateattributes(x,{'numeric'},{'scalar'}));
    parse(p,varargin{:});  oA = p.Results;
    nonnegConstraint = oA.nonnegConstraint;
    
    fprintf('---- Standard Optimization J(x) = ||W*(M*x-h)||^2+e||W*x||^2\n');
        % 1) Build ponderation (diagonal) matrix
    W = computeWeightVector(w,omegan);
    W = sparse(1:length(W),1:length(W),W); % sparse diagonal matrix
    fprintf('\tMeasurement over %d pulsation bands which cover [%1.1e,%1.1e] rad/s\n',length(omegan)-1,min(omegan),max(omegan));
    omegan = omegan(1:(end-1));
        % 2) Get M(w,xi)
    M = zeros(0);
    if ~isempty(xik)
        fprintf('\tDiffusive part: %d poles in [%1.1e,%1.1e] rad/s\n',length(xik),min(xik),max(xik));
        M = get1stOrderUnitaryFilter(omegan*1i,xik); % diffusive part
    else
        fprintf('\tNo diffusive part.\n');
    end
    if ~isempty(sn)
            % segregate real and complex poles (upper plane)
        sn_r = sn(imag(sn)==0);
        sn_up = sn(imag(sn)>0);
        if (length(sn_r)+2*length(sn_up))~=length(sn)
            error('\tOscillatory part: Poles have been lost during the segregation process...');
        else
            fprintf('\tOscillatory part: %d real poles in [%1.1e,%1.1e] rad/s\n',length(sn_r),min(sn_r),max(sn_r));
            fprintf('\tOscillatory part: %d complex poles, Re in [%1.1e,%1.1e] rad/s\n',2*length(sn_up),min(real(-sn_up)),max(real(-sn_up)));
        end
            % builds M matrix
        if ~isempty(sn_r) % add real poles sn_r
            M = [M, get1stOrderUnitaryFilter(omegan*1i,-sn_r)];
        end
        if ~isempty(sn_up)
                % add complex poles sn_up (Re)
            M_up = get1stOrderUnitaryFilter(omegan*1i,-sn_up); % (jw-sn_up)
            M_upc = get1stOrderUnitaryFilter(omegan*1i,-conj(sn_up)); % (jw-sn_up*)
            M = [M, M_up+M_upc];
                % add complex poles sn_up (Im)
            M = [M, 1i*(M_up-M_upc)];
        end
    else
        fprintf('\tNo Oscillatory part');
    end
    fprintf('\tSize of M: %d x %d\n',size(M,1),size(M,2));
        % 3) Definition of the cost function J(x)=||C*x-d||^2
            % Exact transfert function (force column vector)
    h = H(omegan*1i); h=h(:);
    C = W*M; 
    d = W*h; 
            % Enforce mu reality (as in [Helie06])
            % mu>0 must be checked though (unconstrained optimization)
    C = [real(C);imag(C)];
    d = [real(d); imag(d)];
        % 4) Find min J(mu) through a pseudo-inverse
    %Y = C'*C;
    %fprintf('\tOriginal pb. condition no.: %1.1e\n',cond(Y));
    %fprintf('\tRelaxed pb. condition no.: %1.1e (e=%1.1e)\n',cond(Y+e*eye(size(Y))),e);
    %x = (Y+e*eye(size(Y)))\(C'*d);
    %clear Y        
    if nonnegConstraint==0
       fprintf('Unconstrained linear least squares...\n'); 
        x = (pinv(C))*d; % non constraint
    else
        x = lsqnonneg(C,d); % nonnegativity constraint
        if sum(x<0)~=0
            warning('[%s] There are negative weights despite the constraint.',mfilename);
        end
    end
    fprintf('\t||C*x-d||^2=%1.1e\n',norm(C*x-d,2)^2);
        % 5) Extract the local and diffusive part
    if ~isempty(sn)
        Nxi = length(xik);
        Nr = length(sn_r); % No. of real poles
        Nup = length(sn_up); % No. of complex poles (upper plane)

        muk = zeros(Nxi,1);
        rn = zeros(Nr+2*Nup,1);
        sn = zeros(Nr+2*Nup,1);
                % poles associated with rn (same sorting)
        sn = [sn_r(:);sn_up(:);conj(sn_up(:))];
            % diffusive part (associated to xik)
        muk = x(1:Nxi);
            % local part - real poles (associated to sn)
        rn(1:Nr) = x(Nxi + (1:Nr));
            % local part - upper plane: Re + 1i*Im
        rn(Nr+(1:Nup)) = x(Nxi+Nr+(1:Nup)) + 1i*x(Nxi+Nr+Nup+(1:Nup));
            % local part - lower plane: Re - 1i*Im
        rn(Nr+Nup+(1:Nup)) = conj(rn(Nr+(1:Nup)));
    else
        muk = x;
        rn = [];
        sn = [];
    end
end


function W = computeWeightVector(w,om)
%computeWeightVector Weight vector used in the definition of the cost
%function (diffusive representation). The purpose of this vector is to
%give more weight to some pulsation bands over others.
% Inputs:
%   w (f_handle) weight function w(omega)
%   om (N) discrete pulsation (rad/s), N-1 bands
% Output:
%   W ((N-1)x1) W(i) = sqrt(/w(omega)domega) on the band n°i

    validateattributes(w,{'function_handle'},{'scalar'},1);
    validateattributes(om,{'numeric'},{'vector'},2);
    validateattributes(length(om),{'numeric'},{'scalar','>',1});

    W = zeros(length(om)-1,1);
        % accurate and costly version
%     for j=1:length(W)
%         W(j) = integral(w,om(j),om(j+1));
%     end
        % approximation using second-order Gauss-Lobatto quadrature
    %W = 0.5*(om(2:end)-om(1:(end-1))).*(w(om(2:end))+w(om(1:(end-1))));
        % third-order Gauss-Lobatto quadrature
    W = 0.5*(om(2:end)-om(1:(end-1))).*((1/3)*w(om(2:end))+(4/3)*w((om(2:end)+om(1:(end-1)))/2)+(1/3)*w(om(1:(end-1))));

    W = sqrt(W); % W -> W*W
end
