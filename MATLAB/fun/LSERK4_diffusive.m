function [y,phi,t,Nop] = LSERK4_diffusive(A,C,B,u,y0,phi0,xi,tf,s)
%LSERK4_diffusive LSERK4 explicit time-integration for the ODE:
%               y'=A*y + u(t) + C*phi(t) with y(0)=y0, phi(0)=phi0
%               phi' = -xi*phi + transpose(B)*y
% <N_it> are performed. <N_it+1> values of y will be computed, the first
%being y0, and the <N_it+1>-th one being y(tf).
% Inputs:
%   A (mxm)
%   C (mxm_phi)
%   B (mx1)
%   u (function handle) (1x1) -> (mx1)
%   y0 (mx1)
%   phi0 & xi (m_phix1)
%   tf (1x1) final time (starting time:t=0)
%   s (1x1) safety coeff (1= linear stability limit, for the COUPLED dyn.)
% Outputs:
%   y (mx(N_it+1)) y values
%   phi (m_phix(N_it+1)) phi values
%   t (1x(N_it+1)) times
%   Nop (1x1) Estimate of the number of operations performed

% Assess number of time-steps, based on the COUPLED dynamic
eignval = eig([A,C;kron(ones(length(xi),1),B'),-diag(xi)]);
if(sum((real(eignval)>=0))~=0)
    warning('Time continuous system is not linearly stable.');
end

% Time integration constants
dtmax = 4.4/max(abs(eignval)); % RK4 linear stability limit
dt = s*dtmax;
dt_f = rem(tf,dt); % Final time-step (for the last iteration)
N_it = floor(tf/dt); % No. of iter. to perform with <dt>
    if(N_it==0)
        dt = dt_f;
    else
        dt = [dt*ones(1,N_it),dt_f];
    end
N_it = N_it+1; % Total number of iterations to perform

    % Initialization
m = length(y0);
y = zeros(m,N_it+1); % Output: y(n) = y(t_{n-1})
y(:,1)=y0;
m_phi = length(phi0);
phi = zeros(m_phi,N_it+1);
phi(:,1)=phi0;
t = zeros(1,N_it+1);
t(2:(end-1)) = dt(1:(end-1)).*(1:(N_it-1)); t(end)=t(end-1)+dt_f;
Nop = N_it*(m+m_phi);
% Low Storage Explicit Runge-Kutta 4 time-integration
    % Coefficients definition (Carpenter and al., Fourth-order 2N-storage 
    %Runge-Kutta schemes, 1994)
a = [0,-567301805773/1357537059087,-2404267990393/2016746695238,-3550918686646/2091501179385,-1275806237668/842570457699];
b = [1432997174477/9575080441755, 5161836677717/13612068292357, 1720146321549/2090206949498, 3134564353537/4481467310338, 2277821191437/14882151754819];
c = [0,1432997174477/9575080441755,2526269341429/6820363962896,2006345519317/3224310063776,2802321613138/2924317926251];
fprintf('[LSERK4]: %d iter. & %d oper. to perform (dt=%1.2g dtmax=%1.2g).\n',N_it,Nop,s,dt(1));
reverseStr='';
time = cputime();
    % Operators
L1 = @(y,t)(A*y+u(t));
L2 = @(phi)(C*phi);
Aphi = @(dt)(diag(exp(-dt*xi))); % (NxixNxi)
Aq = @(dt)([(1-exp(-dt*xi))./xi]); % (NxixNq)
quad = @(phi,y,dt)(Aphi(dt)*phi+Aq(dt)*(transpose(B)*y));

    % Temporary values
k=zeros(m,1);
p=zeros(m,1);
r=zeros(m_phi,1);
for n=1:N_it
   msg = sprintf('[LSERK4_explicit]: Percent done: %3.1f', floor(100*n/N_it));
   fprintf([reverseStr, msg]);
   reverseStr = repmat(sprintf('\b'), 1, length(msg));
    p=y(:,n);
    r=phi(:,n);
        for i=1:5
           if i>=2
              r = quad(r,p,(c(i)-c(i-1))*dt(n));
           end
           k = a(i)*k+dt(n)*L1(p,t(n)+c(i)*dt(n))+dt(n)*L2(r);
           p = p + b(i)*k;
        end
    y(:,n+1)=p;
    phi(:,n+1)=quad(r,p,(1-c(5))*dt(n));
end
fprintf(' - Finished. (%2.2g s)\n',cputime()-time);

end

