function [A2N,B2N] = RK_get54Coeffs_2NStorage_Symb()
%getERK54Coeffs_2NStorage _Symb
%Return the coefficients of a 5-stage 4th order Runge-Kutta
%method suited for aeroacoustics (Carpenter et al. 1994).
% The scheme is given in 2N-storage format, with 24-digit precision.
% Output:
% A2N (5)     "A" coefficients
% B2N (5)     "B" coefficients

    warning('[getERK54Coeffs_Symb] Coefficients only given with 26-digit precision');
    % The scheme was originally defined in a 2N-storage format.
    % (Carpenter and al., Fourth-order 2N-storage Runge-Kutta schemes, 1994)
    A2N = [0,-sym(567301805773)/sym(1357537059087),-sym(2404267990393)/sym(2016746695238),-sym(3550918686646)/sym(2091501179385),-sym(1275806237668)/sym(842570457699)];
    B2N = [sym(1432997174477)/sym(9575080441755), sym(5161836677717)/sym(13612068292357), sym(1720146321549)/sym(2090206949498), sym(3134564353537)/sym(4481467310338), sym(2277821191437)/sym(14882151754819)];

end

