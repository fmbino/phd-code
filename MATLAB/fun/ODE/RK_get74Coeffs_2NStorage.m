function [A2N,B2N,C] = RK_get74Coeffs_2NStorage()
%getERK74Coeffs_2NStorage 
%Return the coefficients of a 7-stage 4th order Runge-Kutta (HALE-RK7)
%method suited for aeroacoustics (10.1016/j.jcp.2009.02.015).
% The scheme is given in 2N-storage format, with 12-digit precision.
% Output:
% A2N (5)     "A" coefficients
% B2N (5)     "B" coefficients
% C (1)       Stability region: |lambda*dt|<C
    warning('[getERK74Coeffs] Coefficients only known with 12-digit precision');
    % The scheme was originally defined in a 2N-storage format.
    % High-Accuracy Large-Step Explicit Runge-Kutta (HALE-RK)
    A2N = -[0,0.647900745934,2.704760863204,0.460080550118,0.500581787785,1.906532255913,1.450000000000];
    B2N = [0.117322146869,0.503270262127,0.233663281658,0.283419634625,0.540367414023,0.371499414620,0.136670099385 ];
    C = 4;
end

