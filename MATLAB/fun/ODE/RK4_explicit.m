function [y,t] = RK4_explicit(A,u,y0,tf,s)
%RK4_explicit RK4 explicit time-integration for the ODE:
%               y'=A*y + u(t) with y(0)=y0
% <N_it> are performed. <N_it+1> values of y will be computed, the first
%being y0, and the <N_it+1>-th one being y(tf).
% Inputs:
%   A (mxm)
%   u (function handle)
%   y0 (mx1)
%   tf (1x1) final time (starting time:t=0)
%   s (1x1) safety coeff (1= linear stability limit)
% Outputs:
%   y (mx(N_it+1)) y values
%   t (1x(N_it+1)) times

lambda = eig(A);
if(sum((real(lambda)>=0))~=0)
    warning('Time continuous system is not linearly stable.');
end

m = length(y0);

% Time integration constants
dtmax = 2.5/max(abs(eig(A))); % RK4 linear stability limit
dt = s*dtmax;
dt_f = rem(tf,dt); % Final time-step (for the last iteration)
N_it = floor(tf/dt); % No. of iter. to perform with <dt>
    if(N_it==0)
        dt = dt_f;
    else
        dt = [dt*ones(1,N_it),dt_f];
    end
N_it = N_it+1; % Total number of iterations to perform

    % Initialization
y = zeros(m,N_it+1); % Output: y(n) = y(t_{n-1})
t = zeros(1,N_it+1);
y(:,1)=y0;

t(2:(end-1)) = dt(1:(end-1)).*(1:(N_it-1)); t(end)=t(end-1)+dt_f; % line

% Runge-Kutta time-integration
fprintf('[RK4_explicit]: %d iter. to perform (dt=%1.2g dtmax=%1.2g).\n',N_it,s,dt(1));
reverseStr='';
time = cputime();
for n=1:N_it
   msg = sprintf('[RK4_explicit]: Percent done: %3.1f', floor(100*n/N_it));
   fprintf([reverseStr, msg]);
   reverseStr = repmat(sprintf('\b'), 1, length(msg));
    k1 = A*y(:,n) + u(t(n));
    k2 = A*(y(:,n)+(dt(n)/2)*k1) + u(t(n)+dt(n)/2);
    k3 = A*(y(:,n)+(dt(n)/2)*k2) + u(t(n)+dt(n)/2);
    k4 = A*(y(:,n)+dt(n)*k3) + u(t(n)+dt(n));
    y(:,n+1) = y(:,n) + (dt(n)/6)*(k1+2*k2+2*k3+k4);
end
fprintf(' - Finished. (%2.2g s)\n',cputime()-time);

end

