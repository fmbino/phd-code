function [ y,t ] = ODE_Integration(xi,u,y0,tf,dt_user,n)
%ODE_Integration Integration of an ODE.
%               y'=-diag(xi_1,..,xi_m)*y + u(t) with y(0)=y0
% <N_it> are performed. <N_it+1> values of y will be computed, the first
%being y0, and the <N_it+1>-th one being y(tf).
% The integration is performed through a n-point Lobatto quadrature
% (endpoints of the integration interval are included), for n>1.
% For n=1: a simple 1st-order quadrature.
% Inputs:
%   xi (mx1)
%   u function handle (nxp) -> (nxp)
%   y0 (mx1)
%   tf (1x1) final time 
%   dt (1x1) time step
%   n (1x1) number of point for the Lobatto quadrature (2 to 6)
% Output:
%   y (mx(N_it+1))
%   t (1x(N_it+1))

Nxi = length(xi);
dt_f = rem(tf,dt_user); % Final time-step (for the last iteration)
N_it = floor(tf/dt_user); % No. of iter. to perform with <dt>
    if(N_it==0)
        dt = dt_f;
    else
        dt = [dt_user*ones(1,N_it),dt_f];
    end
N_it = N_it+1; % Total number of iterations to perform
    

    % Initialisation
t = zeros(1,N_it+1);
t(2:(end-1)) = dt(1:(end-1)).*(1:(N_it-1)); t(end)=t(end-1)+dt_f; % line
y = zeros(Nxi,N_it+1);
y(:,1)=y0;
    
    % Definition of the quadrature to use
    %   points v in [-1,1] (column)
    %   weights w (column)
if (n==1)
    vk = [-1]; % choose freely
    wk = [1];
elseif (n==2) % 2 points
    vk = [-1;1];
    wk = [1;1];
elseif (n==3)
    vk = [-1;0;1];
    wk = [1/3;4/3;1/3];
elseif (n==4)
    vk = [-1;-1/sqrt(5);1/sqrt(5);1];
    wk = [1/6;5/6;5/6;1/6];
elseif (n==5)
    vk = [-1;-sqrt(21)/7;0;sqrt(21)/7;1];
    wk = [1/10;49/90;32/45;49/90;1/10];
elseif (n==6)
    vk = [-1;-sqrt((7+2*sqrt(7))/21);-sqrt((7-2*sqrt(7))/21);sqrt((7-2*sqrt(7))/21);sqrt((7+2*sqrt(7))/21);1];
    wk = [1/15;(14-sqrt(7))/30;(14+sqrt(7))/30;(14+sqrt(7))/30;(14-sqrt(7))/30;1/15];
end

    % Affine mapping
    % v in [-1,1] -> tau in [ta,tb]
    tau = @(v,ta,tb)(((tb-ta)/2)*(v+1)+ta);
    % Quadrature matrices
    Ay = @(dt)(diag(exp(-dt*xi))); % (NxixNxi)
    if (n==1)
        Au = @(dt)((1-exp(-dt*xi))./xi); % (Nxix1)
    else
        Au=@(dt)((dt/2)*exp((dt/2)*kron(-xi,transpose(1-vk)))); % (NxixNv)
    end
    
    fprintf('[ODE_Integration]: %d-point Lobatto quadrature. %d iter to perform (dt=%1.2e).\n',n,N_it,dt(1));
    reverseStr='';
    time=cputime();
    for n=1:N_it
        msg = sprintf('[ODE_Integration]: Percent done: %3.1f', floor(100*n/N_it));
        fprintf([reverseStr, msg]);
        reverseStr = repmat(sprintf('\b'), 1, length(msg));
        tauk = tau(vk,t(n),t(n+1));
        y(:,n+1) = Ay(dt(n))*y(:,n) + Au(dt(n))*(u(tauk).*wk);
    end
    fprintf(' - Finished. (%2.2g s)\n',cputime()-time);
end