function p = RK_ComputeOrder(A,b)
%RK_ComputeOrder Compute the order of a discrete Runge-Kutta method,
%defined by the matrix A and vector b (Butcher notations).
% Input:
%   A (nxn) Coefficients a_i,j
%   b (n)   Quadrature weights
% Output:
%   p (1)   Order of the discrete RK method (up to 4)

    % Check validity of the arrays A and b.
validateattributes(A,{'numeric'},{'2d','nonempty','real','finite','nonnan'},'','A');
validateattributes(b,{'numeric'},{'vector','nonempty','real','finite','nonnan','numel',length(A)},'','b');
b = b(:); % vector column
c = sum(A,2); % ci coefficients (Butcher notations)
n= size(A,1); % nu-stage

p =0;
    % Order p=1
R = sum(b)-1;
fprintf('[Order p=1] Error: %1.1e\n',R);
if R<=eps
    p=p+1;
end
    % Order p=2
R = dot(b(:),c(:)) - 1/2;
fprintf('[Order p=2] Error: %1.1e\n',R);
if R<eps
    p=p+1;
end
    % Order p=3
R = dot(b(:),c(:).^2) - 1/3;
R = R + dot(b(:),A*c(:)) - 1/6;
fprintf('[Order p=3] Error: %1.1e\n',R);
if R<eps
    p=p+1;
end
    % Order p=4
R = dot(b(:),c(:).^3) - 1/4;
R = R + dot(b(:),c(:).*(A*c(:))) - 1/8;
R = R + dot(b(:),(A*(c(:).^2))) - 1/12;
R = R + dot(b(:),A*(A*c(:))) - 1/24;
fprintf('[Order p=4] Error: %1.1e\n',R);
if R<eps
    p=p+1;
end
fprintf('Order of the discrete RK method=%d\n',p);
end

