function [A2N,B2N,C] = RK_get64Coeffs_2NStorage()
%getERK64Coeffs_2NStorage 
%Return the coefficients of a 6-stage 4th order Runge-Kutta (HALE-RK6)
%method suited for aeroacoustics (10.1016/j.jcp.2009.02.015).
% The scheme is given in 2N-storage format, with 12-digit precision.
% Output:
% A2N (5)     "A" coefficients
% B2N (5)     "B" coefficients
% C (1)       Stability region: |lambda*dt|<C
    warning('[getERK64Coeffs] Coefficients only known with 12-digit precision');
    % The scheme was originally defined in a 2N-storage format.
    % High-Accuracy Large-Step Explicit Runge-Kutta (HALE-RK)
    A2N = [0,-0.691750960670,-1.727127405211,-0.694890150986,-1.039942756197,-1.531977447611];
    B2N = [0.122000000000,0.477263056358,0.381941220320,0.447757195744,0.498614246822,0.186648570846 ];
    C = 4.4;
end