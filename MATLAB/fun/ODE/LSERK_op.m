function [q,t,Nop,T] = LSERK_op(L,q_0,tf,dt,A2N,B2N,varargin)
%LSERK_op Solve the system of ODEs:
%       q' = L(q,t) (dim. N, timestep dt)
% with initial condition on q (t=0).
% The system is solved through a Low-Storage Explicit Runge-Kutta method,
% defined by the coefficient A2N and B2N (2N-storage format).
% Note: A rule of thumb for the linear stability limit is:
%                       |dt * lambda_max| < C,
% where C is a constant which depends upon A2N and B2N.
% Remark: not much room for further optimisation. Breaking loops,
% removing function handles, or running display have not proven effective.
% Inputs:
%   L           function handle (Nx1,1x1) -> (Nx1)
%               ex: L(q,t) = A*q + u(t) (linear system with input u)
%   q_0 (Nx1) initial condition on q (t=0)
%   tf (1x1)     final time
%   dt (1x1)   time-step
%   A2N (Nrk)   
%   B2n (Nrk)
% Optional inputs: (name-value paris)
%   q_idx (Ni) indices of q to store (default: 1:N)
%   storeOnlyLast (boolean) store only the last iteration (default: 0)
% Outputs:
%   q1 (Nix(Nit+1)) computed values. q(:,i) is an approx. of q(t(i)).
%   t (1x(Nit+1)) corresponding instants : t(1) is 0, t(end) is tf.
%   Nop (1x1) Estimate of the number of operations performed.
%   T (1x1) Elapsed time

    % -- Validate arguments
validateattributes(q_0,{'numeric'},{'vector','nonempty','real','finite','nonnan'});
N = length(q_0);
validateattributes(L,{'function_handle'},{});
validateattributes(L(q_0,1),{'double'},{'vector','nonempty','size',[N,1]});    
validateattributes(tf,{'numeric'},{'scalar','nonempty','real','finite','nonnan'});
validateattributes(dt,{'numeric'},{'scalar','nonempty','real','finite','nonnan'});
        % Optional arguments
p = inputParser; p.FunctionName = mfilename;
addParameter(p,'q_idx',1:N,@(x)(validateattributes(x,{'numeric'},{'vector','nonempty','integer','finite','nonnan'})));
addParameter(p,'storeOnlyLast',0,@(x)(validateattributes(x,{'numeric'},{'scalar'})));
parse(p,varargin{:}); oA = p.Results; % structure which contains optional arguments
q_idx = oA.q_idx; storeOnlyLast=oA.storeOnlyLast;
Ni = length(q_idx);
    % Check validity of the arrays A and b.
    % A: lower triangular, null diagonal
validateattributes(A2N,{'numeric'},{'vector','nonempty','real','finite','nonnan'});
Nrk = length(A2N);
validateattributes(B2N,{'numeric'},{'vector','nonempty','real','finite','nonnan','numel',Nrk});
[AERK,~] = RK_convertCoeffs(A2N,B2N);
c = sum(AERK,2); % ci coefficients (Butcher notations)
clear AERK
fprintf('[LSERK]: %d-stage explicit low-storage Runge-Kutta.\n',Nrk);
fprintf('[LSERK]: DoF: %d, stored DoF: %d (%3.3g%%)\n',N,Ni,100*(Ni/N));
if storeOnlyLast
    fprintf('[LSERK]: Store only last iteration: true\n');
else
    fprintf('[LSERK]: Store only last iteration: false\n');
end
dt_f = rem(tf,dt); % Final time-step (for the last iteration)
N_it = floor(tf/dt); % No. of iter. to perform with <dt>
    if(N_it==0)
        dt = dt_f;
    else
        dt = [dt*ones(1,N_it),dt_f];
    end
N_it = N_it+1; % Total number of iterations to perform
    % Estimate of the number of operations
Nop = N_it*N;
fprintf('[LSERK]: %d iter. & %d oper. to perform (dt=%1.2g).\n',N_it,Nop,dt(1));
    % Perform the LSERK iteration
if storeOnlyLast
    q=q_0(q_idx); % Last computed values
    jq = ones(1,N_it);
else
    q = zeros(Ni,N_it+1); % Output: q(n) = q(t_{n-1})
    q(:,1)=q_0(q_idx);
    jq = 2:(N_it+1); % jq(n) = column index for q(t_{n})
end

t = zeros(1,N_it+1);
t(2:(end-1)) = dt(1:(end-1)).*(1:(N_it-1)); t(end)=t(end-1)+dt_f;

reverseStr='';
time = cputime();
tic();
elapsed = toc();
msg = sprintf('[LSERK]: Starting...');
fprintf([reverseStr, msg]);
reverseStr = repmat(sprintf('\b'), 1, length(msg));
    % Temporary values
k=zeros(N,1);
p=q_0;
for n=1:N_it
    if mod(n,floor(N_it/100))==0
       elapsed = toc();
       msg = sprintf('[LSERK]: Max:%4.2e. Percent done: %3.1f. Elapsed: %s. Remaining: %s',full(max(p(q_idx))),floor(100*n/N_it),secs2hms(elapsed),secs2hms(ceil((N_it-n)/n)*elapsed));
       fprintf([reverseStr, msg]);
       reverseStr = repmat(sprintf('\b'), 1, length(msg));
    end
    for i=1:Nrk
        k = A2N(i)*k + dt(n)*L(p,t(n)+c(i)*dt(n));
        p = p + B2N(i)*k;
    end
    %q(:,n+1)=p(q_idx);
    q(:,jq(n))=p(q_idx);
end
    T = toc()-time;
    fprintf(' - Finished. (%2.2g s)\n',T);
    
    if storeOnlyLast
        t = t(end);
    end
end


function time_string=secs2hms(time_in_secs)
%SECS2HMS - converts a time in seconds to a string giving the time in hours, minutes and second
%Usage TIMESTRING = SECS2HMS(TIME)]);
%Example 1: >> secs2hms(7261)
%>> ans = 2 hours, 1 min, 1.0 sec
%Example 2: >> tic; pause(61); disp(['program took ' secs2hms(toc)]);
%>> program took 1 min, 1.0 secs
%Author: Simon Robinson, MATLAB File exchange
    time_string='';
    nhours = 0;
    nmins = 0;
    if time_in_secs >= 3600
        nhours = floor(time_in_secs/3600);
        if nhours > 1
            hour_string = ' hours, ';
        else
            hour_string = ' hour, ';
        end
        time_string = [num2str(nhours) hour_string];
    end
    if time_in_secs >= 60
        nmins = floor((time_in_secs - 3600*nhours)/60);
        if nmins > 1
            minute_string = ' mins, ';
        else
            minute_string = ' min, ';
        end
        time_string = [time_string num2str(nmins) minute_string];
    end
    nsecs = time_in_secs - 3600*nhours - 60*nmins;
    time_string = [time_string sprintf('%2.1f', nsecs) ' secs'];
end