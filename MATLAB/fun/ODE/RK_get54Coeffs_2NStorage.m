function [A2N,B2N,C] = RK_get54Coeffs_2NStorage()
%getERK54Coeffs_2NStorage 
%Return the coefficients of a 5-stage 4th order Runge-Kutta
%method suited for aeroacoustics (Carpenter et al. 1994).
% The scheme is given in 2N-storage format, with 24-digit precision.
% Output:
% A2N (5)     "A" coefficients
% B2N (5)     "B" coefficients
% C (1)       Stability region: |lambda*dt|<C
    warning('[getERK54Coeffs] Coefficients only known with 26-digit precision');
    % The scheme was originally defined in a 2N-storage format.
    % (Carpenter and al., Fourth-order 2N-storage Runge-Kutta schemes, 1994)
    A2N = [0,-567301805773/1357537059087,-2404267990393/2016746695238,-3550918686646/2091501179385,-1275806237668/842570457699];
    B2N = [1432997174477/9575080441755, 5161836677717/13612068292357, 1720146321549/2090206949498, 3134564353537/4481467310338, 2277821191437/14882151754819];
    C = 4.4;
end

