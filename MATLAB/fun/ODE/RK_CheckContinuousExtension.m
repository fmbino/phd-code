function [ output_args ] = RK_CheckContinuousExtension(A,b,Bpol)
%RK_CheckContinuousExtension Inspect the continuous extension of a discrete
%Runge-Kutta method. The continuous extension is defined by n polynomials.
% See (Bellen & Zennaro, 2003) Chap.5.
% Input:
%   A (nxn) Coefficients a_i,j
%   b (n)   Quadrature weights
%   Bpol (nx(d+1)) n polynomial of degree d.
%           [bi(theta)] i in [1,n] (degree decreases left->right)

        % Check validity of the arrays A and b.
    validateattributes(A,{'numeric'},{'2d','nonempty','real','finite','nonnan'},'','A');
    validateattributes(b,{'numeric'},{'vector','nonempty','real','finite','nonnan','numel',length(A)},'','b');
    validateattributes(Bpol,{'numeric'},{'2d','nonempty','real','finite','nonnan','nrows',size(A,1)},'','Bpol');
    b = b(:); % vector column
    c = sum(A,2); % ci coefficients (Butcher notations)
    nu= size(A,1); % nu-stage
    d = size(Bpol,2)-1; % Max. polynomial degree
    
        % First continuity condition
    if sum(Bpol(:,end)==0)~=nu
        fprintf('[Continuity condition bi(0)=0] Failed.\n');
    else
        fprintf('[Continuity condition bi(0)=0] OK.\n');
    end
    Bpol(:,end) = [];
    Bpol = fliplr(Bpol);
        % Second continuity condition
    R = sum(Bpol,2)-b(:);
    for i=1:length(b)
       fprintf('[Continuity Condition bi(1)=bi] b_%d: Error: %1.1e\n',i,double(R(i)));
    end
        % Uniform order q=1
    R = sum(Bpol,1);
    for j=1:size(Bpol,2)
        R(j) = R(j) - (j==1);
       fprintf('[Uniform order Condition q=1] degree %d: Error: %1.1e\n',j,double(R(j)));
    end
        % Uniform order q=2
    R = transpose(c)*Bpol;
    for j=1:size(Bpol,2)
        R(j) = R(j) - (1/2)*(j==2);
       fprintf('[Uniform order Condition q=2] degree %d: Error: %1.1e\n',j,double(R(j)));
    end
        % Uniform order q=3
    R = transpose(c.^2)*Bpol;
    for j=1:size(Bpol,2)
        R(j) = R(j) - (1/3)*(j==3);
       fprintf('[Uniform order Condition q=3(1)] degree %d: Error: %1.1e\n',j,double(R(j)));
    end
    R = transpose(A*c)*Bpol;
    for j=1:size(Bpol,2)
        R(j) = R(j) - (1/6)*(j==3);
       fprintf('[Uniform order Condition q=3(2)] degree %d: Error: %1.1e\n',j,double(R(j)));
    end
end