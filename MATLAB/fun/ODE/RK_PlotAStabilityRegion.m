function RK_PlotAStabilityRegion(gh,A,b,x,y)
%RK_PlotAStabilityRegion Plot the A-stability region of a discrete 
%Runge-Kutta method, defined by the matrix A and vector b 
%(Butcher notations).
% Based on (Bellen & Zennaro, 2003) Chap.8.
% Input:
%   gh graphic handle
%   A (nxn) Coefficients a_i,j
%   b (n)   Quadrature weights
%   x (2)   Plot range (x-axis)
%   y (2)   Plot range (y-axis)

    if(~ishghandle(gh))
        error('<gh> should be a graphic handle: use gh=figure');
    else
        figure(gh); % Focus
    end
    validateattributes(x,{'numeric'},{'vector','nonempty','real','finite','nonnan','numel',2},'','x');
    validateattributes(y,{'numeric'},{'vector','nonempty','real','finite','nonnan','numel',2},'','y');
        % Check validity of the arrays A and b.
    validateattributes(A,{'numeric'},{'2d','nonempty','real','finite','nonnan'},'','A');
    validateattributes(b,{'numeric'},{'vector','nonempty','real','finite','nonnan','numel',length(A)},'','b');
    b = b(:); % vector column
    c = sum(A,2); % ci coefficients (Butcher notations)
    n= size(A,1); % M-stage explicit Runge-Kutta method

    nu = length(b);
    e = ones(nu,1);
        % A-stability function (Bellen & Zennaro, 2003, chap.8)
    R = @(z)(det(eye(nu)-z*A+z*kron(e(:),transpose(b(:))))./det(eye(nu)-z*A));
        % Compute A-stability region
    N = 1e2;
    x = linspace(min(x),max(x),N);
    y = linspace(min(y),max(y),N);
    [X,Y] = meshgrid(x,y);
    Z = zeros(N,N);
    for i=1:length(x)
        for j=1:length(y)
            Z(i,j)=abs(R(X(i,j)+1i*Y(i,j)));
        end
    end
    %Z = Z<1;
        % Plot 
    surf(X,Y,Z);
    xlabel('Re(lambda*dt)');
    ylabel('Im(lambda*dt)');
    title(sprintf('A-stability region (%d-stage RK)',nu));
    zlim([0,1]); % A-stability region <-> Z<1
    view([0,90]);
end

