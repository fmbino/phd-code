function y_ex = ODE1_ExactSolution(xik,t,u,y0)
%ODE1_ExactSolution Exact solution of the ODE:
%       y'(t)=-Diag[xik]*y(t) + u(t)
% Higly uneficient routine (loop + costly quadrature)
% Inputs:
%   xik (1xm)
%   t (1xn)
%   u (function handle)
%   y0 (mx1) initial condition
% Outputs:
%   y_ex (mxn) exact solution at times <t>

    m = length(xik);
    n = length(t);
    f = @(tau,t)( kron(ones(length(xik),1),u(tau)).*exp(kron(transpose(xik),tau-t)));
    g = @(t)(integral(@(tau)(f(tau,t)),0,t,'ArrayValued',true));

    G = zeros(m,n); % Integral part of the exact solution
    for i=1:length(t)
        G(:,i) = g(t(i));
    end
        % Add the homogenous solution
    y_ex = kron(y0,ones(1,length(t))).*exp(-kron(transpose(xik),t)) + G;

end

