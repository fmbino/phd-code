function dlmwriteHeader(filename,header,data,precision)
%dlmwriteHeader Wrapper around MATLAB function dlmwrite. Write CSV file
%with the specified header and precision.
% Ex: dlmwriteHeader('Analysis-Frac-Model_DiffPart.csv','Freq,Re,Im',[w(:),real(Z(:)),imag(Z(:))],'%1.6e');
% Inputs:
%   filename (string)
%   header (string) comma-separated (ex: 'Field A,Field B,Field C')
%   data (NxP) matrix to write
%   precision (string) fprintf format (ex: '%1.6e')

    % Write header
dlmwrite(filename,header,'Delimiter','');
    % Write data
dlmwrite(filename,data,'-append','Delimiter',',','newline','unix','precision',precision);
end

