function [xsim,zref,time]=comparDIF(lam, mup, x0, T, Nt, xim,xiM,K)
% K est le nombre de xi_k, répartis géométriquement entre xim et xiM
% T est l'horizon temporel de simulation
% x0 la condition initiale
% et classiquement, je me donne xi_min, xi_max sur K points dans l'espace
% des xi : les mu_k sont déterminés de façon optimale de base.
%
% EXAMPLE : 
% clear all
%
%pour observer la convergence ? simu sur T=8, pour K=3, puis 5 puis 10 variables diffusives !
%comparDIF(-1,-2,1,8,50, 1e-2,1e2,3);
%comparDIF(-1,-2,1,8,50, 1e-2,1e2,5);
%comparDIF(-1,-2,1,8,50, 1e-2,1e2,10);
%
%là où les emmerdes commencent c'est à K=10, quand tu augmentes T = 8 puis 10 puis 15... 
% mais sur la REFERENCE, pas du tout sur la simu diffusive OUF !!!!!!!!!!!!
%comparDIF(-1,-2,1,8,50, 1e-2,1e2,10);
%comparDIF(-1,-2,1,10,50, 1e-2,1e2,10);
%comparDIF(-1,-2,1,15,50, 1e-2,1e2,10);
%=> vers T*=9, ma sol de REFERENCE se met à DECROCHER sans qu'on sache pourquoi ?
%
% écrit et testé par D. Matignon denis.matignon@isae.fr
% 10/04/2015
%
% utilise le programme précédent : caputo1 en date du 18/07/2013
%
% PROBLEMES DETECTES : only sur l'appel à solEDF pour les temps LONGS ?!!!
% pour le moment, ne sait pas gérer les vp COMPLEXES, dommage...
% c'est un pb purement Matlab, apparemment : to be continued !!!!!!!!


S=lam+mup;
P=lam*mup;

K;
alpha=1/2 %(ONLY, sinon PAS de solution analytique !)
beta=1-alpha;

rxi=(xiM/xim)^(1/(K-1));

xi= xim*rxi.^(0:K-1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% PARTIE OPTIMISATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% partie détermination des mu_k optimaux
wm=xim*10; %xim/10; ceci, que je croyais mieux donne des mu négatifs !!
wM=xiM/10; %xiM*10; ceci, que je croyais mieux donne des mu négatifs !!
L=2*K;
rw=(wM/wm)^(1/(L-1));
w=wm*rw.^(0:L-1);

% QUESTION : et pourquoi je ne prendrais pas une marge de sécurité avec
% une évaluation sur s=-eps + iw ? ou alors s=+eps + iw ? Réfléchir... 
% bien forcer la réalité du mu, mais à part cela... cela permettrait
% peut-être d'améliorer le conditionnement de la bête, , non ?!
b=(i*w').^(-beta);
size(b);


for l=1:L
   for k=1:K
      A(l,k)=1/(i*w(l)+xi(k)) ;
   end
end
size(A);
% partie imposer la réalité de fait, en augmentant le système, SANS ajouter
% de contraintes de réalité explicitement !
% on DIOT pouvoir faire autrement, SANS doubler la taille : voir mes notes!
Ab=[A ; conj(A)];
bb=[b ; conj(b)];

mu=(Ab'*Ab)\Ab'*bb;
size(mu);
% je DECIDE de FORCER la réalité de mu... just in case, pour la suite !!!
mu= real(mu);
Nmuneg=(sum(mu<0))


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% PARTIE SIMULATION %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% partie CREATION des realisations diffusives
As=-diag(xi);
bs=ones(K,1);
cs=mu.';
ds=sum(mu);

mysimIbeta=ss(As,bs,cs,0); % encode l'INTEGRATEUR FRACTIONNAIRE d'ordre 1/2
mysimDalpha=ss(As,bs,cs*As,ds); % encode le DERIVATEUR FRACTIONNAIRE d'ordre 1/2

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% PARTIE COMPARAISON %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T=5;
% Nt=50;
%t=linspace(0,T,Nt);

At=[-P+S*ds S*cs*As ; bs  As];
Phi0=x0*1./(xi'); %implicitement v_0=x0
Z0=[x0;Phi0];
bt=zeros(K+1,1);
ct=[1 zeros(1,K)];

mysimTot=ss(At,bt,ct,0);
[xsim,time,Zsim] = initial(mysimTot,Z0,T);
plot(time, xsim)

x1=0 % on force la régularité, ou d1/2x(0)=0
zref= solEDF(lam,mup,x0,x1,time);

hold on
plot(time,zref,'r--')
hold off