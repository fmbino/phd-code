function [y,t]= caputo1(alpha,v0, xim,xiM,K)
%caputo1 Comparaison entre caputo et distribution.
% Inputs:
%   alpha (1x1) order
%   v0 ()
%   xim (1x1) xi min
%   xiM (1x1) xi max
%   K (1x1) number of xi_k, (geometric spacing btw xim and xiM)
%
% EXAMPLE : 
% clear all
% caputo1(0.5,1, 1e-2,1e2, 5)
% caputo1(0.5,1, 1e-2,1e2, 10)
% caputo1(0.5,1, 1e-2,1e2, 15)
% caputo1(0.5,1, 1e-2,1e2, 20)
%
% sur les figures 1 et 2, on voit très bien l'amélioration de l'approx en
% fonction de l'ordre K = nombre de xi_k
% dans la figure 3, réponse échelon de l'INTEGRATEUR fractionnaire, en t^(1-alpha)
% dans la figure 4, le subplot 1 doit indiquer la dérivée de RIEMANN-LIOUVILLE
% dans la figure 4, le subplot 2 doit indiquer la dérivée de CAPUTO
%
% écrit et testé par D. Matignon denis.matignon@isae.fr
% 17/07/2013
%
beta=1-alpha;

rxi=(xiM/xim)^(1/(K-1));

xi= xim*rxi.^(0:K-1);

%%%%%%%%%%%%%%% PARTIE OPTIMISATION
% partie détermination des mu_k optimaux
wm=xim*10; %xim/10; ceci, que je croyais mieux donne des mu négatifs !!
wM=xiM/10; %xiM*10; ceci, que je croyais mieux donne des mu négatifs !!
L=2*K;
rw=(wM/wm)^(1/(L-1));
w=wm*rw.^(0:L-1);


% QUESTION : et pourquoi je ne prendrais pas une marge de sécurité avec
% une évaluation sur s=-eps + iw ? ou alors s=+eps + iw ? Réfléchir... 
% bien forcer la réalité du mu, mais à part cela... cela permettrait
% peut-être d'améliorer le conditionnement de la bête, , non ?!
b=(i*w').^(-beta);
size(b);

for l=1:L
   for k=1:K
      A(l,k)=1/(i*w(l)+xi(k)) ;
   end
end
size(A);
% partie imposer la réalité de fait, en augmentant le système, SANS ajouter
% de contraintes de réalité explicitement !
% on DIOT pouvoir faire autrement, SANS doubler la taille : voir mes notes!
Ab=[A ; conj(A)];
bb=[b ; conj(b)];

mu=(Ab'*Ab)\Ab'*bb;
size(mu);
% je DECIDE de FORCER la réalité de mu... just in case, pour la suite !!!
mu= real(mu);
Nmuneg=(sum(mu<0))



%%%%%%%%%%%%%%% PARTIE SIMULATION %%%%%%%%%%%%%%%%
% partie VERIFICATION de l'intégrateur
As=-diag(xi);
bs=ones(K,1);
cs=mu.';

ds=sum(mu);

figure(1)
mysimIbeta=ss(As,bs,cs,0);
%grid 
bode(mysimIbeta)
title('Integrateur fractionnaire.');
grid on
hold on

%subplot(211)
%loglog(w,(i*w).^(-beta))

figure(2)
mysimDalpha=ss(As,bs,cs*As,ds);
%grid
bode(mysimDalpha)
title('Derivateur fractionnaire.');
grid on
hold on


figure(3)
step(mysimIbeta);
title('Réponse échelon de l"INTEGRATEUR fractionnaire, en t^{(1-\alpha)}')

figure(4)
subplot(211)
[Ya,T]=step(mysimDalpha);

v=ones(size(T));
[Yalsim]=lsim(mysimDalpha,v,T);
hold on
plot(T,Yalsim,'b')
title('Réponse échelon du DERIVATEUR fractionnaire de Riemann-Liouville, en 1/t^\alpha')

subplot(212)
size(xi')
X0=1./(xi'); %implicitement v_0=1
size(X0)
[Yalsim0]=lsim(mysimDalpha,v,T,X0);
plot(T,Yalsim0,'r')
title('Réponse échelon du DERIVATEUR fractionnaire de Caputo, = O ?')
hold off