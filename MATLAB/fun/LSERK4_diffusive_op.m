function [q,phi,t,Nop,T] = LSERK4_diffusive_op(L,M,B,q0,phi0,xi,tf,dt)
%LSERK4_diffusive LSERK4 explicit time-integration for the ODE:
%               q'= L(q,t) + M(phi(t)) (dim N) (1)
% where the "diffusive variables", stored in the vector phi, are subject to
% a decoupled system of Nxi ODEs:
%               phi'=-diag(xi)*phi + transpose(B)*y (2)
% with initial condition q(0)=q0, phi(0)=phi0.
% The system (1) is solved through a Low-Storage Explicit Runge-Kutta,
% method,the order of which is 4. The system (2) is advanced in time
% through 1st-order quadrature : 1 quadrature is performed for each
% sub-iteration of the LSERK4.
% Note: The linear stability limit is roughly that of q'=L(q,t) with the
% LSERK4 method.
% Inputs:
%   L           function handle (Nx1,1x1) -> (Nx1)
%               ex: L(q,t) = A*q + u(t) (linear system with input u)
%   M           function handle (Nxix1,1x1) -> (Nx1)
%   B (Nx1)     input matrix
%   q0 (Nx1)    initial condition on q
%   phi0        initial condition on phi
%   xi (Nxix1)  "pole" associated to each "diffusive variable".
%   tf (1x1)    final time (starting time:t=0)
%   dt (1x1)    time-step.
% Outputs:
%   q1 (Nx(Nit+1))      computed values. q(:,i) is an approx. of q(t(i))
%   phi (Nxix(N_it+1))  phi values
%   t (1x(Nit+1))       corresponding instants : t(1) is 0, t(end) is tf
%   Nop (1x1)           Estimate of the number of operations performed
%   T (1x1)             Elapsed time

dt_f = rem(tf,dt); % Final time-step (for the last iteration)
N_it = floor(tf/dt); % No. of iter. to perform with <dt>
    if(N_it==0)
        dt = dt_f;
    else
        dt = [dt*ones(1,N_it),dt_f];
    end
N_it = N_it+1; % Total number of iterations to perform

    % Initialization
N = length(q0);
q = zeros(N,N_it+1); % Output: y(n) = y(t_{n-1})
q(:,1)=q0;
Nxi = length(phi0);
phi = zeros(Nxi,N_it+1);
phi(:,1)=phi0;
t = zeros(1,N_it+1);
t(2:(end-1)) = dt(1:(end-1)).*(1:(N_it-1)); t(end)=t(end-1)+dt_f;
Nop = N_it*(N+Nxi);
% Low Storage Explicit Runge-Kutta 4 time-integration
    % Coefficients definition (Carpenter and al., Fourth-order 2N-storage 
    %Runge-Kutta schemes, 1994)
a = [0,-567301805773/1357537059087,-2404267990393/2016746695238,-3550918686646/2091501179385,-1275806237668/842570457699];
b = [1432997174477/9575080441755, 5161836677717/13612068292357, 1720146321549/2090206949498, 3134564353537/4481467310338, 2277821191437/14882151754819];
c = [0,1432997174477/9575080441755,2526269341429/6820363962896,2006345519317/3224310063776,2802321613138/2924317926251];
fprintf('[LSERK4]: %d iter. & %d oper. to perform (dt=%1.2g).\n',N_it,Nop,dt(1));
reverseStr='';
time = cputime();
    % Operators
Aphi = @(dt)(diag(exp(-dt*xi))); % (NxixNxi)
Aq = @(dt)([(1-exp(-dt*xi))./xi]); % (NxixNq)
quad = @(phi,y,dt)(Aphi(dt)*phi+Aq(dt)*(transpose(B)*y));

    % Temporary values
k=zeros(N,1);
p=zeros(N,1);
r=zeros(Nxi,1);
for n=1:N_it
   msg = sprintf('[LSERK4_diffusive]: Percent done: %3.1f', floor(100*n/N_it));
   fprintf([reverseStr, msg]);
   reverseStr = repmat(sprintf('\b'), 1, length(msg));
    p=q(:,n);
    r=phi(:,n);
        for i=1:5
           if i>=2
              r = quad(r,p,(c(i)-c(i-1))*dt(n));
           end
           k = a(i)*k+dt(n)*L(p,t(n)+c(i)*dt(n))+dt(n)*M(r);
           p = p + b(i)*k;
        end
    q(:,n+1)=p;
    phi(:,n+1)=quad(r,p,(1-c(5))*dt(n));
end
    T = cputime()-time;
    fprintf(' - Finished. (%2.2g s)\n',T);
end

