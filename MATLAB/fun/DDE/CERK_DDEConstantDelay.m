function [t,y] = CERK_DDEConstantDelay(f,tau,y0,tf,dt,A,b,bpol,p,I)
%CERK_DDEConstantDelay Compute a numerical approximation of the solution of
%the constant-delay DDE:
%   y'(t) = f(t,y(t),y(t-tau)) for t in [0,tf]
%   y(t) = y0 for t in [-tau,0]
% The approximation is computed using a discrete explicit Runge Kutta
% method of order p, defined by A, b, and its continuous extension,
% defined by a family of polynomials bi(theta) with 0<theta<1 
% For a target order p, the first p discontinuity points (i*tau) are
% included in the mesh (i>0).
%(Notations from Bellen et Zennaro, 2003).
% Inputs:
%   f           function handle f(t,y,z)
%   tau (1)     constant delay
%   y0 (N)      initial condition
%   tf (1)      final time
%   dt (1)      time-step
%   A (MxM)     Lower triangular matrix with null diagonal
%               (In Butcher notations: A(i,j) = a i,j)
%   b (M)       Vector of quadrature weights (In Butcher notations: bi)
%   bpol (MxP)  M interpolation polynomials bi(theta) 
%               (polyval convention on each row) 
%   p (1)       Target order of the continuous Runge-Kutta method.
%   I (Q)       Indices of y to which the delay is applied.
%               ex: 1:N (i.e. delay applied to each component)
%                   1   (i.e. delay applied to the first component only).
% Outputs:
%   t (1x(Nit+1)) corresponding instants : t(1) is 0, t(end) is tf.
%   y (Nx(Nit+1)) computed values. y(:,i) is an approx. of y(t(i)).

validateattributes(tau,{'numeric'},{'scalar','real','>',0,'finite'},'','tau');
validateattributes(y0,{'numeric'},{'vector','real','finite'},'','tau');
validateattributes(f,{'function_handle'},{},'','f');
validateattributes(f(1,ones(length(y0),1),ones(length(y0),1)),{'numeric'},{'vector','numel',length(y0)},'','f');
validateattributes(tf,{'numeric'},{'scalar','real','>',0,'finite'},'','tf');
validateattributes(dt,{'numeric'},{'scalar','real','>',0,'finite'},'','dt');
validateattributes(p,{'numeric'},{'scalar','real','>',0,'finite'},'','p');
validateattributes(I,{'numeric'},{'vector','integer','nonnegative'},'','I');
if length(I)>length(y0)
   error('[CERK]: I contains %d indices, but y0 is only %d elements long.\n',length(I),length(y0)); 
end
    % Check validity of the arrays A and b.
    % A: lower triangular, null diagonal
validateattributes(A,{'numeric'},{'2d','nonempty','real','finite','nonnan'},'','A');
validateattributes(b,{'numeric'},{'vector','nonempty','real','finite','nonnan','numel',length(A)},'','b');
b = b(:); % vector column
c = sum(A,2); % ci coefficients (Butcher notations)
M = size(A,1); % M-stage explicit Runge-Kutta method
fprintf('[CERK]: Discrete method is a %d-stage explicit Runge-Kutta (order=%d).\n',M,p);
validateattributes(bpol,{'numeric'},{'2d','nonempty','real','finite','nonnan','nrows',M},'','bpol');
fprintf('[CERK]: %d interpolation polynomial of degree %d: b_i on [0,1].\n',M,size(bpol,2));
    % Interpolation polynomials
    % Check that the continuity conditions are verified
for i=1:M % for each polynomial
    if abs(polyval(bpol(i,:),0)-0)>eps(0)
        warning('[CERK] Interpolation polynomial |b%d(0)-0|=%1.2e!=0.',i,polyval(bpol(i,:),0));
    elseif abs(polyval(bpol(i,:),1)-b(i))>eps(b(i))
        warning('[CERK] Interpolation polynomial |b%d(1)-b%d|=%1.2e!=0',i,i,abs(polyval(bpol(i,:),1)-b(i)));
    end
end
N = length(y0);

% Discontinuity points to be included in the mesh
if p>0
    t_d = tau*(1:p);
else
    t_d = [];
end

fprintf('[CERK]: Roughly %d iter. to perform (dt=%1.2g).\n',floor(tf/dt),dt);
fprintf('[CERK]: %d discontinuity points are included (i*tau) i>0.\n',length(t_d));
    % Time-integration main loop
%   t   times
t = 0;
%   dt  time-steps
dt_max = dt;
dt = zeros(0);
%   K   array which stores the valued of 'k' needed for interpolations
K = zeros(0);
%   y
y = y0(:);
time = cputime();
reverseStr='';
while t(end)<tf
   msg = sprintf('[CERK]: Current time: %3.1f (%d%%)', t(end),100*(t(end)/tf));
   %fprintf([reverseStr, msg]);
   %reverseStr = repmat(sprintf('\b'), 1, length(msg));
        % update current time-step dt
    dt(end+1) = getTimeStep(t(end),dt_max,tf,t_d);
    %fprintf('-- Time step for next iteration: dt=%1.4e\n',dt(end));
        % compute M interpolated states y(t_in)
    if length(t)==1 % first iteration
        y_in = kron(y0(I),ones(1,length(c)));
    else
            % last size(K,2)/M elements of t and y (+ the last)
        [y_in,K] = getInterpolatedState(y(I,(end-size(K,2)/M):(end)),K,t((end-size(K,2)/M):(end)),t(end) + dt(end)*c-tau,bpol);
    end
        % compute the M vectors k
    k=zeros(N,M); % beware: k!=K
    yd = zeros(N,1);
    for i=1:M % M stage
        yd(I) = y_in(:,i);
        k(:,i) = f(t(end)+c(i)*dt(end),y(:,end)+dt(end)*k*(A(i,:)'),yd);       
    end
        % advance in time
    y(:,end+1) = y(:,end) + dt(end)*(k*b);
    K(:,end+(1:M)) = k(I,:);
    t(end+1) = t(end) + dt(end);
    %fprintf('\tAdvance: y(%1.4e)=%1.4e\n',t(end),y(end));
end
    T = cputime()-time;
    fprintf(' - Finished. (%2.2g s)\n',T);
end

function dt = getTimeStep(t0,dt_max,tf,t_d)
%getTimeStep Compute the appropriate time-step dt that verifies the given
%constraints :  t+dt <=tf
%               t+dt = t_d (if possible)
% Inputs:
% t0 (1)         Current instant
% dt_max (1)    Target time-step (upper bound)
% tf (1)        Final instant
% t_d (P)       Instants to be included

        % keep only the first discontinuity points in [t0,tf]
    t_d = t_d(t_d<tf);
    t_d = t_d(t_d>t0);
    if ~isempty(t_d)
        t_d = t_d(1);
    end
        % compute dt
    t = t0+dt_max;
    if ~isempty(t_d) && abs(t-t_d)>=eps(t_d)
        dt = min(t_d-t0,dt_max);
    elseif abs(t-tf)>=eps(tf)
        dt = min(tf-t0,dt_max);
    else
        dt = dt_max;
    end
end

function [y_in,K] = getInterpolatedState(y,K,t,t_in,bpol)
%getInterpolatedState Compute interpolated state at t=t_i, i.e. y(t_in).
% Inputs:
%   y (Nx(P+1))     Computed values
%   K (Nx(P*M)) Stored values of "k" (M values for each corresponding t(i))  
%   t (1x(P+1))     Time instants (mesh)
%   t_in (M)    Interpolation times t_in
% Outputs:
%   y_in (NxM)  Interpolated values y(t_in)
%   K (Nx(S*M)) K with the oldest P-S "k" removed.

    N = size(y,1);
    P = size(y,2)-1;
    M = length(t_in);
    if  length(t)~=(P+1)
        error('[CRK,getInterpolatedState]: t has incorrect dimensions.\n')
    end
    if size(K,1)~=N || rem(size(K,2),M)~=0 || rem(size(K,2),P)~=0 
       error('[CRK,getInterpolatedState]: K has incorrect dimensions.\n'); 
    end    
    y_in = zeros(N,M);
    %fprintf('\t-- Start interpolation (find m,theta)\n');
    [m,theta_m,dt_m] = getInterpolatedStateParam(t,t_in);
%     for i=1:M
%         fprintf('\t t_in = %1.4e\n',t_in(i));
%         fprintf('\t With m=%d, theta=%1.4e, dt=%1.4e, error: %1.4e\n',m(i),theta_m(i),dt_m(i),abs(t_in(i)-t(m(i))-theta_m(i)*dt_m(i)));
%     end
        % Polynomial matrix b_i(theta_j)
    Bpol = getInterpolationMatrix(bpol,theta_m);
        % Stored K
    y_in=zeros(N,M);
%    fprintf('\t-- Compute interpolation\n');
    for i=1:M % M stage
        if m(i)>size(K,2)
            error('[CRK,getInterpolatedState]: K stores %d instants, but instant n°%d has been asked.\n',size(K,2),m(i));
        end
        y_in(:,i) = y(:,m(i)) + dt_m(i)*(K(:,(m(i)-1)*M+(1:(M)))*Bpol(:,i));
%        fprintf('\tInterpolation: y(%1.4e)=%1.4e\n',t(m(i))+theta_m(i)*dt_m(i),y_in(1,i));
    end
    m_min = min((m-1)*M);
    if m_min>1
        K(:,1:m_min) = []; % remove first m_min "k"
    end
end

function [m,theta_m,dt_m] = getInterpolatedStateParam(t,t_in)
%getInterpolatedStateParam Express the interpolation time t_in as
% t_in = t(m) + theta_m*dt_m
% Rmk: An error is triggered if no such parameters are found.
% Inputs:
%   t (.)       Time instants
%   t_in  (M)   Interpolation times
% Outputs:
%   m (M)       Interval index
%   theta_m (M)         
%   dt_m (M)    Time-step (equal to dt(m))

    t_in=t_in(:); t=t(:); % vector column
    M = length(t_in);
    m = zeros(M,1);
    theta_m = zeros(M,1);
    dt_m = zeros(M,1);
       % t_in<0 -> t_in=0
    t_in = max(t_in,0);
    for i = 1:M % find m for each interpolation time
        m(i) = find((t_in(i)-t)>=0,1,'last');
        if isempty(m(i))
            error('Interpolation instant t_in(%d) not found in t.\n',i);
        end
    end
    dt_m = t(m(:)+1) - t(m(:));
    theta_m = (t_in-t(m(:)))./dt_m;
end

function Bpol = getInterpolationMatrix(bpol,theta)
%getInterpolationMatrix Return the polynomial matrix
%   b_i(theta_j)
% Inputs:
% bpol (MxP) M Polynomial in MATLAB convention
% theta (R)  Abscissa
% Outputs:
% Bpol (MxR) b_i(theta_j)
    M = size(bpol,1);
    R = length(theta);
    Bpol = zeros(M,R);
    for i=1:M % for each polynomial
        for j=1:length(theta) % for each theta
            Bpol(i,j) = polyval(bpol(i,:),theta(j)); % b_i(theta_j)
        end
    end
end