 function Ah = FractionalDDE_CoupledSystem(A,varargin)
%FractionalDDE_CoupledSystem Compute a discretisation of the semigroup
%generator Ah, such that
%                   dX/dt(t) = Ah*X(t)  (t>0)
% where X = (x,...), where '...'  denotes the additional variables linked
% to each realisation.
% The fractional differential equation is written as
%   dx/dt(t) = A*x(t) + Btau*x(t-tau) + Bfrac*d^alpha*x(t-taufrac)
% Remark: current implementation requires that tau=taufrac.
% Input:
%   A
% Inputs (optional):
%   FracOperator (structure array) Fractional operator. Fields:
%       B   'Bfrac' matrix
%       alpha (0,1)
%       tau [0,infty) 'taufrac'
%   DelayOperator (structure array) Delay operator. Fields:
%       B   'Btau' matrix
%       tau [0,inty)
%   DiscretisationParam (structure array) Fields:
%       Nxi (1) Number of poles (frac discretisation)
%       Nk (1)  Number of elements on (0,1) (delay discretisation)
%       Np (1)  Number of nodes of each elements (delay discretisation)
%   OptimParam (structure array) Fields:
%       ximin (1) Minimal pole xi 
%       ximax (1) Maximal pole xi
%       nonnegConstraint (boolean) enforce nonnegativity of weights
%   QuadMethod (string) quadrature method
%       'GL' Gauss-Legendre method
%       'BK' Birk and Song method
% Output:
%   Ah  

        % -- Validate some arguments
    validateattributes(A,{'double'},{'square'},mfilename,'A');
            % Optional arguments (Default value: empty or null)
    p = inputParser; p.FunctionName = mfilename;
    addParameter(p,'FracOperator',[],@(x)(checkFracOperator(x)));
    addParameter(p,'DelayOperator',[],@(x)(checkDelayOperator(x)));
    addParameter(p,'DiscretisationParam',struct('Nxi',10,'Nk',1,'Np',2),@(x)(checkDiscretisationParam(x)));
    addParameter(p,'OptimParam',[],@(x)(checkOptimParam(x)));
    addParameter(p,'QuadMethod',[],@(x) validateattributes(x,{'char'},{'nonempty'}));

    parse(p,varargin{:});  oA = p.Results;
    FracOperator = oA.FracOperator; DelayOperator = oA.DelayOperator;
    Nxi = oA.DiscretisationParam.Nxi; Np = oA.DiscretisationParam.Np;
    Nk = oA.DiscretisationParam.Nk; OptimParam = oA.OptimParam;
    QuadMethod=oA.QuadMethod;
        % -- Construct Fractional and Delay operators
        if ~isempty(FracOperator)
            if ~any(FracOperator.B(:)) % No B, no fractional derivative
                FracOperator=[];
            else
                % Diffusive weight of fractional integral 1-alpha
            alpha = FracOperator.alpha;
            if isempty(OptimParam)
                 if ~isempty(QuadMethod) && strcmp(QuadMethod,'GL')
                    mu_an = @(xi)(sin((1-alpha)*pi)./(pi*xi.^(1-alpha))); % xi>0
%                [xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(mu_an,Nxi);
                 [xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL_Alt(mu_an,Nxi);
                 elseif ~isempty(QuadMethod) && strcmp(QuadMethod,'BK')
                    [xi,mu] = ComputeDiscreteDiffusiveRep_Quadrature_BirkSong(Nxi,1-alpha,'BirkSong');
                 else
                    mu_an = @(xi)(sin((1-alpha)*pi)./(pi*xi.^(1-alpha))); % xi>0
                    [xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL_Alt(mu_an,Nxi);
                    warning('[%s] Defaulting to GL quadrature rule.',mfilename);
                 end
            else % Alternative: with optimization of mu (worse results)
                ximin = OptimParam.ximin; ximax=OptimParam.ximax;
                xi = logspace(log10(ximin),log10(ximax),Nxi);
                omegan = logspace(log10(min(xi)),log10(max(xi)),1e3);
                if ~isfield(OptimParam,'nonnegConstraint')
                    [mu,~,~] = ComputeDiscreteDiffusiveRep_Optim_Std(xi,[],omegan,@(om)(om./om),@(s)(s.^(alpha-1)),0);
                else
                    [mu,~,~] = ComputeDiscreteDiffusiveRep_Optim_Std(xi,[],omegan,@(om)(om./om),@(s)(s.^(alpha-1)),0,'nonnegConstraint',OptimParam.nonnegConstraint);
                end
            end
                % -- (Temporary) Bessel function instead.
%             warning('[%s] Coupling with Bessel function J0 instead.',mfilename);
%             [xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL_BesselJ0(Nxi);
%             DiffOperator  = DiffusiveOperator(mu,xi,'Laplace',@(s)(s.^alpha),'Type','Standard');
                 % -- Fractional derivative
           DiffOperator  = DiffusiveOperator(mu,xi,'Laplace',@(s)(s.^alpha),'Type','Extended');
            LTIOp{1} = struct('B',FracOperator.B,'C',speye(size(A,1)),'Operator',DiffOperator);
            end
        end
        if ~isempty(DelayOperator)
            DirectDelay{1} = struct('B',DelayOperator.B,'C',speye(size(A,1)));
        end
        % -- Build parabolic realisation (fractional operator)
        % Output:   dX/dt = A*X + B*(C*X)(t-tau)
        % (Beware: tedious case distinction...)
            % Direct-Frac   | No direct delay
        if ~isempty(FracOperator) && (FracOperator.tau==0) && isempty(DelayOperator)
            tau = 0;
            fprintf('[%s] Equation: dx/dt(t) = A*x(t) + Bfrac*d^%d*x(t).\n',mfilename,alpha);
            [A,B,C] = DDELTI_BuildParabolicRealisation(A,[],LTIOp(1),[]);
            % Direct-Frac   | Direct delay
        elseif ~isempty(FracOperator) && (FracOperator.tau==0) && ~isempty(DelayOperator)
            tau = DelayOperator.tau;
            fprintf('[%s] Equation: dx/dt(t) = A*x(t) + Btau*x(t-%1.2e) + Bfrac*d^%d*x(t).\n',mfilename,tau,alpha);
            [A,B,C] = DDELTI_BuildParabolicRealisation(A,DirectDelay,LTIOp(1),[]);
            % Delay-Frac    | No direct delay
        elseif ~isempty(FracOperator) && (FracOperator.tau>0) && isempty(DelayOperator)       
            tau = FracOperator.tau;
            fprintf('[%s] Equation: dx/dt(t) = A*x(t) + Bfrac*d^%d*x(t-%1.2e).\n',mfilename,alpha,tau);
            [A,B,C] = DDELTI_BuildParabolicRealisation(A,[],[],LTIOp(1));
            % Delay-Frac    | Direct delay
        elseif ~isempty(FracOperator) && (FracOperator.tau>0) && ~isempty(DelayOperator)
            tau = FracOperator.tau;
            fprintf('[%s] Equation: dx/dt(t) = A*x(t) + Btau*x(t-%1.2e) + Bfrac*d^%d*x(t-%1.2e).\n',mfilename,tau,alpha,tau);
            [A,B,C] = DDELTI_BuildParabolicRealisation(A,DirectDelay,[],LTIOp(1));
            % No Frac       | Direct delay
        elseif isempty(FracOperator) && ~isempty(DelayOperator)
            tau = DelayOperator.tau;
            fprintf('[%s] Equation: dx/dt(t) = A*x(t) + Btau*x(t-%1.2e).\n',mfilename,tau);
            [A,B,C] = DDELTI_BuildParabolicRealisation(A,DirectDelay,[],[]);
            % No  Frac      | No delay
        else
           tau = 0;
           fprintf('[%s] No FracOperator or DelayOperator given.',mfilename);
           Ah = A;
           B = [];
           C = [];
        end
        % -- Build hyperbolic realisation (fractional operator)
        % Output:   dX/dt = Ah*X
        if tau>0
            Ah = DDEConstantDelay_BuildHyperbolicRealisation(A,B,tau,C,'Np',Np,'Nk',Nk);
        else
            Ah = A;
        end
    fprintf('[%s] Size of coupled system: %d.\n',mfilename,size(Ah,1));
end

function checkFracOperator(St)
%checkFracOperator Check validity of the FracOperator structure.
    validateattributes(St,{'struct'},{},mfilename,'FracOperator');
    if sum(isfield(St,{'B','alpha','tau'}))~=3
        error('FracOperator structure invalid. Expected fields: ''B'',''alpha'',''tau''.');
    end
    validateattributes(St.B,{'numeric'},{'square'},mfilename,'FracOperator.B');
    validateattributes(St.alpha,{'numeric'},{'scalar','>',0,'<',1},mfilename,'FracOperator.alpha');
    validateattributes(St.tau,{'numeric'},{'scalar','>=',0},mfilename,'FracOperator.tau');
end

function checkDelayOperator(St)
%checkDelayOperator Check validity of the DelayOperator structure.
    validateattributes(St,{'struct'},{},mfilename,'DelayOperator');
    if sum(isfield(St,{'B','tau'}))~=2
        error('DelayOperator structure invalid. Expected fields: ''B'',''tau''.');
    end
    validateattributes(St.B,{'numeric'},{'square'},mfilename,'DelayOperator.B');        
    validateattributes(St.tau,{'numeric'},{'scalar','>',0},mfilename,'DelayOperator.tau');
end

function checkDiscretisationParam(St)
%checkDiscretisationParam Check validity of the DiscretisationParam structure.
    validateattributes(St,{'struct'},{},mfilename,'DiscretisationParam');
    if sum(isfield(St,{'Nxi','Np','Nk'}))~=3
        error('DiscretisationParam structure invalid. Expected fields: ''Nxi'',''Np'',''Nk''.');
    end
    validateattributes(St.Np,{'numeric'},{'scalar','integer','>=',2},mfilename,'DiscretisationParam.Np');
    validateattributes(St.Nk,{'numeric'},{'scalar','integer','>=',1},mfilename,'DiscretisationParam.Nk');
    validateattributes(St.Nxi,{'numeric'},{'scalar','integer','>=',1},mfilename,'DiscretisationParam.Nxi');
end

function checkOptimParam(St)
%checkDiscretisationParam Check validity of the DiscretisationParam structure.
    validateattributes(St,{'struct'},{},mfilename,'OptimParam');
    if sum(isfield(St,{'ximin','ximax','nonnegConstraint'}))~=3
        error('OptimParam structure invalid. Expected fields: ''ximin'',''ximax'',''nonnegConstraint''.');
    end
    validateattributes(St.ximin,{'numeric'},{'scalar','>',0},mfilename,'OptimParam.ximin');
    validateattributes(St.ximax,{'numeric'},{'scalar','>',St.ximin},mfilename,'OptimParam.ximax');
    validateattributes(St.nonnegConstraint,{'numeric'},{'scalar'},mfilename,'OptimParam.nonnegConstraint');
end