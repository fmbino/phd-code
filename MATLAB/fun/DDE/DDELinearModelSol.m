function y = DDELinearModelSol(t,y0,lambda,mu,tau)
%DDELinearModelSol Solution of the scalar linear DDE:
%   y'(t) = lambda*y(t) + mu*y(t-tau)
%   y(t)  = y0 (-tau<=t<=0)
% for t in [-tau,2*tau].

    validateattributes(t,{'numeric'},{'vector'});t=t(:)'; % line
    validateattributes(y0,{'numeric'},{'vector'});
    validateattributes(lambda,{'numeric'},{'vector','numel',length(y0)});
    validateattributes(mu,{'numeric'},{'vector','numel',length(y0)});
    validateattributes(tau,{'numeric'},{'scalar'});

    for i=1:length(y0)
        phi1 = @(t)(y0(i)*(1+mu(i)/lambda(i))*exp(lambda(i)*t)-y0(i)*mu(i)/lambda(i)); % 0<=t<=tau
        phi2 = @(t)(phi1(tau)*exp(lambda(i)*(t-tau))+y0(i)*mu(i)*(1+mu(i)/lambda(i))*exp(lambda(i)*(t-tau)).*(t-tau)+y0(i)*(mu(i)/lambda(i))^2*(1-exp(lambda(i)*(t-tau))));
        y(i,:) = (t<=0)*y0(i) + (t<=tau).*(t>0).*phi1(t)+(t<=(2*tau)).*(t>tau).*phi2(t);
    end
end

