function [A,Y0] = DDEConstantDelay_BuildHyperbolicRealisation(A,B,tau,C,varargin)
%DDEConstantDelay_BuildHyperbolicRealisation Compute a global, time-local 
%formulation for the delay differential equation (DDE)
%       dy/dt = A*y(t) + B*(C*y)(t-tau) (t>0) (tau>0)
%       C*y(t) = C*y0(t) for t in [-tau,0],
% using a hyperbolic realisation of the delay (on (0,1)):
%       dPsi/dt + (1/tau)*dPsi/dx = 0    (0<x<1), (t>0)
%       Psi(x,0) = C*y0(-tau*x)
%       Psi(0,t) = C*x(t).
% The global formulation is:
%       dY/dt = A * Y(t), (t>0)
% where Y = [y;Psi], size (Ny + Np*Nk*Nd).
% A DG-FEM is used to discretise the transport equation.
% Inputs:
%   A (Ny*Ny)   
%   B (Ny*Nd)
%   tau (1)     time delay
%   C (NdxNy)   delay matrix (C*y is delayed)
% Inputs (optional)
%   y0 (Ny) initial condition
% Inputs (optional - hyperbolic realisation with DG):
%   Nk (1) number of element in (0,1) 
%   Np (1) number of nodes per element (element order: <Np-1>)
% Output:
%   A (Ne*Ne) global matrix, where Ne = Ny + Np*Nk*Nd
%   Y0 (Ne) initial extended vector

        % -- Validate attributes
    validateattributes(A,{'numeric'},{'2d','square'},1);
    Ny = size(A,1);
    validateattributes(B,{'numeric'},{'2d','nrows',Ny},2);
    Nd = size(B,2);
    validateattributes(tau,{'numeric'},{'scalar','>',0},3);
    validateattributes(C,{'numeric'},{'2d','size',size(B')},4);
            % Optional arguments
    p = inputParser; p.FunctionName = mfilename;
    addParameter(p,'y0',zeros(Ny,1),@(x)(validateattributes(x,{'numeric'},{'vector'})));
    addParameter(p,'Nk',2,@(x)(validateattributes(x,{'numeric'},{'scalar','integer','>=',1})));
    addParameter(p,'Np',2,@(x)(validateattributes(x,{'numeric'},{'scalar','integer','>=',1})));
    parse(p,varargin{:}); oA = p.Results; % structure which contains optional arguments
    y0=oA.y0; Nk = oA.Nk; Np=oA.Np;
    fprintf('[%s] Original system: dy/dt = A*y(t) + B*(C*y)(t-%1.2g).\n',mfilename,tau);
    fprintf('[%s] Original system: %d variables.\n',mfilename,Ny);
    Nel = numel(A)+numel(B)+numel(C);
    fprintf('[%s] Original system: %d elements.\n',mfilename,Nel);
        % --
        % Get DG matrices
            % Global formulation: dPsi/dt = K*Psi + F*xi
            % K (Np*Nk)^2 and F (Np*Nk)
    [K,F] = DG1D_Transport(1/tau,[0,1],Np,Nk);
    fprintf('[%s] Transport equation: dimension %d (Np=%d,Nk=%d)\n',mfilename,Np*Nk,Np,Nk);
    fprintf('[%s] Transport equation: local mass and stiffness %d elements \n',mfilename,numel(F)+numel(K));
        % Build global flux matrix (Np*Nk*Nd)x(Ny)
    F=reshape(kron(F,reshape(C,1,[])),[],size(C,2)); %  [F (x) C_i]_i
        % Build global stiffness matrix (Nd*(Np*Nk))^2 (block diagonal)
    K = kron(speye(Nd),K);
        % Build observation matrix Nd * (Np*Nk*Nd)
    i = 1:Nd;
    C = sparse(i,(Np*Nk)*i,ones(1,length(i)),Nd,Np*Nk*Nd);
        % Build global sparse matrix
    A = [A,B*C;
         F,K];
     % Initial condition
    Psi0 = reshape(kron(ones(Np*Nk,1),y0(:)'),[],1); % Initialisation of auxiliary var.
    Y0 = [y0(:);Psi0(:)];
        % -- Diagnostic message
    fprintf('[%s] Extended system: dy/dt = A*y(t).\n',mfilename);
    Nel2 = numel(A);
    fprintf('[%s] Extended system: %d elements (increase of %3.3g%%).\n',mfilename,Nel2,100*Nel2/Nel);
    fprintf('[%s] Extended system: %d variables (increase of %3.3g%%).\n',mfilename,length(Psi0),100*length(Psi0)/Ny);
end