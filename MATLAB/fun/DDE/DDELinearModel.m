function v = DDELinearModel(t,y,Z,lambda,mu)
%ddeLinearModel Describe the following dde
%   y'(t) = mu*y(t) + nu*y(t-tau)
% in the format prescribed by dde23 (see help).
    v = lambda*y + mu*Z(:,1);
end

