function [A,B,C,X0] = DDELTI_BuildParabolicRealisation(Ax,DirectDelay,LTIOp,LTIOpDelayed,varargin)
%DDELTI_BuildParabolicRealisation Build a DDE of the form
%       dX/dt = A*X + B*(C*X)(t-tau) (t>0),
%where X=[x,phik] (length: Nx+sum(NQ)+sum(NR)), from the DDE:
%       dx/dt = Ax*x + BIk*(CIk*x)(t-tau)
%                    + BQk*Qk(CQk*x)(t) + BRk*Rk(CRk*x)(t-tau),
%where Qk and RK are single-input single-output LTI operators.
% Remark: Empty values are accepted for the arguments
%'DirectDelay', 'LTIOp' and 'LTIOpDelayed'.
% Inputs:
%   Ax (Nx-by-Nx)
%   DirectDelay     Cell array of structures that each contains the fields:
%       B (Nx-by-NI)    'BI' matrix
%       C (NI-by-Nx)    'CI' matrix
%   LTIOp           Cell array of structures that each contains the fields:
%           (non-delayed LTI operator, Q)
%       B (Nx-by-NQkx)    'BQk' matrix
%       C (NQkx-by-Nx)    'CQk' matrix
%       Operator (LTIOperator)    'Qk' LTI operator
%   LTIOpDelayed    Cell array of structures that each contains the fields:
%           (delayed LTI operator, R)
%       B (Nx-by-NRkx)    'BRk' matrix
%       C (NRkx-by-Nx)    'CRk' matrix
%       Operator (LTIOperator)    'Rk' LTI operator
% Inputs (optional - initial value)
%   x0 (Nx-by-1)
% Outputs:
%   A (N+sum(NQ)+sum(NR))^2   state matrix for the coupled system 
%   X0 (N+sum(NQ)+sum(NR))    initial condition

    % -- Validate attributes
        % Main state matrix
    validateattributes(Ax,{'numeric'},{'square'});
    Nx = size(Ax,1);
                % Direct Delay
    checkDirectDelay(DirectDelay,Nx);
            % LTI operator (not delayed)
    checkLTIOp(LTIOp,Nx,'UnDelayed');
            % LTI operator (delayed)
    checkLTIOp(LTIOpDelayed,Nx,'Delayed');
        % Optional arguments (Default value: empty or null)
    p = inputParser; p.FunctionName = mfilename;
            % Initial value
    addParameter(p,'x0',zeros(Nx,1),@(x)(validateattributes(x,{'numeric'},{'vector','numel',Nx})));
    parse(p,varargin{:});  oA = p.Results;
    x0 = oA.x0;
    % -- Various constants
        % Direct delays
    NIx = zeros(length(DirectDelay),1);
    for k=1:length(DirectDelay)
        NIx(k) = size(DirectDelay{k}.B,2);
    end
        % Undelayed LTI operators Qk
    NQPhi = zeros(length(LTIOp),1);
    NQx = zeros(length(LTIOp),1);
    for k=1:length(LTIOp)
            % No. of additional variable per operator [N^Qk_Phi] 
        NQPhi(k) = size(LTIOp{k}.Operator.A,1);
            % No. scalar input per operator [N^Qk_x]
        NQx(k) = size(LTIOp{k}.C,1);
    end
    NQ = NQx.*NQPhi; % [N^Qk]
        % Delayed LTI operators Rk
    NRPhi = zeros(length(LTIOpDelayed),1);
    NRx = zeros(length(LTIOpDelayed),1);
    for k=1:length(LTIOpDelayed)
            % No. of additional variable per operator [N^Rk_Phi] 
        NRPhi(k) = size(LTIOpDelayed{k}.Operator.A,1);
            % No. scalar input per operator [N^Rk_x]
        NRx(k) = size(LTIOpDelayed{k}.C,1);
    end
    NR = NRx.*NRPhi; % [N^Rk]
    % -- Construct "tilde" matrices
        % Undelayed LTI operators Qk
    AtQ = cell(length(LTIOp),1); BtQ = AtQ; CtQ = AtQ; DtQ = AtQ;
    for k=1:length(LTIOp)
        AtQ{k} = kron(speye(NQx(k)),LTIOp{k}.Operator.A); % A tilde Qk
        BtQ{k} = SpecialTensorProd(LTIOp{k}.Operator.B,LTIOp{k}.C); % B tilde Qk
        CtQ{k} = kron(speye(NQx(k)),transpose(LTIOp{k}.Operator.C(:))); % C tilde Qk
        DtQ{k} = LTIOp{k}.Operator.D * LTIOp{k}.C; % D tilde Qk
    end
        % Delayed LTI operators Rk
    AtR = cell(length(LTIOpDelayed),1); BtR = AtR; CtR = AtR; DtR = AtR;
    for k=1:length(LTIOpDelayed)
        AtR{k} = kron(speye(NRx(k)),LTIOpDelayed{k}.Operator.A); % A tilde Rk
        BtR{k} = SpecialTensorProd(LTIOpDelayed{k}.Operator.B,LTIOpDelayed{k}.C); % B tilde Rk
        CtR{k} = kron(speye(NRx(k)),transpose(LTIOpDelayed{k}.Operator.C(:))); % C tilde Rk
        DtR{k} = LTIOpDelayed{k}.Operator.D * LTIOpDelayed{k}.C; % D tilde Rk
    end
    % -- Build A by extending Ax
        % Undelayed LTI operators Qk
    for k=1:length(LTIOp)
        idx = size(Ax,1)+(1:NQ(k));
        Ax(1:Nx,1:Nx) = Ax(1:Nx,1:Nx) + LTIOp{k}.B*DtQ{k};
        Ax(1:Nx,idx) = LTIOp{k}.B*CtQ{k};
        Ax(idx,1:Nx) = BtQ{k};
        Ax(idx,idx) = AtQ{k};
    end
        % Delayed LTI operators Rk
    for k=1:length(LTIOpDelayed)
        idx = size(Ax,1)+(1:NR(k));
        Ax(idx,1:Nx) = BtR{k};
        Ax(idx,idx) = AtR{k};
    end
    clear AtQ BtQ CtQ QtQ BtR AtR
    A = Ax;
    
    % -- Build B matrix
    B = sparse(Nx+sum(NQ)+sum(NR),sum(NIx)+2*sum(NRx));
    for j=1:length(DirectDelay)
        idx = sum(NIx(1:(j-1))) + (1:NIx(j));
        B(1:Nx,idx) = DirectDelay{j}.B;
    end
    for j=1:length(LTIOpDelayed)
        idx = sum(NIx) + sum(NRx(1:(j-1))) + (1:NRx(j));
        B(1:Nx,idx) = LTIOpDelayed{j}.B;
    end
    for j=1:length(LTIOpDelayed)
        idx = sum(NIx) + sum(NRx) + sum(NRx(1:(j-1))) + (1:NRx(j));
        B(1:Nx,idx) = LTIOpDelayed{j}.B;
    end
    
    % -- Build C matrix
    C = sparse(sum(NIx)+2*sum(NRx),Nx+sum(NQ)+sum(NR)); 
    for i=1:length(DirectDelay)
        idx = sum(NIx(1:(i-1))) + (1:NIx(i));
        C(idx,1:Nx) = DirectDelay{i}.C;
    end
    for i=1:length(LTIOpDelayed)
        idx = sum(NIx) + sum(NRx(1:(i-1))) + (1:NRx(i));
        C(idx,1:Nx) = DtR{i};           
    end
    for i=1:length(LTIOpDelayed)
        idx_i = sum(NIx) + sum(NRx) + sum(NRx(1:(i-1))) + (1:NRx(i));
        idx_j = Nx + sum(NQ) + sum(NR(1:(i-1))) + (1:NR(i));
        C(idx_i,idx_j) = CtR{i};
    end
    clear CtR DtR
            % -- Build initial condition (OLD)
    X0 = [x0;zeros(sum(NQ)+sum(NR),1)]; % LTI1 could have a non-null start
    fprintf('[%s] LTI systems initial condition taken null.\n',mfilename);
            % If Rk!=0 and (C*x0!=0 and Phi(0)!=0)
    for k=1:length(LTIOp)
        if any(LTIOp{k}.C*x0)
            warning(['Initial input to undelayed LTI operator no.%d, C*x0, is non-null. ',...
        'There may need to integrate the additional variables Phi over [-tau,0] first.'],k);
        end
    end
    for k=1:length(LTIOpDelayed)
        if any(LTIOpDelayed{k}.C*x0)
            warning(['Initial input to delayed LTI operator no.%d, C*x0, is non-null. ',...
        'There may need to integrate the additional variables Phi over [-tau,0] first.'],k);
        end
    end
        % -- Diagnostic message
    fprintf('[%s] Original system: %d variables.\n',mfilename,Nx);
    for k=1:length(LTIOp)
        fprintf('[%s] Undelayed LTI operator n°%d (dimension %d): %d inputs -> %d add. var.\n',mfilename,k,NQPhi(k),NQx(k),NQ(k));
    end
    for k=1:length(LTIOpDelayed)
        fprintf('[%s] Delayed LTI operator n°%d (dimension %d): %d inputs -> %d add. var.\n',mfilename,k,NRPhi(k),NRx(k),NR(k));
    end
    Nadd = sum(NQ)+sum(NR);
    fprintf('[%s] Additional variables: %d (increase of %3.3g%%), with %d delayed (%3.3g%% of original, %3.3g%% of total).\n',mfilename,Nadd,100*Nadd/Nx,size(C,1),100*size(C,1)/Nx,100*size(C,1)/(Nx+Nadd));
    fprintf('[%s] Extended system: dx/dt = A*x + B*(C*x)(t-tau).\n',mfilename);
    fprintf('[%s] A is %dx%d (%dx%d expected).\n',mfilename,size(A,1),size(A,2),Nx+Nadd,Nx+Nadd);
    fprintf('[%s] B is %dx%d (%dx expected).\n',mfilename,size(B,1),size(B,2),Nx+Nadd);
    fprintf('[%s] C is %dx%d (x%d expected).\n',mfilename,size(C,1),size(C,2),Nx+Nadd);
end

function checkDirectDelay(St,N)
%checkDirectDelay Check the validity of the cell array St.
% Inputs:
%   St   Cell array for DirectDelay
%   N (1)   Size of x

    if isempty(St)
        fprintf('[%s] Direct delay: none given.\n',mfilename);
        return
    end
    validateattributes(St,{'cell'},{},mfilename,'DirectDelay');
    for i=1:length(St)
        validateattributes(St{i},{'struct'},{},mfilename,sprintf('DirectDelay{%d}',i));
        if sum(isfield(St{i},{'B','C'}))~=2
            error('[%s] DirectDelay{%d} structure invalid. Expected fields: ''B'',''C''.',mfilename,i);
        else
            validateattributes(St{i}.B,{'numeric'},{'2d','nrows',N},mfilename,sprintf('DirectDelay{%d}.B',i));
            validateattributes(St{i}.C,{'numeric'},{'2d','size',[size(St{i}.B,2),N]},mfilename,sprintf('DirectDelay{%d}.C',i));
        end
    end
    fprintf('[%s] Direct delay: %d given.\n',mfilename,length(St));
end

function checkLTIOp(St,N,str)
%checkDirectDelay Check the validity of the cell array St.
% Inputs:
%   St   Cell array for LTIop
%   N (1)   Size of x
%   str (string)    arbitrary identifier (for display purposes only)

    if isempty(St)
        fprintf('[%s] %s LTI operators: none given.\n',mfilename,str);
        return
    end
    validateattributes(St,{'cell'},{},mfilename,sprintf('LTIOp%s',str));
    for i=1:length(St)
        validateattributes(St{i},{'struct'},{},mfilename,sprintf('LTIOp%s{%d}',str,i));
        if sum(isfield(St{i},{'B','C','Operator'}))~=3
            error('LTIOp%s{%d} structure invalid. Expected fields: ''B'',''C'',''Operator''.',str,i);
        else
            validateattributes(St{i}.B,{'numeric'},{'2d','nrows',N},mfilename,sprintf('LTIOp%s{%d}.B',str,i));
            validateattributes(St{i}.C,{'numeric'},{'2d','size',[size(St{i}.B,2),N]},mfilename,sprintf('LTIOp%s{%d}.C',str,i));
            validateattributes(St{i}.Operator,{'LTIOperator'},{},mfilename,sprintf('LTIOp%s{%d}.Operator',str,i));
                % scalar input scalar output
            validateattributes(St{i}.Operator.B,{'numeric'},{'vector'},mfilename,sprintf('LTIOp%s{%d}.Operator.B',str,i));
            validateattributes(St{i}.Operator.D,{'numeric'},{'scalar'},mfilename,sprintf('LTIOp%s{%d}.Operator.D',str,i));
        end
    end
    fprintf('[%s] %s LTI operators: %d given.\n',mfilename,str,length(St));
end

function C = SpecialTensorProd(A,B)
%SpecialTensorProd Compute a concatenation of tensor product between the
%vector A and the lines of matrix B.
%           A = [A (x) B(i,:)] for i in [1,P]
% Inputs:
%   A (N)
%   B (PxR)
% Outputs:
%   C ((N*P)xR)

    if numel(A)==1 % Simple case, no need to use kron
       C = A*B;
    else
            % This command can trigger "out of memory"
        C=reshape(kron(A,reshape(B,1,[])),[],size(B,2));       
    end
end