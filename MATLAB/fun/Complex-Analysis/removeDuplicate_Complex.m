function ztrim = removeDuplicate_Complex(z,tol)
%removeDuplicate_Complex Removes complex number deemed duplicate, using the
%following criterion: z1 and z2 are duplicate of each other if |z1-z2|<tol.
% Input:
%   z (N) vector of complex numbers
%   tol (1) tolerance for duplicate check
% Output:
%   ztrim (P) vector of complex numbers, without duplicate

    % Validate arguments
    validateattributes(z,{'numeric'},{},mfilename,'z');
    validateattributes(tol,{'numeric'},{'scalar','positive'},mfilename,'tol');

    % Remove duplicate
    ztrim = z;
    i = 1;
    while i<length(ztrim)
        mask = abs(ztrim-ztrim(i))>tol; mask(i) = 1;
        ztrim = ztrim(mask); % remove duplicate
        i = i+1;
    end
end

