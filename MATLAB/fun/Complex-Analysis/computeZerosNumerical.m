function z = computeZerosNumerical(f,x,y,N,varargin)
%computeZerosNumerical Compute the zeros of function f(z) using MATLAB
%fsolve.
% Inputs:
%   f (function_handle) complex-valued function
%   x (2) [xmin,xmax] search area (abscissa)
%   y (2) [ymin,ymax] search area (abscissa)
%   N (2) [Nx,Ny] steps for grid of initial guess z0
% Input (optional):
%   tol (1) 'tolFun' parameter of fsolve (default:1e-10)
% Outputs:
%   z (P) compute zeros (no NaN, no duplicates)

        % Validate arguments
    validateattributes(f,{'function_handle'},{},mfilename,'f');
    s = linspace(0,1,2);
    validateattributes(f(s),{'numeric'},{'size',size(s)},mfilename,'f(x)');
    clear s
    validateattributes(x,{'numeric'},{'vector','numel',2},mfilename,'x');    
    validateattributes(y,{'numeric'},{'vector','numel',2},mfilename,'y');
    validateattributes(N,{'numeric'},{'integer','vector','numel',2},mfilename,'N');
        % Optional argument
    p = inputParser; p.FunctionName = mfilename;
    addParameter(p,'tol',1e-10,@(x)(validateattributes(x,{'numeric'},{'scalar','positive'})));
    parse(p,varargin{:});  oA = p.Results;
    tol = oA.tol;

        % Compute poles
    [z0r,z0i] = meshgrid(linspace(min(x),max(x),N(1)),linspace(min(y),max(y),N(2)));
    z0 = z0r + 1i*z0i; z0= z0(:); z0 = permute(z0,[3,2,1]); clear z0r z0i
    options=optimoptions(@fsolve,'Display','none','Jacobian','off','DerivativeCheck','off','TolFun',tol);
    z = fsolveLoop(f,z0,options); z=permute(z,[3,2,1]);
        % remove NaN and duplicates
    z = z(~isnan(z));
    z = removeDuplicate_Complex(z,tol);
end

