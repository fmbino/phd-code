function res = computeComplexResidue(f,z0,varargin)
%computeComplexResidue Compute complex residue of function f(z) at z0 using
%line integral (circle around z0).
% Integration radius is determined through a nearest neighbour search and
% dichotomy (slow, but accurate).
% Warning: this function is not infallible, more specifically the algorithm
% used in computeRes may not be satisfactory on some cases. In these cases,
% try to compute the circle integral with a radius of your choice.
% Inputs:
%   f (function_handle)
%   z0 (N) vector of complex number
% Inputs (optional):
%   tol (1) tolerance
% Outputs:
%   res (N) r(i) = Residue(f(z),z=z0(i))

    % Validate arguments
    validateattributes(f,{'function_handle'},{},mfilename,'f');
    x = linspace(0,1,2);
    validateattributes(f(x),{'numeric'},{'size',size(x)},mfilename,'f(x)');
    clear x
    validateattributes(z0,{'numeric'},{},mfilename,'z0');    
        % Optional arguments
                % Optional arguments
    p = inputParser; p.FunctionName = mfilename;
    addParameter(p,'tol',1000*eps,@(x)(validateattributes(x,{'numeric'},{'scalar','positive'})));
    parse(p,varargin{:}); oA = p.Results; % structure which contains optional arguments
    tol = oA.tol;
    % Compute residue
    res = z0;
    if length(z0)==1
        r = (1+abs(z0))/2; % initial guess
        res = computeRes(f,z0,r,tol);        
    else
        for i=1:length(z0)
                % find largest admissible radius
                % (integration path must not circle around two poles, or
                % branch cuts)
            mask = (1:length(z0))~=i;
            [~,rmax] = dsearchn([real(z0(mask)),imag(z0(mask))],[real(z0(i)),imag(z0(i))]);
            r = rmax/2;
            res(i) = computeRes(f,z0(i),r,tol);
        end
    end
end


function res = computeRes(f,z0,r,tol)
%computeRes Compute the residue at z=z0 (scalar), using a dichotomy on
%radius r.
% Inputs:
%   f (function_handle)
%   z0 (1)
%   r (1) starting radius
%   tol (1) tolerance
% Output:
%   res (1) residue

    r_min = 1e-10; % minimum radius for integration
    dres = 2*tol; % change in residue btw two iterations

    res = integral(@(t)r*f(z0+r*exp(1i*t)).*exp(1i*t),0,2*pi)/(2*pi);
    fprintf('[%s] Computing Res(f(z),z=%1.1e+i*%1.1e)\n',mfilename,real(z0),imag(z0));
    while abs(dres)>tol && r_min<r
        r=r/10;
        fprintf('[%s] Integration radius is r=%1.2e,|dres|=%1.1e,res=%1.1e\n',mfilename,r,abs(dres),res);
            % perform integration on {|z-z0(i)|=r}
        res_tmp = integral(@(t)r*f(z0+r*exp(1i*t)).*exp(1i*t),0,2*pi)/(2*pi);
        dres = res_tmp-res;
    end
    if r<r_min
        warning('[%s] Res(f(z),z=%1.1e+i*%1.1e): minimum radius reached. dres=%1.1e.\n',mfilename,real(z0),imag(z0),abs(dres));
    end
end