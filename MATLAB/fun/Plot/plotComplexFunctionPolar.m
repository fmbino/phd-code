function plotComplexFunctionPolar( F,rrange,thetarange)
%plotComplexFunction Summary of this function goes here
% Input:
%   F (function handle) C->C function
%   xrange (1x2)
%   yrange (1x2)
    Nx = 20;
    Ny = 20;
    N = max(Nx,Ny);
    r = linspace(rrange(1),rrange(2),N);
    t = linspace(thetarange(1),thetarange(2),N);
    [R,T]=meshgrid(r,t);
    
    M = abs(F(R.*exp(1i*T))); % Does not always work as expected
    T = angle(F(R.*exp(1i*T)));
    
    [X,Y]=pol2cart(R,T);

    subplot(1,2,1)
    surf(X,Y,M,'EdgeColor','None');
    view([0,90])
    colorbar
    xlabel('x (rad/s)');
    ylabel('y (rad/s)');
    title('Modulus');
    subplot(1,2,2)
    surf(X,Y,T,'EdgeColor','None');
    view([0,90])
    colorbar
    xlabel('x (rad/s)');
    ylabel('y (rad/s)');
    title('Principal Argument ]-pi,pi[');
end