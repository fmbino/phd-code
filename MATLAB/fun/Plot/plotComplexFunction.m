function plotComplexFunction( F,xrange,yrange,N)
%plotComplexFunction Summary of this function goes here
% Input:
%   F (function handle) C->C function
%   xrange (1x2)
%   yrange (1x2)
%   N (1x1) number of point in each direction
    Nx = N;
    Ny = N;
    N = max(Nx,Ny);
    x = linspace(xrange(1),xrange(2),N);
    y = linspace(yrange(1),yrange(2),N);
    [X,Y] = meshgrid(x,y);
    Z = F(X+Y*1i);
    M = abs(F(X+Y*1i)); % Does not always work as expected
    T = angle(F(X+Y*1i));

    subplot(1,2,1)
    surf(X,Y,M,'EdgeColor','None');
    view([0,90])
    colorbar
    xlabel('x (rad/s)');
    ylabel('y (rad/s)');
    title('Modulus');
    subplot(1,2,2)
    surf(X,Y,T,'EdgeColor','None');
    view([0,90])
    colorbar
    xlabel('x (rad/s)');
    ylabel('y (rad/s)');
    title('Principal Argument ]-pi,pi[');

end

