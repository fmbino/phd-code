function plotBodeDiagram(gh,pulse_range,H,name,N)
%plotBodeDiagram Add a Bode diagram to the graphic handle <gh>. If any,
%the previous Bode diagrams are kept.
% Inputs:
%   gh graphic handle
%   pulse_range (2) [wmin,wmax] pulsation range (rad/s)
%   H (N -> N) Laplace transform H(s)
%   name (string)
%   N (1) Number of points for the plot

    if(~ishghandle(gh))
        error('<gh> should be a graphic handle: use gh=figure');
    else
        figure(gh); % Focus
    end
        % Validate arguments
    validateattributes(pulse_range,{'numeric'},{'vector','numel',2},2);
    validateattributes(H,{'function_handle'},{'scalar'},3);
    validateattributes(N,{'numeric'},{'scalar'});
    validateattributes(name,{'char'},{'nonempty'});
    s = (10+1i)*ones(10,1);
    if isempty(H(s)) % prevent error in the empty case
        warning('H(s) is empty: no plot (but no error!)');
    else
        validateattributes(H(s),{'numeric'},{'vector','numel',10});

        w = logspace(log10(pulse_range(1)),log10(pulse_range(2)),N); w=w(:);
        G = 20*log10(abs(H(w*1i))); G = G(:); % Gain in dB
        Phi = angle(H(w*1i)); Phi = Phi(:); % Phase in rad
        slope = polyfit(log10(w),G,1); slope=slope(1);

        subplot(1,2,1)
        semilogx(w,G);
        title('Gain');
        xlabel('pulsation (rad/s)');
        ylabel('Gain (dB)');
        hc=get(legend(gca),'String'); % get legend from current axes.
        legend(hc,sprintf('%s (%1.2g dB/decade)',name,slope));
        hold all
        grid on

        subplot(1,2,2)
        semilogx(w,(180/pi)*unwrap(Phi));
        title('Phase');
        xlabel('pulsation (rad/s)');
        ylabel('Phase (°)');
        %hc=get(legend(gca),'String'); % get legend from current axes.
        %legend(hc,sprintf('%s',name));
        hold all
        grid on
    
    end
end

