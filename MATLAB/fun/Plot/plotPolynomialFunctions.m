function plotPolynomialFunctions(Pol,Int,N)
%plotPolynomialFunctions Plot a family of polynomial functions over a given
%interval.
% Inputs:
% Pol (NxP)  Family of N polynomials, degree at most P. Coefficients in
%            decreasing order (polyval convention).
%               [a_p, ..., a_0]
%               [b_p,...,b_0]
% Int (2)   Plot interval.
% N (1)     Number of points.

validateattributes(Pol,{'numeric'},{'2d','nonempty','nonnan','finite'},'','Pol',1);
validateattributes(Int,{'numeric'},{'vector','nonempty','nonnan','finite','numel',2},'','Int',2);
validateattributes(N,{'numeric'},{'scalar','positive','nonnan','finite'},'','N',3);


x = linspace(min(Int),max(Int),N);
clf
hold all
leg=cell(0);
for i=1:size(Pol,1)
    plot(x,polyval(Pol(i,:),x));
    leg(end+1)={sprintf('P_%d',i)};
end
title('Polynomial family');
xlabel('x');
ylabel('P(x)');
legend(leg);
end

