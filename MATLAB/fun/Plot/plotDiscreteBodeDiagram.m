function plotDiscreteBodeDiagram(gh,w,H,name)
%plotDiscreteBodeDiagram Add a Bode diagram to the graphic handle <gh>. If 
%any, the previous Bode diagrams are kept.
% Inputs:
%   gh graphic handle
%   w (Px1) pulsation range (rad/s)
%   H (Px2) Fourier transform H(w) (complex number)
%   name (string)

    if(~ishghandle(gh))
        error('<gh> should be a graphic handle: use gh=figure');
    else
        figure(gh); % Focus
    end

        % Validate arguments
%     validateattributes(w,{'numeric'},{'2d'});
%     validateattributes(H,{'numeric'},{'2d','size',[size(w,1),1]});
%     validateattributes(name,{'char'},{'nonempty'});

    G = 20*log10(abs(H));
    Phi = angle(H);
    
    subplot(1,2,1)
    semilogx(w,G);
    title('Gain');
    xlabel('pulsation (rad/s)');
    ylabel('Gain (dB)');
    hc=get(legend(gca),'String'); % get legend from current axes.
    legend(hc,sprintf('%s',name));
    hold all
    grid on
    
    subplot(1,2,2)
    semilogx(w,(180/pi)*unwrap(Phi));
    title('Phase');
    xlabel('pulsation (rad/s)');
    ylabel('Phase (°)');
    hc=get(legend(gca),'String'); % get legend from current axes.
    legend(hc,sprintf('%s',name));
    hold all
    grid on

end

