function q_ex = LEE_1D_ExactHarmonicSolution(beta,L,c0,rho0)
%LEE_1D_ExactHarmonicSolution Return the exact harmonic solutio for the
%monodimensional LEE equation.
% Monodimensional LEE. 
% x in [0,L]
%   Source: u(t) = p(t)/z0 = Ai*exp(i*w*t) at x=0 
%   Impedance z(1i*w) at x=L
% Inputs:
%   beta   (PxQ)-> (PxQ) Laplace transform impedance Z(s) (Re(s)>0)
%          expressed with reflexion coeff: (z-1)/(z+1)
%   L   (1)     domain length (m)
%   c0  (1)     sound speed (m/s)
%   rho0 (1)    density (kg/m^3)
% Outputs:
%   q_ex(x,t,w,Ai) = [u, p/z0] (m/s)
%       (P,1,1,1) -> (2,P)
%       w   angular frequency (rad/s)
%       Ai  source amplitude (Pa)
        
    validateattributes(beta,{'function_handle'},{});
    x = linspace(1,10,10);
    y = linspace(-10,10,10);
    [X,Y] = meshgrid(x,y);
    S = X + 1i*Y;
    validateattributes(beta(S),{'numeric'},{'size',size(S)});
    validateattributes(L,{'numeric'},{'scalar','positive'});      
    validateattributes(c0,{'numeric'},{'scalar','positive'});      
    validateattributes(rho0,{'numeric'},{'scalar','positive'});

    k=@(w)(w/c0); % Inviscid wavenumber
        % Reflexion coefficient
    betar = @(w)(beta(1i*w).*exp(-1i*k(w)*2*L));
    u = @(x,t,w,Ai)((Ai/(rho0*c0))*kron((exp(-1i*k(w)*x)-betar(w)*exp(1i*k(w)*x)),exp(1i*w*t)));
    p = @(x,t,w,Ai)(Ai*kron((exp(-1i*k(w)*x)+betar(w)*exp(1i*k(w)*x)),exp(1i*w*t)));
    q_ex = @(x,t,w,Ai)([u(x(:)',t,w,Ai); p(x(:)',t,w,Ai)/(c0*rho0)]);
end