function [y,t]= solEDF(lam,mu,x0,x1,t)

lam2=lam*lam;
mu2=mu*mu;

Alam=(x0*mu-x1)/(mu-lam);
Amu=(x0*lam-x1)/(lam-mu);

Elam=exp(lam2*t).*(1+erf(lam*sqrt(t)));
Emu=exp(mu2*t).*(1+erf(mu*sqrt(t)));

y=Alam*Elam + Amu*Emu;

% plot(t,y)