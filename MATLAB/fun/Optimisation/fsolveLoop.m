function z = fsolveLoop(f,z0,options,varargin)
%fsolveLoop Find the root of the nonlinear, complex-valued function f(z)
%using fsolve, with starting points z0.
%This is essentially a loop around fsolve; loop on the third dimension of
%z0.
% Inputs:
%   f (fun_handle) complex-valued function (must match fsolve
%   expectation, e.g. to supply Jacobian)
%   z0 (:,:,N) starting points
%   options (N) optimisation options (ref. fsolve help)
%   plot (bool) display diagnostic plot
% Outputs:
%   z (:,:,N) roots (NaN if convergence not achieved)

    % validateattributes
    validateattributes(f,{'function_handle'},{},1);
    validateattributes(z0,{'numeric'},{'3d'},2);
    validateattributes(options,{'optim.options.Fsolve'},{},3);
    % find root
    z = zeros(size(z0)); l=size(z0,3);
for i=1:l
    try
        tic
        [zc,~,exitflag] = fsolve(f,z0(:,:,i),options);
        if exitflag==1 % successfull convergence
            z(:,:,i)=zc;
        else
           z(:,:,i)=NaN;
        end
        titer=toc;
        fprintf('[fsolveLoop]; optim n°%d/%d done (%d%%). Remaining: %1.2g min...\n',i,l,floor(100*i/l),titer*(l-i)/60);
    catch
        warning('[fsolveLoop]: fsolve did not complete.');
        z(:,:,i)=NaN;
    end
end
    fprintf('[fsolveLoop]: ... finished.\n');
    % plot
    if ~isempty(varargin)
        if length(f(z0(:,:,1)))>1
            error('cannot plot a vector-valued function');
        elseif length(z0(:,:,1))>1 
            error('cannot plot f(z) if z is not a scalar');
        end
        Z = [z0(:);z(:)];
        z_real = linspace(min(real(Z)),max(real(Z)),5e2);
        z_imag = linspace(min(imag(Z)),max(imag(Z)),5e2);
        [Z_real,Z_imag] = meshgrid(z_real,z_imag);
        Z = (Z_real+1i*Z_imag);
        clf
        hold on
        title(sprintf('|f(z)|'));
        xlabel('Re(Z)');
        ylabel('Im(Z)');
        surf(Z_real,Z_imag,abs(f(Z)),'EdgeColor','none');
        scatter3(real(z),imag(z),1e3+abs(f(z)),'kx','SizeData',150,'LineWidth',5);
        scatter3(real(z0),imag(z0),1e3+abs(f(z)),'ro','SizeData',150,'LineWidth',5);
        %caxis([0 27])
        view([0,90])
        colorbar
    end
end