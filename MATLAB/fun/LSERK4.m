function [y,t,Nop] = LSERK4(A,u,y0,tf, s)
%LSERK4 RK4 explicit time-integration for the ODE:
%               y'=A*y + u(t) with y(0)=y0
% <N_it> are performed. <N_it+1> values of y will be computed, the first
%being y0, and the <N_it+1>-th one being y(tf).
% Inputs:
%   A (mxm)
%   u (function handle) (1x1) -> (mx1)
%   y0 (mx1)
%   tf (1x1) final time (starting time:t=0)
%   s (1x1) safety coeff (1= linear stability limit)
% Outputs:
%   y (mx(N_it+1)) y values
%   t (1x(N_it+1)) times
%   Nop (1x1) Number of operations performed.

% Assess number of time-steps (estimate)
eignval = eig(A);
if(sum((real(eignval)>=0))~=0)
    warning('Time continuous system is not linearly stable.');
end

% Time integration constants
dtmax = 4.4/max(abs(eignval)); % RK4 linear stability limit
dt = s*dtmax;
dt_f = rem(tf,dt); % Final time-step (for the last iteration)
N_it = floor(tf/dt); % No. of iter. to perform with <dt>
    if(N_it==0)
        dt = dt_f;
    else
        dt = [dt*ones(1,N_it),dt_f];
    end
N_it = N_it+1; % Total number of iterations to perform

    % Initialization
m = length(y0);
y = zeros(m,N_it+1); % Output: y(n) = y(t_{n-1})
y(:,1)=y0;
t = zeros(1,N_it+1);
t(2:(end-1)) = dt(1:(end-1)).*(1:(N_it-1)); t(end)=t(end-1)+dt_f;
    % Estimate of the number of operations
Nop = N_it*m;
% Low Storage Explicit Runge-Kutta 4 time-integration
    % Coefficients definition (Carpenter and al., Fourth-order 2N-storage 
    %Runge-Kutta schemes, 1994)
a = [0,-567301805773/1357537059087,-2404267990393/2016746695238,-3550918686646/2091501179385,-1275806237668/842570457699];
b = [1432997174477/9575080441755, 5161836677717/13612068292357, 1720146321549/2090206949498, 3134564353537/4481467310338, 2277821191437/14882151754819];
c = [0,1432997174477/9575080441755,2526269341429/6820363962896,2006345519317/3224310063776,2802321613138/2924317926251];
fprintf('[LSERK4]: %d iter. & %d oper. to perform (dt=%1.2g dtmax=%1.2g).\n',N_it,Nop,dt(1),dtmax);
reverseStr='';
time = cputime();
L = @(y,t)(A*y+u(t));
    % Temporary values
k=zeros(m,1);
p=zeros(m,1);
for n=1:N_it
   msg = sprintf('[LSERK4_explicit]: Percent done: %3.1f', floor(100*n/N_it));
   fprintf([reverseStr, msg]);
   reverseStr = repmat(sprintf('\b'), 1, length(msg));
    p=y(:,n);
    for i=1:5
        k = a(i)*k + dt(n)*L(p,t(n)+c(i)*dt(n));
        p = p + b(i)*k;
    end
    y(:,n+1)=p;
end
fprintf(' - Finished. (%2.2g s)\n',cputime()-time);

end