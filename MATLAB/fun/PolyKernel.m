function Y = PolyKernel(t,beta)
%PolyKernel Polynomial kernel Y_beta, for beta>0. Causal function.
% Input:
%   t (NxP) time
%   beta (1x1) strictly positive real
% Output:
%   Y (NxP) Y = (t^(beta-1))/gamma(beta) for t>=0
    Y = t.^(beta-1);
    Y = Y/gamma(beta);
    Y = Y.*(t>=0);
end

