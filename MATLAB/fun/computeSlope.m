function dydx = computeSlope(x,y,stencil)
%computeSlope Compute slopes using a sample of width given by <stencil>.
%This function is useful to compute convergence rate.
% Input:
%   x (N)
%   y (N)
%   stencil (1) integer >=1
% Output:
%   slope (R) vector with every slopes computed


    validateattributes(x,{'numeric'},{'vector'},mfilename,'x');
    validateattributes(y,{'numeric'},{'vector','numel',length(x)},mfilename,'y');
    validateattributes(stencil,{'numeric'},{'integer','positive'},mfilename,'stencil');
    x = x(:); y = y(:);
    N =length(x);
    dydx = [];
    for i=1:N
        int = i+(0:stencil);
        if max(int)<=N
                pol = polyfit(x(int),y(int),1);
                dydx(end+1)=pol(1);
        end            
    end
end
