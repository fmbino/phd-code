function semilogxComplex(gh,x,z,leg,strspec)
%semilogxComplex Add a complex plot to the figure handle gh.
% Inputs:
%   gh figure handle
%   x (N) x-axis
%   z (N) complex variable
%   leg (String) legend
%   strspec String specifier (for the line specification)

    if(~ishghandle(gh))
        error('<gh> should be a graphic handle: use gh=figure');
    else
        figure(gh); % Focus
    end
    subplot(1,2,1);
    semilogx(x,real(z),strspec);
    gh_leg=get(legend(gca),'String'); % get legend from current axes.
    legend(gh_leg,leg);
    legend('Location','Best');
    ylabel('Re');
    hold all
    subplot(1,2,2);
    semilogx(x,imag(z),strspec);
    ylabel('Im');
    hold all
end