%% Two bottom x-axis
% To executed once a figure is displayed

Fs = 1000;
t =0:(1/Fs):0.1; %seconds
t1 = t.*1000; % msec
y = cos(2*pi*100*t);
plot(t,y);
Ax1 = gca;
Ax2 = axes('Position',get(Ax1,'Position'),'XAxisLocation','top');
%xlim(get(Ax1, 'XLim') * scale_factor);
%set(Ax2, 'XScale', get(Ax1, 'XScale')); %// make logarithmic if first axis is too
%hLine2 = line(t1,y,'color','k','parent',Ax2);
%set(Ax2, 'XScale', get(first_axis, 'XScale'));
%%
first_axis = gca;
sqz = 0.12; %// distance to squeeze the first plot
set(first_axis, 'Position', get(first_axis, 'Position') + [0 sqz 0 -sqz ]);
ax2 = axes('Position', get(first_axis, 'Position') .* [1 1 1 0.001] - [0 sqz 0 0],'Color','none');
scale_factor = 42; %// change this to your satisfaction
xlim(get(first_axis, 'XLim') * scale_factor);
set(ax2, 'XScale', get(first_axis, 'XScale')); %// make logarithmic if first axis is too
%% Stokes number
f = 3.5e3;
St = sqrt(2*pi*f/nu)*dc/2;
%% Five-stage RK method
    % Initialisation
R1=q(:,n); % solution at time n

    % Stage 1
R2=F1(R1,t(n)); % c(1)=0
R1 = R1 + a(1)*dt*R2; % q^(2)
R2 = R1 + (b(1)-a(1))*dt*R2;
    % Stage 2
R1=F1(R1,t(n)+c(2)*dt);
R2 = R2 + a(2)*dt*R1; % q^(3)
R1 = R2 + (b(2)-a(2))*dt*R1;
    % Stage 3
R2 = F(R2,t(n)+c(3)*dt); 
R1 = R1 + a(3)*dt*R2; % q^(4)
R2 = R1 + (b(3)-a(3)))*dt*R2;
    % Stage 4
R1 = F(R1,t(n)+c(4)*dt); 
R2 = R2 + a(5)*dt*R1; % q^(5)
R1 = R2 + (b(4-a(4)))*dt*R1;
    % Stage 5
R2 = F(R2,t(n)+c(5)*dt);
R1 = R1 + b(5)*dt*R2;

%% SDD
t = linspace(0,pi,1e5);
x1 = cos(2*pi*t); % 1-periodic
x2 = cos(2*pi*(t/pi)); % 2-periodic
fprintf('Cross mean is: %1.1e\n',mean(x1.*x2));

%% Bessel
x_k = linspace(-20,20,1e4); u = sqrt(-1i*x_k);
p = [- 1.127121308+0.2409086403*1i,-1.254672407+0.093319206*1i,8.808226894-1.15565082*1i];
    % Dl of phi around sqrt(3*i)
p = [- 1.303204981+0.1188064753*1i,- 0.249941448- 0.132346464*1i,8.173610715-0.059813037*1i];
    % DL of phi around sqrt(5*i)
p = [0.01034075093*1i+0.004581066561,-0.09212175500+0.02104354778*1i,- 1.227872958-0.2182567700*1i,0.117765366+0.2687300240*1i,7.868573575-0.0427804493*1i];
    %p = [-1.303204981,-0.2499414480,8.173610716];
phi = @(u)(-(u.^2)./(1-(2./u).*besselj(1,u)./besselj(0,u)));
figure
subplot(1,2,1)
hold all
plot(sign(x_k).*abs(x_k),real(phi(u)));
plot(sign(x_k).*abs(x_k),real(polyval(p,u)));
plot(sign(x_k).*abs(x_k),real(polyval(real(p),u)));
legend('Phi','Phi Tayl. Exp.','Hermitian Tayl. Exp');
set(gca,'DataAspectRatio',[1,1,1])
subplot(1,2,2)
hold all
plot(sign(x_k).*abs(x_k),imag(phi(u)));
plot(sign(x_k).*abs(x_k),imag(polyval(p,u)));
plot(sign(x_k).*abs(x_k),imag(polyval(real(p),u)));
legend('Phi','Phi Tayl. Exp.','Hermitian Tayl. Exp');
axis(min(x_k),max(x_k),-10,10);
set(gca,'DataAspectRatio',[1,1,1])


%% Comparison of complex power
p=4;
u = @(w)(sqrt(-1i*w));
phi_min = @(w)(1i*u(w).^p); % Im(u)<0, w>0
phi_plus = @(w)(-1i*u(w).^p); % Im(u)>0, w<0
phi = @(w)((1i*w).^(p/2)); % Proposal for an expression
phi = @(w)(-1i*abs(w).*w);
w = linspace(-2,2,1e2);
figure
subplot(1,2,1)
hold all
plot(w,real(phi_plus(w)));
plot(w,real(phi_min(w)));
plot(w,real(phi(w)));
legend('Phi + (w<0)','Phi - (w>0)','Phi?');
set(gca,'DataAspectRatio',[1,1,1])
subplot(1,2,2)
hold all
plot(w,imag(phi_plus(w)));
plot(w,imag(phi_min(w)));
plot(w,imag(phi(w)));
legend('Phi + (w<0)','Phi - (w>0)','Phi?');
set(gca,'DataAspectRatio',[1,1,1])

%% Complex function
p=2;
G = @(u)(1i*sign(imag(u)).*(u.^p));
S = @(u)(-abs(u).^2);
u = @(w)((1i*w).^(0.5)); % path in the complex plane
w = linspace(-2,2,1e2);

clf
subplot(1,2,1)
hold all
plot(w,real(G(u(w))));
plot(w,real(S(u(w))));
legend('G','S');
set(gca,'DataAspectRatio',[1,1,1])
subplot(1,2,2)
hold all
plot(w,imag(G(u(w))));
plot(w,imag(S(u(w))));
legend('G','S');
set(gca,'DataAspectRatio',[1,1,1])

%% Display poly coeff
C=C_10;
fprintf('b0= %1.1e+j%1.1e b1= %1.1e+j%1.1e b2 = %1.1e+j%1.1e\n',real(C(3)),imag(C(3)),real(C(2)),imag(C(2)),real(C(1)),imag(C(1)));

%% Trial of Maa model
    % Maa approximation (corrected and original)
Phi_Maa_co = @(s)(8*(1+(1/32)*abs(s).^2).^(1/2)-s.^2.*(1+(3^2+(1/2)*abs(s).^2).^(-1/2)));
Phi_Maa_or = @(s)((8*(1+(1/32)*(1i*(s.^2))).^(1/2)-s.^2.*(1+(3^2+(1/2)*(1i*(s.^2))).^(-1/2))));
Ph_Maa_2 = @(s)((8*(1+(1/32)*(1i*sign(imag(s)).*(s.^2))).^(1/2)-s.^2.*(1+(3^2+(1/2)*(1i*sign(imag(s)).*(s.^2))).^(-1/2))));

%% Plot reflexion coefficient
freq=logspace(log10(1),log10(20e3),30);
semilogx(freq,abs(beta(2*pi*freq)));
xlabel('Frequency (Hz)');
ylabel('Abs(beta) (rad)');

%% Plot
mu = 1;
xi = 1e2*exp(-1i*pi/4);

H = @(s)( s.*(2*real(mu)*s+2*real(conj(mu)*xi))./(s.^2+2*real(xi)*s+abs(xi)^2) );
wmin=(1e-4);
wmax=(1e4);
a=figure;
plotBodeDiagram(a,[wmin,wmax],H,'1/2-th order',1e3);

%% DSF de cotan(w0*t)
% DSF(n) = 2*sum[sin(2*n*w0*t)] n>0 (DUPRAZ p.63)
 
N=1e4; % number of points
w0 = 1;
np = [1,25]; % series to display   
%---------------------------------
t = linspace(0,pi,N);
phi = 2*sin(2*kron((1:max(np))',w0*t)); % max(np) modes

leg=cell(0);
clf
hold all
plot(t,cot(t));
leg(end+1)={sprintf('cotan(w0*t)')};
for n=np
    y = sum(phi(1:n,:),1);
    plot(t,y);
    leg(end+1)={sprintf('DSF-%d',n)};
end
axis([0,pi,-3,3]);
%set(gca,'DataAspectRatio',[1,1,1]);
legend(leg);
title(sprintf('Fourier series cotan(w0*t) with w0=%1.2g',w0));
xlabel('t');
ylabel('y');

%% DSF de cotan(w0*t-i*eps) with eps>0
% DSF(n) = 1 + 2*sum[exp(-2*eps*n)*exp(-2*i*n*w0*t)] n>0 (DUPRAZ p.63)
 
N=1e4; % number of points
w0 = 1;
epsi = 0;
np = [1,4]; % series to display
%---------------------------------
t = linspace(0,pi,N);
phi = zeros(max(np)+1,length(t));
phi(1,:) = 1i;
phi(2:end,:) = 2*1i*exp(-2*epsi*kron((1:max(np))',ones(1,length(t)))).*exp(-1i*2*w0*kron((1:max(np))',t));
leg=cell(0);
clf
subplot(1,2,1)
hold all
plot(t,real(cot(w0*t-1i*epsi)));
leg(end+1)={sprintf('cotan(w0*t-1i*eps)')};
for n=np
    y = real(sum(phi(1:n,:),1));
    plot(t,y);
    leg(end+1)={sprintf('DSF-%d',n)};
end
legend(leg);
title(sprintf('Fourier series cotan(w0*t) with w0=%1.2g',w0));
xlabel('t');
ylabel('y');
axis([0,pi,-3,3]);
leg=cell(0);
subplot(1,2,2)
hold all
plot(t,imag(cot(w0*t-1i*epsi)));
leg(end+1)={sprintf('cotan(w0*t-1i*eps)')};
for n=np
    y = imag(sum(phi(1:n,:),1));
    plot(t,y);
    leg(end+1)={sprintf('DSF-%d',n)};
end
legend(leg);
title(sprintf('Fourier series cotan(w0*t) with w0=%1.2g',w0));
xlabel('t');
ylabel('y');
axis([0,pi,-3,3]);

%%
hold all
plot(t,-1i*cot(w0*t-1i*epsi));
leg(end+1)={sprintf('cotan(w0*t-1i*eps)')};
for n=np
    y = sum(phi(1:n,:),1);
    plot(t,1+y);
    leg(end+1)={sprintf('DSF-%d',n)};
end
axis([0,pi,-3,3]);
%set(gca,'DataAspectRatio',[1,1,1]);
legend(leg);
title(sprintf('Fourier series cotan(w0*t) with w0=%1.2g',w0));
xlabel('t');
ylabel('y');

%% Approximation of the Lambda fun
condNumber = @(s)(-s/sqrt(-s));
g = @(s)(sqrt(-s));

%% Comparison between high and low frequency viscosity loss
r_low = 32*nu*l/(c0*d^2); % real part of low Stokes model
r_high = @(w)(4*sqrt(nu)*l/(c0*d*sqrt(2))*sqrt(w)); % real part of high st
w_cuton = 1.84*c0/(d/2); % cut-on pulsation
w = 2*pi*linspace(0,1e5,50);
clf
hold all
leg=cell(0);
plot(w/(2*pi),r_low*w./w);
leg(end+1)={'Low frequency'};
plot(w/(2*pi),r_high(w));
leg(end+1)={'High frequency'};
leg(end+1)={'High frequency'};
xlabel('Frequency (Hz)');
ylabel('Re(Z) (rad)');
title(sprintf('Cut-on frequency: %1.1e Hz',w_cuton/(2*pi)));
legend(leg);

%% Dispersion relation Tam (passivity condition)
a0=1;
a1=1e2;
a11=1e2;
Z = @(s)((1i./s).*(a1*s.^2+a0*1i*s+a11)); % Tam

poles = linspace(-10,10,1e2);
clf
plot(real(Z(poles)),imag(Z(poles)),'-');
xlabel('Re(Z)');
ylabel('Im(Z)');

%% Dispersion relation Tam
a0=1;
a1=1;
a12=-1;
k=1;
%f = @(s)((-a0+a1*j*s-a12*sqrt(-1i*s)).*sqrt(s.^2-k^2)./s);
H = @(s)(s.^2./sqrt(s.^2-k^2)+a0*s-1i*a1*s.^2); % Tam
%H = @(s)(-sqrt(s.^2-k^2)./s.*(a0+a12*sqrt(-s)-a1*s)); % Fractionnaire


r_p = 0;
r_c = 1.46;
theta_c = linspace(0,0.99*pi,1e2);
s_c = r_p + r_c*exp(1i*theta_c);
s_c = 1.2i+linspace(-2,2,1e3); % contour in the w-plane
s_f = -1i*linspace(0,10,10); % forbiden contour in the F-plane
%s_f = 1; % forbiden contour in the F-plane

clf
subplot(2,1,1)
hold all
plot(real(s_c),imag(s_c));
plot(real(k),0,'ro');
plot(-real(k),0,'ro');
title('Contour in the w-plane');
xlabel('Real(w)');
ylabel('Imag(w)');
xlim([-4,4])
ylim([-4,4])
set(gca,'DataAspectRatio',[1,1,1])
subplot(2,1,2)
hold all
plot(real(H(s_c)),imag(H(s_c)));
plot(real(s_f),imag(s_f),'ro');
title('Contour in the F-plane');
xlabel('Real(F)');
ylabel('Imag(F)');
xlim([-4,4])
ylim([-4,4])
set(gca,'DataAspectRatio',[1,1,1])

%% Miki impedance model
p = 0.160;
q = 0.109;
res = -0.618;

z1 = @(f)(1+(q-1i*p)*f.^res);
z2 = @(f)(1+q/cos(b*pi/2)*(1i*f).^res);
condNumber = linspace(0,1,1e2);
clf
hold all
plot(real(z1(condNumber)),imag(z1(condNumber)),'r-');
plot(real(z2(condNumber)),imag(z2(condNumber)),'b-');
legend('Mikki','Reformulation');
set(gca,'DataAspectRatio',[1,1,1])

%% Jump in the phi-plane
a = 2;
Z(poles)  
Phi = @(s)((s.^2)./(1+a*s)); % for s!=>1/a
Phi = @(s)(s.^2);
Y = @(x,y)(imag(Phi(-x+1i*y)));

x_k = linspace(-4*(1/a),-1/a,1e2) + 1i*1e-2;
plot(real(x_k),imag(Phi(x_k)));

%% Bruneau wave number for various cavity diameter
Pr = 0.73; % Prandtl number (rad)
dcav = [4,8,9.5,15,20]*1e-3; % Cavity diameters (rad)
lc = 20e-3; % Cavity length (m)
sigma = 5/100; % porosity (rad)
l = (1-)*0.8*1e-3; % perforation length (m)
d = 0.3*1e-3; % diameter (m)
w = linspace(1,3500,1e3);
    % Comparison of the propagation wavenumbers
figure
leg=cell(0);
hold all
for res=1:length(dcav)
    plot(w/(2*pi),real(z_cr(l,d,w)./sigma + coth(1i*lc*k_br(dcav(res),w,Pr))));
    leg(end+1)={sprintf('Bruneau dc=%2g mm',dcav(res)*1e3)};
end
plot(condNumber,Re);
leg(end+1)={'Expe'};
xlabel('Frequency (Hz)');
ylabel('Real(Z/Z_0) (rad)');
title(sprintf('Cavity impedance w/o perforation (lc=%2g mm)',lc*1e3));
legend(leg);
xlim([0,1e3])
ylim([0,2])

%% Poles and residual of Webster-Lokshin
% H(s) = e^-eps*sqrt(s) x [1-r*e^(s+eps*sqrt(s))]^-1 r!=0 and eps>0
clc
r = 1e-1;
epsi = 4;
[sn,rn] = computePolesWebsterLokshin(r,epsi,10);
plot(sn,'o');
axis([-5,0,-5,5])
%% Diffusive weight of FP Cavity
a0 = 4;
a1 = 1e-5*1/340;
aa = 1e1;
P = @(xi)(exp(-2*lc*a1*xi)-2*exp(-2*a0*lc).*cos(2*aa*lc*xi.^alpha)+exp(2*lc*(-2*a0+a1*xi)));
xi = logspace(-14,4,1e2);
semilogx(xi,P(xi));
legend(sprintf('Minimum value: %1.1e',min(P(xi))));
xlabel('xi');
ylabel('mu(xi)');
%% Diffusive weight of Webster-Lokshin
r = 0.1;
c = -log(sqrt(abs(r)));
ecrit = pi/(2*sqrt(c));
condNumber = @(xi,eeps)(r^2-2*r*exp(-2*xi).*cos(2*eeps.*sqrt(xi))+exp(-4*xi));
x_k = linspace(0,5,1e2); % xi
y = linspace(1e-4,1e1,1e1); % eps
[X,Y] = meshgrid(x_k,y);
zmax = 1e1;
Z = condNumber(X,Y);
surf(X,Y,Z)
view([0,0]);
xlabel('xi');
ylabel('eps');
title(sprintf('r=%1.1e, Minimum value is %1.1e',r,min(Z(:))));
%% Diffusive weight of FP Cavity (2)
%r = 1e-4;
%mu8 = @(xi,eeps)((1/pi)*exp(-2*xi).*(r+exp(-2*xi)).*sin(eeps.*sqrt(xi))./(r^2-2*r*exp(-2*xi).*cos(2*eeps.*sqrt(xi))+exp(-4*xi)));

x_k = linspace(0,4,1e3); % xi
y = linspace(1e-4,5,1e1); % eps
[X,Y] = meshgrid(x_k,y);
zmax =1e-1;
Z = mu8(X,Y);
surf(X,Y,Z,'edgecolor','none')
caxis([max(-zmax,min(Z(:))) min(zmax,max(Z(:)))])
view([45,45]);
xlabel('xi');
ylabel('eps');
title(sprintf('r=%1.1e, Minimum value is %1.1e',r,min(Z(:))));
zlim([-2,2])
%% X
a1 = 1/c0;
aa = a1;
r = 1;
f = @()()
%% Second order system
xik = 1e1;
muk = 1;
F_1st = @(s)(F_pc(s,[],[],xik,muk));
sn = [1e1+1i;1e1-1i];
rn = [1;1];
F_2nd = @(s)(F_pc(s,sn,rn,[],[]));
wmin=2*pi*(1e-1);
wmax=2*pi*(1e3);
Np = 1e3;
condNumber=clf;
plotBodeDiagram(condNumber,[wmin,wmax],F_1st,'F 1st (xik=1e1)',Np);
plotBodeDiagram(condNumber,[wmin,wmax],F_2nd,'F 2nd (sn=1e1+1i)',Np);
%% Test semilogxComplex
Np = 1e3;
w = 2*pi*logspace(-1,4,1e3);
condNumber=clf;
% leg=cell(0);
plotComplex(condNumber,w,F_an(1i*w),sprintf('Analytical:%s',name),'r--');
plotComplex(condNumber,w,F_pc(1i*w,sn_interp,rn_interp,xik_interp,muk_interp),sprintf('Interp. Nxi=%d Npoles=%d',length(xik_interp),length(sn_interp)),'ko');
plotComplex(condNumber,w,F_pc(1i*w,[],[],xik_interp,muk_interp),sprintf('Interp. Nxi=%d Npoles=%d',length(xik_interp),0*length(sn_interp)),'');
%semilogx(w,F_pc(f,1i*w,sn_interp,rn_interp,[],[]),sprintf('Interp. Nxi=%d Npoles=%d',0*length(xik_interp),length(sn_interp)));
xlabel('w (rad/s)');
ylabel('Re(F(jw))');
title(sprintf('F(s) (%s)',name));
ylim([-0.1,0.1]);

%% Plot
x_k = linspace(-1,10,2e2);
y_k = linspace(-10,10,2e2);
[X,Y] = meshgrid(x_k,y_k);
S = X + 1i*Y;
Z = 10*S.^2;
%Z = h2_an(S);
clf
subplot(2,1,1)
surf(X,Y,real(Z),'edgecolor','none');
view([0,90])
colorbar
%%
x = 2*pi*linspace(0,1e5,1e2);
h1_an = @(s)(-2./polesEq(s));
Beta = h1_an(1i*x);
Z = (1+Beta)./(1-Beta);
figure(3)
clf
hold all
plot(x,real(Z),'DisplayName','h1');
Beta = 1+h1_an(1i*x);
Z = (1+Beta)./(1-Beta);
plot(x,real(Z),'DisplayName','1+h1');
title('Re(z1)');
legend('show');
%% BOunding box issues with MATLAB export

mesh.show2export();
set(gca, 'Position', get(gca, 'OuterPosition') - ...
get(gca, 'TightInset') * [-1 0 1 0; 0 -1 0 1; 0 0 1 0; 0 0 0 1]);
print(gcf, '-dpdf', 'my-figure.pdf');


%%

set(gcf, 'PaperSize', [6.25 7.5]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperPosition', [0 0 6.25 7.5]);

%%
print(gcf, '-dpng', 'my-figure.png','-r500');

%%
fig = gcf;
fig.PaperPositionMode = 'auto'
fig_pos = fig.PaperPosition;
fig.PaperSize = [fig_pos(3) fig_pos(4)];

%% Multi-pole vs delayed multi-pole
sk = 1i;
mu = 1+1i;
dt = 1;
H = @(s)(mu./(s-sk) + mu'./(s-sk'));
H_delay = @(s)(1+exp(-dt*s).*(mu./(s-sk) + mu'./(s-sk')));
Z_cav = @(s)(coth(1+s));
gh = clf;
w = linspace(0,2e1,1e2);
plotComplex(gh,w,H(1i*w),'','No delay');
hold all
%plotComplex(gh,w,H_delay(1i*w),'','Delay');
%plotComplex(gh,w,Z_cav(1i*w),'','Cavity');

%% Write matrix to HDF5
h5create('myfile.h5','/DS1',[10 20])
A = rand(10,20);
h5write('myfile.h5', '/DS1', A);
h5disp('stateX.h5');
%% Element connectivity

%% Mex files

Nx = 200;
F = sparse(1,1,0,Nx,Nx);
x_k = linspace(0,1,Nx);
[X,Y] = meshgrid(x_k,x_k);

for i=1:length(X)
   F(i,i) =  1;
end

%% Test 2D quadrature rule
condNumber = @(r,s)((r.^2+s.^2).*((r>=-1).*(s>=-1).*((r+s)<=0)));
integral2(condNumber,-1,1,-1,1)
[r,s,w] = Cubature2D(8);
w'*condNumber(r,s)

%% Test parfor
P = 1e5;
Q = 10;
clear C
t = cputime;
parfor i=1:P
for j=1:Q
        % block n°(i,j) in the block matrix
    C(i,:) = ones(1,10);
end 
end
disp(cputime-t)
    %% Second way
A = diag([1,2]);
B = rand(4);
[ma,na] = size(A);
[mb,nb] = size(B);
   % Both inputs full, result is full.
A1 = reshape(A,[1 ma 1 na]);
B1 = reshape(B,[mb 1 nb 1]);
C1 = bsxfun(@times,A1,B1);
K = reshape(C1,[ma*mb na*nb]);

%% Build block diagonal sparse matrix
n = 2;
B = cell(1,n); % list of (sparse) blocks
for i=1:n
   B{i} = i*ones(2,2); 
end
finalresult = blkdiag(B{:});

%% I - Build A(xj) - First way
Nq = 2; % no. of variables
Np = 5; % no. of point per cell
Ax = @(x,y)((x+y)*ones(Nq));
x = [linspace(-1,1,Np)',linspace(-1,1,Np)'];
clc
for i=1:length(x)
   %fprintf('x_%d: %s\n',i,mat2str(Ax(x(i,1),x(i,2)))); 
end
Sr = rand(2,Np);
C1 = buildBlockMatrixFun2D(Sr,x,Ax);
    % Speedier alternative to arrayfun
condNumber = @(x,y)((x+y)*ones(Nq));
x = [linspace(-1,1,Np)',linspace(-1,1,Np)'];
B = arrayfun(condNumber,x(:,1),x(:,2),'UniformOutput', false);

%% Weight for diffusive space
clf
hold all
xi = linspace(0,5,1e2);
leg = cell(0);
plot(xi, (xi.^2)./(1+xi));
leg{end+1}=sprintf('xi^2/(1+xi)');
%plot(xi, 1+xi);
%leg{end+1}=sprintf('1+xi');
plot(xi, 1./(1+xi));
leg{end+1}=sprintf('1/(1+xi)');
%plot(xi, (xi.^2)./((1+xi.^2).*(1+xi)));
%leg{end+1}=sprintf('xi^2/((1+xi^2)*(1+xi))');
xlabel('xi');
title('Weight for Fractional integral');
legend(leg);

%% Passivity with complex poles
sn = -1+i*1/10;
rn = 1-150*1i*imag(sn);
H = @(s)(rn./(s-sn)+conj(rn)./(s-conj(sn)));
w = linspaces(-1,1,1e2);
plot(w,real(H(1i*w)));
xlabel('omega (rad/s)');
ylabel('Re(h)');

%% ss
syms k k0 k1 k2
%assume(k>0 & k<1);
%assume((k0^2 + k1^2)<1);
%assume(k1>0 & k1<1);
W = sym([1,k;k,1]);
Q = sym((1/2)*[sqrt(1+k)+sqrt(1-k),sqrt(1+k)-sqrt(1-k);sqrt(1+k)-sqrt(1-k),sqrt(1+k)+sqrt(1-k)]);
W = sym([1,0,0,k0;
        0,1,0,k1;
        0,0,1,k2;
        k0,k1,k2,1]);
W = sym([1,0,k0;
    0,1,k1;
    k0,k1,1]);
%% 
s1 = 0.1+0i;
s0 = 1+0i;
h=@(s)(1./((s-s1).*(s-s0)));
w = linspace(0,5,1e2);
clf
plot(w,abs(h(1i*w)));

%% Poles of delay system
tau = 1;
a = 4;
b = 0.99*abs(a)*exp(1i*0.2);
h = @(s)(a+b*(exp(-s*tau)));
fun = @(x)(abs(h(x(1)+1i*x(2))));
%fun = @(x)((x(1)+1)*(1+x(2)));
x0 = [0,0];
x = fsolve(fun,x0)
fprintf('Found root: s=%d + %d*i\n',x(1),x(2));
fprintf('Value: |h(s)|=%d\n',fun(x));
plot(x(1),x(2),'o')
xlabel('R(s)');
ylabel('I(s)')
axis([-1,1,-1,1])

%%
a1 = 0.1; tau = 8;
a0 = a1*sqrt(tau);
x = linspace(-1,1,1e4);
condNumber = @(s)(a0+a1*exp(-s*tau)./sqrt(s));
plot(x,real(condNumber(1i*x)))
xlim([min(x),max(x)])
%ylim([-1,1])
xlabel('Im(s=1i*w)');

%%
condNumber = @(x)(abs(x).^(-1/2).*cos(x*tau+sign(x)*pi/4));
g = @(x)(real((1./sqrt(1i*x)).*exp(-1i*x*tau)  )  );
x = linspace(-1,1,1e2);
plot(x,condNumber(x),x,g(x))
legend('Psi*cos','Real part')

%%
clear w f
syms w
assume(w >= 0)
condNumber(w) = w^(-1/2)*cos(+w*tau+pi/4);
solve(diff(condNumber),w)

%% Surface wave
alpha = 0.5+3.5i;

r = linspace(0,1,1e4);
p = besselj(0,alpha*r);
plot(r,real(p));
xlabel('r/b');
ylabel('Pressure');
axis([0,1,0,5]);
title(sprintf('alpha=%d +1i*%d',real(alpha),imag(alpha)))

%% Eigenvalue scaling
%Lambda = eigs(A);
    % Estimate of largest eigenvalue
v = norm(c,2);
Lc = min(mesh.charLength);
Cl = 3/DGCell.dr;
Lambda = Cl*(v/Lc);
opts.tol = 1e-3;
Lambda_exact = abs(eigs(A,1,'lm',opts));
fprintf('Error on largest eigenvalue: %1.2g%%\n',100*(Lambda - Lambda_exact)./Lambda_exact);
fprintf('Estimate of Cl coeff.: %1.2g\n',(Lc/v)*Lambda_exact*DGCell.dr);

%% Function with optional arguments
k = 1; % longitudinal wavenumber
U0 = 0.1; % uniform flow
z0 = 1;
c0 = 1;
n = 2; % mode index
m = 2;
xdg = DGMesh.getNodesCoordinates();
alpha = n*pi/mesh.bound(2,1);
beta = m*pi/mesh.bound(2,2);
p = cos(alpha*xdg(:,1)).*sin(beta*xdg(:,2));
    % Find omega
omega = roots([1,-2*U0*k,(U0^2-c0^2)*k^2-c0^2*(alpha^2+beta^2)]);
omega = omega(real(omega)>=0);
Omega = omega-U0*k;
    % Recover acoustic field
rho0 = z0/c0;
u = k/(rho0*Omega)*p;
v = alpha/(1i*rho0*Omega)*sin(alpha*xdg(:,1)).*cos(beta*xdg(:,2));
w = beta/(1i*rho0*Omega)*cos(alpha*xdg(:,1)).*sin(beta*xdg(:,2));
    % Vector format
x = transpose([u,v,w,p]);
x = reshape(x,[],1);

%% Test
x = linspace(-1,1,1e3);
t = 1e2;
a = @(x)((1-x.^2).^5);
a = @(x)((1-(x/1.5).^2).^5);
u = @(x,t)(u0((2/pi)*atan(exp(-t).*tan((pi*x-1)/2))+1/pi));
y = -35./(256*(x+1))+1./(128*(x-1).^4)-5./(192*(x-1).^3)-35./(256*(x-1))+(35/256)*log(x+1)-15./(256*(x+1).^2)-(35/256)*log(x-1)+15./(256*(x-1).^2)-1./(128*(x+1).^4)-5./(192*(x+1).^3);
%a = @(x)(f(pi*x-1)./pi);
clf
hold on
%plot(x,a(x),'--');
plot(x,y);
%plot(x,u0(x),'k-');
%plot(x,u0(pi*x),'k--');
%plot(x,u(x,t),'r-');
%plot(x,1-x./x,'--');
%plot(x,u(x,t));
%plot(x,cos(pi*x),'--');
%plot(x,cos(pi^3*t+pi*x),'--');
axis([-1,1,-1,1])

%% Build global formulation
Np = 3;
Nk = 5;
Nx = 3;
idx_d = [1,3];
Nd = length(idx_d);
clc
F = rand(Np*Nk,1);
F
    %% First construction
Delta = sparse(1:Nd,idx_d,ones(1,length(idx_d)),Nd,Nx); % Nd x Nx
Delta = reshape(Delta',1,[]); % 1 x (Nd*Nx) 
Delta = kron(F,Delta); % (Np*Nk) x (Nd*Nx)
Delta = reshape(Delta,[],Nx);
full(Delta)
    %% Alternative construction
i = 1:(Np*Nk*Nd);
j = ceil(i./(Np*Nk));
Delta = sparse(i,idx_d(j),repmat(F,[Nd,1]),Np*Nk*Nd,Nx);
full(Delta)

%%
N_tau = 3;
Nh = 4;
Nx = 7;
F = rand(Nh,1);
C = rand(N_tau,Nx);
%%
clc
Rex = zeros(0);
for i=1:N_tau
    Rex=vertcat(Rex,kron(F,C(i,:)));
end
%% First method (not sparse friendly)
C_r = permute(C,[3,2,1]); % First dim -> Third dim
R = bsxfun(@times,sparse(F),sparse(C_r)); % kronecker product
    % concatenate along third dimension
catThirdDim(R)
%% Implicitely Restarted Arnoldi Iteration
clear opt
opt.isreal = 1;
opt.tol = 1e-6;
%opt.maxit = 100;
opt.disp = 1;
tic
R = eigs(A,20,0,opt);
toc
clf
hold on
plot(R,'o');
title('Spectrum')
xlabel('Real(Lambda)');
ylabel('Imag(Lambda)');
%axis([-1,1,-1,1]*1e-2)
plot(0*ylim,ylim,'--');
plot(xlim,0*xlim,'--');

%% Surface plot
x_k = linspace(-50,1,1e2);
[X,Y] = meshgrid(x_k,x_k);
Z = max(4*(1-X).*(1-Y)-(X+Y).^2,0);
figure(1)
surf(X,Y,Z,'EdgeColor','none');
xlabel('x')
ylabel('y')
view(0,90)

%% Condition number
a0 = 1e1; n = [1]; n = n/norm(n);

r = [];
for i=1:length(x)
    r(i) = cond(Zdata(a0,x(i),n));
end
clf
plot(x,r,'o');
ylim([0,100])
ylabel('cond(M(alpha))');
xlabel('alpha');
title(sprintf('a0=0%1.2g,beta0=%1.2g',a0,beta_fun(a0)));

xlabel('a_{0} (resistance)');
ylabel('alpha (flux matrix parameter)');
ylim([-2,2])

%% Plot solution
it = 20; % iteration to plot
var = 3; % variable to plot
% --
it = min(it,size(x,2));
if ~exist('t') || isempty(t)
   t = 1; 
end
Np = DGCell.Np; Nk = mesh.N;
x_plot = DGMesh.interpolateField2PlotNodes(cst.Nq,x(:,it));
x_plot = x_plot(var + cst.Nq*(0:(Np*Nk-1)));
clf
trisurf(trianglesPlot,nodesPlot(:,1),nodesPlot(:,2),real(x_plot),'EdgeColor','None');
x_length = 500; aspratio = 0.3; 
set(gca,'position',[0 0 1 1],'units','normalized');
set(gcf, 'Position', [1000 500 x_l 0.3*x_l]);
axis off
colormap(jet)
view(0,90);
%% fft
Fs = 1000;            % Sampling frequency
T = 1/Fs;             % Sampling period
L = 1000;             % Length of signal
t = (0:L-1)*T;        % Time vector
S = 0.7*sin(2*pi*50*t) + sin(2*pi*120*t);
X = S + 2*randn(size(t));
plot(1000*t(1:50),X(1:50))
title('Signal Corrupted with Zero-Mean Random Noise')
xlabel('t (milliseconds)')
ylabel('X(t)')
Y=fft(X);
P2 = abs(Y/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;
plot(f,P1)

%% Fit base flow with third order polynomial
Mc = 0.475; % centerline Mach
Ma = 0.4; % average Mach
h = 0.15; % duct height

% p(x) = a(1)x^3+a(2)x^2+a(1)*x
A = [(h/2)^3,(h/2)^2,h/2; % f(h/2)
    ((h/2)^3)/4,((h/2)^2)/3,((h/2)^1)/2; % mean over [0,h/2]
    3*(h/2)^2,h,1]; % f'(h/2)
a = A\[Mc;Ma;0];
x = linspace(0,h/2,1e2);
clf
plot(x,polyval([a(:);0],x));
xlabel('y');


%% Hyperbolic velocity profile
% From (Khamis & Brambley, 2017) Eq. (2.4)
Zdata = 0.475; % centerline Mach
delta = 0.228; % boundary layer thickness
%--
clf
    % r in [-1,1]
U = @(r)Zdata*tanh((1-abs(r))*(1/delta))+Zdata*(1-tanh((1/delta))).*((1/delta)*abs(r).*(1+tanh((1/delta)))+1+abs(r)).*(1-abs(r));
dtanh = @(x)1-tanh(x).^2;
dU = @(r)-Zdata/delta*sign(r).*dtanh((1-abs(r))*(1/delta))+Zdata*(1-tanh((1/delta))).*((1/delta)*(1+tanh((1/delta)))+1).*sign(r).*(1-abs(r))+Zdata*(1-tanh((1/delta))).*((1/delta)*abs(r).*(1+tanh((1/delta)))+1+abs(r)).*(-sign(r));
    % y in [min(y),max(y)]
U = @(y)U(2/(max(y)-min(y))*(y-(max(y)+min(y))/2));
dU = @(y)2/(max(y)-min(y))*dU(2/(max(y)-min(y))*(y-(max(y)+min(y))/2));
fprintf('Hyperbolic profile. Centerline Mc=%1.2e. Average: Ma=%1.2e\n',Zdata,integral(U,0,1));
y = linspace(-4,4,1e2);
hold all
plot(y,U(y));
plot(y,dU(y));

%%
z0=2;
a = [1,1,1];
b = [1,1,1];
P = @(s)a(1)+a(2)*sqrt(s)+a(3)*s;
Q = @(s)b(1)+b(2)*sqrt(s)+b(3)*s;
z_an = @(s)(P(s)+z0*coth(Q(s)));
beta_an = @(s)(z2beta(z_an(s)));
y_an = @(s)(1./z_an(s));

R = @(s)z0+P(s)+(z0-P(s)).*exp(-2*Q(s));
h1 = @(s)1./R(s);
h2 = @(s)-exp(-2*b(1) - 2*b(2)*sqrt(s))./R(s);

omega = linspace(1,10,1e3);
clf
hold all
plot(omega,real(y_an(1i*omega)));
plot(omega,real(h1(1i*omega)+exp(-2*b(3)*1i*omega).*h2(1i*omega)));

%% Characteristic equation for fractional DDE
A = -1;
B = 1;
tau = 1;
C = 1;

f = @(s)(abs(s - A - B*exp(-s*tau) - C*sqrt(s)));

x = linspace(-5,1,1e2);
y = linspace(-5,5,1e2);
[Sr,Si] = meshgrid(x,y);
S = (Sr+1i*Si);
Z = abs(f(S));
surf(Sr,Si,Z,'edgecolor','none');
view([0,90])
xlabel('Re(s)');
ylabel('Im(s)');
caxis([0,1]);
colorbar

%% Z & Y formulation
x = linspace(0,1,1e2);
y = linspace(0,1,1e2);
[X,Y] = meshgrid(x,y);
Z = Y./(X+Y);
surf(X,Y,Z,'edgecolor','none');
view([0,90])
xlabel('X');
ylabel('Y');
axis([0,1,0,1,0,5])
caxis([0,10]);
colorbar

%% Admittance for Cummings model
a0 = 1;
Cd = 1;
Z = @(u)(a0*u+Cd*abs(u).*u);
Y = @(p)(2*p./(a0+sqrt(a0^2+4*Cd*abs(p))));
R = @(v)(1+sqrt(1+4*Cd*abs(v)/((1+a0)^2)));
beta = @(v)(((a0-1)/(a0+1))*2*v./R(v) + Cd/((1+a0)^2)*4*abs(v).*v./(R(v)).^2);
u = linspace(-1e2,1e2,1e2);
plot(u,Z(u)-u-beta(Z(u)+u))
xlabel('u')
%% Duct length without reflection
f_source = 1e3; % source in Hz
nper = 50; % number of period
c0 = 344.3181; % speed of sound (m/s)
Lduct = Lduct_exp; % experimental duct length (m)
Lnum = (nper/f_source*c0 + Lduct)/2

%% Bandpass Butterworth filter
% Design a 20th-order Butterworth bandpass filter with a lower cutoff 
%frequency of 500 Hz and a higher cutoff frequency of 560 Hz. Specify a 
% sample rate of 1500 Hz. Use the state-space representation. Design an 
% identical filter using designfilt.


%% Compute SPL of source
T_source = 20;
t = linspace(0,15*T_source,1e3);
pref = 1;

f = @(t) 10^(120/20)*sin(2*pi*t/T_source)*sqrt(2);
20*log10(rms(f(t)))


%% Mach=0.15

y = [0 .5 1 1.5 2 1.5 1 .5 0 -.5 -1 -1.5 -2 -1.5 -1 -.5 0];
N = length(y);
L = 5*1;
M = N*L;
x = 0:L:L*N-1;
xi = 0:M-1;
yi = interpft(y,M);
clf
plot(x,y,'o',xi,yi,'-')
legend('Original data','Interpolated data')
%% Curve fitting
    % Load experimental data
x = M0y; x=x(:);
y = M0_exp_mean; y=y(:);
    % Remove end points
x = x(4:(end-1));
y = y(4:(end-1));
    % Null values at endpoint
x = [50;x;0];
y = [0;y;0];
f=fit(x,y,'smoothingspline');
Mavg = 0.335;
    % Correct average Mach
Mavg_fit = integrate(f,max(x),min(0))/(max(x)-min(x));
M0_fit = @(y)Mavg/Mavg_fit*feval(f,y);
dM0_dy_fit = @(y)Mavg/Mavg_fit*differentiate(f,y);
clf
subplot(2,1,1)
x1 = linspace(0,50,1e3);
x = min(M0y,50); x=x(:);
y = M0_exp_mean; y=y(:);
plot(x1,M0_fit(x1),x,y,'o');
xlabel('y (mm)');
ylabel('M');
subplot(2,1,2)
plot(x1,dM0_dy_fit(x1));
xlabel('y (mm)');
ylabel('dM/dy (/mm)');

%%
x = M0y; x=x(:);
y = M0_exp_mean; y=y(:);
%%
clf
plot(x,y,'o')
%% Phase plot
clf
hold all
plot(freq,angle(beta_an(s,coeffModel_phys)),'b','DisplayName',sprintf('%s (Physical coefficient)',Model_name));
plot(freq,angle(beta_an(s,coeffModel_optim)),'r','DisplayName',sprintf('%s (Optimized coefficient)',Model_name));
plot(Exp_freq,angle(z2beta(Exp_z)),'kx','DisplayName',sprintf('%s',Exp_name));
xlabel('Frequency (Hz)');
ylabel('|reflection coefficient| (rad)');
legend('show')
%% Source and energy analysis
    % Gaussian-modulated sinusoidal pulse
        % fc: central frequency (Hz)
        % bw: frequency bandwidth (Hz)
        % bwr: attenuation at which source stops (dB)
fc = 6e3;     % fc: central frequency (Hz)
bw = 0.3;    % bw: frequency bandwidth (Hz) at bwr
bwr = -15;     % bwr: reference level (dB)
tsrc = 1.2*gauspuls('cutoff',fc,bw,bwr);
f = @(t)GaussianWavePacket(t,fc,bw,bwr).*(t<=tsrc);
t = linspace(0,tsrc,1e3);
clf
subplot(2,1,1)
hold all
plot(t,f(t))
%plot(t,sin(2*pi*fc*t));
xlabel('time (s)');
title('Source');
subplot(2,1,2)
fs = 1/max(diff(t)); % sample frequency (Hz)
[psd,freq] = periodogram(f(t),rectwin(length(t)),length(t),fs);
plot(freq,10*log10(psd/max(psd)));
xlim([0,3e3]);
ylim([-50,0])
grid on
xlabel('frequency (Hz)');
title('Power spectral density (dB/Hz)');
c0 = 340.29;
fprintf('Required length for impedance tube: %1.2e m\n',max(t)*c0/2);
%% PPW hyperbolic realization
Np = 2; % number of node per element
N = 5; % number of element
f = 3e3; % (Hz) max frequency of interest
delay = 2*85.6*1e-3/340; % (s) delay
PPW = Np*N/(delay*f)

%% Check limit of diffusive integral for Webster-Lokshin
epsi = 0.0000000001; % damping parameter epsilon, positive
r = 0.1; % global reflexion coefficient (real)
    % Analytical weight, defined over xi>0
mu_an = @(xi,epsi)((1/pi)*(exp(-4*xi)+r*exp(-2*xi)).*sin(epsi*sqrt(xi))./(exp(-4*xi)-2*r*exp(-2*xi).*cos(2*epsi*sqrt(xi))+r^2));
mu_anIntegral = @(s,epsi)(integral(@(xi)(mu_an(xi,epsi)./(s+xi)),0,inf,'RelTol',1e-10,'AbsTol',1e-10));
h = @(s,epsi)(exp(-epsi*sqrt(s))./(1-r*exp(-2*(s+epsi*sqrt(s)))));
clc
espiArray = linspace(0,1,6e2);
mu_anIntValue = zeros(size(espiArray));
for i=1:length(espiArray)
    mu_anIntValue(i) = real(mu_anIntegral(1i*6,espiArray(i)));
end
omegaArray = linspace(0,10,50);
mu_anIntValue2 = zeros(size(omegaArray));
for i=1:length(omegaArray)
    mu_anIntValue2(i) = real(mu_anIntegral(1i*omegaArray(i),1e-4));
end
clf
subplot(3,1,1)
xi = linspace(0.05,2,1e5);
plot(xi,mu_an(xi,epsi)./xi);
xlabel('xi');
ylim([0,1])
title('mu_an/xi');
subplot(3,1,2)
plot(espiArray,mu_anIntValue);
xlabel('eps');
title('Integral of mu(xi)/xi');
subplot(3,1,3)
plot(omegaArray,mu_anIntValue2);
xlabel('omega');
title('Integral of mu(xi)/(1i*omega+xi)');

%fprintf('%1.4e\n',F_diff_fun(10));

%% Nonlinear scattering operator
Cd = 1; a0 = 1; c0 = 340;
beta_Cummings_denom = @(v)(1+sqrt(1+4*Cd*abs(v)./((1+a0)^2*c0)));
beta = @(v)(beta_fun(a0)*2*v./beta_Cummings_denom(v)+(Cd/(c0*(1+a0)^2))*4*abs(v).*v./(beta_Cummings_denom(v).^2));
Z = @(u)a0*u+(Cd/c0)*u.*abs(u);

v = linspace(0,5,1e3);

figure(5)
clf
hold all
plot(v,beta(Z(v)+v)-(Z(v)-v));
xlabel('v');
ylabel('Delta');

%%
b1 = 2.549592e-04;
fprintf('Delay: %1.3e\n',2*b1);
%% List of TDIBC
    % TDIBC A
load('GFIT-MP-SPL120dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-6Oscillatory2DiffusivePolesFinal.mat','TDIBC');
TDIBC.printPolesAndWeights(6);
[~,~,~,xi,sn]=TDIBC.getPolesandWeights();
fprintf('Max pole: %1.3e kHz\n',max([xi(:);abs(sn(:))])/(2*pi*1e3));
    %% TDIBC B
load('GFIT-MP-SPL120dB-Mavg271-2DFEM_TDIBC_ReflCoeff-ExpeOptim-6Oscillatory1DiffusivePoles.mat','TDIBC');
TDIBC.printPolesAndWeights(6);
[~,~,~,xi,sn]=TDIBC.getPolesandWeights();
fprintf('Max pole: %1.3e kHz\n',max([xi(:);abs(sn(:))])/(2*pi*1e3));
    %% TDIBC C
load('GFIT-MP-SPL120dB-Mavg271-2DFEM_TDIBC_ReflCoeff-ExpeOptim-2OscillatoryPolesPhysivalValueRecovery.mat','TDIBC');
TDIBC.printPolesAndWeights(6);
[~,~,~,xi,sn]=TDIBC.getPolesandWeights();
fprintf('Max pole: %1.3e kHz\n',max([xi(:);abs(sn(:))])/(2*pi*1e3));
    %% TDIBC D
load('GIT-CT57-SPL130dB-Mavg000-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4OscillatoryPolesFinal.mat','TDIBC');
TDIBC.printPolesAndWeights(6);
[~,~,~,xi,sn]=TDIBC.getPolesandWeights();
fprintf('Max pole: %1.3e kHz\n',max([xi(:);abs(sn(:))])/(2*pi*1e3));
    %% TDIBC E
load('GIT-CT57-SPL130dB-Mavg400-2DFEM_TDIBC_ReflCoeff-ExpeOptim-2OscillatoryPolesFinal.mat','TDIBC');
TDIBC.printPolesAndWeights(6);
[~,~,~,xi,sn]=TDIBC.getPolesandWeights();
fprintf('Max pole: %1.3e kHz\n',max([xi(:);abs(sn(:))])/(2*pi*1e3));
    %% TDIBC F
load('GIT-CT57-SPL130dB-Mavg400-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4Oscillatory2DiffusivePoles900HzFinal.mat','TDIBC');
TDIBC.printPolesAndWeights(6);
[~,~,~,xi,sn]=TDIBC.getPolesandWeights();
fprintf('Max pole: %1.3e kHz\n',max([xi(:);abs(sn(:))])/(2*pi*1e3));


%% Verify the representation (B)
G = @(s)(1./(1-r*exp(-2*s)));
omega = linspace(-1,1,1e2);

G_osc_fun = @(s)(residue(1)+((get1stOrderUnitaryFilter(s,-poles(2:end),'type','high-pass'))*residue(2:end)));
G_osc = G_osc_fun(1i*omega);
figure(2)
clf
subplot(1,2,1)
hold all
l
eg = cell(0);
plot(omega,real(G(1i*omega)));
leg{end+1}='F';
plot(omega,real(G_osc),'--');
leg{end+1}='F_{osc}';
xlabel('omega w');
title('Re(F(1i*w))');
%legend(leg);
subplot(1,2,2)
hold all
plot(omega,imag(G(1i*omega)));
plot(omega,imag(G_osc),'--');
xlabel('omega w');
title('Im(F(1i*w))');
%% Plot first term at w=0
Cst = 1/(1-r) + sum(1./(2*poles));

idx = 1:5e2;
Cst = 1/(1-r) + 1/log(r) + 2*log(r)*sum(1./(log(r)^2+(2*pi*idx).^2))
Cst = 1/(1-r)+0.5*coth(log(r)/2)

%%
clf
t = linspace(0,1,1e2);
plot(t,1./(1-t)+0.5*coth(log(t)/2));

%%
N = 1e5;
n = -N:N;
rho = -20;
sn = log(sqrt(rho)) + 1i*n*pi;
clc
0.5*sum(1./sn(:))
0.5*(1+rho)/(rho-1)
clc
sn = log(sqrt(abs(rho))) + 1i*(n+0.5)*pi;
clc
0.5*sum(1./sn(:))

coth(log(abs(rho)))-0.5*coth(log(sqrt(abs(rho))))
0.5*(1+rho)/(rho-1)
%%
coth(log(sqrt(abs(rho))))-(rho-1)/(1+rho)

%% Check series value
N = 1e7;
a = 1e2;
idx = 1:N;
sum(1./(idx.^2+a^2))-(-1./(2*a.^2)+pi./(2*a).*coth(a*pi))
clc
idx = 2*(1:N);
sum(1./(idx.^2+a^2))-(-1./(2*a.^2)+pi./(4*a).*coth(a*pi/2))
clc
idx = 2*(0:N)+1;
sum(1./(idx.^2+a^2))-(pi./(2*a).*coth(a*pi)-pi./(4*a).*coth(a*pi/2))


%%
idx = 0:N;
sum(1./(idx.^2+a^2))-(1+a*pi*coth(a*pi))./(2*a^2)
%%
xi = logspace(log10(xi_min),log10(xi_max),Nxi);
[mu1,r1n,sn] = ComputeDiscreteDiffusiveRep_Optim_Std(xi,sn,omegan,@(om)(om./om),h1_an,0);
%% Check sum of poles
r = -10;
n = -4e3:4e3;
poles = log(sqrt(abs(r)))+1i*pi*(n+0.5);
S = 1/(1-r) + 0.5*sum(1./poles(:))
%% MCRF contradiction
x = linspace(0,1,1e2);
y = linspace(-10,10,1e3);
gamma = 1;
[X,Y] = meshgrid(x,y);
S = X+1i*Y;
tau = 10;
ztau = 1250;
zinf = 1.01*ztau*sqrt(tau)*(6.67742356498299e-01);
Z = real(zinf+ztau*exp(-tau*S).*1./sqrt(S));
Z = Z -1e5*(Z<=0);
Z = real(1i*sin(S./1i));
clf
surf(X,Y,Z,'edgecolor','none');
xlabel('Re(s)');
ylabel('Im(s)');
%axis([0,1,0,1,0,5])
%caxis([-1,0]);
colorbar
view([0,90])
%% MCRF weight
xi = linspace(0,5,1e3);
tau = 0.1;
x0 = fsolve(@(x)x.*tan(x+pi/4)+0.5,2);
phi = @(w)-cos(w*tau+pi/4)./sqrt(w);
clf
hold all
%plot(xi,phi(xi));
plot(xi,tan(xi+pi/4))
plot(xi,-1./(2*xi))
ylim([-10,10])
%%
z = 1+1*1i;
a = 1; % ||p||
b = 1; % ||p||_boundary
c = 1; % ||nabla p||
roots([a*z,b,c*z])

%%
xi = linspace(0,1,1e2);
clf
hold all
plot(xi,xi-1);
plot(xi,1-xi);

%% PLot interval for beta
x = linspace(-1,1,1e3);
z = 1;
alpha = 0.1;
f = @(x)(1+x).^(1/alpha-2)./( z*(1-x).^(1/alpha) +(1+x).^(1/alpha));
clf
hold all
plot(x,f(x));
%plot(x,x.^(1/alpha))
xlabel('x');

%%
clear x f
syms x

a = 0.48; % alpha in (0.33,0.5)
b = 0.09; % Holder exponent
f(x) = (1+x)^(1/a-2)/((1-x)^(1/a)+(1+x)^(1/a));
%f = sqrt(x);
pretty(f)
g(x) = (abs(x+1)^(1-b)) * diff(f);
%pretty(g)
%ezplot(g,[-1,0])
xi = linspace(-0.999999999999,1,1e2);
plot(xi,g(xi))
clc
1/a-2
%subs(g(x),x,-1)
%% 
beta = 6*alpha;
alpha = 0.8;
f = @(x)exp(-((2+x)./(x)).^(1/beta)).*(x.^(-1+(alpha-1)/beta));
clf
x= linspace(0,1e-3,1e4);
plot(x,f(x))

%%
abs(T(2).^(1-alpha)/((1-alpha)*gamma(1-alpha)) - transpose((mu(:)./xi(:)))*(ones(length(xi),length(T(2)))-exp(-kron(xi(:),T(2)))))
%./(T.^(1-alpha)/((1-alpha)*gamma(1-alpha)));

%% Duct cut-off frequency
M = [0,0.1,0.2,0.3];
2720.*sqrt((1-M.^2))

%% Computation of complex residue for poles on cut
rho = 1;
b = 1;
e = 1;
f = @(s)1./(s.*(1-rho*exp(-2*(e*sqrt(s)+b*s))));
f = @(s)1./((1-rho*exp(-2*(e*sqrt(s)+b*s))));
f = @(s)exp(-e*sqrt(s))./s;
z0 = 0;
%residue = computeComplexResidue(f,z0,'tol',1e-10)
r = 1e-10;
res = integral(@(t)r*f(z0+r*exp(1i*t)).*exp(1i*t),0,2*pi)/(2*pi)
%% Residue at branch point
t =0;
b = 1;
e = 0.1;
f = @(s)exp(s*t)./(s.*(1-exp(-2*b*s-2*e*sqrt(s))));
z0=0;
r = 1e-1;
clc
res = integral(@(t)r*f(z0+r*exp(1i*t)).*exp(1i*t),-pi,pi)/(2*pi)
0.5+0.5*t/b
%%
e = 0.1;
f=@(x)sin(e*sqrt(x))./x;
f=@(x)sin(x)./x;
integral(f,-inf,inf)

%%
e = 0.1;
a = 1;
rho = 0.5;
mu = @(x)sin(2*e*sqrt(x))./(1-2*rho*exp(2*a*x).*cos(2*e*sqrt(x))+rho^2*exp(4*a*x));
pole = log(1/rho)/(2*a);
f=@(x)sin(x)./(1-exp(x).*cos(x));

x = linspace(0,1,5e3);
clf
hold all
plot(x,mu(x));
%ylim([-1,1])
%plot(pole*[1,1],ylim,'--');
%plot(xlim,[1,1],'--');

%%
rho = 0.98;
f = @(s)1./(1-rho*exp(-2*s))-0.5;
w = linspace(-10,10,1e2);
plot(w,f(1i*w));
ylim([0,2])
%%
clc
f = @(s)exp(-2*e*sqrt(s))./(1-rho*exp(-2*a*s-2*e*sqrt(s)));
f = @(s)coth(sqrt(s))./s;
f = @(s)tanh(sqrt(s));
identifyDiffusiveRep(f,0,0);
%% Variations of diffusive weight
a = [1,1,10];
mu = @(x)a(2)*sqrt(x)./(a(1)^2+(a(2)^2-2*a(1)*a(3))*x+a(3)^2*x.^2);
e = 10;
b = 1;
rho = 0.1;
mu = @(x)sin(2*e*sqrt(x))./(1-2*rho*exp(2*b*x).*cos(2*e*sqrt(x))+rho^2.*exp(4*b*x));
x = linspace(0,5,1e3);
clf
plot(x,mu(x))
%% Cavity with losses, diffusive part
e = 0;
b = 1;
rho = 0.1;
alpha=0.3;
F_an = @(s)exp(-2*e*s.^alpha)./(1-rho*exp(-2*(b*s+e*s.^alpha)));
F_an = @(s)F_an(s)./s;
clc
identifyDiffusiveRep(F_an,branchpt,0);
%%
f = @(x)1./(1i*x);
f = @(x)coth(1i*x);
f = @(x)sin(x);
clf
x=linspace(-10,10,1e2);
plot(x,angle(f(x)));
%% Kernel singularity
x = linspace(0,1e-4,1e6);
alpha = 0.7;
f = @(x)(x.^(alpha)).*log(x);
clf
plot(x,f(x));
%% Test nonlinear optimization
alpha = 0.5;
N = 10;
xi_min = 1e-3; % if logarithmic pole repartition
xi_max = 1e4;
xi = logspace(log10(xi_min),log10(xi_max),N); % Distribution of poles
w_optim = logspace(log10(min(xi)),log10(max(xi)),1e4);
[xi,mu] = ComputeDiscreteDiffusiveRep_Optim_StdNonLinear(xi,w_optim,@(s)1./sqrt(s));
%% Test quadrature with refinments
alpha=0.5;
h_fun = @(alpha,s)s.^(-alpha);
mu_an=@(xi)sin((alpha)*pi)./(pi*xi.^(alpha));
N = 5;
[xi1,mu1] = ComputeDiscreteDiffusiveRep_QuadratureGL_Alt(mu_an,N,'Optim',struct('Method','NonLinOptimRefl','h_an',@(s)s.*h_fun(alpha,s)));
[xi2,mu2] = ComputeDiscreteDiffusiveRep_Quadrature(mu_an,N,'Optim',struct('Method','NonLinOptimRefl','h_an',@(s)s.*h_fun(alpha,s)));
max(xi1-xi2)
max(mu1-mu2)
%%
f = @(x)x;
isa(f, 'function_handle')
g = @ComputeDiscreteDiffusiveRep_Optim_Ext;
isa(g, 'function_handle')
%%
xi_optim=xi_quad; mu_optim=mu_quad;
%%
[max(xi_optim),max(mu_optim)]
sum(mu_optim)