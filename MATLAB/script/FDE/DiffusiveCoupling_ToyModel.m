%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Time-integration of a Toy Fractional Model
%%%
%                dq/dt = Aq + Bxd[c'q] + u(t)
% with initial state q0, where d is a Caputo derivative. 
% A (NqxNq)
% N (Nqx1)
% B (Nqx1)
% c,q (Nqx1)
% u(t) (1xp) -> (Nqxp)
% The discrete simulation of the Caputo derivative is achieved through
%integration of the extended system [q;phi] where
% phi (Nxix1 is the vector of diffusive variables
% mu (Nxix1) weights of the discrete diffusive representation
% xi (Nxix1) poles of the discrete diffusive representation
% Three integration methods are compared:
%   - LSERK4 on the extended system (coupled dynamic)
%   - LSERK4-quad1
%   - LSERK-split
% Rmk: When changing the parameters, make sure that the new exact solution
% has been updated before computing the error!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% User inputs - 1 doF model
    % Model constants
q0 = 1;
A = [-1];
c = [1];
B = [-1]; % coupling factor
u = @(t)(zeros(size(t)));
    % Time-solving
tf = 10; % Final time

%% User inputs - 2 doF model
    % Model constants
q0 = ones(2,1);
k1=1e-1;
k2=1e-1;
f12=-1e-12; %% Beware: can trigger positive eignval!
f21=-1;
f0 = 1;
f1=-1;
A = [-k1,f12;f21,-k2];
c = [0;1];
B = [0;f1];
u1 = @(t)(0*f0*((1*(t<2)+(t>=2).*cos(2*pi*(t-2)))));
u2 = @(t)(zeros(size(t)));
u3 = @(t)(exp(-1*t)); % (1x1) -> (1x1)
u4 = @(t)(ones(size(t)));
u = @(t)(zeros(Nq,length(t)));
    % Time-solving
tf = 500; % Final time
%% User inputs - Fractional-order term : discretisation 
    % 2 poles in [1,1e1] rad/s
mu = [1.027641e+00;3.248398e+00];
xi = [1;10];
    % 2 poles in [1,1e2] rad/s
% mu = [1.53777;1.538645e+01];
% xi = [1;100];
    % 2 poles in [1,1e3] rad/s
% mu = [1.828828e+00;5.790053e+01];
% xi = [1;1e3];
    % 2 poles in [1,1e4] rad/s
% mu = [1.941762e+00;1.945011e+02];
% xi = [1;1e4];
%% Matrix building
phi0=(c'*q0)*(1./xi);
Nxi = length(xi);
Nq = length(q0);
Mxi = diag(xi);
A_coupled = zeros(Nq+Nxi,Nq+Nxi);
Block1 = A + sum(mu)*B*transpose(c);
Block2 = -B*transpose(mu)*Mxi;
Block3 = ones(Nxi,1)*c';
Block4 = -Mxi;
A_coupled(1:Nq,1:Nq) = Block1;
A_coupled(1:Nq, Nq + (1:Nxi))=Block2;
A_coupled(Nq + (1:Nxi),1:Nq)=Block3;
A_coupled(Nq + (1:Nxi),Nq + (1:Nxi))=Block4;

%% Comparison of the dynamic (linear stability)
eignval_c = sort(eig(A_coupled));
eignval_s = sort(eig(Block1));
eignval_o = sort(eig(A));
C_rk = 4.4; % LSERK4: 
dt_max_c = C_rk/max(abs(eignval_c)); % Coupled dynamic
dt_max_o = C_rk/max(abs(eignval_o)); % % Lin. stability limit of the original dynamic
dt_max_s = C_rk/max(abs(eignval_s)); % Split dynamic
fprintf('Dynamic: Original dt_max_o=%1.2e Split dt_max_s=%1.2e=(%1.2e)*dt_max_o, Coupled dt_max_c=%1.2e=(%1.2e)*dt_max_o\n',dt_max_o,dt_max_s,dt_max_s/dt_max_o,dt_max_c,dt_max_c/dt_max_o);

%% 1 - Accurate & Costly integration
% LSERK4 on the extended system: the coupled dynamic is fully solved.
% -> x:=[q;phi]
dt_full = 5e-1*dt_max_c;
[x_full,t_full,Nop_full] = LSERK4_op(@(x,t)(A_coupled*x+[u(t);zeros(Nxi,length(t))]),[q0;phi0],tf,dt_full);

%% 2 - LSERK4-quad1
%s_light = 2.5*1e-3*(dt_max0/dt_max); 
dt_light = 5e-1*(dt_max_s);
[q_light,phi_light,t_light,Nop_light] = LSERK4_diffusive_op(@(q,t)(Block1*q+u(t)),@(phi)(Block2*phi),c,q0,phi0,xi,tf,dt_light);
x_light = [q_light;phi_light];
%% 3 - LSERK4 with Splitting technique
% Applicable only for Nq>1
if Nq~=2 || sum(B==[0;f1])~=2 || sum(c==[0;1])~=2 
    error('This section does not make sense for this case.');
end
dt_split = 1e-1*min(C_rk/max(abs(eig(A_coupled(1,1)))),dt_max_o);
dt2_split = 1*C_rk/max(abs(eig(A_coupled(2:end,2:end))));
fprintf('Splitting: dt1=%1.2e=(%1.2e)*dt_max0, dt2=%1.2e=(%1.2e)*dt_max0\n',dt_split,dt_split/dt_max_o,dt2_split,dt2_split/dt_max_o);
[q1,q2,t_split,Nop_split] = LSERK4_splitting(@(q,t)(A_coupled(1,1)*q+u1(t)),@(q,t)(A_coupled(2:end,2:end)*q+u2(t)),@(q)(A_coupled(1,2:end)*q),@(q)(A_coupled(2:end,1)*q),q0(1),[q0(2);phi0],tf,dt_split,dt2_split);
x_split = [q1;q2];

%% Comparison Plot
figure
for i=1:Nq
    subplot(ceil((Nq+Nxi)/2),2,i)
    hold all
    leg=cell(0);
    plot(t_full,x_full(i,:));
    leg(end+1)={sprintf('LSERK4 - (%1.2e oper,dt=%1.1e*dt or)',Nop_full,dt_full/dt_max_o)};
    plot(t_light,x_light(i,:),'x');
    leg(end+1)={sprintf('LSERK4-quad1 - (%1.2e oper,dt=%1.1e*dt or)',Nop_light,dt_light/dt_max_o)};
    plot(t_split,x_split(i,:),'o');
    leg(end+1)={sprintf('LSERK4-split - (%1.2e oper,dt=%1.1e*dt or)',Nop_split,dt_split/dt_max_o)};
    xlabel('t');
    title(sprintf('x_%d',i));
    legend(leg);
end
for i=1:Nxi
    subplot(ceil((Nq+Nxi)/2),2,i+Nq)
    hold all
    leg=cell(0);
    plot(t_full,x_full(i+Nq,:))
    leg(end+1)={sprintf('LSERK4 - (%1.2e oper,dt=%1.1e*dt or)',Nop_full,dt_full/dt_max_o)};
    plot(t_light,x_light(i+Nq,:),'x');
    leg(end+1)={sprintf('LSERK4-quad1 - (%1.2e oper,dt=%1.1e*dt or)',Nop_light,dt_light/dt_max_o)};
    plot(t_split,x_split(i+Nq,:),'o');
    leg(end+1)={sprintf('LSERK4-split - (%1.2e oper,dt=%1.1e*dt or)',Nop_split,dt_split/dt_max_o)};
    title(sprintf('x_%d=%2.2g',i,xi(i)));
    xlabel('t');
    legend(leg);
end
%% (Not useful) Test & plot interpolation
% Section to test the interpolation of the full solution on the light mesh

x_full_interp = interp1(t_full,x_full',t_light)';
figure
for i=1:Nq
    subplot(ceil((Nq+Nxi)/2),2,i)
    plot(t_full,x_full(i,:),t_light,x_full_interp(i,:));
    xlabel('t');
    title(sprintf('x_%d - f1=%1.e',i,f1));
    legend(sprintf('Full - (%d it)',length(t_full)),...
           sprintf('Interp on Light'));
end
for i=1:Nxi
    subplot(ceil((Nq+Nxi)/2),2,i+Nq)
    plot(t_full,x_full(i+Nq,:),t_light,x_full_interp(i+Nq,:));
    xlabel('t');
    title(sprintf('phi_%d (xi=%1.2e) - f1=%1.e',i,xi(i),f1));
    legend(sprintf('Full - (%d it)',length(t_full)),...
           sprintf('Interp on Light'));
end
%% Case Nq=1 without input - Compute Exact theoretical solutionS
% 1 - Exact solution for a perfectly computed fractional derivative
% We compute the C1 solution of P(d1/2)q=0 for t in [0,tf].

if Nq~=1 || max(max(abs(u(linspace(1,tf,10)))))~=0
    error('This section does not make sense for this case.');
end

lambda = roots([1,-B,-A]); % roots of P(s)
l1=lambda(1);
l2=lambda(2);
clear lambda

q_ex_th = @(t)((q0/(l1-l2))*(l1*MittagLeffler(l2*sqrt(t))-l2*MittagLeffler(l1*sqrt(t))));


% 2 - Exact solution with a discrete approximation of the fractional
% derivative
% Nq=1

if B~=0 % coupled problem
    lambda=eig(A_coupled);
    P = zeros(Nxi+Nq,Nxi+Nq); % Eigenvector matrix
    P = zeros(Nq+Nxi,Nq+Nxi);
    P(1,:)=ones(1,Nxi+Nq);
    for i=1:Nxi
        for j=1:(Nxi+Nq)
            P(1+i,j)=(lambda(j)+xi(i))^(-1);
        end
    end
    G = P\[q0;phi0];
    x_ex_fun = @(t)(P*(exp(lambda*t).*kron(G,ones(1,length(t)))));
    clear lambda
elseif B==0 % uncoupled problem
    x_ex_fun = @(t)([q0*exp(A*t);phi0*exp(-kron(xi,t))+q0*kron((A+xi).^(-1),exp(A*t))+    -q0*kron((A+xi).^(-1),exp(-kron(xi,t)))]);
end
% 3 - Plot comparison
x_ex = x_ex_fun(t_full);
figure
for i=1:Nq
    subplot(ceil((Nq+Nxi)/2),2,i)
    hold all
    plot(t_full,x_full(i,:),t_light,x_light(i,:));
    plot(t_full,q_ex_th(t_full));
    plot(t_full,x_ex(i,:));
    xlabel('t');
    title(sprintf('x_%d',i));
    legend(sprintf('Full - (%d it,s=%1.1e)',length(t_full),s_full),...
           sprintf('Light - (%d it,s=%1.1e)',length(t_light),s_light),...
           sprintf('Exact theoretical solution'),...
           sprintf('Exact solution'));
end
for i=1:Nxi
    subplot(ceil((Nq+Nxi)/2),2,i+Nq)
    hold all
    plot(t_full,x_full(i+Nq,:),t_light,x_light(i+Nq,:));
    plot(t_full,x_ex(i+Nq,:));
    xlabel('t');
    title(sprintf('phi_%d (xi=%1.2e)',i,xi(i)));
    legend(sprintf('Full - (%d it,s=%1.1e)',length(t_full),s_full),...
           sprintf('Light - (%d it,s=%1.1e)',length(t_light),s_light),...
           sprintf('Exact solution'));
end

%% Case Nq=2 without input - Compute theoretical solution

if Nq~=2 || max(max(abs(u(linspace(1,tf,10)))))~=0 || sum(B==[0;f1])~=2 || sum(c==[0;1])~=2 
    error('This section does not make sense for this case.');
end

% 1 - Exact solution with a discrete approximation of the fractional
% derivative
% Nq=1

if B(1)==0 && B(2)~=0 && c(1)==0 && c(2)==1 % coupled problem
    lambda=eig(A_coupled);
    P = zeros(Nxi+Nq,Nxi+Nq); % Eigenvector matrix
    P(2,:)=ones(1,Nxi+Nq);
    P(1,:) = f12*(transpose(lambda)+k1).^(-1);
    for i=1:Nxi
        for j=1:(Nxi+Nq)
            P(2+i,j)=(lambda(j)+xi(i))^(-1);
        end
    end
    G = P\[q0;phi0];
    x_ex_fun = @(t)(P*(exp(lambda*t).*kron(G,ones(1,length(t)))));
    clear lambda
end
% 2 - Plot comparison
x_ex = x_ex_fun(t_full);
figure
for i=1:Nq
    subplot(ceil((Nq+Nxi)/2),2,i)
    hold all
    leg=cell(0);
    plot(t_full,x_full(i,:));
    leg(end+1)={sprintf('LSERK4 - (%1.2e oper,dt=%1.1e*dt or)',Nop_full,dt_full/dt_max_o)};
    plot(t_light,x_light(i,:),'x');
    leg(end+1)={sprintf('LSERK4-quad1 - (%1.2e oper,dt=%1.1e*dt or)',Nop_light,dt_light/dt_max_o)};
    plot(t_split,x_split(i,:),'o');
    leg(end+1)={sprintf('LSERK4-split - (%1.2e oper,dt=%1.1e*dt or)',Nop_split,dt_split/dt_max_o)};
    plot(t_full,x_ex(i,:));
    leg(end+1)={sprintf('Exact solution')};
    xlabel('t');
    title(sprintf('x_%d',i));
    legend(leg);    
end
for i=1:Nxi
    subplot(ceil((Nq+Nxi)/2),2,i+Nq)
    hold all
    leg=cell(0);
    plot(t_full,x_full(i+Nq,:))
    leg(end+1)={sprintf('LSERK4 - (%1.2e oper,dt=%1.1e*dt or)',Nop_full,dt_full/dt_max_o)};
    plot(t_light,x_light(i+Nq,:),'x');
    leg(end+1)={sprintf('LSERK4-quad1 - (%1.2e oper,dt=%1.1e*dt or)',Nop_light,dt_light/dt_max_o)};
    plot(t_split,x_split(i+Nq,:),'o');
    leg(end+1)={sprintf('LSERK4-split - (%1.2e oper,dt=%1.1e*dt or)',Nop_split,dt_split/dt_max_o)};
    plot(t_full,x_ex(i+Nq,:));
    leg(end+1)={sprintf('Exact solution')};
    title(sprintf('xi_%d=%2.2g',i,xi(i)));
    xlabel('t');
    legend(leg);
end

%% Compute errors
% Order validation w/o exact solution, through interpolation
% L2_LSERK4_split: L2 error of LSERK4-split
% L2_LSERK4_diffusive: L2 error of LSERK4-diffusive
% L2_LSERK4 = L2 error of the standard LSERK4
% Format:
%   L(:,j) = L2 error on each variables, at time-step n°j
dt_ref = dt_max_c;
dt = min([1e-1,2e-1,5e-1,1e0,3,5e0,1e1,2e1,5e1,130,2e2,5e2,1e3,2e3,5e3]*dt_ref,tf);
L2_LSERK4_split = zeros(Nxi+Nq,length(dt));
L2_LSERK4_diffusive = zeros(Nxi+Nq,length(dt));
L2_LSERK4 = zeros(Nxi+Nq,length(dt));
for i=1:length(dt)
        % LSERK4
    [x_lserk4,t_lserk4] = LSERK4_op(@(x,t)(A_coupled*x+[u(t);zeros(Nxi,1)]),[q0;phi0],tf, dt(i));
        % LSERK4-diffusive
    [q_light,phi_light,t_light] = LSERK4_diffusive_op(@(q,t)(Block1*q+u(t)),@(phi)(Block2*phi),c,q0,phi0,xi,tf,dt(i));
    x_light=[q_light;phi_light];
            % LSERK4-split
    [q1,q2,t_split,Nop_split] = LSERK4_splitting(@(q,t)(A_coupled(1,1)*q+u1(t)),@(q,t)(A_coupled(2:end,2:end)*q+u2(t)),@(q)(A_coupled(1,2:end)*q),@(q)(A_coupled(2:end,1)*q),q0(1),[q0(2);phi0],tf,dt(i),dt2_split);    
    x_split = [q1;q2];
        % Computation of L2 error
        L2_LSERK4_split(:,i) = sqrt(mean((x_split-x_ex_fun(t_split)).^2,2));
        L2_LSERK4_diffusive(:,i) = sqrt(mean((x_light-x_ex_fun(t_light)).^2,2));
        L2_LSERK4(:,i) = sqrt(mean((x_lserk4-x_ex_fun(t_lserk4)).^2,2));
end
%% Plot full L2 error
figure
leg = cell(0);
Order_int = zeros(0);
loglog(dt,mean(L2_LSERK4_diffusive,1),'-bo');
hold on
loglog(dt,mean(L2_LSERK4,1),'-ro');
loglog(dt,mean(L2_LSERK4_split,1),'-go');
xlabel(sprintf('dt (dt_{max}=%1.1e, dt^1_{max}=%1.1e, dt^0_{max}=%1.1e',dt_max_c,dt_max_s,dt_max_o));
ylabel('L2 error');
title(sprintf('2DoF - Comparison - tf=%1.1e',tf));
legend('LSERK4-diffusive','LSERK4','LSERK4-split');
%% Plot L2 errors
j_r = 2:3; % Column on which the order is assessed
clf
for i=1:Nq
    subplot(ceil((Nq+Nxi)/2),2,i)
    leg=cell(0);
    loglog(dt/dt_max_c,L2_LSERK4(i,:),'-x');
    a = polyfit(log(dt(j_r)),log(L2_LSERK4(i,j_r)),1); Order_LSERK4(i) = a(1);
    leg(end+1)={sprintf('LSERK4 (%1.1e)',Order_LSERK4(i))};
    hold all
    loglog(dt/dt_max_c,L2_LSERK4_diffusive(i,:),'-o');
    a = polyfit(log(dt(j_r)),log(L2_LSERK4_diffusive(i,j_r)),1); Order_LSERK4_diff(i) = a(1);
    leg(end+1)={sprintf('LSERK4-quad1 (%1.1e)',Order_LSERK4_diff(i))};
    loglog(dt/dt_max_c,L2_LSERK4_split(i,:),'-d'); 
    a = polyfit(log(dt(j_r)),log(L2_LSERK4_split(i,j_r)),1); Order_LSERK4_split(i) = a(1);
    leg(end+1)={sprintf('LSERK4-split (%1.1e)',Order_LSERK4_split(i))};
    loglog(dt_max_c/dt_max_c*[1,1],[1e-17,1e2],'k--');
    leg(end+1)={sprintf('dt max coupled')};
    loglog(dt_max_s/dt_max_c*[1,1],[1e-17,1e2],'k--');
    leg(end+1)={sprintf('dt max split')};
    loglog(dt_max_o/dt_max_c*[1,1],[1e-17,1e2],'k--');
    leg(end+1)={sprintf('dt max original')};
    xlabel(sprintf('dt / dt max coupled'));
    ylabel('L2 error');
    axis([min(dt/dt_max_c), max(dt/dt_max_c),1e-16,10])
    legend(leg);
    title(sprintf('x_%d',i));
end
for i=1:Nxi
    subplot(ceil((Nq+Nxi)/2),2,i+Nq)
    leg=cell(0);
    loglog(dt/dt_max_c,L2_LSERK4(i+Nq,:),'-x');
    a = polyfit(log(dt(j_r)),log(L2_LSERK4(i+Nq,j_r)),1); Order_LSERK4(i+Nq) = a(1);
    leg(end+1)={sprintf('LSERK4 (%1.1e)',Order_LSERK4(i))};
    hold all
    loglog(dt/dt_max_c,L2_LSERK4_diffusive(i+Nq,:),'-o');
    a = polyfit(log(dt(j_r)),log(L2_LSERK4_diffusive(i+Nq,j_r)),1); Order_LSERK4_diff(i+Nq) = a(1);
    leg(end+1)={sprintf('LSERK4-diffusive (%1.1e)',Order_LSERK4_diff(i))};
    loglog(dt/dt_max_c,L2_LSERK4_split(i+Nq,:),'-d');
    a = polyfit(log(dt(j_r)),log(L2_LSERK4_split(i+Nq,j_r)),1); Order_LSERK4_split(i+Nq) = a(1);
    leg(end+1)={sprintf('LSERK4-split (%1.1e)',Order_LSERK4_split(i))};
    loglog(dt_max_c/dt_max_c*[1,1],[1e-17,1e2],'k--');
    leg(end+1)={sprintf('dt max coupled')};
    loglog(dt_max_s/dt_max_c*[1,1],[1e-17,1e2],'k--');
    leg(end+1)={sprintf('dt max split')};
    loglog(dt_max_o/dt_max_c*[1,1],[1e-17,1e2],'k--');
    leg(end+1)={sprintf('dt max original')};
    xlabel(sprintf('dt/dt_max_c'));
    ylabel('L2 error');
    axis([min(dt/dt_max_c), max(dt/dt_max_c),1e-16,10])
    title(sprintf('phi_%d (xi=%1.2e)',i,xi(i)));
    legend(leg);
end