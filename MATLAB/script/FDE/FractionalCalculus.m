%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Fractional Calculus
%--------------------------------------------------------------------------
% Plot of analytical solutions, to show some features of fractional
%calculus
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Fractional primitive

FracInt = @(t,alpha)(1/(alpha*gamma(alpha))*t.^alpha);

t = linspace(1e-12,1,10^2);

alpha = [0.25,0.5,0.8,1,1.2];

clf
hold all
leg=cell(0);
for al=alpha
    plot(t,FracInt(t,al));
    leg(end+1)={sprintf('I %1.1g',al)};
end
xlabel('t');
ylabel('y');
title('Various fractional primitive of f=1 (t>0)');
legend(leg);

%% Fractional kernels

FracKer = @(t,alpha)(1/(gamma(alpha))*t.^(alpha-1));

t = [linspace(0,1e-1,30),linspace(1e-1,2,20)];

%alpha = [1/20,1/10,1/2,1];
alpha = [1,1+1/2,1+1,1+3/2];
clf
hold all
leg=cell(0);
for al=alpha
    plot(t,FracKer(t,al));
    leg(end+1)={sprintf('Y %1.1g',al)};
end
xlabel('t');
ylabel('y');
title('Various fractional kernel for alpha above1');
legend(leg);
axis([0,max(t),0,2]);
set(gca,'DataAspectRatio',[1,1,1]);