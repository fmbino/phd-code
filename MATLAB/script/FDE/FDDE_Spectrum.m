%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spectrum of fractional delay differential equation
%%%
% Purpose: 1) Discretise the fractional delay differential equation
%  dx/dt(t) = A*x(t) + Btau*x(t-tau) + Bfrac*d^alpha*x(t-taufrac)
% as 
%                       dX/dt(t) = Ah*X(t)
% using the function 'FractionalDDE_CoupledSystem'.
% 2) Study the spectrum of Ah, to get insights into stability.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% I - Define equations and discretisation parameters
Nxi = 40; % No. of point to discretise fractional derivative
Np = 15; Nk=6; % No. of point to discretise delay
fprintf('Number of additional variable: Nxi=%d, N_tau=%d, N_tau*N_xi=%d\n',Nxi,Np*Nk,Nxi*Np*Nk);
    % Btau = 0
A_coupled_FDE = @(A,Bfrac,alpha,taufrac)(FractionalDDE_CoupledSystem(A,'FracOperator',struct('B',Bfrac,'alpha',alpha,'tau',taufrac),'DiscretisationParam',struct('Nxi',Nxi,'Np',Np,'Nk',Nk)));
    % Bfrac = 0
A_coupled_DDE = @(A,Btau,tau)(FractionalDDE_CoupledSystem(A,'DelayOperator',struct('B',Btau,'tau',tau),'DiscretisationParam',struct('Nxi',Nxi,'Np',Np,'Nk',Nk)));
    % Btau!=0 et Bfrac!=0
A_coupled_FDDE = @(A,Btau,tau,Bfrac,alpha,taufrac)(FractionalDDE_CoupledSystem(A,'DelayOperator',struct('B',Btau,'tau',tau),'FracOperator',struct('B',Bfrac,'alpha',alpha,'tau',taufrac),'DiscretisationParam',struct('Nxi',Nxi,'Np',Np,'Nk',Nk)));
%% Study DDE
% dx/dt(t) = A*x(t) + Btau*x(t-tau)
A = 1/2;
Btau = 1.9;
tau = 1;
% --
clf
hold all
Ah = A_coupled_DDE(A,Btau,tau);
Spec = eig(full(Ah));
[Rm,RmI] = max(real(Spec(:)));
plot(real(Spec),imag(Spec),'.');
hc=get(legend(gca),'String'); % get legend from current axes.
legend(hc,sprintf('(-A=%1.2g |B|=|%1.2g tau=%1.2g| N_{tau}=%d | UnsPole:(%1.2g,%1.2g)',-max(real(eig(A))),sqrt(max(abs(eig(Btau'*Btau)))),tau,Np*Nk,real(Spec(RmI)),imag(Spec(RmI))));
legend('Location','eastoutside');
title('Spectrum: dx/dt=A*x+B*x(t-tau)');
xlabel('Re(s)');
ylabel('Im(s)');
xlim([-10*tau,1]);
ylim([-20,20]);
%% Study FDE
% dx/dt(t) = A*x(t) + Bfrac*d^alpha*x(t-taufrac)
A = -1/2;
g = -1.01;
Bfrac = -g;
alpha = 0.5;
taufrac = 0;
% --
clf
hold all
Spec = eig(full(A_coupled_FDE(A,Bfrac,alpha,taufrac)));
[Rm,RmI] = max(real(Spec(:)));
plot(real(Spec),imag(Spec),'.');
hc=get(legend(gca),'String'); % get legend from current axes.
legend(hc,sprintf('(-A=%1.2g |B|=|%1.2g| tau=%1.2g | N_{xi}=%d | UnsPole:(%1.2g,%1.2g)',-max(real(eig(A))),sqrt(max(abs(eig(Bfrac'*Bfrac)))),taufrac,Nxi,real(Spec(RmI)),imag(Spec(RmI))));
legend('Location','eastoutside');
title('Spectrum: dx/dt(t) = A*x(t) + B*d^{alpha}*x(t-tau)');
xlabel('Re(s)');
ylabel('Im(s)');
xlim([-2,1]);
ylim([-1,1]);

%% Study FDE (Parametric)
N = 5e1;
a = 1;
g_angle = linspace(-pi,pi,N);
g_abs = 0.2*ones(1,N);
g = g_abs.*exp(1i*g_angle);
% -- 
A = a;
Bfrac = -g;
taufrac = 0;
alpha = 0.5;
figure
hold all
for i=1:length(g)
    Spec = eig(full(A_coupled_FDE(A,Bfrac(i),alpha,taufrac)));
    [Rm,RmI] = max(real(Spec(:)));
    plot(real(Spec),imag(Spec),'.');
    %hc=get(legend(gca),'String'); % get legend from current axes.
    %legend(hc,sprintf('(-A=%1.2g |B|=|%1.2g| tau=%1.2g | N_{xi}=%d | UnsPole:(%1.2g,%1.2g)',-max(real(eig(A))),sqrt(max(abs(eig(Bfrac(i)'*Bfrac(i))))),taufrac,Nxi,real(Spec(RmI)),imag(Spec(RmI))));
    %legend('Location','eastoutside');
    title('Spectrum: dx/dt(t) = A*x(t) + B*d^{alpha}*x(t-tau)');
    xlabel('Re(s)');
    ylabel('Im(s)');
    xlim([-2,1]);
    ylim([-1,1]);
end

%% Study FDE with delay
% dx/dt(t) = A*x(t) + Btau*x(t-tau) + Bfrac*d^alpha*x(t-taufrac)
A = -1;
Btau = abs(A)/2;
tau = 10;
g=1;
alpha = 0.5;
Bfrac = -g;
taufrac = tau;
% --
Ah=A_coupled_FDDE(A,Btau,tau,Bfrac,alpha,taufrac);
Spec = eig(full(Ah));
[Rm,RmI] = max(real(Spec(:)));
%%
figure
plot(real(Spec),imag(Spec),'r.','MarkerSize',15);
hc=get(legend(gca),'String'); % get legend from current axes.
legend(hc,sprintf('(-A=%1.2g |Btau|=|%1.2g| tau=%1.2g taufrac=%1.2g | \nN_{xi}=%d N_{tau}=%d | \nUnsPole:(%1.2g,%1.2g)',-max(real(eig(A))),sqrt(max(abs(eig(Btau'*Btau)))),tau,taufrac,Nxi,Np*Nk,real(Spec(RmI)),imag(Spec(RmI))));
legend('Location','eastoutside');
title('Spectrum: dx/dt(t) = A*x(t) + Btau*x(t-tau) + Bfrac*d^{alpha}*x(t-taufrac)');
xlabel('Re(s)');
ylabel('Im(s)');
xlim([-0.3,0.1]);
ylim([-5,5]);