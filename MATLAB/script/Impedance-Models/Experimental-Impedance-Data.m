%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Fit of experimental impedance data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load the models from 'Definition-Impedance-Models', if needed.

%% Misc. parameters
boolf = 1; % Plot in frequency (Hz) (=1) or |Stokes number| (rad) (=0)
if boolf==1
    fvar_title = sprintf('Frequency (Hz)');
else
    fvar_title = sprintf('|Stokes number| (rad)');
end
%% Parameters for the Extended Helmholtz Resonator (EHR)
% For use with the impedance model <z_ehr_tuned>
% Beware of the diameter used for uvisc_ehr!
%r_ehr = 16*nu*l/(c0*d^2); % Resistance (Crandall High Stokes)
%m_ehr = l/c0; % Mass (Crandall High Stokes)
%b_ehr = 1; % Prop. factor
uvisc_ehr = 8.73+0*(8.73+6)/2; % Tuning |Stokes number| (rad)
w_ehr = w_fun(uvisc_ehr*sqrt(-1i),dc); % Corresponding tuning pulsation (rad/s)
%w_ehr = w_fun(uvisc_ehr*sqrt(-1i),d); % Corresponding tuning pulsation (rad/s)
if boolf==1
    fvar_ehr = w_ehr/(2*pi);
else
    fvar_ehr = uvisc_ehr;
end
%% NASA GFIT Measurements - Microperforated liner
% This one suffers from 3D effect that has not been taken into account in
% the eduction process : the middle peak should not be fitted.
% NASA GFIT microperforated
%   --Facesheet
%   Porosity 5%
%   Thickness 0.8mm
%   Hole diameter 0.3mm
%   --Honeycomb
%   Cell diameter 9.5mm
%   Cell depth 38.1mm
%-- Perforated plate constants (if any)
sigma = 5/100; % Porosity (rad)
l = 0.8e-3; % Cylinder thickness (m)
d = 0.3e-3; % Cylinder diameter (m)
%-- Cavity constant
lc = 38.1e-3; % Cavity length (m)
dc = 9.5e-3; % Cavity diameter (m)
%% I - Load & display experimental data
clear Z_err
Z = csvread('NASA_GFIT_MicroPerf_120dB_NoFlow_FullWidth.csv',1,0);
Z_err = Z(:,4)+1i*Z(:,5); % (Estimated) error (rad)
Z = [Z(:,1), Z(:,2)+1i*Z(:,3)]; % [Freq (Hz), Normalized Impedance (rad)]
if boolf==0
    Z(:,1) = St_fun(2*pi*Z(:,1),d); % [Stokes Number (rad)]
end

clf
subplot(1,2,1)
leg=cell(0);
hold all
errorbar(abs(Z(:,1)),real(Z(:,2)),real(Z_err/2),'-o');
leg(end+1)={'GFIT,M=0,120dB'};
legend(leg);
xlabel(fvar_title);
ylabel('Normalized resistance (rad)');
ylim([-1,5])
subplot(1,2,2)
leg=cell(0);
hold all
errorbar(abs(Z(:,1)),imag(Z(:,2)),imag(Z_err/2),'-o');
leg(end+1)={'GFIT,M=0,120dB'};
legend(leg);
xlabel(fvar_title);
ylabel('Normalized reactance (rad)');
ylim([-5,1.20])

%% II - Compare to model (impedance)
u_visc = linspace(1,10,8e1);
%u_visc = linspace(6,7,1e5);
%u_visc = [linspace(1,6.1,25),linspace(6.1,6.5,65),linspace(6.5,8,10)];
%u_visc = [linspace(0,1,20),linspace(1,6.1,20),linspace(6.1,6.5,60),linspace(6.5,8.6,15),linspace(8.6,10,50)];
St = sqrt(-1i)*u_visc;
w = w_fun(St,d);
w_ref = w_fun(Z(:,1),d);

    % L2 errors of the un-corrected models
%err_high = 3.790333e-1+1i*1.141972;
%err_low = 2.756315e-1+1i*1.076663;
%err_mid = 3.491181e-1+1i*9.530967e-1;
%Z(:,2) = Z(:,2)-z_cav(k0(w_ref),0.95*lc); % cavity subtraction from exp.

if boolf==0
   fvar = u_visc;
else
    fvar = w/(2*pi);
end 
f=clf;
hold all
leg=cell(0);
%--
subplot(1,2,1)
errorbar(abs(Z(:,1)),real(Z(:,2)),real(Z_err/2),'-o');
leg(end+1)={'GFIT,M=0,120dB'};
legend(leg);
subplot(1,2,2)
errorbar(abs(Z(:,1)),imag(Z(:,2)),imag(Z_err/2),'-o');
%--
z=@(w)((z_MP(k_br(d,w,Pr),l,k_br(dc,w,Pr),lc,sigma))./sigma);
lg = sprintf('k Br. kc Br. (generic) (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - Z(:,2)),2),max(real(z(w))));
plotComplex(f,fvar,z(w),'k',lg);
%-
% z=@(w)((z_MP(k_br(d,w,Pr),l,k0(w),lc,sigma))./sigma);
% lg = sprintf('k Br. kc LEE (generic) (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - Z(:,2)),2),max(real(z(w))));
% plotComplex(f,fvar,z(w),'r',lg);
%--
% z=@(w)((z_cr(l,d,w)./sigma + z_cav(k_br(dc,w,Pr),lc)));
% lg=sprintf('Crandall + kc Br. (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - Z(:,2)),2),max(real(z(w))));
% plotComplex(f,fvar,z(w),'k--',lg);
%--
z=@(w)((z_cr(l,d,w)./sigma + z_cav(k_high(dc,w,Pr),lc)));
lg=sprintf('Crandall + kc Br. high (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - Z(:,2)),2),max(real(z(w))));
plotComplex(f,fvar,z(w),'b--',lg);
%--
% z=@(w)((z_cr(l,d,w)./sigma + z_cav(k0(w),lc)));
% lg=sprintf('Crandall + kc LEE (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - Z(:,2)),2),max(real(z(w))));
% plotComplex(f,fvar,z(w),'r--',lg);
%--
% z=@(w)((z_cr_high(l,d,w)+0*Cor_visc_high(d,w)+0*Cor_rad(d,w)+0*Cor_rad_int(d,sigma,w))./sigma + 1*z_cav(k0(w),lc));
% lg=sprintf('Crandall High. + kc LEE (l2=%1.1e)',norm(real(z(w_ref) - Z(:,2)),2));
% lg=sprintf('Crandall High. + kc LEE (l2=%1.2g %%)',100*(norm(real(z(w_ref) - Z(:,2)),2)-real(err_high))/real(err_high));
% plotComplex(f,fvar,z(w),'-',lg);
%--
% z=@(w)((z_cr_low(l,d,w)+0*Cor_visc_low(d)+0*Cor_rad(d,w)+0*Cor_rad_int(d,sigma,w))./sigma + 1*z_cav(k0(w),lc));
% lg=sprintf('Crandall Low + kc LEE (l2=%1.1e)',norm(real(z(w_ref) - Z(:,2)),2));
% lg=sprintf('Crandall Low + kc LEE (l2=%1.2g %%)',100*(norm(real(z(w_ref) - Z(:,2)),2)-real(err_low))/real(err_low));
% plotComplex(f,fvar,z(w),'-',lg);
%--
% z=@(w)((z_maa(l,d,w)+0*Cor_visc_mid(w)+0*Cor_rad(d,w)+0*Cor_rad_int(d,sigma,w))./sigma + 1*z_cav(k0(w),lc));
% lg=sprintf('Maa + kc LEE (l2=%1.1e)',norm(real(z(w_ref) - Z(:,2)),2));
% lg=sprintf('Maa + kc LEE (l2=%1.2g %%)',100*(norm(real(z(w_ref) - Z(:,2)),2)-real(err_mid))/real(err_mid));
% plotComplex(f,fvar,z(w),'-',lg);
%--
% lg=sprintf('EHR (|St-eps|=%1.2g,|f-eps|=%1.2g Hz)',uvisc_ehr,w_ehr/(2*pi));
% plotComplex(f,fvar,real(z_ehr_tuned(r_ehr/sigma,m_ehr/sigma,b_ehr,lc,dc,w_ehr,Pr,w)),'-',lg);
% plot(fvar_ehr*[1,1],[0,7],'--');
%--
subplot(1,2,1)
xlabel(fvar_title);
ylabel('Normalized resistance (rad)');
%set(gca,'DataAspectRatio',[1,1,1])
ylim([0,5])
subplot(1,2,2)
xlabel(fvar_title);
ylabel('Normalized reactance (rad)');
%set(gca,'DataAspectRatio',[1,1,1])
ylim([-5,5])
%% II - Compare to model (|absorbtion coefficient|)
u_visc = linspace(1,10,2e2);
%u_visc = [linspace(1,3.5,10),linspace(3.5,4.1,10),linspace(4.1,6.6,25),linspace(6.6,7,20),linspace(7,8,10)];
St = sqrt(-1i)*u_visc;
w = w_fun(St,d);
w_ref = w_fun(Z(:,1),d);

if boolf==0
   fvar = u_visc;
else
    fvar = w/(2*pi);
end 
clf
leg=cell(0);
hold all
plot(abs(Z(:,1)),alpha_b(Z(:,2)),'-ro');
leg(end+1)={'GFIT,M=0,120dB'};
%--
% z=@(w)((z_MP(k_br(d,w,Pr),l,k_br(dc,w,Pr),lc,sigma))./sigma);
% z=@(w)(alpha_b(z(w)));
% plot(fvar,real(z(w)),'k');
% leg(end+1)={sprintf('k Br. kc Br. (generic) (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - alpha_b(Z(:,2))),2),max(real(z(w))))};
%--
% z=@(w)((z_MP(k_br(d,w,Pr),l,k0(w),lc,sigma))./sigma);
% z=@(w)(alpha_b(z(w)));
% plot(fvar,real(z(w)),'r');
% leg(end+1)={sprintf('k Br. kc LEE (generic) (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - alpha_b(Z(:,2))),2),max(real(z(w))))};
%--
z=@(w)((z_cr(l,d,w)./sigma + z_cav(k_br(dc,w,Pr),lc)));
z=@(w)(alpha_b(z(w)));
plot(fvar,real(z(w)),'k-');
leg(end+1)={sprintf('Crandall + kc Br. (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - alpha_b(Z(:,2))),2),max(real(z(w))))};
%--
% z=@(w)((z_cr(l,d,w)./sigma + z_cav(k0(w),lc)));
% z=@(w)(alpha_b(z(w)));
% plot(fvar,real(z(w)),'r--');
% leg(end+1)={sprintf('Crandall + kc LEE (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - alpha_b(Z(:,2))),2),max(real(z(w))))};
%--
plot(fvar,alpha_b(z_ehr_tuned(r_ehr/sigma,m_ehr/sigma,b_ehr,lc,dc,w_ehr,Pr,w)),'-');
leg(end+1)={sprintf('EHR (|St-eps|=%1.2g,|f-eps|=%1.2g Hz)',uvisc_ehr,w_ehr/(2*pi))};
plot(fvar_ehr*[1,1],[-5,5],'b--');
%--
legend(leg);
xlabel(fvar_title);
ylabel('Absorption coeff. alpha_b (rad)');
%set(gca,'DataAspectRatio',[1,1,1])
ylim([0,1])
%% NASA GIT - Ceramic Tubular liner (CT57) [Jones2005]
% Normalized educed impedance on a CT57 liner from "Benchmark Data for 
%Evaluation of Aeroacoustic Propagation Codes with Grazing Flow".
% Itensity level: 130dB
% Eduction method: 2D Finite Element Method (2DFEM)
%                  Quasi-3D Finite Element Method (Q3DFEM)
% Assumptions: Plane wave (below cut-off frequency@3.3kHz)
% Liner description:
%   Surface porosity 57%
%   Cavity diameter 0.6mm
%   Cavity length 85.6mm
sigma = 57/100;
dc = 0.6e-3;
lc = 85.6e-3;
%% I - Load &display experimental data
Z_Q3D = importZdata('CT57_NoFlow_Q3DFEM.csv', 2, 7);
Z_Q3D = [Z_Q3D(:,1), Z_Q3D(:,2)+1i*Z_Q3D(:,3)];
Z_2D = importZdata('CT57_NoFlow_2DFEM.csv', 2, 7);
Z_2D = [Z_2D(:,1), Z_2D(:,2)+1i*Z_2D(:,3)];
    % Data from [Dragna2016] PDF (Jones2005 with more points)
R_Dragna = importZdata('CT57-130dB-PDF-Dragna-Resistance.csv', 2, 27);
R_Dragna = R_Dragna(:,1:2);
I_Dragna = importZdata('CT57-130dB-PDF-Dragna-Reactance.csv', 2, 27);
I_Dragna = I_Dragna(:,1:2);
clear Z_err
Z_mean = Z_2D;
if boolf==0
    Z_mean(:,1) = St_fun(2*pi*Z_mean(:,1),dc); % Stokes number (rad)
end
Z_mean(:,2) = mean([Z_Q3D(:,2),Z_2D(:,2)],2);
Z_err(:,1) = abs(real(Z_2D(:,2)-Z_mean(:,2))); % error on real part
Z_err(:,2) = abs(imag(Z_2D(:,2)-Z_mean(:,2))); % error on imag part
figure
subplot(1,2,1)
leg=cell(0);
hold all
plot(abs(Z_mean(:,1)),real(Z_Q3D(:,2)),'-o');
leg(end+1)={'Educed (Q3D-FEM)'};
plot(abs(Z_mean(:,1)),real(Z_2D(:,2)),'-o');
leg(end+1)={'Educed (2D-FEM)'};
errorbar(abs(Z_mean(:,1)),real(Z_mean(:,2)),Z_err(:,1),'-o');
leg(end+1)={'Mean'};
legend(leg);
xlabel(fvar_title);
ylabel('Normalized resistance (rad)');
ylim([0,5])
subplot(1,2,2)
leg=cell(0);
hold all
plot(abs(Z_mean(:,1)),imag(Z_Q3D(:,2)),'-o');
leg(end+1)={'Educed (Q3D-FEM)'};
plot(abs(Z_mean(:,1)),imag(Z_2D(:,2)),'-o');
leg(end+1)={'Educed (2D-FEM)'};
errorbar(abs(Z_mean(:,1)),imag(Z_mean(:,2)),Z_err(:,2),'-o');
leg(end+1)={'Mean'};
legend(leg);
xlabel(fvar_title);
ylabel('Normalized reactance (rad)');
ylim([-3,2])
%gh=figure;
%plotDiscreteBodeDiagram(gh,2*pi*Z(:,1),Z(:,2),'[Jones05] CT57 Liner No Flow');
%% II - Compare to model (impedance)
u_visc = linspace(0,20,1.1e3);
St = sqrt(-1i)*u_visc;
w = w_fun(St,dc);
w_ref = w_fun(Z_mean(:,1),dc);
if boolf==0
   fvar = u_visc;
else
    fvar = w/(2*pi);
end 
f=clf;
%--
% subplot(1,2,1)
% leg=cell(0);
% hold all
% errorbar(abs(Z_mean(:,1)),real(Z_mean(:,2)),Z_err(:,1),'-ro');
% leg(end+1)={'CT57, M=0, 130dB'};
% legend(leg);
% subplot(1,2,2)
% leg=cell(0);
% hold all
% errorbar(abs(Z_mean(:,1)),imag(Z_mean(:,2)),Z_err(:,2),'-ro');
%--
% lg=sprintf('kc Br.');
% plotComplex(f,fvar,z_cav(k_br(dc,w,Pr),lc)./sigma,'k--',lg);
%--
% lg=sprintf('kc Br. (lc reduced by 14%%)');
% plotComplex(f,fvar,z_cav(k_br(dc,w,Pr),0.86*lc)./sigma,'k-',lg);
%--
% lg=sprintf('kc Br. No thermal effects');
% k_br2 = @(d,w,Pr)(k0(w).*sqrt((1+(gamma-1)*Lambda(sqrt(Pr)*knu(w)*d/2))./(1-Lambda(sqrt(-1i*w/(nu))*d/2))));
% plotComplex(f,fvar,z_cav(k_br2(2*dc,w,Pr),lc)./sigma,'b--',lg);
%--
% lg=sprintf('kc Br. No thermal effects (lc reduced by 14%%)');
% k_br2 = @(d,w,Pr)(k0(w).*sqrt((1+0*(gamma-1)*Lambda(sqrt(Pr)*knu(w)*d/2))./(1-Lambda(sqrt(-1i*w/(nu))*d/2))));
% plotComplex(f,fvar,z_cav(k_br2(dc,w,Pr),0.89*lc)./sigma,'b-',lg);
%--
% lg=sprintf('LEE');
% RT = sqrt(nu)/(c0*(dc/2))*(1+(gamma-1)/sqrt(3*Pr));
% plotComplex(f,fvar,coth((1i*w/c0 + 0*RT*sqrt(1i*w))*(76e-3))./sigma,'g',lg);
%--
lg=sprintf('kc Br. High');
plotComplex(f,fvar,coth(jk_high(dc,1i*w,Pr)*0.87*lc)/sigma,'-',lg);
%plotComplex(f,fvar,z_cav(k_high(dc,w,Pr),0.87*lc)./sigma,'-',lg);
lg = sprintf('kc Br. Low');
%plotComplex(f,fvar,z_cav( k_low_ko(St_fun(w,dc)).*k0(w),lc)./sigma,'-',lg);
%--
%lg=sprintf('kc LEE');
%plotComplex(f,fvar,z_cav(k0(w),lc)./sigma,'-',lg);
%--
% lg=sprintf('EHR (|St-eps|=%1.2g,|f-eps|=%1.2g Hz)',uvisc_ehr,w_ehr/(2*pi));
%plotComplex(f,fvar,z_ehr_tuned(0,0,1/sigma,lc,dc,w_ehr,Pr,w),'-',lg);
% subplot(1,2,1)
% plot(fvar_ehr*[1,1],[0,5],'--');
% subplot(1,2,2)
% plot(fvar_ehr*[1,1],[0,5],'--');
%--
%%%%% JASA Models: Fractional and EHR
%--
% lg=sprintf('kc Br. High (corrected) JASA');
% a1 = 0.85/c0;
% a12 = sqrt(nu)*(1/(c0*(dc/2)))*(1+(gamma-1)/sqrt(Pr));
% plotComplex(f,fvar,coth((a1*1i*w+a12*sqrt(1i*w))*lc)./sigma,'-',lg);
%--
% lg=sprintf('EHR JASA');
% a1 = 1/c0;
% a0 = 4.66;
% plotComplex(f,fvar,coth((a1*1i*w+a0)*0.97*lc)./sigma,'-',lg);
%--
subplot(1,2,1)
hc=get(legend(gca),'String'); % get legend from current axes.
plot(R_Dragna(:,1),R_Dragna(:,2),'ro');
legend(hc,sprintf('%s','CT57, M=0, 130dB'));
xlabel(fvar_title);
ylabel('Normalized resistance (rad)');
ylim([0,6]);
xlim([0*500,3000]);
%set(gca,'DataAspectRatio',[1,1,1])
subplot(1,2,2)
plot(I_Dragna(:,1),I_Dragna(:,2),'ro');
xlabel(fvar_title);
ylabel('Normalized reactance (rad)');
ylim([-2.5,6])
xlim([0*500,3000]);
%set(gca,'DataAspectRatio',[1,1,1])

%% II - Compare to model (absorption coefficient)
u_visc = linspace(1,20,1.1e2);
St = sqrt(-1i)*u_visc;
w = w_fun(St,dc);
w_ref = w_fun(Z_mean(:,1),dc);
if boolf==0
   fvar = u_visc;
else
    fvar = w/(2*pi);
end 
clf
leg=cell(0);
hold all
errorbar(abs(Z_mean(:,1)),abs(alpha_b(Z_mean(:,2))),Z_err(:,1),'-ro');
leg(end+1)={'CT57, M=0, 130dB'};
%--
% plot(fvar,alpha_b(z_cav(k_br(dc,w,Pr),lc)./sigma),'k-');
% leg(end+1)={'kc Br.'};
%--
plot(fvar,alpha_b(z_cav(k_high(dc,w,Pr),lc)./sigma),'-');
leg(end+1)={'kc Br. High'};
%--
% plot(fvar,alpha_b(z_cav( k_low_ko(St_fun(w,dc)).*k0(w),lc)./sigma),'-');
% leg(end+1)={'kc Br. Low'};
%--
% plot(fvar,alpha_b(z_cav(k0(w),lc)./sigma),'-');
% leg(end+1)={'kc LEE'};
%--
%plot(fvar,alpha_b(z_ehr(r,m,b,e,lc,w)./sigma),'-');
%leg(end+1)={'EHR'};
plot(fvar,alpha_b(z_ehr_tuned(0,0,1/sigma,lc,dc,w_ehr,Pr,w)),'k-');
leg(end+1)={sprintf('EHR (|St-eps|=%1.2g,|f-eps|=%1.2g Hz)',uvisc_ehr,w_ehr/(2*pi))};
plot(fvar_ehr*[1,1],[0,1],'b--');
%--
legend(leg);
xlabel(fvar_title);
ylabel('Absorbtion coeff. alpha b (rad)');
ylim([0,1])
%set(gca,'DataAspectRatio',[1,1,1])

%% Unpublished ONERA Measurements - Microperforated liner (MA150307)
% Impedance tube measurements made at ONERA.
%   --Perforated plate (same as [Primus2013])
%   Porosity 5%
%   Thickness 0.8mm
%   Hole diameter 0.3mm
%   --Honeycomb
%   Cell diameter 9mm
%   Cell depth 20mm
%-- Perforated plate constants
sigma = 5/100; % Porosity (rad)
l = 0.8e-3; % Cylinder thickness (m)
d = 0.3e-3; % Cylinder diameter (m)
%-- Cavity constant
lc = 20e-3; % Cavity length (m)
dc = 9e-3; % Cavity diameter (m)
% Correction:
%l=0.95*l; % shorter perforation (to lower the viscous losses)
% lc=0.91*lc; % shorter cavity (to increase the resonant frequency)
% sigma = 8/100;
%% I - Load & display experimental data
% Output fields:
%   Z_exp (rad) real part + 1i* imag part (normalised impedance)
%   Alpha_exp (rad) absorption coefficient
ExpData110dB = xlsread('ATK_MA150307_110dB.xls');
ExpData120dB = xlsread('ATK_MA150307_120dB.xls');
ExpData130dB = xlsread('ATK_MA150307_130dB.xls');
    % Low-pass filter
[b,a]=butter(4,0.02,'low');
ExpData110dB(:,8)=filter(b,a,ExpData110dB(:,8));
ExpData120dB(:,8)=filter(b,a,ExpData120dB(:,8));
ExpData130dB(:,8)=filter(b,a,ExpData130dB(:,8));
ExpData110dB(:,9)=filter(b,a,ExpData110dB(:,9));
ExpData120dB(:,9)=filter(b,a,ExpData120dB(:,9));
ExpData130dB(:,9)=filter(b,a,ExpData130dB(:,9));
ExpData110dB(:,7)=filter(b,a,ExpData110dB(:,7));
ExpData120dB(:,7)=filter(b,a,ExpData120dB(:,7));
ExpData130dB(:,7)=filter(b,a,ExpData130dB(:,7));
    % Remove low & high frequencies
trim2=9e2;trim1=250;
ExpData110dB = ExpData110dB(trim1:40:end-trim2,:);
ExpData120dB = ExpData120dB(trim1:40:end-trim2,:);
ExpData130dB = ExpData130dB(trim1:40:end-trim2,:);

w_exp = 2*pi*ExpData110dB(:,1); % Experimental pulsation (rad/s)
St_exp = St_fun(w_exp,d); % Experimental Stokes number (rad)
if boolf==0
   fvarexp = St_exp;
else
   fvarexp = ExpData110dB(:,1);
end 
Z_exp = struct('dB110',ExpData110dB(:,8)+1i*ExpData110dB(:,9),...
           'dB120',ExpData120dB(:,8)+1i*ExpData120dB(:,9),...
           'dB130',ExpData130dB(:,8)+1i*ExpData130dB(:,9));
Alpha_exp = struct('dB110',ExpData110dB(:,7),'dB120',ExpData120dB(:,7),...
           'dB130',ExpData130dB(:,7));
clear ExpData110dB ExpData120dB ExpData130dB

clf
subplot(2,2,1)
leg=cell(0);
hold all
plot(abs(fvarexp),real(Z_exp.dB110),'-');
leg(end+1)={'Imp. Tube,M=0,110dB'};
plot(abs(fvarexp),real(Z_exp.dB120),'-');
leg(end+1)={'Imp. Tube,M=0,120dB'};
plot(abs(fvarexp),real(Z_exp.dB130),'-');
leg(end+1)={'Imp. Tube,M=0,130dB'};
legend(leg);
xlabel(fvar_title);
ylabel('Normalized resistance (rad)');
ylim([0,5]);
subplot(2,2,2)
leg=cell(0);
hold all
plot(abs(fvarexp),imag(Z_exp.dB110),'-');
leg(end+1)={'Imp. Tube,M=0,110dB'};
plot(abs(fvarexp),imag(Z_exp.dB120),'-');
leg(end+1)={'Imp. Tube,M=0,120dB'};
plot(abs(fvarexp),imag(Z_exp.dB130),'-');
leg(end+1)={'Imp. Tube,M=0,130dB'};
legend(leg);
xlabel(fvar_title);
ylabel('Normalized reactance (rad)');
ylim([-5,5]);
subplot(2,2,3)
leg=cell(0);
hold all
plot(abs(fvarexp),Alpha_exp.dB110,'-');
leg(end+1)={'Imp. Tube,M=0,110dB'};
plot(abs(fvarexp),Alpha_exp.dB120,'-');
leg(end+1)={'Imp. Tube,M=0,120dB'};
plot(abs(fvarexp),Alpha_exp.dB130,'-');
leg(end+1)={'Imp. Tube,M=0,130dB'};
legend(leg);
ylim([0,1]);
xlabel(fvar_title);
ylabel('Absorption coefficient (rad)');

%% II - Compare to model (impedance)
u_visc = linspace(1,10,1e2);
St = sqrt(-1i)*u_visc;
w = w_fun(St,d);
w_ref = w_exp;
Z_ref = Z_exp.dB110;
    % L2 errors of the un-corrected models
%err_high = 3.790333e-1+1i*1.141972;
%err_low = 2.756315e-1+1i*1.076663;
%err_mid = 3.491181e-1+1i*9.530967e-1;

%Z(:,2) = Z(:,2)-z_cav(k0(w_ref),0.95*lc); % cavity subtraction from exp.
if boolf==0
   fvar = u_visc;
else
    fvar = w/(2*pi);
end 
f=clf;
%--
lg=sprintf('Imp. Tube,M=0,110dB');
plotComplex(f,abs(fvarexp),Z_ref,'r-',lg);
%--
% z=@(w)((z_cr(l,d,w)./sigma + z_cav(k_high(dc,w,Pr),lc)));
% lg=sprintf('Crandall + kc Br. (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - Z_ref),2),max(real(z(w))));
% plotComplex(f,fvar,z(w),'k--',lg);
%--
a1 = 0.9/c0;
aa = 4*sqrt(nu)*(1/(c0*(dc/2)))*(1+(gamma-1)/sqrt(Pr));
z=@(w)(z_cr(0.78*l,d,w)./sigma + coth((a1*1i*w+aa*sqrt(1i*w))*lc));
Z = z(w);
lg=sprintf('Crandall + kc Br. (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - Z_ref),2),max(real(z(w))));
plotComplex(f,fvar,z(w),'k--',lg);
%--
% z=@(w)((z_cr(l,d,w)./sigma + z_cav(k0(w),lc)));
% lg=sprintf('Crandall + kc LEE (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - Z_ref),2),max(real(z(w))));
% plotComplex(f,fvar,z(w),'r--',lg);
%--
% lg=sprintf('Cr. High + EHR @(|St-eps|=%1.2g,|f-eps|=%1.2g Hz)',uvisc_ehr,w_ehr/(2*pi));
% plotComplex(f,fvar,z_cr_high(l,d,w)./sigma + z_ehr_tuned(0,0,b_ehr,lc,dc,w_ehr,Pr,w),'b-',lg);
% subplot(1,2,1)
%plot(fvar_ehr*[1,1],[0,7],'--');
%--
subplot(1,2,1)
xlabel(fvar_title);
ylabel('Normalized resistance (rad)');
%set(gca,'DataAspectRatio',[1,1,1])
ylim([0,5]);
subplot(1,2,2)
xlabel(fvar_title);
ylabel('Normalized reactance (rad)');
% %set(gca,'DataAspectRatio',[1,1,1])
ylim([-5,5]);
%% II - Compare to model (|absorbtion coefficient|)
u_visc = linspace(1,9.5,2e2);
St = sqrt(-1i)*u_visc;
w = w_fun(St,d);
w_ref = w_exp;
Z_ref = Z_exp.dB110;
if boolf==0
   fvar = u_visc;
else
    fvar = w/(2*pi);
end 
clf
leg=cell(0);
hold all
plot(abs(fvarexp),Alpha_exp.dB110,'r-');
leg(end+1)={'Imp. Tube,M=0,110dB'};
%--
% z=@(w)((z_MP(k_br(d,w,Pr),l,k_br(dc,w,Pr),lc,sigma))./sigma);
% z=@(w)(alpha_b(z(w)));
% plot(fvar,real(z(w)),'k');
% leg(end+1)={sprintf('k Br. kc Br. (generic) (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - alpha_b(Z_ref)),2),max(real(z(w))))};
%--
% z=@(w)((z_MP(k_br(d,w,Pr),l,k0(w),lc,sigma))./sigma);
% z=@(w)(alpha_b(z(w)));
% plot(fvar,real(z(w)),'r');
% leg(end+1)={sprintf('k Br. kc LEE (generic) (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - alpha_b(Z_ref)),2),max(real(z(w))))};
%--
% z=@(w)((z_cr(l,d,w)./sigma + z_cav(k_br(dc,w,Pr),lc)));
% z=@(w)(alpha_b(z(w)));
% plot(fvar,real(z(w)),'k--');
% leg(end+1)={sprintf('Crandall + kc Br. (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - alpha_b(Z_ref)),2),max(real(z(w))))};
%--
% z=@(w)((z_cr(l,d,w)./sigma + z_cav(k0(w),lc)));
% z=@(w)(alpha_b(z(w)));
% plot(fvar,real(z(w)),'r--');
% leg(end+1)={sprintf('Crandall + kc LEE (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - alpha_b(Z_ref)),2),max(real(z(w))))};
%--
plot(fvar,alpha_b(z_cr_high(l,d,w)./sigma + z_ehr_tuned(0,0,b_ehr,lc,dc,w_ehr,Pr,w)),'b-');
leg(end+1)={sprintf('Cr. High + EHR @(|St-eps|=%1.2g,|f-eps|=%1.2g Hz)',uvisc_ehr,w_ehr/(2*pi))};
plot(fvar_ehr*[1,1],[0,7],'--');
%--
legend(leg);
xlabel(fvar_title);
ylabel('Absorption coeff. alpha_b (rad)');
%set(gca,'DataAspectRatio',[1,1,1])
ylim([0,1]);
%% Unpublished ONERA Measurements - Wiremesh liner (MA150305)
% Impedance tube measurements made at ONERA.
%   --Facesheet (resistance of 0.5)
%   Porosity ?%
%   Thickness ? 
%   Hole diameter ?
%   --Honeycomb
%   Cell diameter 38.1mm
%   Cell depth 3/8 inch
%-- Perforated plate constants (Pure resistance R=0.5)
R=0.46;
%-- Cavity constant
lc = 38.1e-3; % Cavity length (m)
dc = (3/8)*0.0254; % Cavity diameter (m)
% Corrections
%lc = 35.8e-3; % shorter cavity (lower resonant frequency)
%% I - Load & display experimental data
% Output fields:
%   Z_exp (rad) real part + 1i* imag part (normalised impedance)
%   Alpha_exp (rad) absorption coefficient

ExpData110dB = xlsread('ATK_MA150305_110dB.xls');
ExpData120dB = xlsread('ATK_MA150305_120dB.xls');
ExpData130dB = xlsread('ATK_MA150305_130dB.xls');
    % Low-pass filter
[b,a]=butter(4,0.02,'low');
ExpData110dB(:,8)=filter(b,a,ExpData110dB(:,8));
ExpData120dB(:,8)=filter(b,a,ExpData120dB(:,8));
ExpData130dB(:,8)=filter(b,a,ExpData130dB(:,8));
ExpData110dB(:,9)=filter(b,a,ExpData110dB(:,9));
ExpData120dB(:,9)=filter(b,a,ExpData120dB(:,9));
ExpData130dB(:,9)=filter(b,a,ExpData130dB(:,9));
ExpData110dB(:,7)=filter(b,a,ExpData110dB(:,7));
ExpData120dB(:,7)=filter(b,a,ExpData120dB(:,7));
ExpData130dB(:,7)=filter(b,a,ExpData130dB(:,7));
    % Remove low & high frequencies
trim1=250; trim2=1e3;
ExpData110dB = ExpData110dB(trim1:50:end-trim2,:);
ExpData120dB = ExpData120dB(trim1:50:end-trim2,:);
ExpData130dB = ExpData130dB(trim1:50:end-trim2,:);

w_exp = 2*pi*ExpData110dB(:,1); % Experimental pulsation (rad/s)
St_exp = St_fun(w_exp,dc); % Experimental Stokes number (rad)
if boolf==0
   fvarexp = St_exp;
else
   fvarexp = ExpData110dB(:,1);
end
Z_exp = struct('dB110',ExpData110dB(:,8)+1i*ExpData110dB(:,9),...
           'dB120',ExpData120dB(:,8)+1i*ExpData120dB(:,9),...
           'dB130',ExpData130dB(:,8)+1i*ExpData130dB(:,9));
Alpha_exp = struct('dB110',ExpData110dB(:,7),'dB120',ExpData120dB(:,7),...
           'dB130',ExpData130dB(:,7));
clear ExpData110dB ExpData120dB ExpData130dB

clf
subplot(2,2,1)
leg=cell(0);
hold all
plot(abs(fvarexp),real(Z_exp.dB110),'-');
leg(end+1)={'Imp. Tube,M=0,110dB'};
plot(abs(fvarexp),real(Z_exp.dB120),'-');
leg(end+1)={'Imp. Tube,M=0,120dB'};
plot(abs(fvarexp),real(Z_exp.dB130),'-');
leg(end+1)={'Imp. Tube,M=0,130dB'};
legend(leg);
xlabel(fvar_title);
ylabel('Normalized resistance (rad)');
ylim([0,2]);
subplot(2,2,2)
leg=cell(0);
hold all
plot(abs(fvarexp),imag(Z_exp.dB110),'-');
leg(end+1)={'Imp. Tube,M=0,110dB'};
plot(abs(fvarexp),imag(Z_exp.dB120),'-');
leg(end+1)={'Imp. Tube,M=0,120dB'};
plot(abs(fvarexp),imag(Z_exp.dB130),'-');
leg(end+1)={'Imp. Tube,M=0,130dB'};
legend(leg);
xlabel(fvar_title);
ylabel('Normalized reactance (rad)');
ylim([-5,5]);
subplot(2,2,3)
leg=cell(0);
hold all
plot(abs(fvarexp),Alpha_exp.dB110,'-');
leg(end+1)={'Imp. Tube,M=0,110dB'};
plot(abs(fvarexp),Alpha_exp.dB120,'-');
leg(end+1)={'Imp. Tube,M=0,120dB'};
plot(abs(fvarexp),Alpha_exp.dB130,'-');
leg(end+1)={'Imp. Tube,M=0,130dB'};
legend(leg);
ylim([0,1]);
xlabel(fvar_title);
ylabel('Absorption coefficient (rad)');

%% II - Compare to model (impedance)
u_visc = linspace(1,220,1e2);
St = sqrt(-1i)*u_visc;
w = w_fun(St,dc);
w_ref = w_exp;
Z_ref = Z_exp.dB110;

    % L2 errors of the un-corrected models
%err_high = 3.790333e-1+1i*1.141972;
%err_low = 2.756315e-1+1i*1.076663;
%err_mid = 3.491181e-1+1i*9.530967e-1;

%Z(:,2) = Z(:,2)-z_cav(k0(w_ref),0.95*lc); % cavity subtraction from exp.

if boolf==0
   fvar = u_visc;
else
    fvar = w/(2*pi);
end
f=clf;
lg='Imp. Tube,M=0,110dB';
plotComplex(f,abs(fvarexp),Z_exp.dB110,'r-',lg);
%--
% z=@(w)((z_MP(k_br(d,w,Pr),l,k_br(dc,w,Pr),lc,sigma))./sigma);
% lg=sprintf('k Br. kc Br. (generic) (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - Z_ref),2),max(real(z(w))));
% plotComplex(f,fvar,z(w),'k',lg);
%--
% z=@(w)((z_MP(k_br(d,w,Pr),l,k0(w),lc,sigma))./sigma);
% lg=sprintf('k Br. kc LEE (generic) (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - Z_ref),2),max(real(z(w))));
% plotComplex(f,fvar,z(w),'r',lg);
%--
z=@(w)((R + z_cav(k_br(dc,w,Pr),lc)));
lg=sprintf('Crandall + kc Br. (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - Z_ref),2),max(real(z(w))));
plotComplex(f,fvar,z(w),'k--',lg);
%--
z=@(w)((0.5 + z_cav(k0(w),lc)));
lg=sprintf('Crandall + kc LEE (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - Z_ref),2),max(real(z(w))));
plotComplex(f,fvar,z(w),'r--',lg);
%--
%lg=sprintf('EHR (|St-eps|=%1.2g,|f-eps|=%1.2g Hz)',uvisc_ehr,w_ehr/(2*pi));
%plotComplex(f,fvar,z_ehr_tuned(R,0,b_ehr,lc,dc,w_ehr,Pr,w),'b-',lg);
%plot(fvar_ehr*[1,1],[0,7],'--');
%--
subplot(1,2,1)
xlabel(fvar_title);
ylabel('Normalized resistance (rad)');
%set(gca,'DataAspectRatio',[1,1,1])
ylim([0,5]);
subplot(1,2,2)
xlabel(fvar_title);
ylabel('Normalized reactance (rad)');
%set(gca,'DataAspectRatio',[1,1,1])
ylim([-5,5]);
%% II - Compare to model (|absorbtion coefficient|)
u_visc = linspace(1,220,5e1);
St = sqrt(-1i)*u_visc;
w = w_fun(St,dc);
w_ref = w_exp;
Z_ref = Z_exp.dB110;
if boolf==0
   fvar = u_visc;
else
    fvar = w/(2*pi);
end
clf
leg=cell(0);
hold all
plot(abs(fvarexp),Alpha_exp.dB110,'r-');
leg(end+1)={'Imp. Tube,M=0,110dB'};
%--
% z=@(w)((z_MP(k_br(d,w,Pr),l,k_br(dc,w,Pr),lc,sigma))./sigma);
% z=@(w)(alpha_b(z(w)));
% plot(fvar,real(z(w)),'k');
% leg(end+1)={sprintf('k Br. kc Br. (generic) (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - alpha_b(Z_ref)),2),max(real(z(w))))};
%--
% z=@(w)((z_MP(k_br(d,w,Pr),l,k0(w),lc,sigma))./sigma);
% z=@(w)(alpha_b(z(w)));
% plot(fvar,real(z(w)),'r');
% leg(end+1)={sprintf('k Br. kc LEE (generic) (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - alpha_b(Z_ref)),2),max(real(z(w))))};
%--
z=@(w)((R + z_cav(k_br(dc,w,Pr),lc)));
z=@(w)(alpha_b(z(w)));
plot(fvar,real(z(w)),'k--');
leg(end+1)={sprintf('Crandall + kc Br. (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - alpha_b(Z_ref)),2),max(real(z(w))))};
%--
% z=@(w)((0.5 + z_cav(k0(w),lc)));
% z=@(w)(alpha_b(z(w)));
% plot(fvar,real(z(w)),'r--');
% leg(end+1)={sprintf('Crandall + kc LEE (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - alpha_b(Z_ref)),2),max(real(z(w))))};
%--
plot(fvar,alpha_b(z_ehr_tuned(R,0,b_ehr,lc,dc,w_ehr,Pr,w)),'-');
leg(end+1)={sprintf('EHR (|St-eps|=%1.2g,|f-eps|=%1.2g Hz)',uvisc_ehr,w_ehr/(2*pi))};
plot(fvar_ehr*[1,1],[0,7],'--');
%--
legend(leg);
xlabel(fvar_title);
ylabel('Absorption coeff. alpha_b (rad)');
%set(gca,'DataAspectRatio',[1,1,1])
ylim([0,1]);
%% Unpublished ONERA Measurements - DDOF(project ALIAS)
% DDOF liner (concept 2)
%   --1st plate
%   Porosity 2.5%
%   Thickness 1mm 
%   Hole diameter 0.3mm
%   --1st Honeycomb
%   Cell diameter ?mm
%   Cell depth 5mm
%   --2nd plate
%   Porosity 5%
%   Thickness 0.5mm 
%   Hole diameter 0.53mm
%   --2nd honeycomb
%   Cell diameter ?mm 
%   Cell depth 40mm
sigma1 = 2.5/100; % Porosity (rad)
sigma2=5/100;
l1 = 1e-3; % Cylinder thickness (m)
d1 = 0.3e-3; % Cylinder diameter (m)
l2 = 0.5e-3;
d2 = 0.53e-3;
%-- Cavity constant
lc1 = 5e-3; % Cavity length (m)
dc1 = 9e-3; % Cavity diameter (m)
dc1 = (3/8)*0.0254;
lc2 = 40e-3;
dc2 = dc1;
% Correction
sigma1=5/100;
%% I - Load & display experimental data
ExpData110dB = csvread('Impedance_DDOF_Concept2_110dB.csv',2);
    % Low-pass filter
[b,a]=butter(4,0.50,'low');
ExpData110dB(:,2)=filter(b,a,ExpData110dB(:,2));
ExpData110dB(:,3)=filter(b,a,ExpData110dB(:,3));
    % Remove low & high frequencies
trim1=20; trim2=150;
ExpData110dB = ExpData110dB(trim1:6:end-trim2,:);

w_exp = 2*pi*ExpData110dB(:,1); % Experimental pulsation (rad/s)
St_exp = St_fun(w_exp,d1); % Experimental Stokes number (rad)
Z_exp = ExpData110dB(:,2)+1i*ExpData110dB(:,3);

if boolf==1
    fvarexp=ExpData110dB(:,1);
else
    fvarexp=St_exp;
end
clear ExpData110dB
clf
subplot(1,2,1)
leg=cell(0);
hold all
plot(abs(fvarexp),real(Z_exp),'-');
leg(end+1)={'Imp. Tube,M=0,110dB'};
legend(leg);
xlabel(fvar_title);
ylabel('Normalized resistance (rad)');
ylim([0,5]);
subplot(1,2,2)
leg=cell(0);
hold all
plot(abs(fvarexp),imag(Z_exp),'-');
leg(end+1)={'Imp. Tube,M=0,110dB'};
legend(leg);
xlabel(fvar_title);
ylabel('Normalized reactance (rad)');
ylim([-5,5]);
%% II - Compare to model (impedance)
u_visc = linspace(1,8.5,2e2);
St = sqrt(-1i)*u_visc;
w = w_fun(St,d1);
w_ref = w_exp;
Z_ref = Z_exp;

    % L2 errors of the un-corrected models
%err_high = 3.790333e-1+1i*1.141972;
%err_low = 2.756315e-1+1i*1.076663;
%err_mid = 3.491181e-1+1i*9.530967e-1;

%Z(:,2) = Z(:,2)-z_cav(k0(w_ref),0.95*lc); % cavity subtraction from exp.
if boolf==1
    fvar=w/(2*pi);
else
    fvar=u_visc;
end

f=clf;
%--
lg='DDOF,M=0,110dB';
plotComplex(f,abs(fvarexp),Z_exp,'r-',lg);
%--
z1 = @(w)(z_cr(l1,d1,w)); % Normalised impedance 1st plate (rad)
z2 = @(w)(z_cr(l2,d2,w)); % Normalised impedance 2nd plate (rad)
k1 = @(w)(k_br(dc1,w,Pr)); % Propagation wave number 1st cavity (m^-1)
k2 = @(w)(k_br(dc2,w,Pr)); % Propagation wave number 1st cavity (m^-1)
z = @(w)((z_DDOF(z1(w),z2(w),k1(w),k2(w),lc1,lc2,sigma1,sigma2)));
lg=sprintf('Cr./Br. + Cr./Br. (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - Z_ref),2),max(real(z(w))));
plotComplex(f,fvar,z(w),'k',lg);
%--
z1 = @(w)(z_cr(l1,d1,w)); % Normalised impedance 1st plate (rad)
z2 = @(w)(z_cr(l2,d2,w)); % Normalised impedance 2nd plate (rad)
k1 = @(w)(w/c0); % Propagation wave number 1st cavity (m^-1)
k2 = @(w)(w/c0); % Propagation wave number 1st cavity (m^-1)
z = @(w)((z_DDOF(z1(w),z2(w),k1(w),k2(w),lc1,lc2,sigma1,sigma2)));
lg=sprintf('Cr./LEE + Cr./LEE (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - Z_ref),2),max(real(z(w))));
plotComplex(f,fvar,z(w),'b',lg);
%--
subplot(1,2,1)
xlabel(fvar_title);
ylabel('Normalized resistance (rad)');
%set(gca,'DataAspectRatio',[1,1,1])
ylim([0,5]);
subplot(1,2,2)
xlabel(fvar_title);
ylabel('Normalized reactance (rad)');
%set(gca,'DataAspectRatio',[1,1,1])
ylim([-5,5]);
%% II - Compare to model (|absorption coefficient|)
u_visc = linspace(1,8.5,1.2e2);
St = sqrt(-1i)*u_visc;
w = w_fun(St,d1);
w_ref = w_exp;
Z_ref = Z_exp;

    % L2 errors of the un-corrected models
%err_high = 3.790333e-1+1i*1.141972;
%err_low = 2.756315e-1+1i*1.076663;
%err_mid = 3.491181e-1+1i*9.530967e-1;

%Z(:,2) = Z(:,2)-z_cav(k0(w_ref),0.95*lc); % cavity subtraction from exp.
if boolf==1
    fvar=w/(2*pi);
else
    fvar=u_visc;
end

clf
leg=cell(0);
hold all
plot(abs(fvarexp),abs(alpha_b(Z_exp)),'r-');
leg(end+1)={'DDOF,M=0,110dB'};
%--
z1 = @(w)(z_cr(l1,d1,w)); % Normalised impedance 1st plate (rad)
z2 = @(w)(z_cr(l2,d2,w)); % Normalised impedance 2nd plate (rad)
k1 = @(w)(k_br(dc1,w,Pr)); % Propagation wave number 1st cavity (m^-1)
k2 = @(w)(k_br(dc2,w,Pr)); % Propagation wave number 1st cavity (m^-1)
z = @(w)((z_DDOF(z1(w),z2(w),k1(w),k2(w),lc1,lc2,sigma1,sigma2)));
plot(fvar,abs(alpha_b(z(w))),'k');
leg(end+1)={sprintf('Cr./Br. + Cr./Br. (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - Z_ref),2),max(real(z(w))))};
%--
z1 = @(w)(z_cr(l1,d1,w)); % Normalised impedance 1st plate (rad)
z2 = @(w)(z_cr(l2,d2,w)); % Normalised impedance 2nd plate (rad)
k1 = @(w)(w/c0); % Propagation wave number 1st cavity (m^-1)
k2 = @(w)(w/c0); % Propagation wave number 1st cavity (m^-1)
z = @(w)((z_DDOF(z1(w),z2(w),k1(w),k2(w),lc1,lc2,sigma1,sigma2)));
plot(fvar,abs(alpha_b(z(w))),'b');
leg(end+1)={sprintf('Cr./LEE + Cr./LEE (l2=%1.1e,max=%1.1e)',norm(real(z(w_ref) - Z_ref),2),max(real(z(w))))};
%--
legend(leg);
xlabel(fvar_title);
ylabel('Absorption coeff. alpha_b (rad)');
%set(gca,'DataAspectRatio',[1,1,1])
ylim([0,1]);