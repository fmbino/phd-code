%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Definition of impedance models
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definition of the notations:
% St    (complex) Stokes number (rad).
% u_visc (real) modulus of the Stokes number (rad)
% w     (real,positive or negative) pulsation (rad/s)
% d
% l
% lc
% dc
% For plots using non-dimensional quantities, alternative functions are
% defined, and use the following arguments:
% rld   dimensional ratio l/((d/2)^2)
% rlc   non-dimensional ratio lc/l (rad)
% rd    non-dimensional ratio  (dc/d) (rad)

%% Fluid quantities
% Air @26°C
rho0=1.177; % air density
nu = 1.568e-5; % kinematic viscosity
gamma = 1.4; % ratio of spec heat constant
c0=340.29; % speed of sound
mu = rho0*nu; % dynamic viscosity
Pr = 0.707; % Prandtl number
alpha = nu/Pr; % thermal diffusivity
% Air @15°C
rho0=1.225; % air density
nu = 1.48e-5; % kinematic viscosity
gamma = 1.4; % ratio of spec heat constant
c0=340.27; % speed of sound
mu = rho0*nu; % dynamic viscosity
Pr = 0.707; % Prandtl number (unsure)
alpha = nu/Pr; % thermal diffusivity
%% Definition of misc. functions
    % Lambda function
Lambda = @(s)((2./s).*(besselj(1,s)./besselj(0,s)));
LambdaI = @(s)((2./s).*(besseli(1,s)./besseli(0,s)));
    % (Hermitian) asymptotic expansion of the Lambda function
Lambda_high = @(s)(2*1i/s*sign(imag(s)));
    % Deduce pulsation (rad/s) from Stokes number (rad)
w_fun = @(St,d)(1i*nu/((d/2)^2)*(St.^2));
    % Deduce Stokes number (rad) from pulsation (rad/s)
St_fun = @(w,d)(sqrt(-1i*w/nu)*(d/2));
    % Deduce absorption reflexion coefficient from normalized impedance
    % Only the magnitude matters
alpha_b = @(z)(1-abs((z-1)./(z+1)).^2);
%% Definition of the various wave numbers
% ! Approximate wave numbers are not hermitian
k0 = @(w)(w/c0); % Free space Wave number (m^-1)
k0l = @(St,rld)(1i*(nu/c0)*rld*St.^2); % Product k0*l (rad)
knu = @(w)(sqrt(-1i*w/nu)); % Viscous diffusion wave number (m^-1)
kt = @(w)(sqrt(-1i*w/alpha)); % Thermal diffusion wave number (m^-1)
    % Wave number for the propagation in a cylinder of diameter d (Stokes)
    % Source: Bruneau, Manuel acoustique. (no thermal effect: '0')
k_br = @(d,w,Pr)(k0(w).*sqrt((1+(gamma-1)*Lambda(sqrt(Pr)*knu(w)*d/2))./(1-Lambda(knu(w)*d/2))));
k_br_ko = @(St,Pr)(sqrt((1+(gamma-1)*Lambda(sqrt(Pr)*St))./(1-Lambda(St))));
    % Approximated versions
k_high = @(d,w,Pr)(k0(w).*(1+(2/d)*(1+(gamma-1)/sqrt(Pr))*sqrt(-1i*nu./w)));
k_high_ko = @(St,Pr)(1+1i*sign(imag(St)).*(1./St)*(1+(gamma-1)/sqrt(Pr)));
k_low_ko = @(St)((sqrt(-8*gamma./(St.^2))));
    % (Hermitian) high/low stokes number approximation of 1i*k_br(w)
k_high_i = @(d,w,Pr)((1i*k0(w)+(sqrt(nu)/(c0*(d/2)))*(1+(gamma-1)/sqrt(Pr))*sqrt(1i*w)));
k_low_i = @(d,w)(sqrt(8*gamma*nu)/(c0*(d/2))*sqrt(1i*w));
%% Definition of the various wave numbers (hermitian j*k(s), s=j*w)
jk0 = @(s)(s/c0);
%jk_FPIM = @(s)(1);
jk_br = @(d,s,Pr)(jk0(s).*sqrt((1+(gamma-1)*LambdaI(sqrt(Pr)*sqrt(s/nu)*d/2))./(1-LambdaI(sqrt(s/nu)*d/2))));
    % Bruneau wave number high frequency approx.
jk_high = @(d,s,Pr)(s/c0 + 1/c0*(2/d)*(1+(gamma-1)/sqrt(Pr))*sqrt(nu)*sqrt(s));
%% Non-physical model
    % Extended Helmholtz Resonator (EHR)
        % Original definition from [Rienstra2006]
        % Parameters are typically the result of some data fitting
z_ehr = @(r,m,b,nu,dt,e,w)(r+1i*m*w-1i*b*cot(w*nu*dt/2-1i*e/2));
        % "Tuned" model: parameters in the cotan are computed using the
        % Bruneau wavenumber at a given pulsation w_ehr
z_ehr_tuned = @(r,m,b,lc,dc,w_ehr,Pr,w)(r+1i*m*w-1i*b*cot(w*(lc*real(k_br(dc,w_ehr,Pr))/w_ehr)-1i*(-lc*imag(k_br(dc,w_ehr,Pr))))); 
    % Simple numerical model
z_num = @(b0,b1,b2,w)(b0 + b1*(1i*w) + b2*sqrt(1i*w) + z_cav(lc,w));
%% Definition of the impedance models
% ! Some formulae for approximate models are not hermitian.
% Below are expression of Normalized impedance model (rad), for one hole.
% Variables are:
%-- Perforated plate constants (if any)
% sigma     Porosity (rad)
% l         Thickness (m)
% d         Diameter (m)
%-- Cavity constants
% lc         Depth (m)
% dc        Diameter (m)
% Terminology:
%   low/mid/high: Stokes Number range <1,[1,10],>10.

    % Corrections
        % Viscous correction at low Stokes number
Cor_visc_low = @(d)(32*nu/(c0*d));
        % Viscous correction at high Stokes number
Cor_visc_high = @(d,w)((16*nu)/(c0*d)+(4*sqrt(nu)/c0)*sqrt(1i*w));
        % Viscous correction at mid Stokes number (Maa)
Cor_visc_mid = @(w)((sqrt(nu)/c0)*sqrt(1i*w));
        % Radiation correction
Cor_rad = @(d,w)((1/8)*(d*k0(w)).^2 + 1i*k0(w)*2*(4*d)/(3*pi));
        % Radiation & Interaction correction
Cor_rad_int = @(d,sigma,w)((1/8)*(d*k0(w)).^2 + 1i*k0(w)*2*(4*d)/(3*pi)*(1-0.7*sqrt(sigma)));
    
    % Generic model for a MP liner
    % k -> wave number for the perforation
    % kc -> wave number for the cavity
z_MP = @(k,l,kc,lc,sigma)((1i*tan(k*l)-1i*sigma*cot(kc*lc))./(1+sigma*tan(k*l).*cot(kc*lc)));
    % Generic model for a cavity liner
z_cav = @(kc,lc)(-1i*cot(lc*kc));
    % Exact Crandall w/o corrections (Popie & Malmary consistent)
z_cr = @(l,d,w)((1i*k0(w)*l)./(1-Lambda(knu(w)*d/2)));
z_cr_alt = @(St,rld)((1i*k0l(St,rld))./(1-Lambda(St)));
    % Crandall Low Stokes approximation
z_cr_low = @(l,d,w)(32*nu*l/(c0*d^2)+1i*((4/3)*l*k0(w)));
z_cr_low_alt = @(St,rld)(8*nu/c0*rld+1i*4/3*k0l(St,rld));
        % Corrected version (Viscosity+Interaction+Radiation)
z_cr_low_cor = @(l,d,sigma,w)(z_cr_low(l,d,w)+Cor_visc_low(d)+Cor_rad_int(d,sigma,w));
    % Crandall High Stokes approximation (with additional resistance term)
z_cr_high = @(l,d,w)(12*nu*l/(c0*d^2) + 4*sqrt(nu)*l/(c0*d)*sqrt(1i*w)+1i*l*k0(w));
% wrong constant term:
%z_cr_high_alt = @(St,rld)(4*nu/c0*rld+ 1i*2*nu/c0*rld*sign(-imag(St)).*St+1i*k0l(St,rld));
        % Corrected version (Viscosity+Interaction+Radiation) (=Malmary)
z_cr_high_cor = @(l,d,sigma,w)(z_cr_high(l,d,w)+Cor_visc_high(d,w)+Cor_rad_int(d,sigma,w));
    % Maa model (without correction)
        % Original Model (non-hermitian)
z_maa_or = @(l,d,w)(((32*nu*l)/(c0*d^2))*sqrt(1+((d^2)/128)*(1i*knu(w).^2))+ 1i*l*k0(w).*(1+(3^2+(d^2/8)*(1i*knu(w)).^2).^(-1/2)));
        % Model corrected for hemitianity
z_maa_cor = @(l,d,w)(((32*nu*l)/(c0*d^2))*sqrt(1+((d^2)/128)*abs(knu(w)).^2)+ 1i*l*k0(w).*(1+(3^2+(d^2/8)*abs(knu(w)).^2).^(-1/2)));
z_maa_alt = @(St,rld)(8*nu/c0*rld*(1+(1i*St.^2)/32).^(1/2)+1i*(k0l(St,rld)).*(1+(9+1i*(St.^2)/2).^(-1/2)));

    % Crandall model for a DDOF liner
    %   z1 ->   Normalised impedance 1st plate (rad) (no division by
    %   porosity) ex: Crandall
    %   z2 ->   Normalised impedance 2nd plate (rad) (no division by
    %   porosity) ex: Crandall
    %   k1 ->   Propagation wave number 1st cavity (m^-1) ex: Bruneau
    %   k2 ->   Propagation wave number 1st cavity (m^-1) ex: Bruneau
z_DDOF = @(z1,z2,k1,k2,lc1,lc2,sigma1,sigma2)(z1/sigma1 + ((z2/sigma2-1i*cot(k2*lc2)).*cos(k1*lc1)+1i*sin(k1*lc1))./(cos(k1*lc1)+1i*(z2/sigma2-1i*cot(k2*lc2)).*sin(k1*lc1)));