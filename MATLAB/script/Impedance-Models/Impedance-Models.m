%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Comparison and plots of impedance models
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load the models from 'Definition-Impedance-Models'

%% Comparison of propagation wave number for a cylinder k/k0 (OK for positive pulsation)
u_visc = [-linspace(1e-6,10,2e1),linspace(1e-6,10,2e1)]; % |Stokes wave number|
%u_visc = linspace(1,1e2,1e3);
St = (u_visc>0).*(sqrt(-1i)*abs(u_visc)) + (u_visc<0).*(sqrt(1i)*abs(u_visc));
    % Comparison of the propagation wave number
f=clf;
%--
lg='Bruneau';
plotComplex(f,u_visc,k_br_ko(St,Pr),'',lg);
lg='Br. High';
plotComplex(f,u_visc,k_high_ko(St,Pr),'',lg);
lg='Low-St.';
plotComplex(f,u_visc,real(k_low_ko(St)),'',lg);
%--
subplot(1,2,1)
xlabel('Stokes wave number (k_{nu}xd/2) (rad)');
ylabel('Real(k/k_0) (rad)');
axis([min(u_visc),max(u_visc),0,3])
subplot(1,2,2)
xlabel('Stokes wave number (k_{nu}xd/2) (rad)');
ylabel('Imag(k/k_0) (rad)');
axis([min(u_visc),max(u_visc),-3,0])
%% Bruneau wave number path
St = [sqrt(1i)*linspace(0,5,1e2),sqrt(-1i)*linspace(0,5,1e2)];
w = w_fun(St,dc);
clf
leg=cell(0);
hold all
plot(real(1i*k0(w)),imag(1i*k0(w)));
leg(end+1)={'LEE'};
plot(real(1i*k_br(dc,w,Pr)),imag(1i*k_br(dc,w,Pr)));
leg(end+1)={'Bruneau'};
% plot(real(1i*k_high(dc,w,Pr)),imag(1i*k_high(dc,w,Pr)));
% leg(end+1)={'Bruneau High Stokes'};
plot(real(k_high_i(dc,w,Pr)),imag(k_high_i(dc,w,Pr)));
leg(end+1)={'Bruneau High Stokes j*k(w)'};
plot(real(k_low_i(dc,w)),imag(k_low_i(dc,w)));
leg(end+1)={'Bruneau Low Stokes j*k(w)'};
xlabel('Real(j*k) (m^-1)');
ylabel('Imag(j*k) (m^-1)');
title(sprintf('(dc=%1.1e m, Pr=%1.1g) |k_{nu}xdc/2| in [%d,%d]',dc,Pr,min(abs(St)),max(abs(St))));
legend(leg);

%% Comparison of the corresponding cavity impedance (OK)
% An arbitrary ratio must be given, but it fits anyway.
rld = 9.51e5; % Ratio (l / (d/2)^2) (m^-1)

u_visc = linspace(0,10,1e3);
St = sqrt(-1i)*u_visc;
    % Comparison of the propagation wav number
f=clf;
lg='Bruneau';
plotComplex(f,u_visc,z_cav(k_br_ko(St,Pr).*k0l(St,rld),1),'',lg);
lg='Br. High';
plotComplex(f,u_visc,z_cav(k_high_ko(St,Pr).*k0l(St,rld),1),'',lg);
% lg='Br. Low';
% plotComplex(f,u_visc,z_cav(k_low_ko(St).*k0l(St,rld),1),'',lg);
lg = 'LEE';
plotComplex(f,u_visc,z_cav(k0l(St,rld),1),'',lg);
subplot(1,2,1)
xlabel('Stokes number');
ylabel('Real(z cavity)');
axis([min(u_visc),max(u_visc),-1e-1,5])
subplot(1,2,2)
xlabel('Stokes number');
ylabel('Imag(z cavity)');
axis([min(u_visc),max(u_visc),-2,3])
%% Model comparison for a MP liner (perforation + cavity)
% Perforation: l,d / Cavity: lc, dc
rld = 3.55e4; % Ratio (l / (d/2)^2) (m^-1)
rlc = 47.62; % Ratio lc/l (rad)
rd = 31.67; % Ratio of the diameter (dc/d) (rad) 
sigma=5/100; % porosity (1=only hole)

k0lc = @(St,rld,rlc)(k0l(St,rld)*(rlc)); % Product k0*lc (rad)

u_visc = linspace(1,10,5.5e3);
St = sqrt(-1i)*u_visc;

f=clf;
%--
z=@(St)(z_MP(k_br_ko(St,Pr).*k0l(St,rld),1,k_br_ko(rd*St,Pr).*k0lc(St,rld,rlc),1,sigma)./sigma);
lg=sprintf('k Br. kc Br. (generic) (max=%1.1e)',max(real(z(St))));
plotComplex(f,u_visc,z(St),'k',lg);
%--
z=@(St)(z_MP(k_br_ko(St,Pr).*k0l(St,rld),1,k0lc(St,rld,rlc),1,sigma)./sigma);
lg=sprintf('k Br. kc LEE (generic) (max=%1.1e)',max(real(z(St))));
plotComplex(f,u_visc,z(St),'r',lg);
%--
z=@(St)(z_cr_alt(St,rld)./sigma + z_cav(k_br_ko(rd*St,Pr).*k0lc(St,rld,rlc),1));
lg=sprintf('Crandall + kc Br. (generic) (max=%1.1e)',max(real(z(St))));
plotComplex(f,u_visc,z(St),'k--',lg);
%--
z=@(St)(z_cr_alt(St,rld)./sigma + z_cav(k0l(St,rld),1));
lg=sprintf('Crandall + kc LEE (generic) (max=%1.1e)',max(real(z(St))));
plotComplex(f,u_visc,z(St),'r--',lg);
%--
subplot(1,2,1)
xlabel('Stokes number');
ylabel('Real(z) Normalised impedance');
axis([min(u_visc),max(u_visc),0,0.2])
subplot(1,2,2)
xlabel('Stokes number');
ylabel('Imag(z hole)');
axis([min(u_visc),max(u_visc),0,0.1])
%% Comparison of Crandall model, approximations & Maa (OK)
% Perforation: l,d
rld = 3.55e4; % Ratio (l / (d/2)^2) (m^-1)

u_visc = linspace(1e-4,15,3e1); % Stokes number (rad)
St = sqrt(-1i)*u_visc;

f=clf;
lg='Crandall';
plotComplex(f,u_visc,z_cr_alt(St,rld),'',lg);
lg='Cr. High';
plotComplex(f,u_visc,z_cr_high_alt(St,rld),'',lg);
lg='Cr. Low';
plotComplex(f,u_visc,z_cr_low_alt(St,rld),'',lg);
lg='Maa';
plotComplex(f,u_visc,z_maa_alt(St,rld),'',lg);
subplot(1,2,1)
xlabel('Stokes number');
ylabel('Real(z hole)');
axis([min(u_visc),max(u_visc),0,0.1])
subplot(1,2,2)
xlabel('Stokes number');
ylabel('Imag(z hole)');
axis([min(u_visc),max(u_visc),-1e-2,0.5])
%% Plot the Lambda function [SELF-SUFFICIENT]
% The Lambda function appears for the propagation in a cylinder.
% u is a complex variable.
% |u| would be the Stokes wave number.
Lambda = @(u)((2./u).*(besselj(1,u)./besselj(0,u)));
s = sqrt(-1i)*linspace(1,1e1,3e1);
subplot(1,2,1)
plot(abs(s),real(Lambda(s)));
xlabel('|u|');
ylabel('Real(Lambda(sqrt(-i)*u))');
axis([min(abs(s)),max(abs(s)),0,1]);
subplot(1,2,2)
plot(abs(s),imag(Lambda(s)));
xlabel('|u|');
ylabel('Imag(Lambda(sqrt(-i)*u))');
axis([min(abs(s)),max(abs(s)),-1,0]);

%% Approximation of the Crandall Model - Phi(u) [SELF-SUFFICIENT]
% Approximation of the fonction Phi(s), which appears in the Crandall
% model.
% s is a complex variable.
% u is the Stokes number (path in the complex plane). Defined as
%u=sqrt(-iw), so w>0 <-> Im(u)<0 and vice-versa.

    % Phi function of the Crandall Model
Phi = @(s)(-s.^2./(1-(2./s).*(besselj(1,s))./(besselj(0,s))));
    % Maa approximation (corrected and original)
Phi_Maa_co = @(s)(8*(1+(1/32)*abs(s).^2).^(1/2)-s.^2.*(1+(3^2+(1/2)*abs(s).^2).^(-1/2)));
Phi_Maa_or = @(s)((8*(1+(1/32)*(1i*(s.^2))).^(1/2)-s.^2.*(1+(3^2+(1/2)*(1i*(s.^2))).^(-1/2))));
    % Taylor expansion of Phi(s) in the complex plane
    % Polynomials functions are defined to ensure hermitian symmetry
    % If the constant coeff. is complex, there will be a discontinuity on
    % the reactance.
        % Around 0 (real coeff, hermitian symmetry ensured)
C_0 = [-11/34836480, 0,-7/829440,0,-1/4320,0,-1/144,0,-4/3,0,8];
Pol_0 = @(s)(polyval(C_0((end-3):end),s));
    % around 3*sqrt(-j)
s_3 = 3*sqrt(-1i);
C_3 = [-(1.127121309-0.2409086421*1i),-(5.014554327-5.897378724*1i),8.512802493+11.84796297*1i];
Pol_3 = @(s)((imag(s)<=0).*polyval(C_3,s-s_3)+(imag(s)>0).*polyval(conj(C_3),s-conj(s_3)));
    % around 4*sqrt(-j)
s_4 = 4*sqrt(-1i);
C_4 = [-0.9579542195 + 0.1802092455*1i,-6.162438391+7.677254446*1i,9.370945350+20.62775837*1i];
Pol_4 = @(s)((imag(s)<=0).*polyval(C_4,s-s_4)+(imag(s)>0).*polyval(conj(C_4),s-conj(s_4)));
    % around 5*sqrt(-j)
s_5 = 5*sqrt(-1i);
C_5 = [-(0.9252048219 - 0.05767404997*1i),-(7.313755218 - 9.156349624*1i),10.57884762 + 31.34904291*1i];
Pol_5 = @(s)((imag(s)<=0).*polyval(C_5,s-s_5)+(imag(s)>0).*polyval(conj(C_5),s-conj(s_5)));
    % around 6*sqrt(-j)
s_6 = 6*sqrt(-1i);
C_6 = [-(0.9642430819 - 0.007443026479*1i),- (8.612046751 - 10.52838100*1i),11.91605078 + 43.93249613*1i];
Pol_6 = @(s)((imag(s)<=0).*polyval(C_6,s-s_6)+(imag(s)>0).*polyval(conj(C_6),s-conj(s_6)));
    % around 7*sqrt(-j)
s_7 = 7*sqrt(-1i);
C_7 = [-(0.9870142346 - 0.003893006213*1i),-(9.988847191 - 11.91710751*1i),13.27578956 + 58.44081959*1i];
Pol_7 = @(s)((imag(s)<=0).*polyval(C_7,s-s_7)+(imag(s)>0).*polyval(conj(C_7),s-conj(s_7)));
    % around 8*sqrt(-j)
s_8 = 8*sqrt(-1i);
C_8 = [-(0.9926420754 - 0.004926662537*1i),- (11.38311654 - 13.32448021*1i),14.64372873 + 74.92030143*1i];
Pol_8 = @(s)((imag(s)<=0).*polyval(C_8,s-s_8)+(imag(s)>0).*polyval(conj(C_8),s-conj(s_8)));
    % around 9*sqrt(-j)
s_9 = 9*sqrt(-1i);
C_9 = [- (0.9945269220 - 0.003809069085*1i),- (12.78205376 - 14.73599933*1i),16.02112259 + 93.38454901*1i];
Pol_9 = @(s)((imag(s)<=0).*polyval(C_9,s-s_9)+(imag(s)>0).*polyval(conj(C_9),s-conj(s_9)));
    % around 10*sqrt(-j)
s_10 = 10*sqrt(-1i);
C_10 = [-(0.9960479261 - 0.002637056337*1i),- (14.18514104 - 16.14808268*1i),17.40614750 + 113.8378097*1i];
Pol_10 = @(s)((imag(s)<=0).*polyval(C_10,s-s_10)+(imag(s)>0).*polyval(conj(C_10),s-conj(s_10)));
    % around 2*sqrt(-j)
s_X = 2.5*sqrt(-1i);
C_X = [- (1.259292688 - 0.1768422078*1i),- (3.902517206 - 4.298124525*1i),8.158194763 + 6.427899488*1i];
Pol_X = @(s)((imag(s)<=0).*polyval(C_X,s-s_X)+(imag(s)>0).*polyval(conj(C_X),s-conj(s_X)));

    % Asymptotic expansion (in the lower plane)
C_Hi = [-1,2*1i,0];
Pol_high = @(s)((imag(s)<=0).*polyval(C_Hi,s) + (imag(s)>0).*polyval(conj(C_Hi),s));

C_Hi_alt = [-1,2*1i,4]; % Alternative definition
Pol_high_alt = @(s)((imag(s)<=0).*polyval(C_Hi_alt,s) + (imag(s)>0).*polyval(conj(C_Hi_alt),s));

u_int = [linspace(1e-3,4^2,50),linspace(4^2,15^2,50)];
u = sqrt(-1i*[-fliplr(u_int),u_int]); % Stokes number
s = u;
clf
subplot(1,2,1)
err_ref = norm(real(Pol_high_alt(s)-Phi(s)));
leg=cell(0);
hold all
plot(-abs(s).*sign(angle(s)),real(Phi(s)));
leg(end+1)={'Phi(u)'};
% plot(-abs(s).*sign(angle(s)),real(Pol_high(s)));
% leg(end+1)={sprintf('St=inf (%1.2g%%)',100*(norm(real(Pol_high(s)-Phi(s)))/err_ref-1))};
plot(-abs(s).*sign(angle(s)),real(Pol_high_alt(s)));
leg(end+1)={sprintf('St=inf (%1.2g%%)',100*(norm(real(Pol_high_alt(s)-Phi(s)))/err_ref-1))};
plot(-abs(s).*sign(angle(s)),real(Phi_Maa_co(s)));
leg(end+1)={sprintf('Maa (%1.2g%%)',100*(norm(real(Phi_Maa_co(s)-Phi(s)))/err_ref-1))};
% plot(-abs(s).*sign(angle(s)),real(Pol_X(s)));
% leg(end+1)={sprintf('St=X (%1.2g%%)',100*(norm(real(Pol_X(s)-Phi(s)))/err_ref-1))};
plot(-abs(s).*sign(angle(s)),real(Pol_0(s)));
leg(end+1)={sprintf('St=0 (%1.2g%%)',100*(norm(real(Pol_0(s)-Phi(s)))/err_ref-1))};
% plot(-abs(s).*sign(angle(s)),real(Pol_3(s)));
% leg(end+1)={sprintf('St=3 (%1.2g%%)',100*(norm(real(Pol_3(s)-Phi(s)))/err_ref-1))};
% plot(-abs(s).*sign(angle(s)),real(Pol_4(s)));
% leg(end+1)={sprintf('St=4 (%1.2g%%)',100*(norm(real(Pol_4(s)-Phi(s)))/err_ref-1))};
% plot(-abs(s).*sign(angle(s)),real(Pol_5(s)));
% leg(end+1)={sprintf('St=5 (%1.2g%%)',100*(norm(real(Pol_5(s)-Phi(s)))/err_ref-1))};
% plot(-abs(s).*sign(angle(s)),real(Pol_6(s)));
% leg(end+1)={sprintf('St=6 (%1.2g%%)',100*(norm(real(Pol_6(s)-Phi(s)))/err_ref-1))};
plot(-abs(s).*sign(angle(s)),real(Pol_7(s)));
leg(end+1)={sprintf('St=7 (%1.2g%%)',100*(norm(real(Pol_7(s)-Phi(s)))/err_ref-1))};
% plot(-abs(s).*sign(angle(s)),real(Pol_8(s)));
% leg(end+1)={sprintf('St=8 (%1.2g%%)',100*(norm(real(Pol_8(s)-Phi(s)))/err_ref-1))};
% plot(-abs(s).*sign(angle(s)),real(Pol_9(s)));
% leg(end+1)={sprintf('St=9 (%1.2g%%)',100*(norm(real(Pol_9(s)-Phi(s)))/err_ref-1))};
% plot(-abs(s).*sign(angle(s)),real(Pol_10(s)));
% leg(end+1)={sprintf('St=10 (%1.2g%%)',100*(norm(real(Pol_10(s)-Phi(s)))/err_ref-1))};
xlabel('Stokes number |ks d/2|');
ylabel('Real (rad)');
legend(leg)
axis([0,7,4,16])
subplot(1,2,2)
err_ref = norm(imag(Pol_high_alt(s)-Phi(s)));
%u_int = [linspace(1e-3,4^2,5*1e4),linspace(4,15^2,5*1e4)];
%u = sqrt(-1i*[-fliplr(u_int),u_int]); % Stokes number
%s = u;
leg=cell(0);
hold all
plot(-abs(s).*sign(angle(s)),imag(Phi(s)));
leg(end+1)={'Phi(u)'};
% plot(-abs(s).*sign(angle(s)),imag(Pol_high(s)));
% leg(end+1)={sprintf('St=inf (%1.2g%%)',100*(norm(imag(Pol_high(s)-Phi(s)))/err_ref-1))};
plot(-abs(s).*sign(angle(s)),imag(Pol_high_alt(s)));
leg(end+1)={sprintf('St=inf (alt) (%1.2g%%)',100*(norm(imag(Pol_high_alt(s)-Phi(s)))/err_ref-1))};
plot(-abs(s).*sign(angle(s)),imag(Phi_Maa_co(s)));
leg(end+1)={sprintf('Maa (%1.2g%%)',100*(norm(imag(Phi_Maa_co(s)-Phi(s)))/err_ref-1))};
% plot(-abs(s).*sign(angle(s)),imag(Pol_X(s)));
% leg(end+1)={sprintf('St=X (%1.2g%%)',100*(norm(imag(Pol_X(s)-Phi(s)))/err_ref-1))};
plot(-abs(s).*sign(angle(s)),imag(Pol_0(s)));
leg(end+1)={sprintf('St=0 (%1.2g%%)',100*(norm(imag(Pol_0(s)-Phi(s)))/err_ref-1))};
% plot(-abs(s).*sign(angle(s)),imag(Pol_3(s)));
% leg(end+1)={sprintf('St=3 (%1.2g%%)',100*(norm(imag(Pol_3(s)-Phi(s)))/err_ref-1))};
% plot(-abs(s).*sign(angle(s)),imag(Pol_4(s)));
% leg(end+1)={sprintf('St=4 (%1.2g%%)',100*(norm(imag(Pol_4(s)-Phi(s)))/err_ref-1))};
% plot(-abs(s).*sign(angle(s)),imag(Pol_5(s)));
% leg(end+1)={sprintf('St=5 (%1.2g%%)',100*(norm(imag(Pol_5(s)-Phi(s)))/err_ref-1))};
% plot(-abs(s).*sign(angle(s)),imag(Pol_6(s)));
% leg(end+1)={sprintf('St=6 (%1.2g%%)',100*(norm(imag(Pol_6(s)-Phi(s)))/err_ref-1))};
plot(-abs(s).*sign(angle(s)),imag(Pol_7(s)));
leg(end+1)={sprintf('St=7 (%1.2g%%)',100*(norm(imag(Pol_7(s)-Phi(s)))/err_ref-1))};
% plot(-abs(s).*sign(angle(s)),imag(Pol_8(s)));
% leg(end+1)={sprintf('St=8 (%1.2g%%)',100*(norm(imag(Pol_8(s)-Phi(s)))/err_ref-1))};
% plot(-abs(s).*sign(angle(s)),imag(Pol_9(s)));
% leg(end+1)={sprintf('St=9 (%1.2g%%)',100*(norm(imag(Pol_9(s)-Phi(s)))/err_ref-1))};
% plot(-abs(s).*sign(angle(s)),imag(Pol_10(s)));
% leg(end+1)={sprintf('St=10 (%1.2g%%)',100*(norm(imag(Pol_10(s)-Phi(s)))/err_ref-1))};
xlabel('Stokes number |ks d/2|');
ylabel('Imag (rad)');
legend(leg)
axis([0.1,7,-2,20])

%% Relative error plot

u_int = [linspace(25,50^2,20)];
u = sqrt(-1i*[u_int]); % Stokes number
s = u;
clf
subplot(1,2,1)
leg = cell(0);
hold all
plot(-abs(s).*sign(angle(s)),(real(Pol_high_alt(s)-Phi(s))./real(Phi(s))).^2);
leg(end+1)={sprintf('St=inf')};
plot(-abs(s).*sign(angle(s)),(real(Phi_Maa_co(s)-Phi(s))./real(Phi(s))).^2);
leg(end+1)={sprintf('Maa')};
plot(-abs(s).*sign(angle(s)),(real(Pol_7(s)-Phi(s))./real(Phi(s))).^2);
leg(end+1)={sprintf('St=7')};
axis([25,max(abs(s)),0,0.5*1e-2])
xlabel('Stokes number');
ylabel('Relative error on resistance');
legend(leg);
subplot(1,2,2)
u_int = [linspace(10,20^2,20)];
u = sqrt(-1i*[u_int]); % Stokes number
s = u;
leg = cell(0);
hold all
plot(-abs(s).*sign(angle(s)),(imag(Pol_high_alt(s)-Phi(s))./imag(Phi(s))).^2);
leg(end+1)={sprintf('St=inf')};
plot(-abs(s).*sign(angle(s)),(imag(Phi_Maa_co(s)-Phi(s))./imag(Phi(s))).^2);
leg(end+1)={sprintf('Maa')};
plot(-abs(s).*sign(angle(s)),(imag(Pol_7(s)-Phi(s))./imag(Phi(s))).^2);
leg(end+1)={sprintf('St=7')};
axis([10,max(abs(s)),0,5*1e-5])
xlabel('Stokes number');
ylabel('Relative error on reactance');
legend(leg);

%% Plot of the Phi function of the Maa model
    % Test
Phi_Maa_2 = @(s)((8*(1+(1/32)*(-1i*sign(imag(s)).*(s.^2))).^(1/2)-s.^2.*(1+(3^2+(1/2)*(-1i*sign(imag(s)).*(s.^2))).^(-1/2))));

u_int = linspace(1e-3,3^2,2e1);
u = sqrt(-1i*[-fliplr(u_int),u_int]); % Stokes number
s = u;
figure
subplot(1,2,1)
leg=cell(0);
hold all
plot(-abs(s).*sign(angle(s)),(real(Phi_Maa_or(s))));
leg(end+1)={sprintf('Original Maa')};
plot(-abs(s).*sign(angle(s)),(real(Phi_Maa_co(s))));
leg(end+1)={sprintf('Corrected Maa')};
plot(-abs(s).*sign(angle(s)),(real(Phi_Maa_2(s))));
leg(end+1)={sprintf('Other Maa correction')};
legend(leg);
xlabel('Stokes number');
ylabel('Real (rad)');
subplot(1,2,2)
leg=cell(0);
hold all
plot(-abs(s).*sign(angle(s)),(imag(Phi_Maa_or(s))));
leg(end+1)={sprintf('Original Maa')};
plot(-abs(s).*sign(angle(s)),(imag(Phi_Maa_co(s))));
leg(end+1)={sprintf('Corrected Maa')};
plot(-abs(s).*sign(angle(s)),(imag(Phi_Maa_2(s))));
leg(end+1)={sprintf('Other Maa correction')};
legend(leg);
xlabel('Stokes number');
ylabel('Real (rad)');

%% Aproximation of J1/J0
% The behaviour at infinity is not right (approx from z=0)
St = linspace(0,1e1,1e2); % Stokes number
s = St*V;
Y1 = 1./(2*1./s-(1./(2*2./s-1./(2*3./s-1./(2*4./s-1./(2*5./s-1./(2*6./s-1./(2*7./s)))))))); % Continued fraction approx
Y2 = cos(s-pi/2-pi/4)./cos(s-pi/4);

ber0 = 1 - (1/64)*St.^4;
bei0 = (1/4)*St.^2 -(1/2304)*St.^6;
ber1 = (cos(3*pi/4)/(2*gamma(2)))*St + (cos(5*pi/4)/(8*gamma(3)))*St.^3;
bei1 = (sin(3*pi/4)/(2*gamma(2)))*St + (cos(5*pi/4)/(8*gamma(3)))*St.^3;
Y3 = -(ber0+1i*bei0)./(ber1+1i*bei1);
figure
subplot(1,2,1)
leg=cell(0);
hold all
plot(abs(s),real(besselj(1,s)./besselj(0,s)))
leg(end+1)={'J_1/J_0'};
plot(abs(s),real(Y1),'--');
leg(end+1)={'Cont. Frac. Approx. (7 terms)'};
plot(abs(s),real(Y2),'--');
leg(end+1)={'Asymp. Approx.'};
plot(abs(s),real(Y3),'--');
leg(end+1)={'With Kelvin Fun.'};
xlabel('u (Stokes Number)');
ylabel('Real part');
legend(leg);
subplot(1,2,2)
leg=cell(0);
hold all
plot(abs(s),imag(besselj(1,s)./besselj(0,s)));
leg(end+1)={'J_1/J_0'};
plot(abs(s),imag(Y1),'--');
leg(end+1)={'Cont. Frac. Approx. (7 terms)'};
plot(abs(s),imag(Y2),'--');
leg(end+1)={'Asymp. Approx.'};
plot(abs(s),imag(Y3),'--');
leg(end+1)={'With Kelvin Fun.'};
xlabel('u (Stokes Number)');
ylabel('Imag. part');
legend(leg);

%% Ratio of resonant frequency for the Helmholtz and quarter-wave resonators
% [SELF-SUFFICIENT]
rl=linspace(0,0.10,10); % ratio neck length / cavity length : l/lcH (rad)
rd = [2,4,8];
figure
leg=cell(0);
hold all
for i=1:length(rd)
    plot(100*rl,(1+rl)./sqrt(1+4*rd(i)*rl));
    leg(end+1)={sprintf('ratiod %d',rd(i))};
end
xlabel('Ratio neck length / cavity length (%)');
ylabel('Ratio resonant frequency (rad)');
legend(leg);