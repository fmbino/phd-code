%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Laplace transforms of cavity impedance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Grid
dpi = 600;
ar = 1.5/2; % aspect ratio (x/y)
height= 10*0.393701; % height in cm-> inch
width = ar*height;
x = linspace(-1,1,width*dpi);
y = linspace(-1,1,height*dpi);
[Sr,Si] = meshgrid(x,y);
zmax = 2;
S = (Sr+1i*Si);
%% Cavity
lc = 38.1e-3; % Cavity length (m)
dc = 9.5e-3; % Cavity diameter (m)

lc=c0*1;
dc=1;

fprintf('Cotan(w) undefined for <w> prop. to <%1.2e>\n',c0*pi/lc);
%% Transfer function (in s)
    % Lossless model (LEE)
%H = @(s)(-1i*cot(k0(-1i*s)*lc));
% Bruneau model
H = @(s)(-1i*cot(k_br(dc,-1i*s,Pr)*lc));
    % Tube with finite impedance Zc
%Zc = 2.5e0;
%H = @(s)(1i*(Zc*cos(-1i*s)+1i*sin(-1i*s))./(cos(-1i*s)+1i*Zc*sin(-1i*s)));
H = @(s)(sqrt(s./(1+s)));
H = @(s)(sqrt(1+1./(2*s))-1);

a = 5;
b = 3;
c = 6;
H = @(s)((s+a).*sqrt(1./(s.*(s+c))));
Zc = @(s)(sqrt((s+a)./(s)));
k = @(s)(sqrt((s+a)./(s)));
H = @(s)(-1i*Zc(s).*cot(k(s)));
H = @(s)(s.*(1-2./s*besselj(1,sqrt(-s))./besselj(0,sqrt(-s))).^(-1));
k = @(w)(+w.*sqrt((1+1i*w).*(2+1i*w)./(+1i*w)));
w1 = 1;
w2 = 3;
w3 = 0.5;
Zp = @(s)(s.*(1-lambda(sqrt(s))).^(-1));
H = @(s)(sqrt((s+w1).*(s+w2)./(s.*(s+w3))));
lambda = @(s)((2./s).*(besseli(1,s)./besseli(0,s)));
Zp = @(s)(s.*(1-lambda(sqrt(s))).^(-1));
Psi = @(s)((1-lambda(s)).^(-1));
D = @(s)(1./sqrt(((s+5).*s)));
D = @(s)(sqrt(s.*(s+1)));
H = @(s)(exp(-D(s)));
a0 = 4;
a1 = 4;
aa = 5*a1;
jk = @(s)(a0 + a1*s+aa*sqrt(s));
H = @(s)(exp(-2*(a0+aa*sqrt(s)))./(1-exp(-2*jk(s))));
sn = computePolesFPCavity([a0,aa,a1],1,0.5,10);
%H = @(s)(coth(jk(s))-1);
%r = 1;
%eeps = 1;
%H = @(s)(exp(-eeps*sqrt(s))./(1-r*exp(-2*(s+eeps*sqrt(s)))));
%H = @(s)(coth(s));
%H = @(s)(1./(s+eeps*sqrt(s)-log(sqrt(r))));

H = @(s)(1./sqrt(s));
H = @(s)((1-sqrt(s))./(sqrt(s)+1));
    % Generic model
P = @(s)(1);
Q = @(s)(1+sqrt(s)+s);
H = @(s)(P(s)./(2+P(s)-P(s).*exp(-2*Q(s))));
H = @(s)(exp(-sqrt(s)));
% Ozyoruk
r1 = 1e1;
r2 = 1e-4;
r3 = 5;
H = @(s)(r1 + (r2-r1)./(1+r3*s));

H = @(s)(sqrt(s));
H = @(s)(bessely(1,s));
%% Surface modulus & argument
%--
% Sb = logical(imag(S(:,1))<0);
% S = S(Sb,:);
% Sr = Sr(Sb,:);
% Si = Si(Sb,:);
%--

clf
zmax=2;
hold all
subplot(1,2,1)
Z = abs(H(S));
surf(Sr,Si,Z,'edgecolor','none');
colormap(gca,'default');
colorbar
caxis([max(-zmax,min(Z(:))) min(zmax,max(Z(:)))])
axis([min(x),max(x),min(y),max(y),-zmax,zmax])
xlabel('Real(w)')
ylabel('Imag(w)')
title('Abs(H(w))');
view([0,90])

subplot(1,2,2)
%clf
Z = angle(H(S));
surf(Sr,Si,Z,'edgecolor','none');
colormap(gca,'hsv'); % Cyclic/Periodic colormap
set(gca,'DataAspectRatio',[1,1,1])
colorbar
caxis([-pi pi]);
axis([min(x),max(x),min(y),max(y),-pi,pi])
xlabel('Real(w)')
ylabel('Imag(w)')
title('Arg(H(w))');
set(gca,'xtick',[])
set(gca,'xticklabel',[])
set(gca,'ytick',[])
set(gca,'yticklabel',[])
view([0,90])

%% Surface (in s)
clf
subplot(1,2,1)
Z = real(H(S));
surf(Sr,Si,Z,'edgecolor','none');
colorbar
caxis([max(-zmax,min(Z(:))) min(zmax,max(Z(:)))])
axis([min(x),max(x),min(y),max(y),-zmax,zmax])
xlabel('Real(s)')
ylabel('Imag(s)')
title('Real(H(s))');
view([0,90])
subplot(1,2,2)
Z = imag(H(S));
surf(Sr,Si,Z,'edgecolor','none');
colorbar
caxis([max(-zmax,min(Z(:))) min(zmax,max(Z(:)))]);
axis([min(x),max(x),min(y),max(y),-zmax,zmax])
xlabel('Real(s)')
ylabel('Imag(s)')
title('Imag(H(s))');
view([0,90])
%% Transfer function (in w)

    % Lossless model (LEE)
%H = @(s)(-1i*cot(k0(-1i*s)*lc));
% Bruneau model
H = @(s)(-1i*cot(k_br(dc,s,Pr)*lc));
H = @(s)(-1i*cot(1e0*s+1*sqrt(1i*s)));
H = @(s)((1+exp(-2*1i*k_high_i(dc,s,Pr)*lc))./(1-exp(-2*1i*k_high_i(dc,s,Pr)*lc)));
%H = @(s)(1./sqrt(1i*s));
%H = @(s)(1./s);
    % Tube with finite impedance Zc
%Zc = 2.5e0;
%H = @(s)(1i*(Zc*cos(-1i*s)+1i*sin(-1i*s))./(cos(-1i*s)+1i*Zc*sin(-1i*s)));
%H = @(s)(log(sin(s)));
H = @(s)(log(sin(s+sqrt(s))));

%% Surface (in w)
zmax=2;
clf
subplot(1,2,1)
Z = real(H(S));
surf(Sr,Si,Z,'edgecolor','none');
colorbar
caxis([max(-zmax,min(Z(:))) min(zmax,max(Z(:)))])
axis([min(x),max(x),min(y),max(y),-zmax,zmax])
xlabel('Real(w)')
ylabel('Imag(w)')
title('Real(H(w))');
view([0,90])
subplot(1,2,2)
Z = imag(H(S));
surf(Sr,Si,Z,'edgecolor','none');
colorbar
caxis([max(-zmax,min(Z(:))) min(zmax,max(Z(:)))]);
axis([min(x),max(x),min(y),max(y),-zmax,zmax])
xlabel('Real(w)')
ylabel('Imag(w)')
title('Imag(H(w))');
view([0,90])


%% y-cut plot
y_cut = [0,1e-1,1]; % cut plot
clf
subplot(1,2,1)
leg = cell(0);
for i=1:length(y_cut)
    hold all
    z = real(cot(x+1i*y_cut(i)));
    plot(x,z);
    leg{end+1}=sprintf('y=%1.1e (%1.1e))',y_cut(i),max(abs(z)));
    axis([min(x),max(x),-zmax,zmax])
    xlabel('x')
    legend(leg)
    title('Real part');    
end
%set(gca,'DataAspectRatio',[1,1,1])
plot(zeros(1,2),[10,-10],'--');
plot(pi*ones(1,2),[10,-10],'--');
subplot(1,2,2)
leg = cell(0);
for i=1:length(y_cut)
    hold all
    z = imag(cot(x+1i*y_cut(i)));
    plot(x,z);
    leg{end+1}=sprintf('y=%1.1e (%1.1e)',y_cut(i),min(z));
    axis([min(x),max(x),-zmax,zmax])
    xlabel('x')
    legend(leg)
    title('Imag part');    
end
%set(gca,'DataAspectRatio',[1,1,1])
plot(zeros(1,2),[10,-10],'--');
plot(pi*ones(1,2),[10,-10],'--');

%% x-cut plot
x_cut = [0,1e-1,1]; % 
clf
subplot(1,2,1)
leg = cell(0);
for i=1:length(x_cut)
    hold all
    plot(y,real(cot(x_cut(i)+1i*y)));
    leg{end+1}=sprintf('x=%1.1e',x_cut(i));
    xlabel('y')
    legend(leg)
    title('Real part');    
end
%set(gca,'DataAspectRatio',[1,1,1])
%plot(zeros(1,2),[10,-10],'--');
%plot(pi*ones(1,2),[10,-10],'--');
subplot(1,2,2)
leg = cell(0);
for i=1:length(x_cut)
    hold all
    plot(y,imag(cot(x_cut(i)+1i*y)));
    leg{end+1}=sprintf('x=%1.1e',x_cut(i));
    xlabel('y')
    legend(leg)
    title('Imag part');    
end
%set(gca,'DataAspectRatio',[1,1,1])
%plot(zeros(1,2),[10,-10],'--');
%plot(pi*ones(1,2),[10,-10],'--');


%% Approximation of the cavity impedance
lc=2e2;
dc=1e0;
C=4e-1;
H = @(w)(-1i*cot(k_br(dc,w,Pr)*lc));
H = @(w)(-1i*cot(w+C*sqrt(-1i*w))); % High Stokes approx
G = @(w)(1-2*(-1i*w).^(-1/2).*exp(1i*(3*(w-1.20*pi/2))));
lambda=1*exp(1i*(pi+pi/4));
F = @(w)(1+10*exp(lambda*sqrt(w)).*(1+erf_(lambda*sqrt(w))));
F = @(w)((1-exp(-2*1i*w)).^(-1));
F = @(w)(exp(2*1i*w).*(1-exp(-2*1i*w)).^(-1));
a1=1; a2=1e5;
K = @(s)(1+5*exp(-s).*(exp(-sqrt(s)))./(1-a1*exp(-2*(s+a2*sqrt(s)))));
C=1e-1;
l=1;
w = linspace(1e-2,1e2,1e3);
clf
subplot(1,2,1)
hold all
plot(w,real(H(w)));
plot(w,real(K(1i*w)));
subplot(1,2,2)
hold all
plot(w,imag(H(w)));
plot(w,imag(K(1i*w)));

f = @(z)(-1i*cot(z+C*sqrt(-1i*z)));
%clf
%plot(t,abs(exp(sqrt(2)*(1)*l*sqrt(t)).*(f(t)-1)));