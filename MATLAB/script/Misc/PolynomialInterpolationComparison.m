%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Comparison of several projection method
%
% (1) Lagrange polynomial
% (2) Legendre with exact projection
% (3) Legendre with a 'nodal' projection
% Over [-1,1]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Function to interpolate
N = 5; % number of points
x_p = linspace(-1,1,100);
f = @(x)(exp(-(3+29/500)*x.^2))

Intf = integral(f,-1,1); % 'Exact' integral value
% To compute integral over [-1,1]
[x_gq,w_gq] = JacobiGQ(0,0,2*N);

%% 1 - Lagrange polynomial
% Nodes
x_i = JacobiGL(0,0,N-1);
% Lebesgue constant
lebesgue(x_i);
% Lagrange Poly
f1 = barylag([x_i,f(x_i)],x_p');

Intf1 = barylag([x_i,f(x_i)],x_gq)'*w_gq;

%% 2 - Legendre with exact projection

fn = zeros(N,1);
F = f(x_gq);
for n=1:N
    fn(n)=(w_gq')*(F.*JacobiP(x_gq,0,0,n-1));
end

% Compute values
Leg_xp = zeros(length(x_p),N);
for j=1:N
   Leg_xp(:,j) = JacobiP(x_p,0,0,j-1); 
end
f2 =  Leg_xp*fn;

Leg_xgq = zeros(length(x_gq),N);
for j=1:N
   Leg_xgq(:,j) = JacobiP(x_gq,0,0,j-1); 
end
Intf2 = (Leg_xgq*fn)'*w_gq;
%% 3 - Legendre with a 'nodal' projection
% Unicity of nodal projection -> identical to 1
% Vandermonde matrix
V = zeros(N,N);
for j=1:N
   V(:,j)=JacobiP(x_i,0,0,j-1);
end
% Compute modal coeff
fn = V\f(x_i);
% Compute values
f3 = Leg_xp*fn;
Intf3 = (Leg_xgq*fn)'*w_gq;
%% 4 - Comparative plot
figure
hold all
plot(x_p,f(x_p));
plot(x_p,f1);
plot(x_p,f2);
plot(x_p,f3);
legend(sprintf('f, I=%1.2e',Intf),sprintf('Nodal (Lagrange), I=%1.4e',Intf1),sprintf('Modal (Legendre), I=%1.4e',Intf2),sprintf('Nodal (Legendre), I=%1.4e',Intf3));
xlabel('x');
ylabel('y');

%%
Npoint = @(N)(ceil((3*N+1)/2));
