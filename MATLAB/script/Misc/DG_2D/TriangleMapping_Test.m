%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Test triangle mapping from Hesthaven Chap.6
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Triangle definition
x = [-1,4,-1];
y = [-1,-2,1];
T = [x(:),y(:)];
%% Compute metric
    % Jacobian
J = (x(2)-x(1))*(y(3)-y(1))-(x(3)-x(1))*(y(2)-y(1));
J = J/4;
    % Inverse of the Jacobian
rx = (y(3)-y(1))/(2*J);
ry = -(x(3)-x(1))/(2*J);
sx = -(y(2)-y(1))/(2*J);
sy = (x(2)-x(1))/(2*J);
    % Unit Normal vector
n1 = -[sx,sy]; n1 = n1/norm(n1,2);
n2 = [rx+sx,ry+sy]; n2 = n2/norm(n2,2);
n3 = -[rx,ry]; n3 = n3/norm(n3,2);
%% Plot

clf
hold all
set(gca,'DataAspectRatio',[1,1,1])
scatter(x,y);
plot([x(1),x(2)],[y(1),y(2)],'r');
plot([x(2),x(3)],[y(2),y(3)],'b');
plot([x(3),x(1)],[y(3),y(1)],'g');
title(sprintf('Triangle. J=%1.2g',J));
N = [n1;n2;n3];
quiver([x(1)+x(2)]/2,[y(1)+y(2)]/2,N(1,1),N(1,2),'r','MaxHeadSize',0.4);
quiver([x(2)+x(3)]/2,[y(2)+y(3)]/2,N(2,1),N(2,2),'b','MaxHeadSize',0.4);
quiver([x(1)+x(3)]/2,[y(1)+y(3)]/2,N(3,1),N(3,2),'g','MaxHeadSize',0.4);