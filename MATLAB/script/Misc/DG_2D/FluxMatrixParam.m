%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Plot related to the DG2D method

%% Choosing the parameter alpha for the ghost state
% a0: resistance
% alpha: parameter, between -1 and 1
    % Matrix
M = @(a0,alpha,n)[-alpha*eye(1),(1+alpha)/a0*n(:);a0*(1-alpha)*n(:)',alpha];
    % Condition number: ||M||_inf * ||M^-1||_inf (thanks to maple)
condNumber = @(alpha,a0)(max(abs(alpha)+abs((1+alpha)./a0), abs(a0.*(-1+alpha))+abs(alpha)).*max(abs(alpha)+abs((1+alpha)./a0),abs(a0.*(1-alpha))+abs(alpha)));
condNumber = @(alpha,a0) max(a0.*abs(-1+alpha)+abs(alpha), abs(alpha)+abs(1+alpha)./a0).^2;
    % Plot
alpha = linspace(-1,1,1e2);
a0 = linspace(1e-4,20,1e2);
Y = condNumber(repmat(alpha(:),[1,length(a0)]),repmat(a0(:)',[length(alpha),1]));
[~,I] = min(Y,[],1);
alpha_min = alpha(I);
clf
hold all
plot(a0,(a0-1)./(a0+1),'-');
plot(a0,alpha_min,'-');
xlabel('a0 (resistance)');
ylabel('alpha');
legend('beta_{0}','alpha_{min}');
title(sprintf('Best value for alpha'));