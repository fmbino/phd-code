%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Mesh connectivity
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load a .gmsh file, and build various connectivity tables
%   - Element to Element (EToE, EToF)
%   - Face to Element (FToE,FToF)
% Remark: all of this is now implemented in Mesh_2D_Triangle.
    % local face numbering
vn = [[1,2];[2,3];[1,3]];
%% Load content of .gmsh file
Nodes = h5read('/d/fmontegh/mesh_Nodes.h5','/dset');
Faces = h5read('/d/fmontegh/mesh_Faces.h5','/dset');
Triangles = h5read('/d/fmontegh/mesh_Triangles.h5','/dset');

Nk = size(Triangles,1);
fprintf('Mesh: %d triangles, %d boundary faces.\n',Nk, size(Faces,1));
%% Build element connectivity table
EToE=[]; % E(i,j)=k
EToF=[]; % E(i,j)=l

for i=1:size(Triangles,1) % for each triangle
    for j=1:3 % for each local face
        face = Triangles(i,1+vn(j,:));
            % Which element has the same two nodes ?
        k = meshTriangle_findTriangle(face,Triangles(:,2:end));
        if (max(k)==0) || (length(k)>2) % no match
            error('Incorrect mesh: face (el. %d,face %d) belongs to 0 or more than two triangles.',i,j);
        elseif length(k)==1 % boundary face 
            EToE(i,j)=i; EToF(i,j)=0; % Hestaven convention
            EToE(i,j)=0; EToF(i,j)=0; % Artimon convention
        else % interior face
            k = k(k~=i);
                % Which local face?
            l = meshTriangle_findLocalFace(face,Triangles(k,2:end));
            EToE(i,j)=k; EToF(i,j)=l;
        end
    end
end
fprintf('Element connectivity: %d entries.\n',size(EToE,1));
    % Compare with Hesthaven
%[EToE2,EToF2]=Connect2D(double(Triangles(:,2:end)));
    % Compare with the output of Fortran routine 
EToE3 = double(h5read('/d/fmontegh/mesh_EToE.h5','/dset'));
EToF3 = double(h5read('/d/fmontegh/mesh_EToF.h5','/dset'));

fprintf('[Element connectivity (EToE): [%s] & [%s]\n',sprintf('%d,',min(EToE-EToE3)),sprintf('%d,',max(EToE-EToE3)));
fprintf('[Element connectivity (EToF): [%s] & [%s]\n',sprintf('%d,',min(EToF-EToF3)),sprintf('%d,',max(EToF-EToF3)));
%% Build face connectivity table
FToE=[]; % F(i,j)=k
FToF=[]; % F(i,j)=l
for i=1:size(Faces,1) % for each boundary face
    face = Faces(i,2:3);
        % which element has the same two nodes?
    k = meshTriangle_findTriangle(face,Triangles(:,2:end));
    if (max(k)==0) || (length(k)>1) % no match
        error('Incorrect mesh: boundary face (%d) belongs to 0 or more than one triangle.',i);
    else
            % Which local face?
        l = meshTriangle_findLocalFace(face,Triangles(k,2:end));
            % Boundary face belongs to element k, local face l
        FToE(i)=k; FToF(i)=l;
    end
end
FToE=FToE(:); FToF=FToF(:);
fprintf('Face connectivity: %d entries.\n',length(FToE));

    % Compare with the output of Fortran routine 
FToE3 = double(h5read('/d/fmontegh/mesh_FToE.h5','/dset'));
FToF3 = double(h5read('/d/fmontegh/mesh_FToF.h5','/dset'));

fprintf('[Element connectivity (FToE): [%s] & [%s]\n',sprintf('%d,',min(FToE-FToE3)),sprintf('%d,',max(FToE-FToE3)));
fprintf('[Element connectivity (FToF): [%s] & [%s]\n',sprintf('%d,',min(FToF-FToF3)),sprintf('%d,',max(FToE-FToE3)));

%% Metric
J = zeros(Nk,1); % Jacobian (rad)
rx = zeros(Nk,1); % dr/dx (rad)
ry = zeros(Nk,1); % dr/dy (rad)
sx = zeros(Nk,1); % ds/dx (rad)
sy = zeros(Nk,1); % ds/dy (rad)
    % unitary outward normal
    % n(k,:,l)=[nx,ny] element k, local face l in [1,3]
n = zeros(Nk,2,3); 
edL = zeros(Nk,3); % edge length
for k=1:Nk % for each triangle
    vertices = Triangles(k,2:end);
    x = Nodes(vertices,1); y = Nodes(vertices,2);
    J(k) = (1/4)*((x(2)-x(1))*(y(3)-y(1))-(x(3)-x(1))*(y(2)-y(1)));
    rx(k) = (y(3)-y(1))/(2*J(k));
    ry(k) = -(x(3)-x(1))/(2*J(k));
    sx(k) = -(y(2)-y(1))/(2*J(k));
    sy(k) = (x(2)-x(1))/(2*J(k));
    n(k,:,1) = -[sx(k),sy(k)]; n(k,:,1) = n(k,:,1)/norm(n(k,:,1),2);
    n(k,:,2) = [rx(k)+sx(k),ry(k)+sy(k)]; n(k,:,2) = n(k,:,2)/norm(n(k,:,2),2);
    n(k,:,3) = -[rx(k),ry(k)]; n(k,:,3) = n(k,:,3)/norm(n(k,:,3),2);
    edL(k,1) = norm( [x(1);y(1)] - [x(2);y(2)],2);
    edL(k,2) = norm( [x(2);y(2)] - [x(3);y(3)],2);
    edL(k,3) = norm( [x(3);y(3)] - [x(1);y(1)],2);
end

%% Mesh plot
N = length(Nodes(:,1));
trisurf(Triangles(:,2:end),Nodes(:,1),Nodes(:,2),Nodes(:,2).^3);