%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Plot basis functions for triangles from Hesthaven Chap.6
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Plot modal basis Psi_m
N=4; % Max. polynomial order
m = 10; % Polynomial to plot
%--
r = linspace(-1,1,4e2);
s = linspace(-1,1,4e2);
[R,S] = meshgrid(r,s);
V = Vandermonde2D(N,R(:),S(:));
Vres = reshape(V(:,m),size(R));
mask = ((R>=(-1)).*(S>=(-1)).*((R+S)<=0));
mask(mask==0) = NaN;
surf(R,S,Vres.*mask,'EdgeColor','none');
xlabel('r');
ylabel('s');
title(sprintf('Modal basis: psi_%d (order N=%d, Np=%d)',m,N,(N+1)*(N+2)/2));
view([0,90])
%% Plot nodal basis l_m
N=6; % Max. polynomial order
m = 10; % Polynomial to plot
% --
r = linspace(-1,1,4e2);
s = linspace(-1,1,4e2);
[R,S] = meshgrid(r,s);
V = Vandermonde2D(N,R(:),S(:));
[r_node,s_node] = Nodes2D(N);
[r_node,s_node] = xytors(r_node,s_node);
V_node = Vandermonde2D(N,r_node,s_node);
VToL = transpose(inv(V_node));
m = min(m,(N+1)*(N+2)/2);
VToL = VToL(m,:);
L = V*VToL(:);
L = reshape(L,size(R));
mask = ((R>=(-1)).*(S>=(-1)).*((R+S)<=0));
mask(mask==0) = NaN;
clf
surf(R,S,L.*mask,'EdgeColor','none');
xlabel('r');
ylabel('s');
title(sprintf('Nodal basis: l_%d (order N=%d, Np=%d)',m,N,(N+1)*(N+2)/2));
view([0,90])
hold all
scatter(r_node,s_node,'ro','LineWidth',4)