%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Optimal diffusive representation
%--------------------------------------------------------------------------
% This article follows the optimization method described in [Helie06].
% Given a transfert function H(s) (Laplace Transform)
%   - Compute a diffusive representation through various optim procedure
%   - Plot poles (xi_k) and weight (mu_k)
%   - Plot Bode diagrams
% Rmk:
%   - Restricted to real mu_k (i.e. diffusive representation of the 1st
%   kind)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Test cases
    % Fractional integrator, order beta (Case n°2 from [Helie06])
    % -> Validated
beta=0.5;
H = @(s)(s.^(-beta));
mu = @(xi)(xi.^(-beta).*sin(0.5*pi)/pi);
%%
    % Fractional system of commensurate order (Case n°4 from [Helie06])
    % Transfert function Validated
    % mu to be computed analytically
H2P = @(x)(x.^4 + 0.1*x.^3+x+0.1);
mu2e = @(xi,a,ln)((sin(a*pi)/pi)*((xi.^a)./(xi.^(2*a)-2*ln*cos(a*pi)*(xi.^a)+ln^2)));
mu2 = @(xi)(-0.3704*mu2e(xi,0.5,-1)+1.0010*mu2e(xi,0.5,-0.1)+(-0.3153-0.0260*1i)*mu2e(xi,0.5,0.5+0.8660*1i)+((-0.3153+0.0260*1i))*mu2e(xi,0.5,0.5-0.8660*1i));
H2 = @(s)(1./H2P(s.^(0.5)));
    % Exp case (Case n°6 from [Helie06])
    % -> Validated
H3 = @(s)(exp(-(s.^0.5))./(s.^0.5)); % [Helie06], case 6
mu3 = @(xi)(cos(xi.^(0.5))./(pi*(xi.^0.5)));
    % Oustaloup
    % Transfert function validated
H4 = @(s)((1/100) * ((1+s/0.01)./(1+s/100)).^(0.5));

%% Bode plot
wmin=(1e-4);
wmax=(1e4);
a=figure;
plotBodeDiagram(a,[wmin,wmax],H2,'1/2-th order',200);
%plotBodeDiagram(a,[wmin,wmax],H2,'H4 Helie',200);
%plotBodeDiagram(a,[wmin,wmax],H3,'H6 Helie',200);
clear wmin wmax
%% Plot mu & pole placement
% Input :
%   xik poles (if any)
%   mu  weight (if any)

    % -- Inputs
mu_an = @(xi)(mu(xi));
xi = logspace(log10(1e-4),log10(1e4),10^4);
    % --
figure
subplot(1,2,1)
semilogx(-xi,real(mu_an(xi)),-xi,imag(mu_an(xi)),-xi,zeros(size(xi)));
xlabel('-xi (rad/s)');
ylabel('mu');
legend('Real part','Imag part','mu = 0');
hold all
grid on
if (exist('xik','var') && exist('muk','var'))
    for i=1:length(xik)
        if mu_an(i)>0
            semilogx(-xik(i),real(mu_an(xik(i))),'go','MarkerSize',10,'LineWidth',2);
        else
            semilogx(-xik(i),real(mu_an(xik(i))),'ro','MarkerSize',10,'LineWidth',2);
        end
    end
    hc=get(legend(gca),'String'); % get legend from current axes.
    legend(hc,sprintf('%d poles',length(xik)));
end
subplot(1,2,2)
loglog(-xi,abs(mu_an(xi)));
xlabel('-xi (rad/s)');
ylabel('|mu|');
legend('|mu|');
hold all
grid on
if (exist('xik','var') && exist('muk','var'))
    for i=1:length(xik)
        if mu_an(i)>0
            loglog(-xik(i),abs(mu_an(xik(i))),'go','MarkerSize',10,'LineWidth',2);
        else
            loglog(-xik(i),abs(mu_an(xik(i))),'ro','MarkerSize',10,'LineWidth',2);
        end
    end
    hc=get(legend(gca),'String'); % get legend from current axes.
    legend(hc,sprintf('%d poles',length(xik)));
end
clear xi mup
%% Optimization setup
    % -- Inputs
N_xi = 2;
xi_min = (1e-4);
xi_max = (1e4);
N_o = 10^3; % number of frequency bands
%w = @(om)(om.^2);
w = @(om)(om./om);
%w = @(om)(1./om); % frequency log scale
%w = @(om)(1./(abs(H(om*1i)).^2));
    % xi distribution
xik = logspace(log10(xi_min),log10(xi_max),N_xi);
w = @(om)(om./om);
%w = @(om)(1./om); % frequency log scale
%w = @(om)(1./(abs(H(om*1i)).^2));
omegan = logspace(log10(xi_min),log10(xi_max),N_o+1); % at least two elements
%% I - Standalone pinv optim
sn_optim = [];
[muk,rn_optim,sn_optim] = ComputeDiscreteDiffusiveRep_Optim_Std(xik,sn_optim,omegan,w,H);
%% I - Optimization of mu @ xi
% The poles xik are chosen, not optimized.
    % Building of the diagonal ponderation matrix W
W = computeWeightVector(w,omegan);
W = sparse(1:length(W),1:length(W),W); % sparse diagonal matrix
    % 1st order filters
M = get1stOrderUnitaryFilter(omegan(1:(end-1))*1i,xik);
    % Exact transfert function
h = transpose(H(omegan(1:(end-1))*1i));
    % Definition of the cost function J(mu)
C = W*M; C = [real(C);imag(C)];
d = W*h; d = [real(d); imag(d)];
Jmu = @(mu)(norm(C*mu-d)^2);
%% I - Perform the optimization - Unconstrained optimization
option = optimset('Display','final','TolFun',1e-4,'TolX',1e-4);
muk=fminsearch(Jmu,ones(N_xi,1),option);
clear option
%% I - Perform the optimization - Unconstrained optimization
muk = lsqnonlin(@(mu)(C*mu-d),ones(N_xi,1),zeros(N_xi,1));
%% I - Perform the optimization - Constrained optimization
muk = lsqlin(C,d,-eye(N_xi),zeros(N_xi,1));
%% I - Performn optimization - Unconstrained optimization pinv
% Enforce mu reality (as in [Helie06])
% mu>0 must be checked though (unconstrained optimization)
muk = ((C'*C))\(C'*d);
%% II - Perform xi optimization
% Input: xi_min, xi_max
% Both xi and mu are optimized.
% Rmk : xi is a (N_xi-2) column vector here

    % Initial xi vector.
xio=xik(2:(end-1))'; % typically, logspace distribution
Mf = @(xi)(get1stOrderUnitaryFilter(omegan(1:(end-1))*1i,[xi_min,transpose(xi),xi_max]));
Cf = @(xi)([real(W*Mf(xi));imag(W*Mf(xi))]);

mu_f = @(xi)(lsqlin(Cf(xi),d,-eye(N_xi),zeros(N_xi,1)));
Fxi = @(xi)(Cf(xi)*mu_f(xi)-d);

    % Optimal xi
xik_opt = lsqnonlin(Fxi,xio,zeros(N_xi-2,1));
muk_opt = mu_f(xik_opt);    
    % Prepare plot
xik = [xi_min,xik_opt',xi_max]; % line
muk=muk_opt; % vector
C = Cf(xik_opt);
Jmu = @(mu)(norm(C*mu-d)^2);

%% II - Perform xi optimization (log scale)
% Input: xi_min, xi_max
% Both xi and mu are optimized.
% Rmk : xi is a (N_xi-2) column vector here

    % Initial xi vector.
xik = logspace(log10(xi_min),log10(xi_max),N_xi);
xio=xik(2:(end-1))'; % typically, logspace distribution
xk=linspace(log10(xio(1)),log10(xio(end)),N_xi-2); % xi = 10^x
Mf = @(x)(get1stOrderUnitaryFilter(omegan(1:(end-1))*1i,[xi_min,10.^transpose(x),xi_max]));
Cf = @(x)([real(W*Mf(x));imag(W*Mf(x))]);

mu_f = @(x)(lsqlin(Cf(x),d,-eye(N_xi),zeros(N_xi,1)));
Fxi = @(x)(Cf(x)*mu_f(x)-d);

    % Optimal xi
xk_opt = lsqnonlin(@(x)(Fxi(x)),log10(xio),zeros(N_xi-2,1));
muk_opt = mu_f(xk_opt);    
    % Prepare plot
xik = [xi_min,(10.^xk_opt)',xi_max]; % line
muk=muk_opt; % vector
C = Cf(xk_opt);
Jmu = @(mu)(norm(C*mu-d)^2);

%% III - Plot results
% Input : xik (line), mu (vector)
% Cost function: Jmu as a function of mu

wmin=1e-5;
wmax=1e5;
Np = 8e1;
figure
a=gcf;clf(a);
plotBodeDiagram(a,[wmin,wmax],H,'Target',Np);
Hoptim = @(s,xik,mu)(((get1stOrderUnitaryFilter(s,xik))*mu).');
plotBodeDiagram(a,[wmin,wmax],@(s)(Hoptim(s,xik,muk)),sprintf('Optim - J=%1.2g',Jmu(muk)),Np);

subplot(1,2,1)
semilogx(xik,20*log10(abs(H(xik*1i))),'rx','MarkerSize',10,'LineWidth',2);
semilogx([xik(1),xik(1)],[-60,60],'r--','MarkerSize',10,'LineWidth',2);
semilogx([xik(end),xik(end)],[-60,60],'r--','MarkerSize',10,'LineWidth',2);
xlabel('omega');
hc=get(legend(gca),'String'); % get legend from current axes.
legend(hc,sprintf('%d poles',N_xi));
%hc=get(legend(gca),'String'); % get legend from current axes.
%legend(hc,sprintf('%d freq.',N_o));
subplot(1,2,2)
semilogx(xik,(180/pi)*unwrap(angle(H(xik*1i)),pi),'rx','MarkerSize',10,'LineWidth',2);
semilogx([xik(1),xik(1)],[-90,0],'r--','MarkerSize',10,'LineWidth',2);
semilogx([xik(end),xik(end)],[-90,0],'r--','MarkerSize',10,'LineWidth',2);
xlabel('omega');
hc=get(legend(gca),'String'); % get legend from current axes.
legend(hc,sprintf('%d poles',N_xi));
%hc=get(legend(gca),'String'); % get legend from current axes.
%legend(hc,sprintf('%d freq.',N_o));

%% III - Plot results (Generalized diffusive rep.)
% Input : xik (line), mu (vector)
% Cost function: Jmu as a function of mu

wmin=1e-5;
wmax=1e5;
Np = 3e1;
figure
a=gcf;clf(a);
plotBodeDiagram(a,[wmin,wmax],@(s)(s.*H(s)),'Target',Np);
Hoptim = @(s,xik,mu)(((get1stOrderUnitaryFilter(s,xik))*mu).');
plotBodeDiagram(a,[wmin,wmax],@(s)(s.*(Hoptim(s,xik,muk))),sprintf('Optim - J=%1.2g',Jmu(muk)),Np);

subplot(1,2,1)
semilogx(xik,20*log10(xik.*abs(H(xik*1i))),'rx','MarkerSize',10,'LineWidth',2);
semilogx([xik(1),xik(1)],[-60,60],'r--','MarkerSize',10,'LineWidth',2);
semilogx([xik(end),xik(end)],[-60,60],'r--','MarkerSize',10,'LineWidth',2);
%semilogx(omegan./(2*pi),20*log10(abs(H(omegan*1i))),'kx','MarkerSize',10,'LineWidth',2);
xlabel('omega');
hc=get(legend(gca),'String'); % get legend from current axes.
legend(hc,sprintf('%d poles',N_xi));
%hc=get(legend(gca),'String'); % get legend from current axes.
%legend(hc,sprintf('%d freq.',N_o));
subplot(1,2,2)
semilogx(xik,(180/pi)*(unwrap(angle(H(xik*1i)),pi))+90,'rx','MarkerSize',10,'LineWidth',2);
%semilogx(omegan./(2*pi),(180/pi)*angle(H(omegan*1i)),'kx','MarkerSize',10,'LineWidth',2);
xlabel('omega');
hc=get(legend(gca),'String'); % get legend from current axes.
legend(hc,sprintf('%d poles',N_xi));
%hc=get(legend(gca),'String'); % get legend from current axes.
%legend(hc,sprintf('%d freq.',N_o));
%% Store xi distribution

    % Case n°3 - Distribution used in [Helie06]
    % (use a uniform weigthing function)
xik = logspace(log10(xi_min),log10(1.5),8);
xik = [xik,8,35.82,85,148.5,244.3];