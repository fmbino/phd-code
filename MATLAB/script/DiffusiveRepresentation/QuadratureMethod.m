%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Quadrature strategy for diffusive representation
%%%%
% The purpose of this script is to compute a discrete diffusive
% representation by using interpolation in the xi-space.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% I  - Definition of the diffusive representation
beta = 0.5;
F_an = @(s)(s.^(-beta));
H_an = @(s,F)(s.*F);
mu_an = @(xi)(sin(beta*pi)./(pi*xi.^beta)); % xi>0
f = @(x,s)(mu_an(x)./(s+x));
%% II - Quadrature rules
    % Change of variable (-1,1) -> (0,1)
ki = @(t)((t+1)/2);
    % Change of variable (0,inf) -> (0,1) (from MATLAB integral)
    % Includes a regularization at t=0
psi =  @(t)((t./(1-t)).^2);
dpsi = @(t)(2*t./(1-t).^3);
    % Gauss-Legendre quadrature on (-1,1)
N_gl = 30;
h_gaussleg_1m1 = @(s)(GaussLegendreQuadrature(@(t)(0.5*f(psi(ki(t)),s).*dpsi(ki(t))),-1,1,N_gl));
%% Compute
w = linspace(0,1e4,25);
z_gaussleg_1m1 = zeros(1,length(w));
for i=1:length(w)
    z_gaussleg_1m1(i) = h_gaussleg_1m1(1i*w(i));
end
z_gaussleg_1m1 = H_an(1i*w,z_gaussleg_1m1);
%% Plot
gh=clf;
z_ex =  H_an(1i*w,F_an(1i*w));
plotComplex(gh,w,(z_gaussleg_1m1-z_ex)./z_ex,'r--',sprintf('G.L. N_{GL}=%d',N_gl));
subplot(1,2,1)
title('Approx. of sqrt(s). Relative error.');
xlabel('w (angular freq.)');
subplot(1,2,1)
title('Approx. of H_an(s). Relative error.');
xlabel('w (angular freq.)');
%xlim([0,1])