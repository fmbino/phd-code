%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Local and diffusive (=poles and cut) representation - Part II
%--------------------------------------------------------------------------
% Section II builds various discrete representation(s), all written as:
% Discrete poles & cut representation of F_an(s):
%                (oscillatory part: poles sn and residual rn)
%                (diffusive part: poles xik and weight muk)
%                  r_n        mu_k
% F_pc(s) =      -------  + --------- 
%                s - s_n     s + xi_k
% This approximation is described by four COLUMN vectors:
%                       s_n, r_n, xi_k, mu_k
% Remark: It can be better to compute the weight r_n by computing the associated
% complex residues; this is not done here. See the script 'ComputeOscDiffCT57.m'
%for an example.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Remark: s row or column -> F_pc(s) column
F_pc = @(s,sn,rn,xik,muk)((get1stOrderUnitaryFilter(s,[-sn(:);xik(:)]))*[rn(:);muk(:)]);

%% Compute poles numerically
    % -- Compute poles (pair of conjugate poles)
sn_optim = computeZerosNumerical(polesEq,[-2,0],[0,100],[1,50],'tol',1e-6);
        % force hermitian symmetry
sn_optim = sn_optim(imag(sn_optim)>=0);
sn_optim = [sn_optim(:);conj(sn_optim)];
        % FIX: remove 0 (branch point)
sn_optim = sn_optim(sn_optim~=0);
        % remove duplicate
sn_optim = removeDuplicate_Complex(sn_optim,1e-8);
        % sort pole by growing modulus
[~,I]=sort(abs(sn_optim));
sn_optim = sn_optim(I); clear I
%% Optimization on diffusive part only
% Oscillatory weights are computed using complex residues
% Optimization on diffusive part only
    % Compute oscillatory weights using complex residues
rn_optim = computeComplexResidue(F_an,sn_optim,'tol',1e-10);
    % Compute diffusive part by optimization
xi_min = 2*pi*(1e-2); xi_max = 2*pi*(1e2); Nxi=30;
xik_optim = logspace(log10(xi_min),log10(xi_max),Nxi);
muk_optim = ComputeDiscreteDiffusiveRep_Optim_Std(xik_optim,[],logspace(log10(xi_min),log10(xi_max),1e3),@(om)(om./om),@(s)F_an(s(:))-F_pc(s(:),sn_optim,rn_optim,[],[]),0);
%% Full optimisation
% Optimisation for both the diffusive and oscillatory parts
xi_min = 2*pi*(1e-2); xi_max = 2*pi*(1e2); Nxi=30;
xik_optim = logspace(log10(xi_min),log10(xi_max),Nxi);
[muk_optim,rn_optim,sn_optim] = ComputeDiscreteDiffusiveRep_Optim_Std(xik_optim,sn_optim,logspace(log10(xi_min),log10(xi_max),1e3),@(om)(om./om),F_an,0);
%clear xi_min xi_max Nxi Npoles N_o w omegan w_min w_max
%rn_optim_ana = computeRes(sn_optim); % same sorting between sn and rn
%printOptimizationSummary(xik_optim,muk_optim,sn_optim,rn_optim,rn_optim_ana);
F_optim = @(s)(F_pc(s,sn_optim,rn_optim,xik_optim,muk_optim));
%% Quadrature and optimisation
% Quadrature for diffusive part, optimisation for oscillatory part
% Output: xik_quad, muk_quad, rn_quad
    % -- Quadrature
Nxi = 20;
[xik_quad,muk_quad] = ComputeDiscreteDiffusiveRep_QuadratureGL(mu,Nxi,'CoVParam',2);
    % -- Optimisation for oscillatory part
[~,rn_quad,~] = ComputeDiscreteDiffusiveRep_Optim_Std([],sn_optim,logspace(log10(min(xik_quad)),log10(max(xik_quad)),1e3),@(om)(om./om),@(s)(F_an(s(:))-F_pc(s(:),[],[],xik_quad,muk_quad)),0);
%clear xi_min xi_max Nxi Npoles N_o w omegan w_min w_max
F_quad = @(s)(F_pc(s,sn_optim,rn_quad,xik_quad,muk_quad));