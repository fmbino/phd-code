%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Oscillatory and diffusive (=poles and cut) representation - Part I
%--------------------------------------------------------------------------
% The purpose of this script is to analyse a function F_an(s), to assess
% whether it admits an oscillatory-diffusive representation.
% This section computes diffusive weight mu(xi), poles and residues.
% Two functions are defined:
%       h_diff_fun: diffusive part
%       h_osc_fun: oscillatory part (partial, based on computed poles and
%       residues)
% A function is also avalaible to assess sufficient conditions to have a
% diffusive representation.
% Various plots are available.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Oscillatory-Diffusive representation: Check sufficient conditions
% The function 'identifyDiffusiveRep' go through all of the sufficient
% conditions. Beware that not all conditions can be checked numerically.
identifyDiffusiveRep(F_an,branchpt,0);
%% Build oscillatory-diffusive representation
    % compute poles
poles_x_search = [-10,1];
poles_y_search = [0,10];
poles_y_search = [0,10];
poles = computeZerosNumerical(polesEq,poles_x_search,poles_y_search,[1,ceil(max(poles_y_search)/(2))]);
        % force hermitian symmetry (can create duplicate)
I = imag(poles)>0;
poles = [poles(:);conj(poles(I))];
        % remove duplicate
poles = removeDuplicate_Complex(poles,1e-3);
        % remove 0 (branch point)
%poles = poles(abs(poles-branchpt)>1e-15);
        % sort pole by growing modulus
[~,I]=sort(abs(poles));
poles = poles(I); clear I
    % compute residues
residue = computeComplexResidue(F_an,poles,'tol',1e-5);
    % Diffusive weight
mu=@(xi)(((F_an(xi*exp(-1i*pi))-F_an(xi*exp(1i*pi)))/(2*1i*pi)));
    % Diffusive part
    % (integral can be wrong for negligible diffusive part, causing shift)
F_diff_fun = @(s)(integral(@(xi)(mu(xi)./(s+xi)),0,inf,'RelTol',1e-5,'AbsTol',1e-5));
    % (Partial) oscillatory part
%F_osc_fun = @(s)((get1stOrderUnitaryFilter(s,-poles(:)))*residue(:));
F_osc_fun = @(s)((get1stOrderUnitaryFilterSlow(s,-poles(:)))*residue(:));
%% Build oscillatory-diffusive representation (ANALYTICAL)
% This is an alternative to the section above. Its sole use is to compute
% the poles analytically instead of numerically. (But, of course, an
% analytical expression is not always available.)
% N = 1e3; % number of poles
%     % Poles
% if r>0
%     poles = log(sqrt(r))+1i*pi*(0:N);
%     poles = poles(:)/a;
% else
%     poles = log(sqrt(abs(r)))+1i*pi*(0:N)+1i*pi/2;
%     poles = poles(:)/a;    
% end
    % Undelayed function
%residue = 0.5*ones(size(poles));
    % Undelayed, but regularized function
% residue = 1./(2*poles);
% poles = [0;poles];
% residue = [1/(1-r);residue(:)];
    % Delayed function
% Cst=0;
% residue = exp(-poles(:))/(2*a);
    % Hermitian symmetry
% I = imag(poles(:))>0;
% poles = [poles(:);conj(poles(I))];
% residue = [residue(:);conj(residue(I))];
    % Diffusive part
% F_diff_fun = @(s)(zeros(size(s)));
% mu=@(xi)(zeros(size(xi)));
    % (Partial) oscillatory part
%Cst = 1/(1-r) + sum(1./(2*poles));
% F_osc_fun = @(s)((get1stOrderUnitaryFilter(s,-poles(:)))*residue(:));
%F_osc_fun = @(s)((get1stOrderUnitaryFilterSlow(s,-poles(:)))*residue(:));
    % Undelayed function, alternative representation
% Cst = 1/(1-r);
% residue = 0.5./poles;
%     % Hermitian symmetry
% I = imag(poles(:))>0;
% poles = [poles(:);conj(poles(I))];
% residue = [residue(:);conj(residue(I))];
% %F_osc_fun = @(s)(Cst+(get1stOrderUnitaryFilter(s,-poles(:),'type','high-pass'))*residue(:));
% F_osc_fun = @(s)((get1stOrderUnitaryFilter(s,-poles(:),'type','low-pass'))*residue(:));
% F_diff_fun = @(s)(zeros(size(s)));
% mu=@(xi)(zeros(size(xi)));
%% II - Plot transfer function
% Visually check the following conditions:
%   - F_an analytic in {Re(s)>0}
%   - cut included in (-inf,0)
    % -- user input
        % Surface plot
x = linspace(-25,1,8e2);
y = linspace(-25,25,8e2);
color_zmax=1e1; % saturation color for colorbar (modulus)
        % Fourier transform
omega = linspace(-1,1,1e3);
        % Diffusive weight
xi = linspace(0,1e1,5e1);
    % --
    % (1) Plot modulus
[Sr,Si] = meshgrid(x,y); S = (Sr+1i*Si);
figure(1)
clf
subplot(3,2,1)
% F_osc_surf = zeros(size(S));
% for j=1:size(S,2) 
%     F_osc_surf(:,j) = F_osc_fun(S(:,j));
% end
Z = abs(F_an(S));
surf(Sr,Si,Z,'edgecolor','none');
colormap(gca,'default');
colorbar
caxis([max(-color_zmax,min(Z(:))) min(color_zmax,max(Z(:)))])
axis([min(x),max(x),min(y),max(y),-color_zmax,color_zmax])
xlabel('Real(s)')
ylabel('Imag(s)')
title('|F(s)|');
view([0,90])
    % (2) Plot argument
subplot(3,2,2)
Z = angle(F_an(S)); 
surf(Sr,Si,Z,'edgecolor','none');
colormap(gca,'hsv'); % Cyclic/Periodic colormap
colorbar
caxis([-pi pi]);
axis([min(x),max(x),min(y),max(y),-pi,pi])
xlabel('Real(s)')
ylabel('Imag(s)')
title('Arg(F(s)) (Cyclic colormap)');
view([0,90])
    % (3) Plot poles found in the search area
subplot(3,2,3)
hold on
plot(real(poles),imag(poles),'o');
rectangle('Position',[min(poles_x_search), min(poles_y_search), max(poles_x_search)-min(poles_x_search), max(poles_y_search)-min(poles_y_search)],'LineStyle','--')
xlabel('Real(s)')
ylabel('Imag(s)')
title('Search area & Computed poles');
axis([min(x),max(x),min(y),max(y)]);
    % (4) Plot residue
subplot(3,2,5)
hold all
leg=cell(0);
plot(1:length(residue),abs(residue),'o');
leg{end+1}='|res|';
plot(1:length(residue),real(residue),'x');
leg{end+1}='real(res)';
plot(1:length(residue),imag(residue),'*');
leg{end+1}='imag(res)';
xlabel('Index');
title('Residues at computed poles');
legend(leg);
    % (5) Plot diffusive weight
subplot(3,2,4)
hold all
leg = cell(0);
plot(xi,abs(mu(xi)));
leg{end+1}='|mu|';
plot(xi,real(mu(xi)));
leg{end+1}='real(mu)';
plot(xi,imag(mu(xi)));
leg{end+1}='imag(mu)';
xlabel('xi');
title('Diffusive weight mu(xi)');
legend(leg);
    % (6) Plot Fourier transform: real & imaginary part
        % diffusive part only
F_diff = zeros(1,length(omega));
for i=1:length(omega)
    F_diff(i) = F_diff_fun(1i*omega(i));
end
        % oscillatory part only
F_osc = F_osc_fun(1i*omega);
if isempty(poles)
    F_osc = 0*omega;
end
subplot(3,4,11)
%figure(2)
%clf
%subplot(1,2,1)
hold all
leg = cell(0);
plot(omega,real(F_an(1i*omega)));
leg{end+1}='F';
% plot(omega,real(F_diff));
% leg{end+1}='F_{diff}';
plot(omega,real(F_osc),'--');
leg{end+1}='F_{osc}';
% plot(omega,real(F_diff(:)+F_osc(:)),'--');
% leg{end+1}='F_{osc}+F_{diff}';
% plot(omega,real(F_an(1i*omega)-transpose(F_diff(:))-transpose(F_osc(:))),'--');
% leg{end+1}='F_{an}-F_{osc}-F_{diff}';
% plot(omega,real(F_an(1i*omega)-transpose(F_diff(:))-transpose(F_osc(:)))./real(F_an(1i*omega)),'--');
% leg{end+1}='F_{an}-F_{osc}-F_{diff} (rel)';
% plot(omega,real(F_an(1i*omega)-transpose(F_osc(:)))./real(F_an(1i*omega)),'--');
% leg{end+1}='F_{an}-F_{osc} (rel)';
xlabel('omega w');
title('Re(F(1i*w))');
%legend(leg);
%ylim([0,1])
subplot(3,4,12)
%subplot(1,2,2)
hold all
plot(omega,imag(F_an(1i*omega)));
% plot(omega,imag(F_diff)); 
plot(omega,imag(F_osc),'--');
% plot(omega,imag((F_diff(:)+F_osc(:))),'--');
%plot(omega,imag(F_an(1i*omega)-transpose(F_diff(:))-transpose(F_osc(:))),'--');
%plot(omega,imag(F_an(1i*omega)-transpose(F_diff(:))-transpose(F_osc(:)))./imag(F_an(1i*omega)),'--');
%plot(omega,imag(F_an(1i*omega)-transpose(F_osc(:)))./imag(F_an(1i*omega)),'--');
xlabel('omega w');
title('Im(F(1i*w))');
%% II - Bode plot (F_an & G_an)
wmin=2*pi*(1e-1);
wmax=2*pi*(1e4);
Np = 1e3;
f=clf;
plotBodeDiagram(f,[wmin,wmax],F_an,'F Analytical',Np);
plotBodeDiagram(f,[wmin,wmax],@(s)(H_an(s,F_an(s))),'H Analytical',Np);
%% II - Linear plot (F_an & G_an)
w = 2*pi*linspace(1,1e4,1e2);
clf
subplot(1,2,1)
leg=cell(0);
hold all
plot(w,real(F_an(1i*w)));
leg(end+1)={'F Analytical'};
plot(w,real(H_an(1i*w,F_an(1i*w))));
leg(end+1)={'H Analytical'};
legend(leg);
xlabel('Pulsation (rad/s)');
ylabel('Re');
subplot(1,2,2)
hold all
plot(w,imag(F_an(1i*w)));
plot(w,imag(H_an(1i*w,F_an(1i*w))));
legend(leg);
xlabel('Pulsation (rad/s)');
ylabel('Im');

%% II - Semilinear plot (F_an & G_an)
w = 2*pi*logspace(-1,4,1e3);
clf
subplot(1,2,1)
leg=cell(0);
semilogx(w,real(F_an(1i*w)));
hold all
leg(end+1)={'F Analytical'};
semilogx(w,real(H_an(1i*w,F_an(1i*w))));
leg(end+1)={'H Analytical'};
legend(leg);
xlabel('Pulsation (rad/s)');
ylabel('Re');
subplot(1,2,2)
semilogx(w,imag(F_an(1i*w)));
hold all
semilogx(w,imag(H_an(1i*w,F_an(1i*w))));
xlabel('Pulsation (rad/s)');
ylabel('Re');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%