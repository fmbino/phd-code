%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Oscillatory and diffusive (=poles and cut) representation - Part 0
%--------------------------------------------------------------------------
% The purpose of this script is to define cases to be analysed in
% the next part.
% Each case is defined by:
%   - 'F_an' Function F_an(s) (Laplace Transform, Re(s)>0), which 
% may admit an oscillatory-diffusive representation
%   - 'branchpt' branch points (excluding infinity)
%   - 'H_an' Function H_an(s,F_an(s)), which is the target transfer function
% (it may be identical to F_an(s))
%   - 'name' a name (string), used in subsequent display
%   - 'polesEq' poles of F_an are zeros of polesEq(s)
%   - 'computePoles' (Optional) closed form expression of poles
%   - 'computeRes' (Optional) closed form expression of residues
%   - 'mu_an' (Optional) closed form expression of diffusive weight mu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Fractional integrator, order beta (Case n°2 from [Helie06]) OK
beta=0.5; % coeff. in ]0,1[
F_an = @(s)(s.^(-beta));
H_an = @(s,F)(F);
mu_an = @(xi)(sin(beta*pi)./(pi*xi.^beta)); % xi>0
branchpt = 0;
polesEq = @(s)(s./s); % no oscillatory part
name = sprintf('Integration %2.1g',beta);
%% Fractional derivator, order beta (Case n°3 from [Helie06]) OK
beta=0.5; % coeff. in ]0,1[
a = 2.5e-3; % coefficient a_(1/2) (MA150307, normalized)
F_an = @(s)(s.^(-beta));
H_an = @(s,F)(a*s.*F);
mu_an = @(xi)(sin(beta*pi)./(pi*xi.^beta)); % xi>0
branchpt = 0;
polesEq = @(s)(1); % no oscillatory part
name = sprintf('Derivative %2.1g, aa=%d',beta,a);
clear a
%% Fractional system of commensurate order (Case n°4 from [Helie06]) OK
% Transfert function Validated
% mu to be computed analytically
P = @(s)(s.^4 + 0.1*s.^3+s+0.1);
F_an = @(s)(1./P(sqrt(s)));
H_an = @(s,F)(F);
mu2e = @(xi,a,ln)((sin(a*pi)/pi)*((xi.^a)./(xi.^(2*a)-2*ln*cos(a*pi)*(xi.^a)+ln^2)));
mu_an = @(xi)(-0.3704*mu2e(xi,0.5,-1)+1.0010*mu2e(xi,0.5,-0.1)+(-0.3153-0.0260*1i)*mu2e(xi,0.5,0.5+0.8660*1i)+((-0.3153+0.0260*1i))*mu2e(xi,0.5,0.5-0.8660*1i));
branchpt = 0;
polesEq = @(s)(1);     % no oscillatory part
name = sprintf('Case 4 [Helie06]');
%% Exp case (Case n°6 from [Helie06]) OK
F_an = @(s)(exp(-(s.^0.5))./(s.^0.5)); % [Helie06], case 6
H_an = @(s,F)(F);
mu_an = @(xi)(cos(xi.^(0.5))./(pi*(xi.^0.5))); % xi>0
branchpt = 0;
polesEq = @(s)(1);    % no oscillatory part
name = sprintf('Case 6 [Helie06]');
%% Bessel function (Case n°8 from [Helie08])
F_an = @(s)(1./sqrt(1+s.^2)); % [Helie06], case 6
H_an = @(s,F)(F);
mu_an = @(xi)(xi./xi); % analytical mu(xi) is complex-valued
branchpt = 0;
polesEq = @(s)(1); % no oscillatory part
name = sprintf('Case 8 [Helie06]');
%% [Atala2007] with dynamic tortuosity model (OK)
tau = 1;
F_an = @(s)((sqrt(1+tau*s)-1)./s);
H_an = @(s,F)(s.*F+1);
mu_an = @(xi) ((1/pi*sqrt(abs(tau*xi-1))./xi).*(xi>(1/tau))); % xi>0
polesEq = @(s)(1);    % no oscillatory part
name = sprintf('[Atalla2007] tau=%2.1g s',tau);
%% [Dragna2014] Square-root type impedance model (w1!0, w2=w3=0) (OK)
w1 = 1;
F_an = @(s)(sqrt(s+w1)./sqrt(s)-1);
H_an = @(s,F)(1+F);
mu_an = @(xi)((1/pi)*sqrt(w1./xi-1).*(xi<w1)); % xi>0
polesEq = @(s)(1);    % no oscillatory part
name = sprintf('[Dragna2014] w1=%2.1g rad/s',w1);
%% [Dragna2014] Square-root type impedance model (w1=w2,w3!=0) (OK)
% Represents the four-parameter Attenborough model
w1 = 2;
w3 = 1;
F_an = @(s)(1./sqrt(s.*(s+w3)));
H_an = @(s,F)((s+w1).*F);
mu_an = @(xi)((1/pi)./sqrt(xi.*(w3-xi)).*(xi<w3)); % xi>0
polesEq = @(s)(1);    % no oscillatory part
name = sprintf('[Dragna2014] 4-param w1=%2.1g rad/s, w3=%2.1g rad/s',w1, w3);
%% [Dragna2014] Square-root type impedance model (w1!=w2!=!w3!=0) (OK)
% Represents the mode of Hamet and Beranger: 3 cases to distinguish
% Enforce w1<w2
w1 = 5;
w2 = 1e1;
w3 = 1;
F_an = @(s)(sqrt((s+w1).*(s+w2))./sqrt(s.*(s+w3))-1);
H_an = @(s,F)(1+F);
%mu_an = @(xi)(((xi<w1)-((xi>w2).*(xi<w3))).*((1/pi)*(sqrt(abs(((w1-xi).*(w2-xi))./(xi.*(w3-xi))))))); % xi in ]0,w3[
%mu_an = @(xi)(((xi<w1)+((xi<w2).*(xi>w3))).*((1/pi)*(sqrt(abs(((w1-xi).*(w2-xi))./(xi.*(w3-xi))))))); % xi in ]0,w3[
%mu_an = @(xi)(((xi<w3)+((xi>w1).*(xi<w2))).*((1/pi)*(sqrt(abs(((w1-xi).*(w2-xi))./(xi.*(w3-xi))))))); % xi in ]0,w3[
if w1<w2 && w3>w2 % Case (c): w3>w2
    mu_an = @(xi)((1/pi)*sqrt(abs(xi-w1).*abs(xi-w2))./sqrt(xi.*abs(xi-w3)).*((xi<w1)-(xi>w2).*(xi<w3))); % xi>0
    name = sprintf('[Dragna2014] Hamet&Ber Case (c) (w1,w2,w3)=(%2.1g,%2.1g,%2.1g)',w1,w2,w3);
elseif w1<w2 && w3<w2 && w3>w1 % Case (b): w3 between w1 and w2
    mu_an = @(xi)((1/pi)*sqrt(abs(xi-w1).*abs(xi-w2))./sqrt(xi.*abs(xi-w3)).*((xi<w1)+(xi>w3).*(xi<w2))); % xi>0
    name = sprintf('[Dragna2014] Hamet&Ber Case (b) (w1,w3,w2)=(%2.1g,%2.1g,%2.1g)',w1,w3,w2);
elseif w1<w2 && w3<w1 % Case (a): w3 < w1
    mu_an = @(xi)((1/pi)*sqrt(abs(xi-w1).*abs(xi-w2))./sqrt(xi.*abs(xi-w3)).*((xi<w3)+(xi>w1).*(xi<w2))); % xi>0
    name = sprintf('[Dragna2014] Hamet&Ber Case (a) (w3,w1,w2)=(%2.1g,%2.1g,%2.1g)',w3,w1,w2);
else
    error('Wrong parameters (w1,w2,w3)');    
end
polesEq = @(s)(1);    % no oscillatory part
%% Lossless model (Matignon's thesis, chapter 5)
r = -0.9;
a = 1; % coeff>1/2
name = sprintf('Lossless model r=%1.1e, a=%1.1e',r,a);
    % -- Delayed function
polesEq = @(s)(1-r*exp(-2*a*s));
h = @(s)exp(-s)./polesEq(s); % H^0(s) in chapter 5
F_an = h;
H_an = F_an;
    % -- Undelayed function
polesEq = @(s)(1-r*exp(-2*a*s));
h = @(s)1./polesEq(s); % H^0(s) in chapter 5
F_an = h;
H_an = F_an;
    % -- Undelayed, but regularized function
polesEq = @(s)(s.*(1-r*exp(-2*a*s)));
h = @(s)1./(polesEq(s)); % H^0(s) in chapter 5
F_an = h;
H_an = F_an;
%% Lossy cavity
e = 10;
b = 1;
r = 0.1;
F_an = @(s)exp(-2*e*sqrt(s))./(1-r*exp(-2*e*sqrt(s)-2*b*s));
F_an = @(s)1./(1-r*exp(-2*e*sqrt(s)-2*b*s));
%F_an = @(s)F_an(s)./s;
branchpt=0;
%% Webster-Lokshin (from Heleschewitz's thesis, chapter 9) (OK)
epsi = 0; % damping parameter epsilon, positive
r = 0.4; % global reflexion coefficient (real)
h = @(s)(exp(-epsi*sqrt(s))./(1-r*exp(-2*(s+epsi*sqrt(s)))));
F_an = @(s)h(s)-0*h(1e8)/2;
H_an = @(s,F)(exp(-s).*F); % F with a unitary delay
    % Analytical weight, defined over xi>0
mu_an = @(xi)((1/pi)*(exp(-4*xi)+r*exp(-2*xi)).*sin(epsi*sqrt(xi))./(exp(-4*xi)-2*r*exp(-2*xi).*cos(2*epsi*sqrt(xi))+r^2));
branchpt = 0;
polesEq = @(s)(1-r*exp(-2*(s+epsi*sqrt(s))));
computePoles = @(N)(computePolesWebsterLokshin(r,epsi,N));
computeRes = @(poles)(computeResWebsterLokshin(epsi,poles));
name = sprintf('W.-L. rho=%2.1g eps=%2.1g',r,epsi);
%% Cavity Impedance model with a Frac. Pol. cavity wavenumber (OK)
% H_an(s) = coth(j*k(s)*lc) where:
% lc : cavity length (m)
% j*k(s) : propagation wavenumber (m^-1) 
%          with j*k(s)=a0+aa*s^alpha+a1*s (alpha in ]0,1[)
    
    % Freely chosen coeffs.
a0 = 1;
a1 = 1;
aa = 0.1;
lc = 1;
    % High Stokes Bruneau wavenumber
    % Execute 'Definition-Impedance-Models'
% fprintf('lc=%1.1e mm, dc=%1.1e mm\n',1e3*lc,1e3*dc);
% a0 = 0;
% a1 = 1/c0;
% aa = sqrt(nu)/(c0*dc/2)*((gamma-1)./sqrt(Pr)+1);
    %--
alpha = 1/2; % in ]0,1[
F_an = @(s)(exp(-2*aa*lc*s.^alpha)./(1-exp(-2*lc*(a0+a1*s+aa*s.^alpha))));
H_an = @(s,F)(1+2*exp(-2*(a0+a1*s)*lc).*F);
    % Analytical weight, defined over xi>0
mu_an = @(xi)((1/pi)*exp(-2*lc*a1*xi).*sin(2*aa*lc*xi.^alpha)./(exp(-2*lc*a1*xi)-2*exp(-2*a0*lc).*cos(2*aa*lc*xi.^alpha)+exp(2*lc*(-2*a0+a1*xi))));
branchpt = 0;
polesEq = @(s)(1-exp(-2*lc*(a0+a1*s+aa*s.^alpha)));
computePoles = @(N)(computePolesFPCavity([a0,aa,a1],lc,alpha,N));
computeRes = @(poles)(computeResFPCavity([a0,aa,a1],lc,alpha,poles));
name = sprintf('Cavity FPIM (a0,a1,aa,lc,alpha)=(%2.1g,%2.1g,%2.1g,%2.1g,%2.1g)',a0,a1,aa,lc,alpha);
%% I  Function definition: A Fractional model
a = [1,0.5,1]; % a0 a_alpha a1
alpha = 0.5;
polesEq = @(s)(a(1)+a(2)*s.^alpha+a(3)*s);
branchpt = 0;
F_an = @(s)(1./polesEq(s));
H_an = @(s,F)(F);
name = sprintf('Fractional');
%% I - Function definition: B Cavity model
% z(s) = z0 * coth(Q(s)),
%with Q(s) = b(1) + b(2)*sqrt(s) + b(3)*s
z0 = 1.5;
%b = [0.05,0.004,0.002];
% b = [0,1,1];
b = [0.4,0,1];
Q = @(s)(b(1) + b(2)*sqrt(s) + b(3).*s);
branchpt = 0;
        % Impedance formulation (OK)
polesEq = @(s)(1-exp(-2*Q(s)));
h = @(s)2*z0*exp(-2*b(1))*exp(-2*b(2)*sqrt(s))./polesEq(s);
F_an = @(s)h(s) - h(1e8)/2;
H_an = @(s,F)(z0 + exp(-s*2*b(3)).*F); % z(s)
        % Admittance formulation (OK)
        % y(s) = h1(s) + exp(-s*2*b(3))*h2(s)
polesEq = @(s)z0*(1+exp(-2*Q(s)));
%h1 = @(s)1./polesEq(s); % h1
h2 = @(s)-exp(-2*b(1)-2*b(2)*sqrt(s))./polesEq(s); % h2
F_an = @(s)h1(s) - h1(1e8)/2; 
F_an = @(s)h2(s) - h2(1e8)/2; 
H_an = @(s,F)F; % would need two arguments
        % Reflection coefficient
        % beta(s) = h1(s) + exp(-s*2*b(3))*h2(s)
polesEq = @(s)(1+z0+(z0-1)*exp(-2*Q(s)));
h1 = @(s)(-2./polesEq(s)); % h1
h2 = @(s)(2*exp(-2*(b(1)+b(2)*sqrt(s)))./polesEq(s)); %h2
F_an = @(s)(h1(s)-h1(+1e8)/2);
F_an = @(s)(h2(s)-h2(+1e8)/2);
%H_an = @(s,F)F; % would need two arguments
name = sprintf('Cavity');
%% I - Function definition: C - Perforation + Cavity model
% z(s) = P(s) + z0 * coth(Q(s)),
%with   Q(s) = b(1) + b(2)*sqrt(s) + b(3)*s
%       P(s) = b(1) + b(2)*sqrt(s) + b(3)*s
z0 = 1.2;
a = [1,0,0]; % a_0, a_alpha, a_1
b = [0,0,1]; % b_0, b_1/2, b_1
P = @(s)(a(1) + a(2)*sqrt(s) + a(3).*s);
Q = @(s)(b(1) + b(2)*sqrt(s) + b(3).*s);
H_an = @(s,F)(F);
branchpt = 0;
name = sprintf('Cavity + Perf');
        % Admittance formulation
polesEq = @(s)z0+P(s)+(z0-P(s)).*exp(-2*Q(s));
h1 = @(s)1./polesEq(s);
h2 = @(s)-exp(-2*b(1)-2*b(2)*sqrt(s))./polesEq(s); % h2
%F_an = @(s)(h1(s)-h1(+1e8)/2);
%F_an = @(s)(h2(s)-h2(+1e8)/2);
        % Reflection coefficient
polesEq = @(s)(1+z0+P(s)+(z0-1-P(s)).*exp(-2*Q(s)));
h1 = @(s)(-2./polesEq(s)); % h1
h2 = @(s)(2*exp(-2*(b(1)+b(2)*sqrt(s)))./polesEq(s)); %h2
F_an = @(s)(h1(s)-h1(+1e9)/2);
%F_an = @(s)(h2(s)-h2(+1e8)/2);
%% Test function
a = [0.5,1]; % a_alpha a0
alpha = 0.5;
polesEq = @(s)(a(1)*s.^alpha-exp(a(1)*s.^alpha+a(2)*s));
branchpt = 0;
F_an = @(s)(1./polesEq(s));
H_an = @(s,F)(F);
name = sprintf('Fractional');
%% Test function
a = [0,1]; % a_alpha a0
alpha = 0.5;
polesEq = @(s)(1-exp(a(1)*s.^alpha+a(2)*s));
branchpt = 0;
F_an = @(s)(1./polesEq(s));
H_an = @(s,F)(F);
name = sprintf('Fractional');
%% Crandall model
phi = @(s)(2./s.*besseli(1,s)./besseli(0,s));
polesEq = @(s)(1-phi(sqrt(s)));
F_an = @(s)(1-1./polesEq(s));
branchpt = 0;
%% Direct delay
tau = 1;
a = [1,-0.1];
polesEq = @(s)(a(1)+a(2)*exp(-s*tau));
branchpt = 0;
h = @(s)(1./polesEq(s));
F_an = @(s)h(s)-h(1e8)/2;
H_an = @(s,F)(F);
name = sprintf('Direct delay');