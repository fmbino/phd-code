%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Local and diffusive (=poles and cut) representation - Part II
%--------------------------------------------------------------------------
% Section IV plots and compares the approximations of F(s) and H(s).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Fourier transform F(1i*w)
omega = linspace(-1e1,1e1,1e3);
%--
        % diffusive part only
F_diff = zeros(1,length(omega));
for i=1:length(omega)
    F_diff(i) = F_diff_fun(1i*omega(i));
end
        % oscillatory part only
F_osc = F_osc_fun(1i*omega);
if isempty(poles)
    F_osc = 0*omega;
end
clf
subplot(2,2,1)
hold all
leg = cell(0);
plot(omega,real(F_an(1i*omega)));
leg{end+1}='F';
plot(omega,real(F_diff));
leg{end+1}='F_{diff}';
plot(omega,real(F_osc),'--');
leg{end+1}=sprintf('F_{osc} (%d poles)',length(poles));
plot(omega,real(F_diff(:)+F_osc(:)),'--');
leg{end+1}='F_{osc}+F_{diff}';
xlabel('omega w');
title('F.T.: real(F(1i*w))');
legend(leg);
subplot(2,2,2)
hold all
plot(omega,imag(F_an(1i*omega)));
plot(omega,imag(F_diff));
plot(omega,imag(F_osc),'--');
plot(omega,imag(F_diff(:)+F_osc(:)),'--');
xlabel('omega w');
title('imag(F(1i*w))');
subplot(2,2,3)
hold all
leg = cell(0);
plot(omega,real(F_an(1i*omega)));
leg{end+1}='F';
plot(omega,real(F_quad(1i*omega)));
leg{end+1}=sprintf('F_{quad} (%d xi_k (min:%1.1e,max:%1.1e), %d s_n (min:%1.1e,max:%1.1e)',length(xik_quad),min(xik_quad),max(xik_quad),length(sn_optim),min(real(sn_optim)),max(real(sn_optim)));
plot(omega,real(F_optim(1i*omega)),'--');
leg{end+1}=sprintf('F_{optim} (%d xi_k (min:%1.1e,max:%1.1e), %d s_n (min:%1.1e,max:%1.1e)',length(xik_optim),min(xik_optim),max(xik_optim),length(sn_optim),min(real(sn_optim)),max(real(sn_optim)));
xlabel('omega w');
title('F.T.: real(F(1i*w))');
legend(leg);
subplot(2,2,4)
hold all
plot(omega,imag(F_an(1i*omega)));
plot(omega,imag(F_quad(1i*omega)));
plot(omega,imag(F_optim(1i*omega)),'--');
xlabel('omega w');
title('imag(F(1i*w))');
%% Fourier transform H(1i*w)
omega = linspace(-4e2,4e2,1e3);
%--
        % diffusive part only
F_diff = zeros(1,length(omega));
for i=1:length(omega)
    F_diff(i) = F_diff_fun(1i*omega(i));
end
        % oscillatory part only
F_osc = F_osc_fun(1i*omega);
if isempty(poles)
    F_osc = 0*omega;
end
omega = omega(:);
clf
subplot(2,1,1)
hold all
leg = cell(0);
plot(omega,real(H_an(1i*omega,F_an(1i*omega))));
leg{end+1}='H';
plot(omega,real(H_an(1i*omega,F_quad(1i*omega))));
leg{end+1}=sprintf('H_{quad} (%d xi_k (min:%1.1e,max:%1.1e), %d s_n (min:%1.1e,max:%1.1e)',length(xik_quad),min(xik_quad),max(xik_quad),length(sn_optim),min(real(sn_optim)),max(real(sn_optim)));
plot(omega,real(H_an(1i*omega,F_optim(1i*omega))),'--');
leg{end+1}=sprintf('H_{optim} (%d xi_k (min:%1.1e,max:%1.1e), %d s_n (min:%1.1e,max:%1.1e)',length(xik_optim),min(xik_optim),max(xik_optim),length(sn_optim),min(real(sn_optim)),max(real(sn_optim)));
xlabel('omega w');
title('F.T.: real(H(1i*w))');
legend(leg);
subplot(2,1,2)
hold all
plot(omega,imag(H_an(1i*omega,F_an(1i*omega))));;
plot(omega,imag(H_an(1i*omega,F_quad(1i*omega))));
plot(omega,imag(H_an(1i*omega,F_optim(1i*omega))),'--');
xlabel('omega w');
title('imag(H(1i*w))');
%% IV - Compare diffusive and local parts (poles)
% Enable to assess the dominant parts
sn_plot = sn_interp;
rn_plot = rn_interp;
%--
leg = cell(0);
clf
hold all
    % keep only upper plane poles
rn_plot = rn_plot(imag(sn_plot)>=0);
sn_plot = sn_plot(imag(sn_plot)>=0);
xik_plot = -real(sn_plot);
muk_plot = ComputeDiscreteDiffusiveRep_Interp(xik_plot,mu_an);
plot(xik_plot, muk_plot,'o');
leg(end+1)={sprintf('mu~(xi) (discretised on real(sn))')};
plot(-real(sn_plot), abs(rn_plot),'o')
leg(end+1)={sprintf('Magnitude of the residual associated with real(sn)=-xi')};
xlabel('xi (or -real(sn))');
plot(-real(sn_plot),max(muk_plot)*ones(length(sn_plot)),'-r');
leg(end+1)={sprintf('max(mu)=%1.1g',max(muk_plot))};
ylabel('mu*dxi ([mu]*[xi])');
title(sprintf('Diff. vs Local (%s)',name));
legend(leg);
clear xik_plot muk_plot sn_plot rn_plot
%% V - Bode plot (F_an)
wmin=2*pi*(1e-4);
wmax=2*pi*(1e4);
Np = 1e3;
f=clf;
plotBodeDiagram(f,[wmin,wmax],F_an,'F Analytical',Np);
plotBodeDiagram(f,[wmin,wmax],@(s)(F_pc(s,sn_optim,rn_optim,xik_optim,muk_optim)),sprintf('Optim. Nxi=%d Npoles=%d',length(xik_optim),length(sn_optim)),Np);
if ~isempty(xik_optim) % add the poles used for the optimisation
    subplot(1,2,1)
    leg=get(legend(gca),'String'); % get legend from current axes.
    xlim([wmin wmax]); xlim manual % set and freeze x-axis
    semilogx(xik_optim,20*log10(abs(F_an(1i*xik_optim))),'rx','MarkerSize',10,'LineWidth',2);
    leg(end+1)={sprintf('%d poles (diff.)',length(xik_optim))};
    if ~isempty(sn_optim)
        semilogx(real(-sn_optim),20*log10(abs(F_an(1i*real(sn_optim)))),'ro','MarkerSize',10,'LineWidth',2);
        leg(end+1)={sprintf('%d poles (local)',length(sn_optim))};
    end
    legend(leg);
    legend('Location','Best');
    semilogx([xik_optim(1),xik_optim(1)],ylim,'r--','MarkerSize',10,'LineWidth',2);
    semilogx([xik_optim(end),xik_optim(end)],ylim,'r--','MarkerSize',10,'LineWidth',2);
    subplot(1,2,2)
    xlim([wmin wmax]); xlim manual % set and freeze x-axis
    %semilogx(xik_optim,(180/pi)*unwrap(angle(F_an(xik_optim*1i))),'rx','MarkerSize',10,'LineWidth',2);
    %semilogx(real(-sn_optim),(180/pi)*unwrap(angle(F_an(real(sn_optim)*1i))),'ro','MarkerSize',10,'LineWidth',2);
    semilogx([xik_optim(1),xik_optim(1)],ylim,'r--','MarkerSize',10,'LineWidth',2);
    semilogx([xik_optim(end),xik_optim(end)],ylim,'r--','MarkerSize',10,'LineWidth',2);
end
clear wmin wmax
%% V - Bode plot (target transfert function: H_an)
wmin=2*pi*(1e-5);
wmax=2*pi*(1e5);
Np = 1e3;
f=clf;
plotBodeDiagram(f,[wmin,wmax],@(s)(H_an(s,F_an(s))),'H Analytical',Np);
plotBodeDiagram(f,[wmin,wmax],@(s)((H_an(s,F_pc(s,sn_interp,rn_interp,xik_interp,muk_interp)))),sprintf('Interp. Nxi=%d Npoles=%d',length(xik_interp),length(sn_interp)),Np);
% plotBodeDiagram(f,[wmin,wmax],@(s)((H_an(s,F_pc(s,[],[],xik_interp,muk_interp)))),sprintf('Interp. Nxi=%d Npoles=%d',length(xik_interp),0*length(sn_interp)),Np);
% plotBodeDiagram(f,[wmin,wmax],@(s)((H_an(s,F_pc(s,sn_interp,rn_interp,[],[])))),sprintf('Interp. Nxi=%d Npoles=%d',0*length(xik_interp),length(sn_interp)),Np);
plotBodeDiagram(f,[wmin,wmax],@(s)((H_an(s,F_pc(s,sn_optim,rn_optim,xik_optim,muk_optim)))),sprintf('Optim. Nxi=%d Npoles=%d',length(xik_optim),length(sn_optim)),Np);
if ~isempty(xik_optim) % plot optimisation
    subplot(1,2,1)
    leg=get(legend(gca),'String'); % get legend from current axes.
    xlim([wmin wmax]); xlim manual % set and freeze x-axis
    semilogx(xik_optim,20*log10(abs(H_an(1i*xik_optim,F_an(1i*xik_optim)))),'rx','MarkerSize',10,'LineWidth',2);
    leg(end+1)={sprintf('%d poles (diff.)',length(xik_optim))};
    semilogx(real(-sn_optim),20*log10(abs(H_an(1i*real(sn_optim),F_an(1i*real(sn_optim))))),'ro','MarkerSize',10,'LineWidth',2);
    leg(end+1)={sprintf('%d poles (local)',length(sn_optim))};
    legend(leg);
    legend('Location','Best');
    semilogx([xik_optim(1),xik_optim(1)],ylim,'r--','MarkerSize',10,'LineWidth',2);
    semilogx([xik_optim(end),xik_optim(end)],ylim,'r--','MarkerSize',10,'LineWidth',2);
    subplot(1,2,2)
    xlim([wmin wmax]); xlim manual % set and freeze x-axis
    %semilogx(xik_optim,(180/pi)*unwrap(angle(H_an(xik_optim*1i,F_an(xik_optim*1i))),pi),'rx','MarkerSize',10,'LineWidth',2);
    %semilogx(real(-sn_optim),(180/pi)*unwrap(angle(H_an(sn_optim*1i,F_an(sn_optim*1i))),pi),'ro','MarkerSize',10,'LineWidth',2);
    semilogx([xik_optim(1),xik_optim(1)],ylim,'r--','MarkerSize',10,'LineWidth',2);
    semilogx([xik_optim(end),xik_optim(end)],ylim,'r--','MarkerSize',10,'LineWidth',2);
end
clear wmin wmax
%% V - Semilinear plot (F_an)
Np = 1e3;
w = 2*pi*logspace(-1,4,1e3);
clf
subplot(1,2,1)
leg=cell(0);
semilogx(w,real(F_an(1i*w)));
leg(end+1)={sprintf('Analytical:%s',name)};
hold all
semilogx(w,real(F_pc(1i*w,sn_interp,rn_interp,xik_interp,muk_interp)));
leg(end+1)={sprintf('Interp. Nxi=%d Npoles=%d',length(xik_interp),length(sn_interp))};
semilogx(w,real(F_pc(1i*w,[],[],xik_interp,muk_interp)));
leg(end+1)={sprintf('Interp. Nxi=%d Npoles=%d',length(xik_interp),0*length(sn_interp))};
semilogx(w,real(F_pc(1i*w,sn_interp,rn_interp,[],[])));
leg(end+1)={sprintf('Interp. Nxi=%d Npoles=%d',0*length(xik_interp),length(sn_interp))};
if ~isempty(muk_optim) || ~isempty(rn_optim)
    semilogx(w,real(F_pc(1i*w,sn_optim,rn_optim,xik_optim,muk_optim)));
    leg(end+1)={sprintf('Optim. Nxi=%d Npoles=%d',length(xik_optim),length(sn_optim))};
    xlim([min(w) max(w)]); xlim manual % set and freeze x-axis
    if ~isempty(muk_optim) % add the poles used for the optimisation
        semilogx(xik_optim,real(F_an(1i*xik_optim)),'rx','MarkerSize',10,'LineWidth',2);
        leg(end+1)={sprintf('%d poles (diff.)',length(xik_optim))};
        semilogx([xik_optim(1),xik_optim(1)],ylim,'r--','MarkerSize',10,'LineWidth',2);
        semilogx([xik_optim(end),xik_optim(end)],ylim,'r--','MarkerSize',10,'LineWidth',2);
    end
    if ~isempty(sn_optim) % local part
        semilogx(real(-sn_optim),real(F_an(1i*real(sn_optim))),'ro','MarkerSize',10,'LineWidth',2);
        leg(end+1)={sprintf('%d poles (local)',length(sn_optim))};
    end
end
legend(leg,'Location','Best');
xlabel('w (rad/s)');
ylabel('Re(F(jw))');
title(sprintf('F(s) (%s)',name));
%ylim([-0.1,0.1])
subplot(1,2,2)
semilogx(w,imag(F_an(1i*w)));
hold all
semilogx(w,imag(F_pc(1i*w,sn_interp,rn_interp,xik_interp,muk_interp)));
semilogx(w,imag(F_pc(1i*w,[],[],xik_interp,muk_interp)));
semilogx(w,imag(F_pc(1i*w,sn_interp,rn_interp,[],[])));
semilogx(w,imag(F_pc(1i*w,sn_optim,rn_optim,xik_optim,muk_optim)));
if ~isempty(xik_optim) % add the poles used for the optimisation
    xlim([min(w) max(w)]); xlim manual % set and freeze x-axis
    semilogx(xik_optim,imag(F_an(1i*xik_optim)),'rx','MarkerSize',10,'LineWidth',2);
    if ~isempty(sn_optim)
    semilogx(real(-sn_optim),imag(F_an(1i*real(-sn_optim))),'ro','MarkerSize',10,'LineWidth',2);
    end
    semilogx([xik_optim(1),xik_optim(1)],ylim,'r--','MarkerSize',10,'LineWidth',2);
    semilogx([xik_optim(end),xik_optim(end)],ylim,'r--','MarkerSize',10,'LineWidth',2);
end
xlabel('w (rad/s)');
ylabel('Im(F(jw))');
title(sprintf('F(s) (%s)',name));
%ylim([-0.1,0.1])
%% V - Semilinear plot (Transfert function: H_an)
Np = 1e3;
w = 2*pi*transpose(logspace(-5,5,1e3));
clf
subplot(1,2,1)
leg=cell(0);
semilogx(w,real(H_an(1i*w,F_an(1i*w))));
hold all
leg(end+1)={sprintf('Analytical:%s',name)};
semilogx(w,real(H_an(1i*w,F_pc(1i*w,sn_interp,rn_interp,xik_interp,muk_interp))));
leg(end+1)={sprintf('Interp. Nxi=%d Npoles=%d',length(xik_interp),length(sn_interp))};
% semilogx(w,real(H_an(1i*w,F_pc(1i*w,[],[],xik_interp,muk_interp))));
% leg(end+1)={sprintf('Interp. Nxi=%d Npoles=%d',length(xik_interp),0*length(sn_interp))};
% semilogx(w,real(H_an(1i*w,F_pc(1i*w,sn_interp,rn_interp,[],[]))));
% leg(end+1)={sprintf('Interp. Nxi=%d Npoles=%d',0*length(xik_interp),length(sn_interp))};
semilogx(w,real(H_an(1i*w,F_pc(1i*w,sn_optim,rn_optim,xik_optim,muk_optim))));
leg(end+1)={sprintf('Optim. Nxi=%d Npoles=%d',length(xik_optim),length(sn_optim))};
if ~isempty(xik_optim) % add the poles used for the optimisation
    xlim([min(w) max(w)]); xlim manual % set and freeze x-axis
    semilogx(xik_optim,real(H_an(1i*xik_optim,F_an(1i*xik_optim))),'rx','MarkerSize',10,'LineWidth',2);
    leg(end+1)={sprintf('%d poles (diff.)',length(xik_optim))};
    if ~isempty(sn_optim)
    semilogx(real(-sn_optim),real(H_an(1i*real(sn_optim),F_an(1i*real(sn_optim)))),'ro','MarkerSize',10,'LineWidth',2);
    leg(end+1)={sprintf('%d poles (local)',length(sn_optim))};
    end
    semilogx([xik_optim(1),xik_optim(1)],[ylim],'r--','MarkerSize',10,'LineWidth',2);
    semilogx([xik_optim(end),xik_optim(end)],[ylim],'r--','MarkerSize',10,'LineWidth',2);
end
legend(leg,'Location','Best');
xlabel('w (rad/s)');
ylabel('Re(H(jw))');
title(sprintf('Target function H(s) (%s)',name));
ylim([0,1e3]);
subplot(1,2,2)
semilogx(w,imag(H_an(1i*w,F_an(1i*w))));
hold all
semilogx(w,imag(H_an(1i*w,F_pc(1i*w,sn_interp,rn_interp,xik_interp,muk_interp))));
semilogx(w,imag(H_an(1i*w,F_pc(1i*w,[],[],xik_interp,muk_interp))));
semilogx(w,imag(H_an(1i*w,F_pc(1i*w,sn_interp,rn_interp,[],[]))));
semilogx(w,imag(H_an(1i*w,F_pc(1i*w,sn_optim,rn_optim,xik_optim,muk_optim))));
if ~isempty(xik_optim) % add the poles used for the optimisation
    xlim([min(w) max(w)]); xlim manual % set and freeze x-axis
    semilogx(xik_optim,imag(H_an(1i*xik_optim,F_an(1i*xik_optim))),'rx','MarkerSize',10,'LineWidth',2);
    if ~isempty(sn_optim)
    semilogx(real(-sn_optim),imag(H_an(1i*real(-sn_optim),F_an(1i*real(-sn_optim)))),'ro','MarkerSize',10,'LineWidth',2);
    end
    semilogx([xik_optim(1),xik_optim(1)],[ylim],'r--','MarkerSize',10,'LineWidth',2);
    semilogx([xik_optim(end),xik_optim(end)],[ylim],'r--','MarkerSize',10,'LineWidth',2);
end
xlabel('w (rad/s)');
ylabel('Im(H(jw))');
ylim([-0.1,0.1]);
%% V - Linear plot (F_an)
Np = 5e2;
w = 2*pi*linspace(1,1e4,Np);
clf
subplot(1,2,1)
hold all
leg=cell(0);
plot(w,real(F_an(1i*w)));
leg(end+1)={sprintf('Analytical:%s',name)};
plot(w,real(F_pc(1i*w,sn_interp,rn_interp,xik_interp,muk_interp)));
leg(end+1)={sprintf('Interp. Nxi=%d Npoles=%d',length(xik_interp),length(sn_interp))};
plot(w,real(F_pc(1i*w,[],[],xik_interp,muk_interp)));
leg(end+1)={sprintf('Interp. Nxi=%d Npoles=%d',length(xik_interp),0*length(sn_interp))};
plot(w,real(F_pc(1i*w,sn_interp,rn_interp,[],[])));
leg(end+1)={sprintf('Interp. Nxi=%d Npoles=%d',0*length(xik_interp),length(sn_interp))};
% plot(w,real(F_pc(1i*w,sn_optim,rn_optim,xik_optim,muk_optim)));
% leg(end+1)={sprintf('Optim. Nxi=%d Npoles=%d',length(xik_optim),length(sn_optim))};
% if ~isempty(xik_optim) % add the poles used for the optimisation
%     xlim([min(w),max(w)]); xlim manual % freeze axis before adding the various markers
%     plot(xik_optim,real(F_an(1i*xik_optim)),'rx','MarkerSize',10,'LineWidth',2);
%     leg(end+1)={sprintf('%d poles (diff.)',length(xik_optim))};
%     if ~isempty(sn_optim)
%     plot(real(-sn_optim),real(F_an(1i*real(sn_optim))),'ro','MarkerSize',10,'LineWidth',2);
%     leg(end+1)={sprintf('%d poles (local)',length(sn_optim))};
%     end
%     plot([xik_optim(1),xik_optim(1)],[ylim],'r--','MarkerSize',10,'LineWidth',2);
%     plot([xik_optim(end),xik_optim(end)],[ylim],'r--','MarkerSize',10,'LineWidth',2);
% end
legend(leg,'Location','Best');
xlabel('w (rad/s)');
ylabel('Re(F(jw))');
title(sprintf('F(s) (%s)',name));
%ylim([-0.1,0.1])
subplot(1,2,2)
hold all
plot(w,imag(F_an(1i*w)));
plot(w,imag(F_pc(1i*w,sn_interp,rn_interp,xik_interp,muk_interp)));
plot(w,imag(F_pc(1i*w,[],[],xik_interp,muk_interp)));
plot(w,imag(F_pc(1i*w,sn_interp,rn_interp,[],[])));
% plot(w,imag(F_pc(1i*w,sn_optim,rn_optim,xik_optim,muk_optim)));
% if ~isempty(xik_optim) % add the poles used for the optimisation
%     xlim([min(w),max(w)]); xlim manual % freeze axis before adding the various markers
%     plot(xik_optim,imag(F_an(1i*xik_optim)),'rx','MarkerSize',10,'LineWidth',2);
%     if ~isempty(sn_optim)
%     plot(real(-sn_optim),imag(F_an(1i*real(-sn_optim))),'ro','MarkerSize',10,'LineWidth',2);
%     end
%     plot([xik_optim(1),xik_optim(1)],[ylim],'r--','MarkerSize',10,'LineWidth',2);
%     plot([xik_optim(end),xik_optim(end)],[ylim],'r--','MarkerSize',10,'LineWidth',2);
% end
xlabel('w (rad/s)');
ylabel('Im(F(jw))');
%ylim([-0.1,0.1])
%% V - Linear plot (Transfert function: H_an)
Np = 5e2;
w = 2*pi*transpose(linspace(1e-16,1e4,Np));
clf
subplot(1,2,1)
hold all
leg=cell(0);
plot(w,real(H_an(1i*w,F_an(1i*w))));
leg(end+1)={sprintf('Analytical:%s',name)};
% plot(w,real(H_an(1i*w,F_pc(1i*w,sn_interp,rn_interp,xik_interp,muk_interp))));
% leg(end+1)={sprintf('Interp. Nxi=%d Npoles=%d',length(xik_interp),length(sn_interp))};
%plot(w,real(H_an(1i*w,F_pc(1i*w,[],[],xik_interp,muk_interp))));
%leg(end+1)={sprintf('Interp. Nxi=%d Npoles=%d',length(xik_interp),0*length(sn_interp))};
%plot(w,real(H_an(1i*w,F_pc(1i*w,sn_interp,rn_interp,[],[]))));
%leg(end+1)={sprintf('Interp. Nxi=%d Npoles=%d',0*length(xik_interp),length(sn_interp))};
plot(w,real(H_an(1i*w,F_pc(1i*w,sn_optim,rn_optim,xik_optim,muk_optim))));
leg(end+1)={sprintf('Optim. Nxi=%d Npoles=%d',length(xik_optim),length(sn_optim))};
Z = H_an(1i*w,F_pc(1i*w,sn_optim,rn_optim,xik_optim,muk_optim));
dlmwriteHeader('Approx-Frac-Model_1Pairs.csv','Freq,Re,Im',[w/(2*pi*1e3), real(Z(:)), imag(Z(:))],'%1.6e');
%Z = H_an(1i*xik_optim,F_an(1i*xik_optim))/sigma;
%dlmwriteHeader('Approx-Frac-Model_Diff-Poles.csv','xik,Re,Im',[xik_optim(:)/(2*pi*1e3), real(Z(:)), imag(Z(:))],'%1.6e');
if ~isempty(xik_optim) % add the poles used for the optimisation
    xlim([min(w),max(w)]); xlim manual % freeze axis before adding the various markers
    plot(xik_optim,real(H_an(1i*xik_optim,F_an(1i*xik_optim))),'rx','MarkerSize',10,'LineWidth',2);
    leg(end+1)={sprintf('%d poles (diff.)',length(xik_optim))};
    if ~isempty(sn_optim)
    plot(real(-sn_optim),real(H_an(1i*real(-sn_optim),F_an(1i*real(-sn_optim)))),'ro','MarkerSize',10,'LineWidth',2);
    leg(end+1)={sprintf('%d poles (local)',length(sn_optim))};
    end
    %plot([xik_optim(1),xik_optim(1)],ylim,'r--','MarkerSize',10,'LineWidth',2);
    %plot([xik_optim(end),xik_optim(end)],ylim,'r--','MarkerSize',10,'LineWidth',2);
end
legend(leg,'Location','Best');
xlabel('w (rad/s)');
ylabel('Re(H(jw))');
title(sprintf('Target function H(s) (%s)',name));
ylim([0,5]);
subplot(1,2,2)
hold all
plot(w,imag(H_an(1i*w,F_an(1i*w))));
%plot(w,imag(H_an(1i*w,F_pc(1i*w,sn_interp,rn_interp,xik_interp,muk_interp))));
%plot(w,imag(H_an(1i*w,F_pc(1i*w,[],[],xik_interp,muk_interp))));
%plot(w,imag(H_an(1i*w,F_pc(1i*w,sn_interp,rn_interp,[],[]))));
plot(w,imag(H_an(1i*w,F_pc(1i*w,sn_optim,rn_optim,xik_optim,muk_optim))));
if ~isempty(xik_optim) % add the poles used for the optimisation
    xlim([min(w),max(w)]); xlim manual % freeze axis before adding the various markers
    plot(xik_optim,imag(H_an(1i*xik_optim,F_an(1i*xik_optim))),'rx','MarkerSize',10,'LineWidth',2);
    if ~isempty(sn_optim)
    plot(real(-sn_optim),imag(H_an(1i*real(-sn_optim),F_an(1i*real(-sn_optim)))),'ro','MarkerSize',10,'LineWidth',2);
    end
    plot([xik_optim(1),xik_optim(1)],[-60,60],'r--','MarkerSize',10,'LineWidth',2);
    plot([xik_optim(end),xik_optim(end)],[-60,60],'r--','MarkerSize',10,'LineWidth',2);
end
xlabel('w (rad/s)');
ylabel('Im(H(jw))');
ylim([-2,2]);

%% Plot optimisation results
xi = logspace(log10(1e-4),log10(1e4),10^4);
%--
clf
leg=cell(0);
subplot(1,3,1)
semilogx(xi,real(mu_an(xi)),xi,zeros(size(xi)));
leg(end+1)={'mu(xi)'};leg(end+1)={'mu(xi)=0'};
hold all
grid on
for i=1:length(xik_optim)
    if mu_an(xik_optim(i))>0
        semilogx(xik_optim(i),real(mu_an(xik_optim(i))),'go','MarkerSize',10,'LineWidth',2);
    else
        semilogx(xik_optim(i),real(mu_an(xik_optim(i))),'ro','MarkerSize',10,'LineWidth',2);
    end
end
leg(end+1)={sprintf('%d poles (green=positive)',length(xik_optim))};
xlabel('xi (rad/s)');
ylabel('mu(xi)');
title('Diffusive part');
legend(leg);
subplot(1,3,2)
leg=cell(0);
loglog(xi,abs(mu_an(xi)));
leg(end+1)={'|mu(xi)|'};
hold all
grid on
for i=1:length(xik_optim)
    if mu_an(xik_optim(i))>0
        loglog(xik_optim(i),abs(mu_an(xik_optim(i))),'go','MarkerSize',10,'LineWidth',2);
    else
        loglog(xik_optim(i),abs(mu_an(xik_optim(i))),'ro','MarkerSize',10,'LineWidth',2);
    end
end
xlabel('xi (rad/s)');
ylabel('|mu(xi)|');
title('Diffusive part');
legend(leg);
subplot(1,3,3)
leg=cell(0);
grid on
semilogx(real(-sn_optim),abs(rn_optim./rn_optim_ana),'ko','MarkerSize',10,'LineWidth',2);
leg(end+1)={sprintf('%d poles',length(sn_optim))};
xlabel('-Re[s_n] (rad/s)');
ylabel('|rn_optim/rn_analytical|');
title('Local part');
legend(leg);