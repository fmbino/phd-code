%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Compute oscillatory-diffusive representation
%--------------------------------------------------------------------------
% Purpose: enable fast and easy computation of oscillatory-diffusive
% representation, for NASA GFIT - MicroPerforate [Primus2013].
% It is not meant to enable analysis of new models.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
    % Misc. function
z2beta = @(z)((z-1)./(z+1));
beta2z = @(beta)((1+beta)./(1-beta));
        % discrete oscillatory-diffusive representation
h_od = @(s,sn,rn,xik,muk)((get1stOrderUnitaryFilter(s,[-sn(:);xik(:)]))*[rn(:);muk(:)]);
        % diffusive weight and part
mu_an=@(xi,h)(((h(xi*exp(-1i*pi))-h(xi*exp(1i*pi)))/(2*1i*pi)));
h_diff_fun = @(s,mu)(integral(@(xi)(mu(xi)./(s+xi)),0,inf));
%% I Fluid quantities
% Air @26°C
nu = 1.568e-5; % kinematic viscosity
gamma = 1.4; % ratio of spec heat constant
c0=340.29; % speed of sound
Pr = 0.707; % Prandtl number
% Air @15°C
nu = 1.48e-5; % kinematic viscosity
gamma = 1.4; % ratio of spec heat constant
c0=340.27; % speed of sound
Pr = 0.707; % Prandtl number (unsure)
% Air @295 K (used for impedance identification in [Jones2005])
nu = 1.568e-5; 
gamma = 1.4;
c0 = sqrt(gamma*287.058*295);
Pr = 0.707;
%% I Liner description
% NASA GFIT Measurements - Microperforated liner
% This one suffers from 3D effect that has not been taken into account in
% the eduction process : the middle peak should not be fitted.
% NASA GFIT microperforated
%   --Facesheet
%   Porosity 5%
%   Thickness 0.8mm
%   Hole diameter 0.3mm
%   --Honeycomb
%   Cell diameter 9.5mm
%   Cell depth 38.1mm
%-- Perforated plate constants (if any)
sigma = 5/100; % Porosity (rad)
l = 0.8e-3; % Cylinder thickness (m)
d = 0.3e-3; % Cylinder diameter (m)
%-- Cavity constant
lc = 38.1e-3; % Cavity length (m)
dc = 9.5e-3; % Cavity diameter (m)
    % Load full experimental data
ExpData = ExpDataLoadNASAGFIT2013MP();
%     % Choose experimental data
idx_SPL_source = 1;
idx_Mavg = 1;
Exp_name = ExpData{idx_Mavg,idx_SPL_source}.name;
Exp_freq = ExpData{idx_Mavg,idx_SPL_source}.freq*1000; % optimisation frequencies (Hz)
Exp_z = ExpData{idx_Mavg,idx_SPL_source}.Z_id; % impedance values (complex-valued)
fprintf('Experimental points chosen: %s\n',Exp_name);
    % Keep only given frequencies
Exp_idx_freq = 1:length(Exp_freq); % all frequencies
%Exp_idx_freq = [2,4,6,7,10,12]; % 600Hz,1kHz,1.4kHz,1.6kHz,2.2kHz,2.6kHz
Exp_freq = Exp_freq(Exp_idx_freq)
Exp_z = Exp_z(Exp_idx_freq);
%% II Analytical Model
% Normalized impedance model:
%               z(s) = P(s) + z0*coth(Q(s)),
% with
%   P(s) = a(1) + a(2)*sqrt(s) + a(3)*s
%   Q(s) = b(1) + b(2)*sqrt(s) + b(3)*s,
% Coefficients: [a(1),a(2),a(3),z0,b(1),b(2),b(3)].
    % Corrected physical values for M=0.271
% lc = 1.9*lc;
% dc = 0.5*dc;
% d = 0.5*d;
    % Corrected physical values for M=0
z0 = 1;
a = [16*nu*l/(c0*d^2),4*sqrt(nu)*l/(c0*d),l/c0]/sigma;
b = [0,lc/c0*(2/dc)*(1+(gamma-1)/sqrt(Pr))*sqrt(nu),lc/c0]; 
coeffModel_phys = [transpose(a(:)),z0,transpose(b(:))];
Model_name = 'Perf + Bruneau';
    % --
z_an = @(s,coeff)(coeff(1) + coeff(2)*sqrt(s) + coeff(3)*s+coeff(4)*coth(coeff(5) + coeff(6)*sqrt(s) + coeff(7)*s));
beta_an = @(s,coeff)(z2beta(z_an(s,coeff)));
    % Model coefficient optimisation
f_optim = Exp_freq; % optimisation frequencies (Hz)
z_optim = Exp_z; % impedance values (complex-valued)
fun = @(coeff,fdata)[real(z_an(2*pi*1i*f_optim,coeff));imag(z_an(2*pi*1i*f_optim,coeff))];
options = optimoptions('lsqcurvefit','Display','final-detailed','TolX',1e-15,'TolFun',1e-15,'MaxFunEvals',5e3,'MaxIter',1e3);
coeffModel_optim = lsqcurvefit(fun,coeffModel_phys,[f_optim(:);f_optim(:)],[real(z_optim(:));imag(z_optim(:))],0*coeffModel_phys,[],options);
%% III Compute discrete osc-diff representation
% Decomposition:
%            beta(s) = 1 + h1(s) + exp(-s*delay)*h2(s),
%where both h1 and h2 have an osc.-diff. representation.
     % choose initial coefficients
Coeff = coeffModel_optim; % from optimization
%Coeff = coeffModel_phys; % from physical model
beta_inf = 1; % theoretically, beta_inf = 1
    % --
a = Coeff(1:3); z0=Coeff(4); b = Coeff(5:end);
P = @(s)(a(1)+a(2)*sqrt(s)+a(3)*s);
Q = @(s)(b(1)+b(2)*sqrt(s)+b(3)*s);
polesEq = @(s)(1+z0+P(s)+(z0-1-P(s)).*exp(-2*Q(s)));
h1_an = @(s)(1-beta_inf-2./polesEq(s));
h2_an = @(s)(2*exp(-2*(b(1)+b(2)*sqrt(s)))./polesEq(s));
delay = 2*b(3);
    % -- Discretisation (optimisation)
    % (Alternative is to optimize on diffusive part only and compute
    % complex residues.)
        % Diffusive part (rad/s)
Nxi = 0; xi_min = 2*pi*1e-16; xi_max = 2*pi*1e4;
xi = logspace(log10(xi_min),log10(xi_max),Nxi);
xi = linspace(xi_min,xi_max,Nxi);
%xi = 2*pi*[0.7e4]; Nxi = length(xi);
        % Oscillatory part (rad/s)
poles_x_search = 2*pi*[-12,0]; poles_y_search = 2*pi*[0,12];
sn = computeZerosNumerical(polesEq,poles_x_search,poles_y_search,[2,10],'tol',1e-16); % force hermitian symmetry (can create duplicate)
I = imag(sn)>0;
sn = [sn(:);conj(sn(I))];
            % remove duplicate
sn = removeDuplicate_Complex(sn,0.5);
            % Remove 0 (branch point)
sn = sn(sn~=0);
            % sort pole by growing modulus
[~,I]=sort(abs(sn));
sn = sn(I); clear I
        % Cost function (measurements)
omegan = logspace(log10(xi_min),log10(xi_max),1e3);
[mu1,r1n,sn] = ComputeDiscreteDiffusiveRep_Optim_Std(xi,sn,omegan,@(om)(om./om),h1_an,0);
[mu2,r2n,sn] = ComputeDiscreteDiffusiveRep_Optim_Std(xi,sn,omegan,@(om)(om./om),h2_an,0);
    % -- Definition of operator for use with Artimon
MP_BndOp_beta  = DiffusiveOperator([mu1(:);r1n(:)],[xi(:);-sn(:)],'Laplace',h1_an,'Type','Standard');
MP_BndOpDelayed_beta  = DiffusiveOperator([mu2(:);r2n(:)],[xi(:);-sn(:)],'Laplace',h2_an,'Type','Standard');
TDIBC = ImpedanceBoundaryCondition(sprintf('%s_TDIBC_ReflCoeff-ModelOptim-Nphi%dNs%d',Exp_name,length(xi),length(sn)),z2beta(beta_inf),0,{0},{0},{1},{MP_BndOp_beta},{1},{MP_BndOpDelayed_beta},{delay});
TDIBC.printImpedanceSummary();
clf
TDIBC.plotPolesAndWeights(0.8e3)
    % -- print poles summary
fprintf('--\n%d diffusive poles xi in [%1.1e, %1.1e] Hz\n',length(xi),min(xi)/(2*pi),max(xi)/(2*pi));
fprintf('%d oscillatoriy poles s_n in [%1.1e, %1.1e] Hz\n--\n',length(sn),min(abs(sn))/(2*pi),max(abs(sn))/(2*pi));
%% IIIb Compute discrete osc-diff representation (Fit expe)
% This section computes new values of muj,xi,rjn, and sn by fitting
% beta_TDIBC directly to the experimental data.
% The values of muj,xi,rjn, and sn computed in IIIa are used as initial
% guess.
% Check that the poles and weight are not too large (this is a drawback of
% such an optimization method).
optimOnDelay = 1; % boolean to choose whether delay can be changed
beta_inf = 1;
    % -- Retrieve initial values from first optimization
Nxi = length(xi); % Number of diffusive poles (real-valued)
Npsi = length(r1n); % Number of oscillatory poles (complex-valued)
Np = Nxi+Npsi; % total number of poles
    % -- Definition of the tdibc
    % beta_tdibc(s) =             weight1_k                    weight2_k
    %                 beta_inf + ---------- + exp(-s*delay)*-------------,
    %                              s-poles_k                    s-poles_k
    % with Np poles.
    % Theoretically, beta_inf = 1, but to obtain a TDIBC passive over all
    % its frequency range, it can be necessary to choose beta_inf in
    % (-1,1).
    % Poles and weight stored in 'param' as
    % param = [poles;weight1;weight2;delay] complex-valued.
    %   vector length: 3*Np+1=3*(Nxi+Npsi)+1
beta_tdibc = @(s,poles,weight1,weight2,delay) (beta_inf+get1stOrderUnitaryFilter(s,-poles(:))*weight1(:) + exp(-s(:)*delay).*(get1stOrderUnitaryFilter(s,-poles(:))*weight2(:)));
beta_tdibc = @(s,param)beta_tdibc(s,param(1:Np),param(Np+(1:Np)),param(2*Np+(1:Np)),param(3*Np+1));
    % For the optimization, storage of parameters in 'coeff', real-valued
    % vector of length 3*Np+1.
    % coeff =
    % [-xi;real(sn);imag(sn);mu1;real(rn1);imag(rn1);mu2;real(rn2);imag(rn2);delay]
    % Convert from 'coeff' to 'param'
coeff2param = @(c) [c(0 + (1:Nxi));             c(0 + Nxi + (1:(Npsi/2)))+1i*c(0 + Nxi + (Npsi/2) + (1:(Npsi/2))); conj(c(0 + Nxi + (1:(Npsi/2)))+1i*c(0 + Nxi + (Npsi/2) + (1:(Npsi/2))));
                    c(Nxi+Npsi + (1:Nxi));      c(Nxi+Npsi + Nxi + (1:(Npsi/2)))+1i*c(Nxi+Npsi + Nxi + (Npsi/2) + (1:(Npsi/2))); conj(c(Nxi+Npsi + Nxi + (1:(Npsi/2)))+1i*c(Nxi+Npsi + Nxi + (Npsi/2) + (1:(Npsi/2)))); 
                    c(2*(Nxi+Npsi)+(1:Nxi));    c(2*(Nxi+Npsi) + Nxi + (1:(Npsi/2)))+1i*c(2*(Nxi+Npsi) + Nxi + (Npsi/2) + (1:(Npsi/2))); conj(c(2*(Nxi+Npsi) + Nxi + (1:(Npsi/2)))+1i*c(2*(Nxi+Npsi) + Nxi + (Npsi/2) + (1:(Npsi/2))));
                    c(3*(Nxi+Npsi)+1)];
    % Initial value for optimization coefficient, from previous
    % optimization
idx_polesplus = find(imag(sn)>=0); % must be of length Npsi/2
if length(idx_polesplus)~=(Npsi/2)
   error('Number of poles in the upper half-plane is wrong.');
end
sn_plus = sn(idx_polesplus); % poles with imag>0
r1n_plus = r1n(idx_polesplus); % weight1 corresponding to sn
r2n_plus = r2n(idx_polesplus); % weight2 corresponding to sn
    % To avoid unstable poles, it may be necessary to tweak the initial
    % guess.
xi_corrected = xi;
if numel(xi)~=0
    % xi_corrected(1) = xi(1)/2; % shift lower diffusive pole to avoid unstable pole
%     xi_corrected(1) = 2*pi*0.5e3;
%     xi_corrected = 2*pi*1e3*[0.5,2];
end
coeff_initial = [-xi_corrected(:);real(sn_plus(:));imag(sn_plus(:));mu1(:);real(r1n_plus(:));imag(r1n_plus(:));mu2(:);real(r2n_plus(:));imag(r2n_plus(:));delay];
    % Non-linear optimization
f_optim = Exp_freq;
z_optim = Exp_z;
    % Function definition for optimization
fun = @(coeff,fdata)[real(beta_tdibc(2*pi*1i*f_optim,coeff2param(coeff)));imag(beta_tdibc(2*pi*1i*f_optim,coeff2param(coeff)))];
    % Nonlinear least square optimization
options = optimoptions('lsqcurvefit','Display','final-detailed','TolX',1e-5,'TolFun',1e-5);
    if optimOnDelay==1
        [coeffTDIBC_optim,res] = lsqcurvefit(fun,coeff_initial,[f_optim(:);f_optim(:)],[real(z2beta(z_optim(:)));imag(z2beta(z_optim(:)))],[],[],options);
    else % delay is not an optimization variable
        fun = @(coeff,fdata)fun([coeff(:);delay],fdata);
        [coeffTDIBC_optim,res] = lsqcurvefit(fun,coeff_initial(1:(end-1)),[f_optim(:);f_optim(:)],[real(z2beta(z_optim(:)));imag(z2beta(z_optim(:)))],[],[],options);
        coeffTDIBC_optim = [coeffTDIBC_optim(:);delay];
    end
paramTDIBC_optim = coeff2param(coeffTDIBC_optim);
    % get poles and weight
poles_optim = paramTDIBC_optim(1:Np);
weight1_optim = paramTDIBC_optim(Np+(1:Np));
weight2_optim = paramTDIBC_optim(2*Np+(1:Np));
delay_optim = paramTDIBC_optim(3*Np+1);
    % initial values
poles_init = [-xi(:);sn(:)];
weight1_init = [mu1(:);r1n(:)];
weight2_init = [mu2(:);r2n(:)];
    % -- Compare initial vs optimized diffusive representation
fprintf('-- Poles\n');
fprintf('Re(poles) Im(poles) | Re(poles_optim) Im(poles_optim) | dRe dIm\n-----------------------------------------------------------------------\n');
for i=1:length(poles_optim)
    fprintf('%10.2e %10.2e | %10.2e %10.2e | %10.2e%% %10.2e%%\n',real(poles_init(i)),imag(poles_init(i)),real(poles_optim(i)),imag(poles_optim(i)),100*real(poles_optim(i)-poles_init(i))/real(poles_init(i)),100*imag(poles_optim(i)-poles_init(i))/imag(poles_init(i)));
end
fprintf('-- Weight1 (undelayed operator)\n');
fprintf('Re(weight) Im(weight) | Re(weight_optim) Im(weight_optim) | dRe dIm\n-----------------------------------------------------------------------\n');
for i=1:length(weight1_optim)
    fprintf('%10.2e %10.2e | %10.2e %10.2e | %10.2e%% %10.2e%%\n',real(weight1_init(i)),imag(weight1_init(i)),real(weight1_optim(i)),imag(weight1_optim(i)),100*real(weight1_optim(i)-weight1_init(i))/real(weight1_init(i)),100*imag(weight1_optim(i)-weight1_init(i))/imag(weight1_init(i)));
end
fprintf('-- Weight2 (delayed operator)\n');
fprintf('Re(weight) Im(weight) | Re(weight_optim) Im(weight_optim) | dRe dIm\n-----------------------------------------------------------------------\n');
for i=1:length(weight2_optim)
    fprintf('%10.2e %10.2e | %10.2e %10.2e | %10.2e%% %10.2e%%\n',real(weight2_init(i)),imag(weight2_init(i)),real(weight2_optim(i)),imag(weight2_optim(i)),100*real(weight2_optim(i)-weight2_init(i))/real(weight2_init(i)),100*imag(weight2_optim(i)-weight2_init(i))/imag(weight2_init(i)));
end
fprintf('Delay: %10.2e | %10.2e',delay,delay_optim);
    % -- Definition of time-domain boundary condition
BndOp_beta  = DiffusiveOperator(weight1_optim,-poles_optim,'Laplace',h1_an,'Type','Standard');
BndOpDelayed_beta  = DiffusiveOperator(weight2_optim,-poles_optim,'Laplace',h2_an,'Type','Standard');
TDIBCOptimized = ImpedanceBoundaryCondition(sprintf('%s_TDIBC_ReflCoeff-ExpeOptim-%dpoles',Exp_name,length(poles_optim)),z2beta(beta_inf),0,{0},{0},{1},{BndOp_beta},{1},{BndOpDelayed_beta},{delay_optim});
TDIBCOptimized.printImpedanceSummary();
    % -- print poles summary
fprintf('-- (Optimized) %d poles in [%1.1e, %1.1e] Hz\n',length(poles_optim),min(abs(poles_optim))/(2*pi),max(abs(poles_optim))/(2*pi));
    % plot summary
figure(1)
clf
subplot(3,1,1)
hold all
grid on
plot(real([-xi(:);sn]),imag([-xi(:);sn]),'o','DisplayName',sprintf('Model (delay=%1.2es)',delay));
plot(real(poles_optim),imag(poles_optim),'x','DisplayName',sprintf('Optim (delay=%1.2es)',delay_optim));
A = max(abs([-xi(:);sn]));
plot(A*cos(linspace(0,2*pi,1e2)),A*sin(linspace(0,2*pi,1e2)),'DisplayName','Model radius');
plot(2*pi*5e3*cos(linspace(0,2*pi,1e2)),2*pi*5e3*sin(linspace(0,2*pi,1e2)),'DisplayName','Radius at 5Hz');
legend('show');
% set(gca,'DataAspectRatio',[1,1,1])
title(sprintf('%d Poles (Max:%1.1e rad/s %1.1e Hz)',numel(poles_optim),max(abs(poles_optim)),(1/(2*pi))*max(abs(poles_optim))));
xlabel('Re(s) (rad/s)');
xlabel('Im(s) (rad/s)');
subplot(3,1,2)
hold all
grid on
plot(real([mu1(:);r1n(:)]),imag([mu1(:);r1n(:)]),'o');
plot(real(weight1_optim(:)),imag(weight1_optim(:)),'x');
A = max(abs([mu1(:);r1n(:)]));
plot(A*cos(linspace(0,2*pi,1e2)),A*sin(linspace(0,2*pi,1e2)),'DisplayName','Model radius');
plot(2*pi*5e3*cos(linspace(0,2*pi,1e2)),2*pi*5e3*sin(linspace(0,2*pi,1e2)),'DisplayName','Radius at 1e3');
% set(gca,'DataAspectRatio',[1,1,1])
title(sprintf('Weights of h1 (Max:%1.1e)',max(abs(weight1_optim))));
subplot(3,1,3)
hold all
grid on
plot(real([mu2(:);r2n(:)]),imag([mu2(:);r2n(:)]),'o');
plot(real(weight2_optim(:)),imag(weight2_optim(:)),'x');
A = max(abs([mu2(:);r2n(:)]));
plot(A*cos(linspace(0,2*pi,1e2)),A*sin(linspace(0,2*pi,1e2)),'DisplayName','Model radius');
plot(2*pi*5e3*cos(linspace(0,2*pi,1e2)),2*pi*5e3*sin(linspace(0,2*pi,1e2)),'DisplayName','Radius at 1e3');
% set(gca,'DataAspectRatio',[1,1,1])
title(sprintf('Weights of h1 (Max:%1.1e)',max(abs(weight2_optim))));
    % -- Last check
if max(real(poles_optim))>0
   error('Unstable poles.');
end
%% IV Plot analytical vs discrete vs experimental
    % Load optimized TDIBC from file
%TDIBCtmp = TDIBC;
%load('GIT-CT57-SPL130dB-Mavg000-2DFEM-ReflCoeff-1xik-2sn','TDIBC');
%load('GFIT-MP-SPL120dB-Mavg271-2DFEM_TDIBC_ReflCoeff-ExpeOptim-4poles','TDIBC');
%TDIBCOptimized = TDIBC;
%TIBC = TDIBCtmp;
    %--
    % Plot frequencies (Hz)
freq = linspace(0,1e4,1e4);
figure(2)
clf
s = 1i*2*pi*freq;
subplot(2,2,1)
hold all
plot(freq,real(z_an(s,coeffModel_phys)),'b');
plot(freq,real(z_an(s,coeffModel_optim)),'r');
plot(Exp_freq,real(Exp_z),'kx');
xlabel('Frequency (Hz)');
ylabel('Normalized resistance (rad)');
grid on
%set(gca,'DataAspectRatio',[1,1,1])
ylim([0,4])
subplot(2,2,2)
hold all
plot(freq,imag(z_an(s,coeffModel_phys)),'b');
plot(freq,imag(z_an(s,coeffModel_optim)),'r');
plot(Exp_freq,imag(Exp_z),'kx');
xlabel('Frequency (Hz)');
ylabel('Normalized reactance (rad)');
ylim([-2.5,2.5])
grid on
%set(gca,'DataAspectRatio',[1,1,1])
subplot(2,2,3)
hold all
plot(freq,abs(beta_an(s,coeffModel_phys)),'b');
plot(freq,abs(beta_an(s,coeffModel_optim)),'r');
plot(Exp_freq,abs(z2beta(Exp_z)),'kx');
grid on
xlabel('Frequency (Hz)');
ylabel('|reflection coefficient| (rad)');
%set(gca,'DataAspectRatio',[1,1,1]);
subplot(2,2,4)
hold all
plot(freq,rad2deg(angle(beta_an(s,coeffModel_phys))),'b','DisplayName',sprintf('%s (Physical coefficient)',Model_name));
plot(freq,rad2deg(angle(beta_an(s,coeffModel_optim))),'r','DisplayName',sprintf('%s (Optimized coefficient)',Model_name));
plot(Exp_freq,rad2deg(angle(z2beta(Exp_z))),'kx','DisplayName',sprintf('%s',Exp_name));
xlabel('Frequency (Hz)');
ylabel('phase(reflection coefficient) (deg)');
        % Plot discrete TDIBC
if exist('TDIBC','var')
    TDIBC.plot(freq,'reflcoeff','LineSpec','g-');
end
if exist('TDIBCOptimized','var')
    TDIBCOptimized.plot(freq,'reflcoeff','LineSpec','k--');
end
legend('location','northoutside');
    % Plot table of error at each frequency
if exist('TDIBC','var')
    fprintf('Initial oscillatory-diffusive representation\n');
    fprintf('Freq (kHz)\tRe(Z)       Im(Z)        |beta|       arg(beta)\n');
    fprintf('-----------------------------------------------------------------\n');
    for k=1:length(Exp_freq)
        freq_exp = Exp_freq(k); % frequency
        z_exp = Exp_z(k); % identified z
        beta_exp = z2beta(z_exp); % identified beta
            % TDIBC
        beta_tdibc_val = TDIBC.Laplace(1i*2*pi*freq_exp,1,0);
        z_tdibc = beta2z(beta_tdibc_val);
            % Error
        dangle = rad2deg(angle(beta_tdibc_val)-angle(beta_exp));
        dangle = [dangle,mod(dangle,360)]; [~,idx] = min(abs(dangle)); % closest to 0°
        fprintf('%10.2ekHz %10.4g%% %10.3g%% | %10.4g%% %10.4g°\n',freq_exp,100*(real(z_tdibc-z_exp))/real(z_exp),100*(imag(z_tdibc-z_exp))/imag(z_exp),100*(abs(beta_tdibc_val)-abs(beta_exp))/abs(beta_exp),dangle(idx));
    end
end
if exist('TDIBCOptimized','var')
    fprintf('Optimized oscillatory-diffusive representation\n');
    fprintf('Freq (kHz)\tRe(Z)       Im(Z)        |beta|       arg(beta)\n');
    fprintf('-----------------------------------------------------------------\n');
    for k=1:length(Exp_freq)
        freq_exp = Exp_freq(k); % frequency
        z_exp = Exp_z(k); % identified z
        beta_exp = z2beta(z_exp);
            % TDIBC
        beta_tdibc_val = TDIBCOptimized.Laplace(1i*2*pi*freq_exp,1,0);
        z_tdibc = beta2z(beta_tdibc_val);
            % Error
        dangle = rad2deg(angle(beta_tdibc_val)-angle(beta_exp));
        dangle = [dangle,mod(dangle,360)]; [~,idx] = min(abs(dangle)); % closest to 0°
        fprintf('%10.2ekHz %10.4g%% %10.3g%% | %10.4g%% %10.4g°\n',freq_exp,100*(real(z_tdibc-z_exp))/real(z_exp),100*(imag(z_tdibc-z_exp))/imag(z_exp),100*(abs(beta_tdibc_val)-abs(beta_exp))/abs(beta_exp),dangle(idx));
    end
end
%% V Save time-domain impedance boundary condition
% The TDIBC is saved in .mat file, under the variable name 'TDIBC'.
fprintf('File creation...\n');
save(sprintf('%s',TDIBC.Name),'TDIBC');
TDIBCtemp = TDIBC; TDIBC = TDIBCOptimized;
fprintf('File creation...\n');
save(sprintf('%s',TDIBC.Name),'TDIBC');
TDIBC = TDIBCtemp;
%% V Export curves to file: Experimental data
    % Experimental data
namestr = sprintf('%s_Expe.csv',Exp_name);
dlmwrite(namestr,'Freq(kHz),Re(z),Im(z),|beta|,phase(beta)(deg)','Delimiter','');
dlmwrite(namestr,[Exp_freq(:)/1e3,real(Exp_z(:)),imag(Exp_z(:)),abs(z2beta(Exp_z(:))),rad2deg(angle(z2beta(Exp_z(:))))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
%% V Export Impedance curves to file
    % Plot frequencies (Hz)
freq = linspace(0,1e4,1e3);
s = 1i*2*pi*freq;
    % -- Physical model
Z = z_an(s,coeffModel_phys);
namestr = sprintf('%s_Model_PhysicalCoeff.csv',Exp_name);
dlmwrite(namestr,'Freq(kHz),Re(z),Im(z),|beta|,phase(beta)(deg)','Delimiter','');
dlmwrite(namestr,[freq(:)/1e3,real(Z(:)),imag(Z(:)),abs(z2beta(Z(:))),rad2deg(angle(z2beta(Z(:))))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
Z = z_an(s,coeffModel_optim);
namestr = sprintf('%s_Model_OptimizedCoeff.csv',Exp_name);
dlmwrite(namestr,'Freq(kHz),Re(z),Im(z),|beta|,phase(beta)(deg)','Delimiter','');
dlmwrite(namestr,[freq(:)/1e3,real(Z(:)),imag(Z(:)),abs(z2beta(Z(:))),rad2deg(angle(z2beta(Z(:))))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
    % --  Initial time-domain impedance boundary condition
if exist('TDIBC','var')
    Z = TDIBC.plot(freq,'reflcoeff');
    namestr = sprintf('%s.csv',TDIBC.Name);
    dlmwrite(namestr,'Freq(kHz),Re(z),Im(z),|beta|,phase(beta)(deg)','Delimiter','');
    dlmwrite(namestr,[freq(:)/1e3,real(Z(:)),imag(Z(:)),abs(z2beta(Z(:))),rad2deg(angle(z2beta(Z(:))))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
end
    % -- Optimized time-domain impedance boundary condition
if exist('TDIBCOptimized','var')
Z = TDIBCOptimized.plot(freq,'reflcoeff');
namestr = sprintf('%s.csv',TDIBC.Name);
dlmwrite(namestr,'Freq(kHz),Re(z),Im(z),|beta|,phase(beta)(deg)','Delimiter','');
dlmwrite(namestr,[freq(:)/1e3,real(Z(:)),imag(Z(:)),abs(z2beta(Z(:))),rad2deg(angle(z2beta(Z(:))))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
end
%% IV (Diagnostic) Plot diffusive dashboard
% Check approximation of both h1 and h2.
        % Surface plot (Hz)
x_freq = linspace(-5e3,1,8e2);
y_freq = linspace(-5e3,5e3,8e2);
color_zmax=1e1; % saturation color for colorbar (modulus)
        % Fourier transform (Hz)
freq = linspace(1e1,5e3,1e2);
    %--
    % (1) Plot modulus
[Sr,Si] = meshgrid(2*pi*x_freq,2*pi*y_freq); S = (Sr+1i*Si);
figure(1)
clf
subplot(3,2,1)
Z = 1./abs(polesEq(S));
surf(Sr/(2*pi),Si/(2*pi),Z,'edgecolor','none');
colormap(gca,'default');
colorbar
caxis([max(-color_zmax,min(Z(:))) min(color_zmax,max(Z(:)))])
axis([min(x_freq),max(x_freq),min(y_freq),max(y_freq),-color_zmax,color_zmax])
xlabel('Real(s) (Hz)');
ylabel('Imag(s) (Hz)');
title('1/|PolesEq(s)|');
view([0,90])
    % (2) Plot poles found in the search area
subplot(3,2,2)
hold on
leg=cell(0);
plot(real(sn)/(2*pi),imag(sn)/(2*pi),'o');
leg{end+1}=sprintf('%d s_n [%1.1e, %1.1e] Hz',length(sn),min(abs(sn))/(2*pi),max(abs(sn))/(2*pi));
plot(-xi/(2*pi),0*xi,'x');
leg{end+1}=sprintf('%d xi_k [%1.1e, %1.1e] Hz',length(xi),min(xi)/(2*pi),max(xi)/(2*pi));
rectangle('Position',(2*pi)^(-1)*[min(poles_x_search), min(poles_y_search), max(poles_x_search)-min(poles_x_search), max(poles_y_search)-min(poles_y_search)],'LineStyle','--')
legend(leg,'location','northwest');
xlabel('Real(s) (Hz)')
ylabel('Imag(s) (Hz)')
title(sprintf('Oscillatory and diffusive poles\n'));
axis([min(x_freq),max(x_freq),min(y_freq),max(y_freq)]);
grid on
    % (3-4) Plot Fourier transform: real & imaginary part
omega = 2*pi*freq;
F_an = h1_an;
F_diff = zeros(1,length(omega));
for i=1:length(omega)
    F_diff(i) = h_diff_fun(1i*omega(i),@(xi)mu_an(xi,F_an));
end
F_optim = @(s)(h_od(s,sn,r1n,xi,mu1));
subplot(3,2,3)
hold all
leg = cell(0);
plot(freq,real(F_an(1i*omega)));
leg{end+1}='h1';
plot(freq,real(F_diff));
leg{end+1}='h1_{diff}';
plot(freq,real(F_optim(1i*omega)),'--');
leg{end+1}=sprintf('h1_{optim}');
legend(leg);
xlabel('freq (Hz)');
title('real(h1(1i*w))');
grid on
subplot(3,2,4)
hold all
plot(freq,imag(F_an(1i*omega)));
plot(freq,imag(F_diff));
plot(freq,imag(F_optim(1i*omega)),'--');
xlabel('freq (Hz)');
title('imag(h1(1i*w))');
grid on
    % (5-6) Plot Fourier transform: real & imaginary part
F_an = h2_an;
F_diff = zeros(1,length(omega));
for i=1:length(omega)
    F_diff(i) = h_diff_fun(1i*omega(i),@(xi)mu_an(xi,F_an));
end
F_optim = @(s)(h_od(s,sn,r2n,xi,mu2));
subplot(3,2,5)
hold all
leg = cell(0);
plot(freq,real(F_an(1i*omega)));
leg{end+1}='h2';
plot(freq,real(F_diff));
leg{end+1}='h2_{diff}';
plot(freq,real(F_optim(1i*omega)),'--');
leg{end+1}=sprintf('h2_{optim}');
legend(leg);
xlabel('freq (Hz)');
title('real(h2(1i*w))');
grid on
subplot(3,2,6)
hold all
plot(freq,imag(F_an(1i*omega)));
plot(freq,imag(F_diff));
plot(freq,imag(F_optim(1i*omega)),'--');
xlabel('freq (Hz)');
title('imag(h2(1i*w))');
grid on