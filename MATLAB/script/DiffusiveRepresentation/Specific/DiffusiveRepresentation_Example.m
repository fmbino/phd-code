%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Compute oscillatory-diffusive representations
%--------------------------------------------------------------------------
% Purpose: enable fast and easy computation of oscillatory-diffusive
% representation, for some common models in acoustics.
% It is not meant to enable analysis of new models.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
z2beta = @(z)((z-1)./(z+1));
beta2z = @(beta)((1+beta)./(1-beta));
%% Fractional polynomial
% Model: perforation | Numerical flux: z-formulation
    % -- Definition
a = [0,1,0];
z_an = @(s)(a(1) + a(2)*sqrt(s) + a(3)*s);
h_an = @(s)1./sqrt(s);
z_discrete = @(s,h1,h2)(a(1) + a(2)*s.*h1 + a(3)*s);
    % -- Discretisation (common)
Nxi = 4;
    % -- Discretisation (quadrature)
        % Diffusive weight
mu_an=@(xi)(((h_an(xi*exp(-1i*pi))-h_an(xi*exp(1i*pi)))/(2*1i*pi)));
[xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(mu_an,Nxi,'CoVParam',2);
    % -- Discretisation (optimisation)
        % Diffusive part
% xi_min = 2*pi*1e-0; xi_max = 2*pi*1e4;
% xi = logspace(log10(xi_min),log10(xi_max),Nxi);
        % Cost function (measurements)
% omegan = logspace(log10(xi_min),log10(xi_max),1e3);
% [mu,~,~] = ComputeDiscreteDiffusiveRep_Optim_Std(xi,[],omegan,@(om)(om./om),h_an,0);
    % -- Plot
f = linspace(-max(xi),max(xi),1e4)/(2*pi);
plotDiscreteDiffusiveRep(clf,f,mu,xi,[],[],[],[],[],[],z_discrete,z_an);
    % -- Definition of operator for use with Artimon
BndOp_op  = DiffusiveOperator(mu,xi,'Laplace',@(s)(sqrt(s)),'Type','Extended');
%% Fractional polynomial
% Model: perforation | Numerical flux: beta-formulation
% Decomposition is beta(s) = 1+h(s), with h(s) = -2/(1+z(s))
a = [1,1,0];
z_an = @(s)(a(1)+a(2)*sqrt(s)+a(3)*s); % impedance model
beta_an = @(s)(z2beta(z_an(s))); % reflection coefficient
h_an = @(s)(-2./(1+z_an(s))); % osc.-diff. part of beta
beta_discrete = @(s,h1,h2)(1+h1);
z_discrete = @(s,h1,h2)(beta2z(beta_discrete(s,h1,h2)));
    % -- Discretisation (common)
Nxi = 10;
    % -- Discretisation (quadrature)
        % Diffusive weight
% mu_an=@(xi)(((h_an(xi*exp(-1i*pi))-h_an(xi*exp(1i*pi)))/(2*1i*pi)));
% [xi,mu] = ComputeDiscreteDiffusiveRep_QuadratureGL(mu_an,Nxi,'CoVParam',1.3);
    % -- Discretisation (optimisation)
        % Diffusive part
xi_min = 1e-1; xi_max = 1e4;
xi = logspace(log10(xi_min),log10(xi_max),Nxi);
        % Cost function (measurements)
omegan = logspace(log10(xi_min),log10(xi_max),1e3);
[mu,~,~] = ComputeDiscreteDiffusiveRep_Optim_Std(xi,[],omegan,@(om)(om./om),h_an,0);
    % -- Plot (reflection coefficient)
f = linspace(-max(xi),max(xi),1e4)/(2*pi);
plotDiscreteDiffusiveRep(clf,f,mu,xi,[],[],[],[],[],[],beta_discrete,beta_an);
    % -- Plot (impedance)
plotDiscreteDiffusiveRep(clf,f,mu,xi,[],[],[],[],[],[],z_discrete,z_an);
    % -- Definition of operator for use with Artimon
BndOp_op  = DiffusiveOperator(mu,xi,'Laplace',h_an,'Type','Standard');
%% Fractional coth
% Model: perforation + cavity | Numerical flux: z-formulation
% Decomposition z(s) = coth(Q(s)) = 1 + exp(-s*tau)*h(s)
b = [1,0.5,1];
Q = @(s)(b(1)+b(2)*sqrt(s)+b(3)*s);
z_an = @(s)(coth(Q(s)));
h_an = @(s)(exp(-2*b(1)-2*b(2)*sqrt(s))./(1-2*exp(-2*Q(s))));
z_discrete = @(s,h1,h2)(1+2*exp(-2*b(3)*s).*h1);
polesEq = @(s)1-2*exp(-2*Q(s));
    % -- Discretisation (optimisation)
    % (Alternative is to compute 'rn' using complex residues.)
        % Diffusive part
Nxi = 10; xi_min = 2*pi*1e-1; xi_max = 2*pi*1e2;
xi = logspace(log10(xi_min),log10(xi_max),Nxi);
        % Oscillatory part
            % compute poles
poles_x_search = [-10,0]; poles_y_search = [0,10];
poles = computeZerosNumerical(polesEq,poles_x_search,poles_y_search,[5,5]);
            % force hermitian symmetry (can create duplicate)
I = imag(poles)>0;
poles = [poles(:);conj(poles(I))];
            % remove duplicate
poles = removeDuplicate_Complex(poles,1e-8);
            % Remove 0 (branch point)
poles = poles(poles~=0);
            % sort pole by growing modulus
[~,I]=sort(abs(poles));
poles = poles(I); clear I
        % Cost function (measurements)
omegan = logspace(log10(xi_min),log10(xi_max),1e3);
[mu,rn,poles] = ComputeDiscreteDiffusiveRep_Optim_Std(xi,poles,omegan,@(om)(om./om),h_an,0);
    % -- Plot (impedance)
f = linspace(-max(xi),max(xi),1e4)/(2*pi);
plotDiscreteDiffusiveRep(clf,f,mu,xi,rn,poles,[],[],[],[],z_discrete,z_an);
    % -- Definition of operator for use with Artimon
BndOpDelayed_op  = DiffusiveOperator([mu(:);rn(:)],[xi(:);-poles(:)],'Laplace',h_an,'Type','Standard');
%% Fractional polynomial + Fractional coth
% Model: perforation + cavity | Numerical flux: beta-formulation
% Impedance model: z(s) = P(s) + coth(Q(s))
% Decomposition beta(s) = 1 + h1(s) + exp(-s*tau)*h2(s)
a = [1,0.5,1];
b = [1,0.5,1];
P = @(s)(a(1)+a(2)*sqrt(s)+a(3)*s);
Q = @(s)(b(1)+b(2)*sqrt(s)+b(3)*s);
z_an = @(s)(P(s)+coth(Q(s)));
beta_an = @(s)(z2beta(z_an(s)));
polesEq = @(s)(2+P(s)-2*(1+P(s)).*exp(-2*Q(s)));
h1_an = @(s)(-2./polesEq(s));
h2_an = @(s)(4*exp(-2*(b(1)+b(2)*sqrt(s)))./polesEq(s));
beta_discrete = @(s,h1,h2)(1+h1+exp(-2*b(3)*s).*h2);
z_discrete = @(s,h1,h2)(beta2z(beta_discrete(s,h1,h2)));
    % -- Discretisation (optimisation)
    % (Alternative is to compute 'r1n' and 'r2n' using complex residues.)
        % Diffusive part
Nxi = 20; xi_min = 2*pi*1e-1; xi_max = 2*pi*1e1;
xi = logspace(log10(xi_min),log10(xi_max),Nxi);
        % Oscillatory part
            % compute poles
poles_x_search = [-50,0]; poles_y_search = [0,50];
poles = computeZerosNumerical(polesEq,poles_x_search,poles_y_search,[10,10]);
            % force hermitian symmetry (can create duplicate)
I = imag(poles)>0;
poles = [poles(:);conj(poles(I))];
            % remove duplicate
poles = removeDuplicate_Complex(poles,1e-8);
            % Remove 0 (branch point)
poles = poles(poles~=0);
            % sort pole by growing modulus
[~,I]=sort(abs(poles));
poles = poles(I); clear I
        % Cost function (measurements)
omegan = logspace(log10(xi_min),log10(xi_max),1e3);
[mu1,r1n,poles] = ComputeDiscreteDiffusiveRep_Optim_Std(xi,poles,omegan,@(om)(om./om),h1_an,0);
[mu2,r2n,poles] = ComputeDiscreteDiffusiveRep_Optim_Std(xi,poles,omegan,@(om)(om./om),h2_an,0);
    % -- Plot (reflection coefficient)
f = linspace(-max(xi),max(xi),1e4)/(2*pi);
plotDiscreteDiffusiveRep(clf,f,mu1,xi,r1n,poles,mu2,xi,r2n,poles,beta_discrete,beta_an);
    % -- Plot (impedance)
plotDiscreteDiffusiveRep(clf,f,mu1,xi,r1n,poles,mu2,xi,r2n,poles,z_discrete,z_an);
    % -- Definition of operator for use with Artimon
BndOp_op1  = DiffusiveOperator([mu1(:);r1n(:)],[xi(:);-poles(:)],'Laplace',h1_an,'Type','Standard');
BndOpDelayed_op  = DiffusiveOperator([mu2(:);r2n(:)],[xi(:);-poles(:)],'Laplace',h2_an,'Type','Standard');