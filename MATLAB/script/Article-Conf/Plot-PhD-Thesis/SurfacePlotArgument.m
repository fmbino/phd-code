%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Surface plot of Argument
% Used in chapter 2 to illustrate cut and poles.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Function definition
    % -- Standard fractional derivative
f = @(s)sqrt(s);
Name = 'FracDer';
pole=[];
    % -- Bounded cut
f = @(s)sqrt(s).*sqrt(s+2).*sqrt(s+3);
Name = 'FracDerBoundedCut';
pole=[];
    % -- Bounded cut
a = 2;
f = @(s)1./(sqrt(s).*sqrt(s+a));
Name = sprintf('BoundedCut_a=%d',a);
    % -- Oscillatory-Diffusive
e = 5; % epsilon
b = 1;
rho = 1e-4;
rho = 5;
polesEq = @(s)1-rho*exp(-2*(e*sqrt(s)+b*s));
f = @(s)exp(-2*e*sqrt(s))./polesEq(s);
Name = sprintf('OD_e=%1.1e_b=%1.1e_rho=%1.1e',e,b,rho);

poles_x_search = [-30,1];
poles_y_search = [-20,20];
pole = computeZerosNumerical(polesEq,poles_x_search,poles_y_search,[1,1],'tol',1e-12);
residue = exp(-2*e*sqrt(pole(:)))./(2*b+e./sqrt(pole(:)));
%% Plot argument
x = linspace(-4,1,1e3);
y = linspace(-2.5,2.5,1e3);
x = linspace(-10,0,1e3);
y = linspace(-5,5,1e3);

[Sr,Si] = meshgrid(x,y); S = (Sr+1i*Si);
clf
Z = angle(f(S));
surf(Sr,Si,Z,'edgecolor','none');
colormap(gca,'hsv'); % Cyclic/Periodic colormap
colorbar
caxis([-pi pi]);
axis([min(x),max(x),min(y),max(y),-pi,pi])
xlabel('Real(s)')
ylabel('Imag(s)')
title('Arg(F(s)) (Cyclic colormap)');
view([0,90])
set(gca, 'DataAspectRatio', [1 1 1]);
hold all
%% Plot poles
clf
plot(real(pole),imag(pole),'x','markersize',10);
xlim([-40,1])
ylim([-20,20])
set(gca, 'DataAspectRatio', [1 1 1]);
%% Export to PNG file
colorbar('off')
dpi = 150; % 150dpi for laser printer,300 for high-quality 
filename=sprintf('PlotArgument-%s-x_%d_%d-y_%d_%d-dpi%d.png',Name,min(x),max(x),min(y),max(y),dpi);
        % Remove axis, ticks and labels
set(gca,'YTickLabel',[]);
set(gca,'XTickLabel',[]);
set(gca,'Title',[]);
set(gca,'YLabel',[]);
set(gca,'XLabel',[]);

view(0,90);
set(gca, 'DataAspectRatio', [1 1 1]);
set(gca, 'Position', get(gca, 'OuterPosition') - ...
get(gca, 'TightInset') * [-1 0 1 0; 0 -1 0 1; 0 0 1 0; 0 0 0 1]);
        % Export to png file, and crop
print(gcf, '-dpng', filename,sprintf('-r%d',dpi));
system(sprintf('convert %s -trim %s.crop',filename,filename));
system(sprintf('rm %s',filename));
system(sprintf('mv %s.crop %s',filename,filename));

%% Export poles  to file
filename=sprintf('Poles-%s.csv',Name);
namestr = sprintf('%s.csv',filename);
csvhead = 'Re,Im,ResMag';
dlmwrite(namestr,csvhead,'Delimiter','');
dlmwrite(namestr,[real(pole(:)),imag(pole(:)),abs(residue)],'-append','Delimiter',',','newline','unix','precision','%1.6e');