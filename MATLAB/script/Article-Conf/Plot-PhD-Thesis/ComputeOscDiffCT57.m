%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Chapter 2 - Approximation of CT57 liner model
%--------------------------------------------------------------------------
% Purpose: computation of oscillatory-diffusive representation of CT57
% liner model.
%   - Discretization is done on z
%   - No fit to experimental data (i.e. no nonlinear optimization)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Misc. function
z2beta = @(z)((z-1)./(z+1));
beta2z = @(beta)((1+beta)./(1-beta));
        % discrete oscillatory-diffusive representation
h_od = @(s,sn,rn,xik,muk)((get1stOrderUnitaryFilter(s,[-sn(:);xik(:)]))*[rn(:);muk(:)]);
%% Fluid quantities
% Air @15°C
rho0=1.225; % air density (kg^1m^-3)
nu = 1.48e-5; % kinematic viscosity (m^2/s)
gamma = 1.4; % ratio of spec heat constant (rad)
c0=340.27; % speed of sound (m/s)
Pr = 0.707; % Prandtl number (unsure) (rad)
alpha = nu/Pr; % thermal diffusivity (m^2/s)
%% Load experimental data
ExpData = ExpDataLoadNASAGIT2005CT57();
    % Choose experimental data
idx_SPL_source = 2;
idx_Mavg = 1;
Exp_name = ExpData{idx_Mavg,idx_SPL_source}.name;
Exp_freq = ExpData{idx_Mavg,idx_SPL_source}.freq*1000; % optimisation frequencies (Hz)
Exp_z = ExpData{idx_Mavg,idx_SPL_source}.Z_id; % impedance values (complex-valued)
%% NASA GFIT - Ceramic Tubular liner (CT57) [Jones2005]
% Normalized educed impedance on a CT57 liner from "Benchmark Data for 
%Evaluation of Aeroacoustic Propagation Codes with Grazing Flow".
% Itensity level: 130dB
% Eduction method: 2D Finite Element Method (2DFEM)
%                  Quasi-3D Finite Element Method (Q3DFEM)
% Assumptions: Plane wave (below cut-off frequency@3.3kHz)
% Liner description:
%   Surface porosity 57%
%   Cavity diameter 0.6mm
%   Cavity length 85.6mm
sigma = 57/100; % porosity
dc = 0.8*0.6e-3; % cavity diameter (m)
lc = 0.84*85.6e-3; % cavity length (m)
b = [0,(lc/c0)*(2/dc)*(1+(gamma-1)/sqrt(Pr))*sqrt(nu),lc/c0];
        % High-frequency Bruneau model
z_an = @(s)((1/sigma)*coth(b(1) + b(2)*sqrt(s) + b(3)*s));
beta_an = @(s)z2beta(z_an(s));
Model_name = sprintf('Bruneau-vanilla_b0=%1.2e_b12=%1.2e_b1=%1.2e',b(1),b(2),b(3));
        % Oscillatory-Diffusive represent1ation
        % h denotes the OD part of z
z_oscdiff = @(s,h)(1+2*exp(-2*(b(1)+b(3)*s)).*h)/sigma;
polesEq = @(s) 1-exp(-2*(b(1)+b(3)*s+b(2)*sqrt(s)));
h_an = @(s)exp(-2*b(2)*sqrt(s))./polesEq(s);
%% Compute oscillatory diffusive approximation
% We compute a discretization of h, i.e.
% poles sn,xi and weight rn,mu
    % -- Diffusive part 
Nxi = 2; xi_min = 2*pi*5e1; xi_max = 2*pi*1e4; % (rad/s) % Works nice
xi = logspace(log10(xi_min),log10(xi_max),Nxi); % Logarithmic pole spacing
    % -- Oscillatory part
            % compute poles (rad/s)
poles_x_search = 2*pi*[-3e3,0]; poles_y_search = 2*pi*[0,8e3];
sn = computeZerosNumerical(polesEq,poles_x_search,poles_y_search,[10,10],'tol',1e-15);
            % force hermitian symmetry (can create duplicate)
I = imag(sn)>0;
sn = [sn(:);conj(sn(I))];
            % remove duplicate
sn = removeDuplicate_Complex(sn,1e-3);
            % Remove 0 (branch point does not contribute here)
sn = sn(sn~=0);
            % sort pole by growing modulus
[~,I]=sort(abs(sn));
sn = sn(I); clear I
    % -- Compute discrete OD representation
    % First method: optimization of both rn and mu
w_optim = logspace(log10(min(xi)),log10(max(xi)),1e4); % angular frequencies for cost function
[mu,rn] = ComputeDiscreteDiffusiveRep_Optim_Std(xi,sn,w_optim,@(om)(om./om),@(s)h_an(s),0);
    % -- Compute discrete OD representation -> optim on diffusive only
    % Second method: rn computed from complex residue
    %                optimization on mu only
residue = computeComplexResidue(h_an,sn,'tol',1e-10);
rn = residue;
mu = ComputeDiscreteDiffusiveRep_Optim_Std(xi,[],w_optim,@(om)(om./om),@(s)h_an(s(:))-h_od(s(:),sn,rn,[],[]),0);
%% Plot
    % Plot frequencies (Hz)
freq = linspace(0e2,1.2e4,1e3);
figure(2)
clf
s = 1i*2*pi*freq;
subplot(2,2,1)
hold all
plot(freq,real(z_an(s)),'b');
%plot(freq,real(z_oscdiff(s,h_an(s))),'r--');
plot(freq,real(z_oscdiff(s(:),h_od(s(:),sn,rn,xi,mu))),'r--'); 
plot(Exp_freq,real(Exp_z),'kx');
plot(min(xi)*[1,1]/(2*pi),ylim,'--');
plot(max(xi)*[1,1]/(2*pi),ylim,'--');
xlabel('Frequency (Hz)');
ylabel('Normalized resistance (rad)');
grid on
%set(gca,'DataAspectRatio',[1,1,1])
ylim([0,5.5])
subplot(2,2,2)
hold all
plot(freq,imag(z_an(s)),'b');
plot(Exp_freq,imag(Exp_z),'kx');
plot(freq,imag(z_oscdiff(s(:),h_od(s(:),sn,rn,xi,mu))),'r--'); 
xlabel('Frequency (Hz)');
ylabel('Normalized reactance (rad)');
ylim([-2.5,2.5])
grid on
%set(gca,'DataAspectRatio',[1,1,1])
subplot(2,2,3)
hold all
plot(freq,abs(beta_an(s)),'b');
plot(Exp_freq,abs(z2beta(Exp_z)),'kx');
plot(freq,abs(z2beta(z_oscdiff(s(:),h_od(s(:),sn,rn,xi,mu)))),'r--'); 
grid on
xlabel('Frequency (Hz)');
ylabel('|reflection coefficient| (rad)');
%set(gca,'DataAspectRatio',[1,1,1]);
subplot(2,2,4)
hold all
plot(freq,rad2deg(angle(beta_an(s))),'b','DisplayName',sprintf('%s (Physical coefficient)',Model_name));
plot(Exp_freq,rad2deg(angle(z2beta(Exp_z))),'kx','DisplayName',sprintf('%s',Exp_name));
plot(freq,rad2deg(angle(z_oscdiff(s(:),h_od(s(:),sn,rn,xi,mu)))),'r--'); 
xlabel('Frequency (Hz)');
ylabel('phase(reflection coefficient) (deg)');
%% V Export curves to file: Experimental data
    % Experimental data
namestr = sprintf('%s_Expe.csv',Exp_name);
dlmwrite(namestr,'Freq(kHz),Re(z),Im(z),|beta|,phase(beta)(deg)','Delimiter','');
dlmwrite(namestr,[Exp_freq(:)/1e3,real(Exp_z(:)),imag(Exp_z(:)),abs(z2beta(Exp_z(:))),rad2deg(angle(z2beta(Exp_z(:))))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
%% V Export Impedance curves to file
    % Plot frequencies (Hz)
freq = linspace(0,1.2e4,200);
s = 1i*2*pi*freq;
    % -- Physical model
Z = z_an(s);
namestr = sprintf('%s_%s_PhysicalCoeff.csv',Exp_name,Model_name);
dlmwrite(namestr,'Freq(kHz),Re(z),Im(z),|beta|,phase(beta)(deg)','Delimiter','');
dlmwrite(namestr,[freq(:)/1e3,real(Z(:)),imag(Z(:)),abs(z2beta(Z(:))),rad2deg(angle(z2beta(Z(:))))],'-append','Delimiter',',','newline','unix','precision','%1.3e');
    % -- Oscillatory-Diffusive representation
Z = z_oscdiff(s(:),h_od(s(:),sn,rn,xi,mu));
namestr = sprintf('%s_%s_OscDiff-%dpoles-%ddiff.csv',Exp_name,Model_name,length(sn),length(xi));
dlmwrite(namestr,'Freq(kHz),Re(z),Im(z),|beta|,phase(beta)(deg)','Delimiter','');
dlmwrite(namestr,[freq(:)/1e3,real(Z(:)),imag(Z(:)),abs(z2beta(Z(:))),rad2deg(angle(z2beta(Z(:))))],'-append','Delimiter',',','newline','unix','precision','%1.3e');
%% Surface plot (to check location of poles of polesEq)
x = linspace(-4e3,1,2e3);
y = linspace(2e4,4e4,1e3);
[Sr,Si] = meshgrid(x,y);
S = (Sr+1i*Si);
Z = abs(1./polesEq(S));
clf
surf(Sr,Si,Z,'edgecolor','none');
view([0,90])
xlabel('Re(s)');
ylabel('Im(s)');
%caxis([0,1]);
colorbar