%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Chapter 1 - Cavity impedance model
%--------------------------------------------------------------------------
% Purpose: Plot some cavity impedance model, comparison with CT57 liner
% experimental data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Misc. function
z2beta = @(z)((z-1)./(z+1));
beta2z = @(beta)((1+beta)./(1-beta));
        % discrete oscillatory-diffusive representation
h_od = @(s,sn,rn,xik,muk)((get1stOrderUnitaryFilter(s,[-sn(:);xik(:)]))*[rn(:);muk(:)]);
%% Fluid quantities
% Air @15°C
rho0=1.225; % air density (kg^1m^-3)
nu = 1.48e-5; % kinematic viscosity (m^2/s)
gamma = 1.4; % ratio of spec heat constant (rad)
c0=340.27; % speed of sound (m/s)
Pr = 0.707; % Prandtl number (unsure) (rad)
alpha = nu/Pr; % thermal diffusivity (m^2/s)
%% Load experimental data
ExpData = ExpDataLoadNASAGIT2005CT57();
    % Choose experimental data
idx_SPL_source = 2;
idx_Mavg = 1;
Exp_name = ExpData{idx_Mavg,idx_SPL_source}.name;
Exp_freq = ExpData{idx_Mavg,idx_SPL_source}.freq*1000; % optimisation frequencies (Hz)
Exp_z = ExpData{idx_Mavg,idx_SPL_source}.Z_id; % impedance values (complex-valued)
%% NASA GFIT - Ceramic Tubular liner (CT57) [Jones2005]
% Normalized educed impedance on a CT57 liner from "Benchmark Data for 
%Evaluation of Aeroacoustic Propagation Codes with Grazing Flow".
% Itensity level: 130dB
% Eduction method: 2D Finite Element Method (2DFEM)
%                  Quasi-3D Finite Element Method (Q3DFEM)
% Assumptions: Plane wave (below cut-off frequency@3.3kHz)
% Liner description:
%   Surface porosity 57%
%   Cavity diameter 0.6mm
%   Cavity length 85.6mm
sigma = 57/100; % porosity
dc = 0.8*0.6e-3; % cavity diameter (m)
lc = 0.84*85.6e-3; % cavity length (m)
dc = 0.8*0.6e-3; % cavity diameter (m)
lc = 85.6e-3; % cavity length (m)
Dimension=sprintf('sigma=%4.2e_dc=%4.2e_lc=%4.2d',sigma*100,dc,lc);
        % Full Bruneau model
Phi = @(s)((2./s).*(besseli(1,s)./besseli(0,s)));
knu = @(s)sqrt(s/nu);
kalpha = @(s)sqrt(s/alpha);
z_Br = @(s)(1/sigma)*coth(s*(lc/c0).*sqrt((1+(gamma-1)*Phi(kalpha(s)*dc/2))./(1-Phi(knu(s)*dc/2))));
        % High-frequency Bruneau model
b = [0,(lc/c0)*(2/dc)*(1+(gamma-1)/sqrt(Pr))*sqrt(nu),lc/c0];
z_BrHigh = @(s)((1/sigma)*coth(b(1) + b(2)*sqrt(s) + b(3)*s));
        % Lossless model
z_lossless = @(s)(1/sigma)*coth(s*lc/c0);
%% Plot
    % Plot frequencies (Hz)
freq = linspace(0e2,1.2e4,1e3);
figure(2)
clf
s = 1i*2*pi*freq;
subplot(2,2,1)
hold all
plot(freq,real(z_Br(s)),'b');
plot(freq,real(z_BrHigh(s)),'r');
plot(freq,real(z_lossless(s)),'g');
plot(Exp_freq,real(Exp_z),'kx');
plot(min(xi)*[1,1]/(2*pi),ylim,'--');
plot(max(xi)*[1,1]/(2*pi),ylim,'--');
xlabel('Frequency (Hz)');
ylabel('Normalized resistance (rad)');
grid on
%set(gca,'DataAspectRatio',[1,1,1])
ylim([0,5.5])
subplot(2,2,2)
hold all
plot(freq,imag(z_Br(s)),'b');
plot(freq,imag(z_BrHigh(s)),'r');
plot(freq,imag(z_lossless(s)),'g');
plot(Exp_freq,imag(Exp_z),'kx');
xlabel('Frequency (Hz)');
ylabel('Normalized reactance (rad)');
ylim([-2.5,2.5])
grid on
%set(gca,'DataAspectRatio',[1,1,1])
subplot(2,2,3)
hold all
plot(freq,abs(z2beta(z_Br(s))),'b');
plot(freq,abs(z2beta((z_BrHigh(s)))),'r');
plot(freq,abs(z2beta((z_lossless(s)))),'g');
plot(Exp_freq,abs(z2beta(Exp_z)),'kx');
grid on
xlabel('Frequency (Hz)');
ylabel('|reflection coefficient| (rad)');
%set(gca,'DataAspectRatio',[1,1,1]);
subplot(2,2,4)
hold all
plot(freq,rad2deg(angle(z2beta(z_Br(s)))),'b');
plot(freq,rad2deg(angle(z2beta((z_BrHigh(s))))),'r');
plot(freq,rad2deg(angle(z2beta((z_lossless(s))))),'g');
plot(Exp_freq,rad2deg(angle(z2beta(Exp_z))),'kx','DisplayName',sprintf('%s',Exp_name));
xlabel('Frequency (Hz)');
ylabel('phase(reflection coefficient) (deg)');
%% V Export curves to file: Experimental data
    % Experimental data
namestr = sprintf('%s_Expe.csv',Exp_name);
dlmwrite(namestr,'Freq(kHz),Re(z),Im(z),|beta|,phase(beta)(deg)','Delimiter','');
dlmwrite(namestr,[Exp_freq(:)/1e3,real(Exp_z(:)),imag(Exp_z(:)),abs(z2beta(Exp_z(:))),rad2deg(angle(z2beta(Exp_z(:))))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
%% V Export Impedance curves to file
    % Plot frequencies (Hz)
freq = linspace(0,1.2e4,200);
s = 1i*2*pi*freq;
    % -- Physical model
Z = z_Br(s);
namestr = sprintf('%s_%s_Bruneau.csv',Exp_name,Dimension);
dlmwrite(namestr,'Freq(kHz),Re(z),Im(z),|beta|,phase(beta)(deg)','Delimiter','');
dlmwrite(namestr,[freq(:)/1e3,real(Z(:)),imag(Z(:)),abs(z2beta(Z(:))),rad2deg(angle(z2beta(Z(:))))],'-append','Delimiter',',','newline','unix','precision','%1.3e');
Z = z_BrHigh(s);
namestr = sprintf('%s_%s_BruneauHigh.csv',Exp_name,Dimension);
dlmwrite(namestr,'Freq(kHz),Re(z),Im(z),|beta|,phase(beta)(deg)','Delimiter','');
dlmwrite(namestr,[freq(:)/1e3,real(Z(:)),imag(Z(:)),abs(z2beta(Z(:))),rad2deg(angle(z2beta(Z(:))))],'-append','Delimiter',',','newline','unix','precision','%1.3e');
Z = z_lossless(s);
namestr = sprintf('%s_%s_Lossless.csv',Exp_name,Dimension);
dlmwrite(namestr,'Freq(kHz),Re(z),Im(z),|beta|,phase(beta)(deg)','Delimiter','');
dlmwrite(namestr,[freq(:)/1e3,real(Z(:)),imag(Z(:)),abs(z2beta(Z(:))),rad2deg(angle(z2beta(Z(:))))],'-append','Delimiter',',','newline','unix','precision','%1.3e');