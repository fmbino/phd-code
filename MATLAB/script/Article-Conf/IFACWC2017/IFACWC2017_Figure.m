%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spectrum of fractional delay differential equation
%%%
% Purpose: 1) Discretise the fractional delay differential equation
%  dx/dt(t) = A*x(t) + Btau*x(t-tau) + Bfrac*d^alpha*x(t-taufrac)
% as 
%                       dX/dt(t) = Ah*X(t)
% using the function 'FractionalDDE_CoupledSystem'.
% 2) Study the spectrum of Ah, to get insights into stability.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% I - Define equations and discretisation parameters
Nxi = 50; % No. of point to discretise fractional derivative
Np = 15; Nk=10; % No. of point to discretise delay
fprintf('Number of additional variable: Nxi=%d, N_tau=%d, N_tau*N_xi=%d\n',Nxi,Np*Nk,Nxi*Np*Nk);
    % Btau = 0
A_coupled_FDE = @(A,Bfrac,alpha,taufrac)(FractionalDDE_CoupledSystem(A,'FracOperator',struct('B',Bfrac,'alpha',alpha,'tau',taufrac),'DiscretisationParam',struct('Nxi',Nxi,'Np',Np,'Nk',Nk)));
    % Bfrac = 0
A_coupled_DDE = @(A,Btau,tau)(FractionalDDE_CoupledSystem(A,'DelayOperator',struct('B',Btau,'tau',tau),'DiscretisationParam',struct('Nxi',Nxi,'Np',Np,'Nk',Nk)));
    % Btau!=0 et Bfrac!=0
A_coupled_FDDE = @(A,Btau,tau,Bfrac,alpha,taufrac)(FractionalDDE_CoupledSystem(A,'DelayOperator',struct('B',Btau,'tau',tau),'FracOperator',struct('B',Bfrac,'alpha',alpha,'tau',taufrac),'DiscretisationParam',struct('Nxi',Nxi,'Np',Np,'Nk',Nk)));
%% Figure 2 - Vector-valued case (Theorem 8)
% dx/dt(t) = A*x(t) + Btau*x(t-tau) + Bfrac*d^alpha*x(t-taufrac)
A = (1/2)*[-3,1;1,-3];
Btau = (1/4)*[1,1;1,1];
tau = 10;
g=-2;
alpha = 0.5;
Bfrac = -g*eye(2);
taufrac = 0;
% --
Ah=A_coupled_FDDE(A,Btau,tau,Bfrac,alpha,taufrac);
tic
Spec = eig(full(Ah));
toc
figure(1)
[Rm,RmI] = max(real(Spec(:)));
plot(real(Spec),imag(Spec),'.','MarkerSize',15);
hc=get(legend(gca),'String'); % get legend from current axes.
legend(hc,sprintf('(-A=%1.2g |Btau|=|%1.2g| tau=%1.2g taufrac=%1.2g | \nN_{xi}=%d N_{tau}=%d | \nUnsPole:(%1.2g,%1.2g)',-max(real(eig(A))),sqrt(max(abs(eig(Btau'*Btau)))),tau,taufrac,Nxi,Np*Nk,real(Spec(RmI)),imag(Spec(RmI))));
legend('Location','eastoutside');
title('Spectrum: dx/dt(t) = A*x(t) + Btau*x(t-tau) + Bfrac*d^{alpha}*x(t-taufrac)');
xlabel('Re(s)');
ylabel('Im(s)');
xlim([-0.3,0.1]);
ylim([-20,20]);
%% Figure 2 - Export spectrum to file
    % Keep right part only
Spec = Spec(real(Spec)>(-0.3));
    % Remove essential part (diffusive)
Spec = Spec(imag(Spec)~=0);
    % Write to file
filename = sprintf('VectorValued_Spectrum_g%d.csv',g);
dlmwrite(filename,'Re,Im','Delimiter','');
dlmwrite(filename,[real(Spec(:)),imag(Spec(:))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
%% Figure 3 - Delay-dependent stability on Scalar case (Theorem 2)
% dx/dt(t) = A*x(t) + Btau*x(t-tau) + Bfrac*d^alpha*x(t)
A = -1;
Btau = abs(A)/2;
tau=linspace(1e-4,10,10);
alpha = 0.5;
theta_g = pi*linspace(0.5,1,1e2);
abs_g = [3/4,9/10,1,11/10,3/2]*abs(A);
% --
% We look for theta_max (value above which the FDDE is unstable)
% Theta_g_Max is a function of (A,Btau,tau,abs_g,alpha)
% Here, we only keep the (abs_g,tau) dependency.
Theta_g_Max = zeros(length(tau),length(abs_g));
for i=1:length(tau)
for j=1:length(abs_g)
        % find maximal acceptable value of theta
    Theta_g_Max(i,j) = pi; % all values stable
    k=1;
        % Last theta_g stable, and more theta_g to go
    while (Theta_g_Max(i,j)==pi) && (k<=length(theta_g))
        Ah=A_coupled_FDDE(A,Btau,tau(i),-abs_g(j)*exp(1i*theta_g(k)),alpha,0);
        %Spec = eigs(Ah,2,0);
        Spec = eig(full(Ah));  % costly
        [Rm,RmI] = max(real(Spec(:))); Growth_Ah = real(Spec(RmI));
        idx_unstable = find(Growth_Ah>0);
        if ~isempty(idx_unstable) % theta_g(k) unstable
            Theta_g_Max(i,j)=theta_g(k);
        end
        k=k+1;
    end
end
end
%% Figure 3 - Plot
figure
hold all
leg=cell(0);
for j=1:length(abs_g)
    plot(tau,Theta_g_Max(:,j)/pi,'-x');
    leg(end+1)={sprintf('|g|=%1.2g*|A|',abs_g(j)/abs(A))};
end
legend(leg);
ylim([0,1])
xlabel('tau');
ylabel('Theta_g_max/pi');
title(sprintf('A=%1.2e, B=%1.2e',A,Btau));
%% Figure 3 - Export curve to file
    % Write to file
for j=1:length(abs_g)
    filename = sprintf('ToyModel_ThetaMax_gA%1.2g.csv',abs_g(j)/abs(A));
    dlmwrite(filename,'tau,ThetagMax','Delimiter','');
    dlmwrite(filename,[tau(:),Theta_g_Max(:,j)],'-append','Delimiter',',','newline','unix','precision','%1.6e');
end
%% Figure 4 - Spectrum with composition
% dx/dt(t) = A*x(t) + Btau*x(t-tau) + Bfrac*d^alpha*x(t-taufrac)
A = -1;
Btau = abs(A)/2;
tau = 10;
g=2*abs(A);
alpha = 0.5;
Bfrac = -g;
taufrac = tau;
% --
Ah=A_coupled_FDDE(A,Btau,tau,Bfrac,alpha,taufrac);
Spec = eig(full(Ah));
[Rm,RmI] = max(real(Spec(:)));
figure
plot(real(Spec),imag(Spec),'r.','MarkerSize',15);
hc=get(legend(gca),'String'); % get legend from current axes.
legend(hc,sprintf('(-A=%1.2g |Btau|=|%1.2g| tau=%1.2g taufrac=%1.2g | \nN_{xi}=%d N_{tau}=%d | \nUnsPole:(%1.2g,%1.2g)',-max(real(eig(A))),sqrt(max(abs(eig(Btau'*Btau)))),tau,taufrac,Nxi,Np*Nk,real(Spec(RmI)),imag(Spec(RmI))));
legend('Location','eastoutside');
title('Spectrum: dx/dt(t) = A*x(t) + Btau*x(t-tau) + Bfrac*d^{alpha}*x(t-taufrac)');
xlabel('Re(s)');
ylabel('Im(s)');
xlim([-0.3,0.1]);
ylim([-10,10]);
%% Figure 4 - Export spectrum to file
    % Keep right part only
Spec = Spec(real(Spec)>(-0.3));
    % Remove essential part (diffusive)
Spec = Spec(imag(Spec)~=0);
    % Write to file
filename = sprintf('ToyModel_FracDelayed_Spectrum_gA%1.2g_taufrac%1.2g.csv',g/abs(A),taufrac);
dlmwrite(filename,'Re,Im','Delimiter','');
dlmwrite(filename,[real(Spec(:)),imag(Spec(:))],'-append','Delimiter',',','newline','unix','precision','%1.6e');