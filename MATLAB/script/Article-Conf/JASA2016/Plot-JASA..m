%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% JASA 2016
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Self-sufficient script

%% Fluid quantities
% Air @15°C
rho0=1.225; % air density (kg^1m^-3)
nu = 1.48e-5; % kinematic viscosity (m^2/s)
gamma = 1.4; % ratio of spec heat constant (rad)
c0=340.27; % speed of sound (m/s)
Pr = 0.707; % Prandtl number (unsure) (rad)
alpha = nu/Pr; % thermal diffusivity (m^2/s)
%% I - NASA GFIT - Ceramic Tubular liner (CT57) [Jones2005]
% Normalized educed impedance on a CT57 liner from "Benchmark Data for 
%Evaluation of Aeroacoustic Propagation Codes with Grazing Flow".
% Itensity level: 130dB
% Eduction method: 2D Finite Element Method (2DFEM)
%                  Quasi-3D Finite Element Method (Q3DFEM)
% Assumptions: Plane wave (below cut-off frequency@3.3kHz)
% Liner description:
%   Surface porosity 57%
%   Cavity diameter 0.6mm
%   Cavity length 85.6mm
sigma = 57/100;
dc = 0.6e-3;
lc = 85.6e-3;
% User input:
boolf = 1; % Plot in frequency (Hz) (=1) or |Stokes number| (rad) (=0)
%% 1 - Load &display experimental data
    % Data from [Dragna2016] PDF (Jones2005 with more points)
R_Dragna = importZdata('CT57-130dB-PDF-Dragna-Resistance.csv', 2, 27);
R_Dragna = R_Dragna(:,1:2);
I_Dragna = importZdata('CT57-130dB-PDF-Dragna-Reactance.csv', 2, 27);
I_Dragna = I_Dragna(:,1:2);
if boolf==0
    R_Dragna(:,1) = sqrt(2*pi*R_Dragna(:,1)/nu)*(dc/2);
    I_Dragna(:,1) = sqrt(2*pi*I_Dragna(:,1)/nu)*(dc/2);
elseif boolf==1
    R_Dragna(:,1) = R_Dragna(:,1)/1e3;
    I_Dragna(:,1) = I_Dragna(:,1)/1e3;
end
%% 2 - Compare to model (impedance)
w = 2*pi*linspace(0,1e4,2e2);
%--
if boolf==1
    fvar_title = sprintf('Frequency (kHz)');
    fvar = w/(2*pi*1e3);
    titl = sprintf('|Stokes number| from %1.3e to %1.3e',sqrt(min(w)/nu)*(dc/2),sqrt(max(w)/nu)*(dc/2));
else
    fvar_title = sprintf('|Stokes number| (rad)');
    fvar = sqrt(w/nu)*(dc/2);
    titl = sprintf('Frequency from %1.3e kHz to %1.3e kHz', min(w/(2*pi*1e3)),max(w/(2*pi*1e3)));
end
fig=clf;
%--
a0=0;
a1 = 0.85/c0;
aa = sqrt(nu)*(1/(c0*(dc/2)))*(1+(gamma-1)/sqrt(Pr));
Z=coth((a1*1i*w+aa*sqrt(1i*w))*lc)./sigma;
X = (a1*1i*w+aa*sqrt(1i*w))*lc;

lg=sprintf('kc Br. High (corrected)');
plotComplex(fig,fvar,Z,'-',lg);
dlmwrite('Fit-CT57_Frac-Model.csv','Freq,Re,Im','Delimiter','');
dlmwrite('Fit-CT57_Frac-Model.csv',[fvar(:),real(Z(:)),imag(Z(:))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
% Same formula as Z, but with an error in the delay
%dt= 0.95*2*a1*lc;
%s= 1i*w;
%Z2=1+2*exp(-dt*s).*(exp(-2*aa*lc*sqrt(s))./(1-exp(-2*(a1*s+aa*sqrt(s))*lc)));
%Z2=Z2./sigma;
%dlmwrite('Fit-CT57_Frac-Model-Error-Delay.csv','Freq,Re,Im','Delimiter','');
%dlmwrite('Fit-CT57_Frac-Model-Error-Delay.csv',[fvar(:),real(Z2(:)),imag(Z2(:))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
%--
a1 = 0.97*1/c0;
a0 = 0.97*4.66;
aa=0;
Z = coth((a1*1i*w+a0)*lc)./sigma;
lg=sprintf('EHR');
plotComplex(fig,fvar,Z,'-',lg);
dlmwrite('Fit-CT57_EHR-Model.csv','Freq,Re,Im','Delimiter','');
dlmwrite('Fit-CT57_EHR-Model.csv',[fvar(:),real(Z(:)),imag(Z(:))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
%--
subplot(1,2,1)
title(titl);
hc=get(legend(gca),'String'); % get legend from current axes.
plot(R_Dragna(:,1),R_Dragna(:,2),'ro');
legend(hc,sprintf('%s','CT57, M=0, 130dB'));
xlabel(fvar_title);
ylabel('Normalized resistance (rad)');
ylim([0,6]);
%xlim([0*500,10000]);
%set(gca,'DataAspectRatio',[1,1,1])
subplot(1,2,2)
plot(I_Dragna(:,1),I_Dragna(:,2),'ro');
xlabel(fvar_title);
ylabel('Normalized reactance (rad)');
ylim([-2.5,6])
%xlim([0*500,10000]);
%set(gca,'DataAspectRatio',[1,1,1])

%% II - [PLDIM] Cavity with fractional polynomial wavenumber
% Select name (depending on the coeffs. a0, aa, a1 loaded)
CavN = sprintf('Frac');
%CavN = sprintf('EHR');
%CavN = sprintf('LEE');
%%
    % H_an(s) = coth(j*k(s)*lc)/sigma where:
    % lc : cavity length (m)
    % sigma: porosity
    % j*k(s) : propagation wavenumber (m^-1) 
    %          with j*k(s)=a0+aa*s^alpha+a1*s (alpha in ]0,1[)
    %--
alpha = 1/2; % in ]0,1[
F_an = @(s)(exp(-2*aa*lc*s.^alpha)./(1-exp(-2*lc*(a0+a1*s+aa*s.^alpha))));
H_an = @(s,F)((1+2*exp(-2*(a0+a1*s)*lc).*F)/sigma);
    % Analytical weight, defined over xi>0
mu_an = @(xi)((1/pi)*exp(-2*lc*a1*xi).*sin(2*aa*lc*xi.^alpha)./(exp(-2*lc*a1*xi)-2*exp(-2*a0*lc).*cos(2*aa*lc*xi.^alpha)+exp(2*lc*(-2*a0+a1*xi))));
computePoles = @(N)(computePolesFPCavity([a0,aa,a1],lc,alpha,N));
computeRes = @(poles)(computeResFPCavity([a0,aa,a1],lc,alpha,poles));
name = sprintf('Cavity FPIM (a0,a1,aa,lc,alpha)=(%2.1g,%2.1g,%2.1g,%2.1g,%2.1g)',a0,a1,aa,lc,alpha);
%% Fig. - Poles and residues
N = 10;
poles = computePoles(N);
residual = computeRes(poles);
%dlmwriteHeader('Poles_Frac-Model.csv','Re,Im,ResMag',[real(poles(:))/(2*pi*1e3), imag(poles(:))/(2*pi*1e3), abs(residual(:))],'%1.6e');

%% Fig. Local and diffusive part of F
F_pc = @(s,sn,rn,xik,muk)((get1stOrderUnitaryFilter(s,[-sn(:);xik(:)]))*[rn(:);muk(:)]);
    % Diffusive part
xi_min = 1e-10; xi_max = 1e16; Nxi = 1e3;
    % Local part
Npoles=2e3; % Number of poles
%--
xik_interp = logspace(log10(xi_min),log10(xi_max),Nxi);
[muk_interp] = ComputeDiscreteDiffusiveRep_Interp(xik_interp,mu_an);
sn_interp = computePoles(Npoles);
rn_interp = computeRes(sn_interp);
clear xi_min xi_max Nxi Npoles
if sum(isnan(muk_interp)); error('muk_interp has Nans!'); end
if sum(isnan(rn_interp)); error('rn_interp has Nans!'); end

Np = 5e2;
w = 2*pi*transpose(linspace(1e-16,1e4,Np));
Z = H_an(1i*w,F_pc(1i*w,sn_interp,rn_interp,[],[]));
dlmwriteHeader('Analysis-Frac-Model_LocalPart.csv','Freq,Re,Im',[w/(2*pi*1e3), real(Z(:)), imag(Z(:))],'%1.6e');
Z = H_an(1i*w,F_pc(1i*w,[],[],xik_interp,muk_interp));
dlmwriteHeader('Analysis-Frac-Model_DiffPart.csv','Freq,Re,Im',[w/(2*pi*1e3), real(Z(:)), imag(Z(:))],'%1.6e');
%% Fig. Optimization
