%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spectrum of fractional delay differential equation
%%%
% Purpose: 1) Discretise the fractional delay differential equation
%  dx/dt(t) = A*x(t) + Btau*x(t-tau) + Bfrac*d^alpha*x(t-taufrac)
% as 
%                       dX/dt(t) = Ah*X(t)
% using the function 'FractionalDDE_CoupledSystem'.
% 2) Study the spectrum of Ah, to get insights into stability.
% Rmk: adapted from IFAC WC 2017.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% I - Define equations and discretisation parameters
% dx/dt(t) = A*x(t) + Btau*x(t-tau) + Bfrac*d^alpha*x(t-taufrac)
% This is the equation studied in IFAC WC 2017, Figure 2.
A = (1/2)*[-3,1;1,-3];
Btau = (1/4)*[1,1;1,1];
tau = 10;
g=2; % stable for g>0
alpha = 1/2;
alpha = 3/8;
Bfrac = -g*eye(2);
taufrac = 0*tau;
    % Delay discretization
Np = 80; Nk=1;
%% II - Definition of discretization methods
discM=cell(0);
    % -- Quadrature method
A_fun = @(Nxi)FractionalDDE_CoupledSystem(A,'DelayOperator',struct('B',Btau,'tau',tau),'FracOperator',struct('B',Bfrac,'alpha',alpha,'tau',taufrac),'DiscretisationParam',struct('Nxi',Nxi,'Np',Np,'Nk',Nk),'QuadMethod','GL');
discM{end+1}=struct('name',sprintf('Quad-min'),'fun',A_fun);
%A_fun = @(Nxi)FractionalDDE_CoupledSystem(A,'DelayOperator',struct('B',Btau,'tau',tau),'FracOperator',struct('B',Bfrac,'alpha',alpha,'tau',taufrac),'DiscretisationParam',struct('Nxi',Nxi,'Np',Np,'Nk',Nk),'QuadMethod','BK');
%discM{end+1}=struct('name',sprintf('Quad-BK'),'fun',A_fun);
    % -- Optimization method
        % Unconstrained
xi_min_log = 1e-15; xi_max_log=1e4;
A_fun = @(Nxi)FractionalDDE_CoupledSystem(A,'DelayOperator',struct('B',Btau,'tau',tau),'FracOperator',struct('B',Bfrac,'alpha',alpha,'tau',taufrac),'DiscretisationParam',struct('Nxi',Nxi,'Np',Np,'Nk',Nk),'OptimParam',struct('ximin',xi_min_log,'ximax',xi_max_log,'nonnegConstraint',0));
%discM{end+1}=struct('name',sprintf('Opt-Std-Log-ximin=%.3e-ximax=%.3e',xi_min_log,xi_max_log),'fun',A_fun);
        % Nonnegativity constraints 
%xi_min_log = 1e-5; xi_max_log=1e2;
A_fun = @(Nxi)FractionalDDE_CoupledSystem(A,'DelayOperator',struct('B',Btau,'tau',tau),'FracOperator',struct('B',Bfrac,'alpha',alpha,'tau',taufrac),'DiscretisationParam',struct('Nxi',Nxi,'Np',Np,'Nk',Nk),'OptimParam',struct('ximin',xi_min_log,'ximax',xi_max_log,'nonnegConstraint',1));
discM{end+1}=struct('name',sprintf('Opt-Std-Log-CST-ximin=%.3e-ximax=%.3e',xi_min_log,xi_max_log),'fun',A_fun);
    % Spectrum of coupled system
Nxi = 50; % No. of point to discretise fractional derivative
figure(1)
clf
hold all
leg = cell(0);
SpecCell = cell(0); % spectrum
for i=1:length(discM) % for each discretization method
    for j=1:length(Nxi)
            % build matrix of coupled system
        tic
        Spec = eig(full(discM{i}.fun(Nxi(j))));
%        Spec = eigs(discM{i}.fun(Nxi(j)),2*Nxi(j),'sm',struct('tol',1e-2));
        SpecCell{i,j} = Spec;
        toc
        [Rm,RmI] = max(real(Spec(:)));
        plot(real(Spec),imag(Spec),'.','MarkerSize',15);
        leg{end+1}= sprintf('%s-Nxi=%d\nUnsPole:(%1.2g,%1.2g)',discM{i}.name,Nxi(j),real(Spec(RmI)),imag(Spec(RmI)));
    end
end
legend(leg);
legend('Location','eastoutside');
title(sprintf('Spectrum: dx/dt(t) = A*x(t) + Btau*x(t-tau) + Bfrac*d^{alpha}*x(t-taufrac)\nS(A)=%1.2g |Btau|=|%1.2g| alpha=%1.2g tau=%1.2g taufrac=%1.2g | N_{tau}=%d',max(real(eig(A))),sqrt(max(abs(eig(Btau'*Btau)))),alpha,tau,taufrac,Np*Nk));
xlabel('Re(s)');
ylabel('Im(s)');
    % Zoomed-out view
xlim([-0.3,1e-2]);
ylim([-6,6]);
    % Zoomed-out view (Bessel)
% xlim([-0.3,0.1]);
% ylim([-5,5]);
    % Zoomed-in view (Bessel)
% xlim(1e-3*[-1,1]);
% ylim([-1-1e-1,1+1e-1]);
    % Zommed-in view: current one in paper
% xlim(1e-2*[-1,1]);
% ylim(1e-2*[-1,1]);
    % Zoomed-in view: strong for constrained optim
xlim(1e-5*[-1,1]);
ylim(1e-5*[-1,1]);
%% Export spectrum to file
for i=1:length(discM) % for each discretization method
    for j=1:length(Nxi)
        % Compute max pole
    [Rm,RmI] = max(real(SpecCell{i,j}));
            % Keep right part only
    Spec = SpecCell{i,j}(real(SpecCell{i,j})>(-0.3));
        % Remove essential part (diffusive)
    %Spec = Spec(imag(Spec)~=0);
        % Write to file
   filename = sprintf('SpectrumFDDE-g=%.3e-tau=%.3e-taufrac=%.3e-alpha=%.3e_Np=%d-Nk=%d_%s-Nxi=%d_MaxPole=(%.3e,%.3e).csv',g,tau,taufrac,1-alpha,Np,Nk,discM{i}.name,Nxi(j),real(SpecCell{i,j}(RmI)),imag(SpecCell{i,j}(RmI)));
   dlmwrite(filename,'Re,Im','Delimiter','');
   dlmwrite(filename,[real(Spec(:)),imag(Spec(:))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
    end
end