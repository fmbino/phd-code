%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Discrete diffusive representation using Gauss numerical quadrature
% Made for the SDS 2018 talk on discretization of diffusive representation.
% This file builds and compares the following methods:
%   - Quadrature method
%   - Optimization
%% Definition of transfer function
% The transfer function is described by the following.
%   h_fun    analytical expression of the target function
%   h_ext   'extended diffusive' expression
%   h_std   'standard diffusive' expression
%   h_isext boolean that indicates wether h_fun=h_ext
%   (needed for error computation, h must be non-singular on iR)
% + h_std_time (if available) time-domain expression of h_std
    % -- Fractional integral of order 0<alpha<1
h_fun = @(alpha,s)s.^(-alpha);
h_ext = @(alpha,s)s.^(1-alpha);
h_std = h_fun;
h_isext = 0;
mu_an=@(alpha,xi)sin((alpha)*pi)./(pi*xi.^(alpha));
Y_alpha = @(t,alpha) 1/gamma(alpha)*t.^(alpha-1);     % Fractional kernel
h_std_time = Y_alpha;
    % -- Fractional derivative of order 0<alpha<1
h_fun = @(alpha,s)(s).^(alpha);
h_ext = h_fun;
h_std = @(alpha,s)(s).^(alpha-1);
h_isext = 1;
mu_an=@(alpha,xi)sin((1-alpha)*pi)./(pi*xi.^(1-alpha));
Y_alpha = @(t,alpha) 1/gamma(alpha)*t.^(alpha-1);     % Fractional kernel
h_std_time = @(t,alpha)Y_alpha(t,1-alpha);
    % -- Bounded cut [0,10]
h_fun = @(alpha,s)(s.^(-alpha)).*((s+10).^(-alpha));
h_ext = @(alpha,s)s.*h_fun(alpha,s);
h_std = h_fun;
h_isext = 0;
h_std_time = [];
    % -- Fractional reflection coefficient (No oscillatory part)
a = [1,1,0];
h_fun = @(alpha,s)1./(1+a(1)+a(2)*s.^alpha+a(3)*s);
h_ext = @(alpha,s)s.*h_fun(alpha,s);
h_std = h_fun;
h_isext = 0;
h_std_time = [];
    % -- Bessel function
h_fun = @(alpha,s)1./(sqrt(s-1i).*sqrt(s+1i)); % do not write sqrt(s.^2+1)
h_ext = @(alpha,s)s.*h_fun(alpha,s);
h_std = h_fun;
h_isext = 0;
h_std_time = @(t,alpha)besselj(0,t);
    % -- Function from [Helie06]
    % Oscillatory diffusive weight, does not work well
h_fun = @(alpha,s)exp(-s.^alpha)./s.^alpha;
h_ext = @(alpha,s)s.*h_fun(alpha,s);
h_std = h_fun;
h_isext = 0;
mu_an=@(alpha,xi)real((h_fun(alpha,xi*exp(-1i*pi))-h_fun(alpha,xi*exp(1i*pi)))/(2*1i*pi));
h_std_time = [];
    % -- Other fractional
a = [1,0.1,1];    
h_fun = @(alpha,s)1./(a(1)+a(2)*s.^(alpha)+a(3)*s);
h_ext = @(alpha,s)s.*h_fun(alpha,s);
h_std = h_fun;
h_isext = 0;
h_std_time = [];
%% Diffusive weight
        % Analytical expression of diffusive weight
mu_an=@(alpha,xi)(((h_std(alpha,xi*exp(-1i*pi))-h_std(alpha,xi*exp(1i*pi)))/(2*1i*pi)));
clf
xi = linspace(0,10,1e2);
plot(xi,mu_an(0.5,xi));
xlabel('xi (rad/s)');
title('Diffusive weight mu');
%% Definition of discretization methods
% Each discretization method is described by a structure:
%   -'name'
%   -'fun' Discretization method
%       Input:
%           N (number of nodes)
%           alpha
%           w (range for approximation)
%       Output: 
%           xi,mu (column vectors)
discM=cell(0);
    %==============================================
    % -- Quadrature - Using Shampine change of variables
%discM{1}=struct('name','IFAC','fun',@(alpha,N,w)ComputeDiscreteDiffusiveRep_QuadratureGL(@(xi)mu_an(alpha,xi),N));
    %==============================================
    % -- Quadrature - Using analyzed change of variables
%[n,d]=rat(1/2,1e-2); % rational approximation of irrational number
% beta = @(alpha)1/d; % best choice for fractional operator
% discM{1}=struct('name',sprintf('Quad-beta=1|%d',d),'fun',@(alpha,N,w)ComputeDiscreteDiffusiveRep_QuadratureGL_Alt(@(xi)mu_an(alpha,xi),N,'CoVParam',beta(alpha)));
beta = @(alpha)min(alpha,1-alpha); % best choice for fractional operator
discM{1}=struct('name',sprintf('Quad-min'),'fun',@(alpha,N,w)ComputeDiscreteDiffusiveRep_QuadratureGL_Alt(@(xi)mu_an(alpha,xi),N,'CoVParam',beta(alpha)));
%beta = @(alpha)max(alpha,1-alpha); % best choice for fractional operator
%discM{3}=struct('name',sprintf('Quad-max'),'fun',@(alpha,N,w)ComputeDiscreteDiffusiveRep_QuadratureGL_Alt(@(xi)mu_an(alpha,xi),N,'CoVParam',beta(alpha)));
%beta = @(alpha)0.99*min(alpha,1-alpha); % best choice for fractional operator
%discM{4}=struct('name',sprintf('Quad-minx0.99'),'fun',@(alpha,N,w)ComputeDiscreteDiffusiveRep_QuadratureGL_Alt(@(xi)mu_an(alpha,xi),N,'CoVParam',beta(alpha)));
%beta = @(alpha)1.01*min(alpha,1-alpha); % best choice for fractional operator
%discM{5}=struct('name',sprintf('Quad-minx1.01'),'fun',@(alpha,N,w)ComputeDiscreteDiffusiveRep_QuadratureGL_Alt(@(xi)mu_an(alpha,xi),N,'CoVParam',beta(alpha)));
    %==============================================
    % -- Quadrature from [Birk and Song 2010]
%discM{2}=struct('name',sprintf('Quad-BirkSong'),'fun',@(alpha,N,w)ComputeDiscreteDiffusiveRep_Quadrature_BirkSong(N,alpha,'BirkSong'));
%discM{3}=struct('name',sprintf('Quad-Diethelm'),'fun',@(alpha,N,w)ComputeDiscreteDiffusiveRep_Quadrature_BirkSong(N,alpha,'Diethelm'));
%discM{4}=struct('name',sprintf('Quad-YA'),'fun',@(alpha,N,w)ComputeDiscreteDiffusiveRep_Quadrature_BirkSong(N,alpha,'YA'));
    %==============================================
    % -- Linear Optimization
    % Rmk: Optimizing weights at quadrature nodes does not work well at all.
xi_min_log = 1e-14; % if logarithmic pole repartition
xi_min_lin = 1e-3; % if linear pole repartition
w_optim = @(xi_optim,N,w)logspace(log10(min(xi_optim(N,w))),log10(max(xi_optim(N,w))),1e4);
        %  Standard diff. rep. & log pole repartition
xi_optim = @(N,w,xi_min)logspace(log10(xi_min),log10(max(w)),N); % Distribution of poles
g = @(alpha,N,w,xi_min) ComputeDiscreteDiffusiveRep_Optim_StdWrapper(xi_optim(N,w,xi_min),[],w_optim(@(N,w)xi_optim(N,w,xi_min),N,w),@(om)(om./om),@(s)h_std(alpha,s),0);
%discM{2}=struct('name',sprintf('Opt-Std-Log-ximin=%.3e',xi_min_log),'fun',@(alpha,N,w)g(alpha,N,w,xi_min_log));
%xi_min_log = 1e-14; % if logarithmic pole repartition
%discM{3}=struct('name',sprintf('Opt-Std-Log-ximin=%.3e',xi_min_log),'fun',@(alpha,N,w)g(alpha,N,w,xi_min_log));
%xi_min_log = 1e-10; % if logarithmic pole repartition
%discM{4}=struct('name',sprintf('Opt-Std-Log-ximin=%.3e',xi_min_log),'fun',@(alpha,N,w)g(alpha,N,w,xi_min_log));
%xi_min_log = 1e-6; % if logarithmic pole repartition
%discM{5}=struct('name',sprintf('Opt-Std-Log-ximin=%.3e',xi_min_log),'fun',@(alpha,N,w)g(alpha,N,w,xi_min_log));
        %  Extended diff. rep. & log pole repartition
% xi_optim = @(N,w)logspace(log10(xi_min_log),log10(max(w)),N);
% g = @(alpha,N,w) ComputeDiscreteDiffusiveRep_Optim_ExtWrapper(xi_optim(N,w),[],w_optim(xi_optim,N,w),@(om)(om./om),@(s)h_ext(alpha,s),0);
% discM{1}=struct('name',sprintf('Opt.Ext.Log. xi_{min}=%1.2g',xi_min_log),'fun',g);
        % Standard diff. rep. & linear pole repartition
% xi_optim = @(N,w)linspace(xi_min_lin,max(w),N);
% g = @(alpha,N,w) ComputeDiscreteDiffusiveRep_Optim_StdWrapper(xi_optim(N,w),[],w_optim(xi_optim,N,w),@(om)(om./om),@(s)h_std(alpha,s),0);
% discM{1}=struct('name',sprintf('Opt-Std-Lin-ximin=%.3e',xi_min_lin),'fun',g);
        %  Extended diff. rep. &  linear pole repartition
%xi_optim = @(N,w)linspace(xi_min_lin,max(w),N);
%g = @(alpha,N,w) ComputeDiscreteDiffusiveRep_Optim_ExtWrapper(xi_optim(N,w),[],w_optim(xi_optim,N,w),@(om)(om./om),@(s)h_ext(alpha,s),0);
%discM{1}=struct('name',sprintf('Opt-Ext-Lin-ximin=%.3e',xi_min_lin),'fun',g);
        %==============================================
        % -- Nonlinear optimization
Tol=1e-15; K=1e4; % Tolerance and number of points in cost function
w_optim = @(xi_optim,N,w)logspace(log10(min(xi_optim(N,w))),log10(max(xi_optim(N,w))),K);
            % First linear optimization, then nonlinear
g = @(alpha,N,w,xi_min) ComputeDiscreteDiffusiveRep_Optim_StdNonLinear(xi_optim(N,w,xi_min),w_optim(@(N,w)xi_optim(N,w,xi_min),N,w),@(s)h_std(alpha,s),'Tol',Tol);
%discM{3}=struct('name',sprintf('Opt-Std-Log-ximin=%.3e-NL-N_w=%1.2g-Tol=%1.1e',xi_min_log,K,Tol),'fun',@(alpha,N,w)g(alpha,N,w,xi_min_log));
        %==============================================
        % -- Quadrature then nonlinear optimization
beta = @(alpha)min(alpha,1-alpha); % best choice for fractional operator
                % Optimization on standard formulation
%discM{2}=struct('name',sprintf('Quad-Min-Opt-Std-Log-NL-Tol=%1.3e-K=%d',Tol,K),'fun',@(alpha,N,w)ComputeDiscreteDiffusiveRep_Quadrature(@(xi)mu_an(alpha,xi),N,'CoVParam',beta(alpha),'Optim',struct('Tol',Tol,'K',K,'Method','NonLinOptimStd','h_an',@(s)h_std(alpha,s))));
                % Optimization on standard normalized formulation
%discM{3}=struct('name',sprintf('Quad-Min-Opt-StdNorm-Log-NL-Tol=%1.3e-K=%d',Tol,K),'fun',@(alpha,N,w)ComputeDiscreteDiffusiveRep_Quadrature(@(xi)mu_an(alpha,xi),N,'CoVParam',beta(alpha),'Optim',struct('Tol',Tol,'K',K,'Method','NonLinOptimStdNorm','h_an',@(s)h_std(alpha,s))));
                % Optimization on extended formulation
discM{2}=struct('name',sprintf('Quad-Min-Opt-Ext-Log-NL-Tol=%1.3e-K=%d',Tol,K),'fun',@(alpha,N,w)ComputeDiscreteDiffusiveRep_Quadrature(@(xi)mu_an(alpha,xi),N,'CoVParam',beta(alpha),'Optim',struct('Tol',Tol,'K',K,'Method','NonLinOptimExt','h_an',@(s)h_std(alpha,s))));
                % Optimization on reflection coefficient formulation
%discM{2}=struct('name',sprintf('Quad-Min-Opt-Refl-Log-NL-Tol=%1.3e-K=%d',Tol,K),'fun',@(alpha,N,w)ComputeDiscreteDiffusiveRep_Quadrature(@(xi)mu_an(alpha,xi),N,'CoVParam',beta(alpha),'Optim',struct('Tol',Tol,'K',K,'Method','NonLinOptimRefl','h_an',@(s)h_std(alpha,s))));
                % Optimization on reflection coefficient formulation
                % (normalized)
%discM{2}=struct('name',sprintf('Quad-Min-Opt-ReflNorm-Log-NL-Tol=%1.3e-K=%d',Tol,K),'fun',@(alpha,N,w)ComputeDiscreteDiffusiveRep_Quadrature(@(xi)mu_an(alpha,xi),N,'CoVParam',beta(alpha),'Optim',struct('Tol',Tol,'K',K,'Method','NonLinOptimReflNorm','h_an',@(s)h_std(alpha,s))));
    % -- Quadrature - Using analyzed change of variables (Bessel J0 ONLY)
%discM=cell(0);
%discM{1}=struct('name','SDS-J_{0}','fun',@(alpha,N,w)ComputeDiscreteDiffusiveRep_QuadratureGL_BesselJ0(N));
%% Compute convergence rate
% Compute error for each method, alpha, and number of nodes
alpha = 1/2; % orders of fractional derivative
w_max = 1e4;
w = linspace(-w_max,w_max,1e4); % interval of approximation, omega (rad/s)
T = [1e-1,1e4]; % Time interval for error computation
N = unique(floor(logspace(log10(1),log10(500),200)));
N = 1:500; % used for paper graphs
N = 1:50; % used for paper graphs
    % value of max(xi)
ximax = zeros(-length(N),length(discM),length(alpha));
    % Error tensors
        % Frequency-domain errors
errFreq_Linf = zeros(length(N),length(discM),length(alpha)); % ||h - h_disc||_L^inf
errFreq_L2 = zeros(length(N),length(discM),length(alpha)); % ||h - h_disc||_L^2
        % Time-domain errors
errTime_ptwise = zeros(length(N),length(discM),length(alpha)); % |h-h_disc|(T2)
errTime_L1 = zeros(length(N),length(discM),length(alpha)); % ||h-h_disc||_L^1(T1,T2)
errTime_L1_diff = zeros(length(N),length(discM),length(alpha)); % | ||h||_L^1(0,T2) - ||h_disc||_L^1(0,T2) |
    % -- Time-domain error
h_disc_time = @(t,xi,mu)transpose(mu(:))*exp(-kron(xi,t));
    % Rmk: beware the use of 'integral', check accuracy.
    % Relax tolerance if needed
AbsTol = 5e-2; RelTol = 1e-3;
errTime_ptwise_fun = @(t,xi,mu,alpha)h_std_time(t,alpha)-h_disc_time(t,xi,mu); % pointwise error
errTime_L1_fun = @(T,xi,mu,alpha)integral(@(t)abs(errTime_ptwise_fun(t,xi,mu,alpha)),T(1),T(2),'AbsTol',AbsTol,'RelTol',RelTol)/integral(@(t)h_std_time(t,alpha),T(1),T(2));
errTime_L1_sec_fun = @(T,xi,mu,alpha)abs(integral(@(t)h_std_time(t,alpha)-h_disc_time(t,xi,mu),0,T,'AbsTol',AbsTol,'RelTol',RelTol))/integral(@(t)h_std_time(t,alpha),0,T);
    % -- For fractional derivative only
        % Avoid using inte  gral, direct analytical expression
        % However, its evaluation is associated with significant error
        % 'integral' with a looser tolerance is usually more accurate.
        % Avoid computing the integral of Y_(1-alpha), which is infinite.
%errTime_L1_sec_fun = @(T,xi,mu,alpha)abs(integral(@(t)h_std_time(t,alpha)-h_disc_time(t,xi,mu),0,T,'AbsTol',AbsTol,'RelTol',RelTol))/(T.^(1-alpha)/((1-alpha)*gamma(1-alpha)));
%errTime_L1_fun = @(T,xi,mu,alpha)integral(@(t)abs(errTime_ptwise_fun(t,xi,mu,alpha)),T(1),T(2),'AbsTol',AbsTol,'RelTol',RelTol)/((T(2)^(1-alpha)-T(1)^(1-alpha))/((1-alpha)*gamma(1-alpha)));
%errTime_L1_sec_fun = @(T,xi,mu,alpha)abs(T.^(1-alpha)/((1-alpha)*gamma(1-alpha)) - transpose((mu(:)./xi(:)))*(ones(length(xi),length(T))-exp(-kron(xi(:),T))))./(T.^(1-alpha)/((1-alpha)*gamma(1-alpha)));
    % -- For fractional integral only (used in paper)
errTime_L1_sec_fun = @(T,xi,mu,alpha)abs(integral(@(t)h_std_time(t,alpha)-h_disc_time(t,xi,mu),0,T,'AbsTol',AbsTol,'RelTol',RelTol))/(T.^(alpha)/((alpha)*gamma(alpha)));
errTime_L1_fun = @(T,xi,mu,alpha)integral(@(t)abs(errTime_ptwise_fun(t,xi,mu,alpha)),T(1),T(2),'AbsTol',AbsTol,'RelTol',RelTol)/((T(2)^(alpha)-T(1)^(alpha))/((alpha)*gamma(alpha)));
for i=1:length(N) % for each number of nodes
    fprintf('Start of N=%d/%d (%d left)...\n',N(i),max(N),length(N)-i);
    for j=1:length(discM) % for each method
        for k=1:length(alpha) % for each alpha
            [xi,mu] = discM{j}.fun(alpha(k),N(i),w); % compute [xi,mu]
            ximax(i,j,k) = max(xi);
            h_ex = @(w)h_ext(alpha(k),1i*w); % Exact extended function
            h_disc = sum(get1stOrderUnitaryFilter(1i*w,xi,'type','high-pass')*mu(:),2);
                % (Meaningful) Relative error
            errFreq_Linf(i,j,k) = max(abs(1 - h_disc(:)./h_ex(w(:))));
                % Absolute error (on extended representation)
            errFreq_L2(i,j,k) = sqrt(mean( abs(h_ex(w(:))-h_disc(:)).^2 ));
                % Relative error
            errFreq_L2(i,j,k) = errFreq_L2(i,j,k)/sqrt(mean( abs(h_ex(w(:))).^2 ));
                % Relative error in time domain (if possible)
            if ~isempty(h_std_time)
                errTime_ptwise(i,j,k)=abs(errTime_ptwise_fun(T(2),xi,mu,alpha(k)))/abs(h_std_time(T(2),alpha(k)));
                errTime_L1(i,j,k)=errTime_L1_fun(T,xi,mu,alpha(k));
                errTime_L1_diff(i,j,k)=errTime_L1_sec_fun(T(2),xi,mu,alpha(k));
            end
        end
    end
end
%% Plot one discrete diffusive representation
% Plot the lastly computed diffusive representation
w = linspace(0,w_max,1e2); % interval of approximation, omega (rad/s)
w = logspace(log10(min(xi)/10),log10(10*w_max),1e2); % interval of approximation, omega (rad/s)
h_ex = @(w)h_fun(alpha(k),1i*w); % Exact function
    % Compute h_disc
if h_isext % h_fun is extended
    h_disc = sum(get1stOrderUnitaryFilter(1i*w,xi,'type','high-pass')*mu(:),2);
else % h_fun is standard
    h_disc = sum(get1stOrderUnitaryFilter(1i*w,xi,'type','low-pass')*mu(:),2);
end
figure(3)
clf
subplot(3,2,1)
%hold on
loglog(w,real(h_disc),'DisplayName',sprintf('%s-ximax=%1.1e-N=%d',discM{end}.name,max(w),N(end)));
hold on
loglog(w,real(h_ex(w(:))),'DisplayName','h_{ex}');
loglog(xi,xi./xi,'x');
title('Re(h(iw))');
%legend show
%ylim([0,10])
subplot(3,2,2)
loglog(w,imag(h_disc));
hold on
loglog(w,imag(h_ex(w(:))))
xlabel('w');
title('Im(h(iw))');
subplot(3,2,3)
hold on
plot(w,real(h_disc),'DisplayName',sprintf('%s-ximax=%1.1e-N=%d',discM{end}.name,max(w),N(end)));
plot(w,real(h_ex(w(:))),'DisplayName','h_{ex}');
plot(xi,xi./xi,'x');
title('Re(h(iw))');
xlim([0,1e4])
%legend show
%ylim([0,10])
subplot(3,2,4)
hold on
plot(w,imag(h_disc));
plot(w,imag(h_ex(w(:))))
xlabel('w');
title('Im(h(iw))');
subplot(3,2,5)
hold on
plot(w,abs(1- h_ex(w(:))./h_disc),'DisplayName','h_{ex}');
title('|1-h_{ex}/h_{disc}|(iw)');
if ~isempty(h_std_time)
    subplot(3,2,6)
    hold on
    t = linspace(1e-3,1e1,1e2);
%     plot(log10(t),log10(h_disc_time(t,xi,mu)));
%     plot(log10(t),log10(h_std_time(t,alpha)));
    plot(t,h_disc_time(t,xi,mu));
    plot(t,h_std_time(t,alpha));
%     plot(t,log10(h_disc_time(t,xi,mu)));
%     plot(t,log10(h_std_time(t,alpha)));
    xlabel('t');
    title('h(t)');
    ylim([0,10])
end
%% Export to file
    % Frequency domain
csvhead = sprintf('w,R(hex(w)),R(hdisc(w)),Im(hex(w)),Im(hdisc(w))');
namestr = sprintf('DiffRep_Alpha=%.3e_ximax=%.3e_N=%d_%s_KernelFreq.csv',alpha(k),max(xi),N(i),discM{j}.name);
dlmwrite(namestr,csvhead,'Delimiter','');
dlmwrite(namestr,[w(:),real(h_ex(w(:))),real(h_disc(:)),imag(h_ex(w(:))),imag(h_disc(:))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
    % Time domain
csvhead = sprintf('t,hex,hdisc');
namestr = sprintf('DiffRep_Alpha=%.3e_ximax=%.3e_N=%d_%s_KernelTime.csv',alpha(k),max(xi),N(i),discM{j}.name);
dlmwrite(namestr,csvhead,'Delimiter','');
dlmwrite(namestr,[t(:),h_std_time(t,alpha(k))',h_disc_time(t,xi,mu)'],'-append','Delimiter',',','newline','unix','precision','%1.6e');
    % Pole and weight
csvhead = sprintf('mu,xi,Re(hex(xi)),Im(hex(xi))');
namestr = sprintf('DiffRep_Alpha=%.3e_ximax=%.3e_N=%d_%s_mu-xi.csv',alpha(k),max(xi),N(i),discM{j}.name);
dlmwrite(namestr,csvhead,'Delimiter','');
dlmwrite(namestr,[mu(:),xi(:),real(h_ex(xi(:))),imag(h_ex(xi(:)))],'-append','Delimiter',',','newline','unix','precision','%1.6e');
%% Plot convergence rate
    % -- Frequency domain
figure
clf
subplot(3,1,1)
hold on
leg = cell(0);
xlabel('# of variables');
ylabel('eps');
title(sprintf('||1-h_{disc}(w)/h_{ex}||_{inf} on (%1.2g,%1.2g) rad/s',min(w),max(w)));
for k=1:size(errFreq_Linf,3) % for each alpha
    for j=1:size(errFreq_Linf,2) % for each method
        plot(N,errFreq_Linf(:,j,k));
        stencil = 4; % length of stencil
        slope = computeSlope(log10(N),log10(errFreq_Linf(:,j,k)),stencil); slope = min(slope(~isinf(slope)));
        leg{end+1}=sprintf('%s\n%1.2g/Slope=%1.2g',discM{j}.name,alpha(k),slope);
    end
end
set(gca, 'XTick',0:5:max(N));
set(gca, 'YScale', 'log');
set(gca, 'XScale', 'log');
set(gca,'XMinorTick','on','YMinorTick','on');
legend(leg,'Location','eastoutside');
%set(gca, 'DataAspectRatio', [1 1 1])
grid on
%ylim([-10,3])
subplot(3,1,2)
title(sprintf('||h_{ex}(w)-h_{disc}||_{2}/||h_{ex}||_{2}'));
hold on
leg=cell(0);
for k=1:size(errFreq_L2,3) % for each alpha
    for j=1:size(errFreq_L2,2) % for each method
        plot(N,errFreq_L2(:,j,k));       
        stencil = 4; % length of stencil
        slope = computeSlope(log10(N),log10(errFreq_L2(:,j,k)),stencil); slope = min(slope(~isinf(slope)));
        leg{end+1}=sprintf('%s\n%1.2g/Slope=%1.2g',discM{j}.name,alpha(k),slope);
    end
end
set(gca, 'XTick',0:5:max(N));
set(gca, 'YScale', 'log');
set(gca, 'XScale', 'log');
set(gca,'XMinorTick','on','YMinorTick','on');
legend(leg,'Location','eastoutside');
%set(gca, 'DataAspectRatio', [1 1 1])
grid on
%ylim([-10,3])
subplot(3,1,3)
title(sprintf('xi_{max}'));
hold on
leg=cell(0);
for k=1:size(errFreq_L2,3) % for each alpha
    for j=1:size(errFreq_L2,2) % for each method
        plot(N,ximax(:,j,k));       
        stencil = 4; % length of stencil
        slope = computeSlope(log10(N),log10(ximax(:,j,k)),stencil); slope = max(slope(~isinf(slope)));
        leg{end+1}=sprintf('%s\n%1.2g/Slope=%1.2g',discM{j}.name,alpha(k),slope);
    end
end
legend(leg,'Location','eastoutside');
set(gca, 'XTick',0:5:max(N));
set(gca, 'YScale', 'log');
set(gca, 'XScale', 'log');
set(gca,'XMinorTick','on','YMinorTick','on');
%set(gca, 'DataAspectRatio', [1 1 1])
grid on
%ylim([-10,3])
    % -- Time-domain plot (if available)
if ~isempty(h_std_time)    
figure
clf
subplot(3,1,1)
title(sprintf('|h_{ex}-h_{disc}|/|h_{ex}| at T=%1.2g s',T(2)));
hold on
leg=cell(0);
for k=1:size(errTime_L1,3) % for each alpha
    for j=1:size(errTime_L1,2) % for each method
        plot(N,errTime_ptwise(:,j,k));
        stencil = 4; % length of stencil
        slope = computeSlope(log10(N),log10(errTime_ptwise(:,j,k)),stencil); slope = min(slope(~isinf(slope)));
        leg{end+1}=sprintf('%s\n%1.2g/Slope=%1.2g',discM{j}.name,alpha(k),slope);
    end
end
set(gca, 'XTick',0:5:max(N));
set(gca, 'YScale', 'log');
set(gca, 'XScale', 'log');
set(gca,'XMinorTick','on','YMinorTick','on');
legend(leg,'Location','eastoutside');
%set(gca, 'DataAspectRatio', [1 1 1])
grid on
subplot(3,1,2)
title(sprintf('| ||h_{ex}||_{1}-||h_{disc}||_{1} |/||h_{ex}||_{1} on (0,%1.2g s)',T(2)));
hold on
leg=cell(0);
for k=1:size(errTime_L1,3) % for each alpha
    for j=1:size(errTime_L1,2) % for each method
        plot(N,errTime_L1_diff(:,j,k));
        stencil = 4; % length of stencil
        slope = computeSlope(log10(N),log10(errTime_L1_diff(:,j,k)),stencil); slope = min(slope(~isinf(slope)));
        leg{end+1}=sprintf('%s\n%1.2g/Max. slope=%1.2g',discM{j}.name,alpha(k),slope);
    end
end
set(gca, 'XTick',0:5:max(N));
set(gca, 'YScale', 'log');
set(gca, 'XScale', 'log');
set(gca,'XMinorTick','on','YMinorTick','on');
legend(leg,'Location','eastoutside');
%set(gca, 'DataAspectRatio', [1 1 1])
grid on
subplot(3,1,3)
title(sprintf('||h_{ex} - h_{disc}||_{1}/||h_{ex}||_{1} on (%1.2g,%1.2g s)',T(1),T(2)));
hold on
leg=cell(0);
for k=1:size(errTime_L1,3) % for each alpha
    for j=1:size(errTime_L1,2) % for each method
        plot(N,errTime_L1(:,j,k));
        stencil = 4; % length of stencil
        slope = computeSlope(log10(N),log10(errTime_L1(:,j,k)),stencil); slope = min(slope(~isinf(slope)));
        leg{end+1}=sprintf('%s\n%1.2g/Max. slope=%1.2g',discM{j}.name,alpha(k),slope);
    end
end
set(gca, 'XTick',0:5:max(N));
set(gca, 'YScale', 'log');
set(gca, 'XScale', 'log');
set(gca,'XMinorTick','on','YMinorTick','on');
legend(leg,'Location','eastoutside');
%set(gca, 'DataAspectRatio', [1 1 1])
grid on
end
%% Export to CSV file   
% - FName contains frequency and time used in error measuremments
% CSV file with six columns:
%   N,errFreq_Linf, errFreq_L2, errTime_ptwise, errTime_L1_diff, errTime_L1
csvhead = sprintf('N,errFreq_Linf,errFreq_L2,errTime_ptwise,errTime_L1_diff,errTime_L1,ximax');
for k=1:length(alpha)
    for j=1:length(discM)
        namestr = sprintf('Error_Alpha=%.3e_Wmax=%.3e_T1=%.3e_T2=%.3e_Nmax=%d_%s.csv',alpha(k),max(w),T(1),T(2),max(N),discM{j}.name);
        dlmwrite(namestr,csvhead,'Delimiter','');
        dlmwrite(namestr,[N(:),errFreq_Linf(:,j,k),errFreq_L2(:,j,k),errTime_ptwise(:,j,k),errTime_L1_diff(:,j,k),errTime_L1(:,j,k),ximax(:,j,k)],'-append','Delimiter',',','newline','unix','precision','%1.6e');
    end
end