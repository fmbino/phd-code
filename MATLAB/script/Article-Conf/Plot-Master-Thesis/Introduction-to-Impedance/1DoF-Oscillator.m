%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Plot related to the 1DoF oscillator
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Energy absorption
% u: nondimensionalized plusation w/w0 (rad)
% Q: quality factor (rad)
% w0: undamped resonant pulsation (rad/s)

w1=8e-1;
w2=1^2/w1;
n1=30;
n2=70;
    % Definition of impedance Z and absorption A
Z = @(u,a,Q)(a+1i*(a*Q)*u-1i*(a*Q)*(1./u));
A = @(u,a,Q)(real(Z(u,a,Q))./(2*(abs(Z(u,a,Q))).^2));
u = [logspace(log10(1e-1),log10(w1),n1),logspace(log10(w1),log10(w2),n2),logspace(log10(w2),log10(1e1),n1)];

    % Plot the three curves
clf
semilogx(u,A(u,1,5),'b');
hold all
semilogx(u,A(u,1,1),'r');
semilogx(u,A(u,5,1),'g');
legend(sprintf('{\\alpha=1 Q=5}'),sprintf('{\\alpha=1 Q=1}'),sprintf('{\\alpha=5 Q=1}'));
title(sprintf('Absorption spectrum'));
ylabel(sprintf('$\\frac{R(z)}{2|z|^2}$'));
xlabel(sprintf('$\\nicefrac{\\omega}{\\omega_0}$'));
    % Plot the pulsation range (for the definition of Q)
y=A(u,1,5)>(0.92*max(A(u,1,5))/2);
ind=find(y);
semilogx(u(ind),max(A(u(ind),1,5))/2*y(ind),'--b');
y=A(u,1,1)>(0.92*max(A(u,1,1))/2);
ind=find(y);
semilogx(u(ind),max(A(u(ind),1,1))/2*y(ind),'--r');
y=A(u,5,1)>(0.92*max(A(u,5,1))/2);
ind=find(y);
semilogx(u(ind),max(A(u(ind),5,1))/2*y(ind),'--g');