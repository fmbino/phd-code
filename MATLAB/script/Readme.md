This folder contains various scripts, used throughout the thesis. The scripts are gathered by topic. See the corresponding Readme for more details. Scripts of interest are highlighted below.

# Folder structure

`Acoustics`: Scripts related to analytical formulas encountered in acoustics. It can be ignored.
    
`Article-Conf`: Scripts used to produce figures for articles and conferences.

- `ComputeOscDiffCT57.m`: This script demonstrates the discretization of the oscillatory-diffusive representation (by two methods, with and without computing the residues). It has been used to produce Figure 2.11 of the thesis. By adding a nonlinear optimization against experimental data (as done in `DiffusiveRepresentation_GIT2005_CT57`), this script can be used to compute a TDIBC. *This is the simplest script to get started quickly.*

`DiffusiveRepresentation`: Scripts related to oscillatory-diffusive representations.
    
- `Generic analysis discretisation plot (Template)`: This folder demonstrates the analysis and discretization of a transfer function. This script is generic on purpose.
    - `0.Definition`: Various transfer functions are defined.
    - `1.Analytical`: Investigates whether a given transfer functions has an OD representation, without any discretization. The conditions of the main representation theorem of Chapter 2 are partly verified by the `identifyDiffusiveRep` function.
    - `2.Discretization`: Discretization of the OD representation.
    - `3.Plot`: Various way of plotting the results.

- `Specific`: This folder contains scripts that compute a TDIBC for a given material. The scripts `DiffusiveRepresentation_GFIT2013_Mp` and `DiffusiveRepresentation_GIT2005_CT57` have been used for [10.1016/j.jcp.2018.08.037](https://doi.org/10.1016/j.jcp.2018.08.037). They demonstrate the three steps of the discretization
    - (i) Finding the parameters for the physical models.
    - (ii) Discretization of the OD representation.
    - (iii) Nonlinear fit against experimental data. (Should be improved using vector fitting.)
        
`FDE`: Small scripts related to fractional differential equations.
    
`Impedance-Models`: Scripts that plot various impedance models.

`Misc`: Misc. scripts, can be ignored.

`ODE-DDE`: Scripts related to the solution of ODE and DDE. These scripts demonstrate the use of a continuous Runge-Kutta method (CRK) for solving a Delay Differential Equation (DDE), as well as the computattion of the continuous extension of a Runge-Kutta method.