%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hyperbolic velocity profile
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% From (Khamis & Brambley, 2017) Eq. (2.4)

M = 0.475;
delta = 0.2;
tau = 0.104;
T0 = 0;
%--
N = 1e2;
delta = 1/delta;
r = linspace(0,1,N);
U = @(r)M*tanh((1-r)*delta)+M*(1-tanh(delta)).*(delta*r.*(1+tanh(delta))+1+r).*(1-r);
Mean = integral(U,0,1);
T = T0 + tau*(cosh((1-r)*delta)).^(-1);

subplot(2,1,1)
plot(r,U(r));
title(sprintf('Velocity profile (M=%1.2g,delta=%1.2g,tau=%1.2g)',M,1/delta,tau));
xlabel('r');
ylabel('U');
subplot(2,1,2)
plot(r,T);
title(sprintf('Temperature profile (T0=%1.2g)',T0));
xlabel('r');
ylabel('T');