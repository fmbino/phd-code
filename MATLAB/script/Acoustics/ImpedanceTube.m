%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Impedance tube - Harmonic Analytical solution
%%%
%  (->)--------------|||
%   x=0              x=L
%   Source          Impedance z, reflexion coeff. beta
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -- Inputs
    % air
rho0=1.293; % air density (kg/m3)
c0=340.29; % speed of sound (m/s)
z0 = rho0*c0;
    % tube
L = 1; % Length of the impedance tube (m)
k = @(w)(w/c0);
beta = @(w)(exp(1i*(-2*k(w)*L))); % Wall reflexion coefficient
    % input pressure wave
f = 1e4; % frequency 
Ai = 1; % amplitude
    % Plot param
t=1;
N = 1e6;
% --
x = linspace(0,L,N);
p_i = @(t,x)(Ai*exp(1i*(2*pi*f*t-k(2*pi*f)*x)));
p_r = @(t,x)(beta(2*pi*f)*Ai*exp(1i*(2*pi*f*t+k(2*pi*f)*x)));
u_i = @(t,x)(p_i(t,x)/z0);
u_r = @(t,x)(-p_r(t,x)/z0);

close all
figure
hold all
plot(x,real(p_i(t,x)+p_r(t,x)));
plot(x,real(u_i(t,x)+u_r(t,x)));
plot([L,L],linspace(-Ai,Ai,2),'LineWidth',3);
legend('Pressure','Velocity');
xlabel('x (m)');
ylabel('Pressure (Pa) & Velocity (m/s)');