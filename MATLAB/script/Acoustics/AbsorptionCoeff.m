%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Absorption coefficients
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Closed pipe as a side branch
rS = 1; % ratio S/Sb (rad)
lc = 1; % tube length (m)
alpha_r = @(rS,lambda)((abs(1+2*rS*z_cav(2*pi./lambda,lc))).^(-2)); % reflexion coefficient

n1 = 10;
n2 = 14;
n3 = 15;
n4 = 10;
    % two peaks at 4/3 and 4
lam = lc*[linspace(1,1.5,n1),linspace(1.5,3.8,n2),linspace(3.8,4.20,n3),linspace(4.20,6,n4)];
clf
hold all
leg=cell(0);
plot(lam/lc,alpha_r(2,lam));
leg{end+1}=sprintf('S/Sb = 2');
plot(lam/lc,alpha_r(5,lam));
leg{end+1}=sprintf('S/Sb = 5');
legend(leg);
xlabel('lambda/lc');