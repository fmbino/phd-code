%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Duct acoustics - Impedance wall
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This scripts analyses the dispersion relation
%               D(w,k,Zac)=0
% for duct acoustics. In particular, Briggs-Bers analysis.
%% A. Base field
% Common to all treatment
% With Ingard-Myers, avoid critical layer w=M*k
M = 0.5; % Mach number at center
delta = 1e-4; % hydrodynamic boundary layer thickness
    % Hyperbolic velocity profile (Khamis & Brambley, 2017) Eq. (2.4)
U = @(r)(M*tanh((1-r)/delta)+M*(1-tanh(1/delta)).*((1/delta)*r.*(1+tanh(1/delta))+1+r).*(1-r));
dU = @(r)(M*(tanh(1/delta) - 1)*(r + (r*(tanh(1/delta) + 1))/delta + 1) + (M*(tanh((r - 1)/delta).^2 - 1))/delta + M*((tanh(1/delta) + 1)/delta + 1)*(tanh(1/delta) - 1)*(r - 1));
Rho = @(r)(ones(size(r))); % density profile
%% B. Effective impedance law 
    % 0-th order Ingard-Myers
Zeff = @(w,k,Zac)(Zac.*w./(w-M*k));
dZeff_dk = @(w,k,Zac)(M*Zac.*w./(w-M*k)^2);
    % 1-st order Ingard-Myers (Khamis & Brambley, 2016) Eq. (2.9)
d0 = integral(@(r)(1-Rho(r)),0,1);
d1 = integral(@(r)(1-Rho(r).*U(r)/M),0,1);
dk = integral(@(r)(1-Rho(r).*U(r).^2/M^2),0,1);
ds = -M/(Rho(1)*dU(1)); % (Brambley 2013, Eq. 4c)
%I1 = @(w,k)(ds*M*k./w); % Approximation, quadrature-free
I1 = @(w,k)(integral(@(r)(1-(w-M*k)^2./(w-U(r)*k).^2./Rho(r)),0,1,'AbsTol',1e-3,'RelTol',1e-3));
dI1_dk = @(w,k)(2*M*(1-I1(w,k))/(w-M*k)-2/(w-M*k)*integral(@(r)(U(r).*(w-M*k).^3./(w-U(r)*k).^3),0,1,'AbsTol',1e-3,'RelTol',1e-3));
Zeff = @(w,k,Zac,m)((w./(w-M*k)) .* (Zac+(d0*w.^2-2*M*d1*w.*k+dk*M^2*k.^2)./(1i*w)) ./ (1+Zac.*1i*w.*I1(w,k).*(k.^2+m^2)./(w-M*k).^2));
dZeff_dk = @(w,k,Zac,m)(((- 2*dk*k*M^2 + 2*d1*w*M)*1i)/(((Zac*w*I1(w,k)*(k^2 + m^2)*1i)/(w - M*k)^2 + 1)*(w - M*k)) + (M*w*(Zac - ((dk*M^2*k^2 - 2*d1*M*k*w + d0*w^2)*1i)/w))/(((Zac*w*I1(w,k)*(k^2 + m^2)*1i)/(w - M*k)^2 + 1)*(w - M*k)^2) - (w*(Zac - ((dk*M^2*k^2 - 2*d1*M*k*w + d0*w^2)*1i)/w)*((Zac*k*w*I1(w,k)*2i)/(w - M*k)^2 + (Zac*w*dI1_dk(w,k)*(k^2 + m^2)*1i)/(w - M*k)^2 + (M*Zac*w*I1(w,k)*(k^2 + m^2)*2i)/(w - M*k)^3))/((1 + (Zac*w*I1(w,k)*(k^2 + m^2)*1i)/(w - M*k)^2)^2*(w - M*k)));
%% C. Dispersion relation
D = @(w,k,m,Zac)(dispRel_CylDuctAcoustics(k,m,w,Zeff(w,k,Zac,m),M));
%% I - w and Z fixed. Solve on k
w = 5;
m = 0;
Zac = 1.0 + 1i*linspace(-5,5,2e1);
kmin = -(8+15*1i);
kmax = 8 + 15*1i;
% --
[k0r,k0i] = meshgrid(linspace(real(kmin),real(kmax),5),linspace(imag(kmin),imag(kmax),5));
k0 = k0r + 1i*k0i; k0= k0(:); k0 = permute(k0,[3,2,1]); clear k0r k0i
options=optimoptions(@fsolve,'Display','none','Jacobian','off','DerivativeCheck','off','TolFun',eps);
km = zeros(length(k0),length(Zac));
for i=1:length(Zac)
    kc = fsolveLoop(@(k)(D(w,k,m,Zac(i))),k0,options);
    km(:,i) = permute(kc,[3,2,1]);
end
clear kc
%% Plot modes
clf
hold on
title(sprintf('Duct Acoustics (M=%1.2g,m=%d,w=%1.2g,Z_{ac}=%1.2g+1i*%1.2g)',M,m,w,real(Zac(1)),imag(Zac(1))));
xlabel('Re(k)');
ylabel('Im(k)'); leg=cell(0);
xlim([-8,8])
ylim([-15,15])
col = rand(size(km,2),3);
for i=1:size(km,2) % for each parameter value
    plot(km(:,i),'.','color','k','LineWidth',3,'MarkerSize',20);
end
plot([0,0],ylim,'k--');
plot(xlim,[0,0],'k--');

%% II k fixed, solve on w
k = linspace(0,150,20);
m = 12;
R = 3; d=0.15; b=1.15;h=0;
Zac = @(w,k)(R + 1i*d*w+b./(1i*w)+h*k.^(1.10));
wmin = -10-10*1i;
wmax = 10-1*1i;
% --
[w0r,w0i] = meshgrid(linspace(real(wmin),real(wmax),5),linspace(imag(wmin),imag(wmax),5));
w0 = w0r + 1i*w0i; w0= permute(w0(:),[3,2,1]);
clear w0r w0i
options=optimoptions(@fsolve,'Display','none','Jacobian','off','DerivativeCheck','off','TolFun',eps);
wm = zeros(length(w0),length(k));
for i=1:length(k)
     wc = fsolveLoop(@(w)(D(w,k(i),m,Zac(w,k(i)))),w0,options);
     wm(:,i) = permute(wc,[3,2,1]);
end
%% Plot modes
clf
hold on
title(sprintf('Duct Acoustics (M=%1.2g,m=%d,k=%1.2g,Z=%1.2g+1i*%1.2g)',M,m,k,real(Zac(1,1)),imag(Zac(1,1))));
xlabel('Re(w)');
ylabel('Im(w)'); leg=cell(0);
%xlim([-10,10])
%ylim([-150,1])
col = rand(size(wm,2),3);
for i=1:size(wm,2) % for each parameter value
    plot(wm(:,i),'.','color','k','LineWidth',3,'MarkerSize',20);
    %leg(end+1)={'Real(km)>=0'};
    %plot(-km(:,i),'x');
    %leg(end+1)={'Real(km)<=0 (deduced)'};    
end
plot([0,0],ylim,'k--');
plot(xlim,[0,0],'k--');
xlim([-150,150])
ylim([-25,45])

%% Plot temporal growth rate
%clf
hold on
title(sprintf('Duct Acoustics - Myers - Growth rate (M=%1.2g,m=%d)',M,m));
xlabel('k');
ylabel('Min(Im(w(k)))'); leg=cell(0);
%xlim([-10,10])
%ylim([-150,1])
plot(k,min(imag(wm)),'-o');
plot(xlim,[0,0],'k--');

%% III - Compute double root (ks,ws)
% Computing the double root of the dispersion relation requires to compute
% dD/dk. Overall, the optimisation is way more costly.
m = 24;
R = 3; d=0.15; b=1.15;h=0;
Zac = @(w,k)(R + 1i*d*w+b./(1i*w)+h*k.^(1.10));
wmin = -5+0.1i;
wmax = 5+0.5i;
kmin = -(1e2 + 1e3*1i);
kmax = (1e3 + 1e3*1i);
% --
[w0r,w0i] = meshgrid(linspace(real(wmin),real(wmax),2),linspace(imag(wmin),imag(wmax),2));
w0 = w0r + 1i*w0i; w0= w0(:);
clear w0r w0i
[k0r,k0i] = meshgrid(linspace(real(kmin),real(kmax),25),linspace(imag(kmin),imag(kmax),25));
k0 = k0r + 1i*k0i; k0= k0(:);
clear k0r k0i
z0 = [repelem(k0(:),length(w0)),repmat(w0(:),[length(k0),1])];
z0 = reshape(z0,1,size(z0,2),[]);
clear k0 w0
fprintf('Double root search: %d initial points in CxC\n',length(z0));
options=optimoptions(@fsolve,'Display','none','Jacobian','off','DerivativeCheck','off','TolFun',eps);
    % z = [k,w] (couple of complex numbers)
D = @(w,k,m,Zac)(dispRel_CylDuctAcoustics(k,m,w,Zeff(w,k,Zac,m),M,dZeff_dk(w,k,Zac,m)));
f = @(z)(D(z(2),z(1),m,Zac(z(2),z(1))));
zm = fsolveLoop(f,z0,options);
zm = permute(zm,[3,2,1]);

%% Plot double roots
title('Double root ()');
xlabel('Real(w)');
ylabel('Imag(w)');
clf
plot(real(zm(:,2)),imag(zm(:,2)),'o');
%xlim(real([wmin,wmax]))
%ylim(imag([wmin,wmax]))
xlim([-150,150])
ylim([-25,45])
%% Double root (symbolic)
clear all
syms k w M k m r Zac d0 d1 dk z U rho
    % Psi function (m=0 case)
% Psi(z) = -besselj(0,z)./(z.*besselj(1,z));
% dPsi = diff(Psi,z);
    % Psi function (m!=0 case)
% Psi(z) = ((1/m)*(besselj(m-1,z)+besselj(m+1,z))./(besselj(m-1,z)-besselj(m+1,z)));
% dPsi = diff(Psi,z);
    % Effective impedance law
I0(k) = w^2*d0 -2*w*k*M*d1 + k^2*M^2*dk;
I1 = sym('I1(k)'); 
Zeff = (w/(w-M*k))*(Zac+I0(k)/(1i*w))/(1+1i*w*Zac*(k^2+m^2)*I1/(w-M*k)^2);
dZeff = diff(Zeff,k);
        % I1 integral
g(k) = 1-(w-M*k)^2/(rho*(w-U*k)^2);
diff(g,k)

%% Plot an acoustic mode
alpha = 50 + 1i*1e1;
m = 20;
theta = linspace(0,2*pi,1e3);
r = linspace(0,1,1e2);
pfun = @(r,theta)(besselj(m,alpha*r).*exp(-1i*m*theta));

[R TH] = meshgrid(r,theta);
Z = real(pfun(R,TH));
clf
surf(R.*cos(TH), R.*sin(TH), Z,'EdgeColor','none');
colorbar
%caxis([-10,10])
xlabel('x/R');
ylabel('y/R');
title(sprintf('Acoustic mode m=%d, alpha=%1.2g+1i*%1.2g',m,real(alpha),imag(alpha)));
view([0,90])