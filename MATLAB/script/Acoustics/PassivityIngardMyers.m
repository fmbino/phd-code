%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Passivity check for Ingard-Myers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

a0 = 0; a1 = 0; a2 = 1; a3 = 1e4;
z = @(s,k)(a0 + a1*s + (1./s).*(a2+a3*k.^4)); % Acoustic impedance
Z_eff = @(z,s,k)((s./(s-M*1i*k)).*z); % Effective Impedance (Myers)

M = 0;
k = 0.1;

sr = linspace(1,2,1e3);
si = linspace(-2,2,1e3);
[Sr,Si] = meshgrid(sr,si);
S = Sr+1i*Si;
clf
surf(Sr,Si,real(Z_eff(z(S,k),S,k)),'edgecolor','none');
hold on
%plot(w,min(w./M,max(k)),'r');
% TODO: add plot of the dispersion relation s(k)
colorbar
caxis([-1,1])
%axis([min(x),max(x),min(y),max(y),-zmax,zmax])
xlabel('Re(s)')
ylabel('Im(s)')
title(sprintf('Real(Z_eff(s)), with k=%1.2g',k));
view([0,90])

