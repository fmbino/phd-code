%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definition of base flow velocity model
% --
% Flow are defined in [-1,1] with variable r.
% Dimensional variable is y in [a,b]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Change of variable
    % r in [-1,1], y in [a,b]
y2r = @(y,a,b)2/(b-a)*(y-(b+a)/2); y2r_dy = @(a,b)2/(b-a);
r2y = @(r,a,b) (b+a)/2 + (b-a)/2*r; r2y_dr = @(a,b)(b-a)/2;
    % -- Poiseuille flow
M0_Poi = @(r,Mavg)Mavg*(3/2)*(1-r.^2);
dM0_dr_Poi = @(r,Mavg)Mavg*(3/2)*(-2*r);
    % -- Hyperbolic velocity profile
    % From (Khamis & Brambley, 2017) Eq.(2.4a)
    % Profile re-expressed as a function of average Mach and delta.
        % centerline Mach = f(Mavg,delta)
Mc_fun = @(Mavg,delta)Mavg/(delta*log(cosh(1/delta)) + (1-tanh(1/delta))*((1+tanh(1/delta))/(6*delta)+2/3));
M0_Hy = @(r,Mc,delta)Mc*tanh((1-abs(r))*(1/delta))+Mc*(1-tanh((1/delta))).*((1/delta)*abs(r).*(1+tanh((1/delta)))+1+abs(r)).*(1-abs(r));
        % derivative
dtanh = @(x)1-tanh(x).^2;
dM0_dr = @(r,Mc,delta)-Mc/delta*sign(r).*dtanh((1-abs(r))*(1/delta))+Mc*(1-tanh((1/delta))).*((1/delta)*(1+tanh((1/delta)))+1).*sign(r).*(1-abs(r))+Mc*(1-tanh((1/delta))).*((1/delta)*abs(r).*(1+tanh((1/delta)))+1+abs(r)).*(-sign(r));
        % as function of average Mach and delta
M0_Hy = @(r,Mavg,delta)M0_Hy(r,Mc_fun(Mavg,delta),delta);
dM0_dr_Hy = @(r,Mavg,delta)dM0_dr(r,Mc_fun(Mavg,delta),delta);
    % -- Barycenter of hyperbolic law and Poiseuille
M0_Av_Hy = @(r,Mavg,delta,alpha)(1-alpha)*M0_Hy(r,Mavg,delta)+alpha*M0_Poi(r,Mavg);
dM0_dr_Av_Hy = @(r,Mavg,delta,alpha)(1-alpha)*dM0_dr_Hy(r,Mavg,delta) + alpha*dM0_dr_Poi(r,Mavg);
    % -- Corrected Power law
    % Eq.(23) from "Analytical Velocity Profile in tube for laminar and
    % turbulent flow' (Stigler, 2014)
    % N=1: laminar velocity profile
    % N>1: turbulent velocity profile
    % r in [-1,1], y in [a,b]
M0_Pw = @(r,Mavg,N)Mavg*(N+2)/(N+1)*(1-abs(r).^(N+1));
dM0_dr_Pw = @(r,Mavg,N)(Mavg*(N+2)/(N+1)*(N+1)*(-sign(r).*abs(r).^N));
    % -- Barycenter of Corrected power law and Poiseuille
M0_Av = @(r,Mavg,N,alpha)(1-alpha)*M0_Pw(r,Mavg,N)+alpha*M0_Poi(r,Mavg);
dM0_dr_Av = @(r,Mavg,N,alpha)(1-alpha)*dM0_dr_Pw(r,Mavg,N) + alpha*dM0_dr_Poi(r,Mavg);
    % -- Eddy viscosity model from (Marx&Auregan, JSV2013) Eq.(3&4)
    % Almost identical to hyperbolic law
Re = 10; % Reynolds number based on friction velocity
k = 0.42; % von Karman constant
A = 25.4; % constant
nu = 15.11e-6; % air kinematic viscosity @20°C
    % eddy viscosity model
    % r in [-1,1]
nu_t = @(r,k,Re,A)0.5 + 0.5*( 1 + (k^2*Re^2/9)*(1-r.^2).^2 .* (1+2*r.^2).^2 .* (1-exp((abs(r)-1)*Re/A)).^2 );
dM0_dr_Edv = @(r,Re,nu)-Re*r./nu; % derivative dU0/dr (r in [-1,1])
M0_Edv = @(r)(integral(@(r)dM0_dr_Edv(r,Re,nu_t(r,k,Re,A)),-1,r));
    % -- Fit with cubic spline
    % (Defined once experimental data is loaded.)
M0_fit = @(y)0;
dM0_dy_fit = @(y)0;