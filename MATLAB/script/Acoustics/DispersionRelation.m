%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dispersion relation with uniform flow and Myers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% From (Brambley, 2009) Eq. (7)
% Polynomial in w
% Use expand and collect in Maple to get the polynomial

%% General Data
m = 1;
U = 0.5;

%% I - Impedance: 1i*w*Z(w) = A*w
A = 1;
Zac = @(w)(A*w);
p = @(k,m,U)([(-A^2-1),(4*U*k+2*U*k*A^2),(-6*U^2*k^2+A^2*k^2+A^2*m^2-A^2*U^2*k^2),4*U^3*k^3,-U^4*k^4]);

%% I - Impedance: 1i*w*Z(w) = A*w^2
A = 1;
Zac = @(w)(A*w.^2);
p = @(k,m,U)([-A^2,2*A^2*U*k,A^2*k^2+A^2*m^2-A^2*U^2*k^2-1,4*U*k,-6*U^2*k^2,4*U^3*k^3,-U^4*k^4]);
%% I - Impedance: Z(w) = sqrt(1i*w), so 1i*w*Z(w) = A*w^(3/2)
A = 1e3*1i*sqrt(1i);
Zac = @(w)(A*w.^(3/2));
p = @(k,m,U)([-A^2,2*U*k*A^2-1,4*U*k+A^2*k^2+A^2*m^2-A^2*U^2*k^2,-6*U^2*k^2,4*U^3*k^3,-U^4*k^4]);

%% Plot of Im(w(k)) for k real
k = linspace(1e3,1e5,1e2);
% --
W = zeros(0);
Wmax = zeros(0);
for i = 1:length(k)
W(:,i) = roots(p(k(i),m,U));
W(:,i) = W(:,i).*(real(((W(:,n)-U*k(i)).^2)./(Zac(W(:,n))))>0);
Wmax(i) = -min(imag(W(:,i)));
end
plot(k,Wmax);
title('Fastest growing Surface mode');
xlabel('k');
ylabel('-Im(w)');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Dispersion relation from (Brambley, 2009)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Dispersion relation
    % polynomial in w (second order)
Polw = @(k,mu,U,lambda)([1,-1i*mu*k^2-2*U*k,U^2*k^2+1i*mu*U*k^3-k^2+lambda^2]);
    % polynomial in k (third order)
Polk = @(w,mu,U,lambda)([i*mu*U,(U^2-1-i*mu*w),-2*w*U,w^2+lambda^2]);
    % polynomial for double roots
Polw2 = @(mu,U,lambda)([8*U*mu^4-28*1i^2*mu^4*U,...
    -32*U*1i*mu^3*(1+2*U^2)+(14*(-(2*(1-U^2))*mu^2+4*1i^2*mu^2*(1+2*U^2)))*1i*mu*U+8*1i*mu^3*U*(1-U^2)-147*1i^3*mu^3*U^3,...
    2*U*(-(4*(2-2*U^2))*mu^2+16*1i^2*mu^2*(1+2*U^2)^2)+(14*((4*(1-U^2))*1i*mu*(1+2*U^2)+1i*mu*(2-2*U^2)))*1i*mu*U-(4*(-(2*(1-U^2))*mu^2+4*1i^2*mu^2*(1+2*U^2)))*U*(1-U^2)-36*1i^2*mu^4*U*lambda^2+84*1i^2*mu^2*U^3*(1-U^2),...
    16*U*(2-2*U^2)*1i*mu*(1+2*U^2)+(14*(1-U^2))*(2-2*U^2)*1i*mu*U-(4*((4*(1-U^2))*1i*mu*(1+2*U^2)+1i*mu*(2-2*U^2)))*U*(1-U^2)+(18*(-(2*(1-U^2))*mu^2+4*1i^2*mu^2*(1+2*U^2)))*1i*U*mu*lambda^2-3*1i*mu*U*(126*1i^2*U^2*mu^2*lambda^2+4*U^2*(1-U^2)^2),...
    2*U*(2-2*U^2)^2-4*(1-U^2)^2*(2-2*U^2)*U+(18*((4*(1-U^2))*1i*mu*(1+2*U^2)+1i*mu*(2-2*U^2)))*1i*U*mu*lambda^2+108*1i^2*mu^2*U^3*lambda^2*(1-U^2),...
    (18*(1-U^2))*(2-2*U^2)*1i*U*mu*lambda^2-243*1i^3*mu^3*U^3*lambda^4]);
    % Constants
mu = 0.8;
lambda = 1;
U = 1.2;
%% Sweep k in R
k = linspace(-2,2,5e2);
    % Sweep k
W = zeros(0);
for j=1:length(k)
    W(:,j) = roots(Polw(k(j),mu,U,lambda));
    %[~,I] = sort(imag(W(:,j)));
    %W(:,j) = W(I,j);
end
clf
hold on
leg=cell(0);
for j=1:size(W,1)
    plot(W(j,:),'-');
    leg(end+1)={sprintf('Root %d',j)};
end
W0 = min(imag(W(:))); % temporal growth rate
plot([min(real(W(:))),max(real(W(:)))],W0*[1,1],'-');
leg(end+1)={sprintf('Max. Growth rate W_0=%1.2g',W0)};
Wr = roots(Polw2(mu,U,lambda));
plot(Wr,'x');
leg(end+1)={sprintf('Double root (min: %1.2g)',min(imag(Wr)))};
xlabel('Re(w)');
ylabel('Im(w)');
title(sprintf('w(k) k in [%1.2g,%1.2g]',k(1),k(end)));
legend(leg);
axis([-5,5,-1,1])
%% Sweep w in C
wr = linspace(-1e-5,1e-5,5);
wi = linspace(-5,-0.6,1e2);
col = rand(length(wr),3);
    % Sweep w
K = zeros(0);
for l=1:length(wr) % for each Re(w)
for j=1:length(wi) % for each Im(w)
    K(:,j,l) = roots(Polk(wr(l)+1i*wi(j),mu,U,lambda));
    %[~,I] = sort(imag(K(:,j)));
    %K(:,j) = K(I,j);
end
end
% Plot
clf
subplot(2,1,1)
hold on
for l=1:length(wr)
   plot(wr(l)*ones(length(wi)),wi,'-','color',col(l,:),'LineWidth',3);
end
xlabel('Re(w)');
ylabel('Im(w)');
title(sprintf('Grid of w: %1.2g<Re(w)<%1.2g',min(wr),max(wr)));
ylim([min(wi),0.1])
subplot(2,1,2)
hold on
leg=cell(0);
for l=1:size(K,3)
for j=1:size(K,1)
    plot(K(j,:,l),'o','color',col(l,:),'LineWidth',3);
    leg(end+1)={sprintf('Root %d-%d',j,l)};
end
end
%kmax = min(imag(K(1,:))); % upper analycity strip
%kmin = max(imag(K(2,:))); % lower analycity strip
% plot([min(real(K(:))),max(real(K(:)))],kmax*[1,1],'-');
% plot([min(real(K(:))),max(real(K(:)))],kmin*[1,1],'-');
% leg(end+1)={sprintf('Upper analycity strip: %1.2g',kmax)};
% leg(end+1)={sprintf('Lower analycity strip: %1.2g',kmin)};
xlabel('Re(k)');
ylabel('Im(k)');
title(sprintf('k(w) w in [%1.2g+1i*%1.2g,%1.2g+1i*%1.2g]',real(w(1)),imag(w(1)),real(w(end)),imag(w(end))));
%legend(leg);
%axis([-5,5,-2,1])