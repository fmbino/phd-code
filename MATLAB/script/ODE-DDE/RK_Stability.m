%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Time-integration of the ODE given by the diffusive variables
%
%               y'(t)= -Diag[xi_1,..,xi_m] x y(t) + u(t) 
% t scalar,
% xi > 0 (1x1) poles
% y vector (mx1)
% xik line (1xm)
% muk column (mx1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -- Inputs

xi=[1e-1,1e-4]; % Line vector of poles xi_k
mu = [1,1];
tf = 50;  % Final time
u = @(t)(0*ones(size(t))); % (1x1) -> (1x1)
al=10;
%u = @(t)(exp(-al*t)); % (1x1) -> (1x1)
x0 = ones(length(xik),1); % (mx1) Standard
%y0 = u(0)./transpose(xik); % Extended
% --
y_h_ex = @(t)(kron(y0,ones(1,N_it+1)).*exp(-kron(transpose(xik),t))); % Exact homogeneous solution
y_step_ex = @(t)(kron((1./xik)',ones(1,N_it+1)).*(1-exp(-kron(transpose(xik),t)))); % Exact particular step solution
y_exp_ex = @(t)(kron((1./(-al+xik))',ones(1,N_it+1)).*(exp(-kron(al*ones(size(transpose(xik))),t))-exp(-kron(transpose(xik),t))));

%% ODE45

%% RK4 Time integration
[y,t]=RK4_explicit(-diag(xik),u,y0,tf,0.2);
N_it = length(t)-1;
m=length(xik);

%% LSERK4 Time integration
dt = 1e-1;
[x_full,t_full,Nop_full] = LSERK4_op(@(x,t)(A*x),x0,tf,dt);

%% Exp time integration
dt=1e-2;
[y,t] = ODE_Integration(xik,u,y0,tf,dt,5);
N_it = length(t)-1;
%% Plot diffusive variable
m_p = 2; % index to plot

N_p = N_it; % Number of iterations for the variable to plot

Y_h_ex = y_h_ex(t);
Y_step_ex = y_step_ex(t);
Y_exp_ex = y_exp_ex(t);
figure
hold all
plot(t,y(m_p,:),'x-');
plot(t,Y_h_ex(m_p,:),'-');
plot(t,Y_step_ex(m_p,:)+Y_h_ex(m_p,:),'-');
plot(t,Y_exp_ex(m_p,:)+Y_h_ex(m_p,:),'-');
title('RK4 integration of the diffusive system');
xlabel('t(s)');
ylabel('y');
legend(sprintf('xi_%d=%1.1e',m_p,xik(m_p)),'Exact free solution','Exact step solution','Exact exp solution');

%% Plot observator z
% muk column vector
z_std = transpose(muk)*y; % Standard diffusive rep.
z_ext = transpose(muk)*(-diag(xik))*y+sum(muk)*u(t); % Ext. diffusive rep.

figure
hold all
plot(t,z_std,'-');
plot(t,z_ext,'-');
title('RK4 integration of the diffusive system - Observator');
xlabel('t(s)');
ylabel('z');
legend('Standard','Extended');

%% Plot error 

    % RK4
L2err_RK4 = zeros(0);
s = [1,1e-1,1e-2,1e-3];
for i=1:length(s)
    [y,t]=RK4_explicit(-diag(xik),u,y0,tf,s(i)); N_it=length(t)-1;
    Ysex = kron((1./xik)',ones(1,N_it+1)).*(1-exp(-kron(transpose(xik),t))); % Step response
    Yhex = kron(y0,ones(1,N_it+1)).*exp(-kron(transpose(xik),t)); % Homogeneous response
    Yex = Yhex + Ysex;
    L2err_RK4(end+1) = sum(sum((y-Yex).*(y-Yex)))/length(t);
    L2err_RK4(end)= sqrt(L2err_RK4(end));
end
    % Exp time integration
L2err_ETI = zeros(0);
dt=[40,30,20,10,1];
for i=1:length(dt)
    [y,t] = ODE_Integration(xik,u,y0,tf,dt(i)); N_it = length(t)-1;
    Ysex = kron((1./xik)',ones(1,N_it+1)).*(1-exp(-kron(transpose(xik),t))); % Step response
    Yhex = kron(y0,ones(1,N_it+1)).*exp(-kron(transpose(xik),t)); % Homogeneous response
    Yex = Yhex + Ysex;
    L2err_ETI(end+1) = sum(sum((y-Yex).*(y-Yex)))/length(t);
    L2err_ETI(end)= sqrt(L2err_ETI(end));
end

figure
subplot(1,2,1)
loglog(s,L2err_RK4,'-o');
a = polyfit(log(s),log(L2err_RK4),1); a = a(1);
legend(sprintf('Order: %1.2g',a));
subplot(1,2,2)
loglog(dt,L2err_ETI,'-o');
a = polyfit(log(dt),log(L2err_ETI),1); a = a(1);
legend(sprintf('Order: %1.2g',a));


%clear N s a
%% Asynchronous integration
% 1 integration loop per diffusive variable (everywhere in space)

    % Time integration constants (for each var.)
    
dt = 1.9./xik; % Maximal time-step for each variable
dt_f = rem(tf,dt); % Final time-step (for the last iteration) for each var.
N_it = floor(tf./dt); % No. of iter. to perform with <dt>
dt = dt'; % switch to column format (-> (mxmax(N_it)))
    for i=1:m % for each variable
        if N_it(i)==0
            dt(i,1:1)=dt_f(i);
            N_it(i)=1;
        else
            dt(i,N_it(i)+1)=0; % extend the table
            dt(i,:) = [dt(i,1)*ones(1,N_it(i)),dt_f(i)];
            N_it(i) = N_it(i) + 1;
        end
    end

t = zeros(m,max(N_it)+1);
t(:,2:(end-1)) = dt(:,1:(end-1)).*kron(ones(m,1),1:(max(N_it)-1));
t(:,end) = t(:,end-1)+dt(:,end);


f = @(t,y,xi)(-xi*y+u(t)); % ((1x1),(mx1) -> (mx1))
y = zeros(m,max(N_it)+1); % Output: y(n) = y(t_{n-1})
y(:,1)=y0;
% Runge-Kutta time-integration
time = cputime();
for i=1:m
    for n=1:N_it(i)
        fprintf('Var. n°%d/%d, Iter n°%d/%d, time-step dt=%1.2g\n...',i,m,n,N_it(i),dt(i,n));
        k1 = f(t(i,n),y(i,n),xik(i));
        k2 = f(t(i,n)+dt(i,n),y(i,n)+dt(i,n)*k1,xik(i));
        y(i,n+1) = y(i,n) + (dt(i,n)/2)*(k1+k2);
    end
end
fprintf('Asynchrous integration: %ds\n',cputime()-time);