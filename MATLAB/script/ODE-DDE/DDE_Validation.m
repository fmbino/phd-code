%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Validation of DDE solver using hyperbolic realisation
% --
% The purpose of this script is to validate the function
% 'DDEConstantDelay_BuildHyperbolicRealisation', which solves a delay 
%differential equation (DDE) using a hyperbolic realisation of the delay.
% Validation is done by solving in the frequency domain (validation in the
%time domain can be done on uncoupled DDE).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% I - Definition
N = 1e2; % No. of variables
Ntau = 10; % No. of delayed variables
tau = 1;
A = -50*sparse(ones(N));
B = rand(N,Ntau);
C = rand(Ntau,N);
    % Exact transfer function
H_Exact=@(s)(s*speye(size(A))-A-exp(-s*tau)*B*C);
    % Computed transfer function
H_Discrete = @(s,A)(s*speye(size(A))-A);
    % Exact and computed solution
f = ones(N,1); % left-hand side
x_Exact = @(s)(H_Exact(s)\f);
x_Discrete = @(s,A)(H_Discrete(s,A)\[f;zeros(size(A,1)-N,1)]);
%% II - Discretisation
Np = 2; % No. of nodes per cell
Nk = 2; % No. of cell
Ag = DDEConstantDelay_BuildHyperbolicRealisation(A,B,tau,C,'Np',Np,'Nk',Nk);
%% III Comparison (Laplace)
s = 1e6;
    % Compute sol.
x_E = x_Exact(s); x_D = x_Discrete(s,Ag);
fprintf('||1 - Discrete/Exact||_inf = %2.1e.\n',norm((x_E-x_D(1:N))./x_E,inf));
%% IV Comparison (Fourier, if possible)
w = 5; % pulsation
idx_p = min(1,N); % idx to plot
x_E = x_Exact(1i*w); x_D = x_Discrete(1i*w,Ag);
t = linspace(0,5,1e2);
clf
leg=cell(0);
hold all
plot(t,x_E(idx_p)*exp(1i*w*(t)));
leg(end+1)={sprintf('Exact')};
plot(t,x_D(idx_p)*exp(1i*w*t));
leg(end+1)={sprintf('HR-DG(Np=%d,Nk=%d)',Np,Nk)};
legend(leg);
xlabel('t');
xlabel('x');
title('Validation of Hyperbolic realisation (Harmonic solution)');