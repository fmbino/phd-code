%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Continuous Runge-Kutta method to solve a constant-delay Delayed
% Differential Equation (DDE)
%           y'= f(t,y(t),y(t-tau)) with y(t)=y0(t) -tau<t<0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definition of the DDE:
% y'(t) = lambda*y(t) + mu*y(t-tau) t in [0,tf]
% y(t) = y0 t in [-tau,0]
lambda = diag([1]);
mu = diag([-2]);
tau = 1/3;
y0 = [3];
tf = 2*tau;
y_ex = @(t)(DDELinearModelSol(t,y0,diag(lambda),diag(mu),tau));
name = sprintf('lambda=%1.1e, mu=%1.1e',lambda(1),mu(1));
Ny = length(y0);
%% Time-integration with dde23
options = ddeset('RelTol',1e-10);
sol = dde23(@(t,y,Z)(DDELinearModel(t,y,Z,lambda,mu)),[tau],[y0],[0,tf],options);
t_dde23 = linspace(0,tf,1e3);
y_dde23 = deval(sol,t_dde23);
%% Time-integration with 5-stage 4th-order continuous Runge-Kutta
dt_max = 4.4/max(abs(eig(lambda))); % rule of thumb for linear stability limit
dt_CERK = dt_max/10;
    % 5-stage 4th-order
[A_ERK,b_ERK] = RK_get54Coeffs_2NStorage();
    % 8-stage 4th-order
[A_ERK,b_ERK] = RK_get84Coeffs_2NStorage();
[A_ERK,b_ERK] = RK_convertCoeffs(A_ERK,b_ERK);
c_ERK = sum(A_ERK,2);
    % Natural continuous extension of minimal degree (Bellen 2003)
%Bpol = [3*(2*c_ERK(:)-1).*b_ERK(:),2*(2-3*c_ERK(:)).*b_ERK(:),0*b_ERK(:)];
    % Natural continuous extension of degree 3 (Bellen 2003) 
Bpol= [4*(3*c_ERK(:)-2).*b_ERK(:),3*(3-4*c_ERK(:)).*b_ERK(:),0*b_ERK(:),0*b_ERK(:)];
Bpol(1,:) = [2*(1-4*b_ERK(1)),3*(3*b_ERK(1)-1),1,0];
[t_CERK, y_CERK] = CERK_DDEConstantDelay(@(t,y,z)(lambda*y+mu*z),tau,y0,tf,dt_CERK,A_ERK,b_ERK,Bpol,4,1:Ny);
%% Time-integration using hyperbolic realisation
    % Parameters of hyperbolic realisation
Nk = 15; % element number
Np = 20; % Number of nodes per cell
    % Get coupled system
[A,q0] = DDEConstantDelay_BuildHyperbolicRealisation(lambda,mu,tau,eye(length(y0)),'y0',y0,'Nk',Nk,'Np',Np);
    % Plot spectrum
clf
hold all
leg=cell(0);
plot(eig(full(A(1:Ny,1:Ny))),'o');
leg(end+1)={'System alone'};
plot(eig(full(A(:,:))),'o');
leg(end+1)={'Coupled system'};
plot(eig(full(A((Ny+1):end,(Ny+1):end))),'x');
leg(end+1)={'DG alone'};
%plot(real(A(1,1)),imag(A(1,1)),'x');
plot([0,0],ylim,'r--');
axis([-15,0,-300,300])
xlabel('Re(lambda)');
ylabel('Im(lambda)');
title(sprintf('DDE Semigroup generator spectrum (DG(Nk=%d,Np=%d))',Nk,Np));
    % Time integration
legend(leg);
dt_HR = dt_max/10; % Time step
[y_HR,t_HR] = LSERK4_op(@(x,t)(A*x),q0,tf,dt_HR);
y_HR = y_HR(1:Ny,:);
%% Plot solutions
idxp = min(1,length(y0)); % choose idx to plot
%--
t = linspace(0,tf,1e3);
clf
subplot(1,2,1)
leg=cell(0);
hold all
yex = y_ex(t);yex=yex(idxp,:);
plot(t,yex); clear yex
leg(end+1)={sprintf('Exact solution')};
plot(t_dde23,y_dde23(idxp,:));
leg(end+1)={sprintf('dde23')};
plot(t_CERK,y_CERK(idxp,:));
leg(end+1)={sprintf('CERK dt=%1.1e',dt_CERK)};
plot(t_HR,y_HR(idxp,:));
leg(end+1)={sprintf('HR-DG(Nk=%d,Np=%d) dt=%1.1e',Nk,Np,dt_HR)};
legend(leg);
xlabel('t');
ylabel('y');
title(sprintf('DDE: %s',name));
subplot(1,2,2)
hold all
leg=cell(0);
err = (deval(sol,t_CERK)-y_ex(t_CERK))./(1+y_ex(t_CERK)); err=err(idxp,:);
plot(t_CERK,err); clear err
leg(end+1)={sprintf('dde23')};
err = (y_CERK-y_ex(t_CERK))./(1+y_ex(t_CERK)); err=err(idxp,:);
plot(t_CERK,err);
leg(end+1)={sprintf('CERK')};
err = (y_HR-y_ex(t_HR))./(1+y_ex(t_HR)); err=err(idxp,:);
plot(t_HR,err);
leg(end+1)={sprintf('HR-DG(Nk=%d,Np=%d)',Nk,Np)};
legend(leg);
xlabel('t');
ylabel('y');
title(sprintf('Difference'));
%% Compute L2 error
% Order validation
dt_ref = dt_max;
dt = min([1e-3,2e-3,5e-3,1e-2,2e-2,5e-2,1e-1,2e-1,5e-1,1e0,2e0,5e0,1e1,2e1,5e1]*dt_ref,tf);
L2_CERK = zeros(1,length(dt));
L2_HR = zeros(1,length(dt));
for i=1:length(dt)
                % CERK
    [t_CERK, y_CERK] = CERK_DDEConstantDelay(@(t,y,z)(lambda*y+mu*z),tau,y0,tf,dt(i),A_ERK,b_ERK,Bpol,4,1:Ny);
                % HR-DG
    A = DDEConstantDelay_BuildHyperbolicRealisation(mu,tau,eye(length(y0)),'Nk',Nk,'Np',Np);
    A(1:Ny,1:Ny) = lambda;
    Psi0 = reshape(kron(ones(Np*Nk,1),y0(:)'),[],1); % Initialisation of auxiliary var.
    q0 = [y0(:);Psi0(:)];
    [y_HR,t_HR] = LSERK4_op(@(x,t)(A*x),q0,tf,dt(i));
    y_HR = y_HR(1:Ny,:);
            % Computation of L2 error
    L2_CERK(:,i) = sqrt(mean((y_CERK-y_ex(t_CERK)).^2,2));
    L2_HR(:,i) = sqrt(mean((y_HR-y_ex(t_HR)).^2,2));
end
%% Plot L2 error
j_r = 1:6; % keep six first value
clf
leg=cell(0);
loglog(dt/dt_max,L2_CERK(1,:),'--x');
hold all
a = polyfit(log(dt(j_r)),log(L2_CERK(1,j_r)),1); Order_CERK(i) = a(1);
leg(end+1)={sprintf('CERK (%1.1e)',Order_CERK(i))};
loglog(dt/dt_max,L2_HR(1,:),'--x');
a = polyfit(log(dt(j_r)),log(L2_HR(1,j_r)),1); Order_HR(i) = a(1);
leg(end+1)={sprintf('HR (Np=%d,Nk=%d) (%1.1e)',Np,Nk,Order_HR(i))};
hold all
xlabel(sprintf('dt / dt max stab. linear'));
ylabel(sprintf('L2 error t in [0,%d]',tf));
title('Comparison of integration method for dy=xi*y+u');
axis([min(dt/dt_max), max(dt/dt_max),1e-16,10])
legend(leg);