%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Validation of DDE LTI solver
% --
% The purpose of this script is to validate the function
% 'DDELTI_BuildParabolicRealisation', which solves a delay differential
% equation (DDE) with linear time-invariant (LTI) operators.
% Validation is done by solving in the frequency domain (construction of a
% time-domain solution can be involved for these equations).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
%% I - Definition
Nx = 1e2;
Ax = -50*sparse(ones(Nx));
    % Direct delays
Ntau = [1,4,5];
DirectDelay = cell(length(Ntau),1);
for i=1:length(Ntau)
    DirectDelay{i} = struct('B',rand(Nx,Ntau(i)),'C',rand(Ntau(i),Nx));
end
    % Undelayed LTI operators
NQx = [1,4,5];    
NQphi = [10,5,20];
LTIOp = [];
for i=1:length(NQx)
    LTIOp{i} = struct('B',rand(Nx,NQx(i)),'C',rand(NQx(i),Nx),'Operator',DiffusiveOperator(rand(NQphi(i),1),rand(NQphi(i),1),'Type','Extended'));
end
    % Delayed LTI operators
NRx = [1,4];    
NRphi = [10,10];
LTIOpDelay = [];
for i=1:length(NRx)
    LTIOpDelay{i} = struct('B',rand(Nx,NRx(i)),'C',rand(NRx(i),Nx),'Operator',DiffusiveOperator(rand(NRphi(i),1),rand(NRphi(i),1),'Type','Extended'));
end
%% II Build global formulation
[A,B,C] = DDELTI_BuildParabolicRealisation(Ax,DirectDelay,LTIOp,LTIOpDelay);
    % Computed Transfer function
H_Discrete = @(s,tau,A,B,C)(s*speye(size(A))-A-exp(-tau*s)*B*C);
%% III Compute discrete solution
    % -- user input
s = 1i*50; % chose value in right-half plane (Re(s)>=0)
f = ones(Nx,1); % left-hand side
delay = 1; % scalar
idx_p = min(1,Nx); % idx to plot
t = linspace(0,1,1e2);
    % --
    % Exact transfer function
H_A = s*speye(size(Ax))-Ax;
        % direct delay contribution
H_DirectDelay = 0;
for i=1:length(DirectDelay)
    H_DirectDelay = H_DirectDelay -exp(-s*delay)*DirectDelay{i}.B*DirectDelay{i}.C ;
end
        % undelayed LTI operators contribution
H_LTI = 0;
for i=1:length(LTIOp)
    H_LTI = H_LTI -LTIOp{i}.B*LTIOp{i}.C*LTIOp{i}.Operator.Laplace(s) ;
end
        % delayed LTI operators contribution
H_LTIDel = 0;
for i=1:length(LTIOpDelay)
    H_LTIDel = H_LTIDel -exp(-s*delay)*LTIOpDelay{i}.B*LTIOpDelay{i}.C*LTIOpDelay{i}.Operator.Laplace(s);
end
H_Exact = H_A+H_DirectDelay+H_LTI+H_LTIDel;
    % Exact and discrete solution
x_Exact = H_Exact\f;
x_Discrete = H_Discrete(s,delay,A,B,C)\[f;zeros(size(A,1)-Nx,1)];

% Comparison
    % Laplace
fprintf('||1 - Discrete/Exact||_inf = %2.1e.\n',norm((x_Exact-x_Discrete(1:Nx))./x_Exact,inf));
    % Fourier
clf
leg=cell(0);
hold all
plot(t,x_Exact(idx_p)*exp(s*t));
leg(end+1)={sprintf('Exact')};
plot(t,x_Discrete(idx_p)*exp(s*t));
leg(end+1)={sprintf('Discrete')};
legend(leg);
xlabel('t');