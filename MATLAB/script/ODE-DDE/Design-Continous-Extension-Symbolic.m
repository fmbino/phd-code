%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computation of continuous extension for discrete Runge-Kutte method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Given a discrete Runge-Kutta method, defined by its coefficients A,b,c,
% this scripts can be used for two purposes:
%   (a) Check the "validity" a given family of polynomials.
%   (b) Compute a/the continuous extension of the RK method.
% Rmk: This script requires the 'Symbolic Math' toolbox.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Polynomials storage format:
%   [b1_P ... b1_0] b1(theta) (degree decreases left->right)
%   [b2_P ... b2_0]

%% I - Definitions of Runge-Kutta methods
% See (Bellen & Zennaro, 2003) Chap.5
% Discrete RK can be checked with RK_ComputeOrder(A,b)
%% 1-stage ERK: midpoint rule
    % Disrete RK method
b = sym([1]);
A = sym([1/2]);
c = sum(A,2);

%% 2-stage ERK
    % Trapezoidal rule (p.122)
b = sym([1/2;1/2]);
A = sym([0,0;
        1/2,1/2]);
c = sum(A,2);
    % (Unstable) Polynomial (d=2)
Bpol = sym([-1/2, 1,0;
            1/2, 0,0]);
    % Discrete (implicit) RK method
b = sym([5/3;3/8]);
A = sym([1/12,1/4;
        25/36,1/12]);
c = sum(A,2);
    % (Unstable) Polynomial (d=2)
Bpol = sym([-9/8, 14/8,0;
            9/8, -2*3/8,0]);
%% 3-stage ERK
    % Lobatto IIIA (Ehle) method (p.123)
b = sym([1/6;2/3;1/6]);
A = sym([0,0,0;
        5/24,1/3,-1/24;
        1/6,2/3,1/6]);
c = sum(A,2);
    % Polynomials (d=3)
Bpol = sym([2*1/3, -2*3/4,2*1/2,0;
        -4*1/3, 4*1/2, 0, 0;
        2*1/3, -2*1/4, 0, 0]);    
%% 4-stage Explicit Runge-Kutta method
    % Discrete RK method
b = sym([1/6;1/3;1/3;1/6]);
A =sym( [0,0,0,0;
        1/2,0,0,0;
        0,1/2,0,0;
        0,0,1,0]);
c = sum(A,2);
    % Polynomial (d=2) p.126
Bpol = sym([-1/2,2/3,0;
            0,1/3,0;
            0,1/3,0;
            1/2,-1/3,0]);
    % Polynomial (d=3) p.126
Bpol = sym([2/3,-3/2,1,0;
            -2/3,1,0,0;
            -2/3,1,0,0;
            2/3,-1/2,0,0]);
%% 5-stage Explicit Runge-Kutta method
[A,B] = RK_get54Coeffs_2NStorage_Symb();
[A,b] = RK_convertCoeffs(A,B);
c = sum(A,2);
    % Naive: extension of degree 3 for nu=4 (Bellen 2003) 
%Bpol= [4*(3*c(:)-2).*b(:),3*(3-4*c(:)).*b(:),0*b(:),0*b(:)];
%Bpol(1,:) = [2*(1-4*b(1)),3*(3*b(1)-1),1,0];
%% 6-stage Explicit Runge-Kutta method
[A,B] = RK_get64Coeffs_2NStorage();
[A,b] = RK_convertCoeffs(A,B);
c = sum(A,2);
%% 7-stage Explicit Runge-Kutta method
[A,B] = RK_get74Coeffs_2NStorage();
[A,b] = RK_convertCoeffs(A,B);
c = sum(A,2);
%% 8-stage Explicit Runge-Kutta method
[A,B] = RK_get84Coeffs_2NStorage();
[A,b] = RK_convertCoeffs(A,B);
c = sum(A,2);
%% Check order
RK_ComputeOrder(double(A),double(b));
%% Plot A-stability region
f=figure;
RK_PlotAStabilityRegion(f,double(A),double(b),[-10,1],[-5,5]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% III - Computation of a family of polynomial
% The constraints on the family of polynomials b_i(theta) are written as
%   T*Bpol=U
% where Bpol = [b11...b1d,b21...b2d,...,bnu-d] (nu*d unknowns)
% Each set of conditions add lines to the matrices T and U.
% The conditions used can be chosen freely.
d = 3; % degree of the polynomials sought
%--
if d>length(b)
    error('Degree d too high\n.');
end
nu = length(b);
%--
% B = [b11...b1d,b21..b2d...b-nu-d] (nu*d unknowns)
T = sym(zeros(0)); % T*B=U where B defines the polynomial
U = sym(zeros(0));
%% Second continuity condition
for i=1:nu
   T(end+1,d*(i-1)+(1:d))=1;
   U(end+1) = b(i);
end
%% Uniform order conditions 
% (q=1)
for j=1:d
   T(end+1,j+(0:(nu-1))*d)=1;
end
U(end+(1:d))= (1:d)==1;
if d>1
% (q=2)
for j=1:d
   T(end+1,j+(0:(nu-1))*d)=transpose(c(:));
end
U(end+(1:d))= (1/2)*((1:d)==2);
if d>2
% (q=3) - 1
for j=1:d
   T(end+1,j+(0:(nu-1))*d)=transpose(c(:).^2);
end
U(end+(1:d))= (1/3)*((1:d)==3);
% (q=3) - 2
for j=1:d
   T(end+1,j+(0:(nu-1))*d)=transpose(A*c(:));
end
U(end+(1:d))= (1/6)*((1:d)==3);
end
end
%% Natural Continuous Extension conditions
for r=1:(d-1)
   for i=1:nu
       T(end+1,d*(i-1)+(1:d))=sym(r)./sym(r+(1:d));
   end
   U(end+(1:nu))=b.*(1-c.^r);
end
%% Analysis of the linear system T*Bpol=U
r = rank(T); % rank of the linear system
fprintf('Find nu=%d polynomials of degree %d for a nu=%d-stage RK\n',nu,d,nu);
fprintf('The chosen conditions leads to %d equations for %d unknowns, with:\n',size(T,1),size(T,2));
fprintf('Rank: %d\n',r);
if r~=min(size(T,2),size(T,1))
    deff = size(T,2)-r;
    fprintf('The system is rank deficient. (deficiency: %d)\n',deff);
    fprintf('\t->%d variables are unecessary.\n',deff);
else
    fprintf('The system has full rank.\n');
end
Bpol_R = [];
%% Choose variable to remove
% If there are DoF, the best choice is to remove the coefficient associated
% with the highest degree (-> stability)
% The variable removed are stored in Bpol_R.
r = rank(T);
dof = size(T,2)-r;
if dof>0
    Coldof = d + (0:(nu-1))*d;
    fprintf('Let''s try to remove %d variables...\n',dof);
    fprintf('The columns linked to the coefficients of highest degree %d are:\n',d);
    fprintf('[%s]\n',sprintf('%d ',Coldof(:)));
    if dof>length(Coldof)
        error('There are too many DoF. Removing the coefficients of highest degree cannot be enough.\n'); 
    end
    Col = nchoosek(d + (0:(nu-1))*d,dof); % column combination (highest d degree)
    if dof>1
        X = nchoosek(d-1 + (0:(nu-1))*d,dof-1); % column combination (d-1 degree)
        X = [d*ones(size(X,1),1),X];
        Col = [Col; X];
    end
    found = 0;
    i=0;
    while (~found)
        i = i+1;
        Tr = T;
            % Remove these DoF columns
        Tr(:,Col(i,:)) = []; 
        if rank(Tr)==rank(T)
            T = Tr;
            found = 1;
            Bpol_R = Col(i,:); % Coefficients removed
            fprintf('Removing variables [%s] does not change the rank.\n',sprintf('%d ', Bpol_R));
            fprintf('T has been updated\n');
        else
            fprintf('Removing variables %s change the rank.\n',sprintf('%d ', Col(i,:)));
        end 
    end
else
    fprintf('No DoF. Nothing to be done.\n');
end
%% Solve and plot
% If the RK coefficients are not exact, then symbolic solving will fail, as
% it will see an over-determined system.

    % Numerical solving
    % ! We downgrade to double precision here (16 digits)
Bpol = double(T)\double(U(:)); % approximately solve over-determined system
    % Symbolic solving
%Bpol = T\U(:);

    % Insert back '0' where coefficients have been removed
if length(Bpol_R)>0
Bpol_tmp = sym(zeros(length(Bpol)+length(Bpol_R),1));
j=0; % number of inserted elements
for i=1:length(Bpol_tmp)
    if sum(find(Bpol_R==i)) % i in Bpol_R?
        fprintf('%d has been removed.\n',i)
        Bpol_tmp(i) = 0;
        j = j+1;
    else
        Bpol_tmp(i) = Bpol(i-sum(j));
    end
end
Bpol = Bpol_tmp;
clear Bpol_tmp j
end
    % Reshape
    % b_i(theta): decreasing coefs
Bpol = fliplr(transpose(reshape(Bpol,d,nu)));
Bpol(:,end+1) = 0; % constant coefficient is null
plotPolynomialFunctions(double(Bpol),[0,1],20);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  - Validity check of a given Bpol
RK_CheckContinuousExtension(double(A),double(b),double(Bpol));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% VI - Stability condition for interpolant
% Stability conditions for the interpolant Bpol are given in (Bellen &
% Zennaro, 2003, Chap. 8, Prop. 8.4.2)
% We denote the A-stability function
%   R(z,x) = P(z,x) / Q(z) where 
% where
%   z <-> alpha
%   x <-> theta in [0,1]
syms z x
nu = length(b);
e = sym(ones(nu,1));
clear BpolV % Vector of polynomials b_i(x)
for i=1:size(Bpol,1)
    BpolV(i) = poly2sym(Bpol(i,:),x);
end
P(z,x) = det(eye(nu)-z*A+z*kron(e(:),transpose(BpolV(:))));
P(z,x) = collect(P(z,x),z);
Q(z) = det(eye(nu)-z*A);
Q(z) = collect(Q(z),z);
fprintf('Stable interpolant with respect to the discrete RK method.');
fprintf('A-Stability function: R = P/Q.\n');
fprintf('(i) Degree condition\n');
fprintf('(i) deg(R(.,x)-1) <= deg(R(.,x=1)-1) ?\n');
fprintf('R(.,x)-1 is:\n')
collect(P(z,x)-Q(z),z)/Q(z)
fprintf('R(.,x=1)-1 is:\n')
collect(P(z,1)-Q(z),z)/Q(z)
fprintf('(ii) Pole condition\n');
fprintf('(ii) Does R(z,x) has additional pole of negative real part?\n');
fprintf('R(z,x):\n');
P(z,x)/Q(z)
fprintf('R(z,1):\n');
P(z,1)/Q(z)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Unknown vs Equations
Neq = @(d,nu)(d.*(2.^(d-1))+nu); % Continuity + Order conditions
Nun = @(d,nu)(d.*nu); % Number of unknowns
DoF = @(d,nu)(Nun(d,nu)-Neq(d,nu)); % Lower bound on the DoF
clf
hold all
leg=cell(0);
d = 1:4;
for nu=2:5
        plot(d,DoF(d,nu),'x','LineWidth',3);
        leg(end+1)={sprintf('%d-stage',nu)};
end
plot(d,1-d./d,'k--')
xlabel('Degree d');
ylabel('Lower Bound on DoF');
legend(leg);