%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Continuous and discrete Runge-Kutta methods: comparison
%           y'=A*y + u(t) with y(0)=y0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-- ODE Definition
xi = [1];
A = -diag(xi);
Nxi = length(xi); % = 1
y0 = zeros(Nxi,1);
u = @(t)(ones(Nxi,length(t)));
tf = 10;
%--
y_h_ex = @(t)(kron(y0,ones(1,length(t))).*exp(-kron(xi,t))); % Exact homogeneous solution
y_step_ex = @(t)(kron((1./xi),ones(1,length(t))).*(1-exp(-kron(xi,t)))); % Exact particular step solution
y_ex = @(t)(y_h_ex(t)+y_step_ex(t));
%% -- 5-stage 4th order RK in 2N-storage format
    % Linear stability limit for LSERK4
eignval = sort(eig(A));
C_rk = 4.4; % LSERK4 
dt_max = C_rk/max(abs(eignval));
dt_LSERK4 = 1e-4*dt_max;
[y_LSERK4,t_LSERK4] = LSERK4_op(@(y,t)(A*y+u(t)),y0,tf,dt_LSERK4);
%% -- Explicit Runge Kutta: coefficients definition RK(5,4)
[A_ERK,b_ERK,c_ERK] = getERK54Coeffs();
%% -- Explicit Runge Kutta: time integration
    % Linear stability limit for LSERK4
eignval = sort(eig(A));
C_rk = 4.4; % LSERK4 
dt_max = C_rk/max(abs(eignval));
dt_ERK = 1e-4*dt_max;
[y_ERK,t_ERK] = ERK(@(y,t)(A*y+u(t)),y0,tf,dt_ERK,A_ERK,b_ERK);
%% -- Explicit Continous Runge Kutta: time integration
    % Linear stability limit for LSERK4
eignval = sort(eig(A));
C_rk = 4.4; % LSERK4 
dt_max = C_rk/max(abs(eignval));
dt_CERK = 1e-4*dt_max;
    % Natural continuous extension of minimal degree (Bellen 2003)
Bpol = [3*(2*c_ERK(:)-1).*b_ERK(:),2*(2-3*c_ERK(:)).*b_ERK(:),0*b_ERK(:)];
    % Natural continuous extension of degree 3 (Bellen 2003) 
%Bpol= [4*(3*c_ERK(:)-2).*b_ERK(:),3*(3-4*c_ERK(:)).*b_ERK(:),0*b_ERK(:),0*b_ERK(:)];
%Bpol(1,:) = [2*(1-4*b_ERK(1)),3*(3*b_ERK(1)-1),1,0];
theta = [0.1,0.4,0.8];
[y_ERK,t_ERK,~,~,y_CERK,t_CERK] = CERK(@(y,t)(A*y+u(t)),y0,tf,dt_ERK,A_ERK,b_ERK,Bpol,theta);
%% -- Comparison plot
clf
leg=cell(0);
hold all
plot(t_LSERK4,y_LSERK4);
err = sqrt(mean((y_LSERK4-y_ex(t_LSERK4)).^2,2));
leg(end+1)={sprintf('LSERK4 (dt=%1.1e, err: %1.1e)',dt_LSERK4,err)};
plot(t_ERK,y_ERK);
err = sqrt(mean((y_ERK-y_ex(t_ERK)).^2,2));
leg(end+1)={sprintf('ERK (dt=%1.1e, err: %1.1e)',dt_ERK,err)};
plot(t_CERK,y_CERK);
err = sqrt(mean((y_CERK-y_ex(t_CERK)).^2,2));
leg(end+1)={sprintf('CERK (dt=%1.1e, err: %1.1e)',dt_CERK,err)};
plot(t_LSERK4,y_ex(t_LSERK4));
leg(end+1)={sprintf('Exact')};
xlabel('time (s)');
ylabel('y');
title('ERK vs LSERK4');
legend(leg);
%% Compute L2 error
% Order validation
% L2_LSERK4: L2 error of LSERK4
dt_ref = dt_max;
dt = min([1e-3,2e-3,5e-3,1e-2,2e-2,5e-2,1e-1,2e-1,5e-1,1e0,2e0,5e0,1e1,2e1,5e1]*dt_ref,tf);
L2_LSERK4 = zeros(1,length(dt));
L2_ERK = zeros(1,length(dt));
L2_CERK = zeros(1,length(dt));
for i=1:length(dt)
        % LSERK4
    [y_LSERK4,t_LSERK4] = LSERK4_op(@(y,t)(A*y+u(t)),y0,tf,dt(i));
            % Computation of L2 error
    L2_LSERK4(:,i) = sqrt(mean((y_LSERK4-y_ex(t_LSERK4)).^2,2));
            % ERK
    [y_ERK,t_ERK] = ERK(@(y,t)(A*y+u(t)),y0,tf,dt(i),A_ERK,b_ERK);
            % Computation of L2 error
    L2_ERK(:,i) = sqrt(mean((y_ERK-y_ex(t_ERK)).^2,2));
                % CERK
    [~,~,~,~,y_CERK,t_CERK] = CERK(@(y,t)(A*y+u(t)),y0,tf,dt(i),A_ERK,b_ERK,Bpol,theta);
            % Computation of L2 error
    L2_CERK(:,i) = sqrt(mean((y_CERK-y_ex(t_CERK)).^2,2));
end
%% Plot L2 error
j_r = 1:6; % keep six first value
figure
leg=cell(0);
loglog(dt/dt_max,L2_LSERK4(1,:),'--x');
a = polyfit(log(dt(j_r)),log(L2_LSERK4(1,j_r)),1); Order_LSERK4(i) = a(1);
leg(end+1)={sprintf('LSERK4 (%1.1e)',Order_LSERK4(i))};
hold all
loglog(dt/dt_max,L2_ERK(1,:),'--x');
a = polyfit(log(dt(j_r)),log(L2_ERK(1,j_r)),1); Order_ERK(i) = a(1);
leg(end+1)={sprintf('ERK (%1.1e)',Order_ERK(i))};
loglog(dt/dt_max,L2_CERK(1,:),'--x');
a = polyfit(log(dt(j_r)),log(L2_CERK(1,j_r)),1); Order_CERK(i) = a(1);
leg(end+1)={sprintf('CERK (%1.1e)',Order_CERK(i))};
xlabel(sprintf('dt / dt max stab. linear'));
ylabel(sprintf('L2 error t in [0,%d]',tf));
title('Comparison of integration method for dy=xi*y+u');
axis([min(dt/dt_max), max(dt/dt_max),1e-16,10])
legend(leg);