! Module that contains input/output functions for matrices and vectors.
module matrixIO
use dispmodule
use hdf5
implicit none


interface matrixIO_writeCSV ! overloading
  module procedure matrixIO_writeCSV_Matrix, matrixIO_writeCSV_Vector
end interface

interface matrixIO_writeHDF5 ! overloading
  module procedure matrixIO_writeHDF5_Matrix, matrixIO_writeHDF5_Matrix_int, &
  matrixIO_writeHDF5_Vector, matrixIO_writeHDF5_Vector_int
end interface


contains
subroutine matrixIO_writeCSV_Matrix(M,filename)
! Write array M to a CSV file named filename.
    double precision, intent(in):: M(:,:)
    character(len=*), intent(in):: filename

    print *, "Writing matrix (size ",size(M,1),"x",size(M,2),") to file '", trim(filename), "'..."
    !call disp('M= ',M)
    ! Write matrix to text file
    open(20,action='write',file=filename,access="SEQUENTIAL");
    call disp(M,sep=',',unit=20);
    close(20);
end subroutine
subroutine matrixIO_writeCSV_Vector(M,filename)
! Write vector M to a CSV file named filename.
    double precision, intent(in):: M(:)
    character(len=*), intent(in):: filename;

    print *, "Writing vector (size ",size(M,1),") to file '", trim(filename), "'..."
    !call disp('M= ',M)
    ! Write matrix to text file
    open(20,action='write',file=filename,access="SEQUENTIAL");
    call disp(M,sep=',',unit=20);
    close(20);
end subroutine

subroutine matrixIO_writeHDF5_Matrix(M,filename)
! Write matrix to a HDF5 file.
    double precision, intent(in):: M(:,:)
    character(len=*), intent(in):: filename

    ! -- hdf5 read and write
    CHARACTER(LEN=4), PARAMETER :: dsetname = "dset"     ! Dataset name (doesnt matter)
    INTEGER(HID_T) :: file_id       ! File identifier
    INTEGER(HID_T) :: dset_id       ! Dataset identifier
    INTEGER(HID_T) :: dspace_id     ! Dataspace identifier
    INTEGER(HSIZE_T), DIMENSION(2) :: dims ! Dataset dimensions
    INTEGER     ::   rank =2                       ! Dataset rank
    INTEGER     ::   error ! Error flag
    ! --
    print *, "Writing matrix (size ",size(M,1),"x",size(M,2),") to HDF5 file '", trim(filename), "'..."

    !-- hdf5 write array
      ! Initialize FORTRAN interface.
      CALL h5open_f(error)
      ! Create a new file using default properties.
      CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)
      ! Create the dataspace.
      dims = (/size(M,1),size(M,2)/);
      CALL h5screate_simple_f(rank, dims, dspace_id, error)
      ! Create the dataset with default properties.
      ! Choose H5T_NATIVE_REAL (single precision), H5T_NATIVE_DOUBLE (double precision), H5T_NATIVE_INTEGER, H5T_NATIVE_CHARACTER
      CALL h5dcreate_f(file_id, dsetname, H5T_NATIVE_DOUBLE, dspace_id, &
           dset_id, error)
       ! write dataset
       CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, M, dims, error)
      ! End access to the dataset and release resources used by it.
      CALL h5dclose_f(dset_id, error)
      ! Terminate access to the data space.
      CALL h5sclose_f(dspace_id, error)
      ! Close the file.
      CALL h5fclose_f(file_id, error)
      ! Close FORTRAN interface.
      CALL h5close_f(error)
    !--
end subroutine
subroutine matrixIO_writeHDF5_Matrix_int(M,filename)
! Write matrix to a HDF5 file.
    integer, intent(in):: M(:,:)
    character(len=*), intent(in):: filename

    ! -- hdf5 read and write
    CHARACTER(LEN=4), PARAMETER :: dsetname = "dset"     ! Dataset name (doesnt matter)
    INTEGER(HID_T) :: file_id       ! File identifier
    INTEGER(HID_T) :: dset_id       ! Dataset identifier
    INTEGER(HID_T) :: dspace_id     ! Dataspace identifier
    INTEGER(HSIZE_T), DIMENSION(2) :: dims ! Dataset dimensions
    INTEGER     ::   rank =2                       ! Dataset rank
    INTEGER     ::   error ! Error flag
    ! --
    print *, "Writing matrix (size ",size(M,1),"x",size(M,2),") to HDF5 file '", trim(filename), "'..."

    !-- hdf5 write array
      ! Initialize FORTRAN interface.
      CALL h5open_f(error)
      ! Create a new file using default properties.
      CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)
      ! Create the dataspace.
      dims = (/size(M,1),size(M,2)/);
      CALL h5screate_simple_f(rank, dims, dspace_id, error)
      ! Create the dataset with default properties.
      ! Choose H5T_NATIVE_REAL (single precision), H5T_NATIVE_DOUBLE (double precision), H5T_NATIVE_INTEGER, H5T_NATIVE_CHARACTER
      CALL h5dcreate_f(file_id, dsetname, H5T_NATIVE_INTEGER, dspace_id, &
           dset_id, error)
       ! write dataset
       CALL h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, M, dims, error)
      ! End access to the dataset and release resources used by it.
      CALL h5dclose_f(dset_id, error)
      ! Terminate access to the data space.
      CALL h5sclose_f(dspace_id, error)
      ! Close the file.
      CALL h5fclose_f(file_id, error)
      ! Close FORTRAN interface.
      CALL h5close_f(error)
    !--
end subroutine


subroutine matrixIO_writeHDF5_Vector(M,filename)
! Write matrix to a HDF5 file.
    double precision, intent(in):: M(:)
    character(len=*), intent(in):: filename

    ! -- hdf5 read and write
    CHARACTER(LEN=4), PARAMETER :: dsetname = "dset"     ! Dataset name (doesnt matter)
    INTEGER(HID_T) :: file_id       ! File identifier
    INTEGER(HID_T) :: dset_id       ! Dataset identifier
    INTEGER(HID_T) :: dspace_id     ! Dataspace identifier
    INTEGER(HSIZE_T), DIMENSION(1) :: dims ! Dataset dimensions
    INTEGER     ::   rank =1                       ! Dataset rank
    INTEGER     ::   error ! Error flag
    ! --
    print *, "Writing matrix (size ",size(M),") to HDF5 file '", trim(filename), "'..."

    !-- hdf5 write array
      ! Initialize FORTRAN interface.
      CALL h5open_f(error)
      ! Create a new file using default properties.
      CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)
      ! Create the dataspace.
      dims = size(M,1);
      CALL h5screate_simple_f(rank, dims, dspace_id, error)
      ! Create the dataset with default properties.
      ! Choose H5T_NATIVE_REAL (single precision), H5T_NATIVE_DOUBLE (double precision), H5T_NATIVE_INTEGER, H5T_NATIVE_CHARACTER
      CALL h5dcreate_f(file_id, dsetname, H5T_NATIVE_DOUBLE, dspace_id, &
           dset_id, error)
       ! write dataset
       CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, M, dims, error)
      ! End access to the dataset and release resources used by it.
      CALL h5dclose_f(dset_id, error)
      ! Terminate access to the data space.
      CALL h5sclose_f(dspace_id, error)
      ! Close the file.
      CALL h5fclose_f(file_id, error)
      ! Close FORTRAN interface.
      CALL h5close_f(error)
    !--
end subroutine

subroutine matrixIO_writeHDF5_Vector_int(M,filename)
! Write matrix to a HDF5 file.
    integer, intent(in):: M(:)
    character(len=*), intent(in):: filename

    ! -- hdf5 read and write
    CHARACTER(LEN=4), PARAMETER :: dsetname = "dset"     ! Dataset name (doesnt matter)
    INTEGER(HID_T) :: file_id       ! File identifier
    INTEGER(HID_T) :: dset_id       ! Dataset identifier
    INTEGER(HID_T) :: dspace_id     ! Dataspace identifier
    INTEGER(HSIZE_T), DIMENSION(1) :: dims ! Dataset dimensions
    INTEGER     ::   rank =1                       ! Dataset rank
    INTEGER     ::   error ! Error flag
    ! --
    print *, "Writing matrix (size ",size(M),") to HDF5 file '", trim(filename), "'..."

    !-- hdf5 write array
      ! Initialize FORTRAN interface.
      CALL h5open_f(error)
      ! Create a new file using default properties.
      CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)
      ! Create the dataspace.
      dims = size(M,1);
      CALL h5screate_simple_f(rank, dims, dspace_id, error)
      ! Create the dataset with default properties.
      ! Choose H5T_NATIVE_REAL (single precision), H5T_NATIVE_DOUBLE (double precision), H5T_NATIVE_INTEGER, H5T_NATIVE_CHARACTER
      CALL h5dcreate_f(file_id, dsetname, H5T_NATIVE_INTEGER, dspace_id, &
           dset_id, error)
       ! write dataset
       CALL h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, M, dims, error)
      ! End access to the dataset and release resources used by it.
      CALL h5dclose_f(dset_id, error)
      ! Terminate access to the data space.
      CALL h5sclose_f(dspace_id, error)
      ! Close the file.
      CALL h5fclose_f(file_id, error)
      ! Close FORTRAN interface.
      CALL h5close_f(error)
    !--
end subroutine

end module
