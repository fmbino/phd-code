! Control space for Fortran auto-completion
!> Miscellaneous functions to manipulate arrays.
module miscFunctions


contains
!> Find the indexes whose entry is 'true'.
!! Similar to MATLAB find.
!! @param[in] v logical vector (true or false)
!! @param[out] idx positions of 'true' entries.
!! @remark If no entries are found, idx is an empty vector
subroutine find(v,idx)
    logical, dimension(:), intent(in):: v
    integer, allocatable, dimension(:), intent(out) :: idx
    integer n ! counter
    n = count(v); ! number of true elements
    allocate(idx(n));
    idx = pack([(n,n=1,size(v))],v)
end subroutine


end module
