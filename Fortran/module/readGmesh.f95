! Control space for Fortran auto-completion
!> Module to read a gmesh file. It expects a 2D triangular and conforming mesh.
!! Triangles must have 3-node only (no higher order triangles).
!! Be sure, in gmesh, to mark the boundaries and the various zones.
!! Triangle face ordering (3-node triangle)
!!        v
!!        ^
!!        |
!!        3
!!        |`\
!!        |  `\ Face 2
!!Face 3  |    `\
!!        |      `\
!!        |        `\
!!        1----------2 --> u
!!           Face 1

module readGmesh
    use miscFunctions, only: find
    implicit none

    integer, dimension(3,2), parameter, private ::     trFa = reshape( (/ &
                                                                     1, 2, 1,      &
                                                                     2, 3, 3       &
                                                                      /), (/ 3, 2 /));

    private :: find, findTriangle, findLocalFace

contains

!> Read a triangular mesh, format: gmsh 2.2.
!!  @param[in] filename
!!  @param[out] physicalEntitiesName Name of each "physical entity"
!!  @param[out] physicalEntitiesIdx Index of each "physical entity"
!!  @param[out] nodes Nodes coordinates: [x, y]
!!  @param[out] bFaces Boundary 2-node faces [physicalEntity, Node1, Node2]
!!  @param[out] triangles 3-node triangles [physicalEntity, Node1, Node2, Node2]
!!  @remark The mesh must be made from 3-node triangles, not higher order.
subroutine readGmesh_gmesh2(filename,physicalEntitiesName,physicalEntitiesIdx,nodes,bFaces,triangles)
    character(len=*), intent(in):: filename
    character(len=256),  allocatable, dimension(:),intent(out) :: physicalEntitiesName
    integer,  allocatable, dimension(:),intent(out) :: physicalEntitiesIdx
    double precision, allocatable, dimension(:,:),intent(out) :: nodes
    integer, allocatable, dimension(:,:),intent(out) :: bFaces
    integer, allocatable, dimension(:,:),intent(out) :: triangles

    integer, dimension(10) :: n_tmp ! Temporary integers
    integer :: i,n  ! Counters
    integer :: i_face, i_triangle   ! Counters (no. of faces and triangles)
    integer, allocatable, dimension(:,:) :: Faces_tmp ! Temporary faces array
    integer, allocatable, dimension(:,:) :: Triangles_tmp ! Temporary triangles array
    character(len=256) stringread   ! Temporary string

    open(10,file=filename, action="read");
        ! -------------------------------------------
        ! Read and check Meshformat
    read(10, fmt=*) stringread !$MeshFormat
    if (trim(stringread) .ne. "$MeshFormat") then
        print *, "Unexpected first line: ", trim(stringread), ", instead of $MeshFormat"
        print *, "Exiting..."
        return
    end if
    read(10, fmt=*) stringread
    print *, "Mesh format version (2.2 expected): ", stringread
    read(10, fmt=*) stringread ! $EndMeshFormat
        ! -------------------------------------------
        ! Physical Entities
    read(10, fmt=*) stringread
    if (trim(stringread) .ne. "$PhysicalNames") then
        print *, "Unexpected line: ", trim(stringread), ", instead of $PhysicalNames"
        print *, "Exiting..."
        return
    end if
    print *, stringread
    read (10,fmt=*) n
    print *, "There are", n, "physical entities"
    allocate(physicalEntitiesName(n))
    allocate(physicalEntitiesIdx(n))
    do i = 1, n
        read(10, fmt=*) n_tmp(1),physicalEntitiesIdx(i), physicalEntitiesName(i)
        print *, "Entity no.", physicalEntitiesIdx(i), "is named ", physicalEntitiesName(i)
    end do
    read(10, fmt=*) stringread
    print *, stringread
        ! -------------------------------------------
        ! Nodes
    read(10, fmt=*) stringread
    if (trim(stringread) .ne. "$Nodes") then
        print *, "Unexpected line: ", trim(stringread), ", instead of $Nodes"
        print *, "Exiting..."
        return
    end if
    read (10,fmt=*) n
    print *, "There are", n, "nodes"
    allocate(nodes(n,2)) ! [x,y] Nodes coordinates
    do i = 1, n
            ! Entry example: ( n | x_n y_n z_n)
        read(10, fmt=*) n_tmp(1),nodes(i,1),nodes(i,2)
        !print *, "Node no.", i, "x= ", Nodes(i,1),  "y= ", Nodes(i,2)
    end do
    read(10, fmt=*) stringread
    print *, stringread
        ! -------------------------------------------
        ! Triangles and Faces ("Elements")
    read(10, fmt=*) stringread
    if (trim(stringread) .ne. "$Elements") then
        print *, "Unexpected line: ", trim(stringread), ", instead of $Elements"
        print *, "Exiting..."
        return
    end if
    read (10,fmt=*) n
    print *, "There are", n, "elements"
    print *, "Expected elements:"
    print *, "  3-node triangles (id:2)"
    print *, "  2-node faces (id:1)"
    allocate(Faces_tmp(n,3)) ! same format as Faces (but larger)
    allocate(Triangles_tmp(n,4)) ! same format as Triangles (but larger)
    i_face = 0; ! number of 2-node faces
    i_triangle = 0; ! number of 3-node triangles
    do i = 1,n
            ! Read line (assume 7 columns)
        read(10,fmt=*) n_tmp(1), n_tmp(2), n_tmp(3), n_tmp(4), n_tmp(5), n_tmp(6), n_tmp(7)
            ! Then, several cases must be distinguished, depending on the element at stake
        if (n_tmp(2)==1) then ! Element type = 1 (2-node line)
            i_face = i_face + 1;
            backspace(10) ! rewind by a line
            ! Ex: (10 | 1 | 2 3 1 | 13 2) - > Physical entity: 3, Nodes: 13,2
            read(10,fmt=*) n_tmp(1), n_tmp(2), n_tmp(3), n_tmp(4), n_tmp(5), n_tmp(6), n_tmp(7)
            Faces_tmp(i_face,1) = n_tmp(4);
            Faces_tmp(i_face,2) =  n_tmp(6);
            Faces_tmp(i_face,3) = n_tmp(7);
            !print *, "n°i=",i," line (", Faces_tmp(i_face,2), ",", Faces_tmp(i_face,3), ") Entity:", Faces_tmp(i_face,1)
        else if (n_tmp(2)==2) then ! Element type = 2 (3-node triangle)
            i_triangle = i_triangle + 1;
            backspace(10)
            ! (10 | 2 | 2 5 1 | 111 235 334) -> Physical entity: 5, Nodes: 111 235 334
            read(10,fmt=*) n_tmp(1), n_tmp(2), n_tmp(3), n_tmp(4), n_tmp(5), n_tmp(6), n_tmp(7), n_tmp(8)
            Triangles_tmp(i_triangle,1) = n_tmp(4);
            Triangles_tmp(i_triangle,2) =  n_tmp(6);
            Triangles_tmp(i_triangle,3) = n_tmp(7);
            Triangles_tmp(i_triangle,4) = n_tmp(8);
            !print *, "n°i=",i," line (", Triangles_tmp(i_triangle,2:4) ,") Entity:", Triangles_tmp(i_triangle,1)
        else
            print *, "Unknown element:", n_tmp(2), "Expected: 2 or 1"
        end if
    end do
    print *, "Reading finished."; print *, "Closing file..."
    close( unit=10 );
        ! Put in result in arrays with right dimensions
    allocate(bFaces(i_face,3));
    bFaces=Faces_tmp(1:i_face,:)
    allocate(triangles(i_triangle,4));
    triangles=Triangles_tmp(1:i_triangle,:)
    deallocate(Faces_tmp);
    deallocate(Triangles_tmp);
end subroutine


!> Build the connectivity tables for the triangles. "The element i, local face
!! j (1,2,3) connects to element k, local face l (1,2,3)" translates as
!! EToE(i,j)= k and EToF(i,j) = l.
!! Convention for boundary face: EToE(i,j)= 0 and EToF(i,j) = 0.
!!  @param[out] EToE Element to element connectivity table
!!  @param[out] EToF Element to (local) face connectivity table
!!  @param[in] triangles List of triangles
!!      [physicalEntity, Node1, Node2, Node2]
!!  @remark The triangular mesh is assumed conforming.
subroutine buildElementConnectivity(EToE,EToF,triangles)
    integer, allocatable, dimension(:,:), intent(out) :: EToE
    integer, allocatable, dimension(:,:), intent(out) :: EToF
    integer, dimension(:,:),intent(in) :: triangles

    integer :: i,j,l
    integer, allocatable, dimension(:) :: k
    integer, dimension(2) :: face

    allocate(EToE(size(triangles,1),3))
    allocate(EToF(size(triangles,1),3))

    do i = 1, size(triangles,1) ! loop over each triangle
        do j = 1, 3 ! loop over each local face
            face = triangles(i,1+TrFa(j,:));
                ! which element has the same 2-node face?
            call findTriangle(face,triangles(:,2:4),k);
            if ((maxval(k)==0) .or.(size(k)>2)) then
                print *, "Incorrect mesh: face (el. ", i, "face ",j,") belongs to 0 or more than two triangles."
            else if (size(k)==1) then ! only one element: boundary face
                EToE(i,j) = 0; EToF(i,j)=0; ! convention
            else ! interior face
                k = pack(k,k .ne. i)
                call findLocalFace(face,triangles(k(1),2:4),l);
                EToE(i,j) = k(1); EToF(i,j)=l;
            end if
        end do
    end do
end subroutine


!> Build the connectivity tables for the boundary faces. "The boundary face i
!! connects to element k, local face l" translates as
!! FToE(i)=k and FToF(i) = l
!!  @param[out] FToE Face to element connectivity table
!!  @param[out] FToF Face to (local) face connectivity table
!!  @param[in] bFaces List of boundary 2-node faces
!!      [physicalEntity, Node1, Node2]
!!  @param[in] triangles List of triangles
!!      [physicalEntity, Node1, Node2, Node2]
!!  @remark The triangular mesh is assumed conforming.
subroutine buildBoundaryFaceConnectivity(FToE,FToF,bFaces,triangles)

    integer, allocatable, dimension(:), intent(out) :: FToE
    integer, allocatable, dimension(:), intent(out) :: FToF
    integer, dimension(:,:),intent(in) :: bFaces
    integer, dimension(:,:),intent(in) :: triangles

    integer :: i ! counter
    integer :: l ! local face number
    integer, allocatable, dimension(:) :: k
    integer, dimension(2) :: face

    allocate(FToE(size(bFaces,1)))
    allocate(FToF(size(bFaces,1)))

    do i = 1, size(bFaces,1) ! loop over each boundary face
        face = bFaces(i,2:3);
            ! which element has the same two nodes?
        call findTriangle(face,triangles(:,2:4),k);
        if ((maxval(k)==0) .or.(size(k)>1)) then
            print *, "Incorrect mesh: boundary face ", i," belongs to 0 or more than one triangle."
        else
            ! which local face?
            call findLocalFace(face,triangles(k(1),2:4),l);
            ! Boundary face i connects to element k, local face l
            FToE(i)=k(1); FToF(i)=l;
        end if
    end do
end subroutine

!> Find the number <k> of the triangle which possess <face>.
!! @param[in] face 2-node face
!! @param[in] triangles list of 3-node triangles
!! @param[out] {k triangle number in <triangles>. 0 if no match.}
subroutine findTriangle(face,triangles,k)
    integer, dimension(2), intent(in) :: face
    integer, dimension(:,:),intent(in) :: triangles
    integer, allocatable, dimension(:), intent(out) :: k

    logical, dimension(:,:), allocatable :: B1,B2
    allocate(B1(size(triangles,1),size(triangles,2)))
    allocate(B2(size(triangles,1),size(triangles,2)))

    if (size(triangles,2).ne.3) then
        write(*,*) '<findTriangle>: triangles should have 3 columns'
    end if
    B1 = triangles == face(1);
    B2 = triangles == face(2);
    call find(count(B1.or.B2,dim=2)==2,k);
    if (size(k)==0) then
        k=0;
    end if
end subroutine

!> Find the number <l> (1,2,3) of the triangle face which possess <face>.
!! @param[in] face 2-node face
!! @param[in] triangle 3-node triangle
!! @param[out] {l local face number (1,2,3) (see local face numbering).
!! 0 if no match is found.}
subroutine findLocalFace(face,triangle,l)
    integer, dimension(2), intent(in) :: face
    integer, dimension(3),intent(in) :: triangle
    integer, intent(out) :: l

    integer, dimension(2) :: localFace
    integer :: i
    l = 0; ! value if no face found
    do i = 1, 3
        localFace = triangle(trFa(i,1:2));

        if  (((face(1)==localFace(1)) .and. (face(2)==localFace(2))) &
            .or. &
            ((face(1)==localFace(2)) .and. (face(2)==localFace(1)))) then
            l=i;
       end if
    end do
end subroutine

end module
