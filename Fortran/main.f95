program main
use matrixTest
use rungekutta
use dispmodule
use readGmesh
use hdf5

implicit none

double precision, dimension(:), allocatable :: x0;
double precision, dimension(:), allocatable :: t
double precision, dimension(:,:), allocatable :: x, x_ex
double precision :: tf, dt

    double precision, allocatable, dimension(:,:) :: Nodes
    integer, allocatable, dimension(:,:) :: Faces
    integer, allocatable, dimension(:,:) :: Triangles
    character(len=256),  allocatable, dimension(:) :: PhysicalEntities_name
    integer,  allocatable, dimension(:) :: PhysicalEntities_idx
    integer, allocatable, dimension(:,:) :: EToE, EToF


    integer, allocatable, dimension(:) :: v
    integer, allocatable, dimension(:) :: idx
    integer :: i
! Test matrix I/O
! call matrixTest_main();
! Test Runge-Kutta
!n = 5;
!tf = 2;
!dt = 1e-1;
!allocate(x0(n));
!x0 = (/ (i, i=1,n) /);
!call RK_solve(x0,Fun_g,t,x,tf,dt);
!call disp('Time t=',t);
!call disp('State x=',x);
!allocate(x_ex(size(x,1),size(x,2)))
!do i = 1, size(x,2)
!    x_ex(:,i) = x0*exp(-t(i))
!end do
!call disp('State error dx=',maxval(abs(x-x_ex),2));
!call matrixIO_writeCSV(x,'stateX.csv');
!call matrixIO_writeCSV(t,'timeT.csv');
!call matrixIO_writeHDF5(x,'stateX.h5');
!call matrixIO_writeHDF5(t,'stateT.h5');

    ! Test read_mesh
call readGmesh_gmesh2("mesh.msh",PhysicalEntities_name,PhysicalEntities_idx,Nodes,Faces,Triangles)
!call matrixIO_writeHDF5(Nodes,'mesh_nodes.h5');
!call matrixIO_writeHDF5(Faces,'mesh_faces.h5');
!call matrixIO_writeHDF5(Triangles,'mesh_triangles.h5');
call buildElementConnectivity(EToE,EToF,Faces,Triangles)
call matrixIO_writeHDF5(EToE,'mesh_EToE.h5');
end program main
