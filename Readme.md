This repository contains the source code associated with the PhD thesis:

> Analysis and Discretization of Time-Domain Impedance Boundary Conditions in Aeroacoustics. Florian Monteghetti. ISAE-SUPAERO, Université de Toulouse, 2018. Link: [tel-01910643](https://theses.hal.science/tel-01910643/).


All the relevant code is in the `MATLAB` folder. The folders `MAPLE` and `Fortan` code contains snippets of code in MAPLE and Fortran, they can be ignored.

Some scripts use experimental data, which are provided in the `data` folder.
