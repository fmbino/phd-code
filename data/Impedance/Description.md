**NASA-GIT-2005--AIAA2005-2853**

Educed impedance data from the AIAA paper "Benchmark Data for Evaluation of Aeroacoustic Propagation Codes with Grazing Flow".
Ceramic Tubular liner (CT57).
- `CT57_NoFlow_2DFEM.csv`
- `CT57_NoFlow_Q3DFEM.csv`
  => Data from table 9.
  
**NASA-GFIT-2013--AIAA2013-2273**

Educed impedance data from the AIAA paper "ONERA-NASA Cooperative Effort on Liner Impedance Eduction".
Micro-perforate liner (perforated plate over a honeycomb cell).

- `NASAPred_ONERA Samples.xlsx`
  => Executive source of measurement for the NASA measurement of the non-linear liner. Contains SPL 120dB & 130dB and M 0, 0.2 & 0.3.
- `NASA_GFIT_MicroPerf_120dB_NoFlow_FullWidth.csv`
  => Extracted from `NASAPred_ONERA Samples.xlsx`: contains the full-width case only.
  => Error on resistance and reactance have been ESTIMATED from the Figure 14 of the paper, which gives errors estimates for the impedance eduction made by the ONERA code.
- `Primus2013_Figure14_Values_120dB.csv`
  => Data extracted from the graph of figure 14, to get estimate of the uncertainity values for the ONERA eduction code.
