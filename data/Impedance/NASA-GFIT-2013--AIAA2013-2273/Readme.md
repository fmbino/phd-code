The xlsx files contains NASA measurement on the micro-perforated liner described in AIAA2013-2273.
To enable a convenient use in MATLAB, part of the data has been copied to csv files.
The csv files contain the following quantities:
- Identified impedance.
- SPL along wall (experimental)
- SPL along wall (numerical, 2D FEM solution of Convected Helmholtz Equation with Ingard-Myers)
