NASA Grazing Impedance Tube
"11th AIAA/CEAS Aeroacoustics Conference (26th AIAA Aeroacoustics Conference)
23 - 25 May 2005, Monterey, California
AIAA 2005-2853
Benchmark Data for Evaluation of Aeroacoustic Propagation
Codes with Grazing Flow
M. G. Jones,  W. R. Watson† and T. L. Parrott ‡
NASA Langley Research Center, Hampton, VA 23681-2199, USA"

**Physical dimensions of Grazing Impedance Tube**

- Cross-section: (y,z)
- Longitudinal: x.
- Liner on top wall.
- Source at x=0m (OK)
- Anechoic termination at x=812.8mm (OK)
- Liner length = 406mm (OK)
- Square cross-section: 51mm (OK)
- x = 356 mm is the middle of the lined portion of the duct. (OK)
- Hence, liner is on [153,559] mm.


