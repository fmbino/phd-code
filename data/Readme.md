This folder contains data that complements the MATLAB code.

- `Artimon-DG.pdf` partially describes the DG implemented in MATLAB/ArtimonDG.

Each subfolder is described below:

- `Impedance` contains experimental impedance data. This data is used by scripts that build oscillatory-diffusive representations.

- `JCPIBCDataFolder` contains data associated with the JCP article, both numerical and experimental.

- `Mesh` contains mesh and geometry (GMSH format). Theses meshes are used by the DG scripts.


