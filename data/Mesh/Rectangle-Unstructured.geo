/*********************************************************************
 *  Gmsh geometry for a Rectangle with unstructured meshing.
 *  From Gmsh tutorial 1.
 *  Usage: gmsh -2 file.geo -o mesh.msh
 *********************************************************************/

// -- Inputs variables
// Rectangle of length lx and ly
lx = 1;
ly = 1;
// Typical triangle size
lc = 3e-1;

Point(1) = {0, 0, 0,lc};
Point(2) = {lx, 0,  0,lc} ;
Point(3) = {lx, ly, 0,lc} ;
Point(4) = {0,  ly, 0,lc} ;
Line(1) = {1,2} ;
Line(2) = {3,2} ;
Line(3) = {3,4} ;
Line(4) = {4,1} ;
Line Loop(5) = {4,1,-2,3};
Plane Surface(6) = {5};
	// Definition of physical elements
Physical Line("bottom wall") = {1} ;
Physical Line("right wall") = {2} ;
Physical Line("top wall") = {3} ;
Physical Line("left wall") = {4} ;
Physical Surface("air") = {6} ;
