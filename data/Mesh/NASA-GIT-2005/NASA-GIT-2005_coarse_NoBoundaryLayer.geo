/*********************************************************************
 *  Gmsh geometry for a Duct with two boundary layers
 *  From Gmsh tutorial 1 & 10
 *  Usage: gmsh -2 file.geo -o mesh.msh
 *********************************************************************/


/* NASA Grazing Impedance Tube (2005)
AIAA 2005-2853 
Height: 0.051m
Inlet: x=0m
Outlet: x=0.8128 m
*/
// -- Inputs variables
// Rectangle of length lx and ly
//lx = 9; // 50 period without relection at 1kHz
lx = 0.82;
ly = 0.051;

// Typical triangle size
lc = ly/8;

    // -----------------
    // Mesh with boundary layer
Point(1) = {0, 0, 0,lc};
Point(2) = {lx, 0,  0,lc} ;
Point(3) = {lx, ly, 0,lc} ;
Point(4) = {0,  ly, 0,lc} ;
Line(1) = {1,2} ;
Line(2) = {3,2} ;
Line(3) = {3,4} ;
Line(4) = {4,1} ;
Line Loop(5) = {4,1,-2,3};
Plane Surface(6) = {5};
	// Definition of physical elements
Physical Line("bottom wall") = {1} ;
Physical Line("right wall") = {2} ;
Physical Line("top wall") = {3} ;
Physical Line("left wall") = {4} ;
Physical Surface("air") = {6} ;
