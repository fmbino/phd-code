/*********************************************************************
 *  Gmsh geometry for a Duct with two boundary layers
 *  From Gmsh tutorial 1 & 10
 *  Usage: gmsh -2 file.geo -o mesh.msh
 *********************************************************************/


/* NASA Grazing Impedance Tube (2005)
AIAA 2005-2853 
Height: 0.051m
Inlet: x=0m
Outlet: x=0.8128 m
*/
// -- Inputs variables
// Rectangle of length lx and ly
lx = 4.3; // no reflection at 500Hz
//lx = 1;
ly = 0.051;

// Typical triangle size
lc = ly/2;


Point(1) = {0, 0, 0};
Point(2) = {lx, 0,  0} ;
Point(3) = {lx, ly, 0} ;
Point(4) = {0,  ly, 0} ;
Line(1) = {1,2} ;
Line(2) = {3,2} ;
Line(3) = {3,4} ;
Line(4) = {4,1} ;
Line Loop(5) = {4,1,-2,3};
Plane Surface(6) = {5};
	// Definition of physical elements
Physical Line("bottom wall") = {1} ;
Physical Line("right wall") = {2} ;
Physical Line("top wall") = {3} ;
Physical Line("left wall") = {4} ;
Physical Surface("air") = {6} ;
    // Boundary layer: hwall * ratio^(dist/hwall)
	// First boundary layer (top-bottom)
Field[1] = BoundaryLayer;
Field[1].EdgesList = {1,2,3,4};
Field[1].thickness = ly/2; // Maximal thickness of the boundary layer (default: 0.01)
Field[1].ratio = 1.3; // Size Ratio Between Two Successive Layers (default: 1.1)
Field[1].hfar = lc; //  Element size far from the wall (default: 1)
Field[1].hwall_n = lc/3.5; // Mesh Size Normal to the The Wall (default: 0.1)
Field[1].hwall_t = 0.1; // 
Background Field = 1;
    // Second boundary layer (left-right)
//Field[2] = BoundaryLayer;
//Field[2].EdgesList = {2,4};
//Field[2].thickness = 0.1; // Maximal thickness of the boundary layer (default: 0.01)
//Field[2].ratio = 1.1; // Size Ratio Between Two Successive Layers (default: 1.1)
//Field[2].hfar = lc; //  Element size far from the wall (default: 1)
//Field[2].hwall_n = lc/2; // Mesh Size Normal to the The Wall (default: 0.1)
//Field[2].hwall_t = 0.1; // 
	// Finally, let's use the minimum of all the fields as the background mesh field
//Field[3] = Min;
////Field[3].FieldsList = {1,2};
//Background Field = 1;
	// If the boundary mesh size was too small, we could ask not to extend the
	// elements sizes from the boundary inside the domain:
Mesh.CharacteristicLengthExtendFromBoundary = 0;
