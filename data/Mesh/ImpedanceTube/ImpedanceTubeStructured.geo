/*********************************************************************
 *  Gmsh geometry for a Rectangle with structured meshing.
 *  From Gmsh tutorial 1.
 *  Usage: gmsh -2 file.geo -o mesh.msh
 *********************************************************************/

// -- Inputs variables
// Rectangle of length lx and ly
lx = 1;
ly = 0.01;
x0 = 0;
y0 = 0;
// Number of cell on each line
Nx = 300;
Ny = 1; // Ny = ly/lx * Nx

Point(1) = {x0, y0, 0};
Point(2) = {x0+lx, y0,  0} ;
Point(3) = {x0+lx, y0+ly, 0} ;
Point(4) = {x0,  y0+ly, 0} ;
Line(1) = {1,2} ;
Line(2) = {3,2} ;
Line(3) = {3,4} ;
Line(4) = {4,1} ;
Line Loop(5) = {4,1,-2,3};
Plane Surface(6) = {5};
	// Structured meshing.
Transfinite Line{1,3} = Nx+1;
Transfinite Line{4,-2} = Ny+1;
Transfinite Surface(6) = {1,2,3,4} Right;

	// Definition of physical elements
Physical Line("bottom wall") = {1} ;
Physical Line("right wall") = {2} ;
Physical Line("top wall") = {3} ;
Physical Line("left wall") = {4} ;
Physical Surface("air") = {6} ;
