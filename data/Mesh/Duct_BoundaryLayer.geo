/*********************************************************************
 *  Gmsh geometry for a Duct with two boundary layers
 *  From Gmsh tutorial 1 & 10
 *  Usage: gmsh -2 file.geo -o mesh.msh
 *********************************************************************/

// -- Inputs variables
// Rectangle of length lx and ly
lx = 6;
ly = 0.060606061;

Point(1) = {0, 0, 0};
Point(2) = {lx, 0,  0} ;
Point(3) = {lx, ly, 0} ;
Point(4) = {0,  ly, 0} ;
Line(1) = {1,2} ;
Line(2) = {3,2} ;
Line(3) = {3,4} ;
Line(4) = {4,1} ;
Line Loop(5) = {4,1,-2,3};
Plane Surface(6) = {5};
	// Definition of physical elements
Physical Line("bottom wall") = {1} ;
Physical Line("right wall") = {2} ;
Physical Line("top wall") = {3} ;
Physical Line("left wall") = {4} ;
Physical Surface("air") = {6} ;
	// First boundary layer: hwall * ratio^(dist/hwall)
Field[1] = BoundaryLayer;
Field[1].EdgesList = {1};
Field[1].thickness = ly/3; // Maximal thickness of the boundary layer (default: 0.01)
Field[1].ratio = 1.1; // Size Ratio Between Two Successive Layers (default: 1.1)
Field[1].hfar = ly/5; //  Element size far from the wall (default: 1)
Field[1].hwall_n = ly/10; // Mesh Size Normal to the The Wall (default: 0.1)
Field[1].hwall_t = 0.1; // 
	// Second boundary layer
Field[2] = BoundaryLayer;
Field[2].EdgesList = {3};
Field[2].thickness = ly/3;
Field[2].ratio = 1.1;
Field[2].hfar = ly/5;
Field[2].hwall_n = ly/10;
Field[2].hwall_t = 0.1;
	// Finally, let's use the minimum of all the fields as the background mesh field
Field[3] = Min;
Field[3].FieldsList = {1,2};
Background Field = 3;
	// If the boundary mesh size was too small, we could ask not to extend the
	// elements sizes from the boundary inside the domain:
Mesh.CharacteristicLengthExtendFromBoundary = 0;
